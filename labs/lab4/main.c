#include <dispatcher.h>

int main(void){
    setlocale(LC_CTYPE, "");
    initscr();
    start_color();
    // refresh();    
    cbreak();
    noecho();
    curs_set(INVISIBLE_CURSOR);
    mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);   
    Key_detectMouseMovements();
    mouseinterval(0);
    attrs = (const PBMenuAttrs){ // global constant (aha)
        .attrs = (PanMenuAttrs){
            .fore = COLOR_PAIR(init_pair(1, red(5), black(5))) | A_REVERSE,
            .back = COLOR_PAIR(init_pair(2, blue(3), black(5))),
            .grey = COLOR_PAIR(init_pair(3, white(3), black(5))),
            .selected = COLOR_PAIR(1),
            .reselected = COLOR_PAIR(1) | WA_BOLD
        },
        .frame = &WF_DOUBLE
    };

    noFrameAttrs =  (const PBMenuAttrs){ // global constant (aha)
        .attrs = (PanMenuAttrs){
            .fore = COLOR_PAIR(init_pair(1, red(5), black(5))) | A_REVERSE,
            .back = COLOR_PAIR(init_pair(2, blue(3), black(5))),
            .grey = COLOR_PAIR(init_pair(3, white(3), black(5))),
            .selected = COLOR_PAIR(1),
            .reselected = COLOR_PAIR(1) | WA_BOLD
        },
        .frame = NULL
    };

    EventSystem_init();
    
    // add event handlers
    EventSystem_addHandler(EventHandler_new(NULL, NULL, MainMenu_dispatcher));
    EventSystem_addHandler(EventHandler_new(NULL, NULL, Input_listener));

    // start infinite event loop
    EventSystem_loop();
    // cleanup event systtem


    EventSystem_cleanup();
    Key_undetectMouseMovements();
    endwin();
    system("clear");
    return EXIT_SUCCESS;
}

void MainMenu_labels(int state){
    static Panel * title = NULL;
    switch (state){
        case INIT:{
            title = Panel_new(props(9, 126), loc (8 , lineCenter(126)), &WF_SINGLE, GLOBAL_ATTRS);
            ArtSetup_printExpilicit(title->win, &ART_SETUP, 
                L" :::::::::  :::::::::   ::::::::  :::     ::: ::::::::::: :::::::::  :::::::::: :::::::::  :::::::::: :::::::::  :::::::::  "
                 " :+:    :+: :+:    :+: :+:    :+: :+:     :+:     :+:     :+:    :+: :+:        :+:    :+: :+:        :+:    :+: :+:    :+: "
                 " +:+    +:+ +:+    +:+ +:+    +:+ +:+     +:+     +:+     +:+    +:+ +:+        +:+    +:+ +:+        +:+    +:+ +:+    +:+ "
                 " +#++:++#+  +#++:++#:  +#+    +:+ +#+     +:+     +#+     +#+    +:+ +#++:++#   +#++:++#:  +#++:++#   +#++:++#:  +#++:++#:  "
                 " +#+        +#+    +#+ +#+    +#+  +#+   +#+      +#+     +#+    +#+ +#+        +#+    +#+ +#+        +#+    +#+ +#+    +#+ "
                 " #+#        #+#    #+# #+#    #+#   #+#+#+#       #+#     #+#    #+# #+#        #+#    #+# #+#        #+#    #+# #+#    #+# "
                 " ###        ###    ###  ########      ###     ########### #########  ########## ###    ### ########## ###    ### ###    ### "
            );
            Panel_refreshAll();
            return;
        }
        case FREE:{
            Panel_free(title);
            title = NULL;
            return;
        }
    }  
}

void MainMenu_buttons(int state, wint_t input){
    static PBMenu * menu = NULL;
    switch (state){
        case REACT:{
            if (PBMenu_driver(menu, BM_REACT, input) == true){ // case updated menu
                doupdate();
                if      (PBMenu_isButtonSelected(menu, 0))
                    emit(Goto_MenuList);

                else if (PBMenu_isButtonSelected(menu, 1))
                    emit(Goto_MenuFile);

                else if (PBMenu_isButtonSelected(menu, 2))
                    emit(MainMenu_Exit);
            }
            return;
        }
        case INIT:{
            const Proportions BUTTONS_PROPS = props(3, 14);
            const Location BUTTONS_LOC = getScreenCenter(BUTTONS_PROPS);
            menu = _PBMenu_new(&attrs,  BUTTONS_PROPS, BUTTONS_LOC, VLAYOUT,

                                  L" New list ", 
                                  L" Open file ", 
                wstr(UNI_WARNING) L"   Exit   ");

            PBMenu_post(menu);
            return;
        }
        case RESTART:{
            PBMenu_driver(menu, PBM_RESTART, '\0');
            doupdate();
            return;
        }
        case FREE:{
            PBMenu_unpost(menu);
            PBMenu_freeWhole(menu);
            menu = NULL;
            return;
        }
    }
    
}




typedef enum __MainMenuSt{
    MainMenuSt_Idling,
    MainMenuSt_StartShuttingDown,
    MainMenuSt_ShuttingDown,
    MainMenuSt_StopShuttingDown
} MainMenuSt;

static MainMenuSt MainMenu_getNextState(MainMenuSt currentState, Event * event);

// MAIN MENU DISPATCHER
void MainMenu_dispatcher(EventHandler * self, Event * event){
    wint_t signal = '\0';
    switch (event->type){
        case StartEventTypeId:
        case Goto_MainMenu:{
            MainMenu_labels(INIT);
            MainMenu_buttons(INIT, '\0');
            doupdate();
            return;
        }
        case Goto_MenuList:{

            EventSystem_removeHandler(self);
            EventSystem_addHandler(EventHandler_new(List_new(), (Destructor)List_freeProviders, MenuList_dispatcher));
            
            MainMenu_buttons(FREE, '\0'); 
            MainMenu_labels(FREE);
            return;
        }
        case Goto_MenuFile: {
            EventSystem_removeHandler(self);
            EventSystem_addHandler(EventHandler_new(NULL, NULL, MenuFile_dispatcher));
            
            MainMenu_buttons(FREE, '\0'); 
            MainMenu_labels(FREE);
            return;
        }
        case ExitEventTypeId:{
            MainMenu_buttons(FREE, '\0'); 
            MainMenu_labels(FREE);
            return;
        }
        case UserSignal: signal = *(wint_t *)event->data;
        case MainMenu_Exit:{
            static MainMenuSt state = MainMenuSt_Idling;
            state = MainMenu_getNextState(state, event);
            switch (state){
                case MainMenuSt_Idling:{
                    MainMenu_buttons(REACT, signal);
                    return;
                }
                case MainMenuSt_StartShuttingDown:{
                    Dispather_initShutdownDialog();
                    return;
                }
                case MainMenuSt_ShuttingDown:{
                    switch (Dispatcher_reactDialog(signal)){
                        case CHOOSE_OK:{
                            state = MainMenuSt_Idling;
                            Dispatcher_freeDialog();
                            EventSystem_exit();
                            return;
                        }
                        case CHOOSE_CANCEL:{
                            state = MainMenuSt_Idling;
                            Dispatcher_freeDialog();
                            MainMenu_buttons(RESTART, '\0');
                            return;
                        }
                        case CHOOSE_IDLE:{
                            return;   
                        }
                    }
                }
                case MainMenuSt_StopShuttingDown:{
                    Dispatcher_freeDialog();
                    MainMenu_buttons(RESTART, '\0');
                    return;
                }
            }
        }
    }
}

static MainMenuSt MainMenu_getNextState(MainMenuSt currentState, Event * event){
    Debug_exists(event);
    if (event->type == MainMenu_Exit) return MainMenuSt_StartShuttingDown;
    //
    switch (currentState){
        case MainMenuSt_Idling:
        case MainMenuSt_StopShuttingDown:{
            switch (*(wint_t *)event->data){
                case KEY_ESCAPE: return MainMenuSt_StartShuttingDown;
                default:  return MainMenuSt_Idling;
            }
        }
        case MainMenuSt_StartShuttingDown:
        case MainMenuSt_ShuttingDown:{
            switch (*(wint_t *)event->data){
                case KEY_ESCAPE: return MainMenuSt_StopShuttingDown;
                default:  return MainMenuSt_ShuttingDown;
            }
        }
    }
    return MainMenuSt_Idling;
}
