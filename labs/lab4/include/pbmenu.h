#pragma once


#define _XOPEN_SOURCE_EXTENDED 

#include <stdbool.h>
#include <ncursesw/curses.h>
#include <pan_button.h>
#include <wbutton_ext.h>
#include <wmenu.h>
#include <list.h>
#include <pan_menu.h>
#include <frame.h>

typedef PanItem_Hook PanButton_Hook;
typedef struct __PBMenuAttrs {
    PanMenuAttrs  attrs; 
    const Frame * frame;    
} PBMenuAttrs; 

typedef struct __PBMenu PBMenu;
typedef WItem_Hook WButton_Hook;

unsigned int PBMenu_getButtonsCount(PBMenu * self);
PanButton_Hook PBMenu_getHookAt(PBMenu * self, unsigned int index);
void *       PBMenu_getPtrAt(PBMenu * self, unsigned int index);
bool         PBMenu_getCyclic(PBMenu * self);

PanButton_Hook PBMenu_setHookAt(PBMenu * self, unsigned int index, PanButton_Hook hook);
void *       PBMenu_setPtrAt(PBMenu * self, unsigned int index, void * ptr);
bool         PBMenu_setCyclic(PBMenu * self, bool isCyclic);
    


#define PBM_REACT           (0UL)
#define PBM_RESTART         (1UL)
#define PBM_SET_BUTTON_GREY (1UL << ((sizeof(unsigned long int) * 8) - 1)) 
#define PBM_DESELECT_BUTTON (1UL << ((sizeof(unsigned long int) * 8) - 2))    
#define PBM_SELECT_BUTTON   (1UL << ((sizeof(unsigned long int) * 8) - 3))        
#define PBM_TOGGLE_BUTTON   (1UL << ((sizeof(unsigned long int) * 8) - 4))     
#define PBM_REFRESH_BUTTON  (1UL << ((sizeof(unsigned long int) * 8) - 5))  
#define PBM_JUMP_TO_BUTTON  (1UL << ((sizeof(unsigned long int) * 8) - 6))   

/*
 *  use WM_JUMP_TO and WM_REFRESH option by bitwise OR'ing the index with it
 *  Example: driverBmenu(WM_CANCEL_CHOICE | index); driverWmenu(WB_SET_BUTTON_GREY | index);
 */

#define VERTICAL_PBM   true
#define HORIZONTAL_PBM false

#define _PBMenu_new(attrs, props, location, layout, ...)\
  PBMenu_new(wstrs(__VA_ARGS__), sizeof((wstrs(__VA_ARGS__))) / sizeof(wstrs(__VA_ARGS__))[0], attrs, props, location, layout)

PBMenu * PBMenu_new (wchar_t ** buttonLabels,
                unsigned int    size, 
                 PBMenuAttrs *  attrs,
                 Proportions    props,
                    Location    location,
                     BLayout    layout);



PBMenu * PBMenu_chNew (char ** buttonLabels,
               unsigned int    size, 
                PBMenuAttrs *  attrs,
                Proportions    props,
                   Location    location,
                    BLayout    layout);



PBMenu * PBMenu_strNew (List *  buttonLabels,
                 PBMenuAttrs *  attrs,
                 Proportions    props,
                    Location    location,
                     BLayout    layout);
                    
                   
void PBMenu_free(PBMenu * self);
void PBMenu_freeWhole(PBMenu * self);

PBMenu * PBMenu_newBy(List * buttons, unsigned int size, PBMenuAttrs * attrs);


// m - versions are supposed to work only with mouse events

void PBMenu_post  (PBMenu * menu);
void PBMenu_mpost(PBMenu * menu);

void PBMenu_unpost(PBMenu * menu);

                 
bool PBMenu_driver (PBMenu * menu, unsigned long option, wint_t input);
bool PBMenu_mdriver(PBMenu * menu, unsigned long option, wint_t input);
 
signed long PBMenu_getEnclosedButtonIndex(PBMenu * self, wint_t signal);

bool PBMenu_isButtonSelected(PBMenu * self, unsigned int index);
unsigned int PBMenu_getCurIndex(PBMenu * self);

PBMenu * PBMenu_strRenew(PBMenu * self, List * options);

signed long PBMenu_getSelectedButton(PBMenu * self);


typedef enum PBMenuSt{
    PBMenuSt_MOUSE_OUT,
    PBMenuSt_CREATED,
    PBMenuSt_ADVANCED_SELECTOR,
    PBMenuSt_ADV_AND_TOGGLED,
    PBMenuSt_DISCARDED_SELECTOR,
    PBMenuSt_IDLED,
    PBMenuSt_POSTED,
    PBMenuSt_TOGGLED,
    PBMenuSt_UNPOSTED,
    PBMenuSt_RESTARTED,
    PBMenuSt_REFRESHED
} PBMenuSt;

PBMenuSt PBMenu_getState(PBMenu * self);

WINDOW * PBMenu_getButtonWin(PBMenu * self, size_t i);

void PBMenu_rewriteButton(PBMenu * self, size_t i, wchar_t * newText);