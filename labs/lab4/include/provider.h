
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// PROVIDERS PROCCESSING FUNCTIONS

#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <mem.h>
#include <sstring.h>
#include <list.h>
#include <data_cloud.h>
#include <csv.h>

#define ACCURACY 0.00999999999

typedef struct __Provider {
    // public:

    String * name;

    double tariff;   // average tariff dollars/(Mbit per sec)
    double loss;     // average precentage packetloss at providers node per month

    size_t users;
    DataCloud * cloud; // data cloud storage providing

} Provider;           



Provider * Provider_new(void);
Provider * Provider_newWhole(String * name,
                          DataCloud * cloud, 
                             double   tariff, 
                             double   loss, 
                             size_t   users);

void Provider_free(Provider * self);
void Provider_freeWhole(Provider * self);

String * Provider_nameAt(Provider * self);
double Provider_tariffAt(Provider * self);
double Provider_lossAt(Provider * self);
size_t Provider_usersAt(Provider * self);
DataCloud * Provider_cloudAt(Provider * self);

String * Provider_setName(Provider * self, String * name);
double Provider_setTariff(Provider * self, double tariff);
double Provider_setLoss(Provider * self, double loss);
size_t Provider_setUsers(Provider * self, size_t users);
DataCloud * Provider_setCloud(Provider * self, DataCloud * cloud);

bool Provider_hasCloud(Provider * self);
bool Provider_hasName(Provider * self);

bool Provider_isValidTariff(double tariff);
bool Provider_isValidLoss(double loss);
CsvRow * Provider_toNewCsvRow(Provider * self);

Provider * Provider_stringsToNewProvider(const List * strings);