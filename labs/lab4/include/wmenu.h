#pragma once

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>
#include <ncursesw/panel.h>

#include <frame.h>
#include <proportions.h>
#include <location.h>
#include <witem.h>


// A ➞ B = ¬A ∪ B
#define imply(presupposition, conclusion) (!(presupposition) || (conclusion))

typedef struct __WMenuAttrs {
    attr_t    fore;
    attr_t    back;
    attr_t    grey;
    attr_t    selected;
    attr_t    reselected;
} WMenuAttrs;


typedef struct __WMenu  WMenu;

WMenu * WMenu_new(WMenuAttrs attrs, WItem ** items, unsigned int itemscount, unsigned int curIndex, bool isCyclic);
void WMenu_free(WMenu * self);


#define WA_DEFAULT ((attr_t)1 << ((sizeof(attr_t) * 8) - 1))

WItem **     WMenu_getItems(WMenu * self);
WItem *      WMenu_itemAt(WMenu * self, unsigned int index);  
WItem *      WMenu_getCurItem(WMenu * self);     
unsigned int WMenu_getCurIndex(WMenu * self);    
attr_t       WMenu_getFore(WMenu * self);        
attr_t       WMenu_getBack(WMenu * self);        
attr_t       WMenu_getGrey(WMenu * self);        
attr_t       WMenu_getSelected(WMenu * self);    
attr_t       WMenu_getReselected(WMenu * self);  
unsigned int WMenu_getItemsCount(WMenu * self);  
bool         WMenu_getCyclic(WMenu * self);      


 
WItem ** WMenu_setItems(WMenu * self, WItem ** newItems);                   
WItem * WMenu_setItemAt(WMenu * self, unsigned int index, WItem * newItem);    
unsigned int WMenu_setCurIndex(WMenu * self, unsigned int newIndex);                
unsigned int WMenu_setItemsCount(WMenu * self, unsigned int newItemscount);     
WMenuAttrs WMenu_includeAttrs(WMenu * self, WMenuAttrs attrs);
WMenuAttrs WMenu_excludeAttrs(WMenu * self, WMenuAttrs attrs);
WMenuAttrs WMenu_setAttrs(WMenu * self, WMenuAttrs attrs);    
attr_t WMenu_includeFore(WMenu * self, attr_t foreAttr);                 
attr_t WMenu_includeBack(WMenu * self, attr_t backAttr);                 
attr_t WMenu_includeGrey(WMenu * self, attr_t greyAttr);                 
attr_t WMenu_includeSelected(WMenu * self, attr_t selectedAttr);         
attr_t WMenu_includeReselected(WMenu * self, attr_t reselectedAttr);     
attr_t WMenu_excludeFore(WMenu * self, attr_t foreAttr);                
attr_t WMenu_excludeBack(WMenu * self, attr_t backAttr);                
attr_t WMenu_excludeGrey(WMenu * self, attr_t greyAttr);                
attr_t WMenu_excludeSelected(WMenu * self, attr_t selectedAttr);        
attr_t WMenu_excludeReselected(WMenu * self, attr_t reselectedAttr);    
attr_t WMenu_setFore(WMenu * self, attr_t newFore);                     
attr_t WMenu_setBack(WMenu * self, attr_t newBack);                     
attr_t WMenu_setGrey(WMenu * self, attr_t newGrey);                     
attr_t WMenu_setSelected(WMenu * self, attr_t newSelected);             
attr_t WMenu_setReselected(WMenu * self, attr_t newReselected);         
bool WMenu_setCyclic(WMenu * self, bool newCyclic);                 
bool WMenu_toggleCyclic(WMenu * self);                       




#define WM_FIRST_ITEM      (0UL)      
#define WM_LAST_ITEM       (1UL)         
#define WM_NEXT_ITEM       (2UL)
#define WM_NEXT_SELECTABLE (3UL)
#define WM_PREV_ITEM       (4UL)   
#define WM_PREV_SELECTABLE (5UL)
#define WM_MOUSE_EVENT     (6UL) 
#define WM_RESTART         (7UL)    
#define WM_JUMP_TO         (1UL << ((sizeof(unsigned long int) * 8) - 1))     
#define WM_REFRESH_ITEM    (1UL << ((sizeof(unsigned long int) * 8) - 2))        
#define WM_TOGGLE_ITEM     (1UL << ((sizeof(unsigned long int) * 8) - 3))   
#define WM_SELECT_ITEM     (1UL << ((sizeof(unsigned long int) * 8) - 4))       
#define WM_DESELECT_ITEM   (1UL << ((sizeof(unsigned long int) * 8) - 5)) 
#define WM_SET_ITEM_GREY   (1UL << ((sizeof(unsigned long int) * 8) - 6))  

/*
 *  use WM_JUMP_TO and WM_REFRESH option by bitwise OR'ing the index with it
 *  Example: driverWmenu(WM_JUMP_TO | index); driverWmenu(WM_REFRESH | index)
 */


// MAIN WORKING HORSE:
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
bool WMenu_driver(WMenu * menu, unsigned long int option); //::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

signed long WMenu_getNextSelectableIndex(WMenu * menu);
signed long WMenu_getPrevSelectableIndex(WMenu * menu);

signed long WMenu_getEnclosedItemIndex(WMenu * menu, Location loc);

void WMenu_post  (WMenu * menu);
void WMenu_unpost(WMenu * menu, attr_t remainder);
void WMenu_refresh(WMenu * menu, unsigned int index);

signed long WMenu_getPrevItemIndex(WMenu * menu);
signed long WMenu_getNextItemIndex(WMenu * menu);
WItem * WMenu_getPrevItem(WMenu * menu);
WItem * WMenu_getNextItem(WMenu * menu);

