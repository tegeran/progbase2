#pragma once 

#define _XOPEN_SOURCE_EXTENDED 


#include <stdlib.h>
#include <ncursesw/curses.h>



typedef struct __Frame{
    cchar_t sideT;
    cchar_t sideB;
    //
    cchar_t sideL;
    cchar_t sideR;
    //
    cchar_t cornerTL;
    cchar_t cornerTR;
    //
    cchar_t cornerBL;
    cchar_t cornerBR;
} Frame;

Frame * Frame_new(wchar_t  cornerTopL,
                  wchar_t  sideTop,
                  wchar_t  cornerTopR,
                  wchar_t  sideLeft,
                  wchar_t  sideRight,
                  wchar_t  cornerBtmL,
                  wchar_t  sideBtm,
                  wchar_t  cornerBtmR);

void Frame_free(Frame * self);
cchar_t Frame_setSideT(Frame * self, cchar_t sideT);
cchar_t Frame_setSideB(Frame * self, cchar_t sideB);

cchar_t Frame_setSideL(Frame * self, cchar_t sideL);
cchar_t Frame_setSideR(Frame * self, cchar_t sideR);

cchar_t Frame_setCornerTL(Frame * self, cchar_t cornerTL);
cchar_t Frame_setCornerTR(Frame * self, cchar_t cornerTR);

cchar_t Frame_setCornerBL(Frame * self, cchar_t cornerBL);
cchar_t Frame_setCornerBR(Frame * self, cchar_t cornerBR);              

//////////////////////////////////////////

cchar_t Frame_getSideT(Frame * self);
cchar_t Frame_getSideB(Frame * self);

cchar_t Frame_getSideL(Frame * self);
cchar_t Frame_getSideR(Frame * self);

cchar_t Frame_getCornerTL(Frame * self);
cchar_t Frame_getCornerTR(Frame * self);

cchar_t Frame_getCornerBL(Frame * self);
cchar_t Frame_getCornerBR(Frame * self);   


// Literal macro

#define frame(cornerTopL, sideTop, cornerTopR, sideLeft ,sideRight, cornerBtmL, sideBtm, cornerBtmR) \
{                                               \
    .sideT.attr        = A_NORMAL,              \
    .sideT.chars[0]    = sideTop,               \
    .sideT.chars[1]    = L'\0',                 \
                                                \
    .sideB.attr        = A_NORMAL,              \
    .sideB.chars[0]    = sideBtm,               \
    .sideB.chars[1]    = L'\0',                 \
                                                \
    .sideL.attr        = A_NORMAL,              \
    .sideL.chars[0]    = sideLeft,              \
    .sideL.chars[1]    = L'\0',                 \
                                                \
    .sideR.attr        = A_NORMAL,              \
    .sideR.chars[0]    = sideRight,             \
    .sideR.chars[1]    = L'\0',                 \
                                                \
    .cornerTL.attr     = A_NORMAL,              \
    .cornerTL.chars[0] = cornerTopL,            \
    .cornerTL.chars[1] = L'\0',                 \
                                                \
    .cornerTR.attr     = A_NORMAL,              \
    .cornerTR.chars[0] = cornerTopR,            \
    .cornerTR.chars[1] = L'\0',                 \
                                                \
    .cornerBL.attr     = A_NORMAL,              \
    .cornerBL.chars[0] = cornerBtmL,            \
    .cornerBL.chars[1] = L'\0',                 \
                                                \
    .cornerBR.attr     = A_NORMAL,              \
    .cornerBR.chars[0] = cornerBtmR,            \
    .cornerBR.chars[1] = L'\0'                  \
}


const static Frame WF_DOUBLE = frame(L'╔',L'═',L'╗',
                                     L'║'     ,L'║',
                                     L'╚',L'═',L'╝');

const static Frame WF_SINGLE = frame(L'┏',L'━',L'┓',
                                     L'┃',     L'┃',
                                     L'┗',L'━',L'┛');