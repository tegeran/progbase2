#pragma once

#include <sstring.h>
#include <list.h>
#include <stdlib.h>

typedef struct __DirDriver DirDriver;

DirDriver * DirDriver_new(String * startDir);
DirDriver * DirDriver_newLocal(void);
void DirDriver_free(DirDriver * self);

void DirDriver_appendNewIndirs(DirDriver * self, List * dirs);
void DirDriver_appendNewFiles(DirDriver * self, List * files);

void DirDriver_goNextDir(DirDriver * self, String * nextDir);
void DirDriver_goPrevDir(DirDriver * self);