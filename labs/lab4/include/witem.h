#pragma once 


#include <windows.h>
#include <stdbool.h>

typedef struct __WItem WItem;

typedef  void (*WItem_Hook)(struct __WItem *);


WItem * WItem_newStd(WINDOW * win, bool winIsImmutable);

WItem * WItem_new(WINDOW * win, 
                        bool   winIsImmutable, 
                        bool   value,
                        bool   selectable, 
                        void * ptr, 
                  WItem_Hook   hook);

void WItem_free(WItem * self);


bool WItem_getValue(WItem * self);
WINDOW * WItem_getWin(WItem * self);        
bool WItem_getSelectable(WItem * self);
void * WItem_getPtr(WItem * self);  
WItem_Hook WItem_getHook(WItem * self);       
WINDOW * WItem_getDupwin(WItem * self);

bool WItem_setValue(WItem * self, bool newValue);
bool WItem_toggleValue(WItem * self);    
WINDOW * WItem_setWin(WItem * self, WINDOW * newWin);
bool WItem_setSelectable(WItem * self, bool newSelectable);
bool WItem_toggleSelecrable(WItem * self);
void * WItem_setPtr(WItem * self, void * newPtr);    
WItem_Hook WItem_setHook(WItem * self, WItem_Hook newHook);
WINDOW * WItem_setDupwin(WItem * self, WINDOW * dupwin);


void WItem_execHook(WItem * item);