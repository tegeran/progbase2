#pragma once

#include <witem.h>

WItem ** WItem_newArr(WINDOW ** win, const unsigned int size, bool winsAreImmutable);
void WItem_freeArr(WItem ** self, const unsigned int size);