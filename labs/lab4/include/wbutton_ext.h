#pragma once 

#include <wbutton.h>
#include <list.h>

typedef struct __BLayout{
              bool orientation;
    unsigned short spacing;
} BLayout;

#define blayout(ORIENTATION, SPACING) \
(BLayout){                            \
    .orientation = (ORIENTATION),     \
    .spacing     = (SPACING)          \
}





