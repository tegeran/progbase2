#pragma once
#include <stdbool.h>

typedef struct __DataCloud{
    double capacity;  // terabytes of general available data storage
    double tariff;    // dollars/(Gigabyte per month) for storing data
} DataCloud;

DataCloud * DataCloud_new(double capacity, double tariff);
void DataCloud_free(DataCloud * self);

double DataCloud_capacityAt(DataCloud * self);
double DataCloud_tariffAt(DataCloud * self);

double DataCloud_setCapacity(DataCloud * self, double capacity);
double DataCloud_setTariff(DataCloud * self, double tariff);

bool DataCloud_isValidCapacity(double capacity);
bool DataCloud_isValidTariff(double tariff);