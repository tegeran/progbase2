#pragma once


#define _XOPEN_SOURCE_EXTENDED 

#include <stdbool.h>
#include <ncursesw/curses.h>
#include <wbutton.h>
#include <wbutton_ext.h>
#include <wmenu.h>
#include <list.h>

typedef WMenuAttrs BMenuAttrs;

typedef struct __BMenu BMenu;
typedef WItem_Hook WButton_Hook;

unsigned int BMenu_getButtonsCount(BMenu * self);
WButton_Hook BMenu_getHookAt(BMenu * self, unsigned int index);
void *       BMenu_getPtrAt(BMenu * self, unsigned int index);

WButton_Hook BMenu_setHookAt(BMenu * self, unsigned int index);
void *       BMenu_setPtrAt(BMenu * self, unsigned int index);
    


#define BM_REACT           (0UL)
#define BM_RESTART         (1UL)
#define BM_SET_BUTTON_GREY (1UL << ((sizeof(unsigned long int) * 8) - 1)) 
#define BM_DESELECT_BUTTON (1UL << ((sizeof(unsigned long int) * 8) - 2))    
#define BM_SELECT_BUTTON   (1UL << ((sizeof(unsigned long int) * 8) - 3))        
#define BM_TOGGLE_BUTTON   (1UL << ((sizeof(unsigned long int) * 8) - 4))     
#define BM_REFRESH_BUTTON  (1UL << ((sizeof(unsigned long int) * 8) - 5))  
#define BM_JUMP_TO_BUTTON  (1UL << ((sizeof(unsigned long int) * 8) - 6))   

/*
 *  use WM_JUMP_TO and WM_REFRESH option by bitwise OR'ing the index with it
 *  Example: driverBmenu(WM_CANCEL_CHOICE | index); driverWmenu(WB_SET_BUTTON_GREY | index);
 */

#define VERTICAL_BM   true
#define HORIZONTAL_BM false



BMenu * BMenu_new (wchar_t ** buttonLabels,
              unsigned int    size, 
                BMenuAttrs    attrs,
               Proportions    props,
                  Location    location,
                   BLayout    layout);
                   
void BMenu_free(BMenu * self);
void BMenu_freeWhole(BMenu * self);

BMenu * BMenu_newBy(List * buttons, unsigned int size, BMenuAttrs attrs);

void BMenu_post  (BMenu * menu);

void BMenu_unpost(BMenu * menu, attr_t remainder);

                 
bool BMenu_driver(BMenu * menu, unsigned long option, wint_t input);
 
signed long BMenu_getEnclosedButtonIndex(BMenu * self, wint_t signal);

bool BMenu_isButtonSelected(BMenu * self, unsigned int index);