#pragma once 

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>
#include <ncursesw/panel.h>

#include <frame.h>
#include <proportions.h>
#include <location.h>

// intensity has to be [0, 5]
unsigned short black(unsigned char intensity);
unsigned short white(unsigned char intensity);
#define red(intensity)    (16  + 36 * (intensity))
#define green(intensity)  (16  + 6 * (intensity))
#define blue(intensity)   (16  + (intensity))
#define lblue(intensity)  (21 +  6 * (intensity))
#define orange(intensity) (196 + 6 * (intensity))
#define pink(intensity)   (196 + (intensity))
#define grey(intensity)   (242 - (intensity))

#define Win_maxx(window)   ((window)->_maxx)
#define Win_maxy(window)   ((window)->_maxy)
#define Win_curx(window)   ((window)->_curx)
#define Win_cury(window)   ((window)->_cury)
#define Win_begx(window)   ((window)->_begx)
#define Win_begy(window)   ((window)->_begy)


#define Win_shift_waddch(  window, shift, token) Win_shiftMove((window), (shift)); waddch  (window, token)
#define Win_shift_wadd_wch(window, shift, token) Win_shiftMove((window), (shift)); wadd_wch(window, token)
#define Win_shift_waddwstr(window, shift, str)   Win_shiftMove((window), (shift)); waddwstr(window, str)
#define Win_shift_wprintw( window, shift, ...)   Win_shiftMove((window), (shift)); wprintw (window, __VA_ARGS__)

#define Win_shift_vanishChar(window, shift) Win_shiftMove((window), (shift)); Win_vanishChar(window)
#define Win_shift_vanishLine(window, shift) Win_shiftMove((window), (shift)); Win_vanishLine(window)
#define Win_mvvanishChar(window, y, x)      wmove((window), (y), (x));    Win_vanishChar(window)
#define Win_mvvanishLine(window, y)         wmove((window), (y),  0 );    Win_vanishLine(window)

/*
 * AHTUNG! Function returns a pointer to dynamic memory needed to be freed after usage
 * setWin(): Creates a totally new window according to given parameters.
 * If (frame != NULL) you have to specify attr_t attribute after (frame) argument.
 * Those are used while printing frame around new window.
 * Otherwise, if attr_t argument is absent, then Segmentation fault is possible.
 * If (frame != NULL) creates a new window for frame, boxes it according to Frame (frame)
 * and creates a subwindow which fits into this frame.
 * Use delWFwin if frame was set to free not only subwindow, but a frame around it.
 *
 * Returns: a pointer to new window if not framed or subwindow if framed
 * ------------------------
 * setDerWin(): works analogously to setWin. 
 * The only difference it creates a derivative window for (parent)
*/
WINDOW * Win_new( Proportions   props,
                       Location   beginning,
                    const Frame * const frame, ...);

WINDOW * Win_newDerived(   WINDOW * parent,
                      Proportions   props,
                         Location   beginning,
                      const Frame * const frame, ...);

/*            
* Adds a frame to (win) starting from local (loc) implicitly (doesn't do (wrefresh(win))
* Moves cursor to bottom right corner of drawn frame
* NOTE: does no explicit updates neiher for virt not for actual screen
*/
void Win_addFrame(WINDOW * win,
            Location   loc, 
         Proportions   props,
         const Frame * WF   );

/*
* killWF(); vanishes framed window (win) or window with parent from VIRTUAL SCREEN 
* (it means doupdate() in order to show changes on the real screen after usage).
* Frees memory allocated for framed window (win).
* (attr) is an attribute for blank rectangle left after deleting framed window (win)
* ---------------------------
* killWin(): works analogously to killWF. The diffrence is that (win) may not have a WF
* (parent framed window (window's frame))
*/ 
void Win_killFamily(WINDOW * win, attr_t attr);
void Win_kill(WINDOW * win, attr_t attr);

// AHTUNG! Beware if parent window has one or multiple ancestors,
// you have to free all ancestors before freeing parent

void Win_freeFamily(WINDOW * win);                    // frees win parent (if there exists one) and window

void Win_freeFamilyArr(WINDOW ** win, size_t size);  // frees array of mixed window (with parents or without), 
                                    // frees parent if there exists one)

void Win_freeArr(WINDOW ** win, size_t size);        // frees window array, doesn't touch window's parent

/* 
* Fills (win) by (fillBy) charachters implicitly
*/
void Win_fill(WINDOW * win, const cchar_t * const fillBy);


void Win_wnoutrefreshFamily(WINDOW * win);
void Win_refreshFamily(WINDOW * win);
/*
* Places a space character at current cursor location implicitly.
* Doesn't advance cursor coordinates.
*
* Returns: a character that was overwritten by space at current location.
*/
chtype Win_vanishChar(WINDOW * win);

/*
* Fills current line with spaces. Moves cursor to the beginning of this line.
* NOTE: does no explicit updates neiher for virt not for actual screen
*/
void Win_vanishLine(WINDOW * win);

/*
* Erases (win) and leaves a blank rectangle wich has (attr) attributes
* NOTE does no explicit updates neither for virt nor for actual screen
*/
void Win_vanish(WINDOW * win, attr_t attr);
void Win_addAttr(WINDOW * win, attr_t attr);
void Win_shiftMove(WINDOW * win, int shift);
void Win_appendAttr(WINDOW * win, attr_t attr);



