#pragma once
#include <list.h>
#include <sstring.h>
#include <stdlib.h>

typedef enum {
    CSV_ROW_INCORRECT_VALUE
} CsvRowStatus;

// List contains String objects
typedef struct __CsvTable CsvTable;
// List contains CsvRow objects
typedef struct __CsvRow   CsvRow;

CsvRow * CsvRow_new(void);
String * CsvRow_toNewString(CsvRow * self);
signed long long  CsvRow_getFromString(String * self, CsvRow * row, size_t start);

void CsvRow_free(CsvRow * self);

void CsvRow_add(CsvRow * self, String * value);
void CsvRow_getValues(CsvRow * self, List * values);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_freeWhole(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_getRows(CsvTable * self, List * rows);

CsvTable * CsvTable_newFromString(String * csvString);
String *   CsvTable_toNewString  (CsvTable * self);