#pragma once

#include <windows.h>
#include <frame.h>
#include <sstring.h>
#include <stdbool.h>

typedef struct __Texter Texter;
typedef bool (*InputProcessor)(wint_t *);

Texter * Texter_new(InputProcessor process, Proportions props, 
                    Location loc, const Frame * const frame, ...);

void Texter_free(Texter * self);

String * Texter_toNewString(Texter * self);

InputProcessor Texter_setProcessor(Texter * self, InputProcessor processor);
InputProcessor Texter_processorAt(Texter * self);

bool Texter_process(Texter * self, wint_t input);

size_t Texer_strlen(Texter * self);

void Texter_post(Texter * self);
void Texter_unpost(Texter * self, attr_t remainder);