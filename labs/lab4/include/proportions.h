#pragma once


typedef struct __Proportions{
    int height;
    int width;
} Proportions;


#define props(HEIGHT, WIDTH) ((Proportions){.height = (HEIGHT), .width = (WIDTH)})