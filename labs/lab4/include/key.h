#pragma once

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>

#define INVISIBLE_CURSOR (0)
#define VISIBLE_CURSOR   (1)
#define KEY_ESCAPE  (27)
#define KEY_NEWLINE ('\n')
#define KEY_SPACE   (' ')

/*
 * put this after (mousemask())
 */
void Key_detectMouseMovements(void); 

/*
* put this before (endwin())
*/
void Key_undetectMouseMovements(void); 



// UNI - unicode

// D - double

#define UNI_D_HLINE   "═"
#define UNI_D_VLINE   "║"
#define UNI_D_LTOP    "╔"
#define UNI_D_RTOP    "╗"
#define UNI_D_LBTM    "╚"
#define UNI_D_RBTM    "╝"
#define UNI_D_LEFT_T  "╠"
#define UNI_D_RIGHT_T "╣"
#define UNI_D_TOP_T   "╦"
#define UNI_D_BTM_T   "╩"
#define UNI_D_X       "╬"

// S- single

#define UNI_S_HLINE   "━"
#define UNI_S_VLINE   "┃"
#define UNI_S_LTOP    "┏"
#define UNI_S_RTOP    "┓"
#define UNI_S_LBTM    "┗"
#define UNI_S_RBTM    "┛"
#define UNI_S_LEFT_T  "┣"
#define UNI_S_RIGHT_T "┫"
#define UNI_S_TOP_T   "┳"
#define UNI_S_BTM_T   "┻"
#define UNI_S_X       "╋"            

#define UNI_FOLDER        "📂"
#define UNI_BLANK_BOX     "☐"
#define UNI_CHECKED_BOX   "☑"
#define UNI_UNCHECKED_BOX "☒"

#define UNI_WARNING  "⚠"
#define UNI_CLOCK    "⏳"
#define UNI_SPANNER  "🔧"
#define UNI_GEAR     "⚙"
#define UNI_FLOPPY   "🖫"
#define UNI_MOUSE    "🖰"
#define UNI_KEYBOARD "🖮"

#define UNI_GO_BACK   "🔙"
#define UNI_INFO      "🛈"
#define UNI_QUESTION  "❓"
#define UNI_LITTERBIN "🗑" 


#define UNI_TRI_LBRACKET   "❰"
#define UNI_TRI_RBRACKET   "❱"
#define UNI_CURLY_LBRACKET "❴"
#define UNI_CURLY_RBRACKET "❵"
#define UNI_LPARENTHESIS   "❨"
#define UNI_RPARENTHESIS   "❩"
#define UNI_S_L_QUOTE      "❛"
#define UNI_S_R_QUOTE      "❜"
#define UNI_D_L_QUOTE      "❝"
#define UNI_D_R_QUOTE      "❞"   

// use following only with (UNI_...) tags
#define          wide_string_expanded(ch) L##ch 
#define wstr(ch) wide_string_expanded(ch)

// use following only with (UNI_...) tags
#define         wide_char_expanded(ch) (L##ch)[0]
#define wch(ch) wide_char_expanded(ch)

#define wstrs(...) ((wchar_t * []){__VA_ARGS__})


// Literal macroes

#define cchar(WIDECHAR)         ((cchar_t){.attr = WA_NORMAL, .chars[0] = (WIDECHAR), .chars[1] = L'\0'})
#define cchattr(WIDECHAR, ATTR) ((cchar_t){.attr = (ATTR),    .chars[0] = (WIDECHAR), .chars[1] = L'\0'})


bool Key_isArrow(chtype token);
bool Key_isWASD(chtype token);
bool Key_mouseTouched(WINDOW * win);