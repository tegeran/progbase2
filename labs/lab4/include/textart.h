#pragma once


#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>


#define ART_MAX_ATTRS 10

typedef struct __ArtSetup{
    wchar_t   wchar[ART_MAX_ATTRS];
     attr_t    attr[ART_MAX_ATTRS];
} ArtSetup;


attr_t ArtSetup_getAttr(wchar_t token, const ArtSetup * const setup);

// prints text art with all the symbols in it
void ArtSetup_printExpilicit(WINDOW * win, const ArtSetup * const setup, const wchar_t * art);
// pritns text art only using spaces, which have corresponing color
void ArtSetup_printImplicit(WINDOW * win, const ArtSetup * const setup, const wchar_t * image);