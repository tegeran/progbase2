#pragma once 


#include <windows.h>
#include <stdbool.h>
#include <ncursesw/panel.h>

typedef struct __PanItem PanItem;

typedef  void (*PanItem_Hook)(struct __PanItem *);

typedef struct __PanItem {
    PanItem_Hook   hook; // is executed each time selector is set on this item 
          void * ptr;
         PANEL * pan;
         PANEL * duppan;
        WINDOW * dupwin;
          bool   value;
          bool   selectable;
} PanItem;



PanItem * PanItem_newStd(PANEL * pan, bool panIsImmutable);

PanItem * PanItem_new (PANEL * pan, 
                        bool   panIsImmutable, 
                        bool   value,
                        bool   selectable, 
                        void * ptr, 
                PanItem_Hook   hook);

void PanItem_free(PanItem * self);


bool PanItem_getValue(PanItem * self);    
PANEL * PanItem_getPan(PanItem * self);    
bool PanItem_getSelectable(PanItem * self);
void * PanItem_getPtr(PanItem * self);  
PanItem_Hook PanItem_getHook(PanItem * self);       
WINDOW * PanItem_getDupwin(PanItem * self); 
PANEL * PanItem_getDuppan(PanItem * self);
WINDOW * PanItem_getWin(PanItem * self);
      

bool PanItem_setValue(PanItem * self, bool newValue);
bool PanItem_toggleValue(PanItem * self);    
PANEL * PanItem_setPan(PanItem * self, PANEL * newPan);
bool PanItem_setSelectable(PanItem * self, bool newSelectable);
bool PanItem_toggleSelecrable(PanItem * self);
void * PanItem_setPtr(PanItem * self, void * newPtr);    
PanItem_Hook PanItem_setHook(PanItem * self, PanItem_Hook newHook);
WINDOW * PanItem_setDupwin(PanItem * self, WINDOW * dupwin);
PANEL * PanItem_setDuppan(PanItem * self, PANEL * duppan);


void PanItem_execHook(PanItem * item);


PanItem ** PanItem_newArr(PANEL ** panels, const unsigned int size, bool pansAreImmutable);
void PanItem_freeArr(PanItem ** self, const unsigned int size);