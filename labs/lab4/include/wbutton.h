
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// USER FRIENDLY INTERFACE NCURSES DEMANDING FUNCTIONS

#pragma once
/*
 * setlocale(LC_ALL,"") before initscr() 
 * in order to get UTF-8 format, hopefully
 */

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>
#include <ncursesw/menu.h>
#include <ncursesw/form.h>
#include <ncursesw/panel.h>

#include <stdarg.h>
#include <wchar.h>
#include <assert.h>
#include <locale.h>
#include <string.h>
#include <ctype.h>

#include <frame.h>
#include <proportions.h>
#include <location.h>

#include <witem.h>

// fast button menu units:


typedef struct __WButton WButton;

 
bool          WButton_getSelectable(WButton * self); 
Proportions   WButton_getProps(WButton * self);      
Location      WButton_getLocation(WButton * self);   
WINDOW *      WButton_getTextWin(WButton * self);    
const Frame * WButton_getFrame(WButton * self);


void          WButton_wnoutrefresh(WButton * self);   
void          WButton_refresh(WButton * self);      

void          WButton_setLabel(WButton * self, wchar_t * newLabel);    
bool          WButton_setSelectable(WButton * self, bool newSelectable);   
bool          WButton_toggleSelectable(WButton * self);               
const Frame * WButton_setFrame(WButton * self, const Frame * newFrame);             
Proportions   WButton_setProps(WButton * self, Proportions newProps);             
Location      WButton_setLocation(WButton * self, Location newLocation);       
WINDOW *      WButton_setTextWin(WButton * self, WINDOW * newTextWin);    

// AHTUNG! Never reset button's textwin


WButton * WButton_new(wchar_t * label,
                         bool   selectable,
                  const Frame * frame,
                  Proportions   props,
                     Location   location);

WButton * WButton_newStd(wchar_t * label,
                  Proportions   props,
                     Location   location);

void WButton_free(WButton * self);

void WButton_kill(WButton * self, attr_t remainder);