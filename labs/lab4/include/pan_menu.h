#pragma once

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>
#include <ncursesw/panel.h>

#include <frame.h>
#include <proportions.h>
#include <location.h>
#include <pan_item.h>

// A ➞ B = ¬A ∪ B
#define imply(presupposition, conclusion) (!(presupposition) || (conclusion))

typedef struct __PanMenuAttrs {
    attr_t    fore;
    attr_t    back;
    attr_t    grey;
    attr_t    selected;
    attr_t    reselected;
} PanMenuAttrs;


typedef struct __PanMenu  PanMenu;

PanMenu * PanMenu_new(PanMenuAttrs attrs, PanItem ** items, unsigned int itemscount, unsigned int curIndex, bool isCyclic);
void PanMenu_free(PanMenu * self);


#define WA_DEFAULT ((attr_t)1 << ((sizeof(attr_t) * 8) - 1))

PanItem **     PanMenu_getItems(PanMenu * self);
PanItem *      PanMenu_itemAt(PanMenu * self, unsigned int index);  
PanItem *      PanMenu_getCurItem(PanMenu * self);     
unsigned int PanMenu_getCurIndex(PanMenu * self);    
attr_t       PanMenu_getFore(PanMenu * self);        
attr_t       PanMenu_getBack(PanMenu * self);        
attr_t       PanMenu_getGrey(PanMenu * self);        
attr_t       PanMenu_getSelected(PanMenu * self);    
attr_t       PanMenu_getReselected(PanMenu * self);  
unsigned int PanMenu_getItemsCount(PanMenu * self);  
bool         PanMenu_getCyclic(PanMenu * self);      
PanMenuAttrs PanMenu_getAttrs(PanMenu * self);


 
PanItem ** PanMenu_setItems(PanMenu * self, PanItem ** newItems);                   
PanItem * PanMenu_setItemAt(PanMenu * self, unsigned int index, PanItem * newItem);    
unsigned int PanMenu_setCurIndex(PanMenu * self, unsigned int newIndex);                
unsigned int PanMenu_setItemsCount(PanMenu * self, unsigned int newItemscount);     
PanMenuAttrs PanMenu_includeAttrs(PanMenu * self, PanMenuAttrs attrs);
PanMenuAttrs PanMenu_excludeAttrs(PanMenu * self, PanMenuAttrs attrs);
PanMenuAttrs PanMenu_setAttrs(PanMenu * self, PanMenuAttrs attrs);    
attr_t PanMenu_includeFore(PanMenu * self, attr_t foreAttr);                 
attr_t PanMenu_includeBack(PanMenu * self, attr_t backAttr);                 
attr_t PanMenu_includeGrey(PanMenu * self, attr_t greyAttr);                 
attr_t PanMenu_includeSelected(PanMenu * self, attr_t selectedAttr);         
attr_t PanMenu_includeReselected(PanMenu * self, attr_t reselectedAttr);     
attr_t PanMenu_excludeFore(PanMenu * self, attr_t foreAttr);                
attr_t PanMenu_excludeBack(PanMenu * self, attr_t backAttr);                
attr_t PanMenu_excludeGrey(PanMenu * self, attr_t greyAttr);                
attr_t PanMenu_excludeSelected(PanMenu * self, attr_t selectedAttr);        
attr_t PanMenu_excludeReselected(PanMenu * self, attr_t reselectedAttr);    
attr_t PanMenu_setFore(PanMenu * self, attr_t newFore);                     
attr_t PanMenu_setBack(PanMenu * self, attr_t newBack);                     
attr_t PanMenu_setGrey(PanMenu * self, attr_t newGrey);                     
attr_t PanMenu_setSelected(PanMenu * self, attr_t newSelected);             
attr_t PanMenu_setReselected(PanMenu * self, attr_t newReselected);         
bool PanMenu_setCyclic(PanMenu * self, bool newCyclic);                 
bool PanMenu_toggleCyclic(PanMenu * self);                       


typedef enum PanMenuSt{
    PanMenuSt_MOUSE_OUT,
    PanMenuSt_CREATED,
    PanMenuSt_ADVANCED_SELECTOR,
    PanMenuSt_ADV_AND_TOGGLED,
    PanMenuSt_DISCARDED_SELECTOR,
    PanMenuSt_IDLED,
    PanMenuSt_POSTED,
    PanMenuSt_TOGGLED,
    PanMenuSt_UNPOSTED,
    PanMenuSt_RESTARTED,
    PanMenuSt_REFRESHED
} PanMenuSt;



#define PANM_FIRST_ITEM       (0UL)
#define PANM_LAST_ITEM        (1UL)
#define PANM_NEXT_ITEM        (2UL)
#define PANM_NEXT_SELECTABLE  (3UL)
#define PANM_PREV_ITEM        (4UL)
#define PANM_PREV_SELECTABLE  (5UL)
#define PANM_MOUSE_EVENT      (6UL)
#define PANM_RESTART          (7UL)
#define PANM_DISCARD_SELECTOR (8UL)
#define PANM_FORCE            (1UL << ((sizeof(unsigned long int) * 8) - 1))
#define PANM_JUMP_TO          (1UL << ((sizeof(unsigned long int) * 8) - 2))
#define PANM_REFRESH_ITEM     (1UL << ((sizeof(unsigned long int) * 8) - 3))
#define PANM_TOGGLE_ITEM      (1UL << ((sizeof(unsigned long int) * 8) - 4))
#define PANM_SELECT_ITEM      (1UL << ((sizeof(unsigned long int) * 8) - 5))
#define PANM_DESELECT_ITEM    (1UL << ((sizeof(unsigned long int) * 8) - 6))
#define PANM_SET_ITEM_GREY    (1UL << ((sizeof(unsigned long int) * 8) - 7))

/*
 *  use PANM_JUMP_TO and PANM_REFRESH option by bitwise OR'ing the index with it
 *  Example: driverWmenu(PANM_JUMP_TO | index); driverWmenu(PANM_REFRESH | index)
 */


// MAIN WORKING HORSE:
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
bool PanMenu_driver(PanMenu * menu, unsigned long int option); //::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

PanMenuSt PanMenu_getState(PanMenu * self);

signed long PanMenu_getNextSelectableIndex(PanMenu * menu);
signed long PanMenu_getPrevSelectableIndex(PanMenu * menu);

signed long PanMenu_getEnclosedItemIndex(PanMenu * menu, Location loc);

void PanMenu_post  (PanMenu * menu, signed long long startForm);
void PanMenu_unpost(PanMenu * menu);
void PanMenu_refresh(PanMenu * menu, unsigned int index);

signed long PanMenu_getPrevItemIndex(PanMenu * menu);
signed long PanMenu_getNextItemIndex(PanMenu * menu);
PanItem * PanMenu_getPrevItem(PanMenu * menu);
PanItem * PanMenu_getNextItem(PanMenu * menu);

