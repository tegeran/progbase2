
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// USER FRIENDLY INTERFACE NCURSES DEMANDING FUNCTIONS

#pragma once
/*
 * setlocale(LC_ALL,"") before initscr() 
 * in order to get UTF-8 format, hopefully
 */

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>
#include <ncursesw/menu.h>
#include <ncursesw/form.h>
#include <ncursesw/panel.h>

#include <stdarg.h>
#include <wchar.h>
#include <assert.h>
#include <locale.h>
#include <string.h>
#include <ctype.h>

#include <frame.h>
#include <proportions.h>
#include <location.h>

#include <pan_item.h>

// fast button menu units:


typedef struct __PanButton PanButton;

 
bool          PanButton_getSelectable(PanButton * self); 
Proportions   PanButton_getProps(PanButton * self);      
Location      PanButton_getLocation(PanButton * self);   
WINDOW *      PanButton_getTextWin(PanButton * self);    
PANEL *       PanButton_getPanel(PanButton * self);  
const Frame * PanButton_getFrame(PanButton * self);


void          PanButton_update(PanButton * self);   
void          PanButton_refresh(PanButton * self);      

void          PanButton_setLabel(PanButton * self, wchar_t * newLabel);    
bool          PanButton_setSelectable(PanButton * self, bool newSelectable);   
bool          PanButton_toggleSelectable(PanButton * self);               
const Frame * PanButton_setFrame(PanButton * self, const Frame * newFrame);             
Proportions   PanButton_setProps(PanButton * self, Proportions newProps);             
Location      PanButton_setLocation(PanButton * self, Location newLocation);       
WINDOW *      PanButton_setTextWin(PanButton * self, WINDOW * newTextWin);   
PANEL  *      PanButton_setPanel(PanButton * self, PANEL * newPanel);

// AHTUNG! Never reset button's textwin


PanButton * PanButton_new(const wchar_t * label,
                             bool   selectable,
                      const Frame * frame,
                      Proportions   props,
                         Location   location);

PanButton * PanButton_newStd(const wchar_t * label,
                  Proportions   props,
                     Location   location);



PanButton * PanButton_chNew(const char * label,
                            bool   selectable,
                     const Frame * frame,
                     Proportions   props,
                        Location   location);

PanButton * PanButton_chNewStd(const char * label,
                  Proportions   props,
                     Location   location);


void PanButton_free(PanButton * self);

void PanButton_kill(PanButton * self, attr_t remainder);