#pragma once 

#define _XOPEN_SOURCE_EXTENDED 

#include <ncursesw/curses.h>
#include <ncursesw/panel.h>

#include <frame.h>
#include <proportions.h>
#include <location.h>

typedef struct __Panel {
    PANEL * pan;
   WINDOW * win;
} Panel;


Panel * Panel_new( Proportions   props,
                       Location   beginning,
                    const Frame * const frame, ...);

void Panel_free(Panel * self); 
void Panel_freeArr(Panel ** self, size_t size);     
void Panel_updateAll(void);
void Panel_refreshAll(void);
void Panel_hide(Panel * self);
void Panel_show(Panel * self);