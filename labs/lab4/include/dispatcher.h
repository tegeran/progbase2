#pragma once

#include <locale.h>
#include <windows.h>
#include <key.h>
#include <bmenu.h>
#include <locale.h>
#include <key.h>
#include <csv.h>
#include <textart.h>
#include <ncursesw/form.h>
#include <sstring.h>
#include <ctype.h>
#include <error.h>
#include <pbmenu.h>
#include <panels.h>
#include <events.h>
#include <mem.h>
#include <list_ext.h>
#include <provider.h>
// #include <progbase/events.h>


void MainMenu_dispatcher(EventHandler * self, Event * event);
void MenuFile_dispatcher(EventHandler * self, Event * event);
void MenuList_dispatcher(EventHandler * self, Event * event);
     

void List_freeProviders(List * list);

typedef DestructorFunction Destructor;

const static BLayout VLAYOUT_NOGAP = {VERTICAL_PBM, 0};
const static BLayout VLAYOUT = {VERTICAL_PBM, 1};
const static BLayout HLAYOUT = {HORIZONTAL_PBM, 1};
const static ArtSetup ART_SETUP = {
    .wchar = {L'2'},
    .attr = {0}
};

PBMenuAttrs attrs;
PBMenuAttrs noFrameAttrs;

#define GLOBAL_ATTRS (attrs.attrs.back)


enum {
    UserSignal,

    MainMenu_Exit,

    MenuFile_SelectedFile,

    MenuList_UpdatedProvider,
    MenuList_ChangedProvider,
    MenuList_InsertAfter,
    MenuList_InsertBefore,
    MenuList_DeleteProvider,
    MenuList_EditProvider,
    MenuList_DoTask,
    MenuList_ChosenTask,
    MenuList_Save,
    MenuList_Saved,

    Goto_MenuFile,
    Goto_MenuList,
    Goto_MainMenu
   
};

enum {
    INIT,
    FREE,
    REACT,
    RESTART,
    UPDATE
};

enum OkCancel{
    CHOOSE_OK,
    CHOOSE_IDLE,
    CHOOSE_CANCEL
};

enum MultiButtons{
    MENU_IDLING,
    MENU_CHANGED,
    MENU_SELECTED
};

#define emit(signal) EventSystem_emit(Event_new(NULL, (signal), NULL, NULL))
#define emitData(signal, data) EventSystem_emit(Event_new(NULL, (signal), (data), NULL))

#define lineCenter(X)   ((COLS -  (X)) >> 1)
#define columnCenter(Y) ((LINES - (Y)) >> 1)

Location getLineCenter(Proportions props, unsigned short y);
Location getScreenCenter(Proportions props);

#define Dispather_initShutdownDialog() __Dispatcher_dialog(INIT, '\0',  \
            L"Are you sure you want to shutdown? \n   All unsaved data will be lost")

#define Dispatcher_reactDialog(input)  __Dispatcher_dialog(REACT, (input), NULL)
#define Dispatcher_freeDialog()        __Dispatcher_dialog(FREE, '\0', NULL)
#define Dispatcher_initDialog(message) __Dispatcher_dialog(INIT, '\0',(message))

int __Dispatcher_dialog(int state, wint_t input, wchar_t * essence);
void Input_listener     (EventHandler * self, Event * event);




