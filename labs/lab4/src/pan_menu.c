#include <pan_menu.h>
#include <assert.h>
#include <error.h>
#include <windows.h>
#include <pan_item.h>
#include <mem.h>


typedef struct __PanMenu {
    PanMenuAttrs    attrs;
         PanItem ** items;
    unsigned int    curindex;
    unsigned int    itemscount;
            bool    cyclic;
            bool    hasSelector;
       PanMenuSt    state;
} PanMenu;


PanMenu * PanMenu_new(PanMenuAttrs    attrs, 
                     PanItem ** items, 
                unsigned int    itemscount, 
                unsigned int    curIndex, 
                        bool    isCyclic){
    //
    Debug_tip(itemscount > 0, "An attemt to create menu with no items was made");
    Debug_suppose(imply(items == NULL, itemscount == 0), "");
    assert(curIndex < itemscount || curIndex == 0);
    //
    PanMenu * newbie = Mem_malloc(sizeof(PanMenu));
    *newbie = (PanMenu){
            .attrs = attrs,
            .items = items,
            .curindex = curIndex,
            .itemscount = itemscount,
            .cyclic = isCyclic,
            .state = PanMenuSt_CREATED
    };
    return newbie;
}

void PanMenu_free(PanMenu * self){
    Debug_exists(self);
    //
    free(self);
}


// Getters

PanItem ** PanMenu_getItems(PanMenu * self){
    assert(self != NULL);
    //
    return self->items;
}

PanItem * PanMenu_itemAt(PanMenu * self, unsigned int index){
    assert(self != NULL);
    //
    return self->items[index];
}

PanItem * PanMenu_getCurItem(PanMenu * self){
    assert(self != NULL);
    //
    return self->items[self->curindex];
}

unsigned int PanMenu_getCurIndex(PanMenu * self){
    assert(self != NULL);
    //
    return self->curindex;
}

PanMenuAttrs PanMenu_getAttrs(PanMenu * self){
    Debug_exists(self);
    //
    return self->attrs;
}

attr_t PanMenu_getFore(PanMenu * self){
    assert(self != NULL);
    //
    return self->attrs.fore;
}

attr_t PanMenu_getBack(PanMenu * self){
    assert(self != NULL);
    //
    return self->attrs.back;
}

attr_t PanMenu_getGrey(PanMenu * self){
    assert(self != NULL);
    //
    return self->attrs.grey;
}

attr_t PanMenu_getSelected(PanMenu * self){
    assert(self != NULL);
    //
    return self->attrs.selected;
}

attr_t PanMenu_getReselected(PanMenu * self){
    assert(self != NULL);
    //
    return self->attrs.reselected;
}

unsigned int PanMenu_getItemsCount(PanMenu * self){
    assert(self != NULL);
    //
    return self->itemscount;
}

bool PanMenu_getCyclic(PanMenu * self){
    assert(self != NULL);
    //
    return self->cyclic;
}

// Setters

PanItem ** PanMenu_setItems(PanMenu * self, PanItem ** newItems){
    assert(self != NULL);
    //
    PanItem ** prev = self->items;
    self->items = newItems;
    return prev;
}   

PanItem * PanMenu_setItemAt(PanMenu * self, unsigned int index, PanItem * newItem){
    assert(self != NULL);
    assert(index < self->itemscount);
    //
    PanItem * prev = self->items[index];
    self->items[index] = newItem;
    return prev;
}

unsigned int PanMenu_setCurIndex(PanMenu * self, unsigned int newIndex){
    assert(self != NULL);
    //
    unsigned int prev = self->curindex;
    self->curindex = newIndex;
    return prev;
}

unsigned int PanMenu_setItemsCount(PanMenu * self, unsigned int newItemscount){
    assert(self != NULL);
    //
    unsigned int prev = self->itemscount;
    self->itemscount = newItemscount;
    return prev;
}        

PanMenuAttrs PanMenu_includeAttrs(PanMenu * self, PanMenuAttrs attrs){
    assert(self != NULL);
    //
    PanMenuAttrs prev = self->attrs;
    PanMenu_includeFore(self, attrs.fore);
    PanMenu_includeBack(self, attrs.back);
    PanMenu_includeGrey(self, attrs.grey);
    PanMenu_includeSelected(self, attrs.selected);
    PanMenu_includeReselected(self, attrs.reselected);
    return prev;
}

PanMenuAttrs PanMenu_excludeAttrs(PanMenu * self, PanMenuAttrs attrs){
    assert(self != NULL);
    //
    PanMenuAttrs prev = self->attrs;
    PanMenu_excludeFore(self, attrs.fore);
    PanMenu_excludeBack(self, attrs.back);
    PanMenu_excludeGrey(self, attrs.grey);
    PanMenu_excludeSelected(self, attrs.selected);
    PanMenu_excludeReselected(self, attrs.reselected);
    return prev;
}

PanMenuAttrs PanMenu_setAttrs(PanMenu * self, PanMenuAttrs attrs){
    assert(self != NULL);
    //
    PanMenuAttrs prev = self->attrs;
    self->attrs = attrs;
    return prev;
} 

attr_t PanMenu_includeFore(PanMenu * self, attr_t foreAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.fore;
    self->attrs.fore |= foreAttr;
    return prev;
}                 

attr_t PanMenu_includeBack(PanMenu * self, attr_t backAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.back;
    self->attrs.back |= backAttr;
    return prev;
}                 

attr_t PanMenu_includeGrey(PanMenu * self, attr_t greyAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.grey;
    self->attrs.grey |= greyAttr;
    return prev;
}              

attr_t PanMenu_includeSelected(PanMenu * self, attr_t selectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.selected;
    self->attrs.selected |= selectedAttr;
    return prev;
}

attr_t PanMenu_includeReselected(PanMenu * self, attr_t reselectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.reselected;
    self->attrs.reselected |= reselectedAttr;
    return prev;
}     

attr_t PanMenu_excludeFore(PanMenu * self, attr_t foreAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.fore;
    self->attrs.fore &= ~foreAttr;
    return prev;
}                  

attr_t PanMenu_excludeBack(PanMenu * self, attr_t backAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.back;
    self->attrs.back &= ~backAttr;
    return prev;
}              

attr_t PanMenu_excludeGrey(PanMenu * self, attr_t greyAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.grey;
    self->attrs.grey &= ~greyAttr;
    return prev;
}                

attr_t PanMenu_excludeSelected(PanMenu * self, attr_t selectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.selected;
    self->attrs.selected &= ~selectedAttr;
    return prev;
}

attr_t PanMenu_excludeReselected(PanMenu * self, attr_t reselectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.reselected;
    self->attrs.reselected &= ~reselectedAttr;
    return prev;
}    


attr_t PanMenu_setFore(PanMenu * self, attr_t newFore){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.fore;
    self->attrs.fore = newFore;
    return prev;
}                     

attr_t PanMenu_setBack(PanMenu * self, attr_t newBack){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.back;
    self->attrs.back = newBack;
    return prev;
}                     

attr_t PanMenu_setGrey(PanMenu * self, attr_t newGrey){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.grey;
    self->attrs.grey = newGrey;
    return prev;
}                     

attr_t PanMenu_setSelected(PanMenu * self, attr_t newSelected){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.selected;
    self->attrs.selected = newSelected;
    return prev;
}             

attr_t PanMenu_setReselected(PanMenu * self, attr_t newReselected){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.reselected;
    self->attrs.reselected = newReselected;
    return prev;
}

bool PanMenu_setCyclic(PanMenu * self, bool newCyclic){
    assert(self != NULL);
    //
    bool prev = self->cyclic;
    self->cyclic = newCyclic;
    return prev;
}                 

bool PanMenu_toggleCyclic(PanMenu * self){
    assert(self != NULL);
    //
    self->cyclic = !self->cyclic;
    return !self->cyclic;
}         



// MAIN WORKING HORSE:
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
bool PanMenu_driver(PanMenu * menu, unsigned long int option); //::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


static void setItemToSelected   (PanMenu * menu, unsigned int index);
static void setItemToFore       (PanMenu * menu, unsigned int index);
static void setItemToBack       (PanMenu * menu, unsigned int index);
static void setItemToReselected (PanMenu * menu, unsigned int index);
static void setItemToGrey       (PanMenu * menu, unsigned int index);

static void setSelector  (PanMenu * menu, unsigned int index);
static void unsetSelector(PanMenu * menu);


// Driver fucntions


void PanMenu_post(PanMenu * menu, signed long long startIndex){
    assert(menu != NULL);
    assert(PanMenu_getItems(menu) != NULL);
    assert(PanMenu_itemAt(menu, 0) != NULL);
    menu->state = PanMenuSt_POSTED;
//    //
//    PanItem_setValue(PanMenu_itemAt(menu, 0), false);
//    show_panel(PanItem_getDuppan(PanMenu_itemAt(menu, 0)));
//    setSelector(menu, 0);
//    if ((PanItem_getSelectable(PanMenu_itemAt(menu, 0))) == false)
//        setItemToGrey(menu, 0);
//    //
    for (unsigned int i = 0; i < PanMenu_getItemsCount(menu); ++i){

        PanItem_setValue(PanMenu_itemAt(menu, i), false);
        show_panel(PanItem_getDuppan(PanMenu_itemAt(menu, i)));
        if (i == startIndex){
            setSelector(menu, i);
        } else if ((PanItem_getSelectable(PanMenu_itemAt(menu, i))) == false){
            setItemToGrey(menu, i);
        } else {
            setItemToBack(menu, i);
        }
    }
}

void unsetSelector(PanMenu * menu){
    assert(menu != NULL);
    //
    if (PanItem_getSelectable(PanMenu_getCurItem(menu)) == true){
        if (PanItem_getValue(PanMenu_getCurItem(menu)) == true)
            setItemToSelected(menu, PanMenu_getCurIndex(menu));
        else
            setItemToBack(menu, PanMenu_getCurIndex(menu));
    }
}

void setSelector(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    menu->hasSelector = true;
    PanMenu_setCurIndex(menu, index);
    if (PanItem_getSelectable(PanMenu_getCurItem(menu)) == true){
        if (PanItem_getHook(PanMenu_getCurItem(menu)) != NULL)
            PanItem_execHook(PanMenu_getCurItem(menu));
            
        if (PanItem_getValue(PanMenu_itemAt(menu, index)) == true)
            setItemToReselected(menu, index); // case already was selected        
        else
            setItemToFore(menu, index);
    } 
}

void setItemToBack(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    PanItem * item = PanMenu_itemAt(menu, index);
    if (PanItem_getDuppan(item) == PanItem_getPan(item)){
        wbkgd(PanItem_getDupwin(item), PanMenu_getBack(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        //
    } else if (PanMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(PanItem_getDupwin(item), PanMenu_getBack(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        overwrite(PanItem_getWin(item), PanItem_getDupwin(item));
        //
    } else {
        top_panel(PanItem_getPan(item));
        update_panels();
    }

}

void setItemToSelected(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    PanItem * item = PanMenu_itemAt(menu, index);
    if (PanItem_getDuppan(item) == PanItem_getPan(item)){
        wbkgd(PanItem_getDupwin(item), PanMenu_getSelected(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        //
    } else if (PanMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(PanItem_getDupwin(item), PanMenu_getSelected(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        overwrite(PanItem_getWin(item), PanItem_getDupwin(item));
        //
    } else {
        top_panel(PanItem_getPan(item));
        update_panels();
    }
}

void setItemToFore(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    PanItem * item = PanMenu_itemAt(menu, index);
    if (PanItem_getDuppan(item) == PanItem_getPan(item)){
        wbkgd(PanItem_getDupwin(item), PanMenu_getFore(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        //
    } else if (PanMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(PanItem_getDupwin(item), PanMenu_getFore(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        overwrite(PanItem_getWin(item), PanItem_getDupwin(item));
        //
    } else {
        top_panel(PanItem_getPan(item));
        update_panels();
    }
}

void setItemToReselected(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    PanItem * item = PanMenu_itemAt(menu, index);
    if (PanItem_getDuppan(item) == PanItem_getPan(item)){
        wbkgd(PanItem_getDupwin(item), PanMenu_getReselected(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        //
    } else if (PanMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(PanItem_getDupwin(item), PanMenu_getReselected(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        overwrite(PanItem_getWin(item), PanItem_getDupwin(item));
        //
    } else {
        top_panel(PanItem_getPan(item));
        update_panels();
    }
}

void setItemToGrey(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    PanItem * item = PanMenu_itemAt(menu, index);
    if (PanItem_getDuppan(item) == PanItem_getPan(item)){
        wbkgd(PanItem_getDupwin(item), PanMenu_getGrey(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        //
    } else if (PanMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(PanItem_getDupwin(item), PanMenu_getGrey(menu));
        top_panel(PanItem_getDuppan(item));
        update_panels();
        overwrite(PanItem_getWin(item), PanItem_getDupwin(item));
        //
    } else {
        top_panel(PanItem_getPan(item));
        update_panels();
    }
}

void PanMenu_refresh(PanMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < PanMenu_getItemsCount(menu));
    //
    PanItem * item = PanMenu_itemAt(menu, index); 
    if (PanItem_getSelectable(item) == false){
        setItemToGrey(menu, index);        
    //
    } else if (PanMenu_getCurIndex(menu) == index){
        //
        if (PanItem_getValue(item) == true) {
            setItemToReselected(menu, index);            
        } else {
            setItemToFore(menu, index);
        }
    //
    } else if (PanItem_getValue(item) == true){
        setItemToSelected(menu, index);        
    } else {
        setItemToBack(menu, index);        
    }
}

signed long PanMenu_getEnclosedItemIndex(PanMenu * menu, Location loc){
    assert(menu != NULL);
    //
    PanItem ** item = PanMenu_getItems(menu);
    unsigned int itemscnt = PanMenu_getItemsCount(menu); 
    unsigned int i = PanMenu_getCurIndex(menu);
    signed long j = (signed long)i - 1L;
    while (j >= 0 && i < itemscnt){
        if (wenclose(PanItem_getWin(item[i]), loc.y, loc.x))
            return i;
        if (wenclose(PanItem_getWin(item[j]), loc.y, loc.x))
            return j; 
        ++i;
        --j;
    }
    if (i < itemscnt){
        do {
            if (wenclose(PanItem_getWin(item[i]), loc.y, loc.x))
                return i;
            ++i;
        } while (i < itemscnt);
    }
    else while (j >= 0){
        if (wenclose(PanItem_getWin(item[j]), loc.y, loc.x))
            return j; 
        --j;
    }
    return -1;
}

signed long PanMenu_getNextSelectableIndex(PanMenu * menu){
    assert(PanMenu_getCurIndex(menu) < PanMenu_getItemsCount(menu));
    //    
    signed long i = (signed long)PanMenu_getCurIndex(menu) + 1L;
    while (i < PanMenu_getItemsCount(menu)){
        if (PanItem_getSelectable(PanMenu_itemAt(menu, i)) == true)
            return i;
        ++i;
    }
    i = 0L;
    while (i != PanMenu_getCurIndex(menu)){
        if (PanItem_getSelectable(PanMenu_itemAt(menu, i)) == true)
            return i;
        ++i;
    }
    return -1L;
}

signed long PanMenu_getPrevSelectableIndex(PanMenu * menu){
    assert(PanMenu_getCurIndex(menu) < PanMenu_getItemsCount(menu));
    //
    signed long i = (signed long)PanMenu_getCurIndex(menu) - 1L;
    //
    assert(i < PanMenu_getItemsCount(menu));
    //
    while (i >= 0){
        if (PanItem_getSelectable(PanMenu_itemAt(menu, i)) == true){
            return i;
        }
        --i;
    }
    if (PanMenu_getCyclic(menu) == false) return -1L;
    //
    i = (signed long)PanMenu_getItemsCount(menu) - 1L;
    while (i != PanMenu_getCurIndex(menu)){
        if (PanItem_getSelectable(PanMenu_itemAt(menu, i)) == true)
            return i;
        --i;
    }
    return -1L;
}

PanMenuSt PanMenu_getState(PanMenu * self){
    Debug_exists(self);
    //
    return self->state;
}

bool PanMenu_driver(PanMenu * menu, unsigned long int option){
    assert(menu != NULL);
    //
    unsigned long int modifier = option & PANM_FORCE;
    if (!menu->hasSelector){
        modifier = PANM_FORCE;
    }
    if (modifier){
        option = option & ~modifier;
    }
    switch (option){
        case PANM_FIRST_ITEM:{
            if (PanMenu_getCurIndex(menu) != 0 || modifier){
                unsetSelector(menu);
                setSelector(menu, 0);
                menu->state = PanMenuSt_ADVANCED_SELECTOR;
                return true;
            //
            } else {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
        }
        case PANM_LAST_ITEM:{
            if (PanMenu_getCurIndex(menu) != PanMenu_getItemsCount(menu) - 1 || modifier){
                unsetSelector(menu);
                setSelector(menu, PanMenu_getItemsCount(menu) - 1);
                menu->state = PanMenuSt_ADVANCED_SELECTOR;
                return true;
            //
            } else {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
        }
        case PANM_NEXT_ITEM:{
            signed long nextIndex = PanMenu_getNextItemIndex(menu);
            if (nextIndex != -1L){
                unsetSelector(menu);
                setSelector(menu, (unsigned int)nextIndex);
                menu->state = PanMenuSt_ADVANCED_SELECTOR;
                return true;
            //
            } else {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
        }
        //
        case PANM_NEXT_SELECTABLE:{
            signed long nextSelectable = PanMenu_getNextSelectableIndex(menu);
            if (nextSelectable == -1L) {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
            return PanMenu_driver(menu, PANM_JUMP_TO | modifier | (unsigned int)nextSelectable);
        }
        case PANM_PREV_ITEM:{
            signed long prevIndex = PanMenu_getPrevItemIndex(menu);
            if (prevIndex != -1L){
                unsetSelector(menu);
                setSelector(menu, (unsigned int)prevIndex);
                menu->state = PanMenuSt_ADVANCED_SELECTOR;
                return true;
            //
            } else {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
        }
        case PANM_PREV_SELECTABLE:{
            signed long prevSelectable = PanMenu_getPrevSelectableIndex(menu);
            if (prevSelectable == -1L) {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
            return PanMenu_driver(menu, PANM_JUMP_TO | modifier | (unsigned int)prevSelectable);
        }
        case PANM_MOUSE_EVENT:{
            MEVENT event;
            if (getmouse(&event) == ERR) return false;
            //
            signed long enclosed = PanMenu_getEnclosedItemIndex(menu, loc(event.y, event.x));
            ungetmouse(&event);
            flushinp(); // @ todo it is impossible to getmouse twice for one KEY_MOUSE
            if (enclosed == -1) {
                menu->state = PanMenuSt_MOUSE_OUT;
                return false;
            }
            //
            bool newState = PanMenu_driver(menu, PANM_JUMP_TO | modifier| (unsigned int)enclosed);
            //
            PanMenuSt prevState = menu->state;
            if ((event.bstate & BUTTON1_PRESSED) || (event.bstate & BUTTON1_CLICKED))
                newState |= PanMenu_driver(menu, PANM_TOGGLE_ITEM | modifier | enclosed);
            //
            if (menu->state == PanMenuSt_TOGGLED && prevState == PanMenuSt_ADVANCED_SELECTOR){
                menu->state = PanMenuSt_ADV_AND_TOGGLED;
            }
            return newState || (event.bstate & BUTTON1_RELEASED);
        }
        case PANM_RESTART:{
            menu->state = PanMenuSt_RESTARTED;
            bool newState = false;
            if (PanItem_getValue(PanMenu_itemAt(menu, 0)) == true){
                PanItem_setValue(PanMenu_itemAt(menu, 0), false);
                if (PanMenu_getCurIndex(menu) != 0 )
                    PanMenu_driver(menu, PANM_FIRST_ITEM | modifier);
                else 
                    PanMenu_refresh(menu, 0);
                newState = true;
            //
            } else if (PanMenu_getCurIndex(menu) != 0){
                //
                newState = PanMenu_driver(menu, PANM_FIRST_ITEM | modifier);
                //
            }
            for (int i = 1; i < PanMenu_getItemsCount(menu); ++i){
                newState |= PanMenu_driver(menu, PANM_DESELECT_ITEM | modifier | i);
            }
            return newState;
        }
        case PANM_DISCARD_SELECTOR:{
            if (menu->hasSelector) {
                menu->hasSelector = false;
                menu->state = PanMenuSt_DISCARDED_SELECTOR;
                unsetSelector(menu);
                return true;
            } else {
                menu->state = PanMenuSt_IDLED;
                return false;
            }
        }
        default:{
            if ((option & PANM_JUMP_TO) != 0){ // case PANM_JUMP_TO | index
                unsigned int index = (unsigned int)(option & ~PANM_JUMP_TO);
                assert(index < PanMenu_getItemsCount(menu));
                //
                if (PanMenu_getCurIndex(menu) == index && !modifier) {
                    menu->state = PanMenuSt_IDLED;
                    return false;
                }
                //
                menu->state = PanMenuSt_ADVANCED_SELECTOR;
                unsetSelector(menu);
                setSelector(menu, index);
                return true;
            //
            } else if ((option & PANM_REFRESH_ITEM) != 0){ // case PANM_REFRESH_ITEM | index
                Debug_maxbound(option & ~PANM_REFRESH_ITEM, PanMenu_getItemsCount(menu) - 1);
                menu->state = PanMenuSt_REFRESHED;
                PanMenu_refresh(menu, option & ~PANM_REFRESH_ITEM);
                return true;
            //
            } else if ((option & PANM_TOGGLE_ITEM) != 0){ // case PANM_TOGGLE_ITEM | index
                unsigned int index = (unsigned int)(option & ~PANM_TOGGLE_ITEM);
                Debug_maxbound(index, PanMenu_getItemsCount(menu) - 1);
                //
                if (PanItem_getSelectable(PanMenu_itemAt(menu, index)) == false){
                    menu->state = PanMenuSt_IDLED;
                    return false;
                }
                menu->state = PanMenuSt_TOGGLED;
                //
                PanItem_toggleValue(PanMenu_itemAt(menu, index));
                PanMenu_refresh(menu, index);
                return true;
            //
            } else if ((option & PANM_SELECT_ITEM) != 0){ // case PANM_SELECT_ITEM | index
                unsigned int index = (unsigned int)(option & ~PANM_SELECT_ITEM);
                assert(index < PanMenu_getItemsCount(menu));
                //
                if (PanItem_getSelectable(PanMenu_itemAt(menu, index)) == false
                    || PanItem_getValue(PanMenu_itemAt(menu, index)) == true){
                    menu->state =PanMenuSt_IDLED;
                    return false;
                }
                //
                menu->state = PanMenuSt_TOGGLED;
                PanItem_setValue(PanMenu_itemAt(menu, index), true);
                PanMenu_refresh(menu, index);
                return true;
            //
            } else if ((option & PANM_DESELECT_ITEM) != 0){
                unsigned int index = (unsigned int)(option & ~PANM_DESELECT_ITEM);
                assert(index < PanMenu_getItemsCount(menu));
                //
                if (PanItem_getSelectable(PanMenu_itemAt(menu, index)) == false
                    || PanItem_getValue(PanMenu_itemAt(menu, index)) == false){
                    menu->state = PanMenuSt_IDLED;
                    return false;
                }
                //
                menu->state = PanMenuSt_TOGGLED;
                PanItem_setValue(PanMenu_itemAt(menu, index), false);
                PanMenu_refresh(menu, index);
                return true;
            //
            } else if ((option & PANM_SET_ITEM_GREY) != 0){
                unsigned int index = (unsigned int)(option & ~PANM_SET_ITEM_GREY);
                assert(index < PanMenu_getItemsCount(menu));
                //
                if (PanItem_getSelectable(PanMenu_itemAt(menu, index)) == false){
                    menu->state = PanMenuSt_IDLED;
                    return false;                    
                }
                menu->state = PanMenuSt_TOGGLED;
                PanItem_setSelectable(PanMenu_itemAt(menu, index), false);
                PanMenu_refresh(menu, index);
                return true;
            //
            }
            else Debug_shutdown("PANMENU INTERNAL ERROR: UNHANDLED MENU OPTION RECIEVED.");
            //
            return false;
        }
    }
}

void PanMenu_unpost(PanMenu * menu){
    Debug_exists(menu);
    //
    menu->state = PanMenuSt_UNPOSTED;
    PanItem ** item = PanMenu_getItems(menu);
    unsigned int itemscnt = PanMenu_getItemsCount(menu);
    unsigned int selector = PanMenu_getCurIndex(menu);
    for (int i = 0; i < itemscnt; ++i){
        if (PanItem_getPan(item[i]) == PanItem_getDuppan(item[i])){
            hide_panel(PanItem_getPan(item[i]));
            update_panels();
        //
        } else if (PanItem_getValue(item[i]) == true){
            if (i == selector){
                if (PanMenu_getReselected(menu) != WA_DEFAULT){
                    top_panel(PanItem_getPan(item[i]));
                    update_panels();
                //
                }
            //
            } else if (PanMenu_getSelected(menu) != WA_DEFAULT){
                top_panel(PanItem_getPan(item[i]));
                update_panels();
            //
            } 
        // 
        } else {
            if (i == selector){
                if (PanMenu_getFore(menu) != WA_DEFAULT){
                    top_panel(PanItem_getPan(item[i]));
                    update_panels();
                //
                }
            //
            } else if (PanMenu_getBack(menu) != WA_DEFAULT){
                top_panel(PanItem_getPan(item[i]));
                update_panels();
            //
            }    
        //
        }
    }
}

signed long PanMenu_getPrevItemIndex(PanMenu * menu){
    assert(menu != NULL);
    //
    return (PanMenu_getCurIndex(menu) > 0) ? (signed long)PanMenu_getCurIndex(menu) - 1L : 
           (PanMenu_getCyclic(menu) == true) ? (signed long)PanMenu_getItemsCount(menu) - 1L : -1L;
}

signed long PanMenu_getNextItemIndex(PanMenu * menu){
    assert(menu != NULL);
    //
    return (PanMenu_getCurIndex(menu) < PanMenu_getItemsCount(menu) - 1) ? (signed long)PanMenu_getCurIndex(menu) + 1L : 
           (PanMenu_getCyclic(menu) == true) ? 0L : -1L;
}

PanItem * PanMenu_getPrevItem(PanMenu * menu){
    assert(menu != NULL);
    //
    signed long prevIndex = PanMenu_getPrevItemIndex(menu);
    if (prevIndex == -1L) return NULL;
    return PanMenu_itemAt(menu, prevIndex);
}

PanItem * PanMenu_getNextItem(PanMenu * menu){
    assert(menu != NULL);
    //
    signed long nextIndex = PanMenu_getNextItemIndex(menu);
    if (nextIndex == -1L) return NULL;
    return PanMenu_itemAt(menu, nextIndex);
}
