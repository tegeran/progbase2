#include <witem.h>
#include <assert.h>
#include <error.h>
#include <mem.h>



typedef struct __WItem {
     WItem_Hook   hook; // is executed each time selector is set on this item 
           void * ptr;
         WINDOW * win;
         WINDOW * dupwin;
           bool   value;
           bool   selectable;
} WItem;

WItem * WItem_newStd(WINDOW * win, bool winIsImmutable){
    Debug_exists(win);
    //
    WItem * newbie = malloc(sizeof(WItem));
    Error_checkHeap(newbie);

    *newbie = (WItem){
        .value = false,
        .selectable = true,
        .hook = NULL,
        .ptr = NULL,
        .win = win,
    };
    if (winIsImmutable){
        newbie->dupwin = dupwin(win);
        Error_checkHeap(newbie->dupwin);
    } else {
        newbie->dupwin = win;
    }
    //
    return newbie;
}

WItem * WItem_new(WINDOW * win, 
                        bool   winIsImmutable, 
                        bool   value,
                        bool   selectable, 
                        void * ptr, 
                        WItem_Hook hook){
    //
    Debug_exists(win);
    //
    WItem * newbie = Mem_malloc(sizeof(WItem));
    
    *newbie = (WItem){
        .value = value,
        .selectable = selectable,
        .hook = hook,
        .ptr = ptr,
        .win = win
    };
    if (winIsImmutable){
        newbie->dupwin = dupwin(win);
        Error_checkHeap(newbie->dupwin);
    } else {
        newbie->dupwin = win;
    }
    return newbie;
}

void WItem_free(WItem * self){
    Debug_exists(self);
    //
    if (self->dupwin != self->win) {
        delwin(self->dupwin);        
    }
    free(self);
}


// GETTERS

bool WItem_getValue(WItem * self){
    Debug_exists(self);
    //
    return self->value;
}

WINDOW * WItem_getWin(WItem * self){
    Debug_exists(self);
    //
    return self->win;
}

bool WItem_getSelectable(WItem * self){
    Debug_exists(self);
    //
    return self->selectable;
}
void * WItem_getPtr(WItem * self){
    Debug_exists(self);
    //
    return self->ptr;
}

WItem_Hook WItem_getHook(WItem * self){
    Debug_exists(self);
    //
    return self->hook;
}

// SETTERS

bool WItem_setValue(WItem * self, bool newValue){
    Debug_exists(self);
    //
    bool prev = self->value;
    self->value = newValue;
    return prev;
}

bool WItem_toggleValue(WItem * self){
    Debug_exists(self);
    //
    self->value = !self->value;
    return !self->value;
}    

WINDOW * WItem_setWin(WItem * self, WINDOW * newWin){
    Debug_exists(self);
    //
    WINDOW * prev = self->win;
    self->win = newWin;
    return prev;
}

bool WItem_setSelectable(WItem * self, bool newSelectable){
    Debug_exists(self);
    //
    bool prev = self->selectable;
    self->selectable = newSelectable;
    return prev;
}

bool WItem_toggleSelecrable(WItem * self){
    Debug_exists(self);
    //
    self->selectable = !self->selectable;
    return !self->selectable;
}

void * WItem_setPtr(WItem * self, void * newPtr){
    Debug_exists(self);
    //
    void * prev = self->ptr;
    self->ptr = newPtr;
    return prev;
} 

WItem_Hook WItem_setHook(WItem * self, WItem_Hook newHook){
    Debug_exists(self);
    //
    WItem_Hook prev = self->hook;
    self->hook = newHook;
    return prev;
}


void WItem_execHook(WItem * self){
    Debug_exists(self);
    Debug_exists(WItem_getHook(self) != NULL);
    //
    (*WItem_getHook(self))(self);
}
WINDOW * WItem_getDupwin(WItem * self){
    Debug_exists(self);
    //
    return self->dupwin;
}
WINDOW * WItem_setDupwin(WItem * self, WINDOW * dupwin){
    Debug_exists(self);
    //
    WINDOW * prev = self->dupwin;
    self->dupwin = dupwin;
    return prev;
}