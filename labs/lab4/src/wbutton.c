#include <wbutton.h>
#include <error.h>



typedef struct __WButton {
             WINDOW * textwin;   
               bool   selectable;
        const Frame * frame;
        Proportions   props;
           Location   location;
} WButton;





WButton * WButton_new(wchar_t * label,
                         bool   selectable,
                  const Frame * frame,
                  Proportions   props,
                     Location   location){
    //
    Debug_suppose(label != NULL, "An attempt to set a NULL label for WButton was made");
    //
    WButton * newbie = malloc(sizeof(WButton));
    Error_checkHeap(newbie);
    
    newbie->textwin = Win_new(props, location, frame, stdscr->_attrs);
    newbie->selectable = selectable;
    waddwstr(newbie->textwin, label);
    return newbie;
}

WButton * WButton_newStd(wchar_t * label,
                     Proportions   props,
                        Location   location){
    //
    Debug_suppose(label != NULL, "An attempt to set a NULL label for WButton was made");
    //
    WButton * newbie = malloc(sizeof(WButton));
    Error_checkHeap(newbie);
    
    newbie->textwin = Win_new(props, location, &WF_DOUBLE, stdscr->_attrs);
    newbie->selectable = true;
    waddwstr(newbie->textwin, label);
    return newbie;
}

void WButton_free(WButton * self){
    Debug_exists(self);
    //
    Win_freeFamily(self->textwin);
    free(self);
}

void WButton_kill(WButton * self, attr_t remainder){
    Debug_exists(self);
    //
    Win_killFamily(self->textwin, remainder);
    free(self);
}



bool WButton_getSelectable(WButton * self){
    Debug_exists(self);
    //
    return self->selectable;
}

Proportions WButton_getProps(WButton * self){
    Debug_exists(self);
    //
    return self->props;
}

Location WButton_getLocation(WButton * self){
    Debug_exists(self);
    //
    return self->location;
}

WINDOW * WButton_getTextWin(WButton * self){
    Debug_exists(self);
    //
    return self->textwin;
}

const Frame * WButton_getFrame(WButton * self){
    Debug_exists(self);
    //
    return self->frame;
}

void WButton_setLabel(WButton * self, wchar_t * newLabel){
    Debug_exists(self);
    Debug_suppose(self->textwin != NULL,
                     "An attempt to set label for button with no window was made");
    //
    werase(self->textwin);
    waddwstr(self->textwin, newLabel);
} 

void WButton_wnoutrefresh(WButton * self){
    Debug_exists(self);
    //
    Win_wnoutrefreshFamily(self->textwin);
}

void WButton_refresh(WButton * self){
    Debug_exists(self);
    //
    Win_refreshFamily(self->textwin);
}

bool WButton_setSelectable(WButton * self, bool newSelectable){
    Debug_exists(self);
    //
    bool prev = self->selectable;
    self->selectable = newSelectable;
    return prev;
}

bool WButton_toggleSelectable(WButton * self){
    Debug_exists(self);
    //
    self->selectable = !self->selectable;
    return !self->selectable;
}

const Frame * WButton_setFrame(WButton * self, const Frame * newFrame){
    Debug_exists(self);
    //
    const Frame * prev = self->frame;
    self->frame = newFrame;
    return prev;
}   

Proportions WButton_setProps(WButton * self, Proportions newProps){
    Debug_exists(self);
    //
    Proportions prev = self->props;
    self->props = newProps;
    return prev;
}

Location WButton_setLocation(WButton * self, Location newLocation){
    Debug_exists(self);
    //
    Location prev = self->location;
    self->location = newLocation;
    return prev;
}

WINDOW * WButton_setTextWin(WButton * self, WINDOW * newTextWin){
    Debug_exists(self);
    //
    WINDOW * prev = self->textwin;
    self->textwin = newTextWin;
    return prev;
}