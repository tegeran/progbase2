#include <panels.h>

#include <stdarg.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include <error.h>
#include <mem.h>
#include <windows.h>


void Panel_freeArr(Panel ** self, size_t size){
    Debug_exists(self);
    Debug_minbound(size, 1);
    //
    for (int i = 0; i < size; ++i){
        Debug_exists(self[i]);
        Debug_exists(self[i]->win);
        //
        Panel_free(self[i]);
    }
}

void Panel_free(Panel * self){
    Debug_exists(self);
    Debug_exists(self->win);
    //
    // hide_panel(self->pan);
    // update_panels();
    doupdate();
    del_panel(self->pan);
    Win_freeFamily(self->win);
    free(self);
}


Panel * Panel_new( Proportions   props,
                     Location   beg,
                  const Frame * const frame, ...){
    //
    Panel * self = Mem_malloc(sizeof(Panel));
    if (frame != NULL){
        WINDOW * frameWin = newwin(props.height, props.width, beg.y, beg.x);
        Error_checkHeap(frameWin);
        //
        self->win = derwin(frameWin, props.height - 2, props.width - 2, 1, 1);
        Error_checkHeap(self->win);
        
        // extracting attr_t argument for frame
        va_list argPtr;
        va_start(argPtr, frame);
        attr_t frameAttr = va_arg(argPtr, attr_t);
        va_end(argPtr);
        //
        self->pan = new_panel(frameWin);
        wattrset(frameWin, frameAttr);
        wborder_set(frameWin, &frame->sideL,    &frame->sideR,    
                              &frame->sideT,    &frame->sideB,
                              &frame->cornerTL, &frame->cornerTR,
                              &frame->cornerBL, &frame->cornerBR);
    } else {
        self->win = newwin(props.height, props.width, beg.y, beg.x);
        Error_checkHeap(self->win);
        self->pan = new_panel(self->win);
    }
    return self;
}     

void Panel_updateAll(void){
    update_panels();
}

void Panel_refreshAll(void){
    update_panels();
    doupdate();
}

void Panel_hide(Panel * self){
    Debug_exists(self);
    //
    hide_panel(self->pan);
}

void Panel_show(Panel * self){
    Debug_exists(self);
    //
    show_panel(self->pan);
}