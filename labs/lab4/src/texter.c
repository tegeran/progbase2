#include <texter.h>
#include <windows.h>
#include <ncursesw/curses.h>
#include <ncursesw/form.h>
#include <error.h>
#include <mem.h>
#include <key.h>
#include <panels.h>

typedef struct __Texter {
            String * str;
              FORM * form;
             FIELD * field[2];
             Panel * pan;
    InputProcessor   process;
} Texter;

Texter * Texter_new(InputProcessor process, Proportions props, Location loc, const Frame * const frame, ...){
    Debug_exists(process);
    //
    Texter * newbie = Mem_malloc(sizeof(Texter));
    *newbie = (Texter){
        .process = process,
        .str = String_new(),   
        .field = {new_field(props.height, props.width, 0, 0, 1, 0), NULL},
    };
    if (frame != NULL){
        props.height += 2;
        props.width += 2;
        va_list argPtr;
        va_start(argPtr, frame);
        newbie->pan = Panel_new(props, loc, frame, va_arg(argPtr, attr_t));
        va_end(argPtr);  
    } else {
        newbie->pan = Panel_new(props, loc, NULL);
    }
    newbie->form = new_form(newbie->field);
    set_form_win(newbie->form, newbie->pan->win);
    return newbie;
}

void Texter_free(Texter * self){
    Debug_exists(self);
    //
    free_form(self->form);
    free_field(self->field[0]);
    Panel_free(self->pan);
    String_free(self->str);
    free(self);
}


String * Texter_toNewString(Texter * self){
    Debug_exists(self);
    //
    return String_newCopyOf(self->str);
}

InputProcessor Texter_setProcessor(Texter * self, InputProcessor processor){
    Debug_exists(self);
    Debug_exists(processor);
    //
    InputProcessor prev = self->process;
    self->process = processor;
    return prev;
}

InputProcessor Texter_processorAt(Texter * self){
    Debug_exists(self);
    //
    return self->process;
}


#define isascii(c)	(((c) & ~0x7f) == 0)

bool Texter_process(Texter * self, wint_t input){
    if (!self->process(&input)) return false;
    //
    if (input == KEY_BACKSPACE){
        if (String_length(self->str) > 0){
            String_reduce(self->str, String_length(self->str) - 1);
            form_driver(self->form, REQ_DEL_PREV);
        }
    } else {
        if (!isascii(input)) return false;
        String_appendChar(self->str, input);
        form_driver(self->form, input);
    }
    return true;
}

void Texter_post(Texter * self){
    Panel_show(self->pan);
    Panel_refreshAll();
}

void Texter_unpost(Texter * self, attr_t remainder){
    Panel_hide(self->pan);
}

size_t Texer_strlen(Texter * self){
    Debug_exists(self);
    //
    return String_length(self->str);
}