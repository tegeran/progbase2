#include <key.h>
#include <error.h>


bool Key_isArrow(chtype token){
    return token >= KEY_DOWN && token <= KEY_RIGHT;
}

bool Key_isWASD(chtype token){
    switch (token){
        case 'W':
        case 'w':

        case 'A':
        case 'a':

        case 'S':
        case 's':
        
        case 'D':
        case 'd': return true;
        default:  return false;
    }
}

void Key_detectMouseMovements(void){
    wprintf(L"\033[?1003h\n");   
    wprintf(L"\033[1;1H"); // very thanks to R.A. Hadyniak
}

void Key_undetectMouseMovements(void){
    printf("\033[?1003l\n");
}

bool Key_mouseTouched(WINDOW * win){
    Debug_exists(win);
    MEVENT mevent;
    if (!getmouse(&mevent)) return false;
    return (mevent.bstate == BUTTON1_PRESSED) ? wenclose(win, mevent.y, mevent.x) : false;   
}