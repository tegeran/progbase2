#include <dispatcher.h>


Location getLineCenter(Proportions props, unsigned short y){
    return loc(y, (COLS - props.width) >> 1);
}
Location getScreenCenter(Proportions props){
    return loc((LINES - props.height) >> 1, (COLS - props.width) >> 1);
}

/*
void cleanScreen(void){
    touchwin(stdscr);
    wnoutrefresh(stdscr);
    doupdate();
}*/

void Input_listener(EventHandler * self, Event * event){
    Debug_exists(self);
    Debug_exists(event);
    //
    static WINDOW * inputWin = NULL;
    static wint_t data = '\0';
    switch (event->type){
        case StartEventTypeId:{
            inputWin = newwin(1, 1, 1, 1);
            keypad(inputWin, true);
            return;
        }
        case UpdateEventTypeId:{
            wget_wch(inputWin, &data);
            if (data == KEY_MOUSE) flushinp();
            emitData(UserSignal, &data);
            return;
        }
        case ExitEventTypeId:{
            delwin(inputWin);
            return;
        }
        default:break;
    }
}


int __Dispatcher_dialog(int state, wint_t input, wchar_t * essence){
    static Panel * panel = NULL;
    static PBMenu * menu = NULL;
    switch (state){
        case REACT:{
            if (PBMenu_driver(menu, BM_REACT, input)){
                doupdate();
                if (PBMenu_isButtonSelected(menu, 0))
                    return CHOOSE_OK;
                else if (PBMenu_isButtonSelected(menu, 1)){
                    return CHOOSE_CANCEL;
                }
            } 
            Panel_refreshAll();
            return CHOOSE_IDLE;
        }
        case INIT:{
            const Proportions props = props(9, 38);
            Location loc = getScreenCenter(props);
            loc.y -= 2;
            panel = Panel_new(props, loc, &WF_SINGLE, COLOR_PAIR(2));
            waddwstr(panel->win, essence);
            /////// MENU
            Proportions BUTTONS_PROPS = props(3, 10);
            const Location BUTTONS_LOC = getScreenCenter(props(3, 21));
            menu = _PBMenu_new(&attrs,  BUTTONS_PROPS, BUTTONS_LOC, HLAYOUT,
                
                L" Okay   ", L" Cancel "
            
            );
            PBMenu_post(menu);
            Panel_refreshAll();
            ///////////////////////////////////////////////////////////////////
            return CHOOSE_IDLE;
        }
        case FREE:{
            PBMenu_unpost(menu);
            PBMenu_freeWhole(menu);
            Panel_free(panel);
            update_panels();
            return CHOOSE_IDLE;
        }
    }
    return CHOOSE_IDLE;
}

void List_freeProviders(List * list){
    List_freeWhole(list, (Destructor)Provider_freeWhole);
}