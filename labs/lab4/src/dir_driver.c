#include <dir_driver.h>
#include <mem.h>
#include <error.h>
#include <list_ext.h>

typedef struct __DirDriver {
    String * path;
} DirDriver;

DirDriver * DirDriver_new(String * startDir){
    DirDriver * newbie = Mem_malloc(sizeof(DirDriver));
    //
    *newbie = (DirDriver){
        .path = String_newCopyOf(startDir)
    };
    return newbie; 
}

DirDriver * DirDriver_newLocal(void){
    DirDriver * newbie = Mem_malloc(sizeof(DirDriver));
    //
    *newbie = (DirDriver){
        .path = String_toNewString(".")
    };
    return newbie; 
}

void DirDriver_free(DirDriver * self){
    Debug_exists(self);
    //
    String_free(self->path);
    free(self);
}

void DirDriver_appendNewIndirs(DirDriver * self, List * dirs){
    Debug_exists(self);
    Debug_exists(dirs);
    //
    List * temp = List_new();
    Mem_sgetDirNames(self->path, MEM_PUBLIC_DIR, temp);
    List_listAppend(dirs, temp);
    List_free(temp);
}

void DirDriver_appendNewFiles(DirDriver * self, List * files){
    Debug_exists(self);
    Debug_exists(files);    
    //
    List * temp = List_new();
    Mem_sgetDirNames(self->path, MEM_PUBLIC_FILE, temp);
    List_listAppend(files, temp);
    List_free(temp);
}

#include <panels.h>

void DirDriver_goNextDir(DirDriver * self, String * nextDir){
    Debug_exists(self);
    Debug_exists(nextDir);
    //
    String_appendChar(self->path, '/');
    String_concatenate(self->path, nextDir);
}


void DirDriver_goPrevDir(DirDriver * self){
    signed long long slash = String_searchLastChar(self->path, '/');
    if (slash >= 0 && String_length(self->path) >= 4){
        if (   String_at(self->path, String_length(self->path) - 1) == '.'
            && String_at(self->path, String_length(self->path) - 2) == '.'){
            String_append(self->path, "/..");
        } else {
            String_reduce(self->path, slash);
        }
    } else {
        String_append(self->path, "/..");
    }
}