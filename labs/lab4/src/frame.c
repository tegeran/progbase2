#include <frame.h>
#include <assert.h>
#include <error.h>
#include <wchar.h>
#include <mem.h>


Frame * Frame_new(wchar_t  cornerTopL,
                  wchar_t  sideTop,
                  wchar_t  cornerTopR,
                  wchar_t  sideLeft,
                  wchar_t  sideRight,
                  wchar_t  cornerBtmL,
                  wchar_t  sideBtm,
                  wchar_t  cornerBtmR){
    //
    Frame * newbie = Mem_malloc(sizeof(Frame));
    *newbie = (Frame)frame(cornerTopL, sideTop, cornerTopR, sideLeft, 
                   sideRight, cornerBtmL, sideBtm, cornerBtmR);
    return newbie;
}

void Frame_free(Frame * self){
    Debug_exists(self);
    //
    free(self);
}

cchar_t Frame_setSideT(Frame * self, cchar_t sideT){
    Debug_exists(self);
    //
    cchar_t keeper = self->sideT;
    self->sideT = sideT;
    return keeper;
}

cchar_t Frame_setSideB(Frame * self, cchar_t sideB){
    Debug_exists(self);
    //
    cchar_t keeper = self->sideB;
    self->sideB = sideB;
    return keeper;
}

cchar_t Frame_setSideL(Frame * self, cchar_t sideL){
    Debug_exists(self);
    //
    cchar_t keeper = self->sideL;
    self->sideL = sideL;
    return keeper;
}
cchar_t Frame_setSideR(Frame * self, cchar_t sideR){
    Debug_exists(self);
    //
    cchar_t keeper = self->sideR;
    self->sideR = sideR;
    return keeper;
}

cchar_t Frame_setCornerTL(Frame * self, cchar_t cornerTL){
    Debug_exists(self);
    //
    cchar_t keeper = self->cornerTL;
    self->cornerTL = cornerTL;
    return keeper;
}
cchar_t Frame_setCornerTR(Frame * self, cchar_t cornerTR){
    Debug_exists(self);
    //
    cchar_t keeper = self->cornerTR;
    self->cornerTR = cornerTR;
    return keeper;
}

cchar_t Frame_setCornerBL(Frame * self, cchar_t cornerBL){
    Debug_exists(self);
    //
    cchar_t keeper = self->cornerBL;
    self->cornerBL = cornerBL;
    return keeper;
}
cchar_t Frame_setCornerBR(Frame * self, cchar_t cornerBR){
    Debug_exists(self);
    //
    cchar_t keeper = self->cornerBR;
    self->cornerBR = cornerBR;
    return keeper;
}

//////////////////////////////////////////

cchar_t Frame_getSideT(Frame * self){
    Debug_exists(self);
    //
    return self->sideT;
}
cchar_t Frame_getSideB(Frame * self){
    Debug_exists(self);
    //
    return self->sideB;
}

cchar_t Frame_getSideL(Frame * self){
    Debug_exists(self);
    //
    return self->sideL;
}
cchar_t Frame_getSideR(Frame * self){
    Debug_exists(self);
    //
    return self->sideR;
}

cchar_t Frame_getCornerTL(Frame * self){
    Debug_exists(self);
    //
    return self->cornerTL;
}
cchar_t Frame_getCornerTR(Frame * self){
    Debug_exists(self);
    //
    return self->cornerTR;
}

cchar_t Frame_getCornerBL(Frame * self){
    Debug_exists(self);
    //
    return self->cornerBL;
}

cchar_t Frame_getCornerBR(Frame * self){
    Debug_exists(self);
    //
    return self->cornerBR;
}