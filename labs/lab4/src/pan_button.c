#include <pan_button.h>
#include <error.h>
#include <mem.h>


typedef struct __PanButton {
             WINDOW * textwin;   
              PANEL * panel;
               bool   selectable;
        const Frame * frame;
        Proportions   props;
           Location   location;
} PanButton;


static PanButton * Private_getBlankPanButton(bool selectable,
                                             const Frame * frame,
                                             Proportions props,
                                             Location location){
    //
    //
    PanButton * newbie = Mem_malloc(sizeof(PanButton));
    
    newbie->textwin = Win_new(props, location, frame, stdscr->_attrs);
    if (frame == NULL)
        newbie->panel = new_panel(newbie->textwin);
    else 
        newbie->panel = new_panel(wgetparent(newbie->textwin));
    Error_checkHeap(newbie->panel);
    newbie->selectable = selectable;           
    newbie->frame = frame;
    newbie->props = props;
    newbie->location = location;




    return newbie;                                 
}                                                


PanButton * PanButton_new(const wchar_t * label,
                         bool   selectable,
                  const Frame * frame,
                  Proportions   props,
                     Location   location){
    //
    Debug_suppose(label != NULL, "An attempt to set a NULL label for PanButton was made");
    //
    PanButton * newbie = Private_getBlankPanButton(selectable, frame, props, location);
    waddwstr(newbie->textwin, label);
    return newbie;
}

PanButton * PanButton_newStd(const wchar_t * label,
                     Proportions   props,
                        Location   location){
    //
    Debug_suppose(label != NULL, "An attempt to set a NULL label for PanButton was made");
    //
    PanButton * newbie = Private_getBlankPanButton(true, &WF_DOUBLE, props, location);
    waddwstr(newbie->textwin, label);
    return newbie;
}



PanButton * PanButton_chNew(const char * label,
                             bool   selectable,
                      const Frame * frame,
                      Proportions   props,
                         Location   location){
    //
    Debug_suppose(label != NULL, "An attempt to set a NULL label for PanButton was made");
    //
    PanButton * newbie = Private_getBlankPanButton(selectable, frame, props, location);
    waddstr(newbie->textwin, label);
    return newbie;
}

PanButton * PanButton_chNewStd(const char * label,
                  Proportions   props,
                     Location   location){
    //
    Debug_suppose(label != NULL, "An attempt to set a NULL label for PanButton was made");
    //
    PanButton * newbie = Private_getBlankPanButton(true, &WF_DOUBLE, props, location);
    waddstr(newbie->textwin, label);
    return newbie;
}


void PanButton_free(PanButton * self){
    Debug_exists(self);
    //
    del_panel(self->panel);
    Win_freeFamily(self->textwin);
    free(self);
}

void PanButton_kill(PanButton * self, attr_t remainder){
    Debug_exists(self);
    //
    del_panel(self->panel);
    Win_killFamily(self->textwin, remainder);
    free(self);
}



bool PanButton_getSelectable(PanButton * self){
    Debug_exists(self);
    //
    return self->selectable;
}

Proportions PanButton_getProps(PanButton * self){
    Debug_exists(self);
    //
    return self->props;
}

Location PanButton_getLocation(PanButton * self){
    Debug_exists(self);
    //
    return self->location;
}

WINDOW * PanButton_getTextWin(PanButton * self){
    Debug_exists(self);
    //
    return self->textwin;
}

PANEL * PanButton_getPanel(PanButton * self){
    Debug_exists(self);
    //
    return self->panel;
}

const Frame * PanButton_getFrame(PanButton * self){
    Debug_exists(self);
    //
    return self->frame;
}

void PanButton_setLabel(PanButton * self, wchar_t * newLabel){
    Debug_exists(self);
    Debug_suppose(self->textwin != NULL,
                     "An attempt to set label for button with no window was made");
    //
    werase(self->textwin);
    waddwstr(self->textwin, newLabel);
} 

void PanButton_update(PanButton * self){
    Debug_exists(self);
    //
    update_panels();
}

void PanButton_refresh(PanButton * self){
    Debug_exists(self);
    //
    update_panels();
    doupdate();
}

bool PanButton_setSelectable(PanButton * self, bool newSelectable){
    Debug_exists(self);
    //
    bool prev = self->selectable;
    self->selectable = newSelectable;
    return prev;
}

bool PanButton_toggleSelectable(PanButton * self){
    Debug_exists(self);
    //
    self->selectable = !self->selectable;
    return !self->selectable;
}

const Frame * PanButton_setFrame(PanButton * self, const Frame * newFrame){
    Debug_exists(self);
    //
    const Frame * prev = self->frame;
    self->frame = newFrame;
    return prev;
}   

Proportions PanButton_setProps(PanButton * self, Proportions newProps){
    Debug_exists(self);
    //
    Proportions prev = self->props;
    self->props = newProps;
    return prev;
}

Location PanButton_setLocation(PanButton * self, Location newLocation){
    Debug_exists(self);
    //
    Location prev = self->location;
    self->location = newLocation;
    return prev;
}

WINDOW * PanButton_setTextWin(PanButton * self, WINDOW * newTextWin){
    Debug_exists(self);
    Debug_exists(newTextWin);
    //
    WINDOW * prev = self->textwin;
    self->textwin = newTextWin;
    return prev;
}

PANEL * PanButton_setPanel(PanButton * self, PANEL * newPanel){
    Debug_exists(self);
    Debug_exists(newPanel);
    //
    PANEL * prev = self->panel;
    self->panel = newPanel;
    return prev;
}