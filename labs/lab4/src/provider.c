#include <provider.h>
#include <mem.h>
#include <error.h>

Provider * Provider_new(void){
    Provider * newbie = Mem_malloc(sizeof(Provider));
    *newbie = (Provider){
        .name = NULL,
        .cloud = NULL,
        .tariff = 0,
        .loss = 0,
        .users = 0
    };
    return newbie;
}

Provider * Provider_newWhole(String * name,
                          DataCloud * cloud, 
                          double      tariff, 
                          double      loss, 
                          size_t      users){
        //
    Provider * newbie = Mem_malloc(sizeof(Provider));
    *newbie = (Provider){
        .name = name,
        .cloud = cloud,
        .tariff = tariff,
        .loss = loss,
        .users = users
    };
    return newbie;
}

void Provider_free(Provider * self){
    Debug_exists(self);
    //
    free(self);
}

void Provider_freeWhole(Provider * self){
    Debug_exists(self);
    //
    if (self->name != NULL) String_free(self->name);
    if (self->cloud != NULL) DataCloud_free(self->cloud);
    free(self);
}

String * Provider_nameAt(Provider * self){
    Debug_exists(self);
    //
    return self->name;
}

double Provider_tariffAt(Provider * self){
    Debug_exists(self);
    //
    return self->tariff;
}

double Provider_lossAt(Provider * self){
    Debug_exists(self);
    //
    return self->loss;
}

size_t Provider_usersAt(Provider * self){
    Debug_exists(self);
    //
    return self->users;
}

DataCloud * Provider_cloudAt(Provider * self){
    Debug_exists(self);
    //
    return self->cloud;
}



String * Provider_setName(Provider * self, String * name){
    Debug_exists(self);
    //
    String * prev = self->name;
    self->name = name;
    return prev;
}

double Provider_setTariff(Provider * self, double tariff){
    Debug_exists(self);
    //
    double prev = self->tariff;
    self->tariff = tariff;
    return prev;
}

double Provider_setLoss(Provider * self, double loss){
    Debug_exists(self);
    //
    double prev = self->loss;
    self->loss = loss;
    return prev;
}

size_t Provider_setUsers(Provider * self, size_t users){
    Debug_exists(self);
    //
    size_t prev = self->users;
    self->users = users;
    return prev;
}

DataCloud * Provider_setCloud(Provider * self, DataCloud * cloud){
    Debug_exists(self);
    //
    DataCloud * prev = self->cloud;
    self->cloud = cloud;
    return prev;
}



bool Provider_hasCloud(Provider * self){
    Debug_exists(self);
    //
    return self->cloud != NULL;
}

bool Provider_hasName(Provider * self){
    Debug_exists(self);
    //
    return self->name != NULL;
}


Provider * Provider_stringsToNewProvider(const List * strings){
    if (List_size(strings) != 6 && List_size(strings) != 4) return NULL;
    double tariff;
    if (!String_scanFormat(List_at(strings, 1), "%lf", &tariff)
        || !Provider_isValidTariff(tariff)){
        goto error;
    }
    double loss;
    if(!String_scanFormat(List_at(strings, 2), "%lf", &loss)
        || !Provider_isValidLoss(loss)){
        goto error;
    }
    size_t users;
    if (!String_scanFormat(List_at(strings, 3), "%lu", &users)){
        goto error;
    }
    DataCloud * cloud = NULL;
    if (List_size(strings) == 6) {
        double cloudCapacity;
        if (String_scanFormat(List_at(strings, 4), "%lf", &cloudCapacity)) {
            if (!DataCloud_isValidCapacity(cloudCapacity)) {
                goto error;
            }
            cloud = DataCloud_new(cloudCapacity, 0);
        }
        double cloudTariff;
        if (String_scanFormat(List_at(strings, 5), "%lf", &cloudTariff)) {
            if (!DataCloud_isValidTariff(cloudTariff)) {
                if (cloud)
                    goto err_cloud;
                else
                    goto error;
            }
            DataCloud_setTariff(cloud, cloudTariff);
        }
    }
    Provider * provider = Provider_newWhole(
        String_newCopyOf(List_at(strings, 0)),
        cloud,
        tariff,
        loss,
        users
    );
    return provider;

    err_cloud:
        DataCloud_free(cloud);
    error: 
        return NULL;
    
}



bool Provider_isValidTariff(double tariff){
    return tariff >= 0;
}

bool Provider_isValidLoss(double loss){
    return loss <= 100 && loss >= 0;
}


CsvRow * Provider_toNewCsvRow(Provider * self){

    CsvRow * newbie = CsvRow_new();
    CsvRow_add(newbie, String_newCopyOf(self->name));

    String * s = String_new();
    String_appendFormat(s, "%lf", self->tariff);
    CsvRow_add(newbie, s);

    s = String_new();
    String_appendFormat(s, "%lf", self->loss);
    CsvRow_add(newbie, s);

    s = String_new();
    String_appendFormat(s, "%lu", self->users);
    CsvRow_add(newbie, s);

    if (self->cloud) {
        s = String_new();
        String_appendFormat(s, "%lf", self->cloud->capacity);
        CsvRow_add(newbie, s);

        s = String_new();
        String_appendFormat(s, "%lf", self->cloud->tariff);
        CsvRow_add(newbie, s);
    }
    return newbie;
}