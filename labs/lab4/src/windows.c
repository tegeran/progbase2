#include <windows.h>

#include <stdarg.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include <error.h>

unsigned short black(unsigned char intensity){
    Debug_maxbound(intensity, 5);
    return (intensity == 5) ? 0 : (236 - intensity);
}
unsigned short white(unsigned char intensity){
    Debug_maxbound(intensity, 5);
    return (intensity == 5) ? 7 : (251 + intensity);
}

void Win_addFrame(WINDOW * win,
            Location   loc, 
         Proportions   props,
         const Frame * frame){
    //
    Debug_exists(win);
    Debug_exists(frame);
    //
    Debug_minbound(loc.y, 0);
    Debug_minbound(loc.x, 0);
    //
    Debug_minbound(props.height, 2);
    Debug_minbound(props.width, 2);
    //
    Debug_UTF8();
    //
    mvwadd_wch(win, loc.y,                    loc.x,                   &frame->cornerTL);
    mvwadd_wch(win, loc.y,                    loc.x + props.width - 1, &frame->cornerTR);
    mvwadd_wch(win, loc.y + props.height - 1, loc.x,                   &frame->cornerBL);
    mvwadd_wch(win, loc.y + props.height - 1, loc.x + props.width - 1, &frame->cornerBR);
    props.width -= 2;
    ++loc.x;
    if (props.width > 0){
        mvwhline_set(win, loc.y,                    loc.x, &frame->sideT, props.width);
        mvwhline_set(win, loc.y + props.height - 1, loc.x, &frame->sideB, props.width);
    }
    props.height -= 2;
    ++loc.y;
    if (props.height > 0){
        mvwvline_set(win, loc.y, loc.x - 1,           &frame->sideL, props.height);
        mvwvline_set(win, loc.y, loc.x + props.width, &frame->sideR, props.height);
    }
    wmove(win, loc.y + props.height, loc.x + props.width);
}

chtype Win_vanishChar(WINDOW * win){
    Debug_exists(win);
    //
    chtype vanished = winch(win);
    waddch(win, ' ');
    Win_shiftMove(win, -1);
    return vanished;
}

void Win_vanishLine(WINDOW * win){
    Debug_exists(win);
    mvwhline(win, Win_cury(win), 0, ' ', Win_maxx(win) + 1);
}

void Win_shiftMove(WINDOW * win, signed int shift){
    Debug_exists(win);
    signed int lineLoc = Win_cury(win) * (Win_maxx(win) + 1) + Win_curx(win) + shift;
    if (lineLoc <= 0) {
        wmove(win, 0, 0);
        return;
    }
    unsigned int newy = lineLoc / (Win_maxx(win) + 1);
    if (newy > Win_maxy(win) /* || newLocation.x <= Win_maxx(win) */ )
        wmove(win, Win_maxy(win), Win_maxx(win));
    else
        wmove(win, newy, lineLoc - (newy * (Win_maxx(win) + 1))); 
}

void Win_vanish(WINDOW * win, attr_t attr){
    Debug_exists(win);
    //
    wbkgd(win, attr);
    werase(win);
}


void Win_freeFamily(WINDOW * win){
    Debug_exists(win);
    //
    
    WINDOW * winFrame = wgetparent(win);
    delwin(win);
    if (winFrame != NULL) delwin(winFrame);
}


void Win_freeFamilyArr(WINDOW ** win, size_t size){
    Debug_exists(win);
    Debug_minbound(size, 1);
    //
    for (int i = 0; i < size; ++i){
        Debug_exists(win[i]);
        //
        Win_freeFamily(win[i]);
    }
}

void Win_freeArr(WINDOW ** win, size_t size){
    Debug_exists(win);
    Debug_minbound(size, 1);
    //
    for (int i = 0; i < size; ++i){
        Debug_exists(win[i]);
        //
        delwin(win[i]);
    }
}

void Win_kill(WINDOW * win, attr_t attr){
    Debug_exists(win);
    //
    Win_vanish(win, attr);
    wnoutrefresh(win);    // doupdate() is left for user
    delwin(win);
}


void Win_killFamily(WINDOW * win, attr_t attr){ 
    Debug_exists(win);
    //
    WINDOW * winFrame = wgetparent(win);
    if (winFrame != NULL){
        Win_vanish(winFrame, attr);
        wnoutrefresh(winFrame);       // doupdate() is left for user  
        delwin(win);                 // all ancestors must be freed before freeing parent window 
        delwin(winFrame);
    //
    } else {
        Win_vanish(win, attr);
        wnoutrefresh(win);
        delwin(win);
    }
}


void Win_fill(WINDOW * win, const cchar_t * const fillBy){
    Debug_exists(win);
    //
    const size_t Win_MAXY = Win_maxy(win);
    const size_t Win_MAXX = Win_maxx(win);
    if (fillBy == NULL){
        for (int i = 0; i <= Win_MAXY; i++){
            mvwhline(win, i, 0, ' ', Win_MAXX + 1);
        }    
        wmove(win, 0, 0);
    } else {
        Debug_UTF8();
        for (int i = 0; i <= Win_MAXY; i++){
            mvwhline_set(win, i, 0, fillBy, Win_MAXX + 1);
        }    
        wmove(win, 0, 0);
    }
}

WINDOW * Win_newDerived(   WINDOW * parent,
                      Proportions   props,
                         Location   beg,
                      const Frame * const frame, ...){
    //
    Debug_exists(parent);
    //
    WINDOW * win;
    if (frame != NULL){
        WINDOW * frameWin = derwin(parent, props.height, props.width, beg.y, beg.x);
        Error_checkHeap(frameWin);
        
        //
        win = derwin(frameWin, props.height - 2, props.width - 2, 1, 1);
        Error_checkHeap(win);
        // extracting attr_t argument for frame
        va_list argPtr;
        va_start(argPtr, frame);
        attr_t frameAttr = va_arg(argPtr, attr_t);
        va_end(argPtr);
        //
        wattrset(frameWin, frameAttr);
        wborder_set(frameWin, &frame->sideL,    &frame->sideR,    
                              &frame->sideT,    &frame->sideB,
                              &frame->cornerTL, &frame->cornerTR,
                              &frame->cornerBL, &frame->cornerBR);

        // wmove(win, 0, 0);
    } else {
        win = derwin(parent, props.height, props.width, beg.y, beg.x);
        Error_checkHeap(win);
    }    
    return win;
}

WINDOW * Win_new( Proportions   props,
                     Location   beg,
                  const Frame * const frame, ...){
    //
    WINDOW * win;
    if (frame != NULL){
        WINDOW * frameWin = newwin(props.height, props.width, beg.y, beg.x);
        Error_checkHeap(frameWin);
        //
        win = derwin(frameWin, props.height - 2, props.width - 2, 1, 1);
        Error_checkHeap(win);
        
        // extracting attr_t argument for frame
        va_list argPtr;
        va_start(argPtr, frame);
        attr_t frameAttr = va_arg(argPtr, attr_t);
        va_end(argPtr);
        //
        wattrset(frameWin, frameAttr);
        wborder_set(frameWin, &frame->sideL,    &frame->sideR,    
                              &frame->sideT,    &frame->sideB,
                              &frame->cornerTL, &frame->cornerTR,
                              &frame->cornerBL, &frame->cornerBR);
                              
        // wmove(win, 0, 0);
    } else {
        win = newwin(props.height, props.width, beg.y, beg.x);
        Error_checkHeap(win);
    }
    return win;
}       

void Win_appendAttr(WINDOW * win, attr_t attr){
    Debug_exists(win);
    //
    Location max;
    getmaxyx(win, max.y, max.x);
    if (PAIR_NUMBER(attr) == 0){
        cchar_t cur;
        for (int i = 0; i <= max.y; ++i){
            for (int j = 0; j <= max.x; ++j){
                mvwin_wch(win, i, j, &cur);
                cur.attr |= attr;
                wadd_wch(win, &cur);
            }
        }
    } else {
        wstandend(win);
        cchar_t cur;
        for (int i = 0; i <= max.y; ++i){
            for (int j = 0; j <= max.x; ++j){
                mvwin_wch(win, i, j, &cur);
                cur.attr = attr | (cur.attr & ~A_COLOR);
                wadd_wch(win, &cur);
            }
        }
    }
}

void Win_wnoutrefreshFamily(WINDOW * win){
    Debug_exists(win);
    //
    WINDOW * cur = win;
    while (wgetparent(cur) != NULL){
        cur = wgetparent(cur);
    }
    wnoutrefresh(cur);
}

void Win_refreshFamily(WINDOW * win){
    Win_wnoutrefreshFamily(win);
    doupdate();
}
