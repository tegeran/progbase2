#include <bmenu.h>
#include <pbmenu.h>
#include <pan_menu.h>
#include <pan_button.h>
#include <error.h>
#include <key.h>
#include <windows.h>
#include <mem.h>
#include <array.h>


typedef struct __PBMenu{
              List * buttons;
           PanMenu * menu;
} PBMenu;


PBMenu * PBMenu_new (wchar_t ** buttonLabels,
                unsigned int    size, 
                 PBMenuAttrs  * attrs,
                 Proportions    props,
                    Location    location,
                     BLayout    layout){
    //
    Debug_exists(buttonLabels);
    Debug_exists(*buttonLabels);
    //
    PBMenu * newbie = Mem_malloc(sizeof(PBMenu));
    newbie->buttons = List_new();
    PANEL * pans[size];
    if (layout.orientation == VERTICAL_PBM){
        for (int i = 0; i < size; ++i){
            List_addLast(newbie->buttons, PanButton_new(buttonLabels[i], true, attrs->frame, props, location));
            location.y += props.height + layout.spacing;
            pans[i] = PanButton_getPanel(List_at(newbie->buttons, i));
        }
    } else {
        for (int i = 0; i < size; ++i){
            List_addLast(newbie->buttons, PanButton_new(buttonLabels[i], true, attrs->frame, props, location));
            location.x += props.width + layout.spacing;
            pans[i] = PanButton_getPanel(List_at(newbie->buttons, i));
        }
    }
    newbie->menu = PanMenu_new(attrs->attrs, PanItem_newArr(pans, size, false), size, 0, true);
    return newbie;
}

PBMenu * PBMenu_chNew (char ** buttonLabels,
               unsigned int    size, 
                PBMenuAttrs *  attrs,
                Proportions    props,
                   Location    location,
                    BLayout    layout){
    //
    Debug_exists(buttonLabels);
    Debug_exists(*buttonLabels);
    //
    PBMenu * newbie = Mem_malloc(sizeof(PBMenu));
    newbie->buttons = List_new();
    PANEL * pans[size];
    if (layout.orientation == VERTICAL_PBM){
        for (int i = 0; i < size; ++i){
            List_addLast(
                newbie->buttons, 
                PanButton_chNew(
                    buttonLabels[i], 
                    true, 
                    attrs->frame, 
                    props, 
                    location
                ));
            location.y += props.height + layout.spacing;
            pans[i] = PanButton_getPanel(List_at(newbie->buttons, i));
        }
    } else {
        for (int i = 0; i < size; ++i){
            List_addLast(
                newbie->buttons, 
                PanButton_chNew(
                    buttonLabels[i], 
                    true, 
                    attrs->frame, 
                    props, 
                    location
                ));
            location.x += props.width + layout.spacing;
            pans[i] = PanButton_getPanel(List_at(newbie->buttons, i));
        }
    }
    newbie->menu = PanMenu_new(attrs->attrs, PanItem_newArr(pans, size, false), size, 0, true);
    return newbie;
}

PBMenu * PBMenu_strNew (List *  buttonLabels,
                 PBMenuAttrs *  attrs,
                 Proportions    props,
                    Location    location,
                     BLayout    layout){
 //
    Debug_exists(buttonLabels);
    //
    PBMenu * newbie = Mem_malloc(sizeof(PBMenu));
    newbie->buttons = List_new();
    PANEL * pans[List_size(buttonLabels)];
    if (layout.orientation == VERTICAL_PBM){
        for (int i = 0; i < List_size(buttonLabels); ++i){
            List_addLast(
                newbie->buttons, 
                PanButton_chNew(String_readOnly(List_at(buttonLabels, i)), 
                true,
                attrs->frame,
                props, 
                location
            ));
            location.y += props.height + layout.spacing;
            pans[i] = PanButton_getPanel(List_at(newbie->buttons, i));
        }
    } else {
        for (int i = 0; i < List_size(buttonLabels); ++i){
            List_addLast(
                newbie->buttons, 
                PanButton_chNew(String_readOnly(List_at(buttonLabels, i)), 
                true,
                attrs->frame,
                props, 
                location
            ));
            location.x += props.width + layout.spacing;
            pans[i] = PanButton_getPanel(List_at(newbie->buttons, i));
        }
    }
    newbie->menu = PanMenu_new(
        attrs->attrs, 
        PanItem_newArr(pans, List_size(buttonLabels), false), 
        List_size(buttonLabels), 0, true
    );
    return newbie;
}


void PBMenu_freeWhole(PBMenu * self){
    Debug_exists(self);
    //
    PanItem_freeArr(PanMenu_getItems(self->menu), PanMenu_getItemsCount(self->menu));
    PanMenu_free(self->menu);
    List_freeWhole(self->buttons, (Destructor)PanButton_free);
    free(self);
}

void PBMenu_free(PBMenu * self){
    Debug_exists(self);
    //
    PanItem_freeArr(PanMenu_getItems(self->menu), PBMenu_getButtonsCount(self));
    PanMenu_free(self->menu);
    free(self);
}

PBMenu * PBMenu_newBy(List * buttons, unsigned int size, PBMenuAttrs * attrs){
        //
        Debug_exists(buttons);
        //
        PBMenu * newbie = Mem_malloc(sizeof(PBMenu));
        newbie->buttons = buttons;
        PANEL * pans[size];
        for (int i = 0; i < size; ++i){
            if (wgetparent(PanButton_getTextWin(List_at(buttons, i))) == NULL)
            pans[i] = PanButton_getPanel(List_at(buttons, i));
            else
            pans[i] = PanButton_getPanel(List_at(buttons, i));
        }
        newbie->menu = PanMenu_new(attrs->attrs, PanItem_newArr(pans, size, false), size, 0, true);
        return newbie;
}

void PBMenu_mpost(PBMenu * self){
    Debug_exists(self);
    //
    PanMenu_post(self->menu, -1LL);
}

void PBMenu_post(PBMenu * self){
    Debug_exists(self);
    //
    PanMenu_post(self->menu, PanMenu_getCurIndex(self->menu));
}

void PBMenu_unpost(PBMenu * self){
    Debug_exists(self);
    //
    PanMenu_unpost(self->menu);
}


unsigned int PBMenu_getButtonsCount(PBMenu * self){
    Debug_exists(self);
    //
    return List_size(self->buttons);
}


bool PBMenu_driver(PBMenu * bmenu, unsigned long option, wint_t input){
    Debug_exists(bmenu);
    //
    PanMenu * menu = bmenu->menu;
    //
    switch(option){
        case PBM_REACT:{
            switch (input){
                case L'W':
                case L'w':
                case L'Ц':
                case L'ц':
                case KEY_UP:{
                    signed long prevIndex = PanMenu_getPrevSelectableIndex(menu);
                    if (prevIndex == -1L) return false;
                    if (prevIndex > PanMenu_getCurIndex(menu)){
                        return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)prevIndex);
                    //
                    } else {
                        PanItem * prevItem = PanMenu_itemAt(menu, prevIndex);
                        if (prevItem != NULL && Win_begy(PanItem_getWin(PanMenu_getCurItem(menu))) >
                                                Win_begy(PanItem_getWin(prevItem))){
                            return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)prevIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case L'S':
                case L's':
                case L'Ы':
                case L'ы':
                case L'і':
                case L'І':
                case KEY_DOWN:{
                    signed long nextIndex = PanMenu_getNextSelectableIndex(menu);
                    if (nextIndex == -1) return false;
                    if (nextIndex < PanMenu_getCurIndex(menu)){
                        return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)nextIndex);
                    //
                    } else {
                        PanItem * nextItem = PanMenu_itemAt(menu, nextIndex);
                        if (nextItem != NULL && Win_begy(PanItem_getWin(PanMenu_getCurItem(menu))) <
                                                Win_begy(PanItem_getWin(nextItem))){
                            return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)nextIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case L'A':
                case L'a':
                case L'Ф':
                case L'ф':
                case KEY_LEFT:{
                    signed long prevIndex = PanMenu_getPrevSelectableIndex(menu);
                    if (prevIndex == -1L) return false;
                    if (prevIndex > PanMenu_getCurIndex(menu)){
                        return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)prevIndex);
                    //
                    } else {
                        PanItem * prevItem = PanMenu_itemAt(menu, prevIndex);
                        if (prevItem != NULL && Win_begx(PanItem_getWin(PanMenu_getCurItem(menu))) > Win_begx(PanItem_getWin(prevItem))){
                            return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)prevIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case L'D':
                case L'd':
                case L'В':
                case L'в':
                case KEY_RIGHT:{
                    signed long nextIndex = PanMenu_getNextSelectableIndex(menu);
                    if (nextIndex == -1L) return false;
                    if (nextIndex < PanMenu_getCurIndex(menu)){
                        return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)nextIndex);
                    //
                    } else {
                        PanItem * nextItem = PanMenu_itemAt(menu, nextIndex);
                        if (nextItem != NULL && Win_begx(PanItem_getWin(PanMenu_getCurItem(menu))) <
                                                Win_begx(PanItem_getWin(nextItem))){
                            return PanMenu_driver(menu, PANM_JUMP_TO | (unsigned int)nextIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case KEY_SPACE:
                case KEY_NEWLINE:{
                    return PanMenu_driver(menu, PANM_TOGGLE_ITEM | PanMenu_getCurIndex(menu));
                }
                case KEY_MOUSE:{
                    return PanMenu_driver(menu, PANM_MOUSE_EVENT);
                }
                case KEY_PPAGE:{
                    return PanMenu_driver(menu, PANM_FIRST_ITEM);
                }
                case KEY_NPAGE:{
                    return PanMenu_driver(menu, PANM_LAST_ITEM);
                }
            }
            return false;
        }
        case PBM_RESTART:{
            return PanMenu_driver(menu, PANM_RESTART);
        }
        default:{
            if ((option & PBM_SET_BUTTON_GREY) != 0){
                return PanMenu_driver(menu, PANM_SET_ITEM_GREY |
                            (unsigned int)(option & ~PANM_SET_ITEM_GREY));
            //
            }
            else if ((option & PBM_DESELECT_BUTTON) != 0){
                return PanMenu_driver(menu, PANM_DESELECT_ITEM |
                    (unsigned int)(option & ~PANM_DESELECT_ITEM));
            //
            }
            else if ((option & PBM_SELECT_BUTTON) != 0){
                return PanMenu_driver(menu, PANM_SELECT_ITEM |
                    (unsigned int)(option & ~BM_SELECT_BUTTON));
            //
            }
            else if ((option & PBM_TOGGLE_BUTTON) != 0){
                return PanMenu_driver(menu, PANM_TOGGLE_ITEM |
                    (unsigned int)(option & ~PANM_TOGGLE_ITEM));
            //
            }
            else if ((option & PBM_REFRESH_BUTTON) != 0){
                return PanMenu_driver(menu, PANM_REFRESH_ITEM |
                    (unsigned int)(option & ~PANM_REFRESH_ITEM));
            //
            }
            else if ((option & PBM_JUMP_TO_BUTTON) != 0){
                return PanMenu_driver(menu, PANM_JUMP_TO |
                    (unsigned int)(option & ~PANM_JUMP_TO));
            }
            else assert(0 && "ITERNAL ERROR: UNHANDLED MENU OPTION RECIEVED.");
        }
    }
    return 0;
}


WINDOW * PBMenu_getButtonWin(PBMenu * self, size_t i){
    Debug_exists(self);
    Debug_maxbound(i, List_size(self->buttons) - 1);
    //
    return PanButton_getTextWin(List_at(self->buttons, i));

}


bool PBMenu_mdriver(PBMenu * bmenu, unsigned long option, wint_t input){
    Debug_exists(bmenu);
    //
    PanMenu * menu = bmenu->menu;
    //
    switch(option) {
        case PBM_REACT: {
            if (input != KEY_MOUSE)
                return false;
            PanMenu_driver(menu, PANM_MOUSE_EVENT);
            switch (PanMenu_getState(menu)){
                case PanMenuSt_TOGGLED:
                case PanMenuSt_ADV_AND_TOGGLED:
                case PanMenuSt_ADVANCED_SELECTOR:{
                    return true;
                }
                case PanMenuSt_IDLED:{
                    return false;
                }
                default: PanMenu_driver(menu, PANM_DISCARD_SELECTOR);
            }
            return true;
        }
        case PBM_RESTART: {
            PanMenu_driver(menu, PANM_RESTART);
            return PanMenu_driver(menu, PANM_DISCARD_SELECTOR);
        }
    }
    return 0;
}

signed long PBMenu_getEnclosedButtonIndex(PBMenu * self, wint_t signal){
    MEVENT event;
    if (getmouse(&event) == ERR) return -1L;

    return PanMenu_getEnclosedItemIndex(self->menu, loc(event.y, event.x));
}


bool PBMenu_isButtonSelected(PBMenu * self, unsigned int index){
    Debug_exists(self);
    Debug_maxbound(index, PanMenu_getItemsCount(self->menu) - 1);
    //
    return PanItem_getValue(PanMenu_itemAt(self->menu, index));
}




PanButton_Hook PBMenu_getHookAt(PBMenu * self, unsigned int index){
    Debug_exists(self);
    Debug_maxbound(index, PanMenu_getItemsCount(self->menu) - 1);
    //
    return PanItem_getHook(PanMenu_itemAt(self->menu, index));
}

void *       PBMenu_getPtrAt(PBMenu * self, unsigned int index){
    Debug_exists(self);
    Debug_maxbound(index, PanMenu_getItemsCount(self->menu) - 1);
    //
    return PanItem_getPtr(PanMenu_itemAt(self->menu, index));
}

PanButton_Hook PBMenu_setHookAt(PBMenu * self, unsigned int index, PanButton_Hook hook){
    Debug_exists(self);
    Debug_maxbound(index, PanMenu_getItemsCount(self->menu) - 1);
    //
    return PanItem_setHook(PanMenu_itemAt(self->menu, index), hook);
}

void * PBMenu_setPtrAt(PBMenu * self, unsigned int index, void * ptr){
    Debug_exists(self);
    Debug_maxbound(index, PanMenu_getItemsCount(self->menu) - 1);
    //
    return PanItem_setPtr(PanMenu_itemAt(self->menu, index), ptr);
}


bool PBMenu_getCyclic(PBMenu * self){
    Debug_exists(self);
    //
    return PanMenu_getCyclic(self->menu);
}

bool PBMenu_setCyclic(PBMenu * self, bool isCyclic){
    Debug_exists(self);
    //
    return PanMenu_setCyclic(self->menu, isCyclic);
}
    



PBMenu * PBMenu_strRenew(PBMenu * self, List * options){
    Debug_exists(self);
    Debug_exists(options);
    Debug_suppose(!List_isEmpty(self->buttons), "No buttons in menu");
    //
    //
    PBMenuAttrs attrs = (PBMenuAttrs){
        .attrs = PanMenu_getAttrs(self->menu),
        .frame = PanButton_getFrame(List_at(self->buttons, 0))
    };
    BLayout layout;
    if (List_size(self->buttons) == 1){
        layout = blayout(VERTICAL_PBM, 0);
    } else {
        Location loc0 = PanButton_getLocation(List_at(self->buttons, 0));
        Location loc1 = PanButton_getLocation(List_at(self->buttons, 1));
        Proportions prop = PanButton_getProps(List_at(self->buttons, 0));
        if (loc0.y != loc1.y)
            layout = blayout(VERTICAL_PBM, loc1.y - prop.height - loc0.y);
        else 
            layout = blayout(HORIZONTAL_PBM, loc1.x - prop.width - loc0.x);
    }

    PBMenu * newbie = PBMenu_strNew(
        options,
        &attrs,
        PanButton_getProps(List_at(self->buttons, 0)),
        PanButton_getLocation(List_at(self->buttons, 0)),
        layout
    );
    PBMenu_freeWhole(self);
    return newbie;
}

signed long PBMenu_getSelectedButton(PBMenu * self){
    Debug_exists(self);
    //
    for (unsigned int i = 0; i < PanMenu_getItemsCount(self->menu); ++i){
        if (PanItem_getValue(PanMenu_itemAt(self->menu, i)))
            return i;
    }
    return -1;
}

unsigned int PBMenu_getCurIndex(PBMenu * self){
    Debug_exists(self);
    //
    return PanMenu_getCurIndex(self->menu);
}

PBMenuSt PBMenu_getState(PBMenu * self){
    Debug_exists(self);
    //
    return (PBMenuSt)PanMenu_getState(self->menu);
}

void PBMenu_rewriteButton(PBMenu * self, size_t i, wchar_t * newText){
    Debug_exists(self);
    Debug_exists(newText);
    Debug_maxbound(i, List_size(self->buttons) - 1);
    //
    Win_vanish(PBMenu_getButtonWin(self, i), ((WINDOW *)PBMenu_getButtonWin(self, i))->_attrs);
    waddwstr(PBMenu_getButtonWin(self, i), newText);
    top_panel(PanButton_getPanel(List_at(self->buttons, i)));
}

