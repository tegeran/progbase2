#include <data_cloud.h>
#include <mem.h>
#include <error.h>


DataCloud * DataCloud_new(double capacity, double tariff){
    DataCloud * newbie = Mem_malloc(sizeof(DataCloud));
    newbie->capacity = capacity;
    newbie->tariff = tariff;
    return newbie;
}

void DataCloud_free(DataCloud * self){
    Debug_exists(self);
    //
    free(self);
}

double DataCloud_capacityAt(DataCloud * self){
    Debug_exists(self);
    //
    return self->capacity;
}

double DataCloud_tariffAt(DataCloud * self){
    Debug_exists(self);
    //
    return self->tariff;
}

double DataCloud_setCapacity(DataCloud * self, double capacity){
    Debug_exists(self);
    //
    double prev = self->capacity;
    self->capacity = capacity;
    return prev;
}

double DataCloud_setTariff(DataCloud * self, double tariff){
    Debug_exists(self);
    //
    double prev = self->tariff;
    self->tariff = tariff;
    return prev;
}

bool DataCloud_isValidCapacity(double capacity){
    return capacity >= 0;
}

bool DataCloud_isValidTariff(double tariff){
    return tariff >= 0;
}