#include <csv.h>
#include <mem.h>
#include <error.h>
#include <list_ext.h>
#include <sstring.h>

// List contains String objects
typedef struct __CsvRow{
    List * strs;
} CsvRow;

// List contains CsvRow objects
typedef struct __CsvTable{
    List * rows;
} CsvTable;

CsvRow * CsvRow_new(void){
    CsvRow * newbie = Mem_malloc(sizeof(CsvRow));
    newbie->strs = List_new();
    return newbie;
}


void CsvRow_free(CsvRow * self){
    Debug_exists(self);
    //
    while (!List_isEmpty(self->strs)){
        List_removeAt(self->strs, 0);
    }
    List_free(self->strs);
    free(self);
}

void CsvRow_freeWhole(CsvRow * self){
    Debug_exists(self);
    //
    List_freeWhole(self->strs, (Destructor)String_free);
    free(self);
}


void CsvRow_getValues(CsvRow * self, List * values){
    Debug_exists(self); 
    Debug_exists(values);
    //
    List_listAppend(values, self->strs);
}

CsvTable * CsvTable_new(void){
    CsvTable * newbie = Mem_malloc(sizeof(CsvTable));
    newbie->rows = List_new();
    return newbie;
}

void CsvTable_free(CsvTable * self){
    Debug_exists(self);
    //
    List_freeWhole(self->rows, (Destructor)CsvRow_free);
    free(self);
}


void CsvTable_freeWhole(CsvTable * self){
    Debug_exists(self);
    //
    List_freeWhole(self->rows, (Destructor)CsvRow_freeWhole);
    free(self);
}


void CsvTable_add(CsvTable * self, CsvRow * row){
    Debug_exists(self);
    Debug_exists(row);
    //
    List_addLast(self->rows, row);
}
void CsvTable_getRows(CsvTable * self, List * rows){
    Debug_exists(self);
    Debug_exists(rows);
    //
    List_listAppend(rows, self->rows);
}


signed long long  CsvRow_getFromString(String * self, CsvRow * row, size_t start){
    Debug_exists(self);
    Debug_exists(row);
    Debug_maxbound(start, String_length(self) - 1);
    //
    signed long long i = start;
    while (i < String_length(self)){
        String * string = String_new();
        i = String_appendCsvDeformated(string, self, i);
        if (i == CSV_DEFORMAT_ERROR || i == CSV_DEFORMAT_UTERMINATED_STRING){
            String_free(string); 
            CsvRow_freeWhole(row);
            return -1L;
        }
        CsvRow_add(row, string);
        if (i < String_length(self) && String_at(self, i) == '\n') 
                return i;
        ++i;
    }
    return i;
}


void CsvRow_add(CsvRow * self, String * value){
    Debug_exists(self);
    Debug_exists(value); 
    //
    List_addLast(self->strs, value);
}

String * CsvRow_toNewString(CsvRow * self){
    Debug_exists(self);
    //
    String * newbie = String_new();
    if (List_isEmpty(self->strs)) return newbie;
    for (size_t i = 0; i < List_size(self->strs) - 1; ++i){
        String_appendCsvFromated(newbie, List_at(self->strs, i));
        String_appendChar(newbie, ',');
    }
    String_appendCsvFromated(newbie, List_at(self->strs, List_size(self->strs) - 1));
    return newbie;
}



CsvTable * CsvTable_newFromString(String * csvString){
    CsvTable * newbie = CsvTable_new();
    signed long long i = 0;
    while (i < String_length(csvString)){
        CsvRow * row = CsvRow_new();
        i = CsvRow_getFromString(csvString, row, i);
        if (i == -1L){
            CsvTable_freeWhole(newbie);
            return NULL;
        }
        CsvTable_add(newbie, row);
        ++i;
    }
    return newbie;
}

String * CsvTable_toNewString(CsvTable * self){
    String * newbie = String_new();
    if (List_isEmpty(self->rows)) return newbie;
    //

    int (*sumPtr)(int, int) = NULL;

    for (size_t i = 0; i < List_size(self->rows) - 1; ++i){
        String * str = CsvRow_toNewString(List_at(self->rows, i));
        String_concatenate(newbie, str);
        String_free(str);
        String_appendChar(newbie, '\n');
    }
    String * str = CsvRow_toNewString(List_at(self->rows, List_size(self->rows) - 1));
    String_concatenate(newbie, str);
    String_free(str);
    return newbie;
}