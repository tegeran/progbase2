#include <textart.h>
#include <windows.h>
#include <key.h>
#include <ncursesw/curses.h>
#include <error.h>

attr_t ArtSetup_getAttr(wchar_t token, const ArtSetup * const setup){
    unsigned int i = 0;
    while (i < ART_MAX_ATTRS && token != setup->wchar[i]){
        ++i;
    //   
    }
    if(i == ART_MAX_ATTRS) return stdscr->_attrs;
    //
    return setup->attr[i];
}

void ArtSetup_printExpilicit(WINDOW * win, const ArtSetup * const setup, const wchar_t * art){
    Debug_exists(setup); 
    Debug_exists(win);   
    Debug_exists(art);   
    //
    wchar_t prev = L'\0';
    for (int i = 0; art[i] != L'\0' ; ++i){
        if (prev != art[i] && art[i] != '\n'){
            wattrset(win, ArtSetup_getAttr(art[i], setup));
            prev = art[i];
        }
        wadd_wch(win, &cchar(art[i]));
    }
}

void ArtSetup_printImpilicit(WINDOW * win, const ArtSetup * const setup, const wchar_t * art){
    Debug_exists(setup);   
    Debug_exists(win);     
    Debug_exists(art); 
    //
    wchar_t prev = L'\0';
    immedok(win, true);
    for (int i = 0; art[i] != L'\0' ; ++i){
        if (prev != art[i] && art[i] != '\n'){
            wattrset(win, ArtSetup_getAttr(art[i], setup));
            prev = art[i];
        }
        waddch(win, ' ' | WA_REVERSE);
    }
}




