#include <bmenu.h>
#include <wmenu.h>
#include <wbutton.h>
#include <error.h>
#include <witem_ext.h>
#include <key.h>
#include <windows.h>
#include <mem.h>



typedef struct __BMenu{
              List * buttons;
             WMenu * menu;
} BMenu;


BMenu * BMenu_new (wchar_t ** buttonLabels,
              unsigned int    size, 
                BMenuAttrs    attrs,
               Proportions    props,
                  Location    location,
                   BLayout    layout){
    //
    Debug_exists(buttonLabels);
    Debug_exists(*buttonLabels);
    //
    BMenu * newbie = Mem_malloc(sizeof(BMenu));
    newbie->buttons = List_new();
    WINDOW * wins[size];
    if (layout.orientation == VERTICAL_BM){
        for (int i = 0; i < size; ++i){
            List_addLast(newbie->buttons, WButton_newStd(buttonLabels[i], props, location));
            location.y += props.height + layout.spacing;
            wins[i] = wgetparent(WButton_getTextWin(List_at(newbie->buttons, i)));
        }
    } else {
        for (int i = 0; i < size; ++i){
            List_addLast(newbie->buttons, WButton_newStd(buttonLabels[i], props, location));
            location.x += props.width + layout.spacing;
            wins[i] = wgetparent(WButton_getTextWin(List_at(newbie->buttons, i)));
        }
    }
    newbie->menu = WMenu_new(attrs, WItem_newArr(wins, size, false), size, 0, true);
    return newbie;
}

void BMenu_freeWhole(BMenu * self){
    Debug_exists(self);
    //
    for (size_t i = 0; i < WMenu_getItemsCount(self->menu); ++i){
        delwin(WItem_getWin(WMenu_itemAt(self->menu, i)));
    }
    WItem_freeArr(WMenu_getItems(self->menu), WMenu_getItemsCount(self->menu));
    WMenu_free(self->menu);
    List_freeWhole(self->buttons, (Destructor)WButton_free);
    free(self);
}

void BMenu_free(BMenu * self){
    Debug_exists(self);
    //
    WItem_freeArr(WMenu_getItems(self->menu), BMenu_getButtonsCount(self));
    WMenu_free(self->menu);
    free(self);
}

BMenu * BMenu_newBy(List * buttons, unsigned int size, BMenuAttrs attrs){
        //
        Debug_exists(buttons);
        //
        BMenu * newbie = Mem_malloc(sizeof(BMenu));
        newbie->buttons = buttons;
        WINDOW * wins[size];
        for (int i = 0; i < size; ++i){
            if (wgetparent(WButton_getTextWin(List_at(buttons, i))) == NULL)
                wins[i] = WButton_getTextWin(List_at(buttons, i));
            else
                wins[i] = wgetparent(WButton_getTextWin(List_at(buttons, i)));
        }
        newbie->menu = WMenu_new(attrs, WItem_newArr(wins, size, false), size, 0, true);
        return newbie;
}

void BMenu_post(BMenu * self){
    Debug_exists(self);
    //
    WMenu_post(self->menu);
}

void BMenu_unpost(BMenu * self, attr_t remainder){
    Debug_exists(self);
    //
    WMenu_unpost(self->menu, remainder);
}


unsigned int BMenu_getButtonsCount(BMenu * self){
    Debug_exists(self);
    //
    return List_size(self->buttons);
}

bool BMenu_driver(BMenu * bmenu, unsigned long option, wint_t input){
    Debug_exists(bmenu);
    //
    WMenu * menu = bmenu->menu;
    //
    switch(option){
        case BM_REACT:{
            switch (input){
                case L'W':
                case L'w':
                case L'Ц':
                case L'ц':
                case KEY_UP:{
                    signed long prevIndex = WMenu_getPrevSelectableIndex(menu);
                    if (prevIndex > WMenu_getCurIndex(menu)){
                        return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)prevIndex);
                    //
                    } else if (prevIndex != -1) {
                        WItem * prevItem = WMenu_itemAt(menu, prevIndex);
                        if (prevItem != NULL && Win_begy(WItem_getWin(WMenu_getCurItem(menu))) > 
                                                Win_begy(WItem_getWin(prevItem))){
                            return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)prevIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case L'S':
                case L's':
                case L'Ы':
                case L'ы':
                case L'і':
                case L'І':
                case KEY_DOWN:{
                    signed long nextIndex = WMenu_getNextSelectableIndex(menu);
                    if (nextIndex < WMenu_getCurIndex(menu)){
                        return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)nextIndex);
                    //
                    } else if (nextIndex != -1) {
                        WItem * nextItem = WMenu_itemAt(menu, nextIndex);
                        if (nextItem != NULL && Win_begy(WItem_getWin(WMenu_getCurItem(menu))) <
                                                Win_begy(WItem_getWin(nextItem))){
                            return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)nextIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case L'A':
                case L'a':
                case L'Ф':
                case L'ф':
                case KEY_LEFT:{
                    signed long prevIndex = WMenu_getPrevSelectableIndex(menu);
                    if (prevIndex > WMenu_getCurIndex(menu)){
                        return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)prevIndex);
                    //
                    } else if (prevIndex != -1) {
                        WItem * prevItem = WMenu_itemAt(menu, prevIndex);
                        if (prevItem != NULL && Win_begx(WItem_getWin(WMenu_getCurItem(menu))) > Win_begx(WItem_getWin(prevItem))){
                            return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)prevIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case L'D':
                case L'd':
                case L'В':
                case L'в':
                case KEY_RIGHT:{
                    signed long nextIndex = WMenu_getNextSelectableIndex(menu);
                    if (nextIndex < WMenu_getCurIndex(menu)){
                        return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)nextIndex);
                    //
                    } else if (nextIndex != -1) {
                        WItem * nextItem = WMenu_itemAt(menu, nextIndex);
                        if (nextItem != NULL && Win_begx(WItem_getWin(WMenu_getCurItem(menu))) < 
                                                Win_begx(WItem_getWin(nextItem))){
                            return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)nextIndex);
                        //
                        }
                    //
                    }
                    break;
                }
                case KEY_SPACE:
                case KEY_NEWLINE:{
                    return WMenu_driver(menu, WM_TOGGLE_ITEM | WMenu_getCurIndex(menu));
                }
                case KEY_MOUSE:{
                    return WMenu_driver(menu, WM_MOUSE_EVENT);
                }
                case KEY_PPAGE:{
                    return WMenu_driver(menu, WM_FIRST_ITEM);
                }
                case KEY_NPAGE:{
                    return WMenu_driver(menu, WM_LAST_ITEM);
                }
            }
            return false;
        }
        case BM_RESTART:{
            return WMenu_driver(menu, WM_RESTART);
        }
        default:{
            if ((option & BM_SET_BUTTON_GREY) != 0){
                return WMenu_driver(menu, WM_SET_ITEM_GREY | 
                            (unsigned int)(option & ~BM_SET_BUTTON_GREY));
            //    
            }
            else if ((option & BM_DESELECT_BUTTON) != 0){
                return WMenu_driver(menu, WM_DESELECT_ITEM | 
                    (unsigned int)(option & ~BM_DESELECT_BUTTON));
            //
            }
            else if ((option & BM_SELECT_BUTTON) != 0){
                return WMenu_driver(menu, WM_SELECT_ITEM | 
                    (unsigned int)(option & ~BM_SELECT_BUTTON));
            //
            }
            else if ((option & BM_TOGGLE_BUTTON) != 0){
                return WMenu_driver(menu, WM_TOGGLE_ITEM | 
                    (unsigned int)(option & ~BM_TOGGLE_BUTTON));
            //
            }
            else if ((option & BM_REFRESH_BUTTON) != 0){
                return WMenu_driver(menu, WM_REFRESH_ITEM | 
                    (unsigned int)(option & ~BM_REFRESH_BUTTON));
            //
            }
            else if ((option & BM_JUMP_TO_BUTTON) != 0){
                return WMenu_driver(menu, WM_JUMP_TO | 
                    (unsigned int)(option & ~BM_JUMP_TO_BUTTON));
            }
            else assert(0 && "ITERNAL ERROR: UNHANDLED MENU OPTION RECIEVED."); 
        }
    }
    return 0;
}   

signed long BMenu_getEnclosedButtonIndex(BMenu * self, wint_t signal){
    MEVENT event;
    if (getmouse(&event) == ERR) return -1L;

    return WMenu_getEnclosedItemIndex(self->menu, loc(event.y, event.x));
}


bool BMenu_isButtonSelected(BMenu * self, unsigned int index){
    Debug_exists(self);
    Debug_maxbound(index, WMenu_getItemsCount(self->menu) - 1);
    //
    return WItem_getValue(WMenu_itemAt(self->menu, index));
}