#include <witem_ext.h>
#include <error.h>




WItem ** WItem_newArr(WINDOW ** wins, const unsigned int size, bool winsAreImmutable){
    Debug_exists(wins);
    //
    WItem ** newbie = malloc(size * sizeof(WItem *)); 
    for (int i = 0; i < size; ++i){
        assert(wins[i] != NULL);
        //
        newbie[i] = WItem_newStd(wins[i], winsAreImmutable);
    }
    return newbie;
}

void WItem_freeArr(WItem ** self, const unsigned int size){
    Debug_exists(self);
    //
    for (int i = 0; i < size; ++i){
        assert(self[i] != NULL);
        //
        WItem_free(self[i]);
    }
    free(self);
}