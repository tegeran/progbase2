#include <dispatcher.h>

#include <dir_driver.h>
#include <provider.h>
#include <dynamic_array.h>
#include <frame.h>
#include <ctype.h>
#include <fsm.h>
#include <integer.h>
#include <math.h>


#define mainlist ((List *)dispatcher->data)

static void MenuList_tools      (EventHandler * dispatcher, Event * event);
static void MenuList_provButtons(EventHandler * dispatcher, Event * event);
static void MenuList_dataPanel  (EventHandler * dispatcher, Event * event);
static void MenuList_provEditor (EventHandler * dispatcher, Event * event);
static void MenuList_cloudEditor(EventHandler * dispatcher, Event * event);
static void MenuList_taskHandler(EventHandler * dispatcher, Event * event);
static void MenuList_saveHandler(EventHandler * dispatcher, Event * event);


enum DataPanelSignal{
    DataPan_NAME = 0,
    DataPan_TARIFF,
    DataPan_LOSS,
    DataPan_USERS,
    DataPan_CLOUD
};

typedef struct ProviderEdit{
    Provider * provider;
    enum DataPanelSignal   field;
    WINDOW * editWin;
} ProviderEdit;

static ProviderEdit * ProviderEdit_new(Provider * provider, enum DataPanelSignal field, WINDOW * editWin);

static enum DataPanelSignal ProviderEdit_getField(ProviderEdit * self);
static Provider * ProviderEdit_getProvider       (ProviderEdit * self);
static void ProviderEdit_free                    (ProviderEdit * self);
static WINDOW * ProviderEdit_getWindow(ProviderEdit * self);

typedef enum __MenuListSt{
    MenuListSt_Idling,
    MenuListSt_StartReturning,
    MenuListSt_Returning,
    MenuListSt_StopReturning,

    MenuListSt_EditingProvider,
    MenuListSt_EditingCloud,
    MenuListSt_ChoosingTask,
    MenuListSt_Saving
} MenuListSt;



static MenuListSt MenuList_getNextState(MenuListSt currentState, Event * event);


static void getHighestTariffProviders(List *providers, unsigned int *res, const int K);

void MenuList_dispatcher(EventHandler * self, Event * event){
    static MenuListSt state = MenuListSt_Idling;
    switch(event->type){
        case Goto_MenuList:{
            MenuList_provButtons(self, event);
            MenuList_tools(self, event);
            MenuList_dataPanel(self, event);
            doupdate();
            return;
        }
        case MenuList_UpdatedProvider:{
            state = MenuListSt_Idling;
        }
        case MenuList_DeleteProvider:
        case MenuList_InsertBefore:
        case MenuList_InsertAfter:{
            MenuList_provButtons(self, event);
            MenuList_dataPanel(self, event);
            doupdate();
            return;
        }
        case MenuList_Save:{
            MenuList_saveHandler(self,event);
            state = MenuListSt_Saving;
            doupdate();
            return;
        }
        case MenuList_Saved:{
            state = MenuListSt_Idling;
            return;
        }
        case MenuList_DoTask:{
            if (!List_size(self->data))
                return;
            MenuList_taskHandler(self, event);
            state = MenuListSt_ChoosingTask;
            doupdate();
            return;
        }
        case MenuList_ChosenTask:{
            state = MenuListSt_Idling;
            MenuList_provButtons(self, event);
            doupdate();
            return;
        }
        case MenuList_ChangedProvider:{
            MenuList_dataPanel(self, event);
            doupdate();
            return;
        }
        case MenuList_EditProvider:{
            switch(ProviderEdit_getField(event->data)) {
                case DataPan_CLOUD:{
                    MenuList_cloudEditor(self, event);
                    state = MenuListSt_EditingCloud;
                    break;
                }
                default:{
                    MenuList_provEditor(self, event);
                    state = MenuListSt_EditingProvider;
                    break;
                }
            }
            doupdate();
            return;
        }
        case UserSignal:{
            switch ((state = MenuList_getNextState(state, event))){
                case MenuListSt_EditingProvider:{
                    MenuList_provEditor(self, event);
                    break;
                }
                case MenuListSt_EditingCloud:{
                    MenuList_cloudEditor(self, event);
                    break;
                }
                case MenuListSt_ChoosingTask:{
                    MenuList_taskHandler(self, event);
                    break;
                }
                case MenuListSt_Saving:{
                    MenuList_saveHandler(self, event);
                    break;
                }
                case MenuListSt_Idling:{
                    MenuList_tools(self, event);
                    MenuList_dataPanel(self, event);
                    MenuList_provButtons(self, event);
                    break;
                }
                case MenuListSt_StartReturning:{
                    Dispatcher_initDialog(L"Are you really want to go back to main menu?");
                    break;
                }
                case MenuListSt_Returning:{
                    switch (Dispatcher_reactDialog(*(wint_t *)event->data)){
                        case CHOOSE_OK: {
                            Dispatcher_freeDialog();
                            state = MenuListSt_Idling;
                            emit(Goto_MainMenu);
                            break;
                        }
                        case CHOOSE_CANCEL: {
                            state = MenuListSt_Idling;
                            Dispatcher_freeDialog();
                            break;
                        }
                        case CHOOSE_IDLE: {
                            return;
                        }
                        default: break;
                    }
                    break;
                }
                case MenuListSt_StopReturning:{
                    Dispatcher_freeDialog();
                    break;
                }
            }
            doupdate();
            return;
        }
        case Goto_MainMenu: EventSystem_removeHandler(self);
        case ExitEventTypeId:{
            MenuList_provButtons(self, event);
            MenuList_tools(self, event);
            MenuList_dataPanel(self, event);
            EventSystem_addHandler(EventHandler_new(NULL, NULL, MainMenu_dispatcher));
            return;
        }
    }
}

static MenuListSt MenuList_getNextState(MenuListSt currentState, Event * event){
    Debug_exists(event);
    //
    switch (currentState){
        case MenuListSt_Saving:
        case MenuListSt_ChoosingTask:
        case MenuListSt_EditingCloud:
        case MenuListSt_EditingProvider:{
            return currentState;
        }
        case MenuListSt_Idling:
        case MenuListSt_StopReturning:{
            switch (*(wint_t *)event->data){
                case KEY_ESCAPE: return MenuListSt_StartReturning;
                default: return MenuListSt_Idling;
            }
        }
        case MenuListSt_StartReturning:
        case MenuListSt_Returning:{
            switch (*(wint_t *)event->data){
                case KEY_ESCAPE: return MenuListSt_StopReturning;
                default: return MenuListSt_Returning;
            }
        }
    }
    return MenuListSt_Idling;
}

static List * ProviderList_toNewNames(List * providers){
    List * names = List_new();
    for (size_t i = 0; i < List_size(providers); ++i) {
        List_addLast(names, Provider_nameAt(List_at(providers, i)));
    }
    return names;
}

static void refreshProvButtons(List * providers, PBMenu ** menu){
    if (!List_size(providers)){
        Debug_exists(*menu);
        //
        PBMenu_unpost(*menu);
        PBMenu_freeWhole(*menu);
        *menu = NULL;
        return;
    }
    List * names = ProviderList_toNewNames(providers);
    if (!*menu){
        *menu = PBMenu_strNew(names, &attrs, props(3, 25), loc(1, 1), VLAYOUT);
        PBMenu_post(*menu);
        List_free(names);
        return;
    }
    unsigned int prevIndex = PBMenu_getCurIndex(*menu);
    *menu = PBMenu_strRenew(*menu, names);
    PBMenu_post(*menu);
    if (prevIndex < PBMenu_getButtonsCount(*menu)) {
        PBMenu_driver(*menu, PBM_JUMP_TO_BUTTON | prevIndex, '\0');
    }
    List_free(names);
}


static void ProvButtons_insertNewProvider(List * providers, unsigned int i){
    Debug_exists(providers);
    Debug_maxbound(i, List_size(providers));
    //
    Provider * newbie = Provider_newWhole(String_toNewString("New Provider"), NULL, 0, 0, 0);
    List_insert(providers, i, newbie);
    String_appendFormat(Provider_nameAt(newbie), " %i", i);
}

static void MenuList_provButtons(EventHandler * dispatcher, Event * event){
    static PBMenu * menu = NULL;
    switch(event->type){
        case Goto_MenuList:{
            if (!List_size(dispatcher->data))
                return;

            refreshProvButtons(dispatcher->data, &menu);
            return;
        }
        case MenuList_UpdatedProvider:{
            refreshProvButtons(dispatcher->data, &menu);
            return;
        }
        case MenuList_InsertBefore:{
            if (!menu){
                ProvButtons_insertNewProvider(dispatcher->data, 0);
                emitData(MenuList_ChangedProvider, List_atFirst(dispatcher->data));
            } else {
                ProvButtons_insertNewProvider(dispatcher->data, PBMenu_getCurIndex(menu));
            }
            refreshProvButtons(dispatcher->data, &menu);
            return;
        }
        case MenuList_InsertAfter: {
            if (!menu){
                ProvButtons_insertNewProvider(dispatcher->data, 0);
                emitData(MenuList_ChangedProvider, List_atFirst(dispatcher->data));
            } else {
                ProvButtons_insertNewProvider(dispatcher->data, PBMenu_getCurIndex(menu) + 1);
            }
            refreshProvButtons(dispatcher->data, &menu);
            return;
        }
        case MenuList_DeleteProvider:{
            if (!List_size(dispatcher->data))
                return;
            for (unsigned int i = 0, j = 0; i < PBMenu_getButtonsCount(menu); ++i) {
                if (PBMenu_isButtonSelected(menu, i) || i == PBMenu_getCurIndex(menu)) {
                    Provider_freeWhole(List_removeAt(dispatcher->data, i - j++));
                }
            }
            refreshProvButtons(dispatcher->data, &menu);
            emitData(MenuList_ChangedProvider, menu
                    ? List_at(dispatcher->data, PBMenu_getCurIndex(menu))
                    : NULL);
            return;
        }
        case UserSignal:{
            if (!menu) return;
            if (PBMenu_driver(menu, PBM_REACT, *(wint_t *)event->data)){
                switch (PBMenu_getState(menu)){
                    case PBMenuSt_ADV_AND_TOGGLED:
                    case PBMenuSt_ADVANCED_SELECTOR:{
                        emitData(MenuList_ChangedProvider,
                                 List_at(dispatcher->data, PBMenu_getCurIndex(menu)));
                        return;
                    }
                    default:return;
                }
            }
            return;
        }
        case MenuList_ChosenTask:{
            Debug_exists(menu);
            unsigned int highest[Integer_at(event->data)];
            getHighestTariffProviders(dispatcher->data, highest, Integer_at(event->data));
            PBMenu_driver(menu, PBM_RESTART, '\0');
            for (unsigned int i = 0; i < Integer_at(event->data); ++i){
                PBMenu_driver(menu, PBM_SELECT_BUTTON | highest[i], '\0');
            }
            return;
        }
        case Goto_MainMenu:{
            if (menu) {
                PBMenu_unpost(menu);
                PBMenu_freeWhole(menu);
                menu = NULL;
            }
            return;
        }
    }
}


#define EPSILON 1e-6



typedef struct __Index{
    unsigned int i;
    Provider * val;
} Index;

static Index * Index_new(unsigned int i, void * val){
    Index * newbie = Mem_malloc(sizeof(Index));
    newbie->i = i;
    newbie->val = val;
    return newbie;
}

signed char compareProvidersTariff(Index * left, Index * right){
    Debug_exists(left);
    Debug_exists(right);
    //
    if (fabs(left->val->tariff - right->val->tariff) < EPSILON) return 0;
    return left->val->tariff > right->val->tariff
           ? (signed char)-1
           : (signed char)1;
}

static void Index_free(Index * self){
    Debug_exists(self);
    //
    free(self);
}

static void getHighestTariffProviders(List *providers, unsigned int *res, int K) {
    Debug_maxbound(K, List_size(providers));
    List * temp = List_new();
    for (unsigned int i = 0; i < List_size(providers); ++i){
        List_addToSorted(temp, Index_new(i, List_at(providers, i)), (Comparator)compareProvidersTariff);
    }
    while (K){
        Index * ind = List_removeLast(temp);
        *res++ = ind->i;
        --K;
        Index_free(ind);
    }
    List_freeWhole(temp, (Destructor)Index_free);
}

void MenuList_tools(EventHandler * dispatcher, Event * event) {
    static PBMenu * menu = NULL;
    switch (event->type){
        case Goto_MenuList:{
            menu = _PBMenu_new(
                    &attrs,
                    props(3, 7),
                    loc(1, 30),
                    HLAYOUT,
                    L"  ⩱", L"  ⩲", L"  " wstr(UNI_LITTERBIN),
                    L"TASK" , L"SAVE"
            );
            PBMenu_mpost(menu);
            return;
        }
        case UserSignal:{
            if (!PBMenu_mdriver(menu, PBM_REACT, *(wint_t *)event->data)){
                return;
            }
            switch (PBMenu_getSelectedButton(menu)){
                case 0:{
                    PBMenu_driver(menu, PBM_DESELECT_BUTTON | 0, '\0');
                    emit(MenuList_InsertAfter);
                    return;
                }
                case 1:{
                    PBMenu_driver(menu, PBM_DESELECT_BUTTON | 1, '\0');
                    emit(MenuList_InsertBefore);
                    return;
                }
                case 2:{
                    PBMenu_driver(menu, PBM_DESELECT_BUTTON | 2, '\0');
                    emit(MenuList_DeleteProvider);
                    return;
                }
                case 3:{
                    PBMenu_driver(menu, PBM_DESELECT_BUTTON | 3, '\0');
                    emit(MenuList_DoTask);
                    return;
                }
                case 4:{
                    PBMenu_driver(menu, PBM_DESELECT_BUTTON | 4, '\0');
                    emit(MenuList_Save);
                    return;
                }
                default: return;
            }
        }
        case Goto_MainMenu:{
            PBMenu_unpost(menu);
            PBMenu_freeWhole(menu);
            menu = NULL;
            return;
        }
        default: return;
    }
}

static PBMenu * DataPanel_initMenu(){
    List * data = List_new();
    //
    List_addLast(data, PanButton_newStd(L"Name: ",        props( 3, 39), loc( 5, 30)));
    List_addLast(data, PanButton_newStd(L"Tariff: ",      props( 3, 39), loc( 8, 30)));
    List_addLast(data, PanButton_newStd(L"Packet loss: ", props( 3, 39), loc(11, 30)));
    List_addLast(data, PanButton_newStd(L"Users: ",       props( 3, 39), loc(14, 30)));
    List_addLast(data, PanButton_newStd(L"Cloud\n",       props( 6, 39), loc(17, 30)));
    //
    PBMenu * menu = PBMenu_newBy(data, List_size(data), &attrs);
    return menu;
}

static void DataPanel_cleanMenu(PBMenu * menu){
    PBMenu_rewriteButton(menu, DataPan_NAME,   L"Name: ");
    PBMenu_rewriteButton(menu, DataPan_TARIFF, L"Tariff: ");
    PBMenu_rewriteButton(menu, DataPan_LOSS,   L"Packet loss: ");
    PBMenu_rewriteButton(menu, DataPan_USERS,  L"Users: ");
    PBMenu_rewriteButton(menu, DataPan_CLOUD,  L"Cloud\n");
}


static void DataPanel_appendProvider(PBMenu * menu, Provider * provider){

    waddstr(PBMenu_getButtonWin(menu, DataPan_NAME),   String_readOnly(provider->name));
    wprintw(PBMenu_getButtonWin(menu, DataPan_TARIFF), "%lf", provider->tariff);
    wprintw(PBMenu_getButtonWin(menu, DataPan_LOSS),   "%lf", provider->loss);
    wprintw(PBMenu_getButtonWin(menu, DataPan_USERS),  "%ld", provider->users);

    if (provider->cloud) {
        waddwstr(PBMenu_getButtonWin(menu, DataPan_CLOUD), L"Capacity: ");
        wprintw (PBMenu_getButtonWin(menu, DataPan_CLOUD), "%lf", provider->cloud->capacity);
        waddwstr(PBMenu_getButtonWin(menu, DataPan_CLOUD), L"\nTariff  : ");
        wprintw (PBMenu_getButtonWin(menu, DataPan_CLOUD), "%lf", provider->cloud->tariff);
    }
}

static void MenuList_dataPanel(EventHandler * dispatcher, Event * event){
    static PBMenu * menu = NULL;
    static Provider * provider = NULL;
    switch (event->type){
        case Goto_MenuList:{
            menu = DataPanel_initMenu();
            if (List_size(dispatcher->data)){
                DataPanel_appendProvider(menu, (provider = List_atFirst(dispatcher->data)));
            }
            PBMenu_mpost(menu);
            update_panels();
            return;
        }
        case MenuList_UpdatedProvider:
        case MenuList_ChangedProvider:{

            //
            DataPanel_cleanMenu(menu);
            provider = event->data;
            if (event->data)
                DataPanel_appendProvider(menu, event->data);
//            PBMenu_mdriver(menu, PBM_REACT, KEY_MOUSE); todo visual bug
            update_panels();
            return;
        }
        case UserSignal:{
            Debug_exists(menu);
            //
            if (!PBMenu_mdriver(menu, PBM_REACT, *(wint_t *)event->data))
                return;
            signed long selected = PBMenu_getSelectedButton(menu);
            switch(selected){
                case -1:{
                    return;
                }
                default: {
                    PBMenu_mdriver(menu, PBM_RESTART, '\n');
                    doupdate();
                    if (!provider) return;
                    EventSystem_emit(
                            Event_new(
                                    NULL,
                                    MenuList_EditProvider,
                                    ProviderEdit_new(provider, (enum DataPanelSignal)selected,
                                    PBMenu_getButtonWin(menu, selected)),
                                    (Destructor)ProviderEdit_free
                            )
                    );
                    return;
                };
            }
        }
        case Goto_MainMenu:{
            Debug_exists(menu);
            //
            PBMenu_freeWhole(menu);
            menu = NULL;
            provider = NULL;
            return;
        }
        default: return;
    }
}


static bool isName(String * str){
    return !String_isEmpty(str);
}

static bool isTariff(String * str){
    const char * ch = String_readOnly(str);
    FSMRead_Float state = FSM_FLOAT_LAUNCH;
    while ((state = FSM_readFloat(state, *ch)) != FSM_FLOAT_SHUTDOWN
           && state != FSM_FLOAT_UNTERMINATED) {
        ++ch;
    }
    return *ch == '\0';
}

static bool isLoss(String * str){
     return isTariff(str) && atof(String_readOnly(str)) <= 100;
}
static bool isCapacity(String * str){
    return isTariff(str);
}

static bool isInteger(String * str){
    for (size_t i = 0; i < String_length(str); ++i){
        if (!isdigit(String_at(str, i)))
            return false;
    }
    return true;
}

typedef bool (*AnswerField)(String * str);

static void MenuList_provEditor(EventHandler * dispatcher, Event * event){
    static Provider * prov = NULL;
    static AnswerField isValid = NULL;
    static enum DataPanelSignal field = 0;
    static String * str = NULL;
    static Panel pan = {NULL, NULL};
    //
    switch (event->type){
        case MenuList_EditProvider:{
            field = ProviderEdit_getField(event->data);
            str = String_new();
            prov = ProviderEdit_getProvider(event->data);
            pan.win = ProviderEdit_getWindow(event->data);
            wclear(pan.win);
            wattron(pan.win, WA_REVERSE);
            Win_appendAttr(pan.win, WA_REVERSE);
            wmove(pan.win, 0, 0);
            pan.pan = new_panel(pan.win);
            switch (ProviderEdit_getField(event->data)){
                case DataPan_NAME:{
                    isValid = isName;
                    String_concatenate(str, prov->name);
                    waddwstr(pan.win, L"Name: ");
                    break;
                }
                case DataPan_LOSS:{
                    String_appendFormat(str, "%lf", prov->loss);
                    waddwstr(pan.win, L"Packet loss: ");
                    isValid = isLoss;
                    break;
                }
                case DataPan_TARIFF: {
                    String_appendFormat(str, "%lf", prov->loss);
                    waddwstr(pan.win, L"Tariff: ");
                    isValid = isTariff;
                    break;
                }
                case DataPan_USERS:{
                    String_appendFormat(str, "%lu", prov->users);
                    waddwstr(pan.win, L"Users: ");
                    isValid = isInteger;
                    break;
                }
				default:{Debug_shutdown("Invalid field for provEditor()");}
            }
            waddstr(pan.win, String_readOnly(str));
            Panel_updateAll();
            return;
        }
        case UserSignal:{
            wint_t input = *(wint_t *)event->data;
            switch(input){
                case '\n':{
                    if (isValid(str)) {
                        switch (field) {
                            case DataPan_NAME: {
                                String_free(prov->name);
                                prov->name = str;
                                break;
                            }
                            case DataPan_TARIFF: {
                                int ret = String_scanFormat(str, "%lf", &(prov->tariff));
                                Debug_suppose(ret, "Invalid data scaned");
                                String_free(str);
                                break;
                            }
                            case DataPan_LOSS: {
                                int ret = String_scanFormat(str, "%lf", &(prov->loss));
                                Debug_suppose(ret, "Invalid data scaned");
                                String_free(str);
                                break;
                            }
                            case DataPan_USERS: {
                                int ret = String_scanFormat(str, "%lu", &(prov->users));
                                Debug_suppose(ret, "Invalid data scaned");
                                String_free(str);
                                break;
                            }
                            default: {
                                Debug_shutdown("Invalid field for provEditor()");
                            }
                        }
                    } else String_free(str);
                    del_panel(pan.pan);
                    wattroff(pan.win, WA_REVERSE);
                    Panel_updateAll();
                    emitData(MenuList_UpdatedProvider, prov);
                    return;
                }
                case KEY_ESCAPE:{
                    String_free(str);
                    del_panel(pan.pan);
                    wattroff(pan.win, WA_REVERSE);
                    Panel_updateAll();
                    emitData(MenuList_UpdatedProvider, prov);
                    return;
                }
                case KEY_BACKSPACE:{
                    if (!String_length(str)){
                        return;
                    }
                    String_reduce(str, String_length(str) - 1);
                    Win_shift_vanishChar(pan.win, -1);
                    Panel_updateAll();
                    return;
                }
                default:{
                    if (input >= KEY_MIN
                        || (getcurx(pan.win) == getmaxx(pan.win) - 2
                            && getcury(pan.win) == getmaxy(pan.win) - 1)) {
                        return;
                    }
                    //
                    String_appendChar(str, input);
                    if (!isValid(str)){
                        String_reduce(str, String_length(str) - 1);
                    } else {
                        waddch(pan.win, input);
                        Panel_updateAll();
                    }
                    return;
                }
            }
        }
    }
}

enum DataCloudField{
    DatCl_CAPACITY,
    DatCl_TARIFF
};


static void moveVanishLine(WINDOW * win, Location * loc, DataCloud * cloud, String * str){
    wattroff(win, WA_REVERSE);
    wmove(win, getcury(win), 0);
    wchgat(win, -1, win->_attrs, PAIR_NUMBER(win->_attrs), NULL);
    wmove(win, loc->y, loc->x);
    wattron(win, WA_REVERSE);
    Win_vanishLine(win);
    if (loc->y == 1){
        waddstr(win, "Capacity: ");
        String_appendFormat(str, "%lf", cloud->capacity);
        waddstr(win, String_readOnly(str));
    } else {
        waddstr(win, "Tariff  : ");
        String_appendFormat(str, "%lf", cloud->tariff);
        waddstr(win, String_readOnly(str));
    }
}

static void MenuList_cloudEditor(EventHandler * dispatcher, Event * event){
    static Provider * prov = NULL;
    static AnswerField isValid = NULL;
    static enum DataCloudField field = DatCl_CAPACITY;
    static String * str = NULL;
    static Panel pan = {NULL, NULL};
    static DataCloud * dat = NULL;
    //
    switch (event->type){
        case MenuList_EditProvider:{
            prov = ProviderEdit_getProvider(event->data);
            dat = DataCloud_new(0, 0);
            if (prov->cloud){
                dat->tariff   = prov->cloud->tariff;
                dat->capacity = prov->cloud->capacity;
            }
            isValid = isCapacity;
            str = String_new();
            pan.win = ProviderEdit_getWindow(event->data);
            pan.pan = new_panel((pan.win));
            wmove(pan.win, 2, 0);
            wprintw(pan.win, "Tariff  : %lf", dat->tariff);
            wmove(pan.win, 1, 0);
            wattron(pan.win, WA_REVERSE);
            Win_vanishLine(pan.win);
            waddstr(pan.win, "Capacity: ");
            String_appendFormat(str, "%lf", dat->capacity);
            waddstr(pan.win, String_readOnly(str));
            Panel_updateAll();
            return;
        }
        case UserSignal:{
            wint_t input = *(wint_t *)event->data;
            switch(input){
                case KEY_HOME:{
                    if (prov->cloud) {
                        DataCloud_free(prov->cloud);
                        prov->cloud = NULL;
                    }
                }   //////////////////////
                case KEY_ESCAPE: {
                    DataCloud_free(dat);
                    dat = NULL;
                    String_free(str);
                    del_panel(pan.pan);
                    wattroff(pan.win, WA_REVERSE);
                    Panel_updateAll();
                    emitData(MenuList_UpdatedProvider, prov);
                    return;
                }
                case '\n':{
                    if (isValid(str)) {
                        //
                        if (isValid == isCapacity){
                            int ret = String_scanFormat(str, "%lf", &(dat->capacity));
                            Debug_suppose(ret, "Invalid data scaned");
                        } else {
                            int ret = String_scanFormat(str, "%lf", &(dat->tariff));
                            Debug_suppose(ret, "Invalid data scaned");
                        }
                    }
                    if (prov->cloud){
                        *prov->cloud = *dat;
                        DataCloud_free(dat);
                        dat = NULL;
                    } else {
                        prov->cloud = dat;
                    }
                    String_free(str);
                    del_panel(pan.pan);
                    wattroff(pan.win, WA_REVERSE);
                    Panel_updateAll();
                    emitData(MenuList_UpdatedProvider, prov);
                    return;
                }
//                case KEY_MOUSE:{ // todo
//                }
                case KEY_UP:{
                    if (isValid == isCapacity) return;
                    //
                    if (isValid(str)){
                        String_scanFormat(str, "%lf", &(dat->tariff));
                    } else {
                        wprintw(pan.win, "%lf", dat->tariff);
                    }
                    isValid = isCapacity;
                    String_clear(str);
                    moveVanishLine(pan.win, &loc(1, 0), dat, str);
                    Panel_updateAll();
                    return;
                }
                case KEY_DOWN:{
                    if (isValid == isTariff) return;
                    //
                    if (isValid(str)){
                        String_scanFormat(str, "%lf", &(dat->capacity));
                    } else {
                        wprintw(pan.win, "%lf", dat->capacity);
                    }
                    isValid = isTariff;
                    String_clear(str);
                    moveVanishLine(pan.win, &loc(2, 0), dat, str);
                    Panel_updateAll();
                    return;
                }
                case KEY_BACKSPACE:{
                    if (!String_length(str)){
                        return;
                    }
                    String_reduce(str, String_length(str) - 1);
                    Win_shift_vanishChar(pan.win, -1);
                    Panel_updateAll();
                    return;
                }
                default:{
                    if (input >= KEY_MIN
                        || (getcurx(pan.win) == getmaxx(pan.win) - 2
                            && getcury(pan.win) == getmaxy(pan.win) - 1)) {
                        return;
                    }
                    //
                    String_appendChar(str, input);
                    if (!isValid(str)){
                        String_reduce(str, String_length(str) - 1);
                    } else {
                        waddch(pan.win, input);
                        Panel_updateAll();
                    }
                    return;
                }
            }
        }
    }
}




static void MenuList_taskHandler(EventHandler * dispatcher, Event * event){
    static PBMenu * menu = NULL;
    static Panel pan = {NULL, NULL};
    switch(event->type){
        case MenuList_DoTask:{
            const Proportions props = props(List_size(dispatcher->data) + 3, 11);
            Location loc = getScreenCenter(props);
            pan.win = Win_new(props, loc, &WF_SINGLE, GLOBAL_ATTRS);
            pan.pan = new_panel(wgetparent(pan.win));
            waddstr(pan.win, "choose K:");
            char * str[List_size(dispatcher->data)];
            for (unsigned int i = 0; i < List_size(dispatcher->data); ++i){
                str[i] = "";
            }
            loc.y += 2;
            ++loc.x;
            menu = PBMenu_chNew(str, List_size(dispatcher->data), &noFrameAttrs,
                                props(1, 9), loc, VLAYOUT_NOGAP);

            for (unsigned int i = 0; i < List_size(dispatcher->data); ++i) {
                wprintw(PBMenu_getButtonWin(menu, i), "  %i", i + 1);
            }
            PBMenu_post(menu);
            return;
        }
        case UserSignal:{
            if (!PBMenu_driver(menu, PBM_REACT, *(wint_t *)event->data))
                return;
            //
            signed long i = PBMenu_getSelectedButton(menu);
            if (i < 0) return;
            //
            del_panel(pan.pan);
            Win_freeFamily(pan.win);
            PBMenu_unpost(menu);
            PBMenu_freeWhole(menu);
            EventSystem_emit(Event_new(NULL, MenuList_ChosenTask, Integer_new(i + 1), (Destructor)Integer_free));
            return;
        }
    }
}

static void MenuList_saveHandler(EventHandler * dispatcher, Event * event){
    static String * str = NULL;
    static Panel pan = {NULL, NULL};
    static PANEL * framePan = NULL;
    switch(event->type){
        case MenuList_Save:{
            str = String_new();
            pan.win = Win_new(props(4, 30), loc(10, 20), &WF_SINGLE, GLOBAL_ATTRS);
            pan.pan = new_panel(pan.win);
            framePan = new_panel(wgetparent(pan.win));
            Panel_updateAll();
//            replace_panel(pan.pan, pan.win);
            return;
        }
        case UserSignal:{
            wint_t input = *(wint_t *)event->data;
            switch(input){
                case KEY_ESCAPE:{
                    String_free(str);
                    del_panel(framePan);
                    del_panel(pan.pan);
                    Win_freeFamily(pan.win);
                    Panel_updateAll();
                    emit(MenuList_Saved);
                    return;
                }
                case KEY_BACKSPACE:{
                    if (!String_length(str)){
                        return;
                    }
                    String_reduce(str, String_length(str) - 1);
                    Win_shift_vanishChar(pan.win, -1);
                    Panel_updateAll();
                    return;
                }
                case KEY_NEWLINE:{
                    CsvTable * table = CsvTable_new();
                    for (unsigned int i = 0; i < List_size(dispatcher->data); ++i) {
                        CsvTable_add(table, Provider_toNewCsvRow(List_at(dispatcher->data, i)));
                    }
                    String * result = CsvTable_toNewString(table);
                    CsvTable_freeWhole(table);
                    String_writeToPathString(str, result);
                    String_free(result);
                    String_free(str);
                    del_panel(framePan);
                    del_panel(pan.pan);
                    Win_freeFamily(pan.win);
                    Panel_updateAll();
                    emit(MenuList_Saved);
                    return;
                }
                default:{
                    if (input >= KEY_MIN
                        || (getcurx(pan.win) == getmaxx(pan.win) - 2
                            && getcury(pan.win) == getmaxy(pan.win) - 1)) {
                        return;
                    }
                    //
                    String_appendChar(str, input);
                    waddch(pan.win, input);
                    Panel_updateAll();
                }
            }
        }
    }
}






static ProviderEdit * ProviderEdit_new(Provider * provider, enum DataPanelSignal field, WINDOW * editWin){
    ProviderEdit * newbie = Mem_malloc(sizeof(ProviderEdit));
    *newbie = (ProviderEdit){
            .provider = provider,
            .field = field,
            .editWin = editWin
    };
    return newbie;
}

static void ProviderEdit_free(ProviderEdit * self){
    free(self);
}

static Provider * ProviderEdit_getProvider(ProviderEdit * self){
    Debug_exists(self);
    //
    return self->provider;
}

static enum DataPanelSignal ProviderEdit_getField(ProviderEdit * self){
    Debug_exists(self);
    //
    return self->field;
}

static WINDOW * ProviderEdit_getWindow(ProviderEdit * self){
    Debug_exists(self);
    //
    return self->editWin;
}