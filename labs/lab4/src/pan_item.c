#include <pan_item.h>
#include <assert.h>
#include <error.h>
#include <mem.h>




PanItem * PanItem_newStd(PANEL * pan, bool panIsImmutable){
    Debug_exists(pan);
    //
    PanItem * newbie = Mem_malloc(sizeof(PanItem));

    *newbie = (PanItem){
        .value = false,
        .selectable = true,
        .hook = NULL,
        .ptr = NULL,
        .pan = pan,
    };
    if (panIsImmutable){
        newbie->dupwin = dupwin(panel_window(pan));
        Error_checkHeap(newbie->dupwin);
        newbie->duppan = new_panel(newbie->dupwin);
        Error_checkHeap(newbie->duppan);
    } else {
        newbie->dupwin = panel_window(pan);
        newbie->duppan = pan;
    }
    //
    return newbie;
}

PanItem * PanItem_new (PANEL * pan,
                        bool   panIsImmutable, 
                        bool   value,
                        bool   selectable, 
                        void * ptr, 
                PanItem_Hook   hook){
    //
    Debug_exists(pan);
    //
    PanItem * newbie = Mem_malloc(sizeof(PanItem));
    *newbie = (PanItem){
        .value = value,
        .selectable = selectable,
        .hook = hook,
        .ptr = ptr,
        .pan = pan
    };
    if (panIsImmutable){
        newbie->dupwin = dupwin(panel_window(pan));
        Error_checkHeap(newbie->dupwin);
        newbie->duppan = new_panel(newbie->dupwin);
        Error_checkHeap(newbie->duppan);
    } else {
        newbie->dupwin = panel_window(pan);
        newbie->duppan = pan;
    }
    return newbie;
}

void PanItem_free(PanItem * self){
    Debug_exists(self);
    //
    if (self->duppan != self->pan) {
        del_panel(self->duppan);
        Win_freeFamily(self->dupwin);        
    }
    free(self);
}


// GETTERS

bool PanItem_getValue(PanItem * self){
    Debug_exists(self);
    //
    return self->value;
}

PANEL * PanItem_getPan(PanItem * self){
    Debug_exists(self);
    //
    return self->pan;
}

bool PanItem_getSelectable(PanItem * self){
    Debug_exists(self);
    //
    return self->selectable;
}
void * PanItem_getPtr(PanItem * self){
    Debug_exists(self);
    //
    return self->ptr;
}

PanItem_Hook PanItem_getHook(PanItem * self){
    Debug_exists(self);
    //
    return self->hook;
}       

WINDOW * PanItem_getDupwin(PanItem * self){
    Debug_exists(self);
    //
    return self->dupwin;
}

PANEL * PanItem_getDuppan(PanItem * self){
    Debug_exists(self);
    //
    return self->duppan;
}

// SETTERS

bool PanItem_setValue(PanItem * self, bool newValue){
    Debug_exists(self);
    //
    bool prev = self->value;
    self->value = newValue;
    return prev;
}

bool PanItem_toggleValue(PanItem * self){
    Debug_exists(self);
    //
    self->value = !self->value;
    return !self->value;
}    

PANEL * PanItem_setPan(PanItem * self, PANEL * newPan){
    Debug_exists(self);
    //
    PANEL * prev = self->pan;
    self->pan = newPan;
    return prev;
}

bool PanItem_setSelectable(PanItem * self, bool newSelectable){
    Debug_exists(self);
    //
    bool prev = self->selectable;
    self->selectable = newSelectable;
    return prev;
}

bool PanItem_toggleSelecrable(PanItem * self){
    Debug_exists(self);
    //
    self->selectable = !self->selectable;
    return !self->selectable;
}

void * PanItem_setPtr(PanItem * self, void * newPtr){
    Debug_exists(self);
    //
    void * prev = self->ptr;
    self->ptr = newPtr;
    return prev;
} 

PanItem_Hook PanItem_setHook(PanItem * self, PanItem_Hook newHook){
    Debug_exists(self);
    //
    PanItem_Hook prev = self->hook;
    self->hook = newHook;
    return prev;
}


void PanItem_execHook(PanItem * self){
    Debug_exists(self);
    Debug_exists(PanItem_getHook(self) != NULL);
    //
    (*PanItem_getHook(self))(self);
}

WINDOW * PanItem_setDupwin(PanItem * self, WINDOW * dupwin){
    Debug_exists(self);
    //
    WINDOW * prev = self->dupwin;
    self->dupwin = dupwin;
    return prev;
}

PANEL * PanItem_setDuppan(PanItem * self, PANEL * duppan){
    Debug_exists(self);
    //
    PANEL * prev = self->duppan;
    self->duppan = duppan;
    return prev;
}


WINDOW * PanItem_getWin(PanItem * self){
    Debug_exists(self);
    //
    return panel_window(self->pan);
}




PanItem ** PanItem_newArr(PANEL ** panels, const unsigned int size, bool pansAreImmutable){
    Debug_exists(panels);
    //
    PanItem ** newbie = malloc(size * sizeof(PanItem *)); 
    for (int i = 0; i < size; ++i){
        assert(panels[i] != NULL);
        //
        newbie[i] = PanItem_newStd(panels[i], pansAreImmutable);
    }
    return newbie;
}

void PanItem_freeArr(PanItem ** self, const unsigned int size){
    Debug_exists(self);
    //
    for (int i = 0; i < size; ++i){
        assert(self[i] != NULL);
        //
        PanItem_free(self[i]);
    }
    free(self);
}