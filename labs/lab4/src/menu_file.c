#include <dispatcher.h>
#include <dir_driver.h>
#include <provider.h>

static void MenuFile_buttons(int state, wint_t input);
static void MenuFile_labels(int state);


typedef enum __MenuFileSt{
    MenuFileSt_Idling,
    MenuFileSt_StartReturning,
    MenuFileSt_Returning,
    MenuFileSt_StopReturning
} MenuFileSt;

static MenuFileSt MenuFile_getNextState(MenuFileSt currentState, Event * event);

void MenuFile_dispatcher(EventHandler * self, Event * event){
    switch(event->type){
        case MenuFile_SelectedFile:{
            CsvTable * table = CsvTable_newFromString(event->data);
            if (!table){
                MenuFile_buttons(RESTART, '\0');
                return;
            }
            List * rows = List_new();
            CsvTable_getRows(table, rows);
    
            List * provList = List_new();
            List * providerStrings = List_new();
            for (size_t i = 0; i < List_size(rows); ++i){
                CsvRow_getValues(List_at(rows, i), providerStrings);
                Provider * prov = Provider_stringsToNewProvider(providerStrings);
                if (!prov){
                    List_freeWhole(provList, (Destructor)Provider_freeWhole);
                    CsvTable_freeWhole(table);
                    List_free(rows);
                    List_free(providerStrings);
                    MenuFile_buttons(RESTART, '\0');
                    return;
                }
                List_addLast(provList, prov);
                List_clear(providerStrings);
            }
            List_free(providerStrings);
            List_free(rows);
            CsvTable_freeWhole(table);
            EventSystem_addHandler(EventHandler_new(provList, (Destructor)List_freeProviders, MenuList_dispatcher));
            emit(Goto_MenuList);
            return;
        }
        case Goto_MainMenu:{
            EventSystem_addHandler(EventHandler_new(NULL, NULL, MainMenu_dispatcher));
            EventSystem_removeHandler(self);
            MenuFile_buttons(FREE, '\0');
            return;
        }
        case Goto_MenuFile:{
            // MenuFile_labels(INIT);
            MenuFile_buttons(INIT, '\0');
            return;
        }
        case UserSignal:{
            static MenuFileSt state = MenuFileSt_Idling;
            switch ((state = MenuFile_getNextState(state, event))){
                case MenuFileSt_Idling:{
                    MenuFile_buttons(REACT, *(wint_t *)event->data);
                    return;
                }
                case MenuFileSt_StartReturning:{
                    Dispatcher_initDialog(L"Are you really want to go back to main menu?");
                    return;
                }
                case MenuFileSt_Returning:{
                    switch (Dispatcher_reactDialog(*(wint_t *)event->data)){
                        case CHOOSE_OK: {
                            Dispatcher_freeDialog();
                            state = MenuFileSt_Idling;
                            emit(Goto_MainMenu);
                            doupdate();
                            return;
                        }
                        case CHOOSE_CANCEL: {
                            state = MenuFileSt_Idling;
                            MenuFile_buttons(RESTART, '\0');
                            Dispatcher_freeDialog();
                            doupdate();
                            return;
                        }
                        case CHOOSE_IDLE: {
                            return;
                        }
                    }
                }
                case MenuFileSt_StopReturning:{
                    MenuFile_buttons(RESTART, '\0');
                    Dispatcher_freeDialog();
                    doupdate();
                    return;
                }
            }
            

            return;           
        }
        case Goto_MenuList: EventSystem_removeHandler(self);
        case ExitEventTypeId:{
            // MenuFile_labels(FREE);
            MenuFile_buttons(FREE, '\0');
            return;
        }        
    }
}

static MenuFileSt MenuFile_getNextState(MenuFileSt currentState, Event * event){
    Debug_exists(event);
    //
    switch (currentState){
        case MenuFileSt_Idling:
        case MenuFileSt_StopReturning:{
            switch (*(wint_t *)event->data){
                case KEY_ESCAPE: return MenuFileSt_StartReturning;
                default: return MenuFileSt_Idling;
            }
        }
        case MenuFileSt_StartReturning:
        case MenuFileSt_Returning:{
            switch (*(wint_t *)event->data){
                case KEY_ESCAPE: return MenuFileSt_StopReturning;
                default: return MenuFileSt_Returning;
            }
        }
    }
    return MenuFileSt_Idling;
}




static void Private_addPrefix(List * list, size_t size, const char * prefix){
    for (size_t i = 0; i < size; ++i){
        String_addCharsFirst(List_at(list, i), prefix);
    }
}


static void Private_removePrefix(List * list, size_t size, size_t prefixLen){
    for (size_t i = 0; i < size; ++i){
        String_cut(List_at(list, i), prefixLen, String_length(List_at(list, i)) - 1);
    }
}


static PBMenu * Private_goNextDirectory(List * dirs, 
                                  PBMenu * menu,
                               DirDriver * driver, 
                                  size_t * dirsSize, 
                                  String * nextDir){
    DirDriver_goNextDir(driver, nextDir);

    List_clearWhole(dirs, (Destructor)String_free);
    
    DirDriver_appendNewIndirs(driver, dirs);

    List_addFirst(dirs, String_toNewString(UNI_GO_BACK));
    *dirsSize = List_size(dirs);
    Private_addPrefix(dirs, *dirsSize, UNI_FOLDER " ");
    
    DirDriver_appendNewFiles(driver, dirs);

    PBMenu_unpost(menu);
    menu = PBMenu_strRenew(menu, dirs);
    PBMenu_post(menu);
    doupdate();
    Private_removePrefix(dirs, *dirsSize, 5);
    return menu;
}


static  PBMenu * Private_goPrevDirectory(List * dirs,
                                  PBMenu * menu,
                               DirDriver * driver,
                                  size_t * dirsSize){
    DirDriver_goPrevDir(driver);

    List_clearWhole(dirs, (Destructor)String_free);
    
    DirDriver_appendNewIndirs(driver, dirs);

    List_addFirst(dirs, String_toNewString(UNI_GO_BACK));
    *dirsSize = List_size(dirs);
    Private_addPrefix(dirs, *dirsSize, UNI_FOLDER " ");
    
    DirDriver_appendNewFiles(driver, dirs);

    PBMenu_unpost(menu);
    menu = PBMenu_strRenew(menu, dirs);
    PBMenu_post(menu);
    doupdate();
    Private_removePrefix(dirs, *dirsSize, 5);
    return menu;
}



static void MenuFile_buttons(int state, wint_t input){
    static PBMenu * menu = NULL;
    static DirDriver * dirDriver = NULL;
    static List * dirs = NULL;
    static size_t dirsSize = 0;
    switch (state){
        case REACT:{
            if (!PBMenu_driver(menu, BM_REACT, input)) 
                return;  // case not updated menu
            doupdate();
            if (PBMenu_isButtonSelected(menu, 0)){
                menu = Private_goPrevDirectory(dirs, menu, dirDriver, &dirsSize);
                return;
            }

            signed long selected =  PBMenu_getSelectedButton(menu);
            if (selected == -1L) 
                return;

            if (selected < dirsSize){
                menu = Private_goNextDirectory(dirs, menu, dirDriver, &dirsSize, List_at(dirs, selected));
            } else {
                String * read = String_new();
                if (!String_readFromPathString(List_at(dirs, selected), read)){
					String_free(read);
                    return;
                }
                EventSystem_emit(
                    Event_new(
                        NULL, 
                        MenuFile_SelectedFile, 
                        read,
                        (Destructor)String_free
                    )
                );
            }
            return;
        }
        case INIT:{
            dirs = List_new();
            dirDriver = DirDriver_newLocal();
        
            DirDriver_appendNewIndirs(dirDriver, dirs);
        
            List_addFirst(dirs, String_toNewString(UNI_GO_BACK));
            dirsSize = List_size(dirs);
            Private_addPrefix(dirs, dirsSize, UNI_FOLDER " ");
            
            DirDriver_appendNewFiles(dirDriver, dirs);
        
            const Proportions BUTTONS_PROPS = props(1, 30);
            const Location BUTTONS_LOC = getLineCenter(BUTTONS_PROPS, 0);
            menu = PBMenu_strNew(dirs, &noFrameAttrs, BUTTONS_PROPS, BUTTONS_LOC, VLAYOUT_NOGAP);
            PBMenu_post(menu);
            doupdate();
            return;
        }
        case RESTART:{
            signed long i = PBMenu_getSelectedButton(menu);
            if (i >= 0){
                PBMenu_driver(menu, PBM_DESELECT_BUTTON | i, '\0');
                doupdate();
            }
            return;
        }
        case FREE:{
            List_freeWhole(dirs, (Destructor)String_free);
            DirDriver_free(dirDriver);
            PBMenu_unpost(menu);
            PBMenu_freeWhole(menu);
            return;
        }
    }
    
}



static void MenuFile_labels(int state){
    static Panel * title = NULL;
    switch (state){
        case INIT:{
            title = Panel_new(props(9, 16), loc (3 , lineCenter(16)), &WF_SINGLE, COLOR_PAIR(2));
            ArtSetup_printExpilicit(title->win, &ART_SETUP, 
                L" :::::::::: ::::::::::: :::        ::::::::::  "
                 " :+:            :+:     :+:        :+:         "
                 " +:+            +:+     +:+        +:+         "
                 " :#::+::#       +#+     +#+        +#++:++#    "
                 " +#+            +#+     +#+        +#+         "
                 " #+#            #+#     #+#        #+#         "
                 " ###        ########### ########## ##########  "
            );
            Panel_refreshAll();
            break;
        }
        case FREE:{
            Panel_hide(title);
            Panel_free(title);
            break;
        }
    }  
}
