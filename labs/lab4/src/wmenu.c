#include <wmenu.h>
#include <assert.h>
#include <error.h>
#include <windows.h>
#include <witem.h>
#include <mem.h>


typedef struct __WMenu {
      WMenuAttrs    attrs;
           WItem ** items;
    unsigned int    curindex;
    unsigned int    itemscount;
            bool    cyclic;
} WMenu;


WMenu * WMenu_new(WMenuAttrs    attrs, 
                       WItem ** items, 
                unsigned int    itemscount, 
                unsigned int    curIndex, 
                        bool    isCyclic){
    //
    Debug_suppose(imply(items == NULL, itemscount == 0), "");
    assert(curIndex < itemscount || curIndex == 0);
    //
    WMenu * newbie = Mem_malloc(sizeof(WMenu));
    *newbie = (WMenu){
        .attrs = attrs,
        .items = items,
        .curindex = curIndex,
        .itemscount = itemscount,
        .cyclic = isCyclic
    };
    return newbie;
}

void WMenu_free(WMenu * self){
    Debug_exists(self);
    //
    free(self);
}


// Getters

WItem ** WMenu_getItems(WMenu * self){
    assert(self != NULL);
    //
    return self->items;
}

WItem * WMenu_itemAt(WMenu * self, unsigned int index){
    assert(self != NULL);
    //
    return self->items[index];
}

WItem * WMenu_getCurItem(WMenu * self){
    assert(self != NULL);
    //
    return self->items[self->curindex];
}

unsigned int WMenu_getCurIndex(WMenu * self){
    assert(self != NULL);
    //
    return self->curindex;
}

attr_t WMenu_getFore(WMenu * self){
    assert(self != NULL);
    //
    return self->attrs.fore;
}

attr_t WMenu_getBack(WMenu * self){
    assert(self != NULL);
    //
    return self->attrs.back;
}

attr_t WMenu_getGrey(WMenu * self){
    assert(self != NULL);
    //
    return self->attrs.grey;
}

attr_t WMenu_getSelected(WMenu * self){
    assert(self != NULL);
    //
    return self->attrs.selected;
}

attr_t WMenu_getReselected(WMenu * self){
    assert(self != NULL);
    //
    return self->attrs.reselected;
}

unsigned int WMenu_getItemsCount(WMenu * self){
    assert(self != NULL);
    //
    return self->itemscount;
}

bool WMenu_getCyclic(WMenu * self){
    assert(self != NULL);
    //
    return self->cyclic;
}

// Setters

WItem ** WMenu_setItems(WMenu * self, WItem ** newItems){
    assert(self != NULL);
    //
    WItem ** prev = self->items;
    self->items = newItems;
    return prev;
}   

WItem * WMenu_setItemAt(WMenu * self, unsigned int index, WItem * newItem){
    assert(self != NULL);
    assert(index < self->itemscount);
    //
    WItem * prev = self->items[index];
    self->items[index] = newItem;
    return prev;
}

unsigned int WMenu_setCurIndex(WMenu * self, unsigned int newIndex){
    assert(self != NULL);
    //
    unsigned int prev = self->curindex;
    self->curindex = newIndex;
    return prev;
}

unsigned int WMenu_setItemsCount(WMenu * self, unsigned int newItemscount){
    assert(self != NULL);
    //
    unsigned int prev = self->itemscount;
    self->itemscount = newItemscount;
    return prev;
}        

WMenuAttrs WMenu_includeAttrs(WMenu * self, WMenuAttrs attrs){
    assert(self != NULL);
    //
    WMenuAttrs prev = self->attrs;
    WMenu_includeFore(self, attrs.fore);
    WMenu_includeBack(self, attrs.back);
    WMenu_includeGrey(self, attrs.grey);
    WMenu_includeSelected(self, attrs.selected);
    WMenu_includeReselected(self, attrs.reselected);
    return prev;
}

WMenuAttrs WMenu_excludeAttrs(WMenu * self, WMenuAttrs attrs){
    assert(self != NULL);
    //
    WMenuAttrs prev = self->attrs;
    WMenu_excludeFore(self, attrs.fore);
    WMenu_excludeBack(self, attrs.back);
    WMenu_excludeGrey(self, attrs.grey);
    WMenu_excludeSelected(self, attrs.selected);
    WMenu_excludeReselected(self, attrs.reselected);
    return prev;
}

WMenuAttrs WMenu_setAttrs(WMenu * self, WMenuAttrs attrs){
    assert(self != NULL);
    //
    WMenuAttrs prev = self->attrs;
    self->attrs = attrs;
    return prev;
} 

attr_t WMenu_includeFore(WMenu * self, attr_t foreAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.fore;
    self->attrs.fore |= foreAttr;
    return prev;
}                 

attr_t WMenu_includeBack(WMenu * self, attr_t backAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.back;
    self->attrs.back |= backAttr;
    return prev;
}                 

attr_t WMenu_includeGrey(WMenu * self, attr_t greyAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.grey;
    self->attrs.grey |= greyAttr;
    return prev;
}              

attr_t WMenu_includeSelected(WMenu * self, attr_t selectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.selected;
    self->attrs.selected |= selectedAttr;
    return prev;
}

attr_t WMenu_includeReselected(WMenu * self, attr_t reselectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.reselected;
    self->attrs.reselected |= reselectedAttr;
    return prev;
}     

attr_t WMenu_excludeFore(WMenu * self, attr_t foreAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.fore;
    self->attrs.fore &= ~foreAttr;
    return prev;
}                  

attr_t WMenu_excludeBack(WMenu * self, attr_t backAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.back;
    self->attrs.back &= ~backAttr;
    return prev;
}              

attr_t WMenu_excludeGrey(WMenu * self, attr_t greyAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.grey;
    self->attrs.grey &= ~greyAttr;
    return prev;
}                

attr_t WMenu_excludeSelected(WMenu * self, attr_t selectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.selected;
    self->attrs.selected &= ~selectedAttr;
    return prev;
}

attr_t WMenu_excludeReselected(WMenu * self, attr_t reselectedAttr){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.reselected;
    self->attrs.reselected &= ~reselectedAttr;
    return prev;
}    


attr_t WMenu_setFore(WMenu * self, attr_t newFore){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.fore;
    self->attrs.fore = newFore;
    return prev;
}                     

attr_t WMenu_setBack(WMenu * self, attr_t newBack){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.back;
    self->attrs.back = newBack;
    return prev;
}                     

attr_t WMenu_setGrey(WMenu * self, attr_t newGrey){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.grey;
    self->attrs.grey = newGrey;
    return prev;
}                     

attr_t WMenu_setSelected(WMenu * self, attr_t newSelected){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.selected;
    self->attrs.selected = newSelected;
    return prev;
}             

attr_t WMenu_setReselected(WMenu * self, attr_t newReselected){
    assert(self != NULL);
    //
    attr_t prev = self->attrs.reselected;
    self->attrs.reselected = newReselected;
    return prev;
}

bool WMenu_setCyclic(WMenu * self, bool newCyclic){
    assert(self != NULL);
    //
    bool prev = self->cyclic;
    self->cyclic = newCyclic;
    return prev;
}                 

bool WMenu_toggleCyclic(WMenu * self){
    assert(self != NULL);
    //
    self->cyclic = !self->cyclic;
    return !self->cyclic;
}         



// MAIN WORKING HORSE:
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
bool WMenu_driver(WMenu * menu, unsigned long int option); //::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


static void setItemToSelected   (WMenu * menu, unsigned int index);
static void setItemToFore       (WMenu * menu, unsigned int index);
static void setItemToBack       (WMenu * menu, unsigned int index);
static void setItemToReselected (WMenu * menu, unsigned int index);
static void setItemToGrey       (WMenu * menu, unsigned int index);

static void setSelector  (WMenu * menu, unsigned int index);
static void unsetSelector(WMenu * menu);


// Driver fucntions


void WMenu_post(WMenu * menu){
    assert(menu != NULL);
    assert(WMenu_getItems(menu) != NULL);
    assert(WMenu_itemAt(menu, 0) != NULL);
    //
    WItem_setValue(WMenu_itemAt(menu, 0), false);
    setSelector(menu, 0);
    if ((WItem_getSelectable(WMenu_itemAt(menu, 0))) == false)
        setItemToGrey(menu, 0);
    //
    for (int i = 1; i < WMenu_getItemsCount(menu); ++i){
        WItem_setValue(WMenu_itemAt(menu, i), false);
        if ((WItem_getSelectable(WMenu_itemAt(menu, i))) == false){
            setItemToGrey(menu, i);
        } else {
            setItemToBack(menu, i);
        }
    }
}

void unsetSelector(WMenu * menu){
    assert(menu != NULL);
    //
    if (WItem_getSelectable(WMenu_getCurItem(menu)) == true){
        if (WItem_getValue(WMenu_getCurItem(menu)) == true)
            setItemToSelected(menu, WMenu_getCurIndex(menu));
        else
            setItemToBack(menu, WMenu_getCurIndex(menu));
    }
}

void setSelector(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WMenu_setCurIndex(menu, index);
    if (WItem_getSelectable(WMenu_getCurItem(menu)) == true){
        if (WItem_getHook(WMenu_getCurItem(menu)) != NULL)
            WItem_execHook(WMenu_getCurItem(menu));
            
        if (WItem_getValue(WMenu_itemAt(menu, index)) == true)
            setItemToReselected(menu, index); // case already was selected        
        else
            setItemToFore(menu, index);
    } 
}

void setItemToBack(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WItem * item = WMenu_itemAt(menu, index);
    if (WItem_getDupwin(item) == WItem_getWin(item)){
        wbkgd(WItem_getWin(item), WMenu_getBack(menu));
        wnoutrefresh(WItem_getWin(item));
        //
    } else if (WMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(WItem_getDupwin(item), WMenu_getBack(menu));
        wnoutrefresh(WItem_getDupwin(item));
        overwrite(WItem_getWin(item), WItem_getDupwin(item));
        //
    } else {
        touchwin(WItem_getWin(item));
        wnoutrefresh(WItem_getWin(item));
    }

}

void setItemToSelected(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WItem * item = WMenu_itemAt(menu, index);
    if (WItem_getDupwin(item) == WItem_getWin(item)){
        wbkgd(WItem_getWin(item), WMenu_getSelected(menu));
        wnoutrefresh(WItem_getWin(item));
        //
    } else if (WMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(WItem_getDupwin(item), WMenu_getSelected(menu));
        wnoutrefresh(WItem_getDupwin(item));
        overwrite(WItem_getWin(item), WItem_getDupwin(item));
        //
    } else {
        touchwin(WItem_getWin(item));
        wnoutrefresh(WItem_getWin(item));
    }
}

void setItemToFore(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WItem * item = WMenu_itemAt(menu, index);
    if (WItem_getDupwin(item) == WItem_getWin(item)){
        wbkgd(WItem_getWin(item), WMenu_getFore(menu));
        wnoutrefresh(WItem_getWin(item));
        //
    } else if (WMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(WItem_getDupwin(item), WMenu_getFore(menu));
        wnoutrefresh(WItem_getDupwin(item));
        overwrite(WItem_getWin(item), WItem_getDupwin(item));
        //
    } else {
        touchwin(WItem_getWin(item));
        wnoutrefresh(WItem_getWin(item));
    }
}

void setItemToReselected(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WItem * item = WMenu_itemAt(menu, index);
    if (WItem_getDupwin(item) == WItem_getWin(item)){
        wbkgd(WItem_getWin(item), WMenu_getReselected(menu));
        wnoutrefresh(WItem_getWin(item));
        //
    } else if (WMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(WItem_getDupwin(item), WMenu_getReselected(menu));
        wnoutrefresh(WItem_getDupwin(item));
        overwrite(WItem_getWin(item), WItem_getDupwin(item));
        //
    } else {
        touchwin(WItem_getWin(item));
        wnoutrefresh(WItem_getWin(item));
    }
}

void setItemToGrey(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WItem * item = WMenu_itemAt(menu, index);
    if (WItem_getDupwin(item) == WItem_getWin(item)){
        wbkgd(WItem_getWin(item), WMenu_getGrey(menu));
        wnoutrefresh(WItem_getWin(item));
        //
    } else if (WMenu_getBack(menu) != WA_DEFAULT){
        Win_appendAttr(WItem_getDupwin(item), WMenu_getGrey(menu));
        wnoutrefresh(WItem_getDupwin(item));
        overwrite(WItem_getWin(item), WItem_getDupwin(item));
        //
    } else {
        touchwin(WItem_getWin(item));
        wnoutrefresh(WItem_getWin(item));
    }
}

void WMenu_refresh(WMenu * menu, unsigned int index){
    assert(menu != NULL);
    assert(index < WMenu_getItemsCount(menu));
    //
    WItem * item = WMenu_itemAt(menu, index); 
    if (WItem_getSelectable(item) == false){
        setItemToGrey(menu, index);        
    //
    } else if (WMenu_getCurIndex(menu) == index){
        //
        if (WItem_getValue(item) == true) {
            setItemToReselected(menu, index);            
        } else {
            setItemToFore(menu, index);
        }
    //
    } else if (WItem_getValue(item) == true){
        setItemToSelected(menu, index);        
    } else {
        setItemToBack(menu, index);        
    }
}

signed long WMenu_getEnclosedItemIndex(WMenu * menu, Location loc){
    assert(menu != NULL);
    //
    WItem ** item = WMenu_getItems(menu);
    unsigned int itemscnt = WMenu_getItemsCount(menu); 
    unsigned int i = WMenu_getCurIndex(menu);
    signed long j = (signed long)i - 1L;
    while (j >= 0 && i < itemscnt){
        if (wenclose(WItem_getWin(item[i]), loc.y, loc.x))
            return i;
        if (wenclose(WItem_getWin(item[j]), loc.y, loc.x))
            return j; 
        ++i;
        --j;
    }
    if (i < itemscnt){
        do {
            if (wenclose(WItem_getWin(item[i]), loc.y, loc.x))
                return i;
            ++i;
        } while (i < itemscnt);
    }
    else while (j >= 0){
        if (wenclose(WItem_getWin(item[j]), loc.y, loc.x))
            return j; 
        --j;
    }
    return -1;
}

signed long WMenu_getNextSelectableIndex(WMenu * menu){
    assert(WMenu_getCurIndex(menu) < WMenu_getItemsCount(menu));
    //    
    signed long i = (signed long)WMenu_getCurIndex(menu) + 1L;
    while (i < WMenu_getItemsCount(menu)){
        if (WItem_getSelectable(WMenu_itemAt(menu, i)) == true)
            return i;
        ++i;
    }
    i = 0L;
    while (i != WMenu_getCurIndex(menu)){
        if (WItem_getSelectable(WMenu_itemAt(menu, i)) == true)
            return i;
        ++i;
    }
    return -1L;
}

signed long WMenu_getPrevSelectableIndex(WMenu * menu){
    assert(WMenu_getCurIndex(menu) < WMenu_getItemsCount(menu));
    //
    signed long i = (signed long)WMenu_getCurIndex(menu) - 1L;
    //
    assert(i < WMenu_getItemsCount(menu));
    //
    while (i >= 0){
        if (WItem_getSelectable(WMenu_itemAt(menu, i)) == true){
            return i;
        }
        --i;
    }
    if (WMenu_getCyclic(menu) == false) return -1L;
    //
    i = (signed long)WMenu_getItemsCount(menu) - 1L;
    while (i != WMenu_getCurIndex(menu)){
        if (WItem_getSelectable(WMenu_itemAt(menu, i)) == true)
            return i;
        --i;
    }
    return -1L;
}

bool WMenu_driver(WMenu * menu, unsigned long int option){
    assert(menu != NULL);
    //
    switch (option){
        case WM_FIRST_ITEM:{
            if (WMenu_getCurIndex(menu) != 0){
                unsetSelector(menu);
                setSelector(menu, 0);
                return true;
            //
            } else return false;
        }
        case WM_LAST_ITEM:{
            if (WMenu_getCurIndex(menu) != WMenu_getItemsCount(menu) - 1){
                unsetSelector(menu);
                setSelector(menu, WMenu_getItemsCount(menu) - 1);
                return true;
            //
            } else return false;
        }
        case WM_NEXT_ITEM:{
            signed long nextIndex = WMenu_getNextItemIndex(menu);
            if (nextIndex != -1L){
                unsetSelector(menu);
                setSelector(menu, (unsigned int)nextIndex);
                return true;
            //
            } else return false;
        }
        //
        case WM_NEXT_SELECTABLE:{
            signed long nextSelectable = WMenu_getNextSelectableIndex(menu);
            if (nextSelectable == -1L) return false;
            return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)nextSelectable);
        }
        case WM_PREV_ITEM:{
            signed long prevIndex = WMenu_getPrevItemIndex(menu);
            if (prevIndex != -1L){
                unsetSelector(menu);
                setSelector(menu, (unsigned int)prevIndex);
                return true;
            //
            } else return false;
        }
        case WM_PREV_SELECTABLE:{
            signed long prevSelectable = WMenu_getPrevSelectableIndex(menu);
            if (prevSelectable == -1L) return false;
            return WMenu_driver(menu, WM_JUMP_TO | (unsigned int)prevSelectable);
        }
        case WM_MOUSE_EVENT:{
            MEVENT event;
            if (getmouse(&event) == ERR) return false;
            //
            signed long enclosed = WMenu_getEnclosedItemIndex(menu, loc(event.y, event.x));
            if (enclosed == -1) return false;
            //
            bool newState = WMenu_driver(menu, WM_JUMP_TO | (unsigned int)enclosed);
            if ((event.bstate & BUTTON1_PRESSED) || (event.bstate & BUTTON1_CLICKED))
                newState |= WMenu_driver(menu, WM_TOGGLE_ITEM | enclosed);
            //
            return newState || (event.bstate & BUTTON1_RELEASED);
        }
        case WM_RESTART:{
            bool newState = false;            
            if (WItem_getValue(WMenu_itemAt(menu, 0)) == true){
                WItem_setValue(WMenu_itemAt(menu, 0), false);
                if (WMenu_getCurIndex(menu) != 0)
                    WMenu_driver(menu, WM_FIRST_ITEM);                    
                else 
                    WMenu_refresh(menu, 0);
                newState = true;
            //
            } else if (WMenu_getCurIndex(menu) != 0){
                //
                newState = WMenu_driver(menu, WM_FIRST_ITEM);
                //
            }
            for (int i = 1; i < WMenu_getItemsCount(menu); ++i){
                newState |= WMenu_driver(menu, WM_DESELECT_ITEM | i);
            }
            return newState;
        }
        default:{ 
            if ((option & WM_JUMP_TO) != 0){ // case WM_JUMP_TO | index
                unsigned int index = (unsigned int)(option & ~WM_JUMP_TO);
                assert(index < WMenu_getItemsCount(menu));
                //
                if (WMenu_getCurIndex(menu) == index) return false;
                //
                unsetSelector(menu);
                setSelector(menu, index);
                return true;
            //
            } else if ((option & WM_REFRESH_ITEM) != 0){ // case WM_REFRESH_ITEM | index
                assert((option & ~WM_REFRESH_ITEM) < WMenu_getItemsCount(menu));
                WMenu_refresh(menu, option & ~WM_REFRESH_ITEM);
                return true;
            //
            } else if ((option & WM_TOGGLE_ITEM) != 0){ // case WM_TOGGLE_ITEM | index
                unsigned int index = (unsigned int)(option & ~WM_TOGGLE_ITEM);
                assert(index < WMenu_getItemsCount(menu));
                //
                if (WItem_getSelectable(WMenu_itemAt(menu, index)) == false)
                    return false;
                //
                WItem_toggleValue(WMenu_itemAt(menu, index));
                WMenu_refresh(menu, index);
                return true;
            //
            } else if ((option & WM_SELECT_ITEM) != 0){ // case WM_SELECT_ITEM | index
                unsigned int index = (unsigned int)(option & ~WM_SELECT_ITEM);
                assert(index < WMenu_getItemsCount(menu));
                //
                if (WItem_getSelectable(WMenu_itemAt(menu, index)) == false || WItem_getValue(WMenu_itemAt(menu, index)) == true){
                            return false;
                }
                //
                WItem_setValue(WMenu_itemAt(menu, index), true);
                WMenu_refresh(menu, index);
                return true;
            //
            } else if ((option & WM_DESELECT_ITEM) != 0){
                unsigned int index = (unsigned int)(option & ~WM_DESELECT_ITEM);
                assert(index < WMenu_getItemsCount(menu));
                //
                if (WItem_getSelectable(WMenu_itemAt(menu, index)) == false || WItem_getValue(WMenu_itemAt(menu, index)) == false){
                            return false;
                }
                //
                WItem_setValue(WMenu_itemAt(menu, index), false);
                WMenu_refresh(menu, index);
                return true;
            //
            } else if ((option & WM_SET_ITEM_GREY) != 0){
                unsigned int index = (unsigned int)(option & ~WM_SET_ITEM_GREY);
                assert(index < WMenu_getItemsCount(menu));
                //
                if (WItem_getSelectable(WMenu_itemAt(menu, index)) == false){
                    return false;                    
                }
                WItem_setSelectable(WMenu_itemAt(menu, index), false);
                WMenu_refresh(menu, index);
                return true;
            //
            }
            else assert(0 && "ITERNAL ERROR: UNHANDLED MENU OPTION RECIEVED.");
            //
            return false;
        }
    }
}

void WMenu_unpost(WMenu * menu, attr_t remainder){
    WItem ** item = WMenu_getItems(menu);
    unsigned int itemscnt = WMenu_getItemsCount(menu);
    unsigned int selector = WMenu_getCurIndex(menu);
    for (int i = 0; i < itemscnt; ++i){
        if (WItem_getWin(item[i]) == WItem_getDupwin(item[i])){
            Win_vanish(WItem_getWin(item[i]), remainder);
            wnoutrefresh(WItem_getWin(item[i]));
        //
        } else if (WItem_getValue(item[i]) == true){
            if (i == selector){
                if (WMenu_getReselected(menu) != WA_DEFAULT){
                    touchwin(WItem_getWin(item[i]));
                    wnoutrefresh(WItem_getWin(item[i]));
                //
                }
            //
            } else if (WMenu_getSelected(menu) != WA_DEFAULT){
                touchwin(WItem_getWin(item[i]));
                wnoutrefresh(WItem_getWin(item[i]));
            //
            } 
        // 
        } else {
            if (i == selector){
                if (WMenu_getFore(menu) != WA_DEFAULT){
                    touchwin(WItem_getWin(item[i]));
                    wnoutrefresh(WItem_getWin(item[i]));
                //
                }
            //
            } else if (WMenu_getBack(menu) != WA_DEFAULT){
                touchwin(WItem_getWin(item[i]));
                wnoutrefresh(WItem_getWin(item[i]));
            //
            }    
        //
        }
    }
}

signed long WMenu_getPrevItemIndex(WMenu * menu){
    assert(menu != NULL);
    //
    return (WMenu_getCurIndex(menu) > 0) ? (signed long)WMenu_getCurIndex(menu) - 1L : 
           (WMenu_getCyclic(menu) == true) ? (signed long)WMenu_getItemsCount(menu) - 1L : -1L;
}

signed long WMenu_getNextItemIndex(WMenu * menu){
    assert(menu != NULL);
    //
    return (WMenu_getCurIndex(menu) < WMenu_getItemsCount(menu) - 1) ? (signed long)WMenu_getCurIndex(menu) + 1L : 
           (WMenu_getCyclic(menu) == true) ? 0L : -1L;
}

WItem * WMenu_getPrevItem(WMenu * menu){
    assert(menu != NULL);
    //
    signed long prevIndex = WMenu_getPrevItemIndex(menu);
    if (prevIndex == -1L) return NULL;
    return WMenu_itemAt(menu, prevIndex);
}

WItem * WMenu_getNextItem(WMenu * menu){
    assert(menu != NULL);
    //
    signed long nextIndex = WMenu_getNextItemIndex(menu);
    if (nextIndex == -1L) return NULL;
    return WMenu_itemAt(menu, nextIndex);
}
