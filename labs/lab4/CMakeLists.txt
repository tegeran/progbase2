cmake_minimum_required(VERSION 2.8.9)

project (a.out)
include_directories(include /home/tegeran/libsone/include)
file(GLOB SOURCES "src/*.c" "/home/tegeran/libsone/src/*.c")
add_executable(${PROJECT_NAME} main.c ${SOURCES})
target_link_libraries(${PROJECT_NAME} m ncursesw menuw formw panelw)
SET(CMAKE_C_FLAGS "-g -std=c11 -Wall -pedantic-errors -Werror -Wno-unused")

#-----------------------

project (csv)

include_directories(include /home/tegeran/libsone/include)
file(GLOB SOURCES "src/*.c" "/home/tegeran/libsone/src/*.c")

#Generate the shared (STATIC, SHARED or MODULE) library from the sources
add_library(${PROJECT_NAME} STATIC ${SOURCES})


#-------------------

# install library headers
install(
        DIRECTORY      include/
        DESTINATION    /usr/local/include
)

install(
        DIRECTORY      /home/tegeran/libsone/include/
        DESTINATION    /usr/local/include
)



# Set the location for library installation -- i.e., /usr/local/lib in this case
# not really necessary in this example. Use "sudo make install" to apply
install(
        TARGETS     ${PROJECT_NAME}
        DESTINATION /usr/local/lib
)

