#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "server.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();


	void closeEvent(QCloseEvent *event) override;

private slots:
	void handle_socket_onlineChanged();

	void on_pb_open_clicked();

	void on_pb_close_clicked();

private:
	Ui::MainWindow *ui;
	Server * server;
};

#endif // MAINWINDOW_H
