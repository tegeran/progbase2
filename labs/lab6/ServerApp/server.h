 #ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QHash>
#include <QSet>

#include "tcpserver.h"
#include "tcpsocket.h"
#include "server_user.h"
#include "session.h"
#include "datapacket.h"

#define PORT 2000

class Server : public QObject {
	Q_OBJECT
public:

	explicit Server(QObject * const & parent = nullptr);
	virtual ~Server();

	bool open(const quint16 & port = PORT);
	void close();
	bool isOpened() const;
	int totalOnline() const;
signals:
	void error(const QString &reason);
	void socketConnected();
	void socketDisconnected();
private slots:
	void on_server_newConnection();
	void on_server_acceptError();
	void on_socket_receivedNetPacket(const NetPacket & packet);
	void on_socket_error();
	void on_socket_disconnected();
	void on_session_noUsersLeft();
private:
	TcpServer m_server;
	QHash<TcpSocket *, ServerUser *> m_usersOnline;
	QHash<id_t, Session *> m_sessionsOnline;
	QSet<TcpSocket *> m_unnamedUsers;
	id_t lastSessionId;

	ServerUser * userFromSocket(TcpSocket * const & socket) const;
	Session * sessionFromId(const id_t & id) const;

	void processRequest_CreateSession(TcpSocket & sender,
									const NetPacket & netpacket);

	void processRequest_OnlineSessions(TcpSocket & sender);

	void processRequest_SetUser(TcpSocket & sender,
							   const NetPacket & netpacket);


	void processRequest_ConnectToSession(TcpSocket & sender, const NetPacket & netpacket);

	void processRequest_SavedEntries(TcpSocket & sender, const NetPacket &);


	void sendErrorReport(TcpSocket & socket, const QString & reason);

	template <typename TPacket, typename... TPacketArgs>
	void sendDataToUsersOnline(TPacketArgs &&... args);

	template <typename TPacket, typename... TPacketArgs>
	void sendDataToUsersOnlineExcept(TcpSocket * const &exception,
									 TPacketArgs && ... args);
};


template <typename TPacket, typename... TPacketArgs>
void Server::sendDataToUsersOnline(TPacketArgs && ... args){
	NetPacket netpacket { TPacket::toNetPacket(std::forward<TPacketArgs>(args)...) };
	foreach_citer(iterator, m_usersOnline){
		netpacket.send(*iterator.key());
	}
}


template <typename TPacket, typename... TPacketArgs>
void Server::sendDataToUsersOnlineExcept(TcpSocket * const &exception,
											TPacketArgs && ... args)
{
	NetPacket netpacket { TPacket::toNetPacket(std::forward<TPacketArgs>(args)...) };
	foreach_citer(iterator, m_usersOnline){
		if (iterator.key() != exception){
			netpacket.send(*iterator.key());
		}
	}
}





#endif // SERVER_H




