#include "tcpserver.h"

TcpSocket *TcpServer::nextPendingTcpSocket(){
	return static_cast<TcpSocket *>(nextPendingConnection());
}

void TcpServer::incomingConnection(qintptr socketDescriptor){
	TcpSocket * newSocket { new TcpSocket };
	if (!newSocket->setSocketDescriptor(socketDescriptor)){
		emit acceptError(newSocket->error());
		delete newSocket;
	} else {
		addPendingConnection(static_cast<QTcpSocket *>(newSocket));
	}
}
