#include <QMessageBox>
#include <QCloseEvent>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qnet.h"
#include "qext.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	server(new Server)
{
	ui->setupUi(this);
	setWindowTitle("Providererr server");
	if (!server->open(PORT)){
		QMessageBox::critical(this, tr("SIGSEGV"),
							  tr("SIGSEGV: failed to open server at port %1").arg(PORT),
							  QMessageBox::Ok);
		QWidget::close();
	}
	Qnet::LocalAddresses localAddrs;
	ui->lbl_ipv4->setText(localAddrs.ipv4.toString());
	ui->lbl_ipv6->setText(localAddrs.ipv6.toString());
	ui->lbl_port->setText(QString::number(PORT));
	Qext::Object::connectMultipleSignals(
		server,
		{
			SIGNAL(socketConnected()),
			SIGNAL(socketDisconnected())
		},
		this,
		SLOT(handle_socket_onlineChanged())
	);
}

MainWindow::~MainWindow()
{
	delete ui;
	delete server;
//	server.close();
}


void MainWindow::on_pb_open_clicked(){
	server->open();
	ui->pb_open->setEnabled(false);
	ui->pb_close->setEnabled(true);
}

void MainWindow::on_pb_close_clicked(){
	server->close();
	ui->pb_open->setEnabled(true);
	ui->pb_close->setEnabled(false);
}

void MainWindow::closeEvent (QCloseEvent *event){
	server->close();
	QMainWindow::closeEvent(event);
}

void MainWindow::handle_socket_onlineChanged(){
	ui->lbl_online->setText(QString::number(server->totalOnline()));
}
