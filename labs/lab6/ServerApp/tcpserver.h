#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include "tcpsocket.h"

class TcpServer : public QTcpServer{
	Q_OBJECT
public:
	using QTcpServer::QTcpServer;

	TcpSocket * nextPendingTcpSocket();

	// QTcpServer interface
protected:
	void incomingConnection(qintptr socketDescriptor) override;
};

#endif // TCPSERVER_H
