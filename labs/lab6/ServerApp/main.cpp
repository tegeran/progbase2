#include "mainwindow.h"
#include <QApplication>
#include "provider.h"
#include <QDebug>

#include <QHostAddress>
#include <QNetworkInterface>


#include <std_ext.h>
#include <iostream>


int main(int argc, char *argv[]){
	QApplication a(argc, argv);
	a.setStyleSheet(
		"QWidget{ background-color: rgb(76, 75, 75); }"
		"QWidget:disabled{ 	color: rgb(154, 148, 148);  }"
		"QWidget:enabled {	 color: white;  }"
		"QPushButton:enabled {	background-color: rgb(34, 33, 33); }"
		"QPushButton:disabled {	background-color: rgb(46, 52, 54); }"
		"QPushButton:enabled:hover{ background-color: rgb(29, 14, 58); }"
		"QIcon:disabled{ color: black; background-color: black;}"
	);
	MainWindow w;
	w.show();
	return a.exec();
}
