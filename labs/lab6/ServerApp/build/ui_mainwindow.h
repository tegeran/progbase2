/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFormLayout *formLayout;
    QLabel *slbl_providererrServer;
    QLabel *slbl_runningConfig;
    QLabel *slbl_ipv4;
    QLabel *lbl_ipv4;
    QLabel *slbl_ipv6;
    QLabel *lbl_ipv6;
    QLabel *slbl_port;
    QLabel *lbl_port;
    QLabel *slbl_online;
    QHBoxLayout *horizontalLayout;
    QPushButton *pb_open;
    QPushButton *pb_close;
    QSpacerItem *horizontalSpacer;
    QLabel *lbl_online;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(500, 248);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMaximumSize(QSize(500, 248));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        formLayout = new QFormLayout(centralWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setLabelAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        slbl_providererrServer = new QLabel(centralWidget);
        slbl_providererrServer->setObjectName(QStringLiteral("slbl_providererrServer"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(slbl_providererrServer->sizePolicy().hasHeightForWidth());
        slbl_providererrServer->setSizePolicy(sizePolicy1);
        slbl_providererrServer->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(0, QFormLayout::SpanningRole, slbl_providererrServer);

        slbl_runningConfig = new QLabel(centralWidget);
        slbl_runningConfig->setObjectName(QStringLiteral("slbl_runningConfig"));
        sizePolicy1.setHeightForWidth(slbl_runningConfig->sizePolicy().hasHeightForWidth());
        slbl_runningConfig->setSizePolicy(sizePolicy1);
        slbl_runningConfig->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(1, QFormLayout::SpanningRole, slbl_runningConfig);

        slbl_ipv4 = new QLabel(centralWidget);
        slbl_ipv4->setObjectName(QStringLiteral("slbl_ipv4"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(slbl_ipv4->sizePolicy().hasHeightForWidth());
        slbl_ipv4->setSizePolicy(sizePolicy2);
        slbl_ipv4->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(2, QFormLayout::LabelRole, slbl_ipv4);

        lbl_ipv4 = new QLabel(centralWidget);
        lbl_ipv4->setObjectName(QStringLiteral("lbl_ipv4"));
        sizePolicy1.setHeightForWidth(lbl_ipv4->sizePolicy().hasHeightForWidth());
        lbl_ipv4->setSizePolicy(sizePolicy1);
        lbl_ipv4->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(2, QFormLayout::FieldRole, lbl_ipv4);

        slbl_ipv6 = new QLabel(centralWidget);
        slbl_ipv6->setObjectName(QStringLiteral("slbl_ipv6"));
        sizePolicy2.setHeightForWidth(slbl_ipv6->sizePolicy().hasHeightForWidth());
        slbl_ipv6->setSizePolicy(sizePolicy2);
        slbl_ipv6->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(3, QFormLayout::LabelRole, slbl_ipv6);

        lbl_ipv6 = new QLabel(centralWidget);
        lbl_ipv6->setObjectName(QStringLiteral("lbl_ipv6"));
        sizePolicy1.setHeightForWidth(lbl_ipv6->sizePolicy().hasHeightForWidth());
        lbl_ipv6->setSizePolicy(sizePolicy1);
        lbl_ipv6->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(3, QFormLayout::FieldRole, lbl_ipv6);

        slbl_port = new QLabel(centralWidget);
        slbl_port->setObjectName(QStringLiteral("slbl_port"));
        sizePolicy2.setHeightForWidth(slbl_port->sizePolicy().hasHeightForWidth());
        slbl_port->setSizePolicy(sizePolicy2);
        slbl_port->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(4, QFormLayout::LabelRole, slbl_port);

        lbl_port = new QLabel(centralWidget);
        lbl_port->setObjectName(QStringLiteral("lbl_port"));
        sizePolicy1.setHeightForWidth(lbl_port->sizePolicy().hasHeightForWidth());
        lbl_port->setSizePolicy(sizePolicy1);
        lbl_port->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(4, QFormLayout::FieldRole, lbl_port);

        slbl_online = new QLabel(centralWidget);
        slbl_online->setObjectName(QStringLiteral("slbl_online"));
        sizePolicy2.setHeightForWidth(slbl_online->sizePolicy().hasHeightForWidth());
        slbl_online->setSizePolicy(sizePolicy2);
        slbl_online->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(5, QFormLayout::LabelRole, slbl_online);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pb_open = new QPushButton(centralWidget);
        pb_open->setObjectName(QStringLiteral("pb_open"));
        pb_open->setEnabled(false);
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(pb_open->sizePolicy().hasHeightForWidth());
        pb_open->setSizePolicy(sizePolicy3);

        horizontalLayout->addWidget(pb_open);

        pb_close = new QPushButton(centralWidget);
        pb_close->setObjectName(QStringLiteral("pb_close"));
        sizePolicy3.setHeightForWidth(pb_close->sizePolicy().hasHeightForWidth());
        pb_close->setSizePolicy(sizePolicy3);

        horizontalLayout->addWidget(pb_close);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        formLayout->setLayout(6, QFormLayout::SpanningRole, horizontalLayout);

        lbl_online = new QLabel(centralWidget);
        lbl_online->setObjectName(QStringLiteral("lbl_online"));
        lbl_online->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(5, QFormLayout::FieldRole, lbl_online);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 500, 22));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        slbl_providererrServer->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:20pt; font-weight:600;\">Providererr server</span></p></body></html>", nullptr));
        slbl_runningConfig->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt;\">Running configuration</span></p></body></html>", nullptr));
        slbl_ipv4->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">IPv4 address:</span></p></body></html>", nullptr));
        lbl_ipv4->setText(QString());
        slbl_ipv6->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">IPv6 address:</span></p></body></html>", nullptr));
        lbl_ipv6->setText(QString());
        slbl_port->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Port:</span></p></body></html>", nullptr));
        lbl_port->setText(QString());
        slbl_online->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Online:</span></p></body></html>", nullptr));
        pb_open->setText(QApplication::translate("MainWindow", "Open", nullptr));
        pb_close->setText(QApplication::translate("MainWindow", "Close", nullptr));
        lbl_online->setText(QApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
