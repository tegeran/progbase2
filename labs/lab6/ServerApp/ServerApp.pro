#-------------------------------------------------
#
# Project created by QtCreator 2018-05-17T15:46:25
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ServerApp
TEMPLATE = app

CONFIG += c++2a
QMAKE_CXXFLAGS += -std=c++2a -Wall -Wextra -error -pedantic -Wno-variadic-macros

INCLUDEPATH += ../shared

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        server.cpp \
        tcpserver.cpp \

HEADERS += \
        mainwindow.h \
    ../common_libs/std_ext.h \
    ../common_libs/qext.h \
    ../common_libs/providerlib_global.h \
    ../common_libs/provider.h \
    ../common_libs/netpacket.h \
    ../common_libs/message_exception.h \
    ../common_libs/location.h \
    ../common_libs/error.h \
    ../common_libs/qnet.h \
    server.h \
    ../common_libs/session.h \
    ../common_libs/user.h \
    ../common_libs/datapacket.h \
    ../common_libs/client_session.h \
    ../common_libs/tcpsocket.h \
    tcpserver.h \
    ../common_libs/server_user.h

FORMS += \
        mainwindow.ui

