#include <QDir>

#include "server.h"
#include "qnet.h"
#include "qext.h"
#include "std_ext.h"
#include "netpacket.h"
#include "datapacket.h"


Server::Server(QObject * const & parent)
	: QObject(parent), lastSessionId(0)
{
	connect(
		&m_server,
		SIGNAL(newConnection()),
		this,
		SLOT(on_server_newConnection())
	);
	connect(
		&m_server,
		SIGNAL(acceptError(QAbstractSocket::SocketError)),
		this,
		SLOT(on_server_acceptError())
	);
}

Server::~Server(){
	close();
}

bool Server::open(const quint16 & port){
	QLOG("opening server");
	QSUPPOSE(!m_server.isListening());
	return m_server.listen(QHostAddress::Any, port);
}


void Server::close(){
	foreach_citer(constIterator, m_sessionsOnline){
		delete constIterator.value();
	}
	m_sessionsOnline.clear();

	while(!m_usersOnline.isEmpty()){
		m_usersOnline.begin().key()->close();
	}
	while(!m_unnamedUsers.isEmpty()){
		(*m_unnamedUsers.begin())->close();
	}
	if (m_server.isListening()){
		QLOG("closing server");
		m_server.close();
	}
}

bool Server::isOpened() const{
	return m_server.isListening();
}

void Server::on_server_newConnection(){
	TcpSocket * newSocket { m_server.nextPendingTcpSocket() };
	QSUPPOSE(newSocket && Qnet::isConnected(*newSocket));
	QLOG("received new connection from "
		 << newSocket->peerAddress().toString() << ":"
		 << newSocket->peerPort());

	m_unnamedUsers.insert(newSocket);
	connect(newSocket, SIGNAL(receivedNetPacket(NetPacket)),
			this,        SLOT(on_socket_receivedNetPacket(NetPacket)));

	connect(newSocket, SIGNAL(error(QAbstractSocket::SocketError)),
			this,        SLOT(on_socket_error()));

	connect(newSocket, SIGNAL(disconnected()),
			this,        SLOT(on_socket_disconnected()));
	emit socketConnected();
}

void Server::on_server_acceptError(){
	QLOG_ERROR("server accept error");
}

void Server::on_socket_receivedNetPacket(const NetPacket & netpacket){
	QLOG("new packet received");
	TcpSocket * socket { QSENDER(TcpSocket *) };
	try {
		switch (netpacket.type()){
			case NetPacket::Request_OnlineSessions:{
				processRequest_OnlineSessions(*socket);
				return;
			}
			case NetPacket::Request_SetUser: {
				processRequest_SetUser(*socket, netpacket);
				return;
			}
			case NetPacket::Request_CreateSession:{
				processRequest_CreateSession(*socket, netpacket);
				return;
			}
			case NetPacket::Request_ConnectToSession:{
				processRequest_ConnectToSession(*socket, netpacket);
				return;
			}
			case NetPacket::Request_SavedEntries:{
				processRequest_SavedEntries(*socket, netpacket);
				return;
			}
			default: return;
		}
	} catch (const MessageException & exception){
		emit error(exception.what());
		sendErrorReport(*socket, "undefined request, deserialization failure");
	}
}

void Server::on_socket_error(){
	TcpSocket * badSocket { QSENDER(TcpSocket *) };

	QLOG_ERROR("error: " << badSocket->errorString() << ", from "
			   << badSocket->peerAddress().toString() << ":"
			   << badSocket->peerPort());

	if (Qnet::isConnected(*badSocket))
		badSocket->close();
	emit error(badSocket->errorString());
}

void Server::on_socket_disconnected(){
	TcpSocket * socket { QSENDER(TcpSocket *) };
	if (m_unnamedUsers.remove(socket)){
		QLOG("unnamed user disconnected");
	} else {
		QSUPPOSE(m_usersOnline.contains(socket));
		ServerUser * accordingUser { m_usersOnline.take(socket) };
		if (accordingUser->isConnectedToSession()){
			accordingUser->session()->disconnectUser(socket);
		}
		QLOG("deleting disconnected user " << accordingUser->name() << "");
		delete accordingUser;
	}
	socket->deleteLater();
	emit socketDisconnected();
}

void Server::on_session_noUsersLeft(){
	Session * session { QSENDER(Session *) };
	m_sessionsOnline.take(session->id());
	sendDataToUsersOnline<DataPacket::Id>(
		NetPacket::Update_DeletedSession,
		session->id()
	);
	QLOG("no users left in session " << session->name() << ", removed it");
	delete session;
}

Session *Server::sessionFromId(const id_t & id) const{
	auto resultIterator { m_sessionsOnline.find(id) };
	return resultIterator == m_sessionsOnline.end()
		? nullptr
		: *resultIterator;
}


ServerUser * Server::userFromSocket(TcpSocket * const & socket) const{
	auto resultIterator { m_usersOnline.find(socket) };
	return resultIterator == m_usersOnline.end()
		? nullptr
		: *resultIterator;
}

void Server::processRequest_CreateSession(TcpSocket & sender, const NetPacket & netpacket){
	DataPacket::String packet{
		netpacket.dataPacket<DataPacket::String>()
	};

	ServerUser * user { userFromSocket(&sender) };
	if (!user){
		sendErrorReport(sender, "unnamed user can't create sessions");
		return;
	} else if (user->isConnectedToSession()){
		sendErrorReport(sender, "user is alredy connected to session");
		return;
	}
	Session * newSession {
		new Session(
		++lastSessionId,
		std::move(packet.string),
		&sender,
		user)
	};
	QLOG("created new sesion: " << newSession->name());
	m_sessionsOnline.insert(newSession->id(), newSession);
	sendDataToUsersOnlineExcept<DataPacket::SingleSession>(
			&sender,
			 NetPacket::Update_AddedSession,
			*static_cast<ClientSession *>(
			newSession)
	);
	connect(newSession, SIGNAL(noUsersLeft()), this, SLOT(on_session_noUsersLeft()));
}

void Server::processRequest_OnlineSessions(TcpSocket & sender){
	QTRACE_CALL();
	DataPacket::send<DataPacket::SessionsHashTable>(
			 sender ,
			 NetPacket::Answer_OnlineSessions,
			 m_sessionsOnline
	);
}

void Server::processRequest_SetUser(TcpSocket & sender, const NetPacket & netpacket){
	QTRACE_CALL();
	DataPacket::SingleUser request {
		netpacket.dataPacket<DataPacket::SingleUser>()
	};
	if (!m_unnamedUsers.remove(&sender)){
		sendErrorReport(sender, "username change request for named user");
	} else {
		ServerUser * newUser { new ServerUser(std::move(request.user))};
		m_usersOnline.insert(&sender, newUser);
		QLOG("registered new user under name " << newUser->name());
		DataPacket::send<DataPacket::SingleUser>(
			sender,
			NetPacket::Answer_SetUser,
			*static_cast<User *>(newUser)
		);
	}
}

void Server::processRequest_ConnectToSession(TcpSocket & senderSocket, const NetPacket & netpacket){
	QTRACE_CALL();
	ServerUser * senderUser { userFromSocket(&senderSocket) };
	if (!senderUser){
		sendErrorReport(senderSocket, "unnamed users can't connect to sessions");
		return;
	}

	DataPacket::Id packet { netpacket.dataPacket<DataPacket::Id>() };
	Session * requiredSession { sessionFromId(packet.id) };
	if (!requiredSession){
		sendErrorReport(senderSocket, "no reqiured session was found");
		return;
	}

	if (senderUser->isConnectedToSession()){
		senderUser->session()->disconnectUser(&senderSocket);
	}

	requiredSession->connectUser(&senderSocket, senderUser);
}



void Server::processRequest_SavedEntries(TcpSocket & sender, const NetPacket &){
	QTRACE_CALL();
	QDir dataDirectory(Session::DATA_DIRECTORY);
	DataPacket::send<DataPacket::StringVector>(
		sender,
		NetPacket::Answer_SavedEntries,
		dataDirectory.entryList(QDir::Files | QDir::Readable)
	);
}

void Server::sendErrorReport(TcpSocket & socket, const QString & reason){
	QLOG_ERROR(reason);
	DataPacket::send<DataPacket::String>(socket, NetPacket::Answer_Error, reason);
}


int Server::totalOnline() const{
	return m_usersOnline.size() + m_unnamedUsers.size();
}
