﻿#ifndef SESSION_H
#define SESSION_H

#include <QHash>
#include <QTcpSocket>
#include <QList>
#include <QObject>

#include "provider.h"
#include "std_ext.h"
#include "netpacket.h"
#include "datapacket.h"
#include "client_session.h"
#include "tcpsocket.h"
#include "server_user.h"

class Session : public QObject, public ClientSession{
	DECL_UNCOPIABLE(Session)
	Q_OBJECT
public:
	static constexpr const char * const DATA_DIRECTORY = "./data";

	Session() = delete;
	Session(const id_t & id,
			QString && name,
			TcpSocket * const & initiatorSocket,
			ServerUser * const & initiatorUser);
	~Session();
	const QVector<Provider> & providerVector() const;


signals:
	void noUsersLeft();

public slots:
	void connectUser(TcpSocket * const & userSocket, ServerUser * const & user);
	void disconnectUser(TcpSocket * const & userSocket);
private slots:
	void on_socket_receivedNetPacket(const NetPacket & packet);
private:

	QHash<TcpSocket *, ServerUser *> m_connectedUsers;
	QVector<Provider> m_providerVector;

	void processRequest_AddProvider(TcpSocket * const & senderSocket, const NetPacket & netpacket);
	void processRequest_DeleteProvider(TcpSocket * const & senderSocket, const NetPacket & netpacket);
	void processRequest_EditProvider(TcpSocket * const & senderSocket, const NetPacket & netpacket);
	void processRequest_LoadEntries(TcpSocket * const & senderSocket, const NetPacket & netpacket);
	void processRequest_DumpEntries(TcpSocket * const & senderSocket, const NetPacket & netpacket);
	void processRequest_ClearProviders(TcpSocket * const & senderSocket, const NetPacket & netpacket);


	void sendErrorReport(TcpSocket * const & dest, const QString & report);

	template <typename TDataPacket, typename... TPacketArgs>
	void sendToAllConnectedUsers(TPacketArgs &&... args);

	template <typename TDataPacket, typename... TPacketArgs>
	void sendToAllConnectedUsersExcept(TcpSocket * const & exception,
									   TPacketArgs &&... args);

	void connectMemberSignals(TcpSocket * const & sender);
};


template <typename TDataPacket, typename... TPacketArgs>
void Session::sendToAllConnectedUsers(TPacketArgs &&... args){
	NetPacket netpacket { TDataPacket::toNetPacket(std::forward<TPacketArgs>(args)...) };
	foreach_citer(iterator, m_connectedUsers){
		netpacket.send(*iterator.key());
	}
}

template <typename TDataPacket, typename... TPacketArgs>
void Session::sendToAllConnectedUsersExcept(TcpSocket * const & exception,
											TPacketArgs &&... args)
{
	NetPacket netpacket { TDataPacket::toNetPacket(std::forward<TPacketArgs>(args)...) };
	foreach_citer(iterator, m_connectedUsers){
		if (iterator.key() != exception)
			netpacket.send(*iterator.key());
	}
}











#endif // SESSION_H
