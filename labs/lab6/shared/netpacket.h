#pragma once
#ifndef NETPACKET_H
#define NETPACKET_H

#include <QTcpSocket>
#include <QJsonObject>

#include <utility>

#include "std_ext.h"
#include "error.h"
#include "user.h"


class NetPacket {
public:
	enum Type {
		Null,
		Update_UserDisconnected,    // User
		Update_UserConnected,       // User
		Update_AddedSession,        // Session
		Update_DeletedSession,      // Id

		Request_AddProvider = 5,    // ProviderAndIndex
		Answer_AddedProvider,       // ProviderAndIndex

		Request_EditProvider,       // ProviderAndIndex
		Answer_EditedProvider,      // ProviderAndIndex

		Request_DeleteProvider,		// Id
		Answer_DeletedProvider = 10,// Id

		Update_AddedProvider,       // ProviderAndIndex
		Update_EditedProvider,      // ProviderAndIndex
		Update_DeletedProvider,     // Id

		Request_SetUser,			// String
		Answer_SetUser = 15,		// SingleUser

		Request_CreateSession,		// String
		Answer_CreatedSession,		// ClientSession

		Request_LoadEntries,		// String
		Update_ReloadedEntries,		// ProviderVector

		Request_DumpEntries = 20,   // String
		Update_DumpedEntries,       // String

		Request_SavedEntries,		// TypeOnly
		Answer_SavedEntries,		// StringList

		Request_ClearProviders,     // TypeOnly
		Update_ClearedProviders= 25,// TypeOnly


		Request_OnlineSessions,	    // ProviderAndIndex
		Answer_OnlineSessions,      // SessionsHashTable

		Request_ConnectToSession,   // Id
		Answer_ConnectedToSession,  // SessionConnectionData

		Answer_Error,               // String

		Min = Null,          // private
		Max = Answer_Error   // private
	};

	NetPacket() = delete;
	NetPacket(QTcpSocket & socketToReadFrom);
	NetPacket(NetPacket && rvalue);
	NetPacket(const NetPacket & lvalue);
	NetPacket & operator=(NetPacket && rvalue);
	NetPacket(const Type & type, QJsonValue && dataJsonValue);

	bool isValid() const;
	void send(QTcpSocket & receiverSocket) const;

	template <typename TPacket>
	TPacket dataPacket() const; /* throws MessageException */

	const Type & type() const;

private:
	Type m_type;
	QJsonValue m_dataJsonValue;
};

template <typename TPacket>
TPacket NetPacket::dataPacket() const {
	QSUPPOSE(m_type != Null);
	return TPacket::fromJson(static_cast<const QJsonValue>(m_dataJsonValue));
}


#endif // NETPACKET_H
