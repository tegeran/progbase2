#include <QJsonObject>

#include "client_session.h"
#include "qext.h"
#include "std_ext.h"

#define ID_FIELDNAME "id"
#define NAME_FIELDNAME "name"
#define PROVIDERS_FIELDNAME "providers"

ClientSession::ClientSession(const id_t & id,
							 QString && name)
	: m_id(id), m_name(std::move(name)) {}

ClientSession::ClientSession(ClientSession && rvalue)
	: m_id(rvalue.m_id),
	  m_name(std::move(rvalue.m_name)){}

ClientSession::ClientSession(const ClientSession & lvalue)
	: m_id(lvalue.m_id),
	  m_name(lvalue.m_name) {}

ClientSession & ClientSession::operator=(ClientSession && rvalue){
	m_id = rvalue.m_id;
	m_name = std::move(rvalue.m_name);
	return *this;
}
ClientSession & ClientSession::operator=(const ClientSession & lvalue){
	m_id = lvalue.m_id;
	m_name = lvalue.m_name;
	return *this;
}


id_t ClientSession::id() const              { return m_id;   }
const QString & ClientSession::name() const { return m_name; }

void ClientSession::setId(const id_t & id)  { m_id = id;     }
void ClientSession::setName(QString && name){ m_name = name; }

bool ClientSession::isNull(){
	return m_id == 0;
}

bool ClientSession::isName(const QString & suspect){
	return Qext::isSpacedIdentifier(suspect);
}

QRegularExpression ClientSession::nameRegex(){
	return QRegularExpression(std_ext::SPACED_ID_REGEX);
}

QStringList ClientSession::sessionsToStringList(const QHash<id_t, ClientSession> & sessionsHashTable){
	QStringList sessionsNamesList;
	for (const ClientSession & session : sessionsHashTable){
		sessionsNamesList << session.name();
	}
	return std::move(sessionsNamesList);
}

ClientSession ClientSession::nullobj(){
	return ClientSession(0, "");
}

QJsonValue ClientSession::toJsonValue() const{
	return QJsonValue(QJsonObject(
	{
		Qext::makeQPair(QString(ID_FIELDNAME),   QJsonValue(
												static_cast<qint64>(m_id))
		),
		Qext::makeQPair(QString(NAME_FIELDNAME), QJsonValue(m_name))
	}));
}

ClientSession ClientSession::fromJsonValue(const QJsonValue & value){
	if (!value.isObject()){
		throw MessageException("client session is of non json object type");
	}
	QJsonObject sessionObj { value.toObject() };
	if (!Qext::Json::objectContains(sessionObj,
		{
			ID_FIELDNAME, NAME_FIELDNAME
		}))
	{
		throw MessageException("not all fields are present in client session json object");
	}

	QJsonValue idValue		  { sessionObj[ID_FIELDNAME]        };
	QJsonValue nameValue	  { sessionObj[NAME_FIELDNAME]      };
	if (!(idValue.isDouble() && nameValue.isString()))
	{
		throw MessageException("id value is of non-integral type");
	}

	int id { idValue.toInt() };
	if (!std_ext::isInRangeOf<id_t>(id) || id == 0){
		throw MessageException("invalid id received");
	}

	QString name {nameValue.toString()};

	if (!isName(name))
	{
		throw MessageException("invalid session name received");
	}

	return ClientSession(static_cast<id_t>(id),
						 std::move(name));
}













