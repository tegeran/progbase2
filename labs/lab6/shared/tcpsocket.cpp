#include "tcpsocket.h"
#include <QHostAddress>
#include <QTimer>

TcpSocket::TcpSocket(QObject * const & parent)
	: QTcpSocket(parent)
{
	connect(
		this,
		SIGNAL(readyRead()),
		this,
		SLOT(on_readyRead())
	);
	connect(
		this,
		&QAbstractSocket::stateChanged,
		[this](const QAbstractSocket::SocketState & state){
			QLOG("socket state changed to " << state);
		}
	);
	QLOG("socket created");
}

TcpSocket::~TcpSocket(){
	QLOG("socket destroyed");
}


void TcpSocket::on_readyRead(){
	do {
		QLOG("QTcpSocket::readyRead()~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		NetPacket packet {*static_cast<QTcpSocket *>(this)};
		if (packet.isValid()){
			emit receivedNetPacket(std::move(packet));
		}
	} while (QTcpSocket::bytesAvailable());
}
