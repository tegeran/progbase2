#ifndef USER_H
#define USER_H

#include <QJsonValue>
#include <QString>
#include <QRegularExpression>

#include "std_ext.h"
#include "message_exception.h"


class User{
public:
	explicit User(QString && name = "");
	User(const User & lvalue);
	User(User && rvalue);
	User & operator=(User && rvalue);
	User & operator=(const User & lvalue);

	virtual ~User() = default;

	const QString & name() const;
	void setName(QString && name);

	static QRegularExpression nameRegex();
	static bool isName(const QString & suspect);

	QJsonValue toJsonValue() const;
	static User fromJson(const QJsonValue & jsonValue); /* throws MessageException */

protected:
	QString m_name;
};

#endif // USER_H
