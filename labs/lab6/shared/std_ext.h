#ifndef STD_EXT
#define STD_EXT

#include <iterator>
#include <set>
#include <iostream>
#include <utility>
#include <limits>
#include <type_traits>

#include "error.h"

#define DECL_UNCOPIABLE(Type)				\
	Type(const Type &) = delete;			\
	Type & operator=(const Type &) = delete;

#define DECL_DEFAULT_COPYING(Type)				\
	Type(const Type &) = default;				\
	Type & operator=(const Type &) = default;	\

#define DECL_DEFAULT_MOVING(Type)			\
	Type(Type && ) = default;				\
	Type & operator=(Type &&) = default;	\

#define DECL_DEFAULT_COPY_AND_MOVE(Type) DECL_DEFAULT_MOVING(Type) DECL_DEFAULT_COPYING(Type)

#define DECL_DEFAULT_MOVE_ONLY(Type) DECL_DEFAULT_MOVING(Type) DECL_UNCOPIABLE(Type)

#define DEPRECATED __attribute((deprecated))
#define DEPRECATED_WHY(why) __attribute((deprecated(why)))

#define SIZEAT(arr) (sizeof((arr)) / sizeof((arr)[0]))
#define SKIP_IF(condition) {if ((condition)) {continue;}}

#define foreach_iter(it_id, container) \
	if(const auto & ____cont##it_id = container; true)\
	for(auto it_id(____cont##it_id.begin()); it_id != ____cont##it_id.end(); ++it_id)

#define foreach_citer(it_id, container)\
	if (const auto & ____cont##it_id = container; true)\
	for(auto it_id(____cont##it_id.cbegin()); it_id != ____cont##it_id.cend(); ++it_id)

#define foreach_riter(it_id, container) \
	if(const auto & ____cont##it_id = container; true)\
	for(auto it_id(____cont##it_id.rbegin()); it_id != ____cont##it_id.rend(); ++it_id)

#define foreach_criter(it_id, container)\
	if (const auto & ____cont##it_id = container; true)\
	for(auto it_id(____cont##it_id.crbegin()); it_id != ____cont##it_id.crend(); ++it_id)

namespace std_ext {
	constexpr const char * const SPACED_ID_REGEX = R"~~(^\s*(?:\w\s?)+$)~~";
	constexpr const char * const IPV4_ADDRESS_REGEX = R"~~(^(?:(?:\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.){3}(?:\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])$)~~";

	template <typename T, std::size_t N>
	constexpr std::size_t size(const T (&arr)[N]) noexcept{
		return N;
	}

	template <typename TLeft, typename TRight, typename TCommonSignedness>
	bool isLess_helper(const TLeft & left,
					   const TRight & right,
					   const  TCommonSignedness &,
					   const TCommonSignedness &)
	{
		return left < right;
	}

	template <typename TLeft, typename TRight>
	bool isLess_helper(const TLeft & left,
					   const TRight & right,
					   const std::false_type &,
					   const std::true_type)
	{
		return 0 < right && left < static_cast<std::make_unsigned_t<TRight>>(right);
	}	   // case right == 0 will always result in false

	template <typename TLeft, typename TRight>
	bool isLess_helper(const TLeft & left,
					   const TRight & right,
					   const std::true_type &,
					   const std::false_type)
	{
		return left < 0 || static_cast<std::make_unsigned_t<TLeft>>(left) < right;
	}

	template <typename TLeft, typename TRight>
	bool isLess(const TLeft & left, const TRight & right){
		return isLess_helper(
			left,
			right,
			std::is_signed<TLeft>(),
			std::is_signed<TRight>()
		);
	}


	template <typename TIter>
	void deletePtrs(TIter begin, const TIter & end){
		while (begin != end){
			Debug_exists(*begin);
			delete *begin;
			++begin;
		}
	}

	template <typename TContainer>
	void deletePtrs(TContainer & cont){
		for (auto & ptr : cont){
			Debug_exists(ptr);
			delete ptr;
		}
	}

	template <typename TIter>
	void deleteOverlappingPtrs(TIter begin, const TIter & end){
		if (begin == end) return;
		std::set<decltype(*begin)> set;
		while (begin != end){
			auto occurence = set.find(*begin);
			if (occurence == set.end()){
				Debug_exists(begin);
				delete *begin;
				set.insert(*begin);
			} // else already deleted
		}
	}

	template <typename TContainer>
	void deleteOverlappingPtrs(TContainer & cont){
		deleteOverlappingPtrs(cont.begin(), cont.end());
	}

	template <typename A, typename B, typename C>
	bool inbound(const A &suspect, const B &left, const C &right){
		return suspect >= left && suspect <= right;
	}

	const char * toString(bool boolean);

	template <typename TContainer, typename TNumeric>
	bool hasIndex(const TContainer & cont, const TNumeric &index){
		return isLess(index, cont.size()) && index >= 0;
	}

	template <typename TLimit, typename TValue>
	TValue maxmap(const TValue &value, const TLimit &max){
		return value > max ? max : value;
	}

	template <typename TLimit, typename TValue>
	TValue minmap(const TValue &value, const TLimit &min){
		return value < min ? min : value;
	}

	template <typename TLimits, typename TValue>
	TLimits map(const TValue &value, const TLimits &left, const TLimits &right){
		return value > right
				? right
				: value < left
					? left
					: value;
	}

	int doubleCompare(const double &left, const double &right, const double &epsilon = 1e-6);
	bool doubleEquals(const double &left, const double &right, const double &epsilon = 1e-6);

	template <typename TContainer>
	void print(const TContainer & cont){
		if (cont.cbegin() == cont.cend()){ return; }
		std::cout << "[" << *cont.cbegin();
		for (auto it = cont.cbegin() + 1; it != cont.cend(); ++it) {
			std::cout << ", " << *it;
		}
		std::cout << ']';
	}

	template <typename T, std::size_t N>
	void print(const T (&arr)[N]){
		if (N == 0) { return; }
		std::cout << "[" << arr[0];
		const T * end = arr + N;
		for (const T * it = arr + 1; it != end; ++it) {
			std::cout << ", " << *it;
		}
		std::cout << ']';
	}

	template <typename TContainer, typename TValue>
	auto find(const TContainer & container, const TValue & value){
		return std::find(container.begin(), container.end(), value);
	}

	template <typename TContainer, typename TValue>
	bool contains(const TContainer & container, const TValue & value){
		return std::find(container.begin(), container.end(), value) != container.end();
	}

	template <typename TMinMaxEnum, typename TIntegral>
	inline bool enumContains(const TIntegral & suspect){
		return inbound(suspect, TMinMaxEnum::Min, TMinMaxEnum::Max);
	}

	//    template <typename TIter, typename TMember>
	//	void deleteOverlappingPtrs(TIter begin, const TIter & end, TMember delField){
	//        if (begin == end) return;
	//        std::set<decltype(*begin)> set;
	//        while (begin != end){
	//            auto occurence = set.find(begin->*delField);
	//            if (occurence == set.end()){
	//                Debug_exists(begin->*delField);
	//                delete begin->*delField;
	//                set.insert(*begin->*delField);
	//            } // else already deleted
	//        }
	//    }

	//    template <typename TContainer, typename TMember>
	//	void deleteOverlappingPtrs(TContainer & cont, TMember delField){
	//		deleteOverlappingPtrs(cont.begin(), cont.end(), delField);
	//    }

	template <typename TRange, typename TSuspect, typename TCommonSignedness>
	bool isInRangeOfHelper(const TSuspect & suspect,
						 const TCommonSignedness &,
						 const TCommonSignedness &)
	{
		return std::numeric_limits<TRange>::min() <= suspect
			 && suspect <= std::numeric_limits<TRange>::max(); // both are signed or unsigned
	}

	template <typename TRange, typename TSuspect>
	bool isInRangeOfHelper(const TSuspect & suspect, const std::true_type &, const std::false_type &)
	{
		return suspect <= static_cast<std::make_unsigned_t<TRange>>(
						  std::numeric_limits<TRange>::max() // suspect is unsigned, range is signed
		);
	}

	template <typename TRange, typename TSuspect>
	bool isInRangeOfHelper(const TSuspect & suspect, const std::false_type &, const std::true_type &)
	{
		return 0 <= suspect // range is unsigned, suspect is signed
				&& static_cast<std::make_unsigned_t<TSuspect>>(suspect) <= std::numeric_limits<TRange>::max();
	}

	template <typename TRange, typename TSuspect, typename TFloatOrInt, typename TFloatOrInt2>
	bool isInRangeOf_integralCheckHelper(const TSuspect & suspect,
										 const TFloatOrInt  &,
										 const TFloatOrInt2 &){
		return std::numeric_limits<TRange>::min() <= suspect // one of the values is floating point
				&& suspect <= std::numeric_limits<TRange>::max();
	}


	template <typename TRange, typename TSuspect>
	bool isInRangeOf_integralCheckHelper(const TSuspect & suspect,
										 const std::true_type &,
										 const std::true_type &){
		return isInRangeOfHelper<TRange>(
			suspect,
			std::is_signed<TRange>(),      // both are integral types
			std::is_signed<TSuspect>()
		);
	}

	template <typename TRange, typename TSuspect>
	bool isInRangeOf(const TSuspect & suspect){
		return isInRangeOf_integralCheckHelper<TRange>(
			suspect,
			typename std::is_integral<TRange>::type(),
			typename std::is_integral<TSuspect>::type()
		);
	}

	template <typename TRange>
	bool isInRangeOf(const TRange & suspect){ return true; }
}



#endif // STD_EXT
