#ifndef QNET_H
#define QNET_H
#include <QAbstractSocket>
#include <QNetworkSession>
#include <QSettings>
#include <QByteArray>
#include <QDataStream>
#include <QTcpServer>
#include <QTcpSocket>

#include "error.h"

namespace Qnet {
	void deleteOnDisconnect(const QAbstractSocket & sock);
	// stolen from QtExamples manual on networking
	QNetworkSession * establishNewSession(QSettings & settings,
										  QObject * const &parent = nullptr,
										  const int & msecWait = 5000);

	QNetworkSession * establishNewSession(QSettings && settings,
										  QObject * const &parent = nullptr,
										  const int & msecWait = 5000);


	bool isConnected(const QTcpSocket & suspect);
	bool canWriteTo(const QTcpSocket & suspect);
	bool isUnconnected(const QTcpSocket & suspect);



	template <typename... T>
	void send(QTcpSocket & socket, const T &... data);

	struct LocalAddresses {
		QHostAddress ipv4;
		QHostAddress ipv6;
		LocalAddresses();
	};

	/* Private helpers: */
	template <typename T>
	void sendRecursiveHelper(QDataStream & stream, const T & data);

	template <typename TCurrent, typename... T>
	void sendRecursiveHelper(QDataStream & stream, const TCurrent & data, const T &... remainingData);

	template <typename... T>
	void send(QTcpSocket & socket, const T &... data){
		QByteArray dataArray;
		QDataStream dataStream(&dataArray, QIODevice::WriteOnly);
		sendRecursiveHelper(dataStream, data...);
		socket.write(dataArray);
	}

	template <typename TCurrent, typename... T>
	void sendRecursiveHelper(QDataStream & stream, const TCurrent & data, const T &... remainingData){
		stream << data;
		sendRecursiveHelper(stream, remainingData...);
	}

	template <typename T>
	void sendRecursiveHelper(QDataStream & stream, const T & data){ /* recursion base condition */
		stream << data;
	}
}






#endif // QNET_H
