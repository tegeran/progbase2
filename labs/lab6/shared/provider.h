#ifndef PROVIDER_H
#define PROVIDER_H
#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QVector>
#include "message_exception.h"
#include <queue>
#include <vector>
#include <QByteArray>
#include <QDataStream>
#include <QMetaType>
#include "providerlib_global.h"

class PROVIDERLIBSHARED_EXPORT Provider {
public:
	class DataCloud {
	public:
		explicit DataCloud(const double &tariff = 0, const double &capacity = -1);

		double tariff() const;
		double capacity() const;
		void setTariff(const double & tariff);
		void setCapacity(const double & capacity);

		bool isNull() const;
		static DataCloud nullobj();

		QJsonValue toJsonValue() const;
		static DataCloud fromJsonValue(const QJsonValue & value); /* throws MessageException */

		bool operator==(const DataCloud & suspect) const;
		friend QDataStream & operator<<(QDataStream & stream, const DataCloud & cloud);
		friend QDataStream & operator>>(QDataStream & stream, DataCloud & cloud);

		static bool isTariff(const double & suspect);
		static bool isCapacity(const double & suspect);
	private:
		double m_tariff;
		double m_capacity;
	};

	explicit Provider (
			  QString name = "",
			  double tariff = 0,
			  double loss = 0,
			  int users = 0,
			  const DataCloud & cloud = DataCloud::nullobj()
	);

	Provider(const Provider & toCopy);
	Provider(Provider && toMove);
	Provider & operator=(const Provider & other);
	Provider & operator=(Provider && other);

	bool hasCloud() const;

	const QString & name() const;
	void setName(const QString & name);
	void setName( QString && name);

	double tariff() const;
	void setTariff(double tariff);

	double loss() const;
	void setLoss(double loss);

	int users() const;
	void setUsers(int users);

	DataCloud cloud() const;
	void setCloud(const DataCloud & cloud);

	friend QDataStream &operator<<(QDataStream & stream, const Provider & prov);
	friend QDataStream &operator>>(QDataStream & stream, Provider & prov);

	QJsonObject toJsonObject() const;
	static QJsonArray toJsonArr(const QVector<Provider> & self);
	static void toJsonFile(const QVector<Provider> & vector, const QString & path);

	static Provider fromJsonObject(const QJsonObject & obj);      /* throws MessageException */
	static QVector<Provider> fromJsonArr(const QJsonArray & arr); /* throws MessageException */
	static QVector<Provider> fromJsonFile(const QString & path);  /* throws MessageException */

	static bool isName(const QString & suspect);
	static bool isTariff(const double & suspect);
	static bool isLoss(const double & suspect);
	static bool isUsers(const int & suspect);

	static bool compareTariff(const Provider & left, const Provider & right);

	typedef bool (*ComparTariff)(Provider * const & left, Provider * const & right);
	typedef std::priority_queue<Provider *, std::vector<Provider *>, ComparTariff>
			PriorityQueByTariff;
	static PriorityQueByTariff toStdPriorityQueueByTariff(QVector<Provider> & vector);

	bool operator==(const Provider & suspect);
private:
	QString m_name;
	double m_tariff;
	double m_loss;
	int m_users;
	DataCloud m_cloud;
};

Q_DECLARE_METATYPE(Provider)
Q_DECLARE_METATYPE(Provider::DataCloud)
Q_DECLARE_METATYPE(QVector<Provider>)

#endif // PROVIDER_H
