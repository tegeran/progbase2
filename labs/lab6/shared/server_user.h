#ifndef SERVER_USER_H
#define SERVER_USER_H

#include "user.h"

class Session; /* forward declaration */

class ServerUser : public User{
public:
    explicit ServerUser(QString && name, Session * const & session = nullptr);
    ServerUser(ServerUser && rvalue);
    ServerUser(const ServerUser & lvalue);
    ServerUser & operator=(ServerUser && rvalue);
	ServerUser & operator=(ServerUser & lvalue) = delete;
	ServerUser(User && user, Session * const & session = nullptr);

    Session * const & session() const;
    void setSession(Session * const & session);

    bool isConnectedToSession() const;


private:
    Session * m_session;
};

#endif // SERVER_USER_H
