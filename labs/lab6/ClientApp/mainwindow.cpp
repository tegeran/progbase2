#include <QMessageBox>
#include <QFileDialog>
#include <QTime>
#include <QApplication>

#include <utility>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "error.h"
#include "provider_dialog.h"
#include "commands.h"
#include "qext.h"
#include "user_name_dialog.h"
#include "sessions_list_dialog.h"
#include "entries_dialog.h"
#include "entry_dump_dialog.h"

#define STATUS_BAR_MESSAGE_DURATION 10000

MainWindow::MainWindow(Client * const & client,
					   QVector<Provider> && providers,
					   const QVector<User> & connectedUsers,
					   QWidget * parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	ustack(this),
	model(new ProviderListModel(this)),
	proxyModel(new ProviderFilterProxyModel(QSet<Provider *>(), this)),
	currentFilter(0),
	client(client)
{
	ui->setupUi(this);
	setWindowTitle("Providererr");
	setupRedosUndos();
	setupProviderModel(std::move(providers));
	setupListViews(connectedUsers);
	setupClient();
}

MainWindow::~MainWindow(){
	delete ui;
}

QString MainWindow::globalStyleSheet(){
	return  "QWidget{ background-color: rgb(76, 75, 75); }"
			"QWidget:disabled{ 	color: rgb(154, 148, 148);  }"
			"QWidget:enabled {	 color: white;  }"
			"QPushButton:enabled {	background-color: rgb(34, 33, 33); }"
			"QPushButton:disabled {	background-color: rgb(46, 52, 54); }"
			"QPushButton:enabled:hover{ background-color: rgb(29, 14, 58); }"
			"QIcon:disabled{ color: black; background-color: black;}";

}

void MainWindow::handle_model_dataChanged(QModelIndex, QModelIndex, QVector<int>){
	updateLables();
	updateProxyModel();
}



void MainWindow::on_pb_add_clicked(){
	ProviderDialog dial;
	if (dial.exec() == QDialog::Accepted){
		int selected(current());
		ustack.push(
			new Command::Add(
				*this,
				std::move(dial.provider()),
				selected < 0 ? model->providers().size() : selected
			)
		);
	}
}

void MainWindow::on_pb_delete_clicked(){
	ustack.push(new Command::Delete(*this, current()));
}

void MainWindow::on_pb_edit_clicked(){
	int selected(current());
	ProviderDialog dial(model->providers()[selected]);
	if (dial.exec() == QDialog::Accepted){
		ustack.push(new Command::Edit(
						*this,
						std::move(dial.provider()),
						selected
					)
		);
	}
}
void MainWindow::on_lv_names_doubleClicked(const QModelIndex &index){
	if (index.isValid()){
		on_pb_edit_clicked();
	}
}
void MainWindow::on_lv_execs_doubleClicked(const QModelIndex &index){
	if (index.isValid()){
		on_pb_edit_clicked();
	}
}

void MainWindow::handle_lv_names_currentChanged(const QModelIndex &, const QModelIndex &){
	QLOG("NAMES SELECTED");
	syncSelections();
	updateLables();
	updateButtons();
}

void MainWindow::handle_lv_execs_currentChanged(const QModelIndex & cur, const QModelIndex &){
	QLOG("EXECS SELECTED");
	if (cur.isValid()){
		ui->lv_names->setCurrentIndex(proxyModel->mapToSource(cur));
	}
}


void MainWindow::on_pb_execute_clicked(){
	int filter(ui->sbox_exec->value());
	setFilter(filter);
	ui->pb_execute->setEnabled(false);
}

void MainWindow::on_act_open_triggered(){
	if (QMessageBox::question(
			this,
			tr("Are you sure?"),
			tr("Are you sure you want to open new file?\n"
			   "All unsaved providers will be lost"),
			QMessageBox::Ok | QMessageBox::Cancel
		) == QMessageBox::Cancel
	){
		return;
	}
	connect(
		client,
		SIGNAL(receivedSavedEntries(QStringList)),
		this,
		SLOT(handle_client_receivedSavedEntriesForOpen(QStringList))
	);
	client->requireSavedEntries();
}

void MainWindow::on_act_saveTo_triggered(){
	connect(
		client,
		SIGNAL(receivedSavedEntries(QStringList)),
		this,
		SLOT(handle_client_receivedSavedEntriesForDump(QStringList))
	);
	client->requireSavedEntries();
}


void MainWindow::handle_model_rowsInserted(const QModelIndex &, int, int){
	updateButtons();
	//
	ui->sbox_exec->setMaximum(model->rowCount());
	syncSelections();
}

void MainWindow::handle_model_rowsDeleted(const QModelIndex &, int, int){
	updateButtons();
	updateLables();
	ui->sbox_exec->setMaximum(model->rowCount());
	syncSelections();
}

void MainWindow::handle_model_reseted(){
	updateButtons();
	updateLables();
	ui->sbox_exec->setMaximum(model->rowCount());
}

void MainWindow::handle_lv_names_indexesMoved(){
	updateProxyModel();
}

void MainWindow::on_sbox_exec_valueChanged(int value){
	ui->pb_execute->setEnabled(currentFilter != value);
}

void MainWindow::handle_client_receivedSavedEntriesForOpen(const QStringList & entriesNames){
	disconnect(
		client,
		SIGNAL(receivedSavedEntries(QStringList)),
		this,
		SLOT(handle_client_receivedSavedEntriesForOpen(QStringList))
	);
	EntriesDialog entriesDialog { entriesNames };
	connect(
		client,
		SIGNAL(updateDumpedEntries(QString)),
		&entriesDialog,
		SLOT(tryAddEntry(QString))
	);
	if (entriesDialog.exec() == QDialog::Rejected){
		return;
	}
	client->requireLoadEntries(entriesDialog.selectedEntry());

}

void MainWindow::handle_client_receivedSavedEntriesForDump(const QStringList & entriesNames){
	disconnect(
		client,
		SIGNAL(receivedSavedEntries(QStringList)),
		this,
		SLOT(handle_client_receivedSavedEntriesForDump(QStringList))
	);
	EntryDumpDialog entryDumpDialog { entriesNames };
	if (entryDumpDialog.exec() == QDialog::Rejected){
		return;
	}
	client->requireDumpEntries(entryDumpDialog.chosenEntry());
}

void MainWindow::handle_client_updateReloadedEntries(QVector<Provider> providers){
	model->setProviders(std::move(providers));
}

void MainWindow::handle_client_userConnected(const User & user){
	Qext::StringListModel::prependString(usersOnlineModel, user.name());
}

void MainWindow::handle_client_userDisconnected(const User & user){
	if (Qext::StringListModel::removeString(usersOnlineModel, user.name())){
		QLOG("disconnected user " << user.name() << " from GUi");
	} else {
		QLOG_ERROR("failed to disconnect user " << user.name() << " from GUI");
	}
}

void MainWindow::handle_client_updateEditedProvider(Provider provider, const id_t & position){
	if (!std_ext::hasIndex(model->providers(), position)){
		QLOG_ERROR("failed to edit provider at position " << position);
		return;
	}
	model->setProviderAt(position, std::move(provider));
}

void MainWindow::on_act_clear_triggered(){
	if (QMessageBox::question(this, tr("Are you sure?"),
							  tr("Are you sure you want to clear current project?\n"
								 "All unsaved data will be lost."),
							  QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok)
	{
		client->requireClearProviders();
	}
}



// METHODS =======================================================================

void MainWindow::insertProvider(const int & index, Provider && prov){
	Debug_maxindex(index, model->rowCount());
	model->insertProviderAt(index, std::move(prov));
	updateProxyModel();
}

void MainWindow::deleteProvider(const int & index){
	Debug_maxindex(index, model->rowCount() - 1);
	model->removeRow(index);
	if (currentFilter > model->rowCount())
		setFilter(model->rowCount());
	else
		updateProxyModel();
}

void MainWindow::setStatus(const QString & status){
	ui->statusBar->showMessage(status, STATUS_BAR_MESSAGE_DURATION);
}

QStatusBar & MainWindow::sbar(){
	return *ui->statusBar;
}

int MainWindow::current() const {
	QModelIndex ind(ui->lv_names->currentIndex());
	return ind.isValid() ? ind.row() : -1;
}

void MainWindow::updateLables() {
	int cur(current());
	if (cur == -1){
		Qext::Label::clearLabels(
		{
			 ui->lbl_name,
			 ui->lbl_tariff,
			 ui->lbl_users,
			 ui->lbl_loss,
			 ui->lbl_cloudCapacity,
			 ui->lbl_cloudTariff
		});
		ui->gbox_provider->setEnabled(false);
		return;
	}
	ui->gbox_provider->setEnabled(true);
	const Provider & prov = model->providers()[cur];
	ui->lbl_name->setText(prov.name());
	ui->lbl_tariff->setText(tr("%1$").arg(prov.tariff()));
	ui->lbl_loss->setText(tr("%1%").arg(prov.loss()));
	ui->lbl_users->setText(QString::number(prov.users()));
	if (prov.hasCloud()){
		ui->gbox_cloud->setEnabled(true);
		ui->lbl_cloudTariff->setText(tr("%1$").arg(prov.cloud().tariff()));
		ui->lbl_cloudCapacity->setText(tr("%1Tb").arg(prov.cloud().capacity()));
	} else {
		ui->gbox_cloud->setEnabled(false);
		Qext::Label::clearLabels(
		{
			ui->lbl_cloudCapacity, ui->lbl_cloudTariff
		});
	}
}

void MainWindow::updateProxyModel(){
	if (currentFilter == 0){
		proxyModel->setSet(QSet<Provider *>());
		return;
	}
	Provider::PriorityQueByTariff que = Provider::toStdPriorityQueueByTariff(model->providers());
	proxyModel->setSet(std::move(Qext::Set::fromStdPriorityQueue(que, currentFilter)));
	proxyModel->sort(0, Qt::DescendingOrder);
}

void MainWindow::setFilter(const int & filter){
	ui->lbl_filter->setText(tr("Current tariff filter: %1").arg(currentFilter = filter));
	updateProxyModel();
	syncSelections();
}

void MainWindow::updateButtons(){
	bool b(current() != -1);
	ui->pb_delete->setEnabled(b);
	ui->pb_edit->setEnabled(b);
}

void MainWindow::syncSelections(){
	QModelIndex	sourceInd(ui->lv_names->currentIndex());
	if (sourceInd.isValid()){
		QModelIndex proxyIndex(proxyModel->mapFromSource(sourceInd));
		if (proxyIndex.isValid()){
			ui->lv_execs->setCurrentIndex(proxyIndex);
			return;
		}
	}
	ui->lv_execs->selectionModel()->clearCurrentIndex();
	ui->lv_execs->clearSelection();
}

void MainWindow::setupRedosUndos(){
	ustack.setUndoLimit(5000);

	QAction * undo(ustack.createUndoAction(this));
	QAction * redo(ustack.createRedoAction(this));
	ui->menu_edit->insertAction(ui->act_tmpsave, redo);
	ui->menu_edit->insertAction(redo, undo);
	ui->mainToolBar->addAction(undo);
	ui->mainToolBar->addAction(redo);
	//
	undo->setIcon(QIcon(":/general/icons/backward"));
	redo->setIcon(QIcon(":/general/icons/forward"));

	undo->setShortcut(QKeySequence(Qt::Key_Z | Qt::CTRL));
	redo->setShortcut(QKeySequence(Qt::Key_Y | Qt::CTRL));
	//
	ui->act_saveTo->setShortcut(QKeySequence::SaveAs);
	ui->act_open->setShortcut(QKeySequence::Open);
}

void MainWindow::setupProviderModel(QVector<Provider> && providers){
	connect(
		model,
		SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
		this,
		SLOT(handle_model_dataChanged(QModelIndex, QModelIndex, QVector<int>))
	);
	connect(
		model,
		SIGNAL(rowsInserted(QModelIndex,int,int)),
		this,
		SLOT(handle_model_rowsInserted(QModelIndex,int,int))
	);
	connect(
		model,
		SIGNAL(rowsRemoved(QModelIndex,int,int)),
		this,
		SLOT(handle_model_rowsDeleted(QModelIndex,int,int))
	);
	connect(
		model,
		SIGNAL(modelReset()),
		this,
		SLOT(handle_model_reseted())
	);
	connect(
		model,
		SIGNAL(rowsRemoved(QModelIndex,int,int)),
		this,
		SLOT(handle_lv_names_indexesMoved())
	);
	model->setProviders(std::move(providers));
}

void MainWindow::setupListViews(const QVector<User> & connectedUsers){
	ui->lv_names->setModel(model);
	proxyModel->setSourceModel(model);
	proxyModel->setDynamicSortFilter(true);
	ui->lv_execs->setModel(proxyModel);
	ui->sbox_exec->setMaximum(model->rowCount());
	QStringList userNamesList;
	userNamesList.reserve(connectedUsers.size());
	for (const User & user : connectedUsers){
		userNamesList << user.name();
	}
	usersOnlineModel.setStringList(userNamesList);

	connect(
		ui->lv_names->selectionModel(),
		SIGNAL(currentChanged(QModelIndex,QModelIndex)),
		this,
		SLOT(handle_lv_names_currentChanged(QModelIndex,QModelIndex))
	);
	connect(
		ui->lv_execs->selectionModel(),
		SIGNAL(currentChanged(QModelIndex,QModelIndex)),
		this,
		SLOT(handle_lv_execs_currentChanged(QModelIndex,QModelIndex))
	);
}

void MainWindow::setupClient(){
	ui->lv_usersOnline->setModel(&usersOnlineModel);
	ui->lv_usersOnline->setEditTriggers(QListView::NoEditTriggers);
	ui->lv_usersOnline->setSelectionMode(QAbstractItemView::NoSelection);
	connect(
		client,
		SIGNAL(updateReloadedEntries(QVector<Provider>)),
		this,
		SLOT(handle_client_updateReloadedEntries(QVector<Provider>))
	);
	connect(
		client,
		SIGNAL(updateUserConnected(User)),
		this,
		SLOT(handle_client_userConnected(User))
	);
	connect(
		client,
		SIGNAL(updateUserDisconnected(User)),
		this,
		SLOT(handle_client_userDisconnected(User))
	);
	connect(
		client,
		&Client::successfullyAddedProvider,
		[this](Provider provider, const id_t & index){
			this->insertProvider(index, std::move(provider));
			this->setStatus("Successfully added provider to server");
		}
	);
	connect(
		client,
		&Client::successfullyDeletedProvider,
		[this](const id_t & index){
			this->deleteProvider(index);
			this->setStatus("Successfully deleted provider from server");
		}
	);
	connect(
		client,
		&Client::successfullyEditedProvider,
		[this](Provider provider, const id_t & index){
			handle_client_updateEditedProvider(std::move(provider), index);
			this->setStatus("Successfully edited provider at server");
		}
	);
	connect(
		client,
		&Client::updateAddedProvider,
		[this](Provider provider, const id_t & position){
			this->insertProvider(position, std::move(provider));
		}
	);
	connect(
		client,
		&Client::updateDeletedProvider,
		[this](const id_t & position){
			this->deleteProvider(position);
		}
	);
	connect(
		client,
		SIGNAL(updateEditedProvider(Provider,id_t)),
		this,
		SLOT(handle_client_updateEditedProvider(Provider,id_t))
	);
	connect(
		client,
		&Client::updateClearedProviders,
		[this](){ this->model->clearProviders(); }
	);
}


