﻿#ifndef CLIENT_H
#define CLIENT_H

#include <QTcpSocket>
#include <QHostAddress>
#include <QHash>
#include <QStringList>

#include "user.h"
#include "client_session.h"
#include "std_ext.h"
#include "netpacket.h"
#include "datapacket.h"
#include "tcpsocket.h"

#define SERVER_ADDRESS "127.0.0.1"
#define SERVER_PORT	   2000

class Client : public QObject {
	DECL_UNCOPIABLE(Client)
	Q_OBJECT
public:
	Client();

	void connectToServer(const QHostAddress & address = QHostAddress(SERVER_ADDRESS),
						 const quint16 & port = SERVER_PORT);

	void disconnect();
	const User & user() const;
	bool isConnected() const;

	const TcpSocket & socket() const;
	const ClientSession & session() const;
	QHostAddress serverAddress() const;

signals:	
	void establishedConnection();
	void connectedToSession(ClientSession session,
							QVector<Provider> providers,
							QVector<User> usersOnline);
	void error(QString reason);

	void establishedSelfUser();
	void receivedOnlineSessionsHashTable(QHash<id_t, ClientSession> sessions);
	void receivedSavedEntries(QStringList filenames);

	void updateClearedProviders();
	void createdSession(ClientSession session);
	void updateAddedSession(ClientSession session);
	void updateDeletedSession(id_t sessionId);

	void updateReloadedEntries(QVector<Provider> providers);

	void updateAddedProvider(Provider provider, id_t index);
	void successfullyAddedProvider(Provider provider, id_t index);

	void updateEditedProvider(Provider provider, id_t index);
	void successfullyEditedProvider(Provider provider, id_t index);

	void updateDeletedProvider(id_t index);
	void successfullyDeletedProvider(id_t index);

	void updateUserConnected(User user);
	void updateUserDisconnected(User user);

	void updateDumpedEntries(QString filename);

	void disconnectedFromServer();
public slots:
	void errorAbort(const QString & reason); // aborts connection and emits "reason" error signal
	void requireOnlineSessionsHashTable();
	void requireNewSession(const QString & name);
	void setUser(const User & user);
	void requireSessionConnection(const id_t & session);
	void requireSavedEntries();
	void requireDumpEntries(const QString & filename);
	void requireLoadEntries(const QString & filename);
	void requireAddProvider(const Provider & provider, const id_t & position);
	void requireDeleteProvider(const id_t & position);
	void requireEditProvider(const Provider & provider, const id_t & position);
	void requireClearProviders();
private slots:
	void on_socket_connected();
	void on_socket_error();
	void on_socket_disconnected();
	void on_socket_receivedNetPacket(const NetPacket & netpacket);
private:
	TcpSocket m_socket;
	User m_user;
	ClientSession m_session;

	void processAnswer_Error(const NetPacket & netpacket);

	void processAnswer_OnlineSessions(const NetPacket & netpacket);
	void processAnswer_CreatedSession(const NetPacket & netpacket);
	void processAnswer_ConnectedToSession(const NetPacket & netpacket);
	void processAnswer_SetUser(const NetPacket & netpacket);
	void processAnswer_SavedEntries(const NetPacket & netpacket);

	void processUpdate_AddedSession(const NetPacket & netpacket);
	void processUpdate_DeletedSession(const NetPacket & netpacket);
	void processUpdate_AddedProvider(const NetPacket & netpacket);
	void processUpdate_EditedProvider(const NetPacket & netpacket);
	void processUpdate_DeletedProvider(const NetPacket & netpacket);

	void processAnswer_AddedProvider(const NetPacket & netpacket);
	void processAnswer_EditedProvider(const NetPacket & netpacket);
	void processAnswer_DeletedProvider(const NetPacket & netpacket);

	void processUpdate_UserConnected(const NetPacket & netpacket);
	void processUpdate_UserDisconnected(const NetPacket & netpacket);

	void processUpdate_ReloadedEntries(const NetPacket & netpacket);
	void processUpdate_DumpedEntries(const NetPacket & netpacket);
	void processUpdate_ClearedProviders(const NetPacket & netpacket);


	template <typename TDataPacket, typename... TArgs>
	void sendToServer(TArgs &&... args);
};



template<typename TDataPacket, typename... TArgs>
void Client::sendToServer(TArgs &&... args){
	DataPacket::send<TDataPacket>(m_socket, std::forward<TArgs>(args)...);
}

#endif // CLIENT_H
