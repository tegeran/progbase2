#ifndef CLIENTSESSIONSLISTMODEL_H
#define CLIENTSESSIONSLISTMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include "client_session.h"

class ClientSessionsListModel : public QAbstractListModel{
	Q_OBJECT
public:
	explicit ClientSessionsListModel(QObject *parent = nullptr);

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Add data:
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	// Remove data:
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	ClientSession removeSession(const id_t &id);
	void addSession(ClientSession && session);
	void setSessions(const QHash<id_t, ClientSession> & sessions);

	const QVector<ClientSession> sessions() const;
private:
	QVector<ClientSession> m_sessions;
};

#endif // CLIENTSESSIONSLISTMODEL_H
