﻿#include <algorithm>

#include "sessions_list_dialog.h"
#include "ui_sessions_list_dialog.h"
#include "qext.h"
#include "session_name_dialog.h"

SessionsListDialog::SessionsListDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SessionsListDialog)
{
	ui->setupUi(this);
	ui->pb_connect->setEnabled(false);
	ui->lv_sessionsNames->setModel(&sessionsListModel);
	ui->lv_sessionsNames->setEditTriggers(QListView::NoEditTriggers);
	Qext::Object::connectMultipleSignals(
		ui->lv_sessionsNames->selectionModel(),
		{
			SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
			SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
			SIGNAL(modelChanged(QAbstractItemModel *)),
		},
		this,
		SLOT(handle_current_sessionIndex_changed())
	);
	connect(
		&sessionsListModel,
		SIGNAL(modelReset()),
		this,
		SLOT(handle_current_sessionIndex_changed())
	);
	connect(
		ui->lv_sessionsNames,
		SIGNAL(doubleClicked(QModelIndex)),
		this,
		SLOT(on_pb_connect_clicked())
	);
}

SessionsListDialog::~SessionsListDialog(){
	delete ui;
}

void SessionsListDialog::setOnlineSessions(const QHash<id_t, ClientSession> & sessions){
	sessionsListModel.setSessions(sessions);
}

void SessionsListDialog::addOnlineSession(ClientSession session){
	sessionsListModel.addSession(std::move(session));
}


void SessionsListDialog::on_pb_connect_clicked(){
	QModelIndex currentModelIndex { ui->lv_sessionsNames->currentIndex() };
	QSUPPOSE(currentModelIndex.isValid());
	int currentIndex { currentModelIndex.row() };
	emit connectionRequest(sessionsListModel.sessions()[currentIndex].id());
}

void SessionsListDialog::handle_current_sessionIndex_changed(){
	ui->pb_connect->setEnabled(ui->lv_sessionsNames->currentIndex().isValid());
}

void SessionsListDialog::on_pb_createSession_clicked(){
	SessionNameDialog dialog(this);
	if (dialog.exec() == QDialog::Accepted){
		emit createSessionRequest(dialog.sessionName());
	}
}

void SessionsListDialog::deleteOnlineSession(const id_t & id){
	(void)sessionsListModel.removeSession(id);
}












