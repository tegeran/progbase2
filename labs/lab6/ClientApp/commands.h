#ifndef COMMANDS_H
#define COMMANDS_H
#include <QUndoCommand>

#include "mainwindow.h"
#include "provider.h"

namespace Command {
	class Add : public QUndoCommand {
	public:
		Add(MainWindow & win, Provider && prov, int index, QUndoCommand * comm = nullptr);

		void undo() override ;
		void redo() override;
	private:
		MainWindow & win;
		Provider prov;
		int index;
	};

	class Delete: public QUndoCommand {
	public:
		Delete(MainWindow & win, int index, QUndoCommand * comm = nullptr);

		void undo() override;
		void redo() override;
	private:
		MainWindow & win;
		Provider prov;
		int index;
	};

	class Edit: public QUndoCommand {
	public:
		Edit(MainWindow & win, Provider && prov, int index, QUndoCommand * comm = nullptr);

		void undo() override;
		void redo() override;
	private:
		MainWindow & win;
		Provider previousProvider;
		Provider prov;
		int index;

		void edit(Provider Edit::* previousProvider, Provider Edit::* providerToEdit);
	};
}



#endif // COMMANDS_H
