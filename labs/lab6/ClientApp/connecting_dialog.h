#ifndef CONNECTING_DIALOG_H
#define CONNECTING_DIALOG_H

#include <QDialog>

namespace Ui {
	class ConnectingDialog;
}

class ConnectingDialog : public QDialog
{
	Q_OBJECT

public:
	explicit ConnectingDialog(QWidget *parent = 0);
	~ConnectingDialog();
signals:
	void abortRequest();

private slots:
	void on_pb_abortConnection_clicked();

private:
	Ui::ConnectingDialog *ui;
};

#endif // CONNECTING_DIALOG_H
