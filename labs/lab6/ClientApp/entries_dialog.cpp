#include <QDialogButtonBox>
#include <QPushButton>

#include "entries_dialog.h"
#include "ui_entries_dialog.h"
#include "qext.h"


EntriesDialog::EntriesDialog(const QStringList & entriesNames,
							 QWidget *parent)
	: QDialog(parent),
	ui(new Ui::EntriesDialog)
{
	ui->setupUi(this);
	entriesModel.setStringList(entriesNames);
	ui->lv_entries->setModel(&entriesModel);
	ui->lv_entries->setEditTriggers(QListView::NoEditTriggers);
	Qext::Object::connectMultipleSignals(
		ui->lv_entries->selectionModel(),
		{
			SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
			SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
			SIGNAL(modelChanged(QAbstractItemModel *))
		},
		this,
		SLOT(handle_lv_entries_currentChanged())
	);
	ui->buttonBox->button(QDialogButtonBox::Open)->setEnabled(false);
	connect(
		ui->buttonBox->button(QDialogButtonBox::Cancel),
		SIGNAL(clicked()),
		this,
		SLOT(reject())
	);
	connect(
		ui->buttonBox->button(QDialogButtonBox::Open),
		SIGNAL(clicked()),
		this,
		SLOT(accept())
	);
}

EntriesDialog::~EntriesDialog(){
	delete ui;
}

QString EntriesDialog::selectedEntry() const{
	QModelIndex currentIndex { ui->lv_entries->currentIndex() };
	QSUPPOSE(currentIndex.isValid(), "request for entry with no selected item");
	return currentIndex.data().toString();
}

void EntriesDialog::setEntries(const QStringList & entriesNamesList){
	entriesModel.setStringList(entriesNamesList);
}

void EntriesDialog::tryAddEntry(const QString & entryName){
	Qext::StringListModel::addUniqueString(entriesModel, entryName);
}

void EntriesDialog::handle_lv_entries_currentChanged(){
	ui->buttonBox->button(QDialogButtonBox::Open)->setEnabled(
		ui->lv_entries->currentIndex().isValid()
	);
}


