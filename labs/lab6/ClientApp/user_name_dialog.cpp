#include <QRegularExpressionValidator>
#include <QPropertyAnimation>

#include "user_name_dialog.h"
#include "ui_user_name_dialog.h"
#include "std_ext.h"
#include "user.h"
#include "client_session.h"


UserNameDialog::UserNameDialog(const QString & name, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::UserNameDialog),
	leditValidator(QRegularExpression(User::nameRegex()))
{
	ui->setupUi(this);
	ui->ledit_name->setText(name);
	ui->ledit_name->setValidator(&leditValidator);
	ui->buttonBox->setEnabled(ui->ledit_name->hasAcceptableInput());
}

UserNameDialog::~UserNameDialog(){
	delete ui;
}

QString UserNameDialog::name() const{
	return ui->ledit_name->text().trimmed();
}

void UserNameDialog::on_ledit_name_textChanged(const QString &){
	ui->buttonBox->setEnabled(ui->ledit_name->hasAcceptableInput());
}
