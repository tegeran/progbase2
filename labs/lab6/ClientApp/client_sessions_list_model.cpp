#include "client_sessions_list_model.h"

ClientSessionsListModel::ClientSessionsListModel(QObject *parent)
	: QAbstractListModel(parent) {}

int ClientSessionsListModel::rowCount(const QModelIndex &parent) const{
	if (parent.isValid())
		return 0;
	return m_sessions.size();
}

QVariant ClientSessionsListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();
	return role == Qt::DisplayRole
		? QVariant(m_sessions.at(index.row()).name())
		: QVariant() ;
}

bool ClientSessionsListModel::insertRows(int row, int count, const QModelIndex &parent)
{
	beginInsertRows(parent, row, row + count - 1);
	while(count){
		m_sessions.insert(row, ClientSession());
		--count;
	}
	endInsertRows();
	return true;
}

bool ClientSessionsListModel::removeRows(int row, int count, const QModelIndex &parent)
{
	beginRemoveRows(parent, row, row + count - 1);
	m_sessions.remove(row, count);
	endRemoveRows();
	return true;
}

ClientSession ClientSessionsListModel::removeSession(const id_t & id){
	for(auto iterator { m_sessions.begin() }; iterator != m_sessions.end(); ++iterator){
		if (iterator->id() == id){
			ClientSession session { *iterator };
			removeRow(iterator - m_sessions.begin());
			return session;
		}
	}
	QLOG_ERROR("no client session with id " << id << " was found");
	return ClientSession();
}

void ClientSessionsListModel::addSession(ClientSession && session){
	insertRow(0);
	m_sessions[0] = std::move(session);
	emit dataChanged(index(0), index(0));
}

void ClientSessionsListModel::setSessions(const QHash<id_t, ClientSession> & sessions){
	beginResetModel();
	m_sessions.clear();
	for (const ClientSession & session : sessions){
		m_sessions << session;
	}
	endResetModel();
}

const QVector<ClientSession> ClientSessionsListModel::sessions() const{
	return m_sessions;
}
