#include "connecting_dialog.h"
#include "ui_connecting_dialog.h"

ConnectingDialog::ConnectingDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ConnectingDialog)
{
	ui->setupUi(this);
}

ConnectingDialog::~ConnectingDialog()
{
	delete ui;
}

void ConnectingDialog::on_pb_abortConnection_clicked(){
	emit abortRequest();
}
