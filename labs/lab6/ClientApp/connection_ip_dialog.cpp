#include <QPushButton>
#include <QRegularExpressionValidator>
#include <QRegularExpression>

#include <limits>

#include "std_ext.h"
#include "connection_ip_dialog.h"
#include "ui_connection_ip_dialog.h"
#include "client.h"

ConnectionIpDialog::ConnectionIpDialog(const QString & title,
									   const QString & initialIp,
									   const quint16 & initialPort,
									   const QDialogButtonBox::StandardButton &okayButton,
									   const QDialogButtonBox::StandardButton &cancelButton,
									   QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ConnectionIpDialog)
{
	ui->setupUi(this);
	ui->buttonBox->addButton(okayButton);
	ui->buttonBox->addButton(cancelButton);
	m_PBokay = ui->buttonBox->button(okayButton);
	m_PBcancel = ui->buttonBox->button(cancelButton);
	ui->ledit_ipv4->setText(initialIp);
	connect(
		m_PBcancel,
		SIGNAL(clicked(bool)),
		this,
		SLOT(reject())
	);
	connect(
		ui->ledit_ipv4,
		SIGNAL(textChanged(QString)),
		this,
		SLOT(handle_addressChanged())
	);
	connect(
		m_PBokay,
		SIGNAL(clicked(bool)),
		this,
		SLOT(accept())
	);
	ui->ledit_ipv4->setValidator(
		new QRegularExpressionValidator(
			QRegularExpression(std_ext::IPV4_ADDRESS_REGEX)
		)
	);
	ui->sbox_port->setMaximum(std::numeric_limits<quint16>::max());
	ui->sbox_port->setValue(initialPort);
	ui->slbl_title->setText(
		tr(R"~~(
		   <html>
			   <head/>
			   <body>
				   <p align="center">
					   <span style=" font-size:18pt; font-weight:600;">%1</span>
				   </p>
			   </body>
		   </html>)~~").arg(title)
	);
}

ConnectionIpDialog::~ConnectionIpDialog(){
	delete ui;
}

QString ConnectionIpDialog::chosenIpv4Address() const	{
	return ui->ledit_ipv4->text();
}


void ConnectionIpDialog::handle_addressChanged(){
	m_PBokay->setEnabled(
		ui->ledit_ipv4->hasAcceptableInput()
	);
}


quint16 ConnectionIpDialog::chosenPort() const{
	return static_cast<quint16>(ui->sbox_port->value());
}
