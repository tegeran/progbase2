#include "provider_list_model.h"
#include <utility>
#include "error.h"

#define PROVIDER_VECTOR_MIME_TYPE "application/lab5-provider-vector"

ProviderListModel::ProviderListModel(QObject * parent, QVector<Provider> && vect)
	: QAbstractListModel(parent), vect(std::move(vect))
{
}

int ProviderListModel::rowCount(const QModelIndex &parent) const{
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	return parent.isValid() ? 0 : vect.size();
}

QVariant ProviderListModel::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant();

	switch (role){
		case Qt::DisplayRole: { return QVariant(vect[index.row()].name()); }
		default: return QVariant();
	}
}

Qt::ItemFlags ProviderListModel::flags(const QModelIndex &index) const {
	if (!index.isValid())
		return Qt::NoItemFlags;
	return   Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled
			| Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool ProviderListModel::insertRows(int row, int count, const QModelIndex &parent){
	beginInsertRows(parent, row, row + count - 1);
	vect.insert(row, count, Provider());
	endInsertRows();
	return true;
}

bool ProviderListModel::removeRows(int row, int count, const QModelIndex &parent){
	beginRemoveRows(parent, row, row + count - 1);
	vect.remove(row, count);
	endRemoveRows();
	return true;
}

QVector<Provider> & ProviderListModel::providers(){
	return vect;
}

void ProviderListModel::setProviderAt(const int & index, Provider && provider){
	vect[index] = std::move(provider);
	emit dataChanged(this->index(index), this->index(index), QVector<int>() << Qt::DisplayRole);
}

void ProviderListModel::insertProviderAt(const int & index, Provider && provider){
	beginInsertRows(QModelIndex(), index, index);
	vect.insert(index, std::move(provider));
	endInsertRows();
}

void ProviderListModel::clearProviders(){
	beginResetModel();
	vect.clear();
	endResetModel();
}

void ProviderListModel::setProviders(QVector<Provider> && donor){
	beginResetModel();
	vect = std::move(donor);
	endResetModel();
	emit dataChanged(QModelIndex(), QModelIndex(), QVector<int>() << Qt::DisplayRole);
}

Qt::DropActions ProviderListModel::supportedDropActions() const {
	return Qt::CopyAction | Qt::MoveAction;
}

QStringList ProviderListModel::mimeTypes() const{
	return QStringList() << PROVIDER_VECTOR_MIME_TYPE;
}

QMimeData * ProviderListModel::mimeData(const QModelIndexList & indexes) const{
	QByteArray barr;
	QDataStream stream(&barr, QIODevice::WriteOnly);
	foreach (const QModelIndex & index, indexes){
		if (index.isValid()){
			stream << vect.at(index.row());
		} else {
			QLOG("Invalid index received");
		}
	}
	QMimeData * mime(new QMimeData());
	mime->setData(PROVIDER_VECTOR_MIME_TYPE, barr);
	return mime;
}

bool ProviderListModel::dropMimeData(const QMimeData * mime,
									 Qt::DropAction action,
									 int row,
									 int column,
									 const QModelIndex & parent){
	if (column > 0 || !mime->hasFormat(PROVIDER_VECTOR_MIME_TYPE)) { return false; }
	row = parent.isValid() ? parent.row() : vect.size();
	switch (action){
		case Qt::CopyAction:
		case Qt::MoveAction:{
			QByteArray barr(mime->data(PROVIDER_VECTOR_MIME_TYPE));
			QDataStream stream(&barr, QIODevice::ReadOnly);
			while (!stream.atEnd()){
				Provider prov;
				stream >> prov;
//				vect.insert(row++, std::move(prov));
				insertProviderAt(row++, std::move(prov));
			}
			return true;
		}
//		case Qt::MoveAction: {
//			QByteArray barr(mime->data(PROVIDER_VECTOR_MIME_TYPE));
//			QDataStream stream(&barr, QIODevice::ReadOnly);
//			while (!stream.atEnd()){
//				Provider prov;
//				stream >> prov;
////				int i = vect.indexOf(prov);
////				if (i == -1){
////					QLOG("invalid move operation for \"" << prov.name() << "\" " << "tariff: " << prov.tariff());
////					continue;
////				}
//				insertProviderAt(row++, std::move(prov));
//			}
//			return true;
//		}
		default: return false;
	}
}

//bool ProviderListModel::moveRows(const QModelIndex & sourceParent,
//								 int sourceRow,
//								 int count,
//								 const QModelIndex & destinationParent,
//								 int destinationChild){
//	if (sourceParent.isValid()
//			|| destinationParent.isValid()
//			|| sourceRow == destinationChild - 1
//			|| count == 0){
//		return false;
//	}
//	if (!beginMoveRows(sourceParent, sourceRow, sourceRow + count, destinationParent, destinationChild)){
//		QLOG("failed to move source: " << sourceRow << " count: " << count << " dest: " << destinationChild);
//		return false;
//	}
//	QVector<Provider> mid(vect.mid(sourceRow, count));
////	vect.remove(sourceRow, count);
////	if (destinationChild > sourceRow){
////		destinationChild -= count;
////	}
//	for (Provider & prov : mid){
//		vect.insert(destinationChild++, prov);
//	}
//	endMoveRows();
//	return true;
//}
