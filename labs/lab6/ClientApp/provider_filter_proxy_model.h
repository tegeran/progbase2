#ifndef PROVIDERFILTERPROXYMODEL_H
#define PROVIDERFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>
#include "provider.h"
#include <QSet>

class ProviderFilterProxyModel : public QSortFilterProxyModel {
	Q_OBJECT

public:
	explicit ProviderFilterProxyModel(
									  QSet<Provider *> && set = QSet<Provider *>(),
									  QObject *parent = nullptr
	);
	void setSet(QSet<Provider *> && set);
private:
	QSet<Provider *> set;

	// QSortFilterProxyModel interface
protected:
	bool filterAcceptsRow(int source_row, const QModelIndex & source_parent) const override;
	bool lessThan(const QModelIndex & source_left, const QModelIndex & source_right) const override;

};

#endif // PROVIDERFILTERPROXYMODEL_H
