#ifndef ENTRIES_DIALOG_H
#define ENTRIES_DIALOG_H

#include <QDialog>
#include <QStringList>
#include <QStringListModel>

namespace Ui {
	class EntriesDialog;
}

class EntriesDialog : public QDialog
{
	Q_OBJECT

public:
	explicit EntriesDialog(const QStringList & entriesNames = QStringList(),
						   QWidget *parent = nullptr);
	~EntriesDialog();
	QString selectedEntry() const;

public slots:
	void setEntries(const QStringList & entriesNamesList);
	void tryAddEntry(const QString & entryName);

private slots:
	void handle_lv_entries_currentChanged();

private:
	Ui::EntriesDialog *ui;
	QStringListModel entriesModel;
};

#endif // ENTRIES_DIALOG_H
