#ifndef SESSIONS_LIST_DIALOG_H
#define SESSIONS_LIST_DIALOG_H

#include <QDialog>
#include <QHash>

#include "client_session.h"
#include "client_sessions_list_model.h"

namespace Ui {
	class SessionsListDialog;
}

class SessionsListDialog : public QDialog
{
	Q_OBJECT

public:
	explicit SessionsListDialog(QWidget *parent = 0);
	~SessionsListDialog();
signals:
	void connectionRequest(id_t session);
	void createSessionRequest(QString name);

public slots:
	void setOnlineSessions(const QHash<id_t, ClientSession> & sessions);

	void addOnlineSession(ClientSession session);
	void deleteOnlineSession(const id_t & id);

private slots:
	void on_pb_connect_clicked();
	void handle_current_sessionIndex_changed();

	void on_pb_createSession_clicked();

private:
	Ui::SessionsListDialog * ui;
	ClientSessionsListModel sessionsListModel;
};

#endif // SESSIONS_LIST_DIALOG_H
