#include "provider_dialog.h"
#include "ui_provider_dialog.h"
#include "qext.h"

#include <QPushButton>
#include <QRegularExpressionValidator>
#include <QRegularExpression>
#include <limits>

ProviderDialog::ProviderDialog(const Provider & prov, QWidget * parent) :
	QDialog(parent),
	ui(new Ui::ProviderDialog),
	prov(prov)
{
	ui->setupUi(this);
	ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(Provider::isName(prov.name()));
	if (prov.hasCloud()){
		ui->gbox_cloud->setChecked(true);
		ui->sbox_cloudTariff->setValue(prov.cloud().tariff());
		ui->sbox_cloudCapacity->setValue(prov.cloud().capacity());
	} else {
		ui->gbox_cloud->setChecked(false);
	}
	ui->sbox_loss->setValue(prov.loss());
	ui->sbox_tariff->setValue(prov.tariff());
	ui->sbox_users->setValue(prov.users());
	ui->ledit_name->setText(prov.name());
	ui->ledit_name->setValidator(
				new QRegularExpressionValidator(
					QRegularExpression(R"~~(\s*\w+.*)~~"),
					this
				)
	);
	Qext::Sbox::setMaxUnlimited(*(ui->sbox_cloudCapacity));
	Qext::Sbox::setMaxUnlimited(*(ui->sbox_cloudTariff));
	Qext::Sbox::setMaxUnlimited(*(ui->sbox_tariff));
	Qext::Sbox::setMaxUnlimited(*(ui->sbox_users));
	ui->gbox_cloud->setChecked(prov.hasCloud());
}

ProviderDialog::~ProviderDialog(){
	delete ui;
}

Provider &ProviderDialog::provider(){
	return prov;
}

void ProviderDialog::on_ledit_name_textChanged(const QString &){
	ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(ui->ledit_name->hasAcceptableInput());
}

void ProviderDialog::on_buttonBox_accepted(){
	prov.setName(ui->ledit_name->text());
	prov.setTariff(ui->sbox_tariff->value());
	prov.setLoss(ui->sbox_loss->value());
	prov.setUsers(ui->sbox_users->value());
	if (ui->gbox_cloud->isChecked()){
		prov.setCloud(Provider::DataCloud(
						  ui->sbox_cloudTariff->value(),
						  ui->sbox_cloudCapacity->value()
		));
	} else {
		prov.setCloud(Provider::DataCloud::nullobj());
	}
	this->accept();
}

void ProviderDialog::on_ledit_name_editingFinished(){
	ui->ledit_name->setText(ui->ledit_name->text().trimmed());
}
