#ifndef CONNECTION_IP_DIALOG_H
#define CONNECTION_IP_DIALOG_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QPushButton>

namespace Ui {
	class ConnectionIpDialog;
}

class ConnectionIpDialog : public QDialog
{
	Q_OBJECT

public:
	explicit ConnectionIpDialog(const QString & title = "Enter Providererr server address",
								const QString & initialIp = "127.0.0.1",
								const quint16 & initialPort = 2000,
								const QDialogButtonBox::StandardButton &okayButton
									= QDialogButtonBox::Ok,
								const QDialogButtonBox::StandardButton &cancelButton
									= QDialogButtonBox::Cancel ,
								QWidget *parent = 0);
	~ConnectionIpDialog();
	QString chosenIpv4Address() const;
	quint16 chosenPort() const;
private slots:
	void handle_addressChanged();

private:
	Ui::ConnectionIpDialog *ui;
	QPushButton * m_PBokay;
	QPushButton * m_PBcancel;
};


#endif // CONNECTION_IP_DIALOG_H
