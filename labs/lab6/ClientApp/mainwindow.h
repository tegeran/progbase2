﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUndoStack>
#include <QStackedWidget>
#include <QStringListModel>

#include "provider_list_model.h"
#include "provider_filter_proxy_model.h"
#include "client.h"
#include "client_session.h"

namespace Command {
	class Add;
	class Delete;
	class Edit;
}

namespace Ui {
	class MainWindow;
}


class MainWindow : public QMainWindow{
	Q_OBJECT
	friend class Command::Add;
	friend class Command::Delete;
	friend class Command::Edit;
	friend class MainStackedWidget;

public:
	explicit MainWindow(Client * const & client,
						QVector<Provider> && providers = QVector<Provider>(),
						const QVector<User> & connectedUsers = QVector<User>(),
						QWidget *parent = nullptr);
	~MainWindow();
	static QString globalStyleSheet();
private slots:
	void handle_model_dataChanged(QModelIndex = QModelIndex(), QModelIndex = QModelIndex(), QVector<int> = QVector<int>());

	void on_pb_add_clicked();

	void on_pb_delete_clicked();

	void on_pb_edit_clicked();

	void on_lv_names_doubleClicked(const QModelIndex &index);

	void on_lv_execs_doubleClicked(const QModelIndex &index);

	void handle_lv_names_currentChanged(const QModelIndex &cur, const QModelIndex &prev);
	void handle_lv_execs_currentChanged(const QModelIndex &cur, const QModelIndex &prev = QModelIndex());
	void on_pb_execute_clicked();

	void on_act_open_triggered();

	void on_act_saveTo_triggered();

	void on_act_clear_triggered();

	void handle_model_rowsInserted(const QModelIndex &parent, int first, int last);
	void handle_model_rowsDeleted(const QModelIndex &parent, int first, int last);
	void handle_model_reseted();
	void handle_lv_names_indexesMoved();
	void on_sbox_exec_valueChanged(int arg1);
	void handle_client_receivedSavedEntriesForOpen(const QStringList & entriesNames);
	void handle_client_receivedSavedEntriesForDump(const QStringList & entriesNames);
	void handle_client_updateReloadedEntries(QVector<Provider> providers);
	void handle_client_userConnected(const User & user);
	void handle_client_userDisconnected(const User & user);
	void handle_client_updateEditedProvider(Provider provider, const id_t & position);

	// ---------------------------------------

private:
	Ui::MainWindow * ui;
	QUndoStack ustack;
	ProviderListModel * model;
	ProviderFilterProxyModel * proxyModel;
	int currentFilter;
	Client * client;
	QStringListModel usersOnlineModel;

	void insertProvider(const int & index, Provider && prov);
	void deleteProvider(const int & index);
	void setStatus(const QString & status);
	QStatusBar & sbar();
	int current() const;
	void updateLables();
	void updateProxyModel();
	void setFilter(const int & filter);
	void updateButtons();
	void syncSelections();

	void setupRedosUndos();
	void setupProviderModel(QVector<Provider> && providers);
	void setupListViews(const QVector<User> & connectedUsers = QVector<User>());
	void setupClient();

	void startSessionsListDialog();
};

#endif // MAINWINDOW_H
