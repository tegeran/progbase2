#include <QStatusBar>

#include <algorithm>

#include "commands.h"
#include "qext.h"
#include "std_ext.h"

static bool operator==(const Provider & left, const Provider & right){
	return static_cast<Provider>(left).operator==(right); // ZATUPIL
}

Command::Add::Add(MainWindow & win, Provider && prov, int index, QUndoCommand * comm)
	: QUndoCommand(comm), win(win), prov(prov), index(index)
{
	QUndoCommand::setText(TR("add \"%1\"").arg(this->prov.name()));
	win.setStatus(TR("Added provider \"%1\"").arg(this->prov.name()));
}

void Command::Add::undo(){
	const QVector<Provider> & modelProviders { win.model->providers() };
	if (!std_ext::hasIndex(modelProviders, index)
		|| !(modelProviders.at(index) == prov)){
		auto res { std_ext::find(modelProviders, prov) };
		if (res == modelProviders.end()){
			return;
		}
		index = res - modelProviders.begin();
	}
	win.client->requireDeleteProvider(index);
	Qext::Sbar::showUndone(*this, win.sbar());
}
void Command::Add::redo(){
	const QVector<Provider> & modelProviders { win.model->providers() };
	if (modelProviders.size() < index){
		index = modelProviders.size();
	}
	win.client->requireAddProvider(prov, index);
	Qext::Sbar::showRedone(*this, win.sbar());
}

Command::Delete::Delete(MainWindow & win, int index, QUndoCommand * comm)
	: QUndoCommand(comm), win(win), prov(win.model->providers()[index]), index(index)
{
	QUndoCommand::setText(TR("delete \"%1\"").arg(this->prov.name()));
	win.setStatus(TR("Deleted provider \"%1\"").arg(this->prov.name()));
}

void Command::Delete::undo(){
	const QVector<Provider> & modelProviders { win.model->providers() };
	if (modelProviders.size() < index){
		index = modelProviders.size();
	}
	win.client->requireAddProvider(prov, index);
	Qext::Sbar::showUndone(*this, win.sbar());
}
void Command::Delete::redo(){
	const QVector<Provider> & modelProviders { win.model->providers() };
	if (!std_ext::hasIndex(modelProviders, index)
		|| !(modelProviders.at(index) == prov)){
		auto res { std_ext::find(modelProviders, prov) };
		if (res == modelProviders.end()){
			return;
		}
		index = res - modelProviders.begin();
	}
	win.client->requireDeleteProvider(index);
	Qext::Sbar::showRedone(*this, win.sbar());
}


Command::Edit::Edit(MainWindow & win, Provider && prov, int index, QUndoCommand * comm)
	: QUndoCommand(comm),
	  win(win),
	  previousProvider(win.model->providers().at(index)),
	  prov(std::move(prov)), index(index)
{
	QUndoCommand::setText(TR("edit \"%1\"").arg(this->prov.name()));
	win.setStatus(TR("Edited provider \"%1\"").arg(this->prov.name()));
}

void Command::Edit::undo(){
	edit(&Edit::prov, &Edit::previousProvider);
	Qext::Sbar::showUndone(*this, win.sbar());
}
void Command::Edit::redo(){
	edit(&Edit::previousProvider, &Edit::prov);
	Qext::Sbar::showRedone(*this, win.sbar());
}

void Command::Edit::edit(Provider Edit::*previousProvider, Provider Edit::*providerToEdit){
	const QVector<Provider> & modelProviders { win.model->providers() };
	if (!std_ext::hasIndex(modelProviders, index)
			|| !(modelProviders.at(index) == this->*previousProvider))
	{
		auto res { std_ext::find(modelProviders, this->*previousProvider) };
		if (res == modelProviders.end()){
			return;
		}
		index = res - modelProviders.begin();
	}
	win.client->requireEditProvider(this->*providerToEdit, index);
}
