#ifndef SESSION_NAME_DIALOG_H
#define SESSION_NAME_DIALOG_H

#include <QDialog>

namespace Ui {
	class SessionNameDialog;
}

class SessionNameDialog : public QDialog
{
	Q_OBJECT

public:
	explicit SessionNameDialog(QWidget *parent = 0);
	~SessionNameDialog();

	QString sessionName() const;

private slots:
	void on_ledit_sessionName_textChanged(const QString &arg1);

private:
	Ui::SessionNameDialog *ui;
};

#endif // SESSION_NAME_DIALOG_H
