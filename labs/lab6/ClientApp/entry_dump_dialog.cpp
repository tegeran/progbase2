#include <QPushButton>

#include "entry_dump_dialog.h"
#include "ui_entry_dump_dialog.h"
#include "qext.h"

EntryDumpDialog::EntryDumpDialog(const QStringList & availableEntries, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::EntryDumpDialog)
{
	ui->setupUi(this);
	entriesModel.setStringList(availableEntries);
	ui->lv_entries->setModel(&entriesModel);
	entriesComleter.setModel(&entriesModel);
	entriesComleter.setModelSorting(QCompleter::CaseInsensitivelySortedModel);
	ui->ledit_entryName->setCompleter(&entriesComleter);
	Qext::Object::connectMultipleSignals(
		ui->lv_entries->selectionModel(),
		{
			SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
			SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
			SIGNAL(modelChanged(QAbstractItemModel *))
		},
		this,
		SLOT(handle_entriesModel_currentChanged())
	);
	connect(
		ui->ledit_entryName,
		&QLineEdit::textEdited,
		[this](){ this->ui->lv_entries->selectionModel()->clearSelection(); }
	);
	connect(
		ui->buttonBox->button(QDialogButtonBox::Cancel),
		SIGNAL(clicked()),
		this,
		SLOT(reject())
	);
	connect(
		ui->buttonBox->button(QDialogButtonBox::Save),
		SIGNAL(clicked()),
		this,
		SLOT(accept())
	);
}

EntryDumpDialog::~EntryDumpDialog(){
	delete ui;
}

QString EntryDumpDialog::chosenEntry() const{
	return ui->ledit_entryName->text();
}

void EntryDumpDialog::tryAddEntry(const QString & string){
	Qext::StringListModel::addUniqueString(entriesModel, string);
}

void EntryDumpDialog::handle_entriesModel_currentChanged(){
	QModelIndex currentModelIndex { ui->lv_entries->currentIndex() };
	if (!currentModelIndex.isValid()){
		return;
	}
	ui->ledit_entryName->setText(currentModelIndex.data().toString());
}

