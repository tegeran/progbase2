#-------------------------------------------------
#
# Project created by QtCreator 2018-05-17T15:33:13
#
#-------------------------------------------------

QT       += core gui xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClientApp
TEMPLATE = app
CONFIG += c++2a
QMAKE_CXXFLAGS += -std=c++2a -Wall -Wextra -error -pedantic -Wno-variadic-macros

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../common_libs

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    provider_dialog.cpp \
    provider_list_model.cpp \
    commands.cpp \
    provider_filter_proxy_model.cpp \
    user_name_dialog.cpp \
    sessions_list_dialog.cpp \
    client.cpp \
    session_name_dialog.cpp \
    main_stacked_widget.cpp \
    client_sessions_list_model.cpp \
    entries_dialog.cpp \
    entry_dump_dialog.cpp \
    connection_ip_dialog.cpp \
    connecting_dialog.cpp


HEADERS += \
        mainwindow.h \
    provider_dialog.h \
    provider_list_model.h \
    commands.h \
    provider_filter_proxy_model.h \
    ../common_libs/std_ext.h\
    ../common_libs/message_exception.h \
    ../common_libs/qext.h \
    ../common_libs/error.h \
    ../common_libs/netpacket.h \
    ../common_libs/location.h \
    ../common_libs/provider.h \
    ../common_libs/qext.h \
    ../common_libs/qnet.h \
    ../common_libs/datapacket.h \
    user_name_dialog.h \
    ../common_libs/user.h \
    sessions_list_dialog.h \
    ../common_libs/client_session.h \
    client.h \
    session_name_dialog.h \
    ../common_libs/tcpsocket.h \
    ../common_libs/session.h \
    ../common_libs/server_user.h \
    main_stacked_widget.h \
    client_sessions_list_model.h \
    entries_dialog.h \
    entry_dump_dialog.h \
    connection_ip_dialog.h \
    connecting_dialog.h

FORMS += \
        mainwindow.ui \
        provider_dialog.ui \
    user_name_dialog.ui \
    sessions_list_dialog.ui \
    session_name_dialog.ui \
    entries_dialog.ui \
    entry_dump_dialog.ui \
    connection_ip_dialog.ui \
    connecting_dialog.ui

RESOURCES += icons.qrc


unix:!macx: LIBS += -L$$PWD/../common_libs/ -lprovider

INCLUDEPATH += $$PWD/../common_libs
DEPENDPATH += $$PWD/../common_libs


unix:!macx: LIBS += -L$$PWD/../common_libs/ -lNetLib

INCLUDEPATH += $$PWD/../common_libs
DEPENDPATH += $$PWD/../common_libs
