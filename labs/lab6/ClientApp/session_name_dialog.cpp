#include <QPushButton>
#include <QRegularExpressionValidator>

#include "session_name_dialog.h"
#include "ui_session_name_dialog.h"
#include "client_session.h"

SessionNameDialog::SessionNameDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SessionNameDialog)
{
	ui->setupUi(this);
	ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
	ui->ledit_sessionName->setValidator(
		new QRegularExpressionValidator(ClientSession::nameRegex())
	);
}

SessionNameDialog::~SessionNameDialog()
{
	delete ui;
	delete ui->ledit_sessionName->validator();
}

QString SessionNameDialog::sessionName() const{
	return ui->ledit_sessionName->text();
}

void SessionNameDialog::on_ledit_sessionName_textChanged(const QString &){
	ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(
		ui->ledit_sessionName->hasAcceptableInput()
	);
}
