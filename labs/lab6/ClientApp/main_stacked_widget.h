#ifndef MAIN_STACKED_WIDGET_H
#define MAIN_STACKED_WIDGET_H

#include <QStackedWidget>
#include <QWidget>

#include "mainwindow.h"
#include "user_name_dialog.h"
#include "sessions_list_dialog.h"
#include "client.h"

class MainStackedWidget : public QStackedWidget{
	Q_OBJECT
public:
	MainStackedWidget(QWidget * const & parent = nullptr);

private slots:
	void on_userNameDialog_accepted();
	void handle_requireSessionConnection(const id_t & sessionId);
	void handle_requireNewSession(const QString & sessionName);
	void handle_client_error(const QString & reason);
	void handle_client_createdSession(ClientSession session);
	void handle_client_disconnectedFromServer();
	void handle_client_connectedToSession(ClientSession session,
										  QVector<Provider> providers = QVector<Provider>(),
										  const QVector<User> & connectedUsers = QVector<User>());
	void handle_client_connectedToServer();
	void handle_connectingDialog_abortRequest();
protected:
	void closeEvent(QCloseEvent * event) override;
	void showEvent(QShowEvent *event);
private:
	MainWindow * m_mainWindow;
	Client * m_client;

	void setupClient();
	void startSessionsListDialog();
	void terminate();
	void clearWidgetsStack();
	void showConnectionView();
};

#endif // MAIN_STACKED_WIDGET_H
