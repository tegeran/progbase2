#ifndef ENTRY_DUMP_DIALOG_H
#define ENTRY_DUMP_DIALOG_H

#include <QDialog>
#include <QStringListModel>
#include <QStringList>
#include <QCompleter>

namespace Ui {
	class EntryDumpDialog;
}

class EntryDumpDialog : public QDialog
{
	Q_OBJECT

public:
	explicit EntryDumpDialog(const QStringList & availableEntries = QStringList(),
							 QWidget *parent = 0);
	~EntryDumpDialog();
	QString chosenEntry() const;
	void tryAddEntry(const QString & string);
private slots:
	void handle_entriesModel_currentChanged();

private:
	Ui::EntryDumpDialog *ui;
	QStringListModel entriesModel;
	QCompleter entriesComleter;
};

#endif // ENTRY_DUMP_DIALOG_H
