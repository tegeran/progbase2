#ifndef PROVIDER_DIALOG_H
#define PROVIDER_DIALOG_H

#include <QDialog>

#include "provider.h"

namespace Ui {
	class ProviderDialog;
}

class ProviderDialog : public QDialog{
	Q_OBJECT

public:
	explicit ProviderDialog(const Provider & prov = Provider(), QWidget * parent = nullptr);
	~ProviderDialog();
	Provider & provider();

private slots:
	void on_ledit_name_textChanged(const QString &text);

	void on_buttonBox_accepted();

	void on_ledit_name_editingFinished();

private:
	Ui::ProviderDialog *ui;
	Provider prov;
};

#endif // PROVIDER_DIALOG_H
