#include "client.h"
#include "netpacket.h"
#include "datapacket.h"
#include "message_exception.h"
#include "qnet.h"

Client::Client() {
//	connect (
//		&m_socket,
//		SIGNAL(connectionTimedOut()),
//		this,
//		SLOT(on_socket_error())
//	);
	connect(
		&m_socket,
		SIGNAL(connected()),
		this,
		SLOT(on_socket_connected())
	);
	connect(
		&m_socket,
		SIGNAL(error(QAbstractSocket::SocketError)),
		this,
		SLOT(on_socket_error())
	);
	connect(
		&m_socket,
		SIGNAL(receivedNetPacket(NetPacket)),
		this,
		SLOT(on_socket_receivedNetPacket(NetPacket))
	);
	connect(
		&m_socket,
		SIGNAL(disconnected()),
		this,
		SLOT(on_socket_disconnected())
	);
}

void Client::connectToServer(const QHostAddress & address,
							 const quint16 & port){
	if (m_socket.state() != QAbstractSocket::UnconnectedState){
		QLOG_ERROR("socket is not in unconnected state");
		return;
	}
	m_socket.connectToHost(address, port, QIODevice::ReadWrite);
}

void Client::disconnect(){
	QSUPPOSE(Qnet::isConnected(m_socket));
	m_socket.disconnectFromHost();
}


const User &Client::user() const{
	return m_user;
}

bool Client::isConnected() const{
	return Qnet::isConnected(m_socket);
}

const TcpSocket &Client::socket() const{
	return m_socket;
}

void Client::requireOnlineSessionsHashTable(){
	QSUPPOSE(Qnet::canWriteTo(m_socket), m_socket.state());
	sendToServer<DataPacket::TypeOnly>(
		NetPacket::Request_OnlineSessions
	);
}

void Client::requireNewSession(const QString & name){
	QSUPPOSE(Qnet::canWriteTo(m_socket));
	sendToServer<DataPacket::String>(
		NetPacket::Request_CreateSession,
		name
	);
}

void Client::setUser(const User & user){
	sendToServer<DataPacket::SingleUser>(
		NetPacket::Request_SetUser,
		user
	);
}


void Client::requireSessionConnection(const id_t & sessionId){
	sendToServer<DataPacket::Id>(
		NetPacket::Request_ConnectToSession,
		sessionId
	);
}

void Client::requireSavedEntries(){
	sendToServer<DataPacket::TypeOnly>(
		NetPacket::Request_SavedEntries
	);
}

void Client::requireDumpEntries(const QString & filename){
	sendToServer<DataPacket::String>(
		NetPacket::Request_DumpEntries,
		filename
	);
}

void Client::requireLoadEntries(const QString & filename){
	sendToServer<DataPacket::String>(
		NetPacket::Request_LoadEntries,
		filename
	);
}

void Client::requireAddProvider(const Provider & provider, const id_t & position){
	sendToServer<DataPacket::ProviderAndPosition>(
		NetPacket::Request_AddProvider,
		provider,
		position
	);
}

void Client::requireDeleteProvider(const id_t & position){
	sendToServer<DataPacket::Id>(
		NetPacket::Request_DeleteProvider,
		position
	);
}

void Client::requireEditProvider(const Provider & provider, const id_t & position){
	sendToServer<DataPacket::ProviderAndPosition>(
		NetPacket::Request_EditProvider,
		provider,
		position
	);
}

void Client::requireClearProviders(){
	sendToServer<DataPacket::TypeOnly>(
		NetPacket::Request_ClearProviders
	);
}

void Client::on_socket_connected(){
	m_socket.setSocketOption(QAbstractSocket::LowDelayOption, true);
	emit establishedConnection();
}

void Client::on_socket_error(){
	QLOG_ERROR("server error " << m_socket.errorString());
	emit error(m_socket.errorString());
}

void Client::on_socket_disconnected(){
	emit disconnectedFromServer();
	m_session = ClientSession::nullobj();
}

void Client::on_socket_receivedNetPacket(const NetPacket & netpacket){
	try{
		switch (netpacket.type()){
			case NetPacket::Answer_OnlineSessions:
			{ processAnswer_OnlineSessions(netpacket);     return; }

			case NetPacket::Answer_CreatedSession:
			{ processAnswer_CreatedSession(netpacket);     return; }

			case NetPacket::Answer_ConnectedToSession:
			{ processAnswer_ConnectedToSession(netpacket); return; }

			case NetPacket::Answer_SetUser:
			{ processAnswer_SetUser(netpacket);			   return; }

			case NetPacket::Answer_SavedEntries:
			{ processAnswer_SavedEntries(netpacket);	   return; }

			case NetPacket::Update_AddedSession:
			{ processUpdate_AddedSession(netpacket);       return; }

			case NetPacket::Update_DeletedSession:
			{ processUpdate_DeletedSession(netpacket);	   return; }

			case NetPacket::Update_AddedProvider:
			{ processUpdate_AddedProvider(netpacket);      return; }

			case NetPacket::Update_DeletedProvider:
			{ processUpdate_DeletedProvider(netpacket);    return; }

			case NetPacket::Update_EditedProvider:
			{ processUpdate_EditedProvider(netpacket);     return; }

			case NetPacket::Answer_AddedProvider:
			{ processAnswer_AddedProvider(netpacket);      return; }

			case NetPacket::Answer_DeletedProvider:
			{ processAnswer_DeletedProvider(netpacket);    return; }

			case NetPacket::Answer_EditedProvider:
			{ processAnswer_EditedProvider(netpacket);     return; }

			case NetPacket::Update_UserConnected:
			{ processUpdate_UserConnected(netpacket);      return; }

			case NetPacket::Update_UserDisconnected:
			{ processUpdate_UserDisconnected(netpacket);   return; }

			case NetPacket::Update_DumpedEntries:
			{ processUpdate_DumpedEntries(netpacket);      return; }

			case NetPacket::Answer_Error:
			{ processAnswer_Error(netpacket);              return; }

			case NetPacket::Update_ReloadedEntries:
			{ processUpdate_ReloadedEntries(netpacket);    return; }

			case NetPacket::Update_ClearedProviders:
			{ processUpdate_ClearedProviders(netpacket);   return; }

			default:{
				QLOG("undefined packet type received");
				return;
			}
		}
	} catch (const MessageException & exception){
		emit error(QString(exception.what()));
	}
}


void Client::processAnswer_Error(const NetPacket & netpacket){
	QTRACE_CALL();
	emit error(netpacket.dataPacket<DataPacket::String>().string);
}

void Client::processAnswer_OnlineSessions(const NetPacket & netpacket){
	QTRACE_CALL();
	emit receivedOnlineSessionsHashTable(
		netpacket
		.dataPacket<DataPacket::SessionsHashTable>()
		.sessions
	);
}

void Client::processAnswer_CreatedSession(const NetPacket & netpacket){
	QTRACE_CALL();
	emit createdSession(
		(m_session = netpacket.dataPacket<DataPacket::SingleSession>().session)
	);
}

void Client::processAnswer_ConnectedToSession(const NetPacket & netpacket){
	QTRACE_CALL();
	auto dataPacket { netpacket.dataPacket<DataPacket::SessionConnectionData>() };
	emit connectedToSession(
		(m_session = dataPacket.session),
		dataPacket.providers,
		dataPacket.users
	);
}

void Client::processAnswer_SetUser(const NetPacket & netpacket){
	QTRACE_CALL();
	m_user = netpacket.dataPacket<DataPacket::SingleUser>().user;
	emit establishedSelfUser();
}

void Client::processAnswer_SavedEntries(const NetPacket & netpacket){
	QTRACE_CALL();
	emit receivedSavedEntries(netpacket.dataPacket<DataPacket::StringVector>().strings);
}

void Client::processUpdate_AddedSession(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateAddedSession(netpacket.dataPacket<DataPacket::SingleSession>().session);
}

void Client::processUpdate_DeletedSession(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateDeletedSession(netpacket.dataPacket<DataPacket::Id>().id);
}

void Client::processUpdate_AddedProvider(const NetPacket & netpacket){
	QTRACE_CALL();
	auto datapacket { netpacket.dataPacket<DataPacket::ProviderAndPosition>() };
	emit updateAddedProvider(datapacket.provider, datapacket.position);
}

void Client::processUpdate_EditedProvider(const NetPacket & netpacket){
	QTRACE_CALL();
	auto datapacket { netpacket.dataPacket<DataPacket::ProviderAndPosition>() };
	emit updateEditedProvider(datapacket.provider, datapacket.position);
}

void Client::processUpdate_DeletedProvider(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateDeletedProvider(netpacket.dataPacket<DataPacket::Id>().id);
}

void Client::processAnswer_AddedProvider(const NetPacket & netpacket){
	QTRACE_CALL();
	DataPacket::ProviderAndPosition dataPacket {
		netpacket.dataPacket<DataPacket::ProviderAndPosition>()
	};
	emit successfullyAddedProvider(dataPacket.provider, dataPacket.position);
}

void Client::processAnswer_EditedProvider(const NetPacket & netpacket){
	QTRACE_CALL();
	DataPacket::ProviderAndPosition dataPacket {
		netpacket.dataPacket<DataPacket::ProviderAndPosition>()
	};
	emit successfullyEditedProvider(dataPacket.provider, dataPacket.position);
}

void Client::processAnswer_DeletedProvider(const NetPacket & netpacket){
	QTRACE_CALL();
	emit successfullyDeletedProvider(netpacket.dataPacket<DataPacket::Id>().id);
}

void Client::processUpdate_UserConnected(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateUserConnected(netpacket.dataPacket<DataPacket::SingleUser>().user);
}

void Client::processUpdate_UserDisconnected(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateUserDisconnected(netpacket.dataPacket<DataPacket::SingleUser>().user);
}

void Client::processUpdate_ReloadedEntries(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateReloadedEntries(netpacket.dataPacket<DataPacket::ProviderVector>().providers);
}

void Client::processUpdate_DumpedEntries(const NetPacket & netpacket){
	QTRACE_CALL();
	emit updateDumpedEntries(netpacket.dataPacket<DataPacket::String>().string);
}

void Client::processUpdate_ClearedProviders(const NetPacket &){
	emit updateClearedProviders();
}

const ClientSession &Client::session() const{
	return m_session;
}

QHostAddress Client::serverAddress() const{
	return m_socket.peerAddress();
}

void Client::errorAbort(const QString & reason){
	m_socket.abort();
	emit error(reason);
}
