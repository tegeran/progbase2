#include <QApplication>
#include <QMetaType>
#include <QStyle>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QHostAddress>

#include "error.h"
#include "mainwindow.h"
#include "std_ext.h"
#include "main_stacked_widget.h"

#include <iostream>
#include "std_ext.h"

int main(int argc, char *argv[]){
	QApplication a(argc, argv);
	a.setStyleSheet(MainWindow::globalStyleSheet());
	MainStackedWidget w;
//	a.setStyleSheet(w.styleSheet());
	w.show();
	return a.exec();
}
