/********************************************************************************
** Form generated from reading UI file 'connecting_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONNECTING_DIALOG_H
#define UI_CONNECTING_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_ConnectingDialog
{
public:
    QGridLayout *gridLayout;
    QPushButton *pb_abortConnection;
    QSpacerItem *horizontalSpacer_2;
    QLabel *slbl_connecting;
    QSpacerItem *horizontalSpacer;
    QLabel *slbl_wifi2;

    void setupUi(QDialog *ConnectingDialog)
    {
        if (ConnectingDialog->objectName().isEmpty())
            ConnectingDialog->setObjectName(QStringLiteral("ConnectingDialog"));
        ConnectingDialog->resize(400, 300);
        gridLayout = new QGridLayout(ConnectingDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pb_abortConnection = new QPushButton(ConnectingDialog);
        pb_abortConnection->setObjectName(QStringLiteral("pb_abortConnection"));

        gridLayout->addWidget(pb_abortConnection, 2, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 2, 2, 1, 1);

        slbl_connecting = new QLabel(ConnectingDialog);
        slbl_connecting->setObjectName(QStringLiteral("slbl_connecting"));

        gridLayout->addWidget(slbl_connecting, 0, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

        slbl_wifi2 = new QLabel(ConnectingDialog);
        slbl_wifi2->setObjectName(QStringLiteral("slbl_wifi2"));
        slbl_wifi2->setPixmap(QPixmap(QString::fromUtf8(":/general/icons/wifi2.png")));

        gridLayout->addWidget(slbl_wifi2, 1, 1, 1, 1);


        retranslateUi(ConnectingDialog);

        QMetaObject::connectSlotsByName(ConnectingDialog);
    } // setupUi

    void retranslateUi(QDialog *ConnectingDialog)
    {
        ConnectingDialog->setWindowTitle(QApplication::translate("ConnectingDialog", "Dialog", nullptr));
        pb_abortConnection->setText(QApplication::translate("ConnectingDialog", "ABORT (SIGSEGV)", nullptr));
        slbl_connecting->setText(QApplication::translate("ConnectingDialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:24pt; font-weight:600;\">Connecting..</span></p></body></html>", nullptr));
        slbl_wifi2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ConnectingDialog: public Ui_ConnectingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONNECTING_DIALOG_H
