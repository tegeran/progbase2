/****************************************************************************
** Meta object code from reading C++ file 'main_stacked_widget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../main_stacked_widget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'main_stacked_widget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainStackedWidget_t {
    QByteArrayData data[21];
    char stringdata0[404];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainStackedWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainStackedWidget_t qt_meta_stringdata_MainStackedWidget = {
    {
QT_MOC_LITERAL(0, 0, 17), // "MainStackedWidget"
QT_MOC_LITERAL(1, 18, 26), // "on_userNameDialog_accepted"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 31), // "handle_requireSessionConnection"
QT_MOC_LITERAL(4, 78, 4), // "id_t"
QT_MOC_LITERAL(5, 83, 9), // "sessionId"
QT_MOC_LITERAL(6, 93, 24), // "handle_requireNewSession"
QT_MOC_LITERAL(7, 118, 11), // "sessionName"
QT_MOC_LITERAL(8, 130, 19), // "handle_client_error"
QT_MOC_LITERAL(9, 150, 6), // "reason"
QT_MOC_LITERAL(10, 157, 28), // "handle_client_createdSession"
QT_MOC_LITERAL(11, 186, 13), // "ClientSession"
QT_MOC_LITERAL(12, 200, 7), // "session"
QT_MOC_LITERAL(13, 208, 36), // "handle_client_disconnectedFro..."
QT_MOC_LITERAL(14, 245, 32), // "handle_client_connectedToSession"
QT_MOC_LITERAL(15, 278, 17), // "QVector<Provider>"
QT_MOC_LITERAL(16, 296, 9), // "providers"
QT_MOC_LITERAL(17, 306, 13), // "QVector<User>"
QT_MOC_LITERAL(18, 320, 14), // "connectedUsers"
QT_MOC_LITERAL(19, 335, 31), // "handle_client_connectedToServer"
QT_MOC_LITERAL(20, 367, 36) // "handle_connectingDialog_abort..."

    },
    "MainStackedWidget\0on_userNameDialog_accepted\0"
    "\0handle_requireSessionConnection\0id_t\0"
    "sessionId\0handle_requireNewSession\0"
    "sessionName\0handle_client_error\0reason\0"
    "handle_client_createdSession\0ClientSession\0"
    "session\0handle_client_disconnectedFromServer\0"
    "handle_client_connectedToSession\0"
    "QVector<Provider>\0providers\0QVector<User>\0"
    "connectedUsers\0handle_client_connectedToServer\0"
    "handle_connectingDialog_abortRequest"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainStackedWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    1,   70,    2, 0x08 /* Private */,
       6,    1,   73,    2, 0x08 /* Private */,
       8,    1,   76,    2, 0x08 /* Private */,
      10,    1,   79,    2, 0x08 /* Private */,
      13,    0,   82,    2, 0x08 /* Private */,
      14,    3,   83,    2, 0x08 /* Private */,
      14,    2,   90,    2, 0x28 /* Private | MethodCloned */,
      14,    1,   95,    2, 0x28 /* Private | MethodCloned */,
      19,    0,   98,    2, 0x08 /* Private */,
      20,    0,   99,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11, 0x80000000 | 15, 0x80000000 | 17,   12,   16,   18,
    QMetaType::Void, 0x80000000 | 11, 0x80000000 | 15,   12,   16,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainStackedWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainStackedWidget *_t = static_cast<MainStackedWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_userNameDialog_accepted(); break;
        case 1: _t->handle_requireSessionConnection((*reinterpret_cast< const id_t(*)>(_a[1]))); break;
        case 2: _t->handle_requireNewSession((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->handle_client_error((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->handle_client_createdSession((*reinterpret_cast< ClientSession(*)>(_a[1]))); break;
        case 5: _t->handle_client_disconnectedFromServer(); break;
        case 6: _t->handle_client_connectedToSession((*reinterpret_cast< ClientSession(*)>(_a[1])),(*reinterpret_cast< QVector<Provider>(*)>(_a[2])),(*reinterpret_cast< const QVector<User>(*)>(_a[3]))); break;
        case 7: _t->handle_client_connectedToSession((*reinterpret_cast< ClientSession(*)>(_a[1])),(*reinterpret_cast< QVector<Provider>(*)>(_a[2]))); break;
        case 8: _t->handle_client_connectedToSession((*reinterpret_cast< ClientSession(*)>(_a[1]))); break;
        case 9: _t->handle_client_connectedToServer(); break;
        case 10: _t->handle_connectingDialog_abortRequest(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Provider> >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Provider> >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainStackedWidget::staticMetaObject = {
    { &QStackedWidget::staticMetaObject, qt_meta_stringdata_MainStackedWidget.data,
      qt_meta_data_MainStackedWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainStackedWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainStackedWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainStackedWidget.stringdata0))
        return static_cast<void*>(this);
    return QStackedWidget::qt_metacast(_clname);
}

int MainStackedWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QStackedWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
