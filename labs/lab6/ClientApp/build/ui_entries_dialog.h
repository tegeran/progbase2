/********************************************************************************
** Form generated from reading UI file 'entries_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ENTRIES_DIALOG_H
#define UI_ENTRIES_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>

QT_BEGIN_NAMESPACE

class Ui_EntriesDialog
{
public:
    QGridLayout *gridLayout;
    QListView *lv_entries;
    QDialogButtonBox *buttonBox;
    QLabel *slbl_chooseEntry;

    void setupUi(QDialog *EntriesDialog)
    {
        if (EntriesDialog->objectName().isEmpty())
            EntriesDialog->setObjectName(QStringLiteral("EntriesDialog"));
        EntriesDialog->resize(400, 300);
        gridLayout = new QGridLayout(EntriesDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lv_entries = new QListView(EntriesDialog);
        lv_entries->setObjectName(QStringLiteral("lv_entries"));

        gridLayout->addWidget(lv_entries, 1, 0, 1, 1);

        buttonBox = new QDialogButtonBox(EntriesDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Open);

        gridLayout->addWidget(buttonBox, 2, 0, 1, 1);

        slbl_chooseEntry = new QLabel(EntriesDialog);
        slbl_chooseEntry->setObjectName(QStringLiteral("slbl_chooseEntry"));

        gridLayout->addWidget(slbl_chooseEntry, 0, 0, 1, 1);


        retranslateUi(EntriesDialog);

        QMetaObject::connectSlotsByName(EntriesDialog);
    } // setupUi

    void retranslateUi(QDialog *EntriesDialog)
    {
        EntriesDialog->setWindowTitle(QApplication::translate("EntriesDialog", "Dialog", nullptr));
        slbl_chooseEntry->setText(QApplication::translate("EntriesDialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; font-weight:600;\">Choose saved entry</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EntriesDialog: public Ui_EntriesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ENTRIES_DIALOG_H
