/********************************************************************************
** Form generated from reading UI file 'entry_dump_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ENTRY_DUMP_DIALOG_H
#define UI_ENTRY_DUMP_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>

QT_BEGIN_NAMESPACE

class Ui_EntryDumpDialog
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QListView *lv_entries;
    QLabel *label;
    QLineEdit *ledit_entryName;

    void setupUi(QDialog *EntryDumpDialog)
    {
        if (EntryDumpDialog->objectName().isEmpty())
            EntryDumpDialog->setObjectName(QStringLiteral("EntryDumpDialog"));
        EntryDumpDialog->resize(400, 300);
        gridLayout = new QGridLayout(EntryDumpDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonBox = new QDialogButtonBox(EntryDumpDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Save);

        gridLayout->addWidget(buttonBox, 2, 1, 1, 1);

        lv_entries = new QListView(EntryDumpDialog);
        lv_entries->setObjectName(QStringLiteral("lv_entries"));

        gridLayout->addWidget(lv_entries, 1, 0, 1, 2);

        label = new QLabel(EntryDumpDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 2);

        ledit_entryName = new QLineEdit(EntryDumpDialog);
        ledit_entryName->setObjectName(QStringLiteral("ledit_entryName"));

        gridLayout->addWidget(ledit_entryName, 2, 0, 1, 1);


        retranslateUi(EntryDumpDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), EntryDumpDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), EntryDumpDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(EntryDumpDialog);
    } // setupUi

    void retranslateUi(QDialog *EntryDumpDialog)
    {
        EntryDumpDialog->setWindowTitle(QApplication::translate("EntryDumpDialog", "Dialog", nullptr));
        label->setText(QApplication::translate("EntryDumpDialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; font-weight:600;\">Choose saved entry or create new</span></p></body></html>", nullptr));
        ledit_entryName->setPlaceholderText(QApplication::translate("EntryDumpDialog", "<Enter entry name>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EntryDumpDialog: public Ui_EntryDumpDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ENTRY_DUMP_DIALOG_H
