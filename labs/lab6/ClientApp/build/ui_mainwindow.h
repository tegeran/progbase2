/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *act_open;
    QAction *act_saveTo;
    QAction *act_tmpsave;
    QAction *act_clear;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *pb_add;
    QPushButton *pb_delete;
    QPushButton *pb_edit;
    QSpacerItem *horizontalSpacer;
    QLabel *slbl_objects;
    QListView *lv_names;
    QGroupBox *gbox_provider;
    QFormLayout *formLayout;
    QLabel *slbl_name;
    QLabel *lbl_name;
    QLabel *slbl_tariff;
    QLabel *lbl_tariff;
    QLabel *slbl_users;
    QLabel *lbl_users;
    QLabel *slbl_loss;
    QLabel *lbl_loss;
    QGroupBox *gbox_cloud;
    QFormLayout *formLayout_2;
    QLabel *slbl_cloudCapacity;
    QLabel *lbl_cloudCapacity;
    QLabel *lbl_cloudTariff;
    QLabel *slbl_cloudTariff;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpinBox *sbox_exec;
    QPushButton *pb_execute;
    QLabel *lbl_filter;
    QListView *lv_execs;
    QLabel *slbl_usersOnline;
    QListView *lv_usersOnline;
    QMenuBar *menuBar;
    QMenu *menu_file;
    QMenu *menu_edit;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(787, 491);
        MainWindow->setStyleSheet(QLatin1String("\n"
"QWidget{ background-color: rgb(76, 75, 75); }\n"
"QWidget:disabled{ 	color: rgb(154, 148, 148);  }\n"
"QWidget:enabled {	 color: white;  }\n"
"QPushButton:enabled {	background-color: rgb(34, 33, 33); }\n"
"QPushButton:disabled {	background-color: rgb(46, 52, 54); }\n"
"QPushButton:enabled:hover{ background-color: rgb(29, 14, 58); }\n"
"QIcon:disabled{ color: black; background-color: black;}\n"
""));
        act_open = new QAction(MainWindow);
        act_open->setObjectName(QStringLiteral("act_open"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/general/icons/open-folder-with-document.png"), QSize(), QIcon::Normal, QIcon::Off);
        act_open->setIcon(icon);
        act_saveTo = new QAction(MainWindow);
        act_saveTo->setObjectName(QStringLiteral("act_saveTo"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/general/icons/diskette.png"), QSize(), QIcon::Normal, QIcon::Off);
        act_saveTo->setIcon(icon1);
        act_tmpsave = new QAction(MainWindow);
        act_tmpsave->setObjectName(QStringLiteral("act_tmpsave"));
        act_clear = new QAction(MainWindow);
        act_clear->setObjectName(QStringLiteral("act_clear"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/general/icons/throw-to-paper-bin.png"), QSize(), QIcon::Normal, QIcon::Off);
        act_clear->setIcon(icon2);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        splitter->setChildrenCollapsible(false);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pb_add = new QPushButton(layoutWidget);
        pb_add->setObjectName(QStringLiteral("pb_add"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pb_add->sizePolicy().hasHeightForWidth());
        pb_add->setSizePolicy(sizePolicy);
        pb_add->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(pb_add);

        pb_delete = new QPushButton(layoutWidget);
        pb_delete->setObjectName(QStringLiteral("pb_delete"));
        pb_delete->setEnabled(false);
        sizePolicy.setHeightForWidth(pb_delete->sizePolicy().hasHeightForWidth());
        pb_delete->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(pb_delete);

        pb_edit = new QPushButton(layoutWidget);
        pb_edit->setObjectName(QStringLiteral("pb_edit"));
        pb_edit->setEnabled(false);
        sizePolicy.setHeightForWidth(pb_edit->sizePolicy().hasHeightForWidth());
        pb_edit->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(pb_edit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout);

        slbl_objects = new QLabel(layoutWidget);
        slbl_objects->setObjectName(QStringLiteral("slbl_objects"));
        slbl_objects->setStyleSheet(QStringLiteral(" font-size: 123.4em "));

        verticalLayout_2->addWidget(slbl_objects);

        lv_names = new QListView(layoutWidget);
        lv_names->setObjectName(QStringLiteral("lv_names"));
        lv_names->viewport()->setProperty("cursor", QVariant(QCursor(Qt::ArrowCursor)));
        lv_names->setMouseTracking(false);

        verticalLayout_2->addWidget(lv_names);

        gbox_provider = new QGroupBox(layoutWidget);
        gbox_provider->setObjectName(QStringLiteral("gbox_provider"));
        formLayout = new QFormLayout(gbox_provider);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        slbl_name = new QLabel(gbox_provider);
        slbl_name->setObjectName(QStringLiteral("slbl_name"));
        slbl_name->setTextFormat(Qt::AutoText);
        slbl_name->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(0, QFormLayout::LabelRole, slbl_name);

        lbl_name = new QLabel(gbox_provider);
        lbl_name->setObjectName(QStringLiteral("lbl_name"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lbl_name);

        slbl_tariff = new QLabel(gbox_provider);
        slbl_tariff->setObjectName(QStringLiteral("slbl_tariff"));
        slbl_tariff->setTextFormat(Qt::AutoText);
        slbl_tariff->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(1, QFormLayout::LabelRole, slbl_tariff);

        lbl_tariff = new QLabel(gbox_provider);
        lbl_tariff->setObjectName(QStringLiteral("lbl_tariff"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lbl_tariff);

        slbl_users = new QLabel(gbox_provider);
        slbl_users->setObjectName(QStringLiteral("slbl_users"));
        slbl_users->setTextFormat(Qt::AutoText);
        slbl_users->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(2, QFormLayout::LabelRole, slbl_users);

        lbl_users = new QLabel(gbox_provider);
        lbl_users->setObjectName(QStringLiteral("lbl_users"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lbl_users);

        slbl_loss = new QLabel(gbox_provider);
        slbl_loss->setObjectName(QStringLiteral("slbl_loss"));
        slbl_loss->setTextFormat(Qt::AutoText);
        slbl_loss->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(3, QFormLayout::LabelRole, slbl_loss);

        lbl_loss = new QLabel(gbox_provider);
        lbl_loss->setObjectName(QStringLiteral("lbl_loss"));

        formLayout->setWidget(3, QFormLayout::FieldRole, lbl_loss);

        gbox_cloud = new QGroupBox(gbox_provider);
        gbox_cloud->setObjectName(QStringLiteral("gbox_cloud"));
        gbox_cloud->setEnabled(false);
        gbox_cloud->setCheckable(false);
        gbox_cloud->setChecked(false);
        formLayout_2 = new QFormLayout(gbox_cloud);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        slbl_cloudCapacity = new QLabel(gbox_cloud);
        slbl_cloudCapacity->setObjectName(QStringLiteral("slbl_cloudCapacity"));
        slbl_cloudCapacity->setTextFormat(Qt::AutoText);
        slbl_cloudCapacity->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout_2->setWidget(2, QFormLayout::LabelRole, slbl_cloudCapacity);

        lbl_cloudCapacity = new QLabel(gbox_cloud);
        lbl_cloudCapacity->setObjectName(QStringLiteral("lbl_cloudCapacity"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, lbl_cloudCapacity);

        lbl_cloudTariff = new QLabel(gbox_cloud);
        lbl_cloudTariff->setObjectName(QStringLiteral("lbl_cloudTariff"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, lbl_cloudTariff);

        slbl_cloudTariff = new QLabel(gbox_cloud);
        slbl_cloudTariff->setObjectName(QStringLiteral("slbl_cloudTariff"));
        slbl_cloudTariff->setTextFormat(Qt::AutoText);
        slbl_cloudTariff->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout_2->setWidget(1, QFormLayout::LabelRole, slbl_cloudTariff);


        formLayout->setWidget(4, QFormLayout::SpanningRole, gbox_cloud);


        verticalLayout_2->addWidget(gbox_provider);

        splitter->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(splitter);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        sbox_exec = new QSpinBox(layoutWidget1);
        sbox_exec->setObjectName(QStringLiteral("sbox_exec"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(sbox_exec->sizePolicy().hasHeightForWidth());
        sbox_exec->setSizePolicy(sizePolicy1);
        sbox_exec->setMaximum(0);

        horizontalLayout_2->addWidget(sbox_exec);

        pb_execute = new QPushButton(layoutWidget1);
        pb_execute->setObjectName(QStringLiteral("pb_execute"));
        sizePolicy.setHeightForWidth(pb_execute->sizePolicy().hasHeightForWidth());
        pb_execute->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(pb_execute);


        verticalLayout->addLayout(horizontalLayout_2);

        lbl_filter = new QLabel(layoutWidget1);
        lbl_filter->setObjectName(QStringLiteral("lbl_filter"));

        verticalLayout->addWidget(lbl_filter);

        lv_execs = new QListView(layoutWidget1);
        lv_execs->setObjectName(QStringLiteral("lv_execs"));

        verticalLayout->addWidget(lv_execs);

        slbl_usersOnline = new QLabel(layoutWidget1);
        slbl_usersOnline->setObjectName(QStringLiteral("slbl_usersOnline"));

        verticalLayout->addWidget(slbl_usersOnline);

        lv_usersOnline = new QListView(layoutWidget1);
        lv_usersOnline->setObjectName(QStringLiteral("lv_usersOnline"));

        verticalLayout->addWidget(lv_usersOnline);

        splitter->addWidget(layoutWidget1);

        gridLayout->addWidget(splitter, 0, 1, 2, 2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 787, 22));
        menu_file = new QMenu(menuBar);
        menu_file->setObjectName(QStringLiteral("menu_file"));
        menu_edit = new QMenu(menuBar);
        menu_edit->setObjectName(QStringLiteral("menu_edit"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu_file->menuAction());
        menuBar->addAction(menu_edit->menuAction());
        menu_file->addAction(act_open);
        menu_file->addAction(act_saveTo);
        menu_edit->addAction(act_clear);
        mainToolBar->addAction(act_open);
        mainToolBar->addAction(act_saveTo);
        mainToolBar->addAction(act_clear);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        act_open->setText(QApplication::translate("MainWindow", "Open...", nullptr));
#ifndef QT_NO_TOOLTIP
        act_open->setToolTip(QApplication::translate("MainWindow", "Open new project from .json", nullptr));
#endif // QT_NO_TOOLTIP
        act_saveTo->setText(QApplication::translate("MainWindow", "Save to...", nullptr));
#ifndef QT_NO_TOOLTIP
        act_saveTo->setToolTip(QApplication::translate("MainWindow", "Save project to .json file", nullptr));
#endif // QT_NO_TOOLTIP
        act_tmpsave->setText(QApplication::translate("MainWindow", "Save", nullptr));
#ifndef QT_NO_TOOLTIP
        act_tmpsave->setToolTip(QApplication::translate("MainWindow", "Save current program state", nullptr));
#endif // QT_NO_TOOLTIP
        act_clear->setText(QApplication::translate("MainWindow", "Clear", nullptr));
#ifndef QT_NO_TOOLTIP
        act_clear->setToolTip(QApplication::translate("MainWindow", "Clear current project", nullptr));
#endif // QT_NO_TOOLTIP
        pb_add->setText(QApplication::translate("MainWindow", "Add", nullptr));
        pb_delete->setText(QApplication::translate("MainWindow", "Delete", nullptr));
        pb_edit->setText(QApplication::translate("MainWindow", "Edit", nullptr));
        slbl_objects->setText(QApplication::translate("MainWindow", "Objects:", nullptr));
        gbox_provider->setTitle(QApplication::translate("MainWindow", "Provider:", nullptr));
        slbl_name->setText(QApplication::translate("MainWindow", "<b>Name:</b>", nullptr));
        lbl_name->setText(QString());
        slbl_tariff->setText(QApplication::translate("MainWindow", "<b>Tariff:</b>", nullptr));
        lbl_tariff->setText(QString());
        slbl_users->setText(QApplication::translate("MainWindow", "<b>Users:</b>", nullptr));
        lbl_users->setText(QString());
        slbl_loss->setText(QApplication::translate("MainWindow", "<b>Packet loss:</b>", nullptr));
        lbl_loss->setText(QString());
        gbox_cloud->setTitle(QApplication::translate("MainWindow", "Data cloud", nullptr));
        slbl_cloudCapacity->setText(QApplication::translate("MainWindow", "<b>Capacity:</b>", nullptr));
        lbl_cloudCapacity->setText(QString());
        lbl_cloudTariff->setText(QString());
        slbl_cloudTariff->setText(QApplication::translate("MainWindow", "<b>Tariff:</b>", nullptr));
        pb_execute->setText(QApplication::translate("MainWindow", "Execute", nullptr));
#ifndef QT_NO_TOOLTIP
        lbl_filter->setToolTip(QApplication::translate("MainWindow", "Display X providers with the highest tariff", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_filter->setText(QApplication::translate("MainWindow", "Current tariff filter: 0", nullptr));
        slbl_usersOnline->setText(QApplication::translate("MainWindow", "Users online:", nullptr));
        menu_file->setTitle(QApplication::translate("MainWindow", "File", nullptr));
        menu_edit->setTitle(QApplication::translate("MainWindow", "Edit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
