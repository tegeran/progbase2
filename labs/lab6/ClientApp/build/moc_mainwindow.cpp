/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[42];
    char stringdata0[775];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 24), // "handle_model_dataChanged"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 12), // "QVector<int>"
QT_MOC_LITERAL(4, 50, 17), // "on_pb_add_clicked"
QT_MOC_LITERAL(5, 68, 20), // "on_pb_delete_clicked"
QT_MOC_LITERAL(6, 89, 18), // "on_pb_edit_clicked"
QT_MOC_LITERAL(7, 108, 25), // "on_lv_names_doubleClicked"
QT_MOC_LITERAL(8, 134, 5), // "index"
QT_MOC_LITERAL(9, 140, 25), // "on_lv_execs_doubleClicked"
QT_MOC_LITERAL(10, 166, 30), // "handle_lv_names_currentChanged"
QT_MOC_LITERAL(11, 197, 3), // "cur"
QT_MOC_LITERAL(12, 201, 4), // "prev"
QT_MOC_LITERAL(13, 206, 30), // "handle_lv_execs_currentChanged"
QT_MOC_LITERAL(14, 237, 21), // "on_pb_execute_clicked"
QT_MOC_LITERAL(15, 259, 21), // "on_act_open_triggered"
QT_MOC_LITERAL(16, 281, 23), // "on_act_saveTo_triggered"
QT_MOC_LITERAL(17, 305, 22), // "on_act_clear_triggered"
QT_MOC_LITERAL(18, 328, 25), // "handle_model_rowsInserted"
QT_MOC_LITERAL(19, 354, 6), // "parent"
QT_MOC_LITERAL(20, 361, 5), // "first"
QT_MOC_LITERAL(21, 367, 4), // "last"
QT_MOC_LITERAL(22, 372, 24), // "handle_model_rowsDeleted"
QT_MOC_LITERAL(23, 397, 20), // "handle_model_reseted"
QT_MOC_LITERAL(24, 418, 28), // "handle_lv_names_indexesMoved"
QT_MOC_LITERAL(25, 447, 25), // "on_sbox_exec_valueChanged"
QT_MOC_LITERAL(26, 473, 4), // "arg1"
QT_MOC_LITERAL(27, 478, 41), // "handle_client_receivedSavedEn..."
QT_MOC_LITERAL(28, 520, 12), // "entriesNames"
QT_MOC_LITERAL(29, 533, 41), // "handle_client_receivedSavedEn..."
QT_MOC_LITERAL(30, 575, 35), // "handle_client_updateReloadedE..."
QT_MOC_LITERAL(31, 611, 17), // "QVector<Provider>"
QT_MOC_LITERAL(32, 629, 9), // "providers"
QT_MOC_LITERAL(33, 639, 27), // "handle_client_userConnected"
QT_MOC_LITERAL(34, 667, 4), // "User"
QT_MOC_LITERAL(35, 672, 4), // "user"
QT_MOC_LITERAL(36, 677, 30), // "handle_client_userDisconnected"
QT_MOC_LITERAL(37, 708, 34), // "handle_client_updateEditedPro..."
QT_MOC_LITERAL(38, 743, 8), // "Provider"
QT_MOC_LITERAL(39, 752, 8), // "provider"
QT_MOC_LITERAL(40, 761, 4), // "id_t"
QT_MOC_LITERAL(41, 766, 8) // "position"

    },
    "MainWindow\0handle_model_dataChanged\0"
    "\0QVector<int>\0on_pb_add_clicked\0"
    "on_pb_delete_clicked\0on_pb_edit_clicked\0"
    "on_lv_names_doubleClicked\0index\0"
    "on_lv_execs_doubleClicked\0"
    "handle_lv_names_currentChanged\0cur\0"
    "prev\0handle_lv_execs_currentChanged\0"
    "on_pb_execute_clicked\0on_act_open_triggered\0"
    "on_act_saveTo_triggered\0on_act_clear_triggered\0"
    "handle_model_rowsInserted\0parent\0first\0"
    "last\0handle_model_rowsDeleted\0"
    "handle_model_reseted\0handle_lv_names_indexesMoved\0"
    "on_sbox_exec_valueChanged\0arg1\0"
    "handle_client_receivedSavedEntriesForOpen\0"
    "entriesNames\0handle_client_receivedSavedEntriesForDump\0"
    "handle_client_updateReloadedEntries\0"
    "QVector<Provider>\0providers\0"
    "handle_client_userConnected\0User\0user\0"
    "handle_client_userDisconnected\0"
    "handle_client_updateEditedProvider\0"
    "Provider\0provider\0id_t\0position"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,  149,    2, 0x08 /* Private */,
       1,    2,  156,    2, 0x28 /* Private | MethodCloned */,
       1,    1,  161,    2, 0x28 /* Private | MethodCloned */,
       1,    0,  164,    2, 0x28 /* Private | MethodCloned */,
       4,    0,  165,    2, 0x08 /* Private */,
       5,    0,  166,    2, 0x08 /* Private */,
       6,    0,  167,    2, 0x08 /* Private */,
       7,    1,  168,    2, 0x08 /* Private */,
       9,    1,  171,    2, 0x08 /* Private */,
      10,    2,  174,    2, 0x08 /* Private */,
      13,    2,  179,    2, 0x08 /* Private */,
      13,    1,  184,    2, 0x28 /* Private | MethodCloned */,
      14,    0,  187,    2, 0x08 /* Private */,
      15,    0,  188,    2, 0x08 /* Private */,
      16,    0,  189,    2, 0x08 /* Private */,
      17,    0,  190,    2, 0x08 /* Private */,
      18,    3,  191,    2, 0x08 /* Private */,
      22,    3,  198,    2, 0x08 /* Private */,
      23,    0,  205,    2, 0x08 /* Private */,
      24,    0,  206,    2, 0x08 /* Private */,
      25,    1,  207,    2, 0x08 /* Private */,
      27,    1,  210,    2, 0x08 /* Private */,
      29,    1,  213,    2, 0x08 /* Private */,
      30,    1,  216,    2, 0x08 /* Private */,
      33,    1,  219,    2, 0x08 /* Private */,
      36,    1,  222,    2, 0x08 /* Private */,
      37,    2,  225,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex, 0x80000000 | 3,    2,    2,    2,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,    2,    2,
    QMetaType::Void, QMetaType::QModelIndex,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    8,
    QMetaType::Void, QMetaType::QModelIndex,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,   11,   12,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,   11,   12,
    QMetaType::Void, QMetaType::QModelIndex,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::Int, QMetaType::Int,   19,   20,   21,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::Int, QMetaType::Int,   19,   20,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   26,
    QMetaType::Void, QMetaType::QStringList,   28,
    QMetaType::Void, QMetaType::QStringList,   28,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void, 0x80000000 | 34,   35,
    QMetaType::Void, 0x80000000 | 34,   35,
    QMetaType::Void, 0x80000000 | 38, 0x80000000 | 40,   39,   41,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->handle_model_dataChanged((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3]))); break;
        case 1: _t->handle_model_dataChanged((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 2: _t->handle_model_dataChanged((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 3: _t->handle_model_dataChanged(); break;
        case 4: _t->on_pb_add_clicked(); break;
        case 5: _t->on_pb_delete_clicked(); break;
        case 6: _t->on_pb_edit_clicked(); break;
        case 7: _t->on_lv_names_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 8: _t->on_lv_execs_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 9: _t->handle_lv_names_currentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 10: _t->handle_lv_execs_currentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 11: _t->handle_lv_execs_currentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 12: _t->on_pb_execute_clicked(); break;
        case 13: _t->on_act_open_triggered(); break;
        case 14: _t->on_act_saveTo_triggered(); break;
        case 15: _t->on_act_clear_triggered(); break;
        case 16: _t->handle_model_rowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 17: _t->handle_model_rowsDeleted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 18: _t->handle_model_reseted(); break;
        case 19: _t->handle_lv_names_indexesMoved(); break;
        case 20: _t->on_sbox_exec_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->handle_client_receivedSavedEntriesForOpen((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 22: _t->handle_client_receivedSavedEntriesForDump((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 23: _t->handle_client_updateReloadedEntries((*reinterpret_cast< QVector<Provider>(*)>(_a[1]))); break;
        case 24: _t->handle_client_userConnected((*reinterpret_cast< const User(*)>(_a[1]))); break;
        case 25: _t->handle_client_userDisconnected((*reinterpret_cast< const User(*)>(_a[1]))); break;
        case 26: _t->handle_client_updateEditedProvider((*reinterpret_cast< Provider(*)>(_a[1])),(*reinterpret_cast< const id_t(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Provider> >(); break;
            }
            break;
        case 26:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
