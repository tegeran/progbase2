/********************************************************************************
** Form generated from reading UI file 'user_name_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_NAME_DIALOG_H
#define UI_USER_NAME_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_UserNameDialog
{
public:
    QFormLayout *formLayout;
    QLabel *slbl_providererr;
    QLabel *slbl_enterName;
    QLineEdit *ledit_name;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *UserNameDialog)
    {
        if (UserNameDialog->objectName().isEmpty())
            UserNameDialog->setObjectName(QStringLiteral("UserNameDialog"));
        UserNameDialog->resize(424, 129);
        UserNameDialog->setStyleSheet(QLatin1String("\n"
"QWidget{ background-color: rgb(76, 75, 75); }\n"
"QWidget:disabled{ 	color: rgb(154, 148, 148);  }\n"
"QWidget:enabled {	 color: white;  }\n"
"QPushButton:enabled {	background-color: rgb(34, 33, 33); }\n"
"QPushButton:disabled {	background-color: rgb(46, 52, 54); }\n"
"QPushButton:enabled:hover{ background-color: rgb(29, 14, 58); }\n"
"QIcon:disabled{ color: black; background-color: black;}\n"
""));
        UserNameDialog->setSizeGripEnabled(true);
        UserNameDialog->setModal(false);
        formLayout = new QFormLayout(UserNameDialog);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        slbl_providererr = new QLabel(UserNameDialog);
        slbl_providererr->setObjectName(QStringLiteral("slbl_providererr"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, slbl_providererr);

        slbl_enterName = new QLabel(UserNameDialog);
        slbl_enterName->setObjectName(QStringLiteral("slbl_enterName"));

        formLayout->setWidget(1, QFormLayout::LabelRole, slbl_enterName);

        ledit_name = new QLineEdit(UserNameDialog);
        ledit_name->setObjectName(QStringLiteral("ledit_name"));

        formLayout->setWidget(1, QFormLayout::FieldRole, ledit_name);

        buttonBox = new QDialogButtonBox(UserNameDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        formLayout->setWidget(2, QFormLayout::FieldRole, buttonBox);


        retranslateUi(UserNameDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), UserNameDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), UserNameDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(UserNameDialog);
    } // setupUi

    void retranslateUi(QDialog *UserNameDialog)
    {
        UserNameDialog->setWindowTitle(QApplication::translate("UserNameDialog", "Dialog", nullptr));
        slbl_providererr->setText(QApplication::translate("UserNameDialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:22pt; font-weight:600;\">Providererr</span></p></body></html>", nullptr));
        slbl_enterName->setText(QApplication::translate("UserNameDialog", "Enter your name:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserNameDialog: public Ui_UserNameDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_NAME_DIALOG_H
