/********************************************************************************
** Form generated from reading UI file 'connection_ip_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONNECTION_IP_DIALOG_H
#define UI_CONNECTION_IP_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ConnectionIpDialog
{
public:
    QFormLayout *formLayout;
    QLabel *slbl_serverAddress;
    QLineEdit *ledit_ipv4;
    QLabel *slbl_port;
    QSpinBox *sbox_port;
    QDialogButtonBox *buttonBox;
    QLabel *slbl_title;

    void setupUi(QDialog *ConnectionIpDialog)
    {
        if (ConnectionIpDialog->objectName().isEmpty())
            ConnectionIpDialog->setObjectName(QStringLiteral("ConnectionIpDialog"));
        ConnectionIpDialog->resize(403, 152);
        formLayout = new QFormLayout(ConnectionIpDialog);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        slbl_serverAddress = new QLabel(ConnectionIpDialog);
        slbl_serverAddress->setObjectName(QStringLiteral("slbl_serverAddress"));

        formLayout->setWidget(2, QFormLayout::LabelRole, slbl_serverAddress);

        ledit_ipv4 = new QLineEdit(ConnectionIpDialog);
        ledit_ipv4->setObjectName(QStringLiteral("ledit_ipv4"));
        ledit_ipv4->setMaxLength(32767);

        formLayout->setWidget(2, QFormLayout::FieldRole, ledit_ipv4);

        slbl_port = new QLabel(ConnectionIpDialog);
        slbl_port->setObjectName(QStringLiteral("slbl_port"));

        formLayout->setWidget(3, QFormLayout::LabelRole, slbl_port);

        sbox_port = new QSpinBox(ConnectionIpDialog);
        sbox_port->setObjectName(QStringLiteral("sbox_port"));

        formLayout->setWidget(3, QFormLayout::FieldRole, sbox_port);

        buttonBox = new QDialogButtonBox(ConnectionIpDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::NoButton);
        buttonBox->setCenterButtons(false);

        formLayout->setWidget(4, QFormLayout::SpanningRole, buttonBox);

        slbl_title = new QLabel(ConnectionIpDialog);
        slbl_title->setObjectName(QStringLiteral("slbl_title"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, slbl_title);


        retranslateUi(ConnectionIpDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ConnectionIpDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ConnectionIpDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ConnectionIpDialog);
    } // setupUi

    void retranslateUi(QDialog *ConnectionIpDialog)
    {
        ConnectionIpDialog->setWindowTitle(QApplication::translate("ConnectionIpDialog", "Dialog", nullptr));
        slbl_serverAddress->setText(QApplication::translate("ConnectionIpDialog", "Sever address:", nullptr));
        ledit_ipv4->setInputMask(QString());
        ledit_ipv4->setText(QString());
        ledit_ipv4->setPlaceholderText(QString());
        slbl_port->setText(QApplication::translate("ConnectionIpDialog", "Port", nullptr));
        slbl_title->setText(QApplication::translate("ConnectionIpDialog", "<html><head/><body><p align=\"center\"><br/></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ConnectionIpDialog: public Ui_ConnectionIpDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONNECTION_IP_DIALOG_H
