/****************************************************************************
** Meta object code from reading C++ file 'sessions_list_dialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../sessions_list_dialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sessions_list_dialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SessionsListDialog_t {
    QByteArrayData data[17];
    char stringdata0[270];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SessionsListDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SessionsListDialog_t qt_meta_stringdata_SessionsListDialog = {
    {
QT_MOC_LITERAL(0, 0, 18), // "SessionsListDialog"
QT_MOC_LITERAL(1, 19, 17), // "connectionRequest"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 4), // "id_t"
QT_MOC_LITERAL(4, 43, 7), // "session"
QT_MOC_LITERAL(5, 51, 20), // "createSessionRequest"
QT_MOC_LITERAL(6, 72, 4), // "name"
QT_MOC_LITERAL(7, 77, 17), // "setOnlineSessions"
QT_MOC_LITERAL(8, 95, 25), // "QHash<id_t,ClientSession>"
QT_MOC_LITERAL(9, 121, 8), // "sessions"
QT_MOC_LITERAL(10, 130, 16), // "addOnlineSession"
QT_MOC_LITERAL(11, 147, 13), // "ClientSession"
QT_MOC_LITERAL(12, 161, 19), // "deleteOnlineSession"
QT_MOC_LITERAL(13, 181, 2), // "id"
QT_MOC_LITERAL(14, 184, 21), // "on_pb_connect_clicked"
QT_MOC_LITERAL(15, 206, 35), // "handle_current_sessionIndex_c..."
QT_MOC_LITERAL(16, 242, 27) // "on_pb_createSession_clicked"

    },
    "SessionsListDialog\0connectionRequest\0"
    "\0id_t\0session\0createSessionRequest\0"
    "name\0setOnlineSessions\0QHash<id_t,ClientSession>\0"
    "sessions\0addOnlineSession\0ClientSession\0"
    "deleteOnlineSession\0id\0on_pb_connect_clicked\0"
    "handle_current_sessionIndex_changed\0"
    "on_pb_createSession_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SessionsListDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    1,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,   60,    2, 0x0a /* Public */,
      10,    1,   63,    2, 0x0a /* Public */,
      12,    1,   66,    2, 0x0a /* Public */,
      14,    0,   69,    2, 0x08 /* Private */,
      15,    0,   70,    2, 0x08 /* Private */,
      16,    0,   71,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString,    6,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 11,    4,
    QMetaType::Void, 0x80000000 | 3,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SessionsListDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SessionsListDialog *_t = static_cast<SessionsListDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connectionRequest((*reinterpret_cast< id_t(*)>(_a[1]))); break;
        case 1: _t->createSessionRequest((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->setOnlineSessions((*reinterpret_cast< const QHash<id_t,ClientSession>(*)>(_a[1]))); break;
        case 3: _t->addOnlineSession((*reinterpret_cast< ClientSession(*)>(_a[1]))); break;
        case 4: _t->deleteOnlineSession((*reinterpret_cast< const id_t(*)>(_a[1]))); break;
        case 5: _t->on_pb_connect_clicked(); break;
        case 6: _t->handle_current_sessionIndex_changed(); break;
        case 7: _t->on_pb_createSession_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SessionsListDialog::*_t)(id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SessionsListDialog::connectionRequest)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SessionsListDialog::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SessionsListDialog::createSessionRequest)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SessionsListDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SessionsListDialog.data,
      qt_meta_data_SessionsListDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SessionsListDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SessionsListDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SessionsListDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int SessionsListDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void SessionsListDialog::connectionRequest(id_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SessionsListDialog::createSessionRequest(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
