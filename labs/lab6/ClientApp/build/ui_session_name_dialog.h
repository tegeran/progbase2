/********************************************************************************
** Form generated from reading UI file 'session_name_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SESSION_NAME_DIALOG_H
#define UI_SESSION_NAME_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_SessionNameDialog
{
public:
    QFormLayout *formLayout;
    QLabel *slbl_sessionName;
    QLineEdit *ledit_sessionName;
    QDialogButtonBox *buttonBox;
    QLabel *slbl_creatingNewSession;

    void setupUi(QDialog *SessionNameDialog)
    {
        if (SessionNameDialog->objectName().isEmpty())
            SessionNameDialog->setObjectName(QStringLiteral("SessionNameDialog"));
        SessionNameDialog->resize(402, 117);
        formLayout = new QFormLayout(SessionNameDialog);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        slbl_sessionName = new QLabel(SessionNameDialog);
        slbl_sessionName->setObjectName(QStringLiteral("slbl_sessionName"));

        formLayout->setWidget(1, QFormLayout::LabelRole, slbl_sessionName);

        ledit_sessionName = new QLineEdit(SessionNameDialog);
        ledit_sessionName->setObjectName(QStringLiteral("ledit_sessionName"));

        formLayout->setWidget(1, QFormLayout::FieldRole, ledit_sessionName);

        buttonBox = new QDialogButtonBox(SessionNameDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(2, QFormLayout::SpanningRole, buttonBox);

        slbl_creatingNewSession = new QLabel(SessionNameDialog);
        slbl_creatingNewSession->setObjectName(QStringLiteral("slbl_creatingNewSession"));

        formLayout->setWidget(0, QFormLayout::FieldRole, slbl_creatingNewSession);


        retranslateUi(SessionNameDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SessionNameDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SessionNameDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SessionNameDialog);
    } // setupUi

    void retranslateUi(QDialog *SessionNameDialog)
    {
        SessionNameDialog->setWindowTitle(QApplication::translate("SessionNameDialog", "Dialog", nullptr));
        slbl_sessionName->setText(QApplication::translate("SessionNameDialog", "Session name:", nullptr));
        slbl_creatingNewSession->setText(QApplication::translate("SessionNameDialog", "<html><head/><body><p><span style=\" font-size:16pt;\">Creating new session</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SessionNameDialog: public Ui_SessionNameDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SESSION_NAME_DIALOG_H
