/****************************************************************************
** Meta object code from reading C++ file 'client.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../client.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'client.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Client_t {
    QByteArrayData data[61];
    char stringdata0[999];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Client_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Client_t qt_meta_stringdata_Client = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Client"
QT_MOC_LITERAL(1, 7, 21), // "establishedConnection"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 18), // "connectedToSession"
QT_MOC_LITERAL(4, 49, 13), // "ClientSession"
QT_MOC_LITERAL(5, 63, 7), // "session"
QT_MOC_LITERAL(6, 71, 17), // "QVector<Provider>"
QT_MOC_LITERAL(7, 89, 9), // "providers"
QT_MOC_LITERAL(8, 99, 13), // "QVector<User>"
QT_MOC_LITERAL(9, 113, 11), // "usersOnline"
QT_MOC_LITERAL(10, 125, 5), // "error"
QT_MOC_LITERAL(11, 131, 6), // "reason"
QT_MOC_LITERAL(12, 138, 19), // "establishedSelfUser"
QT_MOC_LITERAL(13, 158, 31), // "receivedOnlineSessionsHashTable"
QT_MOC_LITERAL(14, 190, 25), // "QHash<id_t,ClientSession>"
QT_MOC_LITERAL(15, 216, 8), // "sessions"
QT_MOC_LITERAL(16, 225, 20), // "receivedSavedEntries"
QT_MOC_LITERAL(17, 246, 9), // "filenames"
QT_MOC_LITERAL(18, 256, 22), // "updateClearedProviders"
QT_MOC_LITERAL(19, 279, 14), // "createdSession"
QT_MOC_LITERAL(20, 294, 18), // "updateAddedSession"
QT_MOC_LITERAL(21, 313, 20), // "updateDeletedSession"
QT_MOC_LITERAL(22, 334, 4), // "id_t"
QT_MOC_LITERAL(23, 339, 9), // "sessionId"
QT_MOC_LITERAL(24, 349, 21), // "updateReloadedEntries"
QT_MOC_LITERAL(25, 371, 19), // "updateAddedProvider"
QT_MOC_LITERAL(26, 391, 8), // "Provider"
QT_MOC_LITERAL(27, 400, 8), // "provider"
QT_MOC_LITERAL(28, 409, 5), // "index"
QT_MOC_LITERAL(29, 415, 25), // "successfullyAddedProvider"
QT_MOC_LITERAL(30, 441, 20), // "updateEditedProvider"
QT_MOC_LITERAL(31, 462, 26), // "successfullyEditedProvider"
QT_MOC_LITERAL(32, 489, 21), // "updateDeletedProvider"
QT_MOC_LITERAL(33, 511, 27), // "successfullyDeletedProvider"
QT_MOC_LITERAL(34, 539, 19), // "updateUserConnected"
QT_MOC_LITERAL(35, 559, 4), // "User"
QT_MOC_LITERAL(36, 564, 4), // "user"
QT_MOC_LITERAL(37, 569, 22), // "updateUserDisconnected"
QT_MOC_LITERAL(38, 592, 19), // "updateDumpedEntries"
QT_MOC_LITERAL(39, 612, 8), // "filename"
QT_MOC_LITERAL(40, 621, 22), // "disconnectedFromServer"
QT_MOC_LITERAL(41, 644, 10), // "errorAbort"
QT_MOC_LITERAL(42, 655, 30), // "requireOnlineSessionsHashTable"
QT_MOC_LITERAL(43, 686, 17), // "requireNewSession"
QT_MOC_LITERAL(44, 704, 4), // "name"
QT_MOC_LITERAL(45, 709, 7), // "setUser"
QT_MOC_LITERAL(46, 717, 24), // "requireSessionConnection"
QT_MOC_LITERAL(47, 742, 19), // "requireSavedEntries"
QT_MOC_LITERAL(48, 762, 18), // "requireDumpEntries"
QT_MOC_LITERAL(49, 781, 18), // "requireLoadEntries"
QT_MOC_LITERAL(50, 800, 18), // "requireAddProvider"
QT_MOC_LITERAL(51, 819, 8), // "position"
QT_MOC_LITERAL(52, 828, 21), // "requireDeleteProvider"
QT_MOC_LITERAL(53, 850, 19), // "requireEditProvider"
QT_MOC_LITERAL(54, 870, 21), // "requireClearProviders"
QT_MOC_LITERAL(55, 892, 19), // "on_socket_connected"
QT_MOC_LITERAL(56, 912, 15), // "on_socket_error"
QT_MOC_LITERAL(57, 928, 22), // "on_socket_disconnected"
QT_MOC_LITERAL(58, 951, 27), // "on_socket_receivedNetPacket"
QT_MOC_LITERAL(59, 979, 9), // "NetPacket"
QT_MOC_LITERAL(60, 989, 9) // "netpacket"

    },
    "Client\0establishedConnection\0\0"
    "connectedToSession\0ClientSession\0"
    "session\0QVector<Provider>\0providers\0"
    "QVector<User>\0usersOnline\0error\0reason\0"
    "establishedSelfUser\0receivedOnlineSessionsHashTable\0"
    "QHash<id_t,ClientSession>\0sessions\0"
    "receivedSavedEntries\0filenames\0"
    "updateClearedProviders\0createdSession\0"
    "updateAddedSession\0updateDeletedSession\0"
    "id_t\0sessionId\0updateReloadedEntries\0"
    "updateAddedProvider\0Provider\0provider\0"
    "index\0successfullyAddedProvider\0"
    "updateEditedProvider\0successfullyEditedProvider\0"
    "updateDeletedProvider\0successfullyDeletedProvider\0"
    "updateUserConnected\0User\0user\0"
    "updateUserDisconnected\0updateDumpedEntries\0"
    "filename\0disconnectedFromServer\0"
    "errorAbort\0requireOnlineSessionsHashTable\0"
    "requireNewSession\0name\0setUser\0"
    "requireSessionConnection\0requireSavedEntries\0"
    "requireDumpEntries\0requireLoadEntries\0"
    "requireAddProvider\0position\0"
    "requireDeleteProvider\0requireEditProvider\0"
    "requireClearProviders\0on_socket_connected\0"
    "on_socket_error\0on_socket_disconnected\0"
    "on_socket_receivedNetPacket\0NetPacket\0"
    "netpacket"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Client[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      37,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      21,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  199,    2, 0x06 /* Public */,
       3,    3,  200,    2, 0x06 /* Public */,
      10,    1,  207,    2, 0x06 /* Public */,
      12,    0,  210,    2, 0x06 /* Public */,
      13,    1,  211,    2, 0x06 /* Public */,
      16,    1,  214,    2, 0x06 /* Public */,
      18,    0,  217,    2, 0x06 /* Public */,
      19,    1,  218,    2, 0x06 /* Public */,
      20,    1,  221,    2, 0x06 /* Public */,
      21,    1,  224,    2, 0x06 /* Public */,
      24,    1,  227,    2, 0x06 /* Public */,
      25,    2,  230,    2, 0x06 /* Public */,
      29,    2,  235,    2, 0x06 /* Public */,
      30,    2,  240,    2, 0x06 /* Public */,
      31,    2,  245,    2, 0x06 /* Public */,
      32,    1,  250,    2, 0x06 /* Public */,
      33,    1,  253,    2, 0x06 /* Public */,
      34,    1,  256,    2, 0x06 /* Public */,
      37,    1,  259,    2, 0x06 /* Public */,
      38,    1,  262,    2, 0x06 /* Public */,
      40,    0,  265,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      41,    1,  266,    2, 0x0a /* Public */,
      42,    0,  269,    2, 0x0a /* Public */,
      43,    1,  270,    2, 0x0a /* Public */,
      45,    1,  273,    2, 0x0a /* Public */,
      46,    1,  276,    2, 0x0a /* Public */,
      47,    0,  279,    2, 0x0a /* Public */,
      48,    1,  280,    2, 0x0a /* Public */,
      49,    1,  283,    2, 0x0a /* Public */,
      50,    2,  286,    2, 0x0a /* Public */,
      52,    1,  291,    2, 0x0a /* Public */,
      53,    2,  294,    2, 0x0a /* Public */,
      54,    0,  299,    2, 0x0a /* Public */,
      55,    0,  300,    2, 0x08 /* Private */,
      56,    0,  301,    2, 0x08 /* Private */,
      57,    0,  302,    2, 0x08 /* Private */,
      58,    1,  303,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 6, 0x80000000 | 8,    5,    7,    9,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, QMetaType::QStringList,   17,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 26, 0x80000000 | 22,   27,   28,
    QMetaType::Void, 0x80000000 | 26, 0x80000000 | 22,   27,   28,
    QMetaType::Void, 0x80000000 | 26, 0x80000000 | 22,   27,   28,
    QMetaType::Void, 0x80000000 | 26, 0x80000000 | 22,   27,   28,
    QMetaType::Void, 0x80000000 | 22,   28,
    QMetaType::Void, 0x80000000 | 22,   28,
    QMetaType::Void, 0x80000000 | 35,   36,
    QMetaType::Void, 0x80000000 | 35,   36,
    QMetaType::Void, QMetaType::QString,   39,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   44,
    QMetaType::Void, 0x80000000 | 35,   36,
    QMetaType::Void, 0x80000000 | 22,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   39,
    QMetaType::Void, QMetaType::QString,   39,
    QMetaType::Void, 0x80000000 | 26, 0x80000000 | 22,   27,   51,
    QMetaType::Void, 0x80000000 | 22,   51,
    QMetaType::Void, 0x80000000 | 26, 0x80000000 | 22,   27,   51,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 59,   60,

       0        // eod
};

void Client::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Client *_t = static_cast<Client *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->establishedConnection(); break;
        case 1: _t->connectedToSession((*reinterpret_cast< ClientSession(*)>(_a[1])),(*reinterpret_cast< QVector<Provider>(*)>(_a[2])),(*reinterpret_cast< QVector<User>(*)>(_a[3]))); break;
        case 2: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->establishedSelfUser(); break;
        case 4: _t->receivedOnlineSessionsHashTable((*reinterpret_cast< QHash<id_t,ClientSession>(*)>(_a[1]))); break;
        case 5: _t->receivedSavedEntries((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 6: _t->updateClearedProviders(); break;
        case 7: _t->createdSession((*reinterpret_cast< ClientSession(*)>(_a[1]))); break;
        case 8: _t->updateAddedSession((*reinterpret_cast< ClientSession(*)>(_a[1]))); break;
        case 9: _t->updateDeletedSession((*reinterpret_cast< id_t(*)>(_a[1]))); break;
        case 10: _t->updateReloadedEntries((*reinterpret_cast< QVector<Provider>(*)>(_a[1]))); break;
        case 11: _t->updateAddedProvider((*reinterpret_cast< Provider(*)>(_a[1])),(*reinterpret_cast< id_t(*)>(_a[2]))); break;
        case 12: _t->successfullyAddedProvider((*reinterpret_cast< Provider(*)>(_a[1])),(*reinterpret_cast< id_t(*)>(_a[2]))); break;
        case 13: _t->updateEditedProvider((*reinterpret_cast< Provider(*)>(_a[1])),(*reinterpret_cast< id_t(*)>(_a[2]))); break;
        case 14: _t->successfullyEditedProvider((*reinterpret_cast< Provider(*)>(_a[1])),(*reinterpret_cast< id_t(*)>(_a[2]))); break;
        case 15: _t->updateDeletedProvider((*reinterpret_cast< id_t(*)>(_a[1]))); break;
        case 16: _t->successfullyDeletedProvider((*reinterpret_cast< id_t(*)>(_a[1]))); break;
        case 17: _t->updateUserConnected((*reinterpret_cast< User(*)>(_a[1]))); break;
        case 18: _t->updateUserDisconnected((*reinterpret_cast< User(*)>(_a[1]))); break;
        case 19: _t->updateDumpedEntries((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 20: _t->disconnectedFromServer(); break;
        case 21: _t->errorAbort((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 22: _t->requireOnlineSessionsHashTable(); break;
        case 23: _t->requireNewSession((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 24: _t->setUser((*reinterpret_cast< const User(*)>(_a[1]))); break;
        case 25: _t->requireSessionConnection((*reinterpret_cast< const id_t(*)>(_a[1]))); break;
        case 26: _t->requireSavedEntries(); break;
        case 27: _t->requireDumpEntries((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 28: _t->requireLoadEntries((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 29: _t->requireAddProvider((*reinterpret_cast< const Provider(*)>(_a[1])),(*reinterpret_cast< const id_t(*)>(_a[2]))); break;
        case 30: _t->requireDeleteProvider((*reinterpret_cast< const id_t(*)>(_a[1]))); break;
        case 31: _t->requireEditProvider((*reinterpret_cast< const Provider(*)>(_a[1])),(*reinterpret_cast< const id_t(*)>(_a[2]))); break;
        case 32: _t->requireClearProviders(); break;
        case 33: _t->on_socket_connected(); break;
        case 34: _t->on_socket_error(); break;
        case 35: _t->on_socket_disconnected(); break;
        case 36: _t->on_socket_receivedNetPacket((*reinterpret_cast< const NetPacket(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Provider> >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientSession >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Provider> >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        case 29:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        case 31:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Provider >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (Client::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::establishedConnection)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Client::*_t)(ClientSession , QVector<Provider> , QVector<User> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::connectedToSession)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Client::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::error)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Client::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::establishedSelfUser)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (Client::*_t)(QHash<id_t,ClientSession> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::receivedOnlineSessionsHashTable)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (Client::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::receivedSavedEntries)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (Client::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateClearedProviders)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (Client::*_t)(ClientSession );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::createdSession)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (Client::*_t)(ClientSession );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateAddedSession)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (Client::*_t)(id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateDeletedSession)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (Client::*_t)(QVector<Provider> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateReloadedEntries)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (Client::*_t)(Provider , id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateAddedProvider)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (Client::*_t)(Provider , id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::successfullyAddedProvider)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (Client::*_t)(Provider , id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateEditedProvider)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (Client::*_t)(Provider , id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::successfullyEditedProvider)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (Client::*_t)(id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateDeletedProvider)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (Client::*_t)(id_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::successfullyDeletedProvider)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (Client::*_t)(User );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateUserConnected)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (Client::*_t)(User );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateUserDisconnected)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (Client::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::updateDumpedEntries)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (Client::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Client::disconnectedFromServer)) {
                *result = 20;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Client::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Client.data,
      qt_meta_data_Client,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Client::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Client::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Client.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Client::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 37)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 37;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 37)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 37;
    }
    return _id;
}

// SIGNAL 0
void Client::establishedConnection()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Client::connectedToSession(ClientSession _t1, QVector<Provider> _t2, QVector<User> _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Client::error(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Client::establishedSelfUser()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Client::receivedOnlineSessionsHashTable(QHash<id_t,ClientSession> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Client::receivedSavedEntries(QStringList _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Client::updateClearedProviders()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Client::createdSession(ClientSession _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Client::updateAddedSession(ClientSession _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Client::updateDeletedSession(id_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Client::updateReloadedEntries(QVector<Provider> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Client::updateAddedProvider(Provider _t1, id_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Client::successfullyAddedProvider(Provider _t1, id_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void Client::updateEditedProvider(Provider _t1, id_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void Client::successfullyEditedProvider(Provider _t1, id_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void Client::updateDeletedProvider(id_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void Client::successfullyDeletedProvider(id_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void Client::updateUserConnected(User _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void Client::updateUserDisconnected(User _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void Client::updateDumpedEntries(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void Client::disconnectedFromServer()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
