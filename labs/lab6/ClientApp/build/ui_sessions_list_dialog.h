/********************************************************************************
** Form generated from reading UI file 'sessions_list_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SESSIONS_LIST_DIALOG_H
#define UI_SESSIONS_LIST_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_SessionsListDialog
{
public:
    QGridLayout *gridLayout;
    QListView *lv_sessionsNames;
    QPushButton *pb_connect;
    QPushButton *pb_createSession;
    QLabel *label;

    void setupUi(QDialog *SessionsListDialog)
    {
        if (SessionsListDialog->objectName().isEmpty())
            SessionsListDialog->setObjectName(QStringLiteral("SessionsListDialog"));
        SessionsListDialog->resize(400, 300);
        gridLayout = new QGridLayout(SessionsListDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lv_sessionsNames = new QListView(SessionsListDialog);
        lv_sessionsNames->setObjectName(QStringLiteral("lv_sessionsNames"));

        gridLayout->addWidget(lv_sessionsNames, 1, 0, 1, 2);

        pb_connect = new QPushButton(SessionsListDialog);
        pb_connect->setObjectName(QStringLiteral("pb_connect"));

        gridLayout->addWidget(pb_connect, 2, 0, 1, 1);

        pb_createSession = new QPushButton(SessionsListDialog);
        pb_createSession->setObjectName(QStringLiteral("pb_createSession"));

        gridLayout->addWidget(pb_createSession, 2, 1, 1, 1);

        label = new QLabel(SessionsListDialog);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 2);


        retranslateUi(SessionsListDialog);

        QMetaObject::connectSlotsByName(SessionsListDialog);
    } // setupUi

    void retranslateUi(QDialog *SessionsListDialog)
    {
        SessionsListDialog->setWindowTitle(QApplication::translate("SessionsListDialog", "Dialog", nullptr));
        pb_connect->setText(QApplication::translate("SessionsListDialog", "Connect", nullptr));
        pb_createSession->setText(QApplication::translate("SessionsListDialog", "Create new", nullptr));
        label->setText(QApplication::translate("SessionsListDialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; font-weight:600;\">Choose online session or create new</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SessionsListDialog: public Ui_SessionsListDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SESSIONS_LIST_DIALOG_H
