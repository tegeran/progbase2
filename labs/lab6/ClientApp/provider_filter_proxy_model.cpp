#include "provider_filter_proxy_model.h"
#include "provider_list_model.h"
#include <QSet>
#include <utility>
#include "error.h"
#include "std_ext.h"

ProviderFilterProxyModel::ProviderFilterProxyModel(QSet<Provider *> && set, QObject * parent)
	: QSortFilterProxyModel(parent), set(std::move(set)) {}

bool ProviderFilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex &) const {
	ProviderListModel * source(
				const_cast<ProviderListModel *>(
					static_cast<const ProviderListModel *>(
						sourceModel()
					)
				)
	);
	if (!std_ext::hasIndex(source->providers(), source_row)){
		QLOG("invalid source_row received: " << source_row);
		return false;
	}
	return set.contains(&(source->providers()[source_row]));
}

bool ProviderFilterProxyModel::lessThan(const QModelIndex & source_left,
										const QModelIndex & source_right) const{
	ProviderListModel * source(
				const_cast<ProviderListModel *>(
					static_cast<const ProviderListModel *>(
						sourceModel()
					)
				)
	);
	if (!source_left.isValid()){
		QLOG("SIGSEGV: received invalid left index");
		return true;
	} else if (!source_right.isValid()){
		QLOG("SIGSEGV: received invalid right index");
		return true;
	}
	return Provider::compareTariff(source->providers()[source_left.row()],
								   source->providers()[source_right.row()]
			);
}

void ProviderFilterProxyModel::setSet(QSet<Provider *> && set){
	this->set = std::move(set);
	invalidate();
}
