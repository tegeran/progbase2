#include <QApplication>
#include <QMessageBox>
#include <QTimer>
#include <QCloseEvent>
#include <QLabel>

#include "main_stacked_widget.h"
#include "connection_ip_dialog.h"
#include "connecting_dialog.h"
#include "qext.h"


MainStackedWidget::MainStackedWidget(QWidget * const & parent)
	: QStackedWidget(parent),
		m_mainWindow(nullptr),
		m_client(new Client)
{
	connect(
		m_client,
		SIGNAL(disconnectedFromServer()),
		this,
		SLOT(handle_client_disconnectedFromServer())
	);
	connect(
		m_client,
		SIGNAL(error(QString)),
		this,
		SLOT(handle_client_error(QString))
	);
	connect(
		m_client,
		SIGNAL(createdSession(ClientSession)),
		this,
		SLOT(handle_client_createdSession(ClientSession))
	);
	connect(
		m_client,
		SIGNAL(
			connectedToSession(ClientSession,QVector<Provider>,QVector<User>)
		),
		this,
		SLOT(
			handle_client_connectedToSession(ClientSession,QVector<Provider>,QVector<User>)
		)
	);
	connect(
		m_client,
		SIGNAL(establishedConnection()),
		this,
		SLOT(handle_client_connectedToServer())
	);
}

void MainStackedWidget::on_userNameDialog_accepted(){
	m_client->setUser(User(static_cast<UserNameDialog *>(widget(0))->name()));
	clearWidgetsStack();
	startSessionsListDialog();
}

void MainStackedWidget::handle_requireNewSession(const QString & sessionName){
	QLOG("required new session under name: " << sessionName);
	m_client->requireNewSession(sessionName);
}

void MainStackedWidget::handle_client_error(const QString & reason){
	QMessageBox::critical(
		this,
		"SERVER SIGSEGV",
		tr("SIGSEGV: server failure \"%1\", connection aborted")
			.arg(reason)
	);
	clearWidgetsStack();
	ConnectionIpDialog ipDialog (
			"Connection failure", "127.0.0.1", 2000
	);
	QStackedWidget::addWidget(&ipDialog);
	QWidget::adjustSize();
	if (ipDialog.exec() == QDialog::Accepted){
		Qext::StackedWidget::removeLowestWidget(*this);
		Qext::Meta::scheduleRoutine(
			m_client,
			&Client::connectToServer,
			QHostAddress(ipDialog.chosenIpv4Address()),
			ipDialog.chosenPort()
		);
		showConnectionView();
	} else {
		Qext::StackedWidget::removeLowestWidget(*this);
		terminate();
	}
}


void MainStackedWidget::handle_client_disconnectedFromServer(){
	QLOG_ERROR("CONNECTION LOST");
}


void MainStackedWidget::handle_client_createdSession(ClientSession session){
	handle_client_connectedToSession(
		session,
		QVector<Provider>(),
		QVector<User>() << m_client->user()
	);
}

void MainStackedWidget::handle_client_connectedToSession(ClientSession session,
														 QVector<Provider> providers,
														 const QVector<User> & connectedUsers){
	clearWidgetsStack();
	MainWindow * mainWindow { new MainWindow(m_client, std::move(providers), connectedUsers) };
	mainWindow->setWindowTitle(tr("Providererr session \"%1\"").arg(session.name()));
	QStackedWidget::addWidget(mainWindow);
}


void MainStackedWidget::handle_client_connectedToServer(){
	clearWidgetsStack();
	UserNameDialog * userNameDialog { new UserNameDialog };
	QStackedWidget::addWidget(userNameDialog);
	QWidget::adjustSize();
	connect(
		userNameDialog,
		SIGNAL(accepted()),
		this,
		SLOT(on_userNameDialog_accepted())
	);
	connect(
		userNameDialog,
		&QDialog::rejected,
		[this](){ terminate(); }
	);
}

void MainStackedWidget::handle_connectingDialog_abortRequest(){
	m_client->errorAbort("User aborted connection");
}

void MainStackedWidget::handle_requireSessionConnection(const id_t& sessionId){
	QLOG("required session connection to " << sessionId);
	m_client->requireSessionConnection(sessionId);
}

void MainStackedWidget::closeEvent(QCloseEvent * event){
	if (!m_client->isConnected()
		|| QMessageBox::question(
			this,
			"Goodbye",
			"SIGSEGV: Escape the application?",
			QMessageBox::Ok,
			QMessageBox::Cancel
		) == QMessageBox::Ok
	){
		QStackedWidget::closeEvent(event);
	} else {
		event->ignore();
	}
}

void MainStackedWidget::showEvent(QShowEvent * event){
	QStackedWidget::showEvent(event);
	Qext::Meta::scheduleRoutine(
		[this](){
			ConnectionIpDialog ipDialog {
				"Enter Providererr server address", "127.0.0.1", 2000
			};
			QStackedWidget::addWidget(&ipDialog);
			if (ipDialog.exec() == QDialog::Rejected){
				Qext::StackedWidget::removeLowestWidget(*this);
				terminate(); return;
			} else {
				Qext::StackedWidget::removeLowestWidget(*this);
				m_client->connectToServer(
					QHostAddress(ipDialog.chosenIpv4Address()),
					ipDialog.chosenPort()
				);
				showConnectionView();
			}
		}
	);
}

void MainStackedWidget::startSessionsListDialog(){
	SessionsListDialog * dialog { new SessionsListDialog };
	connect(
		m_client,
		SIGNAL(receivedOnlineSessionsHashTable(QHash<id_t, ClientSession>)),
		dialog,
		SLOT(setOnlineSessions(QHash<id_t,ClientSession>))
	);
	connect(
		m_client,
		SIGNAL(updateAddedSession(ClientSession)),
		dialog,
		SLOT(addOnlineSession(ClientSession))
	);
	connect(
		m_client,
		SIGNAL(updateDeletedSession(id_t)),
		dialog,
		SLOT(deleteOnlineSession(id_t))
	);
	connect(
		dialog,
		SIGNAL(connectionRequest(id_t)),
		this,
		SLOT(handle_requireSessionConnection(id_t))
	);
	connect(
		dialog,
		SIGNAL(createSessionRequest(QString)),
		this,
		SLOT(handle_requireNewSession(QString))
	);

	m_client->requireOnlineSessionsHashTable();
	QStackedWidget::addWidget(dialog);
	QWidget::adjustSize();
}

void MainStackedWidget::terminate(){
	clearWidgetsStack();
	close();
}

void MainStackedWidget::clearWidgetsStack(){
	while (QStackedWidget::count() != 0){
		QWidget * widget { QStackedWidget::widget(0) };
		QStackedWidget::removeWidget(widget);
		delete widget;
	}
}

void MainStackedWidget::showConnectionView(){
	ConnectingDialog * connectingDialog { new ConnectingDialog };
	connect(
		connectingDialog,
		SIGNAL(abortRequest()),
		this,
		SLOT(handle_connectingDialog_abortRequest())
	);
	QStackedWidget::addWidget(connectingDialog);
}
