#ifndef PROVIDERLISTMODEL_H
#define PROVIDERLISTMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QMimeData>
#include "provider.h"

class ProviderListModel : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit ProviderListModel(QObject *parent = nullptr, QVector<Provider> && vect = QVector<Provider>());

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

	// Add data:
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	// Remove data:
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	QVector<Provider> & providers();
	void setProviderAt(const int &index, Provider && provider);
	void insertProviderAt(const int &index, Provider && provider);
	void clearProviders();
	void setProviders(QVector<Provider> && donor);

private:
	QVector<Provider> vect;


	// QAbstractItemModel interface
public:
	Qt::DropActions supportedDropActions() const override;

	// QAbstractItemModel interface
public:
	QStringList mimeTypes() const override;

	// QAbstractItemModel interface
public:
	QMimeData *mimeData(const QModelIndexList & indexes) const override;
	bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent) override;

	// QAbstractItemModel interface
public:
//	bool moveRows(const QModelIndex & sourceParent, int sourceRow, int count, const QModelIndex & destinationParent, int destinationChild) override;
};

#endif // PROVIDERLISTMODEL_H
