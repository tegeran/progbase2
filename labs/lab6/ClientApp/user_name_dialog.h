#ifndef USER_NAME_DIALOG_H
#define USER_NAME_DIALOG_H

#include <QDialog>
#include <QRegularExpressionValidator>

namespace Ui {
	class UserNameDialog;
}

class UserNameDialog : public QDialog {
	Q_OBJECT

public:
	explicit UserNameDialog(const QString & name = "", QWidget *parent = 0);
	~UserNameDialog();
	QString name() const;

private slots:
	void on_ledit_name_textChanged(const QString &currentString);
private:
	Ui::UserNameDialog *ui;
	QRegularExpressionValidator leditValidator;
};

#endif // USER_NAME_DIALOG_H
