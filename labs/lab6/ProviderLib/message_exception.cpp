#include "message_exception.h"
#include <string>


MessageException::MessageException(std::string &&error)
	: _error(std::move(error)) {}

MessageException::MessageException(const std::string &error)
    : _error(error) {}

const char *MessageException::what() const noexcept{
    return _error.c_str();
}
