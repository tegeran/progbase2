#-------------------------------------------------
#
# Project created by QtCreator 2018-05-17T19:38:11
#
#-------------------------------------------------

QT       += xml

QT       -= gui

TARGET = provider
TEMPLATE = lib
CONFIG += c++2a

DEFINES += PROVIDERLIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        provider.cpp \
        qext.cpp \
        std_ext.cpp \
        message_exception.cpp \
        error.cpp \

HEADERS += \
        provider.h \
        providerlib_global.h \
        qext.h \
        std_ext.h\
        message_exception.h \
        error.h \

unix {
    target.path = /home/tegeran/progbase2/labs/lab6
    INSTALLS += target
}
