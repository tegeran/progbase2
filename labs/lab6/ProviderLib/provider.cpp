#include <utility>
#include <QRegularExpression>

#include "provider.h"
#include "std_ext.h"
#include "message_exception.h"
#include "qext.h"
#include <QByteArray>
#include <QDataStream>


#define DATA_CLOUD_FIELDS_AMOUNT 2
#define PROVIDER_FIELDS_AMOUNT 5

Provider::Provider(QString name, double tariff, double loss, int users, const Provider::DataCloud & cloud)
	: m_name(std::move(name)), m_tariff(tariff), m_loss(loss), m_users(users), m_cloud(cloud) {}

Provider::Provider(const Provider & toCopy)
 : m_name(toCopy.m_name),
   m_tariff(toCopy.m_tariff),
   m_loss(toCopy.m_loss),
   m_users(toCopy.m_users),
   m_cloud(toCopy.m_cloud) {}

Provider::Provider(Provider && toMove)
 : m_name(std::move(toMove.m_name)),
   m_tariff(toMove.m_tariff),
   m_loss(toMove.m_loss),
   m_users(toMove.m_users),
   m_cloud(std::move(toMove.m_cloud)) {}

Provider & Provider::operator=(const Provider & other){
	m_name   = other.m_name;
	m_tariff = other.m_tariff;
	m_loss   = other.m_loss;
	m_users  = other.m_users;
	m_cloud  = other.m_cloud;
	return *this;
}

Provider & Provider::operator=(Provider && other){
	m_name   = std::move(other.m_name);
	m_tariff = other.m_tariff;
	m_loss   = other.m_loss;
	m_users  = other.m_users;
	m_cloud  = std::move(other.m_cloud);
	return *this;
}

bool Provider::hasCloud() const{
	return !m_cloud.isNull();
}

const QString & Provider::name() const      { return m_name; }
void Provider::setName(const QString & name){ m_name = name; }
void Provider::setName(QString && name)     { m_name = std::move(name); }

double Provider::tariff() const        { return m_tariff; }
void Provider::setTariff(double tariff){ m_tariff = tariff; }

double Provider::loss() const       { return m_loss; }
void Provider::setLoss(double loss) { m_loss = loss; }
\
int Provider::users() const       { return m_users; }
void Provider::setUsers(int users){ m_users = users;}

Provider::DataCloud Provider::cloud() const      { return m_cloud; }
void Provider::setCloud(const DataCloud & cloud) { m_cloud = cloud; }

QDataStream & operator>>(QDataStream & stream, Provider & prov){
	stream >> prov.m_name >> prov.m_tariff >> prov.m_loss;
	qint32 keeper;
	stream >> keeper;
	prov.m_users = static_cast<int>(keeper);
	return stream >> prov.m_cloud;
}

QDataStream & operator<<(QDataStream & stream, const Provider & prov){
	return stream << prov.m_name
				  << prov.m_tariff
				  << prov.m_loss
				  << static_cast<qint32>(prov.m_users)
				  << prov.m_cloud;

}

QJsonObject Provider::toJsonObject() const {
	return QJsonObject({
		QPair<QString, QJsonValue>("name",   QJsonValue(m_name)),
		QPair<QString, QJsonValue>("tariff", QJsonValue(m_tariff)),
		QPair<QString, QJsonValue>("loss",   QJsonValue(m_loss)),
		QPair<QString, QJsonValue>("users",  QJsonValue(m_users)),
		QPair<QString, QJsonValue>("cloud",  m_cloud.toJsonValue())
					   });
}

QJsonArray Provider::toJsonArr(const QVector<Provider> & self){
	QJsonArray arr;
	for (const Provider & prov : self){
		arr << QJsonValue(prov.toJsonObject());
	}
	return arr;
}

void Provider::toJsonFile(const QVector<Provider> & vector, const QString & path){
	Qext::File::Json::write(QJsonDocument(::Provider::toJsonArr(vector)), path);
}

Provider Provider::fromJsonObject(const QJsonObject & obj){
	if (obj.size() != PROVIDER_FIELDS_AMOUNT){
		throw MessageException("invalid Provider object");
	}
	QJsonValue namval(obj["name"]);
	if (namval.isUndefined() || !namval.isString()){
		throw MessageException("invalid Provider::name");
	}
	QString name(namval.toString());
	if (!isName(name)){
		throw MessageException("invalid Provider::name");
	}
	//========================================
	QJsonValue tarval(obj["tariff"]);
	if (tarval.isUndefined() || !tarval.isDouble()){
		throw MessageException("invalid Provider::tariff");
	}
	double tariff(tarval.toDouble());
	if (!isTariff(tariff)){
		throw MessageException("invalid Provider::tariff");
	}
	//=========================================
	QJsonValue losval(obj["loss"]);
	if (losval.isUndefined() || !losval.isDouble()){
		throw MessageException("invalid Provider::loss");
	}
	double loss(losval.toDouble());
	if (!isLoss(loss)){
		throw MessageException("invalid Provider::loss");
	}
	//=========================================
	QJsonValue usrval(obj["users"]);
	if (usrval.isUndefined() || !usrval.isDouble()){
		throw MessageException("invalid Provider::users");
	}
	int users(usrval.toInt());
	if (!isUsers(users)){
		throw MessageException("invalid Provider::users");
	}
	//===========================================
	return Provider(std::move(name),
					tariff,
					loss,
					users,
					DataCloud::fromJsonValue(obj["cloud"]) /* throws MessageException */
			);
}

QVector<Provider> Provider::fromJsonArr(const QJsonArray & arr){
	QVector<Provider> vector;
	for (const QJsonValue & proval: arr){
		if (!proval.isObject()){
			throw MessageException("invalid Provider object");
		}
		vector << ::Provider::fromJsonObject(proval.toObject());
	}
	return vector;
}

QVector<Provider> Provider::fromJsonFile(const QString & path){
	QJsonDocument doc(Qext::File::Json::read(path)); /* throws MessageException */
	if (!doc.isArray()){
		throw MessageException("invalid QVector<Provider> array");
	}
	return ::Provider::fromJsonArr(doc.array());
}

bool Provider::isName(const QString & suspect){
	return QRegularExpression(R"~~(\s*\w+.*)~~").match(suspect).hasMatch();
}

bool Provider::isTariff(const double & suspect){ return suspect >= 0; }
bool Provider::isLoss(const double & suspect)  { return std_ext::inbound(suspect, 0, 100); }
bool Provider::isUsers(const int & suspect)    { return suspect >= 0; }

bool Provider::compareTariff(const Provider & left, const Provider & right){
	return std_ext::doubleCompare(left.m_tariff, right.m_tariff) < 0;
}

Provider::PriorityQueByTariff Provider::toStdPriorityQueueByTariff(QVector<Provider> & vector){
	PriorityQueByTariff que(
		[](Provider * const & left, Provider * const & right){
			return left->m_tariff < right->m_tariff;
	});
	for (Provider & prov : vector){
		que.push(&prov);
	}
	return que;
}

bool Provider::operator==(const Provider & suspect){
	return m_name == suspect.m_name
			&& std_ext::doubleEquals(m_tariff, suspect.m_tariff)
			&& std_ext::doubleEquals(m_loss, suspect.m_loss)
			&& m_users == suspect.m_users
			&& m_cloud == suspect.m_cloud;
}


















Provider::DataCloud::DataCloud(const double & tariff, const double & capacity)
	: m_tariff(tariff), m_capacity(capacity) {}

double Provider::DataCloud::tariff() const   { return m_tariff; }
double Provider::DataCloud::capacity() const { return m_capacity; }

void Provider::DataCloud::setTariff(const double & tariff)    { m_tariff = tariff; }
void Provider::DataCloud::setCapacity(const double & capacity){ m_capacity = capacity; }

bool Provider::DataCloud::isNull() const{ return std_ext::doubleEquals(-1, m_capacity); }

Provider::DataCloud Provider::DataCloud::nullobj(){ return DataCloud(0, -1); }

QJsonValue Provider::DataCloud::toJsonValue() const{
	return isNull()
			? QJsonValue()
			: QJsonValue(QJsonObject({
										 QPair<QString, QJsonValue>("tariff", QJsonValue(m_tariff)),
										 QPair<QString, QJsonValue>("capacity", QJsonValue(m_capacity))
									 }));
}

Provider::DataCloud Provider::DataCloud::fromJsonValue(const QJsonValue & value){
	if (value.isNull()) {
		return nullobj();
	}
	if (!value.isObject()) {
		throw MessageException("invalid Provider::DataCloud");
	}
	QJsonObject obj(value.toObject());
	if (obj.size() != DATA_CLOUD_FIELDS_AMOUNT
			|| !obj.contains("tariff")
			|| !obj.contains("capacity"))         {
		throw MessageException("invalid Provider::DataCloud");
	}
	QJsonValue tarval(obj["tariff"]);
	QJsonValue capval(obj["capacity"]);
	if (!tarval.isDouble() || !capval.isDouble()) {
		throw MessageException("invalid Provider::DataCloud");
	}
	return DataCloud(tarval.toDouble(), capval.toDouble());
}

bool Provider::DataCloud::operator==(const Provider::DataCloud & suspect) const{
	return std_ext::doubleEquals(m_tariff, suspect.m_tariff)
			&& std_ext::doubleEquals(m_capacity, suspect.m_capacity);
}

QDataStream & operator>>(QDataStream & stream, Provider::DataCloud & cloud){
	return stream >> cloud.m_tariff >> cloud.m_capacity;
}

QDataStream & operator<<(QDataStream & stream, const Provider::DataCloud & cloud){
	return stream << cloud.m_tariff << cloud.m_capacity;
}

bool Provider::DataCloud::isTariff(const double & suspect)  { return Provider::isTariff(suspect); }
bool Provider::DataCloud::isCapacity(const double & suspect){ return suspect >= 0; }

