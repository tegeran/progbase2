#ifndef QEXT
#define QEXT
#include <QString>
#include <string>
#include "message_exception.h"
#include <QException>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "location.h"
#include <QDomDocument>
#include <QSet>
#include <QFile>
#include <QByteArray>
#include "queue"
#include "error.h"
#define TR(...) (QObject::tr((__VA_ARGS__)))

namespace Qext {

    namespace File {

		QString dirName(const QString & fullpath);

        QString read(const QString & path); // throws MessageException
        void write(const QString & src, const QString & path); // throws MessageException

		QByteArray readBytes(const QString & path);
		void writeBytes(const QByteArray & src, const QString & path);

        namespace Json {
            class JsonException : public MessageException {
            private:
                int m_offset;
            public:
                inline int offset();
                JsonException(const char * const & msg, const int & offset);
                JsonException(std::string msg, const int & offset);
                JsonException(const QString &msg, const int & offset);
            };


            QJsonDocument read(const QString & path); // throws JsonException
            void write(const QJsonDocument & document, const QString & path); // throws MessageException
			void write(const QJsonDocument & document, QFile & file); // throws MessageException
        }

        namespace Xml {
            class XmlException : public MessageException {
            private:
                Location<int> m_loc;
            public:
                inline Location<int> location();
                inline int y();
                inline int x();

                XmlException(const char * const & msg, const int &y, const int &x);
                XmlException(std::string msg, const int &y, const int &x);
                XmlException(const QString& msg, const int &y, const int &x);

            };


            QDomDocument read(const QString & path);
            void write(const QDomDocument & document, const QString & path);  // throws MessageException
		}

    }

	namespace Set {
		template <typename TEntity, typename TCont, typename TCompar>
		QSet<TEntity> fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que,
										   int amount){
			QSet<TEntity> set;
			while (amount-- && !que.empty()){
				set << que.top();
				que.pop();
			}
			return set;
		}
	}

	template <typename TEntity, typename TCont, typename TCompar>
	QSet<TEntity> fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que){
		return Qext::Set::fromStdPriorityQueue(que, que.size());
	}

	namespace Json {
		template <int N>
		int objectContains(const QJsonObject & obj, const char * keys[N]){
			for (int i = 0; i < N; ++i){
				if (!obj.contains(keys[i])){
					return i + 1;
				}
			}
			return 0;
		}
	}
}




#endif // QEXT

