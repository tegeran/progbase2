#ifndef QEXT
#define QEXT
#include <QObject>
#include <QString>
#include <QException>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDomDocument>
#include <QSet>
#include <QFile>
#include <QByteArray>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QUndoCommand>
#include <QStatusBar>
#include <QPair>
#include <QLineEdit>
#include <QLabel>
#include <QStringListModel>
#include <QStackedWidget>
#include <QTimer>

#include <utility>
#include <string>
#include <initializer_list>

#include "queue"
#include "error.h"
#include "location.h"
#include "message_exception.h"
#include "std_ext.h"
#include "netlib_global.h"

#define QSENDER(Type) static_cast<Type>(sender())
#define TR(...) (QObject::tr((__VA_ARGS__)))


namespace Qext NETLIBSHARED_EXPORT {

	bool isSpacedIdentifier(const QString & suspect);

	template <typename TFirst, typename TSecond>
	QPair<TFirst, TSecond> makeQPair(TFirst && firstValue, TSecond && secondValue){
		return QPair<TFirst, TSecond>(std::move(firstValue), std::move(secondValue));
	}


    namespace File {

		QString dirName(const QString & fullpath);

        QString read(const QString & path); // throws MessageException
        void write(const QString & src, const QString & path); // throws MessageException

        namespace Xml {
            class XmlException : public MessageException {
            private:
                Location<int> m_loc;
            public:
                inline Location<int> location();
                inline int y();
                inline int x();

                XmlException(const char * const & msg, const int &y, const int &x);
                XmlException(std::string msg, const int &y, const int &x);
                XmlException(const QString& msg, const int &y, const int &x);

            };


			QDomDocument readFromFile(const QString & path);
			void writeToFile(const QDomDocument & document, const QString & path);  // throws MessageException
		}

    }

	namespace Set {
		template <typename TEntity, typename TCont, typename TCompar>
		QSet<TEntity> fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que,
										   int amount){
			QSet<TEntity> set;
			while (amount-- && !que.empty()){
				set << que.top();
				que.pop();
			}
			return set;
		}
	}

	template <typename TEntity, typename TCont, typename TCompar>
	QSet<TEntity> fromStdPriorityQueue(std::priority_queue<TEntity, TCont, TCompar> & que){
		return Qext::Set::fromStdPriorityQueue(que, que.size());
	}

	namespace Json {

		class JsonException : public MessageException {
		private:
			int m_offset;
		public:
			inline int offset();
			JsonException(const char * const & msg, const int & offset);
			JsonException(std::string msg, const int & offset);
			JsonException(const QString &msg, const int & offset);
		};


		QJsonDocument readFromFile(const QString & path); // throws JsonException
		void writeToFile(const QJsonDocument & document, const QString & path); // throws MessageException
		void writeToFile(const QJsonDocument & document, QFile & file); // throws MessageException

		template <int N>
		DEPRECATED_WHY("use std::initializer_list<char *> overload instead")
		int objectContains(const QJsonObject & obj, const char * keys[N]){
			for (int i = 0; i < N; ++i){
				if (!obj.contains(keys[i])){
					return i + 1;
				}
			}
			return 0;
		}

		bool objectContains(const QJsonObject & obj, const std::initializer_list<const char *> & fields);
		QJsonDocument read(const QByteArray & byteArray); /* throws MessageException */
		QJsonDocument read(const QString & string);
	}

	namespace Sbox {
		void setMaxUnlimited(QSpinBox & sbox);
		void setMaxUnlimited(QDoubleSpinBox & sbox);

		void setMinUnlimited(QSpinBox & sbox);
		void setMinUnlimited(QDoubleSpinBox & sbox);
	}

	namespace Sbar {
		void showUndone(const QUndoCommand & command, QStatusBar & bar);
		void showRedone(const QUndoCommand & command, QStatusBar & bar);
	}

	namespace Object{
		void connectMultipleSignals(QObject * sender, std::initializer_list<const char *> sigs,
									QObject * receiver, const char * slot);
	}
	namespace Label{
		void clearLabels(std::initializer_list<QLabel *> labels);
	}
	namespace StringListModel{
		void prependString(QStringListModel & model,
						   const QString & string,
						   const int & role = Qt::DisplayRole);
		bool removeString(QStringListModel & model, const QString & string);
		bool addUniqueString(QStringListModel & model, const QString & string);
	}

	namespace StackedWidget{
		QWidget * removeLowestWidget(QStackedWidget & stackedWidget);
	}

	namespace Meta {

		template <typename TObj, typename TMethod, typename... Args>
		void scheduleRoutine(TObj && obj, TMethod && method, Args&&... args){
			QTimer::singleShot(
				0,
				[obj, method, args...]() mutable {
					(obj.*method)(args...);
				}
			);
		}

		template <typename TObj, typename TMethod, typename... Args>
		void scheduleRoutine(TObj * obj, TMethod && method, Args&&... args){
			QTimer::singleShot(
				0,
				[obj, method, args...]() mutable {
					(obj->*method)(args...);
				}
			);
		}

		template <typename TFunctor>
		void scheduleRoutine(const TFunctor & func){
			QTimer::singleShot(0, func);
		}
	}

}




#endif // QEXT

