﻿#ifndef DATAPACKET_H
#define DATAPACKET_H

#include <QJsonValue>
#include <QStringList>
#include <QTcpSocket>

#include <utility>
#include <QVector>

#include "std_ext.h"
#include "user.h"
#include "client_session.h"
#include "provider.h"
#include "netpacket.h"
#include "server_user.h"
#include "tcpsocket.h"
#include "netlib_global.h"

class NETLIBSHARED_EXPORT Session;

namespace DataPacket NETLIBSHARED_EXPORT{


	template <typename TPacket, typename... TPacketArgs>
	void send(QTcpSocket & socket, TPacketArgs &&... args);


struct NETLIBSHARED_EXPORT SingleUser {
	DECL_DEFAULT_MOVE_ONLY(SingleUser)

	User user;

	SingleUser(User && user)
		: user(std::move(user)) {}

	static SingleUser fromJson(const QJsonValue & jsonValue);
	static NetPacket toNetPacket(const NetPacket::Type & type, const User & user);
};

struct NETLIBSHARED_EXPORT SessionsHashTable {
	DECL_DEFAULT_MOVE_ONLY(SessionsHashTable)

	QHash<id_t, ClientSession> sessions;

	SessionsHashTable() = default;

	static SessionsHashTable fromJson(const QJsonValue & jsonValue);
	static NetPacket toNetPacket(const NetPacket::Type & type,
								 const QHash<id_t, Session *> & sessions);
};

struct NETLIBSHARED_EXPORT TypeOnly {
	DECL_DEFAULT_MOVE_ONLY(TypeOnly)

	TypeOnly() = default;
	static NetPacket toNetPacket(const NetPacket::Type & type);
};

struct NETLIBSHARED_EXPORT String {
	DECL_DEFAULT_MOVE_ONLY(String)

	QString string;
	explicit String(QString && string)
		: string(std::move(string)) {}

	static NetPacket toNetPacket(const NetPacket::Type &type, const QString & string);

	static String
	fromJson(const QJsonValue & jsonValue);
};

struct NETLIBSHARED_EXPORT SingleSession {
	DECL_DEFAULT_MOVE_ONLY(SingleSession)

	ClientSession session;
	explicit SingleSession(ClientSession && session)
		: session(std::move(session)) {}

	static NetPacket toNetPacket(const NetPacket::Type & type, const ClientSession & session);
	static SingleSession fromJson(const QJsonValue & jsonValue);
};

struct NETLIBSHARED_EXPORT Id {
	DECL_DEFAULT_MOVE_ONLY(Id)

	id_t id;
	Id(const id_t sessionId)
		: id(std::move(sessionId)) {}

	static NetPacket toNetPacket(const NetPacket::Type & type, const id_t & id);
	static Id fromJson(const QJsonValue & jsonValue);
};


struct NETLIBSHARED_EXPORT ProviderVector {
	DECL_DEFAULT_MOVE_ONLY(ProviderVector)
	QVector<Provider> providers;

	explicit ProviderVector(QVector<Provider> && providers)
		: providers(std::move(providers)) {}

	static NetPacket toNetPacket(const NetPacket::Type & type,
								 const QVector<Provider> & providers);
	static ProviderVector fromJson(const QJsonValue & jsonValue);
};

struct NETLIBSHARED_EXPORT ProviderAndPosition {
	DECL_DEFAULT_MOVE_ONLY(ProviderAndPosition)
	Provider provider;
	id_t position;

	explicit ProviderAndPosition(Provider && provider, const id_t & position)
		: provider(std::move(provider)), position(position) {}

	static NetPacket toNetPacket(const NetPacket::Type & type,
								 const Provider & provider,
								 const id_t & position);
	static ProviderAndPosition fromJson(const QJsonValue & jsonValue);
};


struct NETLIBSHARED_EXPORT StringVector {
	DECL_DEFAULT_MOVE_ONLY(StringVector)
	QStringList strings;

	explicit StringVector(QStringList && strings)
		: strings(std::move(strings)) {}

	static NetPacket toNetPacket(const NetPacket::Type & type,
								 const QStringList & strings);
	static StringVector fromJson(const QJsonValue & jsonValue);
};


struct NETLIBSHARED_EXPORT SessionConnectionData{
	DECL_DEFAULT_MOVE_ONLY(SessionConnectionData)
	ClientSession session;
	QVector<Provider> providers;
	QVector<User> users;

	explicit SessionConnectionData(ClientSession && session,
								   QVector<Provider> && providers,
								   QVector<User> && users = QVector<User>())
		: session(std::move(session)),
		  providers(std::move(providers)),
		  users(std::move(users)){}

	static NetPacket toNetPacket(const NetPacket::Type & type,
								 const ClientSession & session,
								 const QVector<Provider> & providers,
								 const QHash<TcpSocket *, ServerUser *> users);

	static SessionConnectionData fromJson(const QJsonValue & jsonValue);
};

	template <typename TDataPacket, typename... TPacketArgs>
	NETLIBSHARED_EXPORT void send(QTcpSocket & socket, TPacketArgs &&... args){
//		socket.flush();
		QLOG("before sending bytes to write: " << socket.bytesToWrite() << " to read: " << socket.bytesAvailable());
		(TDataPacket::toNetPacket(std::forward<TPacketArgs>(args)...)).send(socket);
		QLOG("after sending bytes to write: " << socket.bytesToWrite() << " to read: " << socket.bytesAvailable());
	}


	NETLIBSHARED_EXPORT inline void sendSignal(QTcpSocket & socket, const NetPacket::Type & signal){
		send<TypeOnly>(socket, signal);
	}
}



#endif // DATAPACKET_H
