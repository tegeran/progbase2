#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QTcpSocket>
#include "netpacket.h"
#include "netlib_global.h"

class NETLIBSHARED_EXPORT TcpSocket : public QTcpSocket{
	Q_OBJECT
public:
	explicit TcpSocket(QObject * const & parent = nullptr);
	virtual ~TcpSocket();

signals:
	void receivedNetPacket(NetPacket packet);
private slots:
	void on_readyRead();

};

#endif // TCPSOCKET_H
