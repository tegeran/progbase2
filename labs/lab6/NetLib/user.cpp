#include <QJsonObject>
#include <QPair>

#include <utility>

#include "user.h"
#include "qext.h"
#include "message_exception.h"
#include "std_ext.h"

#define NAME_FIELDNAME "name"

User::User(QString && name)
	: m_name(std::move(name)) {}

User::User(const User & lvalue)
	: m_name(lvalue.m_name) {}

User::User(User && rvalue)
	: m_name(std::move(rvalue.m_name)) {}

User & User::operator=(User && rvalue){
	m_name = std::move(rvalue.m_name);
	return *this;
}

User & User::operator=(const User & lvalue){
	m_name = lvalue.m_name;
	return *this;
}

const QString & User::name() const{
	return m_name;
}

void User::setName(QString && name){
	m_name = std::move(name);
}

QRegularExpression User::nameRegex(){
	return QRegularExpression(std_ext::SPACED_ID_REGEX);
}

bool User::isName(const QString & suspect){
	return Qext::isSpacedIdentifier(suspect);
}

QJsonValue User::toJsonValue() const{
	return QJsonValue(
				QJsonObject(
	{
		Qext::makeQPair(QString(NAME_FIELDNAME), QJsonValue(m_name))
	}
	));
}

User User::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isObject()){
		throw MessageException("user instance is of non json object type");
	}

	QJsonObject userObj { jsonValue.toObject() };

	if (!userObj.contains(QLatin1String(NAME_FIELDNAME))){
		throw MessageException("user json object doesn't "
							   "contain field '" NAME_FIELDNAME "'");
	}

	QJsonValue nameValue { userObj[NAME_FIELDNAME] };

	if (!nameValue.isString()){
		throw MessageException("user name field is of non-string type");
	}

	return User(nameValue.toString());
}







