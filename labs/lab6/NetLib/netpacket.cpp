#include <QJsonDocument>
#include <QJsonValue>

#include "netpacket.h"
#include "qnet.h"
#include "std_ext.h"
#include "message_exception.h"
#include "qext.h"
#include "error.h"
#include "user.h"
#include <QDataStream>

#define TYPE_FIELDNAME "type"
#define DATA_FIELDNAME "data"


NetPacket::NetPacket(QTcpSocket & socket){
	QDataStream socketDataStream { &socket };
	socketDataStream.startTransaction();
	QString dataString;
	socketDataStream >> dataString;
	if (!socketDataStream.commitTransaction()){
		QLOG("failed to commit reading transaction (status: " << socketDataStream.status()
			 << "): " << dataString);
		return;
	}
	QLOG("\n>>>>>>>>>\n>>>>>>>>>" << dataString << "\n>>>>>>>>>");
//	#warning TEMPORARY FOR DEBUGGING:
//	socket.startTransaction();
//	QByteArray dataString { socket.readAll() };
//	socket.commitTransaction();

	try {
		QJsonDocument jsonDocument {Qext::Json::read(dataString)}; /* throws MessageException */


		if (!jsonDocument.isObject())				  { goto error_label; }

		QJsonObject packetObj = jsonDocument.object();

		if (!Qext::Json::objectContains(packetObj,
				{TYPE_FIELDNAME, DATA_FIELDNAME}))  { goto error_label; }

		QJsonValue typeValue{ packetObj[TYPE_FIELDNAME] };
		m_dataJsonValue =     packetObj[DATA_FIELDNAME];

		if (!typeValue.isDouble())                    { goto error_label; }

		int packetType { typeValue.toInt() };

		if (!std_ext::enumContains<Type>(packetType)) { goto error_label; }

		m_type = static_cast<enum Type>(packetType);

		return;

	} catch (const MessageException &){
		// ... corrupted request
	}

	error_label:
	m_type = Null;
	QLOG("Invalid request received: " << dataString);
}

NetPacket::NetPacket(NetPacket && rvalue)
	: m_type(rvalue.m_type), m_dataJsonValue(std::move(rvalue.m_dataJsonValue))
{
	rvalue.m_type = Null;
}


NetPacket::NetPacket(const NetPacket & lvalue)
	: m_type(lvalue.m_type), m_dataJsonValue(lvalue.m_dataJsonValue) {}

NetPacket & NetPacket::operator=(NetPacket && rvalue){
	m_type = rvalue.m_type;
	m_dataJsonValue = std::move(rvalue.m_dataJsonValue);
	rvalue.m_type = Null;
	return *this;
}

bool NetPacket::isValid() const{
	return m_type != Null;
}

void NetPacket::send(QTcpSocket & receiverSocket) const	{
	if (!Qnet::canWriteTo(receiverSocket)){
		QLOG("socket is not ready to receive messages: " << receiverSocket.peerAddress().toString());
		return;
	}
	QString dataString { QString::fromUtf8(
		QJsonDocument(
			QJsonObject({
				Qext::makeQPair(QString(TYPE_FIELDNAME), QJsonValue(m_type)),
				Qext::makeQPair(QString(DATA_FIELDNAME), m_dataJsonValue),
			})
		).toJson(QJsonDocument::Compact))
	};
	Qnet::send(receiverSocket, dataString);
	QLOG("\n<<<<<<<<<\n<<<<<<<<<" << dataString << "\n<<<<<<<<<");
	return;
}

const NetPacket::Type & NetPacket::type() const { return m_type; }


NetPacket::NetPacket(const NetPacket::Type & type, QJsonValue && dataJsonValue)
	: m_type(type), m_dataJsonValue(std::move(dataJsonValue)) {}








