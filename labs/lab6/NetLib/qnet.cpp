#include <QNetworkConfigurationManager>
#include <QSettings>
#include <QNetworkConfiguration>
#include <QNetworkSession>
#include <QLatin1String>
#include <QString>

#include "error.h"
#include "qnet.h"

namespace Qnet{

	void deleteOnDisconnect(const QAbstractSocket & sock){
		QObject::connect(&sock, SIGNAL(disconnected()),
						 &sock, SLOT(deleteLater()));
	}

	QNetworkSession * establishNewSession(QSettings & settings,
										  QObject * const &parent,
										  const int & msecWait){
		QNetworkConfigurationManager manager;
		if (!(manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired)){
			return nullptr;
		}
		// If the saved network configuration is not currently discovered use the system default
		QNetworkConfiguration config(manager.configurationFromIdentifier(
										 settings.value(QLatin1String("Network/DefaultNetworkConfiguration"))
										 .toString()
		));
		if ((config.state() & QNetworkConfiguration::Discovered) !=
			QNetworkConfiguration::Discovered) {
			config = manager.defaultConfiguration();
		}

		QNetworkSession * networkSession(new QNetworkSession(config, parent));
		networkSession->open();
		networkSession->waitForOpened(msecWait);

		if (networkSession->isOpen()) {
			// Save the used configuration
			settings.setValue(QLatin1String("Network/DefaultNetworkConfiguration"),
							  config.type() == QNetworkConfiguration::UserChoice
													 ? networkSession->sessionProperty(
														  QLatin1String("UserChoiceConfiguration")
													   ).toString()
													 : config.identifier()
			);
		}
		return networkSession;
	}


	QNetworkSession * establishNewSession(QSettings && settings,
										  QObject * const &parent,
										  const int & msecWait){
		return establishNewSession(settings, parent, msecWait);
	}

	bool isConnected(const QTcpSocket & suspect){
		return suspect.state() == QTcpSocket::ConnectedState;
	}

	bool canWriteTo(const QTcpSocket & suspect){
		return isConnected(suspect) && suspect.isWritable();
	}

	LocalAddresses::LocalAddresses(){
		unsigned char finished = 0;
		for (const QHostAddress & hostAddress
			 : QNetworkInterface::allAddresses())
		{
			if (!hostAddress.isLoopback()){
				if (hostAddress.protocol() == QAbstractSocket::IPv4Protocol){
					ipv4 = std::move(hostAddress);
					if (++finished == 2) { return; }
				} else if (hostAddress.protocol() == QAbstractSocket::IPv6Protocol){
					ipv6 = std::move(hostAddress);
					if (++finished == 2) { return; }
				}
			}
		}
		QSHUTDOWN("this line should be unreachable");
	}

	bool isUnconnected(const QTcpSocket & suspect){
		return suspect.state() == QAbstractSocket::UnconnectedState;
	}

}
