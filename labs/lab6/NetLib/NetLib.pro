#-------------------------------------------------
#
# Project created by QtCreator 2018-05-27T20:00:35
#
#-------------------------------------------------

QT += network xml
QT += gui core widgets

TARGET = NetLib
TEMPLATE = lib
CONFIG += c++2a
DEFINES += NETLIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    message_exception.cpp \
    client_session.cpp \
    datapacket.cpp \
    error.cpp \
    netpacket.cpp \
    qext.cpp \
    qnet.cpp \
    server_user.cpp \
    session.cpp \
    std_ext.cpp \
    tcpsocket.cpp \
    user.cpp

HEADERS += \
        netlib_global.h \ 
    error.h \
    location.h \
    message_exception.h \
    client_session.h \
    datapacket.h \
    netpacket.h \
    provider.h \
    providerlib_global.h \
    qext.h \
    qnet.h \
    server_user.h \
    session.h \
    std_ext.h \
    tcpsocket.h \
    user.h


#LIBS += "/home/tegeran/progbase2/labs/lab6/NetLib/libprovider.so"

#unix:!macx: LIBS += -L$$PWD/build/ -lprovider

#INCLUDEPATH += $$PWD/.
#DEPENDPATH += $$PWD/.
