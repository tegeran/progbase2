#include <QFile>
#include <QTextStream>
#include <QByteArray>
#include <QObject>
#include <QRegularExpression>

#include <limits>

#include "qext.h"
#include "message_exception.h"


namespace Qext {

    namespace File {
        QString read(const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::ReadOnly)){
				throw MessageException(TR("failed to read file \"%1\"")
									   .arg(path).toStdString()
				);
            }
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            return content;
        }

        void write(const QString &src, const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::WriteOnly)){
				throw MessageException(TR("failed to open file \"%1\" for writing")
									   .arg(path).toStdString()
				);
            }
			QTextStream(&file) << src;
            file.close();
		}

        namespace Xml {
            int XmlException::y(){
                return m_loc.y;
            }

            int XmlException::x(){
                return m_loc.x;
            }

            Location<int> XmlException::location(){
                return m_loc;
            }

            XmlException::XmlException(const char * const &msg, const int &y, const int &x)
                : MessageException(msg), m_loc(y, x) {}

            XmlException::XmlException(const QString &msg, const int &y, const int &x)
                : MessageException(msg.toStdString()), m_loc(y, x) {}

            XmlException::XmlException(std::string msg, const int &y, const int &x)
                : MessageException(std::move(msg)), m_loc(y, x) {}


			QDomDocument readFromFile(const QString &path){
                QFile file(path);
                QString err;
                Location<int> eloc;
                QDomDocument document;
                if (!document.setContent(&file, &err, &eloc.y, &eloc.x)){
                    file.close();
					throw MessageException(TR("failed while parsing xml file \"%1\" (%2)").arg(err)
										   .arg(TR("error occured at %1").arg(eloc.toStdString().c_str()))
										   .toStdString()
					);
                }
                file.close();
                return document;
            }

			void writeToFile(const QDomDocument &document, const QString & path){
                QFile file(path);
                if (!file.open(QIODevice::WriteOnly)){
					throw MessageException(TR("failed to write xml document to \"%1\"")
										   .arg(path).toStdString()
					);
				}
				QTextStream(&file) << document.toString();
                file.close();
			}
		}

		QString dirName(const QString & fullpath){
			int lastSlash = fullpath.lastIndexOf('/');
			if (lastSlash == -1){
				return fullpath;
			} else {
				return fullpath.right(fullpath.length() - lastSlash - 1);
			}
		}

	}


	namespace Json {
		QJsonDocument readFromFile(const QString &path){
			QByteArray dataBytes;
			{
				QString content = ::Qext::File::read(path);
				dataBytes.append(content);
			}
			QJsonParseError err;
			QJsonDocument document = { QJsonDocument::fromJson(dataBytes, &err) };
			if (err.error != QJsonParseError::NoError){
				throw JsonException(
							TR("failed while parsing json file \"%1\" (%2)")
								.arg(path).arg(err.errorString()),
							err.offset
				);
			}
			return document;
		}

		inline int JsonException::offset(){
			return m_offset;
		}

		JsonException::JsonException(const char * const &msg, const int &offset)
			: MessageException(msg), m_offset(offset) {}

		JsonException::JsonException(std::string msg, const int &offset)
			: MessageException(std::move(msg)), m_offset(offset) {}

		JsonException::JsonException(const QString &msg, const int &offset)
			: MessageException(msg.toStdString()), m_offset(offset) {}

		void writeToFile(const QJsonDocument &document, const QString &path){
			QFile file(path);
			if (!file.open(QIODevice::WriteOnly)){
				throw MessageException(TR("failed to write json to file \"%1\"")
									   .arg(path).toStdString()
				);
			}
			writeToFile(document, file);
			file.close();
		}

		void writeToFile(const QJsonDocument & document, QFile & file){
			QTextStream(&file) << document.toJson();
		}


		bool objectContains(const QJsonObject & obj, const std::initializer_list<const char *> & fields){
			for (const char * const & field : fields){
				if (!obj.contains(field)){
					return false;
				}
			}
			return true;
		}

		QJsonDocument read(const QByteArray & byteArray){
			QJsonParseError err;
			QJsonDocument document = { QJsonDocument::fromJson(byteArray, &err) };
			if (err.error != QJsonParseError::NoError){
				throw MessageException(
							TR("failed while parsing json data (%1)")
							.arg(err.errorString()).toStdString()
				);
			}
			return document;
		}

		QJsonDocument read(const QString & string){
			return read(string.toUtf8());
		}

	}

	namespace Sbox{

		void setMaxUnlimited(QSpinBox & sbox){
			sbox.setMaximum(std::numeric_limits<int>::max());
		}

		void setMaxUnlimited(QDoubleSpinBox & sbox){
			sbox.setMaximum(std::numeric_limits<double>::max());
		}

		void setMinUnlimited(QSpinBox & sbox){
			sbox.setMinimum(std::numeric_limits<int>::min());
		}

		void setMinUnlimited(QDoubleSpinBox & sbox){
			sbox.setMinimum(std::numeric_limits<double>::min());
		}

	}

	namespace Sbar {
		void showUndone(const QUndoCommand & command, QStatusBar & bar){
			bar.showMessage(TR("Undone: %1").arg(command.text()));
		}

		void showRedone(const QUndoCommand & command, QStatusBar & bar){
			bar.showMessage(command.text());
		}
	}

	namespace Object{
		void connectMultipleSignals(QObject * sender,
									std::initializer_list<const char *> sigs,
									QObject * receiver,
									const char * slot)
		{
			for (const char * const & signal : sigs)
				QObject::connect(sender, signal, receiver, slot);
		}
	}

	bool isSpacedIdentifier(const QString & suspect){
		return QRegularExpression(std_ext::SPACED_ID_REGEX).match(suspect).hasMatch();
	}

	namespace Label{
		void clearLabels(std::initializer_list<QLabel *> labels){
			for (QLabel * const & label : labels){
				label->clear();
			}
		}
	}

	void StringListModel::prependString(QStringListModel & model,
										const QString & string,
										const int & role)
	{
		model.insertRow(0);
		model.setData(model.index(0), QVariant(string), role);
	}


	bool StringListModel::removeString(QStringListModel & model, const QString & string){
		const QStringList & strings { model.stringList() };
		for (auto iterator { strings.begin() }; iterator != strings.end(); ++iterator){
			if (*iterator == string){
				model.removeRow(iterator - strings.begin());
				return true;
			}
		}
		return false;
	}

	bool StringListModel::addUniqueString(QStringListModel & model, const QString & string){
		if (std_ext::contains(model.stringList(), string)){
			return false;
		}
		model.insertRow(0);
		model.setData(model.index(0), QVariant(string), Qt::DisplayRole);
		return true;
	}

	QWidget * StackedWidget::removeLowestWidget(QStackedWidget & stackedWidget){
		if (stackedWidget.count() == 0){
			return nullptr;
		}
		QWidget * removedWidget { stackedWidget.widget(0) };
		stackedWidget.removeWidget(removedWidget);
		return removedWidget;
	}


}
