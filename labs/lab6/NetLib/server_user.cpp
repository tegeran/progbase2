#include "server_user.h"
#include "user.h"

#include <utility>

ServerUser::ServerUser(QString && name, Session * const & session)
    : User(std::move(name)), m_session(session) {}

ServerUser::ServerUser(ServerUser && rvalue)
    : User(std::move(rvalue)), m_session(rvalue.m_session) {}

ServerUser::ServerUser(const ServerUser & lvalue)
    : User(lvalue), m_session(lvalue.m_session) {}
ServerUser & ServerUser::operator=(ServerUser && rvalue){
    User::operator=(static_cast<User &&>(rvalue));
    m_session = rvalue.m_session;
	return *this;
}


ServerUser::ServerUser(User && user, Session * const & session)
	: User(std::move(user)), m_session(session){}


Session * const & ServerUser::session() const{
    return m_session;
}

void ServerUser::setSession(Session * const & session){
    m_session = session;
}

bool ServerUser::isConnectedToSession() const{
	return m_session != nullptr;
}

