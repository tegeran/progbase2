#include <QJsonArray>

#include "datapacket.h"
#include "user.h"
#include "session.h"
#include "client_session.h"
#include "qext.h"

namespace DataPacket {

SingleUser SingleUser::fromJson(const QJsonValue & jsonValue){
	return SingleUser(User::fromJson(jsonValue));
}

NetPacket SingleUser::toNetPacket(const NetPacket::Type & type, const User & user){
	return NetPacket(type, user.toJsonValue());
}
// -----------

SessionsHashTable SessionsHashTable::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isArray())
	{
		throw MessageException("non-array session respond received");
	}
	SessionsHashTable packet;
	QJsonArray jsonArray(jsonValue.toArray());
	packet.sessions.reserve(jsonArray.size());
	for (const QJsonValue & value : jsonArray){
		ClientSession session { ClientSession::fromJsonValue(value) };
		packet.sessions.insert(session.id(), std::move(session));
	}
	return packet;
}

NetPacket SessionsHashTable::toNetPacket(const NetPacket::Type & type,
										 const QHash<id_t, Session *> & sessions){
	QJsonArray jsonArray;
	for (auto iterator { sessions.begin() }; iterator != sessions.end(); ++iterator){
		jsonArray << static_cast<ClientSession *>(iterator.value())->toJsonValue();
	}
	return NetPacket(type, QJsonValue(jsonArray));
}

// ------------------


NetPacket TypeOnly::toNetPacket(const NetPacket::Type & type){
	return NetPacket(type, QJsonValue(QJsonValue::Null));
}

// -----------------


NetPacket String::toNetPacket(const NetPacket::Type & type, const QString & string){
	return NetPacket(type, QJsonValue(string));
}

String String::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isString())
	{
		throw MessageException("string data packet of non-string type");
	}
	return String(jsonValue.toString());
}

// -----------------

NetPacket SingleSession::toNetPacket(const NetPacket::Type & type,
									 const ClientSession & session){
	return NetPacket(type, session.toJsonValue());
}

SingleSession
SingleSession::fromJson(const QJsonValue & jsonValue){
	return SingleSession(ClientSession::fromJsonValue(jsonValue));
}


NetPacket Id::toNetPacket(const NetPacket::Type & type, const id_t & id){
	return NetPacket(type, QJsonValue(static_cast<qint64>(id)));
}

Id Id::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isDouble()){
		throw MessageException("non-integral delted session id type");
	}
	int id { jsonValue.toInt() };
	if (!std_ext::isInRangeOf<id_t>(id)){
		throw MessageException("deleted session id is not in range of id type");
	}
	return Id(static_cast<id_t>(id));
}


NetPacket ProviderVector::toNetPacket(const NetPacket::Type & type,
									const QVector<Provider> & providers){
	return NetPacket(type, Provider::toJsonArr(providers));
}

ProviderVector ProviderVector::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isArray()){
		throw MessageException("non-array providers list");
	}
	return ProviderVector(Provider::fromJsonArr(jsonValue.toArray()));
}
// -------------

NetPacket ProviderAndPosition::toNetPacket(const NetPacket::Type & type,
									  const Provider & provider, const id_t & position)
{
	return NetPacket(type,
		 QJsonValue(
			QJsonObject({
				Qext::makeQPair(QString("provider"), QJsonValue(provider.toJsonObject())),
				Qext::makeQPair(QString("position"), QJsonValue(static_cast<qint64>(position)))
			})
		)
	);
}

ProviderAndPosition ProviderAndPosition::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isObject()){
		throw MessageException("non-object packet data json type");
	}
	QJsonObject obj { jsonValue.toObject() };
	if (!Qext::Json::objectContains(obj, {"provider", "position"})){
		throw MessageException("either provider or position was not found");
	}
	QJsonValue providerValue { obj["provider"] };
	if (!providerValue.isObject()){
		throw MessageException("provider of non-object type");
	}
	QJsonValue positionValue { obj["position"] };
	if (!positionValue.isDouble()){
		throw MessageException("position value of non-integral type");
	}
	int position { positionValue.toInt() };
	if (!std_ext::isInRangeOf<id_t>(position)){
		throw MessageException("position is out of range");
	}

	return ProviderAndPosition(Provider::fromJsonObject(
		providerValue.toObject()),
		static_cast<id_t>(position)
							   );
}

NetPacket StringVector::toNetPacket(const NetPacket::Type & type, const QStringList & strings){
	QJsonArray stringsArray;
	for (const QString & string: strings){
		stringsArray << QJsonValue(string);
	}
	return NetPacket(type, QJsonValue(stringsArray));
}

StringVector StringVector::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isArray()){
		throw MessageException("json string vector of non-array type");
	}
	QStringList stringList;
	QJsonArray stringsJsonArray(jsonValue.toArray());
	stringList.reserve(stringsJsonArray.size());
	for (const QJsonValue & stringValue : stringsJsonArray){
		if (!stringValue.isString()){
			throw MessageException("string vector contains non-string value");
		}
		stringList << stringValue.toString();
	}
	return StringVector(std::move(stringList));
}

NetPacket SessionConnectionData::toNetPacket(const NetPacket::Type & type,
												const ClientSession & session,
												const QVector<Provider> & providers,
												const QHash<TcpSocket *, ServerUser *> users)
{
	QJsonArray usersJsonArray;
	for (ServerUser * const & user : users){
		usersJsonArray << static_cast<User *>(user)->toJsonValue();
	}

	return NetPacket(type,
		QJsonValue(
			QJsonObject({
				Qext::makeQPair(QString("session"), session.toJsonValue()),
				Qext::makeQPair(QString("providers"), QJsonValue(Provider::toJsonArr(providers))),
				Qext::makeQPair(QString("users"), QJsonValue(usersJsonArray))
			})
		)
	);
}

SessionConnectionData SessionConnectionData::fromJson(const QJsonValue & jsonValue){
	if (!jsonValue.isObject())
	{
		throw MessageException("session and provider vector of non json object type");
	}
	QJsonObject jsonObj { jsonValue.toObject() };
	QJsonValue sessionValue { jsonObj["session"] };
	QJsonValue providersValue { jsonObj["providers"] };
	QJsonValue usersValue { jsonObj["users"] };
	if (!(providersValue.isArray() && usersValue.isArray()))
	{
		throw MessageException("supposed vector is of non json array type");
	}
	SessionConnectionData dataPacket {
		ClientSession::fromJsonValue(sessionValue),
		Provider::fromJsonArr(providersValue.toArray())
	};

	for (const QJsonValue & jsonValue : usersValue.toArray()){
		dataPacket.users << std::move(User::fromJson(jsonValue));
	}

	return dataPacket;
}

}
