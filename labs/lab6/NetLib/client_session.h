#ifndef CLIENT_SESSION_H
#define CLIENT_SESSION_H

#include <QString>
#include <QHash>
#include <QStringList>
#include <QMetaType>
#include <QJsonValue>
#include <QRegularExpression>

#include "std_ext.h"
#include "message_exception.h"
#include "provider.h"
#include "netlib_global.h"

typedef unsigned int id_t;

class NETLIBSHARED_EXPORT ClientSession {
public:
	explicit ClientSession(const id_t & id = 0,
						   QString && name = "");
	ClientSession(ClientSession && rvalue);
	ClientSession(const ClientSession & lvalue);
	ClientSession & operator=(ClientSession && rvalue);
	ClientSession & operator=(const ClientSession & lvalue);

	id_t id() const;
	void setId(const id_t & id);

	const QString & name() const;
	void setName(QString && name);

	bool isNull();
	static bool isName(const QString & suspect);

	static QRegularExpression nameRegex();
	static QStringList sessionsToStringList(const QHash<id_t, ClientSession> & sessionsHashTable);
	static ClientSession nullobj();

	QJsonValue toJsonValue() const;
	/* throws MessageException */
	static ClientSession fromJsonValue(const QJsonValue & value);

protected:
	id_t m_id;
	QString m_name;
};

Q_DECLARE_METATYPE(ClientSession)

#endif // CLIENT_SESSION_H
