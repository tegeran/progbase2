#include <utility>
#include <QDir>
#include <QRegularExpression>

#include "session.h"
#include "qnet.h"
#include "datapacket.h"
#include "netpacket.h"
#include "qext.h"

Session::Session(const id_t & id,
				 QString && name,
				 TcpSocket * const & initiatorSocket,
				 ServerUser * const & initiatorUser)
	: ClientSession(id, std::move(name))
{
	m_connectedUsers.insert(initiatorSocket, initiatorUser);
	connectMemberSignals(initiatorSocket);
	initiatorUser->setSession(this);
	DataPacket::send<DataPacket::SingleSession>(
		*initiatorSocket,
		NetPacket::Answer_CreatedSession,
		*this
	);
}

Session::~Session(){
	foreach_citer(iterator, m_connectedUsers){
		iterator.value()->setSession(nullptr);
	}
}


const QVector<Provider> & Session::providerVector() const{
	return m_providerVector;
}

void Session::connectUser(TcpSocket * const & userSocket, ServerUser * const & user){
	sendToAllConnectedUsers<DataPacket::SingleUser>(
		NetPacket::Update_UserConnected,
		*static_cast<User *>(user)
	);
	m_connectedUsers.insert(userSocket, user);
	connectMemberSignals(userSocket);
	user->setSession(this);
	DataPacket::send<DataPacket::SessionConnectionData>(
		*userSocket,
		NetPacket::Answer_ConnectedToSession,
		*this,
		m_providerVector,
		m_connectedUsers
	);
}

void Session::disconnectUser(TcpSocket * const & userSocket){
	sendToAllConnectedUsers<DataPacket::SingleUser>(
		NetPacket::Update_UserDisconnected,
		*m_connectedUsers.take(userSocket)
	);
	if (m_connectedUsers.isEmpty()){
		emit noUsersLeft();
	}
}


void Session::on_socket_receivedNetPacket(const NetPacket & packet){
	TcpSocket * senderSocket { QSENDER(TcpSocket *) };
	try {
		switch (packet.type()){
			case NetPacket::Request_AddProvider:{
				processRequest_AddProvider(senderSocket, packet);
				return;
			}
			case NetPacket::Request_DeleteProvider:{
				processRequest_DeleteProvider(senderSocket, packet);
				return;
			}
			case NetPacket::Request_EditProvider:{
				processRequest_EditProvider(senderSocket, packet);
				return;
			}
			case NetPacket::Request_LoadEntries:{
				processRequest_LoadEntries(senderSocket, packet);
				return;
			}
			case NetPacket::Request_DumpEntries:{
				processRequest_DumpEntries(senderSocket, packet);
				return;
			}
			case NetPacket::Request_ClearProviders:{
				processRequest_ClearProviders(senderSocket, packet);
				return;
			}
			default:{return;}
		}
	} catch (const MessageException & exception) {
		QLOG_ERROR(exception.what());
	}
}

void Session::processRequest_AddProvider(TcpSocket * const & senderSocket,
										 const NetPacket & netpacket)
{
	DataPacket::ProviderAndPosition packet {
		netpacket.dataPacket<DataPacket::ProviderAndPosition>()
	};
	if (packet.position > static_cast<unsigned long>(m_providerVector.size())){
		sendErrorReport(senderSocket, "invalid position to add new provider");
		return;
	}
	m_providerVector.insert(packet.position, std::move(packet.provider));
	sendToAllConnectedUsersExcept<DataPacket::ProviderAndPosition>(
		senderSocket,
		NetPacket::Update_AddedProvider,
		m_providerVector[packet.position],
		packet.position
	);
	DataPacket::send<DataPacket::ProviderAndPosition>(
		*senderSocket,
		NetPacket::Answer_AddedProvider,
		m_providerVector[packet.position],
		packet.position
	);
}

void Session::processRequest_DeleteProvider(TcpSocket * const & senderSocket, const NetPacket & netpacket){
	DataPacket::Id packet {
		netpacket.dataPacket<DataPacket::Id>()
	};
	if (!std_ext::hasIndex(m_providerVector, packet.id)){
		sendErrorReport(senderSocket, "invalid position to delete provider");
		return;
	}
	m_providerVector.remove(packet.id);
	sendToAllConnectedUsersExcept<DataPacket::Id>(
		senderSocket,
		NetPacket::Update_DeletedProvider,
		packet.id
	);
	DataPacket::send<DataPacket::Id>(
		*senderSocket,
		NetPacket::Answer_DeletedProvider,
		packet.id
	);
}

void Session::processRequest_EditProvider(TcpSocket * const & senderSocket, const NetPacket & netpacket){
	DataPacket::ProviderAndPosition packet {
		netpacket.dataPacket<DataPacket::ProviderAndPosition>()
	};
	if (!std_ext::hasIndex(m_providerVector, packet.position)){
		sendErrorReport(senderSocket, "invalid position to edit provider");
		return;
	}
	m_providerVector[packet.position] = std::move(packet.provider);
	sendToAllConnectedUsersExcept<DataPacket::ProviderAndPosition>(
		senderSocket,
		NetPacket::Update_EditedProvider,
		m_providerVector[packet.position],
		packet.position
	);
	DataPacket::send<DataPacket::ProviderAndPosition>(
		*senderSocket,
		NetPacket::Answer_EditedProvider,
		m_providerVector[packet.position],
		packet.position
	);
}

void Session::processRequest_LoadEntries(TcpSocket * const & senderSocket, const NetPacket & netpacket){
	DataPacket::String entryFileName {
		netpacket.dataPacket<DataPacket::String>()
	};
	QDir dataDirectory("./data");
	if (!dataDirectory.exists(entryFileName.string)){
		sendErrorReport(senderSocket, tr("no such file entry was found: %1")
										 .arg(entryFileName.string));
		return;
	}
	try {
		m_providerVector = Provider::fromJsonFile(
			QString("%1/%2")
			.arg(DATA_DIRECTORY)
			.arg(entryFileName.string)
		);
	} catch (const MessageException & exception){
		sendErrorReport(senderSocket, tr("failed to load entry file \"%1\" (%2)")
						.arg(entryFileName.string)
						.arg(exception.what()));
		return;
	}
	sendToAllConnectedUsers<DataPacket::ProviderVector>(
		NetPacket::Update_ReloadedEntries,
		m_providerVector
	);
}

void Session::processRequest_DumpEntries(TcpSocket * const & senderSocket, const NetPacket & netpacket){
	DataPacket::String pathPacket {
		netpacket.dataPacket<DataPacket::String>()
	};
	if (!QRegularExpression(QString(R"~~([^%1]+)~~").arg(QDir::separator()))
			.match(pathPacket.string).hasMatch())
	{
		sendErrorReport(senderSocket, "bad entry filename");
		return;
	}
	Provider::toJsonFile(m_providerVector, QString("%1%2%3")
						 .arg(DATA_DIRECTORY)
						 .arg(QDir::separator())
						 .arg(pathPacket.string));
	sendToAllConnectedUsers<DataPacket::String>(
		NetPacket::Update_DumpedEntries,
		pathPacket.string
	);
}

void Session::processRequest_ClearProviders(TcpSocket * const & senderSocket, const NetPacket &){
	m_providerVector.clear();
	sendToAllConnectedUsers<DataPacket::TypeOnly>(
		NetPacket::Update_ClearedProviders
	);
}

void Session::sendErrorReport(TcpSocket * const & dest, const QString & report){
	QLOG("sent error report: " << report);
	DataPacket::send<DataPacket::String>(*dest, NetPacket::Answer_Error , report);
}


void Session::connectMemberSignals(TcpSocket * const & sender){
	connect(sender, SIGNAL(receivedNetPacket(NetPacket)),
			  this,   SLOT(on_socket_receivedNetPacket(NetPacket)));
}
