<<<<<<< HEAD
﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUndoStack>
#include "provider_list_model.h"
#include "provider_filter_proxy_model.h"


namespace Command {
	class Add;
	class Delete;
	class Edit;
}
=======
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498

namespace Ui {
	class MainWindow;
}

<<<<<<< HEAD

class MainWindow : public QMainWindow{
	Q_OBJECT
	friend class Command::Add;
	friend class Command::Delete;
	friend class Command::Edit;
=======
class MainWindow : public QMainWindow
{
	Q_OBJECT
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
<<<<<<< HEAD
private slots:
	void on_model_data_changed(QModelIndex, QModelIndex, QVector<int>);
	void on_act_tmpsave_triggered();
	void on_ustack_clean_changed(bool yes);

	void on_pb_add_clicked();

	void on_pb_delete_clicked();

	void on_pb_edit_clicked();

	void on_lv_names_doubleClicked(const QModelIndex &index);

	void on_lv_execs_doubleClicked(const QModelIndex &index);

	void on_lv_names_currentChanged(const QModelIndex &cur, const QModelIndex &prev);
	void on_lv_execs_currentChanged(const QModelIndex &cur, const QModelIndex &prev);
	void on_pb_execute_clicked();

	void on_act_open_triggered();

private:
	Ui::MainWindow * ui;
	QUndoStack ustack;
	ProviderListModel * model;
	ProviderFilterProxyModel * proxyModel;
	int currentFilter;

	void insertProvider(const int & index, Provider && prov);
	void deleteProvider(const int & index);
	void setStatus(const QString & status);
	QStatusBar & sbar();
	int current() const;
	void updateLables();
	void updateProxyModel();
=======

private:
	Ui::MainWindow *ui;
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498
};

#endif // MAINWINDOW_H
