#include "commands.h"
#include "qext.h"
#include <QStatusBar>

Command::Add::Add(MainWindow & win, Provider && prov, int index, QUndoCommand * comm)
	: QUndoCommand(comm), win(win), prov(std::move(prov)), index(index)
{
	QUndoCommand::setText(TR("add \"%1\"").arg(this->prov.name()));
	win.setStatus(TR("Added provider \"%1\"").arg(this->prov.name()));
}

void Command::Add::undo(){
	win.deleteProvider(index);
	Qext::Sbar::showUndone(*this, win.sbar());
}
void Command::Add::redo(){
	win.insertProvider(index, Provider(prov));
	Qext::Sbar::showRedone(*this, win.sbar());
}

Command::Delete::Delete(MainWindow & win, int index, QUndoCommand * comm)
	: QUndoCommand(comm), win(win), prov(std::move(win.model->providers()[index])), index(index)
{
	QUndoCommand::setText(TR("delete \"%1\"").arg(this->prov.name()));
	win.setStatus(TR("Deleted provider \"%1\"").arg(this->prov.name()));
}

void Command::Delete::undo(){
	win.insertProvider(index, Provider(prov));
	Qext::Sbar::showUndone(*this, win.sbar());
}
void Command::Delete::redo(){
	win.deleteProvider(index);
	Qext::Sbar::showRedone(*this, win.sbar());
}


Command::Edit::Edit(MainWindow & win, Provider && prov, int index, QUndoCommand * comm)
	: QUndoCommand(comm), win(win), prov(std::move(prov)), index(index)
{
	QUndoCommand::setText(TR("edit \"%1\"").arg(this->prov.name()));
	win.setStatus(TR("Edited provider \"%1\"").arg(this->prov.name()));
}

void Command::Edit::undo(){
	redo();
	Qext::Sbar::showUndone(*this, win.sbar());
}
void Command::Edit::redo(){
	Provider keeper(std::move(win.model->providers()[index]));
	win.model->setProviderAt(index, std::move(prov));
	prov = std::move(keeper);
	Qext::Sbar::showRedone(*this, win.sbar());
}
