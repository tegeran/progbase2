#include "provider_filter_proxy_model.h"
<<<<<<< HEAD
#include "provider_list_model.h"
#include <QSet>
#include <utility>

ProviderFilterProxyModel::ProviderFilterProxyModel(QSet<Provider *> && set, QObject * parent)
	: QSortFilterProxyModel(parent), set(std::move(set)) {}

bool ProviderFilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex &) const {
	ProviderListModel * source(
				const_cast<ProviderListModel *>(
					static_cast<const ProviderListModel *>(
						sourceModel()
					)
				)
	);
	return set.contains(&(source->providers()[source_row]));
}

bool ProviderFilterProxyModel::lessThan(const QModelIndex & source_left, const QModelIndex & source_right) const{
	ProviderListModel * source(
				const_cast<ProviderListModel *>(
					static_cast<const ProviderListModel *>(
						sourceModel()
					)
				)
	);
	return Provider::compareTariff(source->providers()[source_left.row()],
								   source->providers()[source_right.row()]
			);
}

void ProviderFilterProxyModel::setSet(QSet<Provider *> && set){
	this->set = std::move(set);
	invalidate();
=======

ProviderFilterProxyModel::ProviderFilterProxyModel(QObject *parent)
	: QAbstractItemModel(parent)
{
}

QModelIndex ProviderFilterProxyModel::index(int row, int column, const QModelIndex &parent) const
{
	// FIXME: Implement me!
}

QModelIndex ProviderFilterProxyModel::parent(const QModelIndex &index) const
{
	// FIXME: Implement me!
}

int ProviderFilterProxyModel::rowCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return 0;

	// FIXME: Implement me!
}

int ProviderFilterProxyModel::columnCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return 0;

	// FIXME: Implement me!
}

QVariant ProviderFilterProxyModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	// FIXME: Implement me!
	return QVariant();
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498
}
