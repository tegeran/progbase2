#ifndef STD_EXT
#define STD_EXT

#include <iterator>
#include <set>
#include "error.h"

#define SIZEOF(arr) (sizeof((arr)) / sizeof((arr)[0]))
#define SKIP_IF(condition) {if ((condition)) {continue;}}

namespace std_ext {
    // from c++17 standart:
    template <typename T, std::size_t N>
    constexpr std::size_t size(const T (&arr)[N]) noexcept{
        return N;
    }


    template <typename TIter>
    void kill(TIter begin, const TIter & end){
        while (begin != end){
            Debug_exists(*begin);
            delete *begin;
            ++begin;
        }
    }

    template <typename TContainer>
    void kill(TContainer & cont){
        for (auto & ptr : cont){
            Debug_exists(ptr);
            delete ptr;
        }
    }

    template <typename TIter>
    void overkill(TIter begin, const TIter & end){
        if (begin == end) return;
        std::set<decltype(*begin)> set;
        while (begin != end){
            auto occurence = set.find(*begin);
            if (occurence == set.end()){
                Debug_exists(begin);
                delete *begin;
                set.insert(*begin);
            } // else already deleted
        }
    }

    template <typename TContainer>
    void overkill(TContainer & cont){
        overkill(cont.begin(), cont.end());
    }


    template <typename TIter, typename TMember>
    void overkill(TIter begin, const TIter & end, TMember delField){
        if (begin == end) return;
        std::set<decltype(*begin)> set;
        while (begin != end){
            auto occurence = set.find(begin->*delField);
            if (occurence == set.end()){
                Debug_exists(begin->*delField);
                delete begin->*delField;
                set.insert(*begin->*delField);
            } // else already deleted
        }
    }

    template <typename TContainer, typename TMember>
    void overkill(TContainer & cont, TMember delField){
		overkill(cont.begin(), cont.end(), delField);
    }

	template <typename A, typename B, typename C>
	bool inbound(const A &suspect, const B &left, const C &right){
		return suspect >= left && suspect <= right;
	}

    const char * toString(bool boolean);

	template <typename TContainer, typename TNumeric>
	bool hasIndex_Helper(const TContainer & cont, const TNumeric &index, std::true_type){
		return 0 <= index && index < cont.size();
	}

	template <typename TContainer, typename TNumeric>
	bool hasIndex_Helper(const TContainer & cont, const TNumeric &index, std::false_type){
		return index < cont.size();
	}

	template <typename TContainer, typename TNumeric>
	bool hasIndex(const TContainer & cont, const TNumeric &index){
		return hasIndex_Helper(cont, index, std::is_signed<TNumeric>());
	}

	template <typename TLimit, typename TValue>
	TValue maxmap(const TValue &value, const TLimit &max){
		return value > max ? max : value;
	}

	template <typename TLimit, typename TValue>
	TValue minmap(const TValue &value, const TLimit &min){
		return value < min ? min : value;
	}

	template <typename TLimits, typename TValue>
	TLimits map(const TValue &value, const TLimits &left, const TLimits &right){
		return value > right
				? right
				: value < left
					? left
					: value;
	}

	int doubleCompare(const double &left, const double &right, const double &epsilon = 1e-6);
	bool doubleEquals(const double &left, const double &right, const double &epsilon = 1e-6);
}



#endif // STD_EXT

