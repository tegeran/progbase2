#include "qext.h"
#include <QFile>
#include <QTextStream>
#include <QByteArray>
#include "message_exception.h"
#include <QObject>
<<<<<<< HEAD
#include <limits>
=======
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498


namespace Qext {

    namespace File {
        QString read(const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::ReadOnly)){
				throw MessageException(TR("failed to read file \"%1\"")
									   .arg(path).toStdString()
				);
            }
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            return content;
        }

        void write(const QString &src, const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::WriteOnly)){
				throw MessageException(TR("failed to open file \"%1\" for writing")
									   .arg(path).toStdString()
				);
            }
			QTextStream(&file) << src;
            file.close();
        }

        namespace Json {
            QJsonDocument read(const QString &path){
                QByteArray dataBytes;
                {
                    QString content = ::Qext::File::read(path);
                    dataBytes.append(content);
                }
                QJsonParseError err;
                QJsonDocument document = { QJsonDocument::fromJson(dataBytes, &err) };
                if (err.error != QJsonParseError::NoError){
					throw JsonException(
								TR("failed while parsing json file \"%1\" (%2)")
									.arg(path).arg(err.errorString()),
								err.offset
					);
                }
                return document;
            }

            inline int JsonException::offset(){
                return m_offset;
            }

            JsonException::JsonException(const char * const &msg, const int &offset)
                : MessageException(msg), m_offset(offset) {}

            JsonException::JsonException(std::string msg, const int &offset)
                : MessageException(std::move(msg)), m_offset(offset) {}

            JsonException::JsonException(const QString &msg, const int &offset)
                : MessageException(msg.toStdString()), m_offset(offset) {}

            void write(const QJsonDocument &document, const QString &path){
                QFile file(path);
                if (!file.open(QIODevice::WriteOnly)){
					throw MessageException(TR("failed to write json to file \"%1\"")
										   .arg(path).toStdString()
					);
                }
				write(document, file);
				file.close();
			}

			void write(const QJsonDocument & document, QFile & file){
				QTextStream(&file) << document.toJson();
			}

        }

        namespace Xml {
            int XmlException::y(){
                return m_loc.y;
            }

            int XmlException::x(){
                return m_loc.x;
            }

            Location<int> XmlException::location(){
                return m_loc;
            }

            XmlException::XmlException(const char * const &msg, const int &y, const int &x)
                : MessageException(msg), m_loc(y, x) {}

            XmlException::XmlException(const QString &msg, const int &y, const int &x)
                : MessageException(msg.toStdString()), m_loc(y, x) {}

            XmlException::XmlException(std::string msg, const int &y, const int &x)
                : MessageException(std::move(msg)), m_loc(y, x) {}


            QDomDocument read(const QString &path){
                QFile file(path);
                QString err;
                Location<int> eloc;
                QDomDocument document;
                if (!document.setContent(&file, &err, &eloc.y, &eloc.x)){
                    file.close();
					throw MessageException(TR("failed while parsing xml file \"%1\" (%2)").arg(err)
										   .arg(TR("error occured at %1").arg(eloc.toStdString().c_str()))
										   .toStdString()
					);
                }
                file.close();
                return document;
            }

            void write(const QDomDocument &document, const QString & path){
                QFile file(path);
                if (!file.open(QIODevice::WriteOnly)){
					throw MessageException(TR("failed to write xml document to \"%1\"")
										   .arg(path).toStdString()
					);
				}
				QTextStream(&file) << document.toString();
                file.close();
			}
		}

		QString dirName(const QString & fullpath){
			int lastSlash = fullpath.lastIndexOf('/');
			if (lastSlash == -1){
				return fullpath;
			} else {
				return fullpath.right(fullpath.length() - lastSlash - 1);
			}
		}

		QByteArray readBytes(const QString & path){
			// @todo
//			QFile file(path);
//			if (!file.open(QIODevice::ReadOnly)){
//				throw MessageException(TR("failed to read binary file \"%1\"").arg(path).toStdString());
//			}
//			QByteArray arr;
			//			QDataStream(&file) >> arr;
			return QByteArray();
		}

		void writeBytes(const QByteArray & src, const QString & path)
		{
			//@todo:
		}

<<<<<<< HEAD
	}

	namespace Sbox{
		void setMaxUnlimited(QSpinBox & sbox){
			sbox.setMaximum(std::numeric_limits<int>::max());
		}

		void setMaxUnlimited(QDoubleSpinBox & sbox){
			sbox.setMaximum(std::numeric_limits<double>::max());
		}

		void setMinUnlimited(QSpinBox & sbox){
			sbox.setMinimum(std::numeric_limits<int>::min());
		}

		void setMinUnlimited(QDoubleSpinBox & sbox){
			sbox.setMinimum(std::numeric_limits<double>::min());
		}

	}

	void Sbar::showUndone(const QUndoCommand & command, QStatusBar & bar){
		bar.showMessage(TR("Undone [%1]").arg(command.text()));
	}

	void Sbar::showRedone(const QUndoCommand & command, QStatusBar & bar){
		bar.showMessage(TR("Done [%1]").arg(command.text()));
	}
=======
    }
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498
}
