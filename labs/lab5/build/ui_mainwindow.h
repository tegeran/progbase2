/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *act_open;
    QAction *act_saveTo;
    QAction *act_tmpsave;
    QAction *act_clear;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pb_add;
    QPushButton *pb_delete;
    QPushButton *pb_edit;
    QHBoxLayout *horizontalLayout_2;
    QSpinBox *sbox_exec;
    QPushButton *pb_execute;
    QLabel *slbl_objects;
    QLabel *lbl_filter;
    QListView *lv_names;
    QListView *lv_execs;
    QGroupBox *gbox_provider;
    QFormLayout *formLayout;
    QLabel *slbl_name;
    QLabel *lbl_name;
    QLabel *slbl_tariff;
    QLabel *slbl_users;
    QLabel *lbl_users;
    QLabel *slbl_loss;
    QLabel *lbl_loss;
    QGroupBox *gbox_cloud;
    QFormLayout *formLayout_2;
    QLabel *slbl_cloudTariff;
    QLabel *slbl_cloudCapacity;
    QLabel *lbl_cloudTariff;
    QLabel *lbl_cloudCapacity;
    QLabel *lbl_tariff;
    QMenuBar *menuBar;
    QMenu *menu_file;
    QMenu *menu_edit;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(625, 447);
        act_open = new QAction(MainWindow);
        act_open->setObjectName(QStringLiteral("act_open"));
        act_saveTo = new QAction(MainWindow);
        act_saveTo->setObjectName(QStringLiteral("act_saveTo"));
        act_tmpsave = new QAction(MainWindow);
        act_tmpsave->setObjectName(QStringLiteral("act_tmpsave"));
        act_clear = new QAction(MainWindow);
        act_clear->setObjectName(QStringLiteral("act_clear"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pb_add = new QPushButton(centralWidget);
        pb_add->setObjectName(QStringLiteral("pb_add"));

        horizontalLayout->addWidget(pb_add);

        pb_delete = new QPushButton(centralWidget);
        pb_delete->setObjectName(QStringLiteral("pb_delete"));
        pb_delete->setEnabled(false);

        horizontalLayout->addWidget(pb_delete);

        pb_edit = new QPushButton(centralWidget);
        pb_edit->setObjectName(QStringLiteral("pb_edit"));
        pb_edit->setEnabled(false);

        horizontalLayout->addWidget(pb_edit);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        sbox_exec = new QSpinBox(centralWidget);
        sbox_exec->setObjectName(QStringLiteral("sbox_exec"));
        sbox_exec->setMaximum(0);

        horizontalLayout_2->addWidget(sbox_exec);

        pb_execute = new QPushButton(centralWidget);
        pb_execute->setObjectName(QStringLiteral("pb_execute"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pb_execute->sizePolicy().hasHeightForWidth());
        pb_execute->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(pb_execute);


        gridLayout->addLayout(horizontalLayout_2, 0, 1, 1, 1);

        slbl_objects = new QLabel(centralWidget);
        slbl_objects->setObjectName(QStringLiteral("slbl_objects"));

        gridLayout->addWidget(slbl_objects, 1, 0, 1, 1);

        lbl_filter = new QLabel(centralWidget);
        lbl_filter->setObjectName(QStringLiteral("lbl_filter"));

        gridLayout->addWidget(lbl_filter, 1, 1, 1, 1);

        lv_names = new QListView(centralWidget);
        lv_names->setObjectName(QStringLiteral("lv_names"));

        gridLayout->addWidget(lv_names, 2, 0, 1, 1);

        lv_execs = new QListView(centralWidget);
        lv_execs->setObjectName(QStringLiteral("lv_execs"));

        gridLayout->addWidget(lv_execs, 2, 1, 2, 1);

        gbox_provider = new QGroupBox(centralWidget);
        gbox_provider->setObjectName(QStringLiteral("gbox_provider"));
        formLayout = new QFormLayout(gbox_provider);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        slbl_name = new QLabel(gbox_provider);
        slbl_name->setObjectName(QStringLiteral("slbl_name"));

        formLayout->setWidget(0, QFormLayout::LabelRole, slbl_name);

        lbl_name = new QLabel(gbox_provider);
        lbl_name->setObjectName(QStringLiteral("lbl_name"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lbl_name);

        slbl_tariff = new QLabel(gbox_provider);
        slbl_tariff->setObjectName(QStringLiteral("slbl_tariff"));

        formLayout->setWidget(1, QFormLayout::LabelRole, slbl_tariff);

        slbl_users = new QLabel(gbox_provider);
        slbl_users->setObjectName(QStringLiteral("slbl_users"));

        formLayout->setWidget(2, QFormLayout::LabelRole, slbl_users);

        lbl_users = new QLabel(gbox_provider);
        lbl_users->setObjectName(QStringLiteral("lbl_users"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lbl_users);

        slbl_loss = new QLabel(gbox_provider);
        slbl_loss->setObjectName(QStringLiteral("slbl_loss"));

        formLayout->setWidget(3, QFormLayout::LabelRole, slbl_loss);

        lbl_loss = new QLabel(gbox_provider);
        lbl_loss->setObjectName(QStringLiteral("lbl_loss"));

        formLayout->setWidget(3, QFormLayout::FieldRole, lbl_loss);

        gbox_cloud = new QGroupBox(gbox_provider);
        gbox_cloud->setObjectName(QStringLiteral("gbox_cloud"));
        gbox_cloud->setEnabled(true);
        gbox_cloud->setCheckable(false);
        gbox_cloud->setChecked(false);
        formLayout_2 = new QFormLayout(gbox_cloud);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        slbl_cloudTariff = new QLabel(gbox_cloud);
        slbl_cloudTariff->setObjectName(QStringLiteral("slbl_cloudTariff"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, slbl_cloudTariff);

        slbl_cloudCapacity = new QLabel(gbox_cloud);
        slbl_cloudCapacity->setObjectName(QStringLiteral("slbl_cloudCapacity"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, slbl_cloudCapacity);

        lbl_cloudTariff = new QLabel(gbox_cloud);
        lbl_cloudTariff->setObjectName(QStringLiteral("lbl_cloudTariff"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, lbl_cloudTariff);

        lbl_cloudCapacity = new QLabel(gbox_cloud);
        lbl_cloudCapacity->setObjectName(QStringLiteral("lbl_cloudCapacity"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, lbl_cloudCapacity);


        formLayout->setWidget(4, QFormLayout::SpanningRole, gbox_cloud);

        lbl_tariff = new QLabel(gbox_provider);
        lbl_tariff->setObjectName(QStringLiteral("lbl_tariff"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lbl_tariff);


        gridLayout->addWidget(gbox_provider, 3, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 625, 22));
        menu_file = new QMenu(menuBar);
        menu_file->setObjectName(QStringLiteral("menu_file"));
        menu_edit = new QMenu(menuBar);
        menu_edit->setObjectName(QStringLiteral("menu_edit"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu_file->menuAction());
        menuBar->addAction(menu_edit->menuAction());
        menu_file->addAction(act_open);
        menu_file->addAction(act_saveTo);
        menu_edit->addAction(act_tmpsave);
        menu_edit->addAction(act_clear);
        mainToolBar->addAction(act_open);
        mainToolBar->addAction(act_saveTo);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        act_open->setText(QApplication::translate("MainWindow", "Open...", nullptr));
        act_saveTo->setText(QApplication::translate("MainWindow", "Save to...", nullptr));
        act_tmpsave->setText(QApplication::translate("MainWindow", "Save", nullptr));
        act_clear->setText(QApplication::translate("MainWindow", "Clear", nullptr));
        pb_add->setText(QApplication::translate("MainWindow", "Add", nullptr));
        pb_delete->setText(QApplication::translate("MainWindow", "Delete", nullptr));
        pb_edit->setText(QApplication::translate("MainWindow", "Edit", nullptr));
        pb_execute->setText(QApplication::translate("MainWindow", "Execute", nullptr));
        slbl_objects->setText(QApplication::translate("MainWindow", "Objects:", nullptr));
        lbl_filter->setText(QApplication::translate("MainWindow", "Current filter: 0", nullptr));
        gbox_provider->setTitle(QApplication::translate("MainWindow", "Provider:", nullptr));
        slbl_name->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        lbl_name->setText(QString());
        slbl_tariff->setText(QApplication::translate("MainWindow", "Tariff:", nullptr));
        slbl_users->setText(QApplication::translate("MainWindow", "Users:", nullptr));
        lbl_users->setText(QString());
        slbl_loss->setText(QApplication::translate("MainWindow", "Packet loss:", nullptr));
        lbl_loss->setText(QString());
        gbox_cloud->setTitle(QApplication::translate("MainWindow", "Data cloud", nullptr));
        slbl_cloudTariff->setText(QApplication::translate("MainWindow", "Tariff:", nullptr));
        slbl_cloudCapacity->setText(QApplication::translate("MainWindow", "Capacity:", nullptr));
        lbl_cloudTariff->setText(QString());
        lbl_cloudCapacity->setText(QString());
        lbl_tariff->setText(QString());
        menu_file->setTitle(QApplication::translate("MainWindow", "File", nullptr));
        menu_edit->setTitle(QApplication::translate("MainWindow", "Edit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
