/********************************************************************************
** Form generated from reading UI file 'provider_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROVIDER_DIALOG_H
#define UI_PROVIDER_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ProviderDialog
{
public:
    QFormLayout *formLayout_2;
    QLabel *slbl_name;
    QLineEdit *ledit_name;
    QLabel *slbl_tariff;
    QLabel *slbl_users;
    QLabel *slbl_loss;
    QGroupBox *gbox_cloud;
    QFormLayout *formLayout;
    QLabel *slbl_cloudTariff;
    QLabel *slbl_cloudCapacity;
    QDoubleSpinBox *sbox_cloudTariff;
    QDoubleSpinBox *sbox_cloudCapacity;
    QDialogButtonBox *buttonBox;
    QDoubleSpinBox *sbox_tariff;
    QDoubleSpinBox *sbox_loss;
    QSpinBox *sbox_users;

    void setupUi(QDialog *ProviderDialog)
    {
        if (ProviderDialog->objectName().isEmpty())
            ProviderDialog->setObjectName(QStringLiteral("ProviderDialog"));
        ProviderDialog->resize(399, 279);
        formLayout_2 = new QFormLayout(ProviderDialog);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setLabelAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        formLayout_2->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        slbl_name = new QLabel(ProviderDialog);
        slbl_name->setObjectName(QStringLiteral("slbl_name"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, slbl_name);

        ledit_name = new QLineEdit(ProviderDialog);
        ledit_name->setObjectName(QStringLiteral("ledit_name"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, ledit_name);

        slbl_tariff = new QLabel(ProviderDialog);
        slbl_tariff->setObjectName(QStringLiteral("slbl_tariff"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, slbl_tariff);

        slbl_users = new QLabel(ProviderDialog);
        slbl_users->setObjectName(QStringLiteral("slbl_users"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, slbl_users);

        slbl_loss = new QLabel(ProviderDialog);
        slbl_loss->setObjectName(QStringLiteral("slbl_loss"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, slbl_loss);

        gbox_cloud = new QGroupBox(ProviderDialog);
        gbox_cloud->setObjectName(QStringLiteral("gbox_cloud"));
        gbox_cloud->setEnabled(true);
        gbox_cloud->setFlat(false);
        gbox_cloud->setCheckable(true);
        gbox_cloud->setChecked(false);
        formLayout = new QFormLayout(gbox_cloud);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        slbl_cloudTariff = new QLabel(gbox_cloud);
        slbl_cloudTariff->setObjectName(QStringLiteral("slbl_cloudTariff"));

        formLayout->setWidget(0, QFormLayout::LabelRole, slbl_cloudTariff);

        slbl_cloudCapacity = new QLabel(gbox_cloud);
        slbl_cloudCapacity->setObjectName(QStringLiteral("slbl_cloudCapacity"));

        formLayout->setWidget(1, QFormLayout::LabelRole, slbl_cloudCapacity);

        sbox_cloudTariff = new QDoubleSpinBox(gbox_cloud);
        sbox_cloudTariff->setObjectName(QStringLiteral("sbox_cloudTariff"));

        formLayout->setWidget(0, QFormLayout::FieldRole, sbox_cloudTariff);

        sbox_cloudCapacity = new QDoubleSpinBox(gbox_cloud);
        sbox_cloudCapacity->setObjectName(QStringLiteral("sbox_cloudCapacity"));

        formLayout->setWidget(1, QFormLayout::FieldRole, sbox_cloudCapacity);


        formLayout_2->setWidget(4, QFormLayout::SpanningRole, gbox_cloud);

        buttonBox = new QDialogButtonBox(ProviderDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout_2->setWidget(5, QFormLayout::SpanningRole, buttonBox);

        sbox_tariff = new QDoubleSpinBox(ProviderDialog);
        sbox_tariff->setObjectName(QStringLiteral("sbox_tariff"));
        sbox_tariff->setMinimum(0);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, sbox_tariff);

        sbox_loss = new QDoubleSpinBox(ProviderDialog);
        sbox_loss->setObjectName(QStringLiteral("sbox_loss"));
        sbox_loss->setMaximum(100);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, sbox_loss);

        sbox_users = new QSpinBox(ProviderDialog);
        sbox_users->setObjectName(QStringLiteral("sbox_users"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, sbox_users);


        retranslateUi(ProviderDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProviderDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProviderDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ProviderDialog);
    } // setupUi

    void retranslateUi(QDialog *ProviderDialog)
    {
        ProviderDialog->setWindowTitle(QApplication::translate("ProviderDialog", "Dialog", nullptr));
        slbl_name->setText(QApplication::translate("ProviderDialog", "Name:", nullptr));
        ledit_name->setPlaceholderText(QApplication::translate("ProviderDialog", "<non-empty>", nullptr));
        slbl_tariff->setText(QApplication::translate("ProviderDialog", "Tariff:", nullptr));
        slbl_users->setText(QApplication::translate("ProviderDialog", "Users:", nullptr));
        slbl_loss->setText(QApplication::translate("ProviderDialog", "Packet loss:", nullptr));
        gbox_cloud->setTitle(QApplication::translate("ProviderDialog", "Data cloud", nullptr));
        slbl_cloudTariff->setText(QApplication::translate("ProviderDialog", "Tariff:", nullptr));
        slbl_cloudCapacity->setText(QApplication::translate("ProviderDialog", "Capacity:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProviderDialog: public Ui_ProviderDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROVIDER_DIALOG_H
