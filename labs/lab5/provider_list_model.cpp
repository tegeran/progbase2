#include "provider_list_model.h"
<<<<<<< HEAD
#include <utility>

ProviderListModel::ProviderListModel(QObject * parent, QVector<Provider> && vect)
	: QAbstractListModel(parent), vect(std::move(vect))
{
}

int ProviderListModel::rowCount(const QModelIndex &parent) const{
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	return parent.isValid() ? 0 : vect.size();
}

QVariant ProviderListModel::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant();

	switch (role){
		case Qt::DisplayRole: { return QVariant(vect[index.row()].name()); }
		default: return QVariant();
	}
}

Qt::ItemFlags ProviderListModel::flags(const QModelIndex &index) const {
	if (!index.isValid())
		return Qt::NoItemFlags;
	return   Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled
			| Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool ProviderListModel::insertRows(int row, int count, const QModelIndex &parent){
	beginInsertRows(parent, row, row + count - 1);
	vect.insert(row, count, Provider());
	endInsertRows();
	return true;
}

bool ProviderListModel::removeRows(int row, int count, const QModelIndex &parent){
	beginRemoveRows(parent, row, row + count - 1);
	vect.remove(row, count);
	endRemoveRows();
	return true;
}

QVector<Provider> & ProviderListModel::providers(){
	return vect;
}

void ProviderListModel::setProviderAt(const int & index, Provider && provider){
	vect[index] = std::move(provider);
	emit dataChanged(this->index(index), this->index(index), QVector<int>() << Qt::DisplayRole);
}

void ProviderListModel::insertProviderAt(const int & index, Provider && provider){
	beginInsertRows(QModelIndex(), index, index);
	vect.insert(index, std::move(provider));
	endInsertRows();
}

void ProviderListModel::clearProviders(){
	beginResetModel();
	vect.clear();
	endResetModel();
}

void ProviderListModel::setProviders(QVector<Provider> && donor){
	beginResetModel();
	vect = std::move(donor);
	endResetModel();
	emit dataChanged(QModelIndex(), QModelIndex(), QVector<int>() << Qt::DisplayRole);
=======

ProviderListModel::ProviderListModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

int ProviderListModel::rowCount(const QModelIndex &parent) const
{
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	if (parent.isValid())
		return 0;

	// FIXME: Implement me!
}

QVariant ProviderListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	// FIXME: Implement me!
	return QVariant();
}

bool ProviderListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (data(index, role) != value) {
		// FIXME: Implement me!
		emit dataChanged(index, index, QVector<int>() << role);
		return true;
	}
	return false;
}

Qt::ItemFlags ProviderListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool ProviderListModel::insertRows(int row, int count, const QModelIndex &parent)
{
	beginInsertRows(parent, row, row + count - 1);
	// FIXME: Implement me!
	endInsertRows();
}

bool ProviderListModel::removeRows(int row, int count, const QModelIndex &parent)
{
	beginRemoveRows(parent, row, row + count - 1);
	// FIXME: Implement me!
	endRemoveRows();
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498
}
