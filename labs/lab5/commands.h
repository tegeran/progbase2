#ifndef COMMANDS_H
#define COMMANDS_H
<<<<<<< HEAD
#include <QUndoCommand>

#include "mainwindow.h"
#include "provider.h"

namespace Command {
	class Add : public QUndoCommand {
	public:
		Add(MainWindow & win, Provider && prov, int index, QUndoCommand * comm = nullptr);

		void undo() override;
		void redo() override;
	private:
		MainWindow & win;
		Provider prov;
		int index;
	};

	class Delete: public QUndoCommand {
	public:
		Delete(MainWindow & win, int index, QUndoCommand * comm = nullptr);

		void undo() override;
		void redo() override;
	private:
		MainWindow & win;
		Provider prov;
		int index;
	};

	class Edit: public QUndoCommand {
	public:
		Edit(MainWindow & win, Provider && prov, int index, QUndoCommand * comm = nullptr);

		void undo() override;
		void redo() override;
	private:
		MainWindow & win;
		Provider prov;
		int index;
	};
}


=======
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498

#endif // COMMANDS_H
