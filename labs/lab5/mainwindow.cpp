#include "mainwindow.h"
#include "ui_mainwindow.h"
<<<<<<< HEAD
#include "error.h"
#include "provider_dialog.h"
#include "commands.h"
#include "qext.h"
#include <QMessageBox>
#include <QFileDialog>

#define TMP_PATH "/tmp/op2lab5.tmp"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	ustack(this),
	model(new ProviderListModel(this)),
	proxyModel(new ProviderFilterProxyModel(QSet<Provider *>(), this)),
	currentFilter(0)
{
	ui->setupUi(this);
	ustack.setUndoLimit(5000);
	connect(&ustack, SIGNAL(cleanChanged(bool)), this, SLOT(on_ustack_clean_changed(bool)));

	QAction * undo(ustack.createUndoAction(this));
	QAction * redo(ustack.createRedoAction(this));
	ui->menu_edit->insertAction(ui->act_tmpsave, redo);
	ui->menu_edit->insertAction(redo, undo);
	undo->setShortcut(QKeySequence(Qt::Key_Z | Qt::CTRL));
	redo->setShortcut(QKeySequence(Qt::Key_Y | Qt::CTRL));
	ui->act_saveTo->setShortcut(QKeySequence::SaveAs);
	ui->act_open->setShortcut(QKeySequence::Open);
	ui->act_tmpsave->setShortcut(QKeySequence::Save);

	connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
			this,  SLOT(on_model_data_changed(QModelIndex, QModelIndex, QVector<int>)));
	ui->lv_names->setModel(model);
	proxyModel->setSourceModel(model);
	proxyModel->setDynamicSortFilter(true);
	ui->lv_execs->setModel(proxyModel);

	try {
		model->setProviders(Provider::fromJsonFile(TMP_PATH));
		setStatus(tr("Successfully loaded previous session"));
	} catch (const MessageException & ex) {
		setStatus(tr("Failed to load previous session: %1").arg(ex.what()));
	}
	if (model->providers().isEmpty()){
		ui->gbox_provider->setEnabled(false);
		ui->gbox_cloud->hide();
	} else {
		ui->sbox_exec->setMaximum(model->rowCount());
	}

	connect(ui->lv_names->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			this, SLOT(on_lv_names_currentChanged(QModelIndex,QModelIndex)));
	connect(ui->lv_execs->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
			this, SLOT(on_lv_execs_currentChanged(QModelIndex,QModelIndex)));
}

MainWindow::~MainWindow(){
	delete ui;
}

void MainWindow::on_model_data_changed(QModelIndex, QModelIndex, QVector<int>){
	updateLables();
	ui->sbox_exec->setMaximum(model->rowCount());
	updateProxyModel();
}


void MainWindow::on_ustack_clean_changed(bool yes){
	this->setWindowTitle(yes ? "Providererr" : "Providererr*");
}

void MainWindow::on_act_tmpsave_triggered(){
	try {
		Provider::toJsonFile(model->providers(), TMP_PATH);
		setStatus(tr("Successfully saved current session"));
		ustack.setClean();
	} catch (const MessageException & ex) {
		setStatus(tr("Failed to save current session: %1").arg(ex.what()));
	}
}

void MainWindow::on_pb_add_clicked(){
	ProviderDialog dial;
	if (dial.exec() == QDialog::Accepted){
		int selected(current());
		ustack.push(new Command::Add(
						*this,
						std::move(dial.provider()),
						selected < 0 ? model->providers().size() : selected
					)
		);
		ui->sbox_exec->setMaximum(model->rowCount());
	}
}

void MainWindow::on_pb_delete_clicked(){
	ustack.push(new Command::Delete(*this, current()));
	ui->sbox_exec->setMaximum(model->rowCount());
	if (currentFilter > model->rowCount()){
		currentFilter = model->rowCount();
		updateProxyModel();
	}
}

void MainWindow::on_pb_edit_clicked(){
	int selected(current());
	ProviderDialog dial(model->providers()[selected]);
	if (dial.exec() == QDialog::Accepted){
		ustack.push(new Command::Edit(
						*this,
						std::move(dial.provider()),
						selected
					)
		);
	}
}

void MainWindow::on_lv_names_doubleClicked(const QModelIndex &index){
	if (index.isValid()){
		on_pb_edit_clicked();
	}
}

void MainWindow::on_lv_execs_doubleClicked(const QModelIndex &index){
	if (index.isValid()){
		on_pb_edit_clicked();
	}
}

void MainWindow::on_lv_names_currentChanged(const QModelIndex & cur, const QModelIndex &){
	QLOG("NAMES SELECTED");
	if (cur.isValid()){
		ui->pb_delete->setEnabled(true);
		ui->pb_edit->setEnabled(true);
		QModelIndex proxyIndex(proxyModel->mapFromSource(cur));
		if (!proxyIndex.isValid()){
			ui->lv_execs->clearSelection();
			ui->lv_execs->selectionModel()->clearCurrentIndex();
		} else {
			ui->lv_execs->setCurrentIndex(proxyIndex);
		}
	} else {
		ui->lv_execs->clearSelection();
		ui->pb_edit->setEnabled(false);
		ui->pb_delete->setEnabled(false);
	}
	updateLables();
}

void MainWindow::on_lv_execs_currentChanged(const QModelIndex & cur, const QModelIndex &){
	QLOG("EXECS SELECTED");
	if (cur.isValid()){
		ui->lv_names->setCurrentIndex(proxyModel->mapToSource(cur));
	}
}


void MainWindow::on_pb_execute_clicked(){
	int filter(ui->sbox_exec->value());
	if (currentFilter != filter){
		ui->lbl_filter->setText(tr("Current filter: %1").arg(filter));
		currentFilter = filter;
		updateProxyModel();
	}
}

// METHODS =======================================================================

void MainWindow::insertProvider(const int & index, Provider && prov){
	Debug_maxindex(index, model->providers().size());
	model->insertProviderAt(index, std::move(prov));
}

void MainWindow::deleteProvider(const int & index){
	Debug_maxindex(index, model->providers().size() - 1);
	model->removeRow(index);
}

void MainWindow::setStatus(const QString & status){
	ui->statusBar->showMessage(status);
}

QStatusBar & MainWindow::sbar(){
	return *ui->statusBar;
}

int MainWindow::current() const{
	QModelIndex ind(ui->lv_names->currentIndex());
	return ind.isValid() ? ind.row() : -1;
}

void MainWindow::updateLables() {
	int cur(current());
	if (cur == -1){
		ui->lbl_name->setText("");
		ui->lbl_tariff->setText("");
		ui->lbl_users->setText("");
		ui->lbl_loss->setText("");
		ui->gbox_provider->setEnabled(false);
		ui->gbox_cloud->hide();
		return;
	}
	ui->gbox_provider->setEnabled(true);
	const Provider & prov = model->providers()[cur];
	ui->lbl_name->setText(prov.name());
	ui->lbl_tariff->setText(tr("%1$").arg(prov.tariff()));
	ui->lbl_loss->setText(tr("%1%").arg(prov.loss()));
	ui->lbl_users->setText(QString::number(prov.users()));
	if (prov.hasCloud()){
		ui->gbox_cloud->show();
		ui->lbl_cloudTariff->setText(tr("%1$").arg(prov.cloud().tariff()));
		ui->lbl_cloudCapacity->setText(tr("%1Tb").arg(prov.cloud().capacity()));
	} else {
		ui->gbox_cloud->hide();
	}
}

void MainWindow::updateProxyModel(){
	if (currentFilter == 0){
		proxyModel->setSet(QSet<Provider *>());
		return;
	}
	Provider::PriorityQueByTariff que = Provider::toStdPriorityQueueByTariff(model->providers());
	proxyModel->setSet(std::move(Qext::Set::fromStdPriorityQueue(que, currentFilter)));
	proxyModel->sort(0, Qt::DescendingOrder);
}

void MainWindow::on_act_open_triggered(){
	if (QMessageBox::question(this, tr("Are you sure?"), tr("Are you sure tou want to open new file?"
															"\nAll unsaved providers will be lost"),
							  QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Cancel)
	{
		return;
	}
	QString path(QFileDialog::getOpenFileName(this, "Select json file"));
	if (path.isEmpty()){ return; }
	try {
		model->setProviders(Provider::fromJsonFile(path));
		ustack.clear();
		on_act_tmpsave_triggered();
		setStatus(tr("Successfully loaded new project"));
		updateProxyModel();
	} catch (const MessageException & ex){
		QMessageBox::critical(this, tr("SIGSEGV"), tr("SIGSEGV: %1").arg(ex.what()));
		setStatus(tr("Failed to open new project"));
	}
}
=======

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498
