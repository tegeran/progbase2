#ifndef PROVIDERFILTERPROXYMODEL_H
#define PROVIDERFILTERPROXYMODEL_H

<<<<<<< HEAD
#include <QSortFilterProxyModel>
#include "provider.h"
#include <QSet>

class ProviderFilterProxyModel : public QSortFilterProxyModel {
	Q_OBJECT

public:
	explicit ProviderFilterProxyModel(
									  QSet<Provider *> && set = QSet<Provider *>(),
									  QObject *parent = nullptr
	);
	void setSet(QSet<Provider *> && set);
private:
	QSet<Provider *> set;

	// QSortFilterProxyModel interface
protected:
	bool filterAcceptsRow(int source_row, const QModelIndex & source_parent) const override;
	bool lessThan(const QModelIndex & source_left, const QModelIndex & source_right) const override;

};

#endif // PROVIDERFILTERPROXYMODEL_H
=======
#include <QAbstractItemModel>

class ProviderFilterProxyModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	explicit ProviderFilterProxyModel(QObject *parent = nullptr);

	// Basic functionality:
	QModelIndex index(int row, int column,
					  const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex parent(const QModelIndex &index) const override;

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
};

#endif // PROVIDERFILTERPROXYMODEL_H
>>>>>>> d60606abb1fdee4a7080ca9e5b99690a09192498
