#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <tree.h>
#include <tree_ext.h>
#include <trie.h>
#include <wchar.h>
#include <floatingpoint.h>
#include <mem.h>
#include <list.h>
#include <lexer.h>
#include <list_ext.h>
#include <token.h>
#include <ctype.h>
#include <sstring.h>
#include <hash_table.h>
#include <assert.h>
#include <dict.h>
#include <var_integer.h>
#include <parser.h>

bool isWordLetter (char ch){
    return isalnum(ch);
}


int main(void) {
    system("clear");
    setlocale(LC_CTYPE, "");

    String * str = String_new();
    String_readFromPath("../main.js", str);

    List * tokens = List_new();
    if (Lexer_splitTokens(String_readOnly(str), tokens) != LEXER_SUCCESS){
        String_free(str);
        List_free(tokens);
        return EXIT_FAILURE;
    }
    Tree * ast = Parser_buildNewAstTree(tokens, str);
    if (ast) {
        // Tree_printFancy(ast, loc(1, 1), (Printer)AstNode_printWide, true);
        //  ^^^^  deprecated as is not applicable for  ^^^^
        // trees with size more than a single console window
        //
        // String * str declaration near entry point
        //
        String_clear(str);
        Tree_appendToString(ast, str, (StringWriter)AstNode_appendToString, true);
        String_print(str);
        String_writeToPath("../main_ast.txt", str);
        //
        // releasing memory
        String_free(str);
        Tree_freeWhole(ast, (Destructor)AstNode_freeWhole);
        Lexer_clearTokens(tokens);
        List_free(tokens);
    
       return EXIT_SUCCESS;
    } else { // case ast == NULL
        String_free(str);
        Lexer_clearTokens(tokens);
        List_free(tokens);
       return EXIT_FAILURE;    
    }
}