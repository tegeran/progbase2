#include "lexertl/stream_shared_iterator.hpp"
#include <fstream>
#include "lexertl/generator.hpp"
#include <iostream>
#include <cui.h>
#include "lexertl/lookup.hpp"
#include "lexer.h"
#include "var_substring.h"

int main () {
    using namespace Calamity;
    Lexer lexer;

    String code;
    try {
        code = String::readFromFile("../main.js");
    } catch (const MessageException & exception){
        std::cout << "SIGSEGV: " << exception.what();
        return EXIT_FAILURE;
    }
    String errorStr(lexer.splitTokens(&code));
    std::cout << Cui::reverse
              << Cui::bold
              <<"Lexical analysis finished with code:"
              << Cui::reset
              << '\n'
              << errorStr;

    return EXIT_SUCCESS;
}