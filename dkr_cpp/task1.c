#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <tree.h>
#include <tree_ext.h>
#include <trie.h>
#include <wchar.h>
#include <floatingpoint.h>
#include <mem.h>
#include <list.h>
#include <lexer.h>
#include <list_ext.h>
#include <token.h>
#include <ctype.h>
#include <sstring.h>
#include <hash_table.h>
#include <assert.h>
#include <dict.h>
#include <var_integer.h>
#include <parser.h>

bool isWordLetter (char ch){
    return isalnum(ch);
}


int main(void) {
    system("clear");
    setlocale(LC_CTYPE, "");
    
    String * str = String_new();
    String_readFromPath("../main.js", str);

    List * tokens = List_new();
    if (Lexer_splitTokens(String_readOnly(str), tokens) != LEXER_SUCCESS){
        List_free(tokens);
        String_free(str);
        return EXIT_FAILURE;
    }
    String_free(str);
    //
    Lexer_printTokensWide(tokens);
    Lexer_writeTokensTo("../main_tokens.txt", tokens);
    Lexer_clearTokens(tokens);
    List_free(tokens);

    return EXIT_SUCCESS;
}