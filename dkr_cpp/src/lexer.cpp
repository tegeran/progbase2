#include <iostream>
#include <lexer.h>
#include <error.h>
#include <token.h>
#include <cui.h>
#include <lexertl/match_results.hpp>
#include <boost/range.hpp>
#include "lexertl_gen.h"

namespace Calamity {

    static String unexpectedToken(const String & code, String::const_iterator & tokenIt){
        Point2D<size_t> mistakePoint(code.point2DOf(tokenIt));
        String errorStr;
        errorStr.push_back(
            "unexpected token [", *tokenIt ,"] at (", mistakePoint.y, " : ", mistakePoint.x, ")\n",
            mistakePoint.y,". [", code.lineOf(tokenIt), ']'
        );
        return errorStr;
    }

    String Lexer::splitTokens(const String * const & code) {
        namespace ltl = lexertl;
        using Cui::newline;

        if (!code){
            return "code == nullptr";
        }
        m_code = code;
        ltl::smatch match(code->begin(), code->end());

        std::cout << Cui::bold << "Starting tokenization...\n" << Cui::bold_off;

        for (ltlgen::lookup(match); match.id; ltlgen::lookup(match)) {
            if (match.id == ltl::smatch::npos()){
                return unexpectedToken(*code, match.first);
            }

            if (match.id == static_cast<decltype(match.id)>(Token::Type::Newline)) {
                std::cout << newline;
            } else {
                m_tokens.emplace_back(
                    static_cast<Token::Type>(match.id),
                    match.first,
                    match.second
                );
                std::cout << m_tokens.back() << " ";
            }


        }

        std::cout << Cui::bold << "\nFinished tokenization\n" << Cui::bold_off;

        return "success";
    }
}