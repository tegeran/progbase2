#include "message_exception.h"
#include <string>
#include "error.h"


MessageException::MessageException(std::string &&error)
	: m_error(std::move(error))
{
	Debug_log("MessageException instance was thrown -> " << m_error.c_str());
}

MessageException::MessageException(const std::string &error)
	: m_error(error) {
	Debug_log("MessageException instance was thrown -> " << m_error.c_str());
}

const char *MessageException::what() const noexcept{
    return m_error.c_str();
}
