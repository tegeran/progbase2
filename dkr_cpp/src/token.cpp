#include <token.h>
#include <error.h>
#include <cui.h>
#include <lexertl/rules.hpp>

namespace Calamity {

    static const Token::TokenType_Name_Regex grammarInfo[]{
        { Token::Type::Eoi            , "Eoi"      , "<no_regex_eoi>"        },
        { Token::Type::Enum           , "enum"     , R"(enum)"               },
        { Token::Type::Void           , "void"     , R"(void)"               },
        { Token::Type::Try            , "try"      , R"(try)"                },
        { Token::Type::Catch          , "catch"    , R"(catch)"              },
        { Token::Type::Switch         , "switch"   , R"(switch)"             },
        { Token::Type::Case           , "case"     , R"(case)"               },
        { Token::Type::This           , "this"     , R"(this)"               },
        { Token::Type::Break          , "break"    , R"(break)"              },
        { Token::Type::Continue       , "continue" , R"(continue)"           },
        { Token::Type::Do             , "do"       , R"(do)"                 },
        { Token::Type::In             , "in"       , R"(in)"                 },
        { Token::Type::Default        , "default"  , R"(default)"            },
        { Token::Type::Delete         , "delete"   , R"(delete)"             },
        { Token::Type::Throw          , "throw"    , R"(throw)"              },
        { Token::Type::Import         , "import"   , R"(import)"             },
        { Token::Type::Finally        , "finally"  , R"(finally)"            },
        { Token::Type::Typeof         , "typeof"   , R"(typeof)"             },
        { Token::Type::Yield          , "yield"    , R"(yield)"              },
        { Token::Type::Var            , "var"      , R"(var)"                },
        { Token::Type::Let            , "let"      , R"(let)"                },
        { Token::Type::Const          , "const"    , R"(const)"              },
        { Token::Type::New            , "new"      , R"(new)"                },
        { Token::Type::False          , "false"    , R"(false)"              },
        { Token::Type::For            , "for"      , R"(for)"                },
        { Token::Type::Function       , "function" , R"(function)"           },
        { Token::Type::If             , "if"       , R"(if)"                 },
        { Token::Type::Else           , "else"     , R"(else)"               },
        { Token::Type::True           , "true"     , R"(true)"               },
        { Token::Type::Undefined      , "undefined", R"(undefined)"          },
        { Token::Type::Null           , "null"     , R"(null)"               },
        { Token::Type::While          , "while"    , R"(while)"              },
        { Token::Type::Return         , "return"   , R"(return)"             },
        { Token::Type::Endl           , "endl"     , R"(endl)"               },
        { Token::Type::Identity       , "==="      , R"(===)"                },
        { Token::Type::NotIdentity    , "!=="      , R"(!==)"                },
        { Token::Type::Increment      , "++"       , R"(\+\+)"               },
        { Token::Type::Decrement      , "--"       , R"(\-\-)"               },
        { Token::Type::And            , "&&"       , R"(&&)"                 },
        { Token::Type::Assign         , "="        , R"(=)"                  },
        { Token::Type::RightCurly     , "}"        , R"(\})"                 },
        { Token::Type::LeftCurly      , "{"        , R"(\{)"                 },
        { Token::Type::BitAnd         , "&"        , R"(&)"                  },
        { Token::Type::BitOr          , "|"        , R"(\|)"                 },
        { Token::Type::BitNot         , "~"        , R"(~)"                  },
        { Token::Type::BitXor         , "^"        , R"(\^)"                 },
        { Token::Type::Coma           , ","        , R"(,)"                  },
        { Token::Type::Division       , "/"        , R"(\/)"                 },
        { Token::Type::Equality       , "=="       , R"(==)"                 },
        { Token::Type::GreaterOrEqual , ">="       , R"(>=)"                 },
        { Token::Type::GreaterThan    , ">"        , R"(>)"                  },
        { Token::Type::LeftBracket    , "["        , R"(\[)"                 },
        { Token::Type::LeftPar        , "("        , R"(\()"                 },
        { Token::Type::LessOrEqual    , "<="       , R"(<=)"                 },
        { Token::Type::LessThan       , "<"        , R"(<)"                  },
        { Token::Type::Minus          , "-"        , R"(\-)"                 },
        { Token::Type::Multiplication , "*"        , R"(\*)"                 },
        { Token::Type::Neq            , "!="       , R"(!=)"                 },
        { Token::Type::Not            , "!"        , R"(!)"                  },
        { Token::Type::Or             , "||"       , R"(\|\|)"               },
        { Token::Type::Plus           , "+"        , R"(\+)"                 },
        { Token::Type::Power          , "**"       , R"(\*\*)"               },
        { Token::Type::Ellipsis       , "..."      , R"(\.\.\.)"             },
        { Token::Type::Remainder      , "%"        , R"(%)"                  },
        { Token::Type::RightBracket   , "]"        , R"(\])"                 },
        { Token::Type::RightPar       , ")"        , R"(\))"                 },
        { Token::Type::Colon          , ":"        , R"(:)"                  },
        { Token::Type::Semicolon      , ";"        , R"(;)"                  },
        { Token::Type::Arrow          , "=>"       , R"(=>)"                 },
        { Token::Type::Dot            , "."        , R"(\.)"                 },
        { Token::Type::Identifier     , "ID"       , R"([a-zA-Z_$](\w|\$)*)" },
        { Token::Type::String         , "STR"      , R"((\".*?\")|('.*?'))"  },
        { Token::Type::Number         , "NUMBER"   , R"(\d+(\.?\d*))"        },
        { Token::Type::Newline        , "\\n"      , R"(\n)"                 },
        { Token::Type::SLComment      , "<sl_comm>", R"(\/\/.*?($|\n))"      },
        { Token::Type::MLComment      , "<ml_comm>", R"(\/\*.*?\*\/)"        },
        { Token::Type::Whitespace     , "<whitesp>", R"((\s{-}\n)+)"         }
    };


    bool Token::isKeyword(const Token::Type & suspect) {
        return suspect >= Token::Type::M_FirstKetword && suspect <= Token::Type::M_LastKeyword;
    }


    bool Token::isPunctuation(const Token::Type & suspect) {
        return suspect >= Token::Type::M_FirstPunct && suspect <= Token::Type::M_LastPunct;
    }

    Token::Token(
            const Token::Type & type,
            const String::const_iterator & begin,
            const String::const_iterator & end)
            : m_type(type), m_substr(begin, end) {}


    bool Token::operator==(const Token & other) const {
        return this == &other || (
                m_substr == other.m_substr
                && m_type == other.m_type
        );
    }

    std::ostream & operator<<(std::ostream & stream, const Token::Type & type) {
        return stream << Token::typeName(type);
    }

    const char * Token::typeName() const {
        return typeName(m_type);
    }

    const char * Token::typeName(const Type & type) {
        return grammarInfo[static_cast<int>(type)].name;
    }

    std::ostream & operator<<(std::ostream & stream, const Token & self) {
        typedef Token::Type Type;
        using namespace Cui;

        if (self.isKeyword()) {
            return stream << fgnd(2, 2, 5)
                          << '{'
                          << bold
                          << self.type()
                          << bold_off
                          << '}'
                          << reset;
        } else if (self.isPunctuation()) {
            return stream << fgnd(5, 2, 0)
                          << '{'
                          << bold
                          << self.type()
                          << bold_off
                          << '}'
                          << reset;
        }
        stream << '{' << bold << self.type();
        switch (self.type()) {
            case Type::Identifier:
                [[fallthrough]]
            case Type::Number:
            case Type::String:{
                stream << ", " << self.m_substr << bold_off;
                break;
            }
            default: {

            }
        }
        return stream << '}';
    }

    String & operator+=(String & string, const Token & self) {
        typedef Token::Type Type;
        string += "{";
        string += self.typeName();
        switch (self.type()) {
            case Type::Number:
            case Type::Identifier: {
                (string += ", ") += self.m_substr;
                break;
            }
            case Type::String: {
                ((string += ", \"") += self.m_substr) += "\"";
                break;
            }
            default: break;
        }
        return string += "}";
    }

    lexertl::rules Token::makeLexertlRules() {
        lexertl::rules rules(0); // zero in order to omit default dot_not_newline option
        for (auto & iterator(grammarInfo + 1);
                          iterator != std::end(grammarInfo);
                          ++iterator
            ){
            rules.push(
                iterator->regex,
                static_cast<lexertl::rules::id_type>(iterator->type)
            );
        }
        return rules;
    }


}