#include "var_boolean.h"
#include <error.h>

namespace Calamity {

    Boolean::Boolean(const bool & b) : m_bool(b) {}

    Boolean::operator bool() const {
        return m_bool;
    }

    Boolean & Boolean::operator=(const bool & boolean) {
        m_bool = boolean;
        return *this;
    }


    std::ostream & operator<<(std::ostream & stream, const Boolean & self) {
        return stream << self.toCString();
    }

    const char * Boolean::toCString() const {
        return m_bool ? "true" : "false";
    }

    const char16_t * Boolean::toU16CString() const {
        return m_bool ? u"true" : u"false";
    }

    bool Boolean::operator==(const Boolean & other) const {
        return other.m_bool == m_bool;
    }

    bool Boolean::operator!=(const Boolean & other) const {
        return other.m_bool != m_bool;
    }

    bool Boolean::toggle() {
        return (m_bool = !m_bool);
    }

    bool Boolean::value() const {
        return m_bool;
    }

    String Boolean::toString() const {
        return toCString();
    }

}