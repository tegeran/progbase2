#pragma once

#include <iostream>
#include "std_ext.h"
#include "var_string.h"

namespace Calamity {

	class Boolean {
		bool m_bool;

	public:
		DECL_DEFAULT_COPY_AND_MOVE(Boolean)

		Boolean(const bool &b = false);

		explicit operator bool() const;
		bool value() const;

		Boolean &operator=(const bool &boolean);
		friend std::ostream &operator<<(std::ostream &stream, const Boolean &self);

		void print(std::ostream &stream = std::cout) { stream << *this; }

		String toString() const;
		const char *toCString() const;
		const char16_t *toU16CString() const;

		bool operator==(const Boolean &other) const;
		bool operator!=(const Boolean &other) const;

		// returns new toggled value
		bool toggle();
	};
}