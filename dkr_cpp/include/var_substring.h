//
// Created by tegeran on 01.07.18.
//

#ifndef TASK3_OUT_VAR_SUBSTR_H
#define TASK3_OUT_VAR_SUBSTR_H

#include <algorithm>
#include <boost/range.hpp>
#include <boost/lexical_cast.hpp>
#include "var_string.h"

namespace Calamity {
    template<typename TChar>
    class BasicSubstring : public boost::iterator_range<typename BasicString<TChar>::const_iterator> {
    public:
        typedef boost::iterator_range<typename BasicString<TChar>::const_iterator> SuperRange;
        typedef BasicString<TChar> SuperString;
        using SuperRange::SuperRange;

        inline typename SuperString::size_type amountOf(const TChar & character) const{
            return static_cast<typename SuperString::size_type>(
                std::count(SuperRange::begin(), SuperRange::end(), character)
            );
        }

        inline double parseDouble() const {
            auto beginIt(SuperRange::begin());
            return boost::lexical_cast<double>(&(*beginIt), SuperRange::end() - beginIt);
        }

        inline bool operator==(const BasicSubstring & other) const
        { return SuperRange::begin() == other.begin() && SuperRange::end() == other.end(); }



    };


    template <typename TChar>
    inline BasicSubstring<TChar> makeSubstring(
        const typename BasicString<TChar>::const_iterator & begin,
        const typename BasicString<TChar>::const_iterator & end
    ) { return BasicSubstring<TChar>(begin, end); }

    typedef BasicSubstring<char> Substring;

}


#endif //TASK3_OUT_VAR_SUBSTR_H
