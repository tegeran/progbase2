#ifndef MESSAGEEXCEPTION_H
#define MESSAGEEXCEPTION_H

#include <stdexcept>
#include <string>

class  MessageException : public std::exception {
    std::string m_error;
public:

    explicit MessageException(const std::string & error);
    explicit MessageException(std::string && error);


    virtual const char * what() const noexcept final override;
};

#endif // MESSAGEEXCEPTION_H
