#ifndef XMLENTITYSTORAGE_H
#define XMLENTITYSTORAGE_H
#include "pupil_storage.h"
#include "pupil.h"
#include "message_exception.h"
#include <string>
#include <vector>

class XmlPupilStorage : public PupilStorage {
public:
    XmlPupilStorage(const std::string & filePath);
    XmlPupilStorage(std::string && filePath);

    std::vector<Pupil> load() const;
    void save(const std::vector<Pupil> & entities) const;
};

#endif // XMLENTITYSTORAGE_H
