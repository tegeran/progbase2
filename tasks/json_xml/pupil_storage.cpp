#include "pupil_storage.h"
#include <string>

PupilStorage::PupilStorage(const std::string &name)
    : _name(name) {}


PupilStorage::PupilStorage(std::string &&name)
    : _name(name) {}

std::string & PupilStorage::name(){
    return this->_name;
}

const std::string &PupilStorage::name() const {
    return this->_name;
}

QFile * PupilStorage::open(const enum QIODevice::OpenModeFlag &mode) const{
    QFile * file = new QFile(QString::fromStdString(this->name()));
    if (!file->open(mode)){
        delete file;
        throw MessageException("failed to open file \'" + this->name() + '\'');
    }
    return file;
}
