#include "storage_manager.h"
#include <string>
#include "message_exception.h"
#include "pupil_storage.h"
#include <utility>

PupilStorage * StorageManager::createStorage(std::string storageFileName) {
    std::string::size_type dot = storageFileName.rfind('.');

    if (dot == std::string::npos
     || storageFileName.back() == '.'){
        //
        throw MessageException("unspecified file extension \"" + storageFileName + "\"");
    }

    if (!storageFileName.compare(dot + 1, std::string::npos, "xml")){
        return new XmlPupilStorage(std::move(storageFileName));
    }
    if (!storageFileName.compare(dot + 1, std::string::npos, "json")){
        return new JsonPupilStorage(std::move(storageFileName));
    }

    throw MessageException("invalid file extension \"" + storageFileName +  "\"");
}
