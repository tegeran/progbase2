#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H

#include "pupil_storage.h"
#include <string>
#include "message_exception.h"
#include "xml_pupil_storage.h"
#include "json_pupil_storage.h"

class StorageManager {
public:
    StorageManager() = delete;

    // no reference for argument, reasons in pupil.h

    static PupilStorage * createStorage(std::string storageFileName); // throws MessageException
};


#endif // STORAGEMANAGER_H
