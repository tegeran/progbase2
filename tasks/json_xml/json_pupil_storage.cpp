#include "json_pupil_storage.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QTextStream>
#include <QDataStream>
#include <string>
#include "std_ext.h"
#include <QSet>
#include <utility>
#include "qext.h"

JsonPupilStorage::JsonPupilStorage(const std::string &filePath)
    : PupilStorage(filePath) {}

JsonPupilStorage::JsonPupilStorage(std::string &&filePath)
    : PupilStorage(filePath) {}

namespace JJson{
    template <int N>
    static bool onlyKeys(const QJsonObject &obj, const QString (&keys)[N]){
        if (obj.size() != N)
            return false;
        for (int i = 0; i < N; ++i){
            if (!obj.contains(keys[i]))
                return false;
        }
        return true;
    }
}

std::vector<Pupil> JsonPupilStorage::load() const {
    QJsonDocument document = Qext::File::Json::read(QString::fromStdString(this->name()));

    QSet<std::string *> bookSet;

    auto check_syntax = [&bookSet](const bool & check) mutable -> void{
        if (!check){
            std_ext::kill(bookSet);
            throw MessageException("invalid json instance syntax");
        }
    };

    check_syntax(document.isArray());
    QJsonArray pupArr = document.array();
    std::vector<Pupil> vect;
    vect.reserve(pupArr.size());
    for (const QJsonValue & pupVal: pupArr){
        check_syntax(pupVal.isObject());
        QJsonObject pupObj = pupVal.toObject();
        check_syntax(JJson::onlyKeys(pupObj,{
             QStringLiteral("name"),
             QStringLiteral("_class"),
             QStringLiteral("age"),
             QStringLiteral("score"),
             QStringLiteral("alive"),
             QStringLiteral("textbooks")
        }));
        QJsonValueRef val = pupObj["name"];
        check_syntax(val.isString());
        Pupil pup(val.toString().toStdString());

        val = pupObj["_class"];
        check_syntax(val.isString());
        pup._class = val.toString().toStdString();

        val = pupObj["age"];
        check_syntax(val.isDouble());
        pup.age = val.toInt();

        val = pupObj["score"];
        check_syntax(val.isDouble());
        pup.score = val.toDouble();

        val = pupObj["alive"];
        check_syntax(val.isBool());
        pup.alive = val.toBool();

        val = pupObj["textbooks"];
        check_syntax(val.isArray());
        for(const QJsonValue & book : val.toArray()){ // rvalue's lifetime is extended
            check_syntax(book.isString());
            std::string title = book.toString().toStdString();
            if (!bookSet.contains(&title)){
                std::string * newBook = new std::string(std::move(title));
                pup.textbooks.emplace_back(newBook);
                bookSet << newBook;
            } else {
               pup.textbooks.emplace_back(*(bookSet.find(&title)));
            }
        }
        vect.emplace_back(std::move(pup));
    }
    return vect;
}



void JsonPupilStorage::save(const std::vector<Pupil> &entities) const {
    QJsonDocument document;
    QJsonArray pupArr;
    for (const Pupil & pup : entities){
        QJsonObject pupObj;
        pupObj.insert("name",   QJsonValue(QString::fromStdString(pup.name)));
        pupObj.insert("_class", QJsonValue(QString::fromStdString(pup._class)));
        pupObj.insert("age",   QJsonValue(pup.age));
        pupObj.insert("score",  QJsonValue(pup.score));
        pupObj.insert("alive",  QJsonValue(pup.alive));
        QJsonArray booksArr;
        for (std::string * const & book : pup.textbooks){
            booksArr << QJsonValue(QString::fromStdString(*book));
        }
        pupObj.insert("textbooks", booksArr);
        pupArr << pupObj;
    }
    document.setArray(pupArr);
    Qext::File::Json::write(document, QString::fromStdString(this->name()));
}





