#ifndef STD_EXT
#define STD_EXT

#include <iterator>
#include <set>
#include "error.h"

#define SIZEOF(arr) (sizeof((arr)) / sizeof((arr)[0]))
#define SKIP_IF(condition) {if ((condition)) {continue;}}

namespace std_ext {
    // from c++17 standart:
    template <typename T, std::size_t N>
    constexpr std::size_t size(const T (&arr)[N]) noexcept{
        return N;
    }


    template <typename TIter>
    void kill(TIter begin, const TIter & end){
        while (begin != end){
            Debug_exists(*begin);
            delete *begin;
            ++begin;
        }
    }

    template <typename TContainer>
    void kill(TContainer & cont){
        for (auto & ptr : cont){
            Debug_exists(ptr);
            delete ptr;
        }
    }

    template <typename TIter>
    void overkill(TIter begin, const TIter & end){
        if (begin == end) return;
        std::set<decltype(*begin)> set;
        while (begin != end){
            auto occurence = set.find(*begin);
            if (occurence == set.end()){
                Debug_exists(begin);
                delete *begin;
                set.insert(*begin);
            } // else already deleted
        }
    }

    template <typename TContainer>
    void overkill(TContainer & cont){
        overkill(cont.begin(), cont.end());
    }


    template <typename TIter, typename TMember>
    void overkill(TIter begin, const TIter & end, TMember delField){
        if (begin == end) return;
        std::set<decltype(*begin)> set;
        while (begin != end){
            auto occurence = set.find(begin->*delField);
            if (occurence == set.end()){
                Debug_exists(begin->*delField);
                delete begin->*delField;
                set.insert(*begin->*delField);
            } // else already deleted
        }
    }

    template <typename TContainer, typename TMember>
    void overkill(TContainer & cont, TMember delField){
        overkill(cont.begin(), cont.end(), delField);
    }

    const char * toString(bool boolean);

}



#endif // STD_EXT

