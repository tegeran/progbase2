#ifndef PUPILSTORAGE_H
#define PUPILSTORAGE_H
#include <QFile>
#include <vector>
#include <utility>
#include <string>
#include "pupil.h"
#include "message_exception.h"

class PupilStorage{
private:
    std::string _name;
protected:

    QFile * open(const enum QIODevice::OpenModeFlag &mode) const; // throws MessageException()

    PupilStorage(const std::string & name);
    PupilStorage(std::string && name);
public:
    virtual ~PupilStorage() = default;

          std::string &name();
    const std::string &name() const ;

    virtual std::vector<Pupil> load() const = 0;
    virtual void save(const std::vector<Pupil> & entities) const = 0;
};





#endif // PUPILSTORAGE_H
