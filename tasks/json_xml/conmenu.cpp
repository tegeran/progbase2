//
// Created by tegeran on 20.03.18.
//

#include "conmenu.h"
#include "progbase-cpp/console.h"
#include "cui.h"
#include <stdio.h>
#include "error.h"

using namespace std;
using namespace progbase::console;
using namespace Cui;

ConMenu::ConMenu(vector<const char *> & options) : options(options), location(1, 1){
    Debug_minbound(options.size(), 1);
    Debug_exists(options[0]);
    this->actual = 0;
}

void ConMenu::advanceDown(){
    if (actual == options.size() - 1) {
        move(location.y + actual, location.x);
        cout << options[actual];
        move(location.y, location.x);
        cout << reverse << options[actual = 0] << reverse_off;
    }
    else {
        move(location.y + actual, location.x);
        cout << options[actual++];
        move(location.y + actual, location.x);
        cout << reverse << options[actual] << reverse_off;
    }
}

void ConMenu::advanceUp(){
    if (actual == 0) {
        actual = options.size() - 1;
        move(location.y, location.x);
        cout << options[0];
        move(location.y + options.size() - 1, location.x);
        cout << reverse << options[options.size() - 1] << reverse_off;
    }
    else {
        move(location.y + actual, location.x);
        cout << options[actual--];
        move(location.y + actual, location.x);
        cout << reverse << options[actual] << reverse_off;
    }
}

signed long long ConMenu::listen() {
    //
    Cui::hideCursor();
    Cin_ext temp = Cin_ext();   // saved state is restored after temp is destroyed off the stack
    temp.allchars();
    temp.noecho();
    cin >> noskipws;
    do {
        cout << flush;
        char ch;
        cin >> ch;
        switch (ch){
            case 'W':
            case 'w':{
                advanceUp();
                break;
            }
            case 'S':
            case 's':{
                advanceDown();
                break;
            }
            case '\n':
            case ' ':{
                Cui::showCursor();
                cin >> skipws;
                return actual;
            }
            case 27:{
                Cui::showCursor();
                cin >> skipws;
                return -1;
            }
            default:break;
        }
    } while (true);
}

void ConMenu::post(unsigned short y, unsigned short x) {
    Debug_minbound(y, 1);
    Debug_minbound(x, 1);
    //
    location = Location< unsigned short >(y, x);
    refresh();
}

void ConMenu::unpost() {
    cout << reset;
    move(location.y, location.x);
    unsigned short y = location.y;
    for (size_t i = 0; i < options.size(); ++i) {
        const char * ch = options[i];
        Debug_exists(ch);
        while(*ch){
            cout << ' ';
            ++ch;
        }
        move(++y, location.x);
    }
}

void ConMenu::refresh() {
    unsigned short y = location.y;
    move(y, location.x);                 // spliting into two cycles may bring better performance for a bit
    unsigned int i = 0;
    while (i < actual){
        cout << options[i++];
        move(++y, location.x);
    }
    cout << reverse << options[actual] << reverse_off;
    while (++i < options.size()){
        move(++y, location.x);
        cout << options[i];
    }
}
