#include "xml_pupil_storage.h"
#include <QtXml>
#include <pupil.h>
#include <string>
#include <QFile>
#include <QSet>
#include <memory>
#include "std_ext.h"
#include <iterator>
#include "qext.h"

XmlPupilStorage::XmlPupilStorage(const std::string &filePath)
    : PupilStorage(filePath) {}

XmlPupilStorage::XmlPupilStorage(std::string &&filePath)
    : PupilStorage(filePath) {}


class CheckSyntax{
private:
    QSet<std::string *> & bookSet;

public:
    CheckSyntax(QSet<std::string *> & bookSet) : bookSet(bookSet) {}

    int operator()(const bool & expr){
        if (!expr){
            for (std::string * bookMem : bookSet){
                delete bookMem;
            }
            throw MessageException("invalid xml instance syntax");
        }
        return 0;
    }
};


std::vector<Pupil> XmlPupilStorage::load() const{
    QDomDocument document = Qext::File::Xml::read(QString::fromStdString(this->name()));

    QSet<std::string *> bookSet;

    CheckSyntax check_syntax(bookSet);

    QDomElement core = document.documentElement();

    check_syntax(core.tagName() == "Pupils");

    QDomNodeList pupils = core.childNodes();


    // check wether pupils.size() == 0 causes undefined behaviour

    std::vector<Pupil> vect;
    vect.reserve(pupils.size());
    // macroes are provided by "std_ext.h"
    for (int i = 0; i < pupils.size(); ++i){
        QDomNode pupNode = pupils.at(i);
        SKIP_IF(pupNode.isComment());
        check_syntax(pupNode.isElement());

        QDomElement pupEl = pupNode.toElement();
        check_syntax(pupEl.tagName() == "Pupil");

        // CHECKING ATTRS-----------------------------------------------
        const QString attrNames[] = {"name", "_class", "age", "score", "alive"};
        check_syntax(pupEl.attributes().size() == std_ext::size(attrNames));
        for (const auto & attr: attrNames){
            check_syntax(pupEl.hasAttribute(attr));
        }
        // --------------------------------------------------------------

        Pupil pup;

        pup._class = pupEl.attribute("_class").toStdString();
        pup.name   = pupEl.attribute("name").toStdString();
        {
            bool state = true;
            pup.age   = pupEl.attribute ("age").toUShort(&state); check_syntax(state);
            pup.score = pupEl.attribute("score").toFloat(&state); check_syntax(state);
        }

        QString attr = pupEl.attribute("alive");

        pup.alive = (attr == "true")
                ? true
                :   (attr == "false")
                ? false
                : check_syntax(false);

        QDomNodeList pupBooks = pupEl.childNodes();
        for (int i = 0; i < pupBooks.size(); ++i){
            QDomNode bookNode = pupBooks.at(i);
            SKIP_IF(bookNode.isComment());
            check_syntax(bookNode.isElement());

            QDomElement bookEl = bookNode.toElement();

            QDomNodeList bookTitleNodeList = bookEl.childNodes();

            check_syntax(bookTitleNodeList.size() == 1);

            QDomNode titleNode = bookTitleNodeList.at(0);

            check_syntax(bookEl.tagName() == "textbook"
                         && bookEl.attributes().isEmpty()
                         && titleNode.isText());

            std::string title = titleNode.toText().data().toStdString();

            if (!bookSet.contains(&title)){
                std::string * newBook = new std::string(std::move(title));
                pup.textbooks.emplace_back(newBook);
                bookSet << newBook;
            } else {
                pup.textbooks.emplace_back(*(bookSet.find(&title)));
            }

        }
        vect.emplace_back(std::move(pup));
    }

    return vect;
}



void XmlPupilStorage::save(const std::vector<Pupil> &entities) const {
    QDomDocument document;
    QDomElement core = document.createElement("Pupils");
    for (const Pupil& puppy: entities){
        QDomElement pupEl = document.createElement("Pupil");
        pupEl.setAttribute("name", QString::fromStdString(puppy.name));
        pupEl.setAttribute("_class", QString::fromStdString(puppy._class));
        pupEl.setAttribute("age", QString::number(puppy.age));
        pupEl.setAttribute("score", QString::number(puppy.score));
        for (const std::string * const &book: puppy.textbooks){
            QDomElement bookEl = document.createElement("textbook");
            bookEl.appendChild(document.createTextNode(QString::fromStdString(*book)));
            pupEl.appendChild(bookEl);
        }
        pupEl.setAttribute("alive", std_ext::toString(puppy.alive));
        core.appendChild(pupEl);
    }
    document.appendChild(core);
    Qext::File::Xml::write(document, QString::fromStdString(this->name()));
}












