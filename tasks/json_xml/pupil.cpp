#include "pupil.h"
#include <string>
#include <vector>
#include <utility>
#include <QDebug>

Pupil::Pupil(std::string                     name,
             std::string                   _class,
             unsigned short                   age,
             float                          score,
             std::vector<std::string *> textbooks,
             bool                           alive)

    : name     (std::move(name     )),
      _class   (std::move(_class   )),
      age      (std::move(age      )),
      score    (std::move(score    )),
      textbooks(std::move(textbooks)),
      alive    (std::move(alive    )) {

//    qDebug() << "--> CREATED " << this->name.c_str();
}

Pupil::Pupil(const Pupil &toCopy)
    : name     (toCopy.name     ),
      _class   (toCopy._class   ),
      age      (toCopy.age      ),
      score    (toCopy.score    ),
      textbooks(toCopy.textbooks),
      alive    (toCopy.alive    )  {

//    qDebug() << "--> PANIC! COPY_PASTED " << this->name.c_str();
}

Pupil::Pupil(Pupil &&toMove)
    : name     (std::move(toMove.name     )),
      _class   (std::move(toMove._class   )),
      age      (std::move(toMove.age      )),
      score    (std::move(toMove.score    )),
      textbooks(std::move(toMove.textbooks)),
      alive    (std::move(toMove.alive    ))  {

//    qDebug() << "--> MOVED " << this->name.c_str();

    toMove.age = 0;
    toMove.score = 0;
    toMove.alive = false;
}

Pupil & Pupil::operator=(const Pupil &toCopyAssign){
    this->name      = toCopyAssign.name;
    this->_class    = toCopyAssign._class;
    this->age       = toCopyAssign.age;
    this->score     = toCopyAssign.score;
    this->textbooks = toCopyAssign.textbooks;
    this->alive     = toCopyAssign.alive;


//    qDebug() << "--> PANIC! COPY_ASSIGNED " << this->name.c_str();
    return *this;
}

Pupil &Pupil::operator=(Pupil &&toMoveAssign){
    this->name      = std::move(toMoveAssign.name     );
    this->_class    = std::move(toMoveAssign._class   );
    this->age       = std::move(toMoveAssign.age      );
    this->score     = std::move(toMoveAssign.score    );
    this->textbooks = std::move(toMoveAssign.textbooks);
    this->alive     = std::move(toMoveAssign.alive    );

//    qDebug() << "--> MOVE_ASSIGNED " << this->name.c_str();

    toMoveAssign.age = 0;
    toMoveAssign.score = 0;
    toMoveAssign.alive = false;
    return *this;
}


