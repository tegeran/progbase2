#include <QCoreApplication>
#include <pupil.h>
#include <xml_pupil_storage.h>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include "storage_manager.h"
#include <cstdlib>
#include "std_ext.h"
#include <set>
#include "cui.h"


std::ostream & operator<<(std::ostream & stream, const Pupil & pup){

    stream << "name: \t\""   << pup.name   << "\""
           << "\n_class: \"" << pup._class << "\""
           << "\nage: \t"    << pup.age
           << "\nscore: \t"  << pup.score
           << "\nalive: \t"  << std::boolalpha << pup.alive
           << "\nbooks: \t";
    if (pup.textbooks.empty()){
        return stream << "(no textbooks)";
    }
    for (std::string * const & book : pup.textbooks){
        stream << '\"' << *book << "\" ";
    }
    return stream;
}



namespace Pupils{
    void overkillBooks(std::vector<Pupil> & vec){
        if (vec.empty())
            return;

        std::set<std::string *> set;

        for (Pupil & pup : vec){
            for (std::string * & book : pup.textbooks){
                auto res = set.find(book);
                if (res == set.end()){
                    delete book;
                    set.insert(book);
                } // else already deleted
            }
        }
    }

    void overkillBooks(std::vector< std::vector<Pupil> > &loads){
        for (std::vector<Pupil> &pups : loads){
            overkillBooks(pups);
        }
    }

    std::ostream & operator<<(std::ostream & stream, const std::vector<Pupil> & pupils){
        for (const Pupil & pup : pupils){
           stream << pup << "\n~~~~~~~~~~~~~~~~~~~\n";
        }
        return stream;
    }


    void print(const std::vector< std::vector<Pupil> > & loads,
               const std::vector< PupilStorage *> & stors){
        for (size_t i = 0; i < loads.size(); ++i){
            Debug_exists(stors[i]);
            //
            std::cout << Cui::reverse
                      << "From file: \"" << stors[i]->name() << "\":\n"
                      << Cui::reverse_off
                      << loads[i] << '\n';

        }
    }
}

namespace VitahaException {
    static void println(const MessageException & e){
        std::cerr << Cui::fgnd(5,0,0)
                  << "Unresolved segmentation fault (core dumped): "
                  << e.what() << Cui::reset << '\n';
    }

}
int main(){
    std::string books[]{
        "Azbuka",
        "Matanaliz",
        "Khimiya"
    };

    std::vector<Pupil> pupils;
    pupils.reserve(3);

    // initializing with array e.g. std::vector pupils{Pupil(), Pupil()} copies values
    // whereas push or emplace_back moves them

    pupils.emplace_back(Pupil("Taras Nevkusniy", "KP-71", 12, -12,
              {books + 1 , books + 2}));

    pupils.emplace_back(Pupil("Zahar Berkut", "KB-71", 32, 72));

    pupils.emplace_back(Pupil("Jason Kostomarov", "KM-71", 45, -0.23,
              {books, books +1, books + 2}, false));

    std::vector<PupilStorage *> stors(3, nullptr);
    //-------------INIT-----------------------
    try {
        stors[0] = StorageManager::createStorage("data.xml");
        stors[1] = StorageManager::createStorage("data.json");
        stors[2] = StorageManager::createStorage("data.fake");
    }
    catch (const MessageException & e){
        Debug_suppose(stors[2] == nullptr, "Panic");
        (void)stors.pop_back();
        VitahaException::println(e);
    }
    //------------------SAVE-------------------
    try {
        for (PupilStorage * const & stor : stors){
            Debug_exists(stor);
            stor->save(pupils);
        }
    }
    catch (const MessageException & e) {
        VitahaException::println(e);
        std_ext::kill(stors);
        std::cerr << "\n AAAaaa ze systemny fail";
        return EXIT_FAILURE;
    }
    Cui::wait("Lomay menya polnost'yu"); // PAUSE
    std::cout << "\n\n";
    // --------------------LOAD-----------------
    std::vector< std::vector<Pupil> > loads;

    for (auto && it = stors.begin(); it != stors.end(); ){
        Debug_exists(*it);
        try {
            loads.emplace_back(std::move((*it)->load()));
            ++it;
        } catch (const MessageException &e){
            VitahaException::println(e);
            delete *it;
            it = stors.erase(it);
        }
    }

    Pupils::print(loads, stors);

    std_ext::kill(stors);
    Pupils::overkillBooks(loads);

    return EXIT_SUCCESS;
}

