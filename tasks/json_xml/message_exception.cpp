#include "message_exception.h"
#include <string>


MessageException::MessageException(std::string &&error)
    : _error(error) {} // error is already rvalue reference, so std::move() is unnecessary

MessageException::MessageException(const std::string &error)
    : _error(error) {}

const char *MessageException::what() const noexcept{
    return _error.c_str();
}
