#ifndef JSONENTITYSTORAGE_H
#define JSONENTITYSTORAGE_H
#include "pupil_storage.h"
#include "pupil.h"
#include "message_exception.h"
#include <vector>
#include <string>

class JsonPupilStorage : public PupilStorage {
public:
    JsonPupilStorage(const std::string & filePath);
    JsonPupilStorage(std::string && filePath);

    std::vector<Pupil> load() const;
    void save(const std::vector<Pupil> & entities) const;
};

#endif // JSONENTITYSTORAGE_H
