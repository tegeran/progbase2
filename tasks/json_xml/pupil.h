#ifndef PUPIL_H
#define PUPIL_H
#include <string>
#include <vector>

class Pupil{
public:
    std::string                     name;
    std::string                   _class;
    unsigned short                   age;
    float                          score;
    std::vector<std::string *> textbooks;
    bool                           alive;

/*
 * Why no references? Because arguments can be either lvalues or rvalues,
 * so handling all variants demands 2^(fields count) constructors
 * Thats why I preferred cathing all arguments by value (rvalues are not copied
 * due to copy elision compiler optimization (tested)) and moving them to
 * newly created object.
 * (even CLion's intellisence hinted me to do it that way)
 *
 * https://habrahabr.ru/post/322132/
 * https://mpark.github.io/programming/2014/06/07/beware-of-perfect-forwarding-constructors/
 * https://stackoverflow.com/questions/12953127/what-are-copy-elision-and-return-value-optimization
 */

    explicit Pupil(
            std::string            name      = "no name",
            std::string            _class    = "no class",
            unsigned short         age       = 0,
            float                  score     = 0,
            std::vector<std::string *>  textbooks = std::vector<std::string *>(),
            bool                   alive     = true);

    Pupil(const Pupil & toCopy);
    Pupil(Pupil && toMove);
    Pupil & operator=(const Pupil & toCopyAssign);
    Pupil & operator=(Pupil && toMoveAssign);


};

#endif // PUPIL_H
