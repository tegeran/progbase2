QT += core
QT -= gui
QT += xml

CONFIG += c++14

TARGET = json_xml
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    pupil.cpp \
    message_exception.cpp \
    xml_pupil_storage.cpp \
    pupil_storage.cpp \
    json_pupil_storage.cpp \
    storage_manager.cpp \
    conmenu.cpp \
    cui.cpp \
    error.cpp \
    qext.cpp \
    std_ext.cpp

HEADERS += \
    pupil.h \
    message_exception.h \
    storage_manager.h \
    xml_pupil_storage.h \
    pupil_storage.h \
    json_pupil_storage.h \
    std_ext.h \
    error.h \
    conmenu.h \
    cui.h \
    location.h \
    qext.h

