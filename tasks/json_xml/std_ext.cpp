#include "std_ext.h"

namespace std_ext {

    const char * toString(bool boolean){
        return boolean ? "true" : "false";
    }
}
