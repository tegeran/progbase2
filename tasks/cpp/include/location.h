#pragma once
#include <iostream>

template <typename TDistance>
class Location{
public:
    TDistance y;
    TDistance x;

    explicit Location(TDistance y = 0, TDistance x = 0);


    // https://stackoverflow.com/a/44712138/9259330
    template <typename UDistance>
    friend std::ostream & operator <<(std::ostream &stream, Location<UDistance> & self);
};

template <typename TDistance>
Location<TDistance>::Location(TDistance y, TDistance x){
    this->x = x;
    this->y = y;
}

template <typename TDistance>
std::ostream & operator <<(std::ostream & stream, Location<TDistance> & self){
    return stream << '(' << self.y << ", " << self.x << ')';
}