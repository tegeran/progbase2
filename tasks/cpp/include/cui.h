#pragma once

#include <location.h>
//#include <cstdio>
#include <cwchar>
#include <iostream>
#include <termios.h>



namespace Cui {
    class Foreground { // manipulator
    private:
        unsigned char red;
        unsigned char green;
        unsigned char blue;

    public:
        Foreground(unsigned char red, unsigned char green, unsigned char blue);

        friend std::ostream &operator<<(std::ostream & stream, const Foreground &obj);

        friend Foreground fgnd(unsigned char red,
                               unsigned char green,
                               unsigned char blue);
    };
    ///////////////////////////////////////////////////////////////
    class Background { // manipulator
    private:
        unsigned char red;
        unsigned char green;
        unsigned char blue;

    public:
        Background(unsigned char red, unsigned char green, unsigned char blue);

        friend std::ostream &operator<<(std::ostream & stream, const Background &obj);

        friend Background bkgnd(unsigned char red,
                                unsigned char green,
                                unsigned char blue);
    };

    // no arguments ostream manipulators
    std::ostream & reset      (std::ostream & stream);
    std::ostream & reverse    (std::ostream & stream);
    std::ostream & reverse_off(std::ostream & stream);

    std::istream & flush(std::istream & stream);

    // returns cout (advances current caret location)
    std::ostream & move(unsigned short y, unsigned short x);

    void printHLine(unsigned short size, char token);

    void printVLine(unsigned short size, char token, Location<unsigned int> loc);

    Location<unsigned int> getLocation(void);

    std::ostream & hideCursor();
    std::ostream & showCursor();

    void nap();
    void wait(const char * && message = "Press any key to continue");


    // clear console window
    void recover();

    class Cin_ext {
    public:
        Cin_ext();
        ~Cin_ext();
        void allchars();
        void noecho();
        void reset();

    private:
        struct termios vanilla;
    };



    template<typename TEntity>
    void require(std::istream & stream, TEntity &entity) {
        do {
            stream >> entity;
            if (stream.fail()){
                stream >> flush;
//                stream.clear();
                continue;
                //
            } else { return; }

        } while (true);
    }

	template<typename TEntity, typename TFunct>
	void require(std::istream & stream, TEntity &entity, TFunct isValid) {
		do {
			stream >> entity;
			if (stream.fail() || !isValid(entity)){
				stream >> flush;
//                stream.clear();
				continue;
				//
			} else { return; }

		} while (true);
	}

}