#ifndef CIRCLE_H
#define CIRCLE_H

#include <string>
#include <math.h>
#include <istream>
#include "cui.h"

template <typename TDistance, typename TLocation>
class Circle{

public:

    explicit Circle(
           std::string      name     = "",
           const TDistance & radius   = static_cast<TDistance>(0),
           const TLocation & center   = TLocation(0,0)
    );

    // redundant ~Circle();

	std::string    setName  (std::string    & name);
    TDistance setRadius(TDistance & radius);
    TLocation setCenter(TLocation & center);

	std::string    getName  () const;
    TDistance getRadius() const;
    TLocation getCenter() const;

    static bool isRadius(TDistance);

    TDistance circumference();


    // https://stackoverflow.com/a/44712138/9259330
    template <typename UDistance, typename ULocation>
    friend std::ostream & operator <<(std::ostream & stream, Circle<UDistance, ULocation> & self);

	template <typename UDistance, typename ULocation>
	friend std::istream & operator >>(std::istream & stream, Circle<UDistance, ULocation> & self);

private:
	std::string name;
    TDistance   radius;
    TLocation   center;
};

template <typename UDistance, typename ULocation>
std::ostream & operator <<(std::ostream & stream, Circle<UDistance, ULocation> & self){
    return stream <<        '\"' << self.name   		 << "\"\n"
                  << "Radius:  " << self.radius          << '\n'
                  << "Center:  " << self.center          << '\n'
                  << "Circumf: " << self.circumference() << '\n';
}


template <typename UDistance, typename ULocation>
std::istream & operator >>(std::istream & stream, Circle<UDistance, ULocation> & self){
	std::cout << "Name: ";
	Cui::require(std::cin, self.name);
	std::cout << "Radius: ";
	Cui::require(std::cin, self.radius, Circle<UDistance, ULocation>::isRadius);
	std::cout << "Center x: ";
	Cui::require(std::cin, self.center.x);
	std::cout << "Center y: ";
	Cui::require(std::cin, self.center.y);
	return stream;
}






template <typename TDistance, typename TLocation>
Circle<TDistance, TLocation>::Circle(std::string  name, const TDistance & radius, const TLocation & center)
	: name(name), radius(radius), center(center){
    if (!Circle<TDistance, TLocation>::isRadius(radius)) throw MATH_ERREXCEPT;
}

template <typename TDistance, typename TLocation>
TDistance Circle<TDistance, TLocation>::circumference(){
    return 2 * M_PI * this->radius;
}



template <typename TDistance, typename TLocation>
std::string Circle<TDistance, TLocation>::setName(std::string & name){
	std::string prev = this->name;
    this->name = name;
    return prev;
}


template <typename TDistance, typename TLocation>
TDistance Circle<TDistance, TLocation>::setRadius(TDistance & radius){
    if  (!Circle<TDistance, TLocation>::isRadius(radius)) throw MATH_ERREXCEPT;
    TDistance prev = this->radius;
    this->radius = radius;
    return prev;
}

template <typename TDistance, typename TLocation>
TLocation Circle<TDistance, TLocation>::setCenter(TLocation & center){
    TLocation prev = this->center;
    this->center = center;
    return prev;
}

template <typename TDistance, typename TLocation>
std::string Circle<TDistance, TLocation>::getName  () const{
    return this->name;
}

template <typename TDistance, typename TLocation>
TDistance Circle<TDistance, TLocation>::getRadius() const{
    return this->radius;
}

template <typename TDistance, typename TLocation>
TLocation Circle<TDistance, TLocation>::getCenter() const{
    return this->center;
}

template<typename TDistance, typename TLocation>
bool Circle<TDistance, TLocation>::isRadius(TDistance suspect) {
    return suspect >= 0;
}




#endif // CIRCLE_H
