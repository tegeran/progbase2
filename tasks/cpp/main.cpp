#include <iostream>
#include <circle.h>
#include <vector>
#include "location.h"
#include "cui.h"
#include <utility>

#include "conmenu.h"



typedef Circle<long double, Location<long double> > RealCircle;
typedef Location<long double> RealLocation;


template <typename TEntity, typename Funct>
void pshowValid(std::vector<TEntity> haystack, Funct isValid){
	for(TEntity & entity: haystack){
		if (isValid(*entity)){
			std::cout << *entity << '\n';
		}
	}
}


std::ostream & operator<<(std::ostream & stream, std::vector<RealCircle *> vitya){
    unsigned int i = 0;
    for (std::vector<RealCircle *>::iterator it = vitya.begin(); it != vitya.end(); ++it) {
        stream << i++ << ". " << **it << '\n';
    }
    return stream;
}


using namespace Cui;

int main(){
	auto * circles = new std::vector <RealCircle *>;

    auto * options = new std::vector<const char *> (4);

	(*options)[0] = "Show vector";
	(*options)[1] = "Add new item to vector";
	(*options)[2] = "Show only circles with circumference higher than ...";
	(*options)[3] = "Gg wp";

    ConMenu menu = ConMenu(*options);

    recover(); // flush screen (does system("clear"))
	menu.post();
    do {
        switch (menu.listen()){
            case -1:
            case  3:{
                delete options;
                for (RealCircle * circl : *circles){
					delete circl;
				}
				delete circles;
                return EXIT_SUCCESS;
            }
            case 0:{
				recover();
                std::cout << "Total circles " << reverse
						  << '[' << circles->size() << "]\n" << reverse_off
						  << *circles;
                Cui::wait();
				recover();
				break;
            }
			case 1:{
				recover();
				auto * newbie = new RealCircle();
				std::cin >> *newbie >> flush; // std::flush << insertion manipulator conflicts
				circles->push_back(newbie);
				recover();
				break;
			}
			case 2:{
				long double L = 0;
				recover();
				std::cout << "Expecting parameter: ";
				require(std::cin, L); // read form cin untill got adequate value
				std::cout << "Circles having circumference higher than " << reverse
						  << '[' << L << ']' << reverse_off << std::endl;
				std::cin >> flush;

				pshowValid(*circles, [L](RealCircle & entity) -> bool {
					return entity.circumference() > L;
				});

				Cui::wait();
				recover();
				break;
			}
            default:break;
        }
		menu.refresh();
		continue;  // does skipping cycle condition bring better performance ?
    } while (true);

	delete options;
	for (RealCircle * circl : *circles){
		delete circl;
	}
	delete circles;
	return EXIT_SUCCESS;
}

