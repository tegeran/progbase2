#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <progbase/events.h>
#include <progbase/console.h>
#include <progbase/list.h>
#include <assert.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>

typedef DestructorFunction Destructor;
typedef bool (*AnswerChar)(char);
typedef struct __Character{
    char ch;
} Character;

#define imply(presupposition, conclusion) (!(presupposition) || (conclusion))

#ifndef NO_DEBUG
    #define Debug_exists(expression)  assert((expression) && "An attempt to dereference NULL pointer was made");
    #define Debug_UTF8() assert(strcmp(setlocale(0, NULL), "C") && "An attempt to print wide character without UTF-8 format was made")
    #define Debug_inbound(leftBound, suspect, rightBound) assert((suspect) >= (leftBound) && (suspect) <= (rightBound) && "Index out of bounds exception");
    #define Debug_maxbound(suspect, rightBound) assert((suspect) <= (rightBound) && "Index out of bounds exception");
    #define Debug_minbound(suspect, leftBound) assert((suspect) >= (leftBound) && "Index out of bounds exception");
    #define Debug_suppose(expression, errorMessage) assert((expression) && (errorMessage))
    #define Debug_shutdown(reason) assert(0 && (reason))

        #ifndef NO_DEBUG_TIPS
            #define Debug_tip(expression, tipMessage) assert((expression) && (tipMessage))
        #else
            #define Debug_tip(expression, tipMessage)
        #endif
#else
    #define Debug_exists(expression)
    #define Debug_UTF8(...) 
    #define Debug_inbound(leftBound, suspect, rightBound)
    #define Debug_maxbound(suspect, rightBound)
    #define Debug_minbound(suspect, leftBound) 
    #define Debug_suppose(expression, errorMessage)
    #define Debug_shutdown(reason)

    #define Debug_tip(expression, tipMessage)
#endif

void Error_checkHeap(void * heapMemory);

typedef struct __Integer{
    int val;
} Integer;
void Integer_initRandomness(unsigned int seed);
Integer * Integer_new(int val);
Integer * Integer_newRandom(int min, int max);
int Integer_set(Integer * self, int val);
int Integer_at(Integer * self);
void Integer_free(Integer * self);
void Integer_print(Integer * self);
void Integer_printWide(Integer * self);
bool Integer_isNegative(Integer * self);
signed char Integer_compare(Integer * left, Integer * right);

Character * Character_new(char entity);
char Character_at(Character * self);
void Character_free(Character * self);

// -------------------------------------------------------------------------------


enum {
    CharacterSignal,
    UserSignal
};

typedef signed char (*Comparator)(void *, void *);

void List_freesh(List * self);
void redrawTask(void);
void * List_remooveAt(List * self, size_t index);
void printUserList(List * self);
signed int List_linearSearch(List * self, void * pattern, Comparator isTarget);

void userList        (EventHandler * self, Event * event);
void userListener    (EventHandler * self, Event * event);
void keyInputListener(EventHandler * self, Event * event);

int main(int argc, char ** argv){
    Console_hideCursor();
    Console_lockInput();
    

    EventSystem_init();
    
    EventSystem_addHandler(EventHandler_new(NULL, NULL , keyInputListener));
    EventSystem_addHandler(EventHandler_new(NULL, NULL , userListener));
    EventSystem_addHandler(EventHandler_new(List_new(),  (Destructor)List_freesh , userList));

    EventSystem_loop();

    EventSystem_cleanup();

    Console_showCursor();
    Console_unlockInput();

    return EXIT_SUCCESS;
}



void keyInputListener(EventHandler * self, Event * event){
    Debug_exists(self);
    Debug_exists(event);
    //
    switch (event->type){
        case UpdateEventTypeId:{
            if (Console_isKeyDown())
                EventSystem_emit(
                    Event_new(
                        NULL, 
                        CharacterSignal, 
                        Character_new(
                            getchar()
                        ), 
                        (Destructor)Character_free
                    )
                );
            return;
        }
        default: return;
    }
}


void userListener(EventHandler * self, Event * event){
    switch (event->type){
        case CharacterSignal:{
            if (Character_at(event->data) == 27) EventSystem_exit();
            if (Character_at(event->data) < '1' || Character_at(event->data) > '5') return;
            
            else EventSystem_emit(
                Event_new(
                    NULL, 
                    UserSignal, 
                    Integer_new(
                        Character_at(event->data) - '0'
                    ),
                    NULL
                )
            );
        }
    }
}


void userList(EventHandler * self, Event * event){
    Debug_exists(self);
    Debug_exists(event);
    //
    switch(event->type){
        case UserSignal:{
            Integer * etalon = event->data;
            signed int i = List_linearSearch(self->data, etalon, (Comparator)Integer_compare);
            if (i >= 0){
                Integer_free(List_remooveAt(self->data, i));      
                Integer_free(etalon);
            } else {
                List_add(self->data, etalon);
            }
            redrawTask();
            printUserList(self->data);
            return;
        }
        case ExitEventTypeId:{
            while (!List_isEmpty(self->data)){
                Integer_free(List_remooveAt(self->data, 0));
            }
            return;
        }
        case StartEventTypeId:{
            redrawTask();
            printUserList(self->data);
            return;
        }
    }
}















signed int List_linearSearch(List * self, void * pattern, Comparator isTarget){
    Debug_exists(self);
    Debug_exists(isTarget);
    //
    signed int i = 0;
    while (i < List_count(self)){
        if (isTarget(List_get(self, i), pattern) == 0){
            return i;
       }
       ++i;
    }
    return -i - 1;
}

void printUserList(List * self){
    Debug_exists(self);
    //
    if (List_isEmpty(self)){
        printf("No users online");
        return;
    }
    printf ("Users online: \n");
    unsigned int i = 0;
    while (i < List_count(self)){
        printf("user %i\n", Integer_at(List_get(self, i++)));
    }
}


void * List_remooveAt(List * self, size_t index){
    void * entity = List_get(self, index);
    List_removeAt(self, index);
    return entity;
}


void redrawTask(void){
    system("clear");
    printf("На основі модуля системи подій (EventSystem) із бібліотеки libprogbase реалізувати подійно-орієнтовану програму:\n"
           "Створити обробник, що на нажаття кнопок [1-5] генерує події входу/виходу людей із текстового чату.\n"
           "Створити обробника, що позначає чат із учасниками, обробляє події від першого обробника і виводить список учасників чату..\n"
           "Текст варіанту завдання має бути завжди видимий у консолі програми.\n"
           "Весь код рішення описати в одному файлі main.c. Використання глобальних та статичних змінних заборонено.\n\n");
}



// i miss my list
void List_freesh(List * self){
    List_free(&self);
}

// -----------------------------------------------------------------------------------------------









void * Mem_malloc(size_t size){
    void * newbie = malloc(size);
    Error_checkHeap(newbie);
    return newbie;
}


Character * Character_new(char entity){
    Character * newbie = Mem_malloc(sizeof(char));
    newbie->ch = entity;
    return newbie;
}


char Character_at(Character * self){
    Debug_exists(self);
    //
    return self->ch;
}

void Character_free(Character * self){
    Debug_exists(self);
    //
    free (self);
}




































































void Error_checkHeap(void * heapMemory){
    if (heapMemory == NULL){
        perror("Heap overflow was detected.");
        abort();
    }
}

void Integer_initRandomness(unsigned int seed){
    srand(seed);    
}


Integer * Integer_new(int val){
    Integer * newbie = malloc(sizeof(Integer));
    Error_checkHeap(newbie);

    newbie->val = val;
    return newbie;
}


Integer * Integer_newRandom(int min, int max){
    Debug_suppose(min <= max, "Illegal random range bounds");
    // 
    return Integer_new((rand() % (max - min + 1)) + min);
}

void Integer_free(Integer * self){
    Debug_exists(self);
    //
    free(self);
}

void Integer_print(Integer * self){
    Debug_exists(self);
    //
    printf("%i", self->val);
}

void Integer_printWide(Integer * self){
    Debug_UTF8();
    wprintf(L"%i", self->val);
}

int Integer_set(Integer * self, int val){
    Debug_exists(self);
    //
    int keeper = self->val;
    self->val = val;
    return keeper;
}

int Integer_at(Integer * self){
    Debug_exists(self);
    //
    return self->val;
}

bool Integer_isNegative(Integer * self){
    return Integer_at(self) < 0;
}


signed char Integer_compare(Integer * left, Integer * right){
    Debug_exists(left);
    Debug_exists(right);
    //
    if (left->val == right->val) return 0; 
    return left->val > right->val ? -1 : 1;
}
























