#ifndef CI_UNIVER_H
#define CI_UNIVER_H

#include <string>
#include <vector>
#include "csv.h"

class Student {
public:
	explicit Student(std::string name = "Student",
					 std::string group = "AA-11",
					 const double & rating = 0);

	#define STUDENT_FIELDS_AMOUNT 3
	std::string name;  // full name
	std::string group;
	     double rating;

	bool operator == (const Student & other);
	bool compare(const Student & other);
	bool operator <(const Student & other);
};


class StudList{
	// vectors are better than list (c) Bjarne Stroustrup
	// https://www.youtube.com/watch?v=YQs6IC-vgmo
public:
	explicit StudList(std::string id = "no id");

	StudList(StudList && moving);
	StudList(const StudList & coping);

	std::string   id;
	std::vector<Student *> vect;

	static StudList fromCsv(const std::string &csv);
	std::string toCsv();
};

class Teacher{
public:
	explicit Teacher(StudList * const &studs  = nullptr,
					 std::string name         = "No Name",
			         std::string dep          = "Pentagon",
			         std::string labor        = "assistant",
			         const double     &salary = 100500
	);


	StudList * students;
	std::string   name;   // full name
	std::string   dep;    // department (kafedra)
	std::string   labor;  // assistant, teacher, professor ...
		 double   salary;

	void attach(StudList *const &list);
	void detach();

	StudList excludeStudentsOf(const Teacher &bobba);
};






#endif