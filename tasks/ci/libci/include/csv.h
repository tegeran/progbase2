//
// Created by tegeran on 30.03.18.
//

#ifndef CI_CSV_H
#define CI_CSV_H

#include "string"
#include "vector"
#include <stdexcept>

namespace Csv{

	class DeformatException : public std::exception{
	private:
		std::string reason;
	public:
		explicit DeformatException(const std::string &reason);

		const char * what() const noexcept final override;

	};



	class Row : public std::vector<std::string> {
	public:
		Row() = default; // a sweet coverage for std::vector<std::string>
	};

	class Table: public std::vector<Row> {
	public:
		Table();

		explicit Table(const std::string & src);               // throws DeformatException

		std::string toStdString() const noexcept;

		  									  		  // appends new Csv::Rows formed by given src
		void fromStdString(const std::string &src);   // throws DeformatException


		void add(const Row & entity);
		void add(Row && entity);
		void prepend(const Row & entity);
		void remove(const size_t &index);

		Table & operator += (const Row & entity);
		Table & operator += (Row && entity);
		Table & operator += (const Table & toConcatenate);
		Table & operator += (Table && toConcatenate);

		Table & operator <<(const Row & entity);
		Table & operator <<(Row && entity);
		friend Table & operator >>(const Row & entity, Table & self);

	};



}
#endif //CI_CSV_H
