#include "univer.h"
#include "csv.h"
#include <iostream>
#include "error.h"
#include <set>
#include <cmath>
#include <utility>


Student::Student(std::string name, std::string group, const double &rating)
	: name(std::move(name)), group(std::move(group)), rating(rating) {}


void Teacher::attach(StudList *const &list) {
	students = list;
}

StudList Teacher::excludeStudentsOf(const Teacher &bobba) {
	//
	if (!this->students) {
		return StudList();
	}
	if (!bobba.students){
		StudList list = *(this->students);
		list.id += " xor (null)";
		return list;
	}

	StudList list(this->students->id + " xor " + bobba.students->id);
	std::set<Student *> set;
	for (Student * const & studyaga : bobba.students->vect){
		set.insert(studyaga);
	}

	for (Student * const & student : this->students->vect){
		if (set.find(student) == set.end()){
			list.vect.push_back(student);
		}
	}
	return list;
}

Teacher::Teacher(StudList *const &studs,
				 std::string name,
				 std::string dep,
				 std::string labor,
				 const double &salary)
	: students(studs),
	  name(std::move(name)),
	  dep(std::move(dep)),
	  labor(std::move(labor)),
	  salary(salary) {}

void Teacher::detach() {
	this->students = nullptr;
}


std::string StudList::toCsv() {
	Csv::Table table;
	Csv::Row row;
	row.emplace_back(id);
	table << std::move(row);

	for(Student * const &stud: this->vect){
		Debug_exists(stud);
		//
		row = Csv::Row();
		row.push_back(stud->name);
		row.push_back(stud->group);
		row.push_back(std::to_string(stud->rating));
		table << std::move(row);
	}
	return table.toStdString();
}


bool Student::operator==(const Student &other) {
	if (this == &other)
 		return true;
	return this->name.compare(other.name) == 0
			&& this->group.compare(other.group) == 0
			&& std::fabs(this->rating - other.rating) < 1e-6;
}

bool Student::compare(const Student &other) {
	return *this == other;
}

bool Student::operator<(const Student &other) {
	return 	   this->name   < other.name
			&& this->group  < other.group
			&& this->rating < other.rating;
}

static void freeStudents(StudList & list){
	for(Student * & stud: list.vect){
		delete stud;
	}
}

StudList StudList::fromCsv(const std::string &csv){
	Csv::Table table(csv);
	if (table.size() < 1 || table[0].size() != 1) {
		throw Csv::DeformatException("student list invalid csv format");
	}
	StudList list(std::move(table[0][0]));
	for (size_t i = 1; i < table.size(); ++i) {
		if (table[i].size() != STUDENT_FIELDS_AMOUNT){
			goto exception_free;
		}
		try {
			list.vect.emplace_back(
					new Student(
					std::move(table[i][0]),
					std::move(table[i][1]),
					std::stod(table[i][2])
					)
			);
		}
		catch (std::invalid_argument){
			goto exception_free;
		}
		catch (std::out_of_range){
			freeStudents(list);
			throw Csv::DeformatException("student score out of range");
		}
	}

	return list;

	exception_free:
	freeStudents(list);
	throw Csv::DeformatException("student list invalid csv format");
}

StudList::StudList(StudList &&moving)
		: id(std::move(moving.id)), vect(std::move(moving.vect)) {}

StudList::StudList(const StudList &coping)
		: id(coping.id), vect(coping.vect) {}

StudList::StudList(std::string id)
	: id(id), vect() {}

