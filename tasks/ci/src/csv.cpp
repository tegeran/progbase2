//
// Created by tegeran on 30.03.18.
//

#include "csv.h"
#include "error.h"

namespace Csv {

	namespace FSM {

		class Read {
		public:

			Read() = default;

			enum State {
				LAUNCH,
				SHUTDOWN,

				START_STRING,
				READ_STRING,
				READ_ORDINARY,
				QUERY_DQUOTE,
				READ_DQUOTE,

				START_NEW_LINE,
				START_NEW_VALUE,

				ERROR
			};

			State respond(char signal) {
				switch (state) {
					case START_NEW_LINE:
					case START_NEW_VALUE:
					case LAUNCH: {
						switch (signal) {
							case '\n': return state = START_NEW_LINE;
							case '\0': return state = SHUTDOWN;
							case ',' : return state = START_NEW_VALUE;
							case '\"': return state = START_STRING;
							default  : return state = READ_ORDINARY;
						}
					}
					case READ_DQUOTE:
					case READ_STRING:
					case START_STRING: {
						switch (signal) {
							case '\0': return state = ERROR;
							case '\"': return state = QUERY_DQUOTE;
							default  : return state = READ_STRING;
						}
					}
					case READ_ORDINARY: {
						switch (signal) {
							case '\"': return state = ERROR;
							case '\n': return state = START_NEW_LINE;
							case ',' : return state = START_NEW_VALUE;
							case '\0': return state = SHUTDOWN;
							default  : return state = READ_ORDINARY;
						}
					}
					case QUERY_DQUOTE: {
						switch (signal) {
							case '\"': return state = READ_DQUOTE;
							case '\n': return state = START_NEW_LINE;
							case ',' : return state = START_NEW_VALUE;
							case '\0': return state = SHUTDOWN;
							default  : return state = ERROR;
						}
					}
					default: Debug_shutdown("FSM was supposed to be shutdown at this state");
				}

			} // respond()

			bool hasFinished(){
				switch (state){
					case SHUTDOWN:
						return true;
					default:
						return false;
				}
			}

		private:

			State state = LAUNCH;

		};


		// namespace FSM:

		class Write {
		public:

			Write() = default;

			enum State {
				LAUNCH,
				SHUTDOWN,
				SHUTDOWN_TERMINATE_STRING,

				SKIP_STRING,
				SKIP,
				DETECTED_STRING,
				DETECTED_STRING_INSERT_DQUOTE,
				INSERT_DQUOTE
			};

			State respond(const char &signal) {
				switch (state) {
					case SKIP:
					case LAUNCH: {
						switch (signal) {
							case '\"': return state = DETECTED_STRING_INSERT_DQUOTE;
							case '\n':
							case ',' : return state = DETECTED_STRING;
							case '\0': return state = SHUTDOWN;
							default  : return state = SKIP;

						}
					}
					case DETECTED_STRING_INSERT_DQUOTE:
					case INSERT_DQUOTE:
					case DETECTED_STRING:
					case SKIP_STRING: {
						switch(signal){
							case '\"': return state = INSERT_DQUOTE;
							case '\0': return state = SHUTDOWN_TERMINATE_STRING;
							default  : return state = SKIP_STRING;
						}
					}
					default: Debug_shutdown("FSM was supposed to be shutdown at this state");
				}

			} // respond()

			bool hasFinished(){
				switch (state){
					case SHUTDOWN:
					case SHUTDOWN_TERMINATE_STRING:
						return true;
					default:
						return false;
				}
			}

		private:

			State state = LAUNCH;

		};

	} // namespace FSM

		Table::Table(): std::vector<Row>() {}
		Table::Table(const std::string & src){
			fromStdString(src);
		}

		std::string Table::toStdString() const noexcept{
			std::string newbie;

			for (auto it = this->begin(); it != this->end(); ++it){
				for (auto rowIt = it->begin(); rowIt != it->end(); ++rowIt){
					FSM::Write fsm;
					const std::string & str = *rowIt;
					size_t i = 0;
					size_t localStart = newbie.length();
					do {
						switch (fsm.respond(str[i])){
							case FSM::Write::DETECTED_STRING:{
								newbie.insert(localStart, 1, '\"');
								// ...
							}
							case FSM::Write::SKIP:
							case FSM::Write::SKIP_STRING:{
								newbie += str[i];
								break;
							}
							case FSM::Write::DETECTED_STRING_INSERT_DQUOTE:{
								newbie += "\"\"";
								newbie.insert(localStart, 1, '\"');
								break;
							}
							case FSM::Write::INSERT_DQUOTE:
								newbie += "\"\"";
								break;
							case FSM::Write::SHUTDOWN_TERMINATE_STRING:{
								newbie += '\"';
								break;
							}
							default: break;
						}
						++i;
					} while (!fsm.hasFinished());
					newbie += ',';
				}
				newbie[newbie.length() - 1] = '\n';
			}
			newbie.resize(newbie.length() - 1);
			return newbie;
		}

		// appends new Csv::Rows formed by given src
		void Table::fromStdString(const std::string &src) {
			if (src.empty()) return;
			size_t i = 0;
			this->emplace_back(Csv::Row());
			this->at(0).emplace_back(std::string());
			FSM::Read fsm;
			do {
				switch (fsm.respond(src[i])){
					case FSM::Read::READ_STRING:
					case FSM::Read::READ_ORDINARY:
					case FSM::Read::READ_DQUOTE:{
						this->at(this->size() - 1)[this->at(this->size() - 1).size() - 1] += src[i];
						break;
					}
					case FSM::Read::START_NEW_LINE:{
						this->emplace_back(Csv::Row());
						// ...
					}
					case FSM::Read::START_NEW_VALUE:{
						this->at(this->size() - 1).emplace_back(std::string());
						break;
					}
					case FSM::Read::ERROR:{
						throw DeformatException(std::string("invalid comma separated values format"));
					}
					default: break;
				}
				++i;
			} while (!fsm.hasFinished());
		}


		void Table::add(const Row & entity){
			this->push_back(entity);
		}

		void Table::add(Row && entity){
			this->emplace_back(entity);
		}

		void Table::prepend(const Row & entity){
			this->insert(this->begin(), entity);
		}

		void Table::remove(const size_t &index){
			Debug_maxbound(index, this->size() - 1);
			this->remove(index);
		}


		Table & Table::operator += (const Row & entity){
			this->push_back(entity);
			return *this;
		}
		Table & Table::operator += (const Table & toConcatenate){
			for (const Row &eachRow : toConcatenate){
				this->push_back(eachRow);
			}
			return *this;
		}

		Table & Table::operator += (Table && toConcatenate){
			for (const Row &eachRow : toConcatenate){
				this->emplace_back(std::move(eachRow));
			}
			return *this;
		}


		Table & Table::operator <<(const Row & entity){
			return *this += entity;
		}

		Table & operator >>(const Row & entity, Table & self){
			self.prepend(entity);
			return self;
		}

	Table & Table::operator +=(Row &&entity) {
		this->emplace_back(entity);
		return *this;
	}

	Table & Table::operator <<(Row &&entity){
		this->emplace_back(entity);
		return *this;
	}


	const char * DeformatException::what() const noexcept {
			return reason.c_str();
	}

	DeformatException::DeformatException(const std::string &reason)
		: reason(reason) {}


}