#define _BSD_SOURCE
#define _POSIX_SOURCE

#include <cui.h>
#include <zconf.h>
#include "error.h"
#include <limits>

using namespace std;

namespace Cui {

    Foreground::Foreground(unsigned char red, unsigned char green, unsigned char blue) {
        this->red = red;
        this->green = green;
        this->blue = blue;
    }

    std::ostream &operator<<(std::ostream &stream, const Cui::Foreground &object) {
        return stream << "\033[38;5;"
                      << 16 + 36 * object.red + 6 * object.green + object.blue
                      << "m";
    }


    Foreground fgnd(unsigned char red, unsigned char green, unsigned char blue) {
        return Foreground(red, green, blue);
    }




    Background::Background(unsigned char red, unsigned char green, unsigned char blue) {
        this->red = red;
        this->green = green;
        this->blue = blue;
    }

    std::ostream &operator<<(std::ostream &stream, const Cui::Background &object) {
        return stream << "\033[48;5;"
                      << 16 + 36 * object.red + 6 * object.green + object.blue
                      << "m";
    }


    Background bkgnd(unsigned char red, unsigned char green, unsigned char blue) {
        return Background(red, green, blue);
    }



	std::ostream & reset(std::ostream &stream) {
	    return stream << "\033[m";
	}

	std::ostream & reverse(std::ostream &stream){
	    return stream <<"\033[7m";
	}


	std::ostream & reverse_off(std::ostream & stream){
	    return stream <<"\033[27m";
	}


	std::ostream &  move(unsigned short y, unsigned short x){
	    return cout << "\033[" << y << ';' << x << 'H'; // Thanks RA for conPos, really appreciate
	}

	void printHLine(unsigned short size, char token){
	    for (unsigned short i = 0; i < size; ++i){
	        cout << token;
	    }
	}

	void printVLine(unsigned short size, char token, Location<unsigned int> loc){
	    for (unsigned short i = 0; i < size; ++i){
	        cout << token;
	        ++loc.y;
	        Cui::move(loc.y, loc.x);
	    }
	}

	// thanks Ruslan Hadyniak for conGetPos (from C):
	Location<unsigned int> getLocation(void) {
	    Location<unsigned int> pos = Location<unsigned int>(0, 0);
	    struct termios save, raw;
	    fflush(stdout);
	    tcgetattr(0, &save);
	    cfmakeraw(&raw);
	    tcsetattr(0, TCSANOW, &raw);
	    if (isatty(fileno(stdin))) {
	        char cmd[] = "\033[6n";
	        char buf[8] = {0};
	        write(1, cmd, sizeof(cmd));
	        read(0, buf, sizeof(buf));
	        sscanf(buf, "\033[%u;%u", &(pos.y), &(pos.x));
	    }
	    tcsetattr(0, TCSANOW, &save);
	    fflush(stdout);
	    return pos;
	}

	std::ostream & hideCursor(){
	    return cout << "\033[?25l";
	}
	std::ostream & showCursor(){
	    return cout << "\033[?25h";
	}
    Cin_ext::Cin_ext() {
        tcgetattr(fileno(stdin), &this->vanilla);
    }

    Cin_ext::~Cin_ext() {
        reset();
    }

	// adapted from https://stackoverflow.com/a/42775495/9259330
    void Cin_ext::allchars() {
        struct termios t = vanilla;
        t.c_lflag &= (~ICANON & ~ECHO);
        t.c_cc[VTIME] = 0;
        t.c_cc[VMIN] = 1;
        if (tcsetattr(fileno(stdin), TCSANOW, &t) < 0) {
            perror("Unable to set terminal to single character mode");
        }
    }

    void Cin_ext::reset() {
        if (tcsetattr(fileno(stdin), TCSANOW, &vanilla) < 0)
            perror("Unable to restore terminal mode");
    }

    // adapted from RA conLockInput
    void Cin_ext::noecho() {
        struct termios t = vanilla;
        t.c_lflag &= !(ECHO | ICANON | ECHOE | ECHOK | ECHONL | ICRNL);
        t.c_cc[VTIME] = 0;
        t.c_cc[VMIN] = 1;
        if (tcsetattr(STDIN_FILENO, TCSANOW, &t) < 0){
            perror("Unable to set terminal to no echoing mode");
        }
    }

    void nap(){
        Cin_ext termio = Cin_ext();
        termio.noecho();
        termio.allchars();
        Cui::hideCursor();
        cin >> noskipws;
        char ch;
        cin >> ch;
        showCursor();
        cin >> skipws;
    }

	std::istream & flush(std::istream &stream) {
		stream.clear();
		stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		return stream;
	}

	void recover() {
		cout << reset;
		system("clear");
	}

	void wait(const char * && message) {
		cout << reverse << message << reverse_off;
		nap();
	}
}