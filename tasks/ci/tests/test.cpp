
#include <vector>
#include <string>
#include <iostream>
#include <check.h>
#include <algorithm>
#include "univer.h"
#include "std_ext.h"

using namespace std::string_literals;

START_TEST (toCsv_empty_oneIdRow) {
		StudList list("Test");
		ck_assert(list.toCsv() == "Test"s);
		StudList list2("");
		ck_assert(list2.toCsv() == ""s);
	}
END_TEST


START_TEST (toCsv_oneStud_oneIdRowAndStudent){
		StudList list("Group");
		Student stud("Name", "KP-71", 25);
		list.vect.push_back(&stud);
		ck_assert(list.toCsv() == "Group\nName,KP-71,25.000000"s);
	}
END_TEST


START_TEST (toCsv_manyStuds_oneIdRowAndManyStudRows){
		StudList list("GLIST");
		Student s1("Nam1", "A, bGr1", 1.25);
		Student s2("", "", 4.7);
		Student s3("\"Nam3", "G\n\"r1", -12);
		Student s4("NoName", "Yee", 8880);
		list.vect.push_back(&s1);
		list.vect.push_back(&s2);
		list.vect.push_back(&s3);
		list.vect.push_back(&s4);

		ck_assert(list.toCsv() ==
		"GLIST\n"
		"Nam1,\"A, bGr1\",1.250000\n"
		",,4.700000\n"
		"\"\"\"Nam3\",\"G\n\"\"r1\",-12.000000\n"
		"NoName,Yee,8880.000000"s);
	}
END_TEST

#define EXCEPT(toTry) try {toTry; ck_assert(false);} catch(...) {}

START_TEST(fromCsv_incorrectSyntax_exception){

		EXCEPT(StudList::fromCsv("Group\""));
		EXCEPT(StudList::fromCsv("Group\n123,3,\ndasdfsd\""));
		EXCEPT(StudList::fromCsv("\n\"SS\"S\",,"));
		EXCEPT(StudList::fromCsv("\"\"\"\n,\"\" \n"))
		EXCEPT(StudList::fromCsv("List\n NAME, Group, non-numeric"));
	}
END_TEST

START_TEST(fromCsv_empty_exception){
		EXCEPT(StudList::fromCsv(""));
	}
END_TEST

START_TEST(fromCsv_oneIdRow_emptyListWithId){
		StudList list = StudList::fromCsv("Only group name");
		ck_assert(list.vect.empty());
		ck_assert(list.id == "Only group name");

	}
END_TEST

START_TEST(fromCsv_oneIdRowAndStudRow_oneStudList){
		StudList list = StudList::fromCsv("ListName\nStudyaga, KP-71, 89");
		ck_assert(list.vect.size() == 1);
		ck_assert(list.id == "ListName");
		ck_assert(*(list.vect[0]) == Student("Studyaga", " KP-71", 89));
		std_ext::kill(list.vect);
	}
END_TEST

START_TEST(fromCsv_noIdRowAndStudents_exception){
		EXCEPT(StudList::fromCsv("Student, Group, 23"));

	}
END_TEST

START_TEST(fromCsv_twoIdRows_exception){
		EXCEPT(StudList::fromCsv("ListName\nListName2\nStudent,Stud,23"));
	}
END_TEST

Suite *test_suite();

START_TEST(fromCsv_oneIdRowAndManyStudents_correctStudList){
		StudList list = StudList::fromCsv(
				        "ListName\n"
						"Stepan,KA-51, 24\n"
						"Vova Vovochkin,OP-2, -8\n"
						"Putin,Navalny, 89"
		);
		ck_assert(list.id == "ListName");
		ck_assert(list.vect.size() == 3);
		ck_assert(*(list.vect[0]) == Student("Stepan", "KA-51", 24));
		ck_assert(*(list.vect[1]) == Student("Vova Vovochkin", "OP-2", -8));
		ck_assert(*(list.vect[2]) == Student("Putin", "Navalny", 89));
		std_ext::kill(list.vect);
	}
END_TEST


START_TEST(excludeStudentsOf_nullLists_emptyList){
		Teacher t;
		Teacher t2;
		ck_assert(t.excludeStudentsOf(t2).vect.empty());

		StudList sl;
		t.attach(&sl);
		ck_assert(t.excludeStudentsOf(t2).vect.empty());

		t.detach();
		t2.attach(&sl);
		ck_assert(t.excludeStudentsOf(t2).vect.empty());
	}
END_TEST

START_TEST(excludeStudentsOf_thisWithEmptyList_emptyList){
		StudList sl;
		StudList sl2;
		Student s;
		sl2.vect.emplace_back(&s);

		Teacher t(&sl);
		Teacher t2(&sl2);

		ck_assert(t.excludeStudentsOf(t2).vect.empty());

		t2.students->vect.pop_back();
		sl.vect.emplace_back(&s);

		ck_assert(t.excludeStudentsOf(t2).vect.size() == 1);
	}
END_TEST

START_TEST(excludeStudentsOf_secondHasNoCommon_thisList){
		StudList sl;
		StudList sl1;
		Student s ("Vanya");
		Student s1("Petya");
		Student s2("Masha");
		Student s3("Ruslan");
		Student s4("Jack");

		sl.vect = {&s, &s1, &s2};
		sl1.vect = {&s3, &s4};

		Teacher t(&sl);
		Teacher t1(&sl1);

		StudList res = t.excludeStudentsOf(t1);

		ck_assert_int_eq(res.vect.size(), 3);
		ck_assert(*(res.vect[0]) == Student("Vanya"));
		ck_assert(*(res.vect[1]) == Student("Petya"));
		ck_assert(*(res.vect[2]) == Student("Masha"));
	}
END_TEST

START_TEST(excludeStudentsOf_thisWithOneCommon_emptyList){
		StudList sl;
		StudList sl1;
		Student s ("Vanya");
		Student s1("Petya");
		Student s2("Masha");
		Student s3("Ruslan");
		Student s4("Jack");

		sl.vect = {&s, &s1, &s2};
		sl1.vect = {&s3, &s2, &s4};

		Teacher t(&sl);
		Teacher t1(&sl1);

		StudList res = t.excludeStudentsOf(t1);

		ck_assert_int_eq(res.vect.size(), 2);
		ck_assert(*(res.vect[0]) == Student("Vanya"));
		ck_assert(*(res.vect[1]) == Student("Petya"));
	}
END_TEST

START_TEST(excludeStudentsOf_thisWithMultipleCommon_xorList){
		StudList sl;
		StudList sl1;
		Student s ("Vanya");
		Student s1("Petya");
		Student s2("Masha");
		Student s3("Ruslan");
		Student s4("Jack");
		Student s5("Kedr");
		Student s6("Kedr");
		Student s7("Tupo");
		Student s8("Cpp");
		Student s9("Super");

		sl.vect = {&s, &s1, &s2, &s5, &s6, &s7, &s9};
		sl1.vect = {&s3, &s2, &s4, &s7, &s5, &s8};

		Teacher t(&sl);
		Teacher t1(&sl1);

		StudList res = t.excludeStudentsOf(t1);

		ck_assert_int_eq(res.vect.size(), 4);
		ck_assert(*(res.vect[0]) == Student("Vanya"));
		ck_assert(*(res.vect[1]) == Student("Petya"));
		ck_assert(*(res.vect[2]) == Student("Kedr"));
		ck_assert(*(res.vect[3]) == Student("Super"));

	}
END_TEST

int main() {
	Suite *s = test_suite();
	SRunner *sr = srunner_create(s);
	 srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!

	srunner_run_all(sr, CK_VERBOSE); // CK_NORMAL

	int num_tests_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return num_tests_failed;
}

Suite *test_suite() {
	Suite *s = suite_create("Module");
	TCase *tc_sample;

	tc_sample = tcase_create("CI_TEST");
	// PARSER
	tcase_add_test(tc_sample, toCsv_empty_oneIdRow);
	tcase_add_test(tc_sample, toCsv_oneStud_oneIdRowAndStudent);
	tcase_add_test(tc_sample, toCsv_manyStuds_oneIdRowAndManyStudRows);
	tcase_add_test(tc_sample, fromCsv_incorrectSyntax_exception);
	tcase_add_test(tc_sample, fromCsv_empty_exception);
	tcase_add_test(tc_sample, fromCsv_oneIdRow_emptyListWithId);
	tcase_add_test(tc_sample, fromCsv_oneIdRowAndStudRow_oneStudList);
	tcase_add_test(tc_sample, fromCsv_noIdRowAndStudents_exception);
	tcase_add_test(tc_sample, fromCsv_twoIdRows_exception);
	tcase_add_test(tc_sample, fromCsv_oneIdRowAndManyStudents_correctStudList);
	tcase_add_test(tc_sample, excludeStudentsOf_nullLists_emptyList);
	tcase_add_test(tc_sample, excludeStudentsOf_thisWithEmptyList_emptyList);
	tcase_add_test(tc_sample, excludeStudentsOf_secondHasNoCommon_thisList);
	tcase_add_test(tc_sample, excludeStudentsOf_thisWithOneCommon_emptyList);
	tcase_add_test(tc_sample, excludeStudentsOf_thisWithMultipleCommon_xorList);

	suite_add_tcase(s, tc_sample);

	return s;
}