cmake_minimum_required(VERSION 3.9)


#LIBRARY--------------------
project(univer)
include_directories(include)
file(GLOB SOURCES "src/*.cpp")

SET(CMAKE_CXX_FLAGS "-g -std=c++17 -Wall -pedantic-errors -Werror -Wno-unused ")

add_library(${PROJECT_NAME} STATIC ${SOURCES})

#TESTS----------------------
project (test.out)

include_directories(include)
file(GLOB TESTS_EXECUTABLE "tests/test.cpp")

add_executable(${CMAKE_PROJECT_NAME} ${TESTS_EXECUTABLE} ${SOURCES})
target_link_libraries(${CMAKE_PROJECT_NAME} m progbase check subunit rt pthread)

#-----------------------------------------------------------------
#
# Create make test
#
#-----------------------------------------------------------------
enable_testing()
add_test(unit-tests ${CMAKE_PROJECT_NAME})

## Coverage
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/scripts/cmake)
include(CodeCoverage)
setup_target_for_coverage(${CMAKE_PROJECT_NAME}_coverage ${CMAKE_PROJECT_NAME} coverage)
SET(CMAKE_CXX_FLAGS "-g -O0 -fprofile-arcs -ftest-coverage")
SET(CMAKE_CXX_FLAGS "-std=c++17 -g -O0 -fprofile-arcs -ftest-coverage")