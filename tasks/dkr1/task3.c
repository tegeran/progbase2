#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <tree.h>
#include <tree_ext.h>
#include <trie.h>
#include <wchar.h>
#include <floatingpoint.h>
#include <mem.h>
#include <list.h>
#include <lexer.h>
#include <list_ext.h>
#include <token.h>
#include <ctype.h>
#include <sstring.h>
#include <hash_table.h>
#include <assert.h>
#include <dict.h>
#include <integer.h>
#include <parser.h>
#include <interpreter.h>

bool isWordLetter (char ch){
    return isalnum(ch);
}

int main(void) {
    system("clear");
    setlocale(LC_CTYPE, "");
    String * str = String_new();
    String_readFromPath("../main.cala", str);
    DynamicType * status = Calamity_execute(str);
    if (status){
        setlocale(LC_CTYPE, "");
        fputs(">> Program exited with status: \n", stdout);
        DynamicType_writeTo(stdout, status);
        DynamicType_free(status);
        putwchar('\n');
    }
    String_free(str);
    return EXIT_SUCCESS;
}