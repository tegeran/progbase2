#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <cui.h>

void Error_checkHeap(void * heapMemory){
    if (heapMemory == NULL){
        perror("Heap overflow was detected.");
        abort();
    }
}


static  unsigned short  _____stackLevel = 0;

unsigned short _____getLevel(void){
    return _____stackLevel;
}

void _____stackPushFunc(const char * func){
    ++_____stackLevel;
    Cui_printHLine(_____stackLevel, LEVEL_TOKEN);   
    fputs("[", stdout);
    fputs(func, stdout);
    fputs("]", stdout);
}
void _____stackPopFunc(const char ** func){
    //putchar('\n');
    --_____stackLevel;
    Cui_printHLine(_____stackLevel, LEVEL_TOKEN);
    fputs("<", stdout);
    fputs(*func, stdout);
    fputs("<<", stdout);
    puts(ESCAPING_STR);
}