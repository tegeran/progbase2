#include <wide_character.h>
#include <error.h>
#include <mem.h>
#include <wchar.h>
#include <wctype.h>

WideCharacter * WideCharacter_new(wchar_t ch){
    WideCharacter * newbie = Mem_malloc(sizeof(WideCharacter));
    //
    newbie->ch = ch;
    return newbie;
}

void WideCharacter_free(WideCharacter * self){
    Debug_exists(self);
    //
    free(self);
}

wchar_t WideCharacter_at(WideCharacter * self){
    Debug_exists(self);
    //
    return self->ch;
}

wchar_t WideCharacter_set(WideCharacter * self, wchar_t ch){
    Debug_exists(self);
    //
    wchar_t prev = self->ch;
    self->ch = ch;
    return prev;
}

void WideCharacter_print(WideCharacter * self){
    Debug_exists(self);
    //
    putwchar(self->ch);
}

signed char WideCharacter_compare(WideCharacter * left, WideCharacter * right){
    Debug_exists(left);
    Debug_exists(right);
    //
    if (left->ch > right->ch) return -1;
    else return right->ch > left->ch;
}

void WideCharacter_writeTo(FILE * file, WideCharacter * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    fputwc(self->ch, file);
}

bool WideCharacter_isVowel(WideCharacter * self){
    Debug_exists(self);
    //
    return WChar_isVowel(self->ch);    
}

bool WChar_isVowel(wchar_t self){
    switch (self){
        case 'A':
        case 'a':
        case 'E':
        case 'e':
        case 'I':
        case 'i':
        case 'O':
        case 'o':
        case 'U':
        case 'u':
        case 'Y':
        case 'y':
        case L'Я':
        case L'я':
        case L'Ю':
        case L'ю':
        case L'Э':
        case L'э':
        case L'Е':
        case L'е':
        case L'Ы':
        case L'ы':
        case L'И':
        case L'и':
        case L'О':
        case L'о':
        case L'Ї':
        case L'ї':
        case L'І':
        case L'і':
        case L'У':
        case L'у':
        case L'Є':
        case L'є':
            return true;
        default:
            return false;
    } 
}

bool WideCharacter_isConsonant(WideCharacter * self){
    Debug_exists(self);
    //
    return WChar_isConsonant(self->ch);
}

bool WChar_isConsonant(wchar_t self){
    return iswalpha(self) && !WChar_isVowel(self);
}