#include <slnode.h>
#include <assert.h>
#include <stdlib.h>

#include <error.h>

void SLNode_freeWhole(SLNode * self, Destructor valDestructor){
    Debug_exists(self);
    Debug_exists(valDestructor);
    //
    (*valDestructor)(self->val);
    free(self);
}

// Construcor and destructor
// AHTUNG PROGRAMMIERER! Constructor aborts workflow if experineced heap overflow
SLNode * SLNode_new(void * val){
    SLNode * newbie = malloc(sizeof(SLNode));
    Error_checkHeap(newbie);
    newbie->next = NULL;
    newbie->val = val;
    return newbie;
}

void SLNode_free(SLNode * self){
    Debug_exists(self);
    //
    free(self);
}


// Getters
void * SLNode_at(SLNode * self){
    Debug_exists(self);
    //
    return self->val;
}

SLNode * SLNode_getNext(SLNode * self){
    Debug_exists(self);
    //
    return self->next;
}

// Setters
void * SLNode_set(SLNode * self, void * newVal){
    Debug_exists(self);
    //
    void * prev = self->val;
    self->val = newVal;
    return prev;
}

SLNode * SLNode_setNext(SLNode * self, SLNode * next){
    Debug_exists(self);
    //
    void * prev = self->next;
    self->next = next;
    return prev;
}


void * SLNode_removeNext(SLNode * self){
    Debug_exists(self);
    Debug_exists(self->next);
    //
    SLNode * trash = self->next;
    self->next = trash->next;
    void * surviver = trash->val;
    free(trash);
    return surviver;
}

SLNode * SLNode_popNext(SLNode * self){
    Debug_exists(self);
    Debug_exists(self->next);
    //
    SLNode * soldier = self->next;
    self->next = soldier->next;
    soldier->next = NULL;
    return soldier;
}

void SLNode_addAfter(SLNode * self, SLNode * newbie){
    Debug_exists(self);
    Debug_exists(newbie);
    //
    newbie->next = self->next;
    self->next = newbie;
}