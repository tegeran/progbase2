#include <list_ext.h>
#include <error.h>
#include <wchar.h>

List * List_newArrayCopy(void ** array, size_t size){
    Debug_exists(array);
    //
    List * newbie = List_new();
    for (size_t i = 0; i < size; ++i){
        List_addLast(newbie, array[i]);
    }
    return newbie;
}

List * List_newArrayCopyOfRange(void ** array, size_t from, size_t to){
    Debug_exists(array);
    Debug_maxbound(from, to);
    //
    List * newbie = List_new();
    while (from <= to){
        List_addLast(newbie, array[from++]);
    }
    return newbie;
}

List * List_newListCopy(List * source){
    Debug_exists(source);
    //
    List * newbie = List_new();
    for (size_t i = List_size(source) - 1; i != 0; --i){
        List_addFirst(newbie, List_at(source, i));
    }
    return newbie;
}

List * List_newListCopyOfRange(List * source, size_t from, size_t to){
    Debug_exists(source);
    Debug_inbound(from, to, List_size(source) - 1);
    //                           
    List * newbie = List_new();
    while(from <= to){
        List_addLast(newbie, List_at(source, from++));
    }
    return newbie;
}

List * List_newUnited(List ** srcLists, size_t size){
    Debug_exists(srcLists);
    //
    List * newbie = List_new();
    for (size_t i = 0; i < size; ++i){
        Debug_exists(srcLists);
        //
        List_listAppend(newbie, srcLists[i]);
    }
    return newbie;
}

void List_arrayInsert(List * self, size_t listIndex, void ** array, size_t size){
    Debug_exists(self);
    Debug_exists(array);
    Debug_maxbound(listIndex, List_size(self));;
    //
    for (size_t i = 0; i < size; ++i, ++listIndex){
        List_insert(self, listIndex, array[i]);
    }
}

void List_arrayAppend(List * self, void ** array, size_t size){
    Debug_exists(self);
    Debug_exists(array);
    //
    for(size_t i = 0; i < size; ++i){
        List_addLast(self, array[i]);
    }
}

void List_listAppend(List * self, List * source){
    Debug_exists(self);
    Debug_exists(source);
    //
    for (size_t i = 0; i < List_size(source); ++i){
        List_addLast(self, List_at(source, i));
    }
}

void List_listInsert(List * self, size_t selfIndex, List * source){
    Debug_exists(self);
    Debug_exists(source);
    Debug_maxbound(selfIndex, List_size(self));
    //
    for (size_t i = 0; i < List_size(source); ++i, ++selfIndex){
        List_insert(self, selfIndex, List_at(source, i));
    }
}


void List_merge(List * self, List * source, Comparator precedence){
    Debug_exists(self);
    Debug_exists(source);
    Debug_exists(precedence);
    //
    size_t i = 0;
    size_t j = 0;
    while (i < List_size(self) && j < List_size(source)){
        if (precedence(List_at(self, i), List_at(source, j)) != 1){
            List_insert(self, i, List_at(source, j));
            ++j;
            ++i;
        }
        ++i;
    }
    if (i == List_size(self)){
        while (j != List_size(source)){
            List_addLast(self, List_at(source, j++));
        }
    }
}


signed long long List_linearSearch(List * self, void * key, Comparator compare){
    Debug_exists(self);
    Debug_exists(compare);
    //
    for (size_t i = 0; i < List_size(self); ++i){
        if (compare(key, List_at(self, i)) == 0){
            return i;
        }
    }
    return -1L;
}

void List_print(List * self, Printer print){
    Debug_exists(self);
    Debug_exists(print);
    //
    if (List_isEmpty(self)){
        return;
    }
    size_t i = 0;
    while (i < List_size(self) - 1){
        print(List_at(self, i++));
        putchar(' ');
    }
    print(List_at(self, i));
}

void List_printWide(List * self, Printer print){
    Debug_exists(self);
    Debug_exists(print);
    Debug_UTF8();
    //
    if (List_isEmpty(self)){
        return;
    }
    size_t i = 0;
    while(i < List_size(self) - 1){
        print(List_at(self, i++));
        putwchar(L' ');
    }
    print(List_at(self, i));
}

void List_writeTo(FILE * file, List * self, Printer print){
    Debug_exists(self);
    Debug_exists(file);
    Debug_exists(print);
    //
    if (List_isEmpty(self)){
        fwprintf(file, L"[ ]");
        return;
    }
    fwprintf(file, L"[ ");
    size_t i = 0;
    while(i < List_size(self) - 1){
        print(List_at(self, i));
        fwprintf(file, L", ");
        ++i;
    }
    print(List_at(self, i));
    fwprintf(file, L" ]");
}

size_t List_addToSorted(List * self, void * entity, Comparator compare){
    Debug_exists(self);
    Debug_exists(compare);
    //
    for (size_t i = 0; i < List_size(self); ++i){
        if (compare(List_at(self, i), entity) != 1){
            List_insert(self, i, entity);
            return i;
        }
    }
    List_addLast(self, entity);
    return List_size(self) - 1;
}

