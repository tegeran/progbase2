#include <floatingpoint.h>
#include <error.h>
#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>
#include <mem.h>

Float * Float_new(float f){
    Float * newbie = Mem_malloc(sizeof(Float));
    newbie->f = f;
    return newbie;
}

void Float_free(Float * self){
    Debug_exists(self);
    //
    free(self);
}

float Float_at(Float * self){
    Debug_exists(self);
    //
    return self->f;
}

float Float_set(Float * self, float f){
    Debug_exists(self);
    //
    float prev = self->f;
    self->f = f;
    return prev;
}

void Float_print(Float * self){
    Debug_exists(self);
    //
    printf("%f", self->f);
}

void Float_printWide(Float * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    wprintf(L"%f", self->f);
}

static float absolute(float f){return (f > 0) ? f : -f;}

signed char Float_compare(Float * self, Float * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    if (absolute(self->f - other->f) < FLOAT_PRECISION)
        return 0;
    else return (self->f < other->f) ? 1 : -1;
}

void Float_writeTo(FILE * file, Float * self){
    Debug_exists(file);
    Debug_exists(self);
    //
    fprintf(file, "%f", self->f);
}

signed char float_compare(float self, float other){
    if (absolute(self - other) < FLOAT_PRECISION)
        return 0;
    else return (self < other) ? -1 : 1;
}