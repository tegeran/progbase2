#include <list.h>
#include <dlnode.h>
#include <mem.h>

#define NO_DEBUG_TIPS

#include <error.h>

typedef struct __List {
    DLNode * dummy;
    DLNode * cur;
    size_t index; // location
    size_t size;
} List;

static size_t distance(size_t bibba, size_t bobba);
static unsigned char min(size_t a, size_t b, size_t c);
static DLNode * moveForward(DLNode * cur, size_t start, size_t finish);
static DLNode * moveBackward(DLNode * cur, size_t start, size_t finish);
static void   move(List * self, size_t index);
static bool   hasMore(const List * self);
static void * removeCur(List * self);
static void * removeNode(DLNode * node);
static void   moveFirst(List * self);
static void   moveLast(List * self);
static void   moveNext(List * self);
static void   movePrev(List * self);
static void * peek(const List * self);
static void * set(List * self, void * val);
static void addNext(DLNode * src, DLNode * newbie);
static void addPrev(DLNode * src, DLNode * newbie);
static void refreshCur(List * self);

// Constructors and destructors
// AHTUNG PROGRAMMIERER! Constructors abort workflow if experienced heap overflow

List * List_new(void){
    List * newbie = malloc(sizeof(List));

    Error_checkHeap(newbie);
    
    newbie->dummy = DLNode_new(NULL);
    newbie->cur = newbie->dummy;

    DLNode_setNext(newbie->dummy, newbie->dummy);
    DLNode_setPrev(newbie->dummy, newbie->dummy);

    newbie->size = 0;
    newbie->index = 0;

    return newbie;
}


List * List_newCap(size_t size, void * init){
    List * newbie = List_new();

    while(size != 0){
        List_addLast(newbie, init);
        --size;
    }
    return newbie;
}

void List_free(List * self){
    Debug_exists(self);
    //
    if (!List_isEmpty(self)){
        moveFirst(self);
        do {
            (void)removeCur(self);
        } while (hasMore(self));
    }
    
    DLNode_free(self->dummy);    
    free(self);
}


void List_freeWhole(List * self, Destructor killer){
    Debug_exists(self);
    Debug_exists(killer);
    //
    if (!List_isEmpty(self)){
        moveFirst(self);
        do {
            killer(removeCur(self));
        } while (hasMore(self));
    }
    DLNode_free(self->dummy);
    free(self);
}


void List_removeBy(List * self, Answer caseRemove, Destructor killer){
    Debug_exists(self);
    Debug_exists(caseRemove);
    Debug_exists(killer);
    //
    if (List_isEmpty(self)) return;
    
    moveFirst(self);
    while(hasMore(self)){
        if ((*caseRemove)(DLNode_at(self->cur))){
            (*killer)(removeCur(self));
        } else {
            moveNext(self);
        }
    }
}

// NOTE: first node has index 0
void * List_at(const List * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->size - 1);
    Debug_suppose(!List_isEmpty(self), "An attempt to get value from empty list was made");
    //
    move((List *)self, index);
    return peek(self);
}

void * List_set(List * self, size_t index, void * value){
    Debug_exists(self);
    Debug_maxbound(index, self->size - 1);
    Debug_suppose(!List_isEmpty(self), "An attempt to set value for empty list was made");
    //
    move(self, index);
    return set(self, value);
}

void * List_removeAt(List * self, size_t index){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "An attempt to remove node from empty list was made");
    Debug_maxbound(index, self->size -1);
    //
    move(self, index);
    return removeCur(self);
}

void * List_removeFirst(List * self){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "An attempt to remove node from empty list was made");
    //
    void * trash = self->dummy->next->val;
    DLNode_free(DLNode_ncRemove(self->dummy->next));
    --self->size;
    self->cur = self->dummy->next;
    self->index = 0;
    return trash;
}
void * List_removeLast(List * self){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "An attempt to remove node from empty list was made");
    //
    void * trash = self->dummy->prev->val;
    DLNode_free(DLNode_ncRemove(self->dummy->prev));
    --self->size;
    self->cur = self->dummy->next;
    self->index = 0;
    return trash;
}

void List_insert(List * self, size_t index, void * value){
    Debug_exists(self);
    Debug_maxbound(index, self->size);
    //
    if (index == self->size) {
        List_addLast(self, value);
        return;
    }
    move(self, index);
    addPrev(self->cur, DLNode_new(value));
    ++self->size;
    ++self->index;
}

size_t List_size(const List * self){
   Debug_exists(self);
   //
   return self->size; 
}

// adds a new object to the end of list (time complexity O(1))
void List_addLast(List * self, void * value){
    Debug_exists(self);
    //
    addPrev(self->dummy, DLNode_new(value));
    ++self->size;
    refreshCur(self);
}

void List_addFirst(List * self, void * value){
    Debug_exists(self);
    //
    addNext(self->dummy, DLNode_new(value));
    ++self->size;
    ++self->index;
    refreshCur(self);
}

bool List_isEmpty(const List * self){
    Debug_exists(self);
    //
    return DLNode_getNext(self->dummy) == self->dummy;
}

void List_clear(List * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    //
    if (List_isEmpty(self)) return;
    moveFirst(self);
    while (hasMore(self)){
        destroy(removeCur(self));
    }
}


typedef struct __Enumerator {
    const List * list;
    DLNode * cur;
} Enumerator;

void Enumerator_free(Enumerator * self){
    Debug_exists(self);
    //
    free(self);
}

void * Enumerator_current(Enumerator * self){
    Debug_exists(self);
    Debug_suppose(self->cur != self->list->dummy, "Enumerator out of bounds exception");
    //
    return DLNode_at(self->cur);
}

bool Enumerator_moveNext(Enumerator * self){
    Debug_exists(self);
    Debug_suppose(self->cur != self->list->dummy, "Enumerator out of bounds exception");
    //
    self->cur = self->cur->next;
    return self->cur != self->list->dummy;
}

bool Enumerator_isInbound(Enumerator * self){
    Debug_exists(self);
    //
    return self->cur != self->list->dummy;
}


void Enumerator_reset(Enumerator * self){
    Debug_exists(self);
    //
    self->cur = self->list->dummy;
}

Enumerator * List_getNewEnumerator(const List * self){
    Debug_exists(self);
    //
    Enumerator * newbie = Mem_malloc(sizeof(Enumerator));
    *newbie = (Enumerator){
        .list = self,
        .cur = self->dummy
    };
    return newbie;
}


void * List_remove(List * self, void * pattern, Comparator caseRemove){
    Debug_exists(self);
    Debug_exists(caseRemove);
    size_t i = 0;
    while (caseRemove(pattern, List_at(self, i)) != 0){
        ++i;
    }
    Debug_suppose(i < List_size(self) - 1, "An attemt to remove non-existing element from list was made"); 
    return List_removeAt(self, i);
}




void List_cut(List * self, size_t left, size_t right, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    Debug_suppose(self->size != 0, "An attempt to cut an empty list was made");
    Debug_maxbound(left, right);
    Debug_maxbound(right, self->size - 1);
    while (left != 0){
        destroy(List_removeAt(self, 0));
        --left;
        --right;
    }
    while (self->size != right + 1){
        destroy(List_removeLast(self));
    }
}









static size_t distance(size_t bibba, size_t bobba) {
    return (bibba >  bobba) ? bibba - bobba : bobba - bibba;
}

static unsigned char min(size_t a, size_t b, size_t c){
    return (a < b) ? 
          ((a < c) ? 1 : 3):
          ((b < c) ? 2 : 3);
}

static DLNode * moveForward(DLNode * cur, size_t start, size_t finish){
    Debug_suppose(start <= finish, "Invalid forward move bounds");
    //
    while (start != finish){
        Debug_exists(cur);
        //
        cur = DLNode_getNext(cur);
        ++start;
    }
    return cur;
}

static DLNode * moveBackward(DLNode * cur, size_t start, size_t finish){
    Debug_suppose(start >= finish, "Invalid backward move bounds");
    //
    while (start != finish){
        Debug_exists(cur);
        //
        cur = DLNode_getPrev(cur);
        --start;
    }
    return cur;
}

static void move(List * self, size_t index){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "An attmept to perform a move in empty list was made");
    Debug_suppose(index < self->size, "Invaid move index");
    //
    switch(min(distance(self->index, index), self->size - index, index)){
        case 1: {
            if (self->index < index)
                self->cur = moveForward(self->cur, self->index, index);
            else
                self->cur = moveBackward(self->cur, self->index, index);
            break;
        }
        case 2:{
            self->cur = moveBackward(DLNode_getPrev(self->dummy), self->size - 1, index);
            break;
        }
        case 3:{
            self->cur = moveForward(DLNode_getNext(self->dummy), 0, index);
            break;
        }
    }
    self->index = index;
}

static bool hasMore(const List * self){
    Debug_exists(self);
    //
    return self->cur != self->dummy;
}

static void * removeCur(List * self){
    Debug_exists(self);
    Debug_suppose(hasMore(self), "An attempt to remove dummy was made");
    //
    moveNext(self);
    --self->size;
    --self->index;
    return removeNode(DLNode_getPrev(self->cur));
}

static void * removeNode(DLNode * node){
    Debug_exists(node);
    Debug_suppose(DLNode_getNext(node) != NULL, "No next node was found");
    Debug_suppose(DLNode_getPrev(node) != NULL, "No prev node was found");
    //
    void * val = DLNode_at(node);
    DLNode_setNext(DLNode_getPrev(node), DLNode_getNext(node));
    DLNode_setPrev(DLNode_getNext(node), DLNode_getPrev(node));
    DLNode_free(node);
    return val;
}

static void moveFirst(List * self){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "Move in empty list");
    //
    self->index = 0;
    self->cur = DLNode_getNext(self->dummy);
}

static void moveLast(List * self){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "Move in empty list");
    //
    self->index = self->size - 1;
    self->cur = DLNode_getPrev(self->dummy);
}

static void moveNext(List * self){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "Move in empty list");
    //
    self->cur = DLNode_getNext(self->cur);
    ++self->index;
}

static void movePrev(List * self){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self), "Move in empty list");
    //
    self->cur = DLNode_getPrev(self->cur);
    --self->index;
}

static void * peek(const List * self){
    Debug_exists(self);
    Debug_suppose(hasMore(self), "An attempt to peek value from dummy was made");
    //
    return DLNode_at(self->cur);
}

static void * set(List * self, void * val){
    Debug_exists(self);
    Debug_suppose(hasMore(self), "An attempt to set value for dummy was made");
    //
    return DLNode_set(self->cur, val);
}

static void addNext(DLNode * src, DLNode * newbie){
    Debug_exists(src);
    Debug_suppose(DLNode_getNext(src) != NULL, "No next node was found");
    //
    DLNode_setNext(newbie, DLNode_getNext(src));
    DLNode_setPrev(newbie, src);
    DLNode_setPrev(DLNode_getNext(src), newbie);
    DLNode_setNext(src, newbie);
}

static void addPrev(DLNode * src, DLNode * newbie){
    Debug_exists(src);
    Debug_suppose(DLNode_getPrev(src) != NULL, "No prev node was found");
    //
    DLNode_setPrev(newbie, DLNode_getPrev(src));
    DLNode_setNext(newbie, src);
    DLNode_setNext(DLNode_getPrev(src), newbie);
    DLNode_setPrev(src, newbie);
}

static void refreshCur(List * self){
    Debug_exists(self);
    //
    if (self->size != 0 && self->cur == self->dummy){
        self->cur = DLNode_getNext(self->dummy);
        self->index = 0;
    }
}


typedef struct __Iterator {
    const List * list;
    const DLNode * node;
    signed long long index;
} Iterator;

typedef long IteratorDistance;

void Iterator_free(Iterator * self){
    Debug_exists(self);
    //
    free(self);
}

void * Iterator_value    (const Iterator * self){
    Debug_exists(self);
    Debug_suppose(self->list->cur != self->list->dummy, "Iterator out of bounds exception");    
    //
    return self->node->val;
}


void   Iterator_next     (Iterator * self){
    Debug_exists(self);
    Debug_maxbound(self->index, self->list->size - 1);    
    //
    ++self->index;
    self->node = self->node->next;
}

void   Iterator_prev     (Iterator * self){
    Debug_exists(self);
    Debug_minbound(self->index, 0);
    //
    --self->index;
    self->node = self->node->prev;
}

void   Iterator_advance  (Iterator * self, IteratorDistance n){
    Debug_exists(self);
    Debug_maxbound(n, self->list->size - self->index - 1);
    Debug_minbound(n, -self->index);
    //
    (void)List_at(self->list, self->index + n);
    self->node = self->list->cur;
    self->index = self->list->index;
}

bool   Iterator_equals   (const Iterator * self, const Iterator * other){
    Debug_exists(self);
    Debug_exists(other);
    Debug_suppose(imply(self->list == other->list && self->index == other->index, 
                        self->node == other->node), "Iterator logic error, needs debugging");
    //
    return self->node == other->node;
}

IteratorDistance Iterator_distance (Iterator * begin, Iterator * end){
    Debug_exists(begin);
    Debug_exists(end);
    Debug_suppose(begin->list == end->list, "Calculating distance between iterators"
                                             " for different lists exception");
    Debug_tip(end->index >= begin->index, "End iterator is treated like begin iterator");                                             
    //
    return end->index - begin->index;    
}

Iterator * List_getNewBeginIterator (const List * self){
    Debug_exists(self);
    //
    Iterator * newbie = Mem_malloc(sizeof(Iterator));
    *newbie = (Iterator){
        .list  = self,
        .node  = self->dummy->next,
        .index = 0
    };
    return newbie;
}

Iterator * List_getNewEndIterator   (const List * self){
    Debug_exists(self);
    //
    Iterator * newbie = Mem_malloc(sizeof(Iterator));
    *newbie = (Iterator){
        .list  = self,
        .node  = self->dummy->prev,
        .index = self->size - 1
    };
    return newbie;
}

bool Iterator_inbound(const Iterator * self){
    Debug_exists(self);
    //
    return self->node != self->list->dummy;
}

void List_insertionSort(List * self, Comparator compare){
    Debug_exists(self);
    Debug_exists(compare);
    //
    if (self->size == 0) return;
    //
    self->cur = self->dummy->next;
    self->index = 0;
    while (self->index < self->size){
        DLNode * cmp = self->cur->prev;
        DLNode * next = self->cur->next;
        while (cmp != self->dummy && compare(cmp->val, self->cur->val) == -1){
            cmp = cmp->prev;
        }
        if (cmp->next != self->cur){
            (void)DLNode_ncRemove(self->cur);
            DLNode_ncAddAfter(cmp, self->cur);
        }
        self->cur = next;
        ++self->index;
    }
    self->index = 0;
    self->cur = self->dummy->next;
}

void * List_atLast(const List * self){
    Debug_exists(self);
    Debug_suppose(self->size > 0, "An attempt to extract value from empty list was made");
    return self->dummy->prev->val;
}

void * List_atFirst(const List * self){
    Debug_exists(self);
    Debug_suppose(self->size > 0, "An attempt to extract value from empty list was made");
    return self->dummy->next->val;
}

void * List_setFirst(List * self, void * value){
    Debug_exists(self);
    Debug_suppose(self->size > 0, "An attempt to set value for empty list was made");
    void * prev = self->dummy->next->val;
    self->dummy->next->val = value;
    return prev;
}
void * List_setLast(List * self, void * value){
    Debug_exists(self);
    Debug_suppose(self->size > 0, "An attempt to set value for empty list was made");
    void * prev = self->dummy->prev->val;
    self->dummy->prev->val = value;
    return prev;
}

