#include <stack.h>
#include <error.h>
#include <stdlib.h>
#include <slnode.h>

typedef struct __Stack {
    SLNode * top;
} Stack;

Stack * Stack_new(void){
    Stack * newbie = malloc(sizeof(Stack));
    Error_checkHeap(newbie);
    newbie->top = NULL;
    return newbie;
}

void Stack_free(Stack * self){
    Debug_exists(self);
    //
    while (self->top){
        SLNode * next = self->top->next;
        SLNode_free(self->top);
        self->top = next;
    }
    free(self);
}

void Stack_freeWhole(Stack * self, Destructor valDestructor){
    Debug_exists(self);
    Debug_exists(valDestructor);
    //
    while(!Stack_isEmpty(self)){
        valDestructor(Stack_pop(self));
    }
    free(self);
}

void Stack_push(Stack * self, void * val){
    Debug_exists(self);
    //
    SLNode * newbie = SLNode_new(val);
    SLNode_setNext(newbie, self->top);
    self->top = newbie;
}

void * Stack_peek(Stack * self){
    Debug_exists(self);
    Debug_suppose(!Stack_isEmpty(self), "An attempt to peek value from empty stack was made");
    //
    return SLNode_at(self->top);
}

void * Stack_pop(Stack * self){
    Debug_exists(self);
    Debug_suppose(!Stack_isEmpty(self), "An attempt to peek value from empty stack was made");
    //
    SLNode * popped = self->top;
    self->top = SLNode_getNext(self->top);

    void * poppedVal = SLNode_at(popped);
    SLNode_free(popped);
    return poppedVal;
}

bool Stack_isEmpty(Stack * self){
    Debug_exists(self);
    //
    return self->top == NULL;
}