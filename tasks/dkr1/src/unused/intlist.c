#include <intlist.h>
#include <list.h>
#include <integer.h>

#include <list_ext.h>
#include <error.h>



typedef struct __IntList {
    List * list;
} IntList;



IntList * IntList_new(void){
    IntList * newbie = malloc(sizeof(IntList));
    Error_checkHeap(newbie);
    newbie->list = List_new();
    return newbie;
}

void IntList_free(IntList * self){
    Debug_exists(self);
    //
    List_freeWhole(self->list, (Destructor)Integer_free);
    free(self);
}

void IntList_addLast(IntList * self, int value){
    Debug_exists(self);
    //
    List_addLast(self->list, Integer_new(value));
}

void IntList_addFirst(IntList * self, int value){
    Debug_exists(self);
    //
    List_addFirst(self->list, Integer_new(value));
}

void IntList_insert(IntList * self, size_t index, int value){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->list) - 1);
    //
    List_insert(self->list, index, Integer_new(value));
}

int IntList_removeAt(IntList * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->list) - 1);
    //
    Integer * trash = List_removeAt(self->list, index);
    int val = Integer_at(trash);
    Integer_free(trash);
    return val;
}

size_t IntList_size(IntList * self){
    Debug_exists(self);
    //
    return List_size(self->list);
}

int IntList_at(IntList * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->list) - 1);
    //
    return Integer_at(List_at(self->list, index));
}

/**
 *  @returns old value at index
 */
int IntList_set(IntList * self, size_t index, int value){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->list) - 1);
    //
    return Integer_set(List_at(self->list, index), value);
}
