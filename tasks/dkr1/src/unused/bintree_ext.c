#include <bintree_ext.h>
#include <assert.h>
#include <error.h>
#include <integer.h>
#include <stdio.h>
#include <progbase/console.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>


extern const wchar_t TREE_VLINE;
extern const wchar_t TREE_RIGHT_INBRANCH[];
extern const wchar_t TREE_RIGHT_OUTBRANCH[];
extern const wchar_t TREE_LEFT_OUTBRANCH[];


// Через те що, бібліотека <wchar.h> якось конфліктує з функціями i/o для стандартних char 
// прийшлося переписати функції, що використовують їх під використання відповідних
// функцій бібліотеки широких чарів

static void move(signed int y, signed int x){
    wprintf(L"\033[%i;%iH", y, x); // Thanks RA for conPos
}

static void printIntTree(BinTree * self, int level);
static size_t printFancy(BinTree * self, signed int y, signed int x, Printer print);
static void printHLine(wchar_t token, unsigned int size);
static void printVLine(wchar_t token, unsigned short height, signed int y, signed int x);

BinTree * BinTree_newInt(int val){
    return BinTree_new(Integer_new(val));
}

void BinTree_freeWhole(BinTree * self, Destructor valDestructor){
    Debug_exists(self);
    Debug_exists(valDestructor);
    //
    if (BinTree_hasLeft(self)){
        BinTree_freeWhole(BinTree_getLeft(self), valDestructor);
    }
    if (BinTree_hasRight(self)){
        BinTree_freeWhole(BinTree_getRight(self), valDestructor);
    }
    BinTree_freeAll(self, valDestructor);
}

int BinTree_getInt(BinTree * self){
    Debug_exists(self);
    //
    return Integer_at(BinTree_at(self));
}

int BinTree_setInt(BinTree * self, int val){
    Debug_exists(self);
    //
    return Integer_set(BinTree_at(self), val);
}

bool BinTree_contains(BinTree * self, void * val, Comparator compare){
    if (self == NULL) return false;
    signed char compared = (*compare)(val, BinTree_at(self));
    switch (compared){
        case 0:
            return true;
        case 1:
            return BinTree_contains(BinTree_getLeft(self), val, compare);
        case -1:
            return BinTree_contains(BinTree_getRight(self), val, compare);
        default:
            Debug_shutdown("Unhandled comparator return value received");
    }
}

static void printIntTree(BinTree * self, int level){
    printHLine(L'.', level << 1);
    if (self == NULL){
        wprintf(L"(null)\n");
        return;
    }
    Integer_printWide(BinTree_at(self));
    putwchar(L'\n');
    if (!BinTree_isLeaf(self)){
        printIntTree(BinTree_getLeft(self), level + 1);
        printIntTree(BinTree_getRight(self), level + 1);
    }
}

void BinTree_printIntFormat(BinTree * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    printIntTree(self, 0);
}

void BinTree_printIntInOrder(BinTree * self){
    Debug_UTF8();
    if (self == NULL) return;
    //
    BinTree_printIntInOrder(BinTree_getLeft(self));
    //
    Integer_printWide(BinTree_at(self)); 
    wprintf(L", ");
    //
    BinTree_printIntInOrder(BinTree_getRight(self));
}

void BinTree_printFancy(BinTree * self, Location loc, Printer print){
    Debug_UTF8();
    Debug_exists(print);
    Debug_exists(self);
    Debug_minbound(loc.y, 1);
    Debug_minbound(loc.x, 1);
    //
    move(loc.y + printFancy(self, loc.y, loc.x, print) , 1);
}

static void printVLine(wchar_t token, unsigned short height, signed int y, signed int x){
    while (height != 0){
        move(y++, x);
        putwchar(token);
        --height;
    }
}

static size_t printFancy(BinTree * self, signed int y, signed int x, Printer print){
    if (self == NULL) return 0;
    //
    int kinder = 0;
    fflush(stdout);
    move(y, x);
    (*print)(BinTree_at(self));
    if (BinTree_hasRight(self)){
        move(y + 1, x);
        if (BinTree_hasLeft(self)){
            wprintf(TREE_RIGHT_INBRANCH);
            kinder = printFancy(BinTree_getRight(self), y + 1, 
                x + sizeof(TREE_RIGHT_INBRANCH) / sizeof(TREE_RIGHT_INBRANCH[0]) - 1, print);
        }
        else {
            wprintf(TREE_RIGHT_OUTBRANCH); 
            kinder = printFancy(BinTree_getRight(self), y + 1,
                x + sizeof(TREE_RIGHT_OUTBRANCH) / sizeof(TREE_RIGHT_OUTBRANCH[0]) - 1, print);
        }
    }
    if (BinTree_hasLeft(self)){
        if (BinTree_hasRight(self)){
            printVLine(TREE_VLINE, kinder - 1, y + 2 , x);
        }
        move(y + 1 + kinder, x);
        wprintf(TREE_LEFT_OUTBRANCH);
        kinder += printFancy(BinTree_getLeft(self), y + 1 + kinder,
            x + sizeof(TREE_LEFT_OUTBRANCH) / sizeof(TREE_LEFT_OUTBRANCH[0]) - 1, print);
    }
    return kinder + 1;
    
}


static void printHLine(wchar_t token, unsigned int size){
    while(size != 0){
        putwchar(token);
        --size;
    }
}


BinTree * BinTree_search(BinTree * self, void * val, Comparator compare){
    if (self == NULL) return NULL;
    if (compare(val, BinTree_at(self)) == 0) return self;
    
    BinTree * leftBranch = BinTree_search(BinTree_getLeft(self), val, compare);
    if (leftBranch != NULL) 
        return leftBranch;
    else
    return BinTree_search(BinTree_getRight(self), val, compare);
}
