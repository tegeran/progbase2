#include <dynamic_array.h>
#include <stdlib.h>
#include <error.h>
#include <mem.h>


typedef struct __DynamicArray{
    void ** items;
    size_t size;
    size_t capacity;
} DynamicArray;

static void Private_ensureCapacity(DynamicArray * self){
    if (self->size == self->capacity){
        self->capacity = self->capacity << 1;
        self->items = Mem_reallocate(self->items, sizeof(void*) * self->capacity); // mem reallocate performs a check
    }                                                                // for NULL pointer
}


DynamicArray * DynamicArray_new(void){
    DynamicArray * newbie = Mem_malloc(sizeof(DynamicArray)); // mem malloc performs a check for NULL pointer
    newbie->capacity = DynamicArray_INITIAL_CAPACITY;
    newbie->items = malloc(sizeof(void*) * newbie->capacity);
    newbie->size = 0;
    return newbie;
}

void DynamicArray_free(DynamicArray * self){
    Debug_exists(self);
    Debug_tip(self->size == 0, "An attempt to free not empty array was made");
    //
    free(self->items);
    free(self);
}

void DynamicArray_freeWhole(DynamicArray * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    //
    for (size_t i = 0; i < self->size; ++i){
        destroy(self->items[i]); // case self->items[i] == NULL destroy function must handle it
    }
    free(self->items);
    free(self);
}


void * DynamicArray_at(DynamicArray * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->size - 1);
    //
    return self->items[index];
}

size_t DynamicArray_size(DynamicArray * self){
    Debug_exists(self);
    //
    return self->size;
}

void * DynamicArray_set(DynamicArray * self, size_t index, void * value){
    Debug_exists(self);
    Debug_maxbound(index, self->size - 1);
    //
    void * prev = self->items[index];
    self->items[index] = value;
    return prev;
}

void DynamicArray_insert(DynamicArray * self, size_t index, void * value){
    Debug_exists(self);
    Debug_maxbound(index, self->size);
    //
    Private_ensureCapacity(self);
    for (size_t i = self->size++; i > index; --i){
        self->items[i] = self->items[i - 1];
    }
    self->items[index] = value;
}

//
void DynamicArray_removeBy(DynamicArray * self, Answer caseRemove, Destructor destroy){
    Debug_exists(self);
    Debug_exists(caseRemove);
    Debug_exists(destroy);
    //
    for (size_t i = 0; i < self->size; ++i){
        if (caseRemove(self->items[i])){
            destroy(DynamicArray_removeAt(self, i--));
        }
    }
}
void DynamicArray_clear(DynamicArray * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    //
    for (size_t i = 0; i < self->size; ++i){
        destroy(self->items[i]); // case self->items[i] == NULL destroy function must handle it
    }
    self->size = 0;
}   

// NOTE: first node has index 0

void * DynamicArray_removeAt(DynamicArray * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->size - 1);
    //
    void * badboy = self->items[index];
    for (size_t i = index + 1; i < self->size; ++i){
        self->items[i - 1] = self->items[i];
    }
    --self->size;
    return badboy;
}


void DynamicArray_addLast(DynamicArray * self, void * value){
    Debug_exists(self);
    //
    Private_ensureCapacity(self);
    self->items[self->size++] = value;
}

void DynamicArray_addFirst(DynamicArray * self, void * value){
    Debug_exists(self);
    //
    Private_ensureCapacity(self);
    for (size_t i = self->size++; i > 0; --i){
        self->items[i] = self->items[i - 1];
    }
    self->items[0] = value;
}

bool DynamicArray_isEmpty(DynamicArray * self){
    Debug_exists(self);
    //
    return self->size == 0;
}

size_t DynamicArray_memoryUsage(DynamicArray * self){
    Debug_exists(self);
    //
    return self->capacity * sizeof(void *);
}