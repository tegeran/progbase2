#include <dynamic_array_ext.h>
#include <error.h>
#include <stdio.h>
#include <wchar.h>

void DynamicArray_print(DynamicArray * self, Printer print){
    Debug_exists(self);
    Debug_exists(print);
    //
    if (DynamicArray_isEmpty(self)){
        printf("[ ]");
        return;
    }
    printf("[ ");
    size_t i = 0; 
    while (i < DynamicArray_size(self) - 1){
        print(DynamicArray_at(self, i++));
        printf(", ");
    }
    print(DynamicArray_at(self, i));
    printf(" ]");
}

void DynamicArray_printWide(DynamicArray * self, Printer print){
    Debug_exists(self);
    Debug_exists(print);
    Debug_UTF8();
    //
    if (DynamicArray_isEmpty(self)){
        wprintf(L"[ ]");
        return;
    }
    wprintf(L"[ ");
    size_t i = 0; 
    while (i < DynamicArray_size(self) - 1){
        print(DynamicArray_at(self, i++));
        wprintf(L", ");
    }
    print(DynamicArray_at(self, i));
    wprintf(L" ]");
}

void DynamicArray_writeTo(FILE * file, DynamicArray * self, Printer print){
    Debug_exists(self);
    Debug_exists(file);
    Debug_exists(print);
    //
    if (DynamicArray_isEmpty(self)){
        fwprintf(file, L"[ ]");
        return;
    }
    fwprintf(file, L"[ ");
    size_t i = 0;
    while(i < DynamicArray_size(self) - 1){
        print(DynamicArray_at(self, i));
        fwprintf(file, L", ");
        ++i;
    }
    print(DynamicArray_at(self, i));
    fwprintf(file, L" ]");
}