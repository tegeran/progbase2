#include <bstree.h>
#include <stdlib.h>
#include <error.h>
#include <bintree_ext.h>
#include <integer.h>



typedef struct __BSTree {
    BinTree * root;
} BSTree;

static void clear(BinTree * self);
static void insert(BinTree * self, int key);

BSTree * BSTree_new(void){
    BSTree * newbie = malloc(sizeof(BSTree));
    Error_checkHeap(newbie);
    //
    newbie->root = NULL;
    return newbie;
}

void BSTree_free(BSTree * self){
    Debug_exists(self);
    Debug_tip(BSTree_isEmpty(self), "An attempt to free not empty binary search tree was made");
    //
    free(self);
}

void BSTree_freeWhole(BSTree * self){
    Debug_exists(self);   
    //
    BSTree_clear(self);
    //
    BSTree_free(self);
}

bool BSTree_isEmpty(BSTree * self){
    Debug_exists(self);
    //
    return self->root == NULL;
}

void BSTree_insert(BSTree * self, int key){
    Debug_exists(self);
    //
    if (BSTree_isEmpty(self))
        self->root = BinTree_newInt(key);
    else 
        insert(self->root, key);
}

void BSTree_clear(BSTree * self){
    Debug_exists(self);
    //
    if (BSTree_isEmpty(self) == false)
        BinTree_freeWhole(self->root, (Destructor)Integer_free);

    //
    self->root = NULL;
}

void BSTree_printInOrder(BSTree * self){
    Debug_exists(self);
    //
    if (!BSTree_isEmpty(self)){
        BinTree_printIntInOrder(self->root);
    }
}

void BSTree_printFormat(BSTree * self){
    Debug_exists(self);
    //
    if (!BSTree_isEmpty(self)){
        BinTree_printIntFormat(self->root);
    }
}

void BSTree_insertArr(BSTree * self, int * arr, size_t size){
    Debug_exists(self);
    Debug_exists(arr);
    //
    for (size_t i = 0; i < size; ++i){
        BSTree_insert(self, arr[i]);
    }
}



static void clear(BinTree * self){
    Debug_exists(self);
    //
    BinTree_freeWhole(self, (Destructor)Integer_free);
}

static void insert(BinTree * self, int key){
    Debug_exists(self);
    //
    assert(key != Integer_at(BinTree_at(self)));

    if (key < Integer_at(BinTree_at(self))){
        //
        if (BinTree_getLeft(self) == NULL)
            BinTree_setLeft(self, BinTree_newInt(key));
        else
            insert(BinTree_getLeft(self),  key);
    //        
    }   
    else {
        if (BinTree_getRight(self) == NULL)
            BinTree_setRight(self, BinTree_newInt(key));
        else
            insert(BinTree_getRight(self),  key);
    //    
    }
}


bool BSTree_contains(BSTree * self, int val){
    Debug_exists(self);
    if (BSTree_isEmpty(self)) return false;
    Integer * toCompare = Integer_new(val);
    bool answer =  BinTree_contains(self->root, toCompare, (Comparator)Integer_compare);
    Integer_free(toCompare);
    return answer;
}

void BSTree_printFancy(BSTree * self, Location loc){
    Debug_exists(self);
    //
    if (!BSTree_isEmpty(self))
        BinTree_printFancy(self->root, loc, (Printer)Integer_printWide);
}