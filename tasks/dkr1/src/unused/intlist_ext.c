#include <intlist_ext.h>
#include <stdio.h>
#include <list_ext.h>
#include <integer.h>
#include <intlist.h>
#include <wchar.h>
#include <error.h>

IntList * IntList_newArrayCopy(int * array, size_t size){
    Debug_exists(array);
    //
    IntList * newbie = IntList_new();
    for (int i = 0; i < size; ++i){
        IntList_addLast(newbie, (array[i]));
    }
    return newbie;
}

IntList * IntList_newInit(size_t size, int values){
    IntList * newbie = IntList_new();
    while (size != 0) {
        IntList_addLast(newbie, values);
        --size;
    }
    return newbie;
}

IntList * IntList_newInitRandom(size_t size, int seed, int min, int max){
    IntList * newbie = IntList_new();
    srand(seed);
    while(size != 0){
        IntList_addLast(newbie, rand() % (max - min + 1) + min);
        --size;
    }
    return newbie;
}

void IntList_print(IntList * self){
    Debug_exists(self);
    //
    printf("[ ");
    const size_t SIZE = IntList_size(self);
    for (int i = 0; i < SIZE - 1; ++i){
        printf("%i, ", IntList_at(self, i));
    }
    printf("%i ]", IntList_at(self, SIZE - 1));
}

void IntList_printWide(IntList * self){
    Debug_exists(self);
    //
    wprintf(L"[ ");
    const size_t SIZE = IntList_size(self);
    for (int i = 0; i < SIZE - 1; ++i){
        wprintf(L"%i, ", IntList_at(self, i));
    }
    wprintf(L"%i ]", IntList_at(self, SIZE - 1));
}

void IntList_removeBy(IntList * self, AnswerInt caseRemove){
    Debug_exists(self);
    Debug_exists(caseRemove);
    //
    size_t size = IntList_size(self);
    for (int i = 0; i < size; ++i){
        if ((*caseRemove)(IntList_at(self, i))){
            IntList_removeAt(self, i);
            --size;
            --i;
        }
    }
}



