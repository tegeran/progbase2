#include <array.h>

#define NO_DEBUG_TIPS
#include <error.h>
#include <mem.h>

Array * Array_new(size_t size){
    Array * newbie = Mem_malloc(sizeof(Array));
    newbie->a = Mem_malloc(size * sizeof(void *));
    newbie->size = size;
    return newbie;
}

void Array_free(Array * self){
    Debug_exists(self);
    //
    free(self->a);
    free(self);
}

size_t Array_sizeAt(Array * self){
    Debug_exists(self);
    //
    return self->size;
}

void * Array_at(Array * self, size_t index){
    Debug_exists(self);
    Debug_tip(index < self->size, "Possible array index out of bounds exception");
    //
    return self->a[index];
}

void ** Array_arrayAt(Array * self){
    Debug_exists(self);
    //
    return self->a;
}

size_t Array_setSize(Array * self, size_t size){
    Debug_exists(self);
    //
    size_t prev = self->size;
    self->size = size;
    return prev;
}

void * Array_set(Array * self, size_t index, void * val){
    Debug_exists(self);
    Debug_tip(index < self->size, "Possible array index out of bounds exception");
    //
    void * prev = self->a[index];
    self->a[index] = val;
    return prev;
}

void ** Array_setArray(Array * self, void ** array){
    Debug_exists(self);
    //
    void ** prev = self->a;
    self->a = array;
    return prev;
}







typedef signed char (*Comparator)(void * left, void * right);

void Array_swap(Array * self, size_t little, size_t pip){
    Debug_exists(self);
    Debug_maxbound(little, self->size - 1);
    Debug_maxbound(pip, self->size - 1);
    //
    void * keeper = self->a[little];
    self->a[little] = self->a[pip];
    self->a[pip] = keeper;
}

static signed long long Private_binarySearch(void ** arr, void * template, Comparator compare, size_t left, size_t right){
    if (left > right) return -1L;
    size_t middle = (right + left) >> 1;
    switch (compare(arr[middle], template)){
        case 1  : return Private_binarySearch(arr, template, compare, middle + 1, right);
        case -1 : return Private_binarySearch(arr, template, compare, left, middle - 1);
    }
    return middle;
}

signed long long Array_binarySearch(Array * self, void * template, Comparator compare){
    Debug_exists(self);
    Debug_exists(self->a);
    Debug_exists(compare);
    //
    return Private_binarySearch(self->a, template, compare, 0, self->size - 1);
}

signed long long Array_linearSearch(Array * self, void * template, Comparator compare){
    Debug_exists(self);
    Debug_exists(compare);
    Debug_exists(self->a);
    //
    size_t i = 0;
    while (i < self->size){
        if (compare(self->a[i], template) == 0)
            return i;
        ++i;
    }
    return -(signed long long)i - 1LL;
}

void Array_reverse(Array * self){
    Debug_exists(self);
    Debug_exists(self->a);
    //
    for (size_t i = 0, j = self->size - 1; i < j; ++i, --j){
        Array_swap(self, i, j);
    }
}

void Array_fill(Array * self, void * value){
    Debug_exists(self);
    Debug_exists(self->a);
    //
    for (size_t i = 0; i < self->size; ++i){
        self->a[i] = value;        
    }
}


static unsigned int absolute(signed int integer){
    return integer > 0 ? integer : -integer;
}

void Array_shift(Array * self, signed int shift){
    Debug_exists(self);
    Debug_exists(self->a);
    //
    if (absolute(shift) >= self->size || shift == 0) return;
    //
    if (shift > 0){
        for (signed long long i = self->size - 1 - shift; i >= 0; --i){
            self->a[i + shift] = self->a[i];
        }
    }
    else {
        for (size_t i = -shift; i < self->size; ++i){
            self->a[i + shift] = self->a[i];
        }
    }
}

void Array_nullify(Array * self){
    Debug_exists(self);
    Debug_exists(self->a);
    //
    for (size_t i = 0; i < self->size; ++i){
        self->a[i] = NULL;
    }
}