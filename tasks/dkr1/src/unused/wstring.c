#include <wstring.h>
#include <error.h>
#include <mem.h>
#include <fsm.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>


#define WIDE_STRING_INITIAL_CAPACITY 16

typedef struct __WideString{
    wchar_t * str;
    size_t len;
    size_t capacity;
} WideString;

static void Private_ensureCapacity(WideString * self, size_t appending){
    if (self->len + appending >= self->capacity){
        do {
            self->capacity = self->capacity << 1;
        } while (self->len + appending >= self->capacity);

        self->str = Mem_reallocate(self->str, sizeof(wchar_t) * self->capacity); // mem reallocate performs a check
    }                                                                // for NULL pointer
}

WideString * WideString_toNewWideString(const wchar_t * wchars){
    Debug_exists(wchars);
    //
    WideString * newbie = WideString_new();
    WideString_append(newbie, wchars);
    return newbie;
}


// adds a new object to the end of list (time complexity O(1))
void WideString_append(WideString * self, const wchar_t * wchars){
    Debug_exists(self);
    Debug_exists(wchars);
    //
    Private_ensureCapacity(self, wcslen(wchars));
    while (*wchars != '\0'){
        self->str[self->len++] = *wchars++;
    };
    self->str[self->len] = '\0';
}

void WideString_appendWChar(WideString * self, wchar_t wcharacter){
    Debug_exists(self);
    if (wcharacter == '\0') return;
    //
    Private_ensureCapacity(self, 1);
    self->str[self->len++] = wcharacter;
    self->str[self->len] = '\0';
}

void WideString_concatenate(WideString * self, const WideString * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    wchar_t * cur = other->str;
    Private_ensureCapacity(self, other->len);
    while (*cur != '\0'){
        self->str[self->len++] = *cur++;
    }
    self->str[self->len] = '\0';
}

wchar_t * WideString_toNewWChars(const WideString * self){
    Debug_exists(self);
    //
    wchar_t * newbie = Mem_malloc(sizeof(wchar_t) * (self->len + 1));
    const wchar_t * cur = self->str;
    for (size_t i = 0; *cur != '\0'; ++i){
        newbie[i] = *cur++;
    }
    newbie[self->len] = '\0';
    return newbie;
}


void WideString_print(const WideString * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    const wchar_t * cur = self->str;
    while (*cur != '\0'){
        putwchar(*cur++);
    }
}

void WideString_writeTo(FILE * file, const WideString * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    fputws(self->str, file);
}


WideString * WideString_new(void){
    WideString * newbie = Mem_malloc(sizeof(WideString)); // mem malloc performs a check for NULL pointer
    newbie->capacity = WIDE_STRING_INITIAL_CAPACITY;
    newbie->str = Mem_malloc(sizeof(wchar_t) * newbie->capacity);
    newbie->len = 0;
    newbie->str[0] = '\0';
    return newbie;
}

void WideString_free(WideString * self){
    Debug_exists(self);
    //
    free(self->str);
    free(self);
}

wchar_t WideString_at(const WideString * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1); // restrict interacting with '\0'
    //
    return self->str[index];
}

size_t WideString_length(const WideString * self){
    Debug_exists(self);
    //
    return self->len;
}

wchar_t WideString_set(WideString * self, size_t index, wchar_t value){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1); // restrict interacting with '\0'
    //
    wchar_t prev = self->str[index];
    self->str[index] = value;
    if (value == '\0'){
        self->len = index;
    }
    return prev;
}

void WideString_insert(WideString * self, size_t index, wchar_t value){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1);
    //
    if (value == '\0'){
        self->str[index] = '\0';
        self->len = index;
        return;
    }
    Private_ensureCapacity(self, 1);
    for (size_t i = self->len++; i > index; --i){
        self->str[i] = self->str[i - 1];
    }
    self->str[index] = value;
}

//
void WideString_removeBy(WideString * self, AnswerWChar caseRemove){
    Debug_exists(self);
    Debug_exists(caseRemove);
    //
    for (size_t i = 0; i < self->len; ++i){
        if (caseRemove(self->str[i])){
            WideString_removeAt(self, i--);
        }
    }
}
void WideString_clear(WideString * self){
    Debug_exists(self);
    //
    self->len = 0;
    self->str[0] = '\0';
}   


wchar_t WideString_removeAt(WideString * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1);
    //
    wchar_t badboy = self->str[index];
    for (size_t i = index + 1; i <= self->len; ++i){
        self->str[i - 1] = self->str[i];
    }
    --self->len;
    return badboy;
}
void WideString_addWCharsFirst(WideString * self, wchar_t * wchars){
    Debug_exists(self);
    Debug_exists(wchars);
    //
    if (*wchars == '\0') return;
    size_t adding = wcslen(wchars);        
    size_t newLen = adding + self->len;   // query new length
    size_t i = self->len;                 
    Private_ensureCapacity(self, adding);
    while (i != 0){
        self->str[newLen--] = self->str[i--];
    }
    self->str[newLen] = self->str[0];     // transfering 0-th wcharacter
    while (*wchars){
        self->str[i++] = *wchars++;        // i == 0 at this point
    }
    self->len = self->len + adding;
}

void WideString_addWCharFirst(WideString * self, wchar_t wcharacter){
    Debug_exists(self);
    //
    if (wcharacter == '\0'){
        self->str[0] = '\0';
        self->len = 0;
        return;
    }
    Private_ensureCapacity(self, 1);
    for (size_t i = ++self->len; i > 0; --i){
        self->str[i] = self->str[i - 1];
    }
    self->str[0] = wcharacter;
}

bool WideString_isEmpty(const WideString * self){
    Debug_exists(self);
    //
    return self->len == 0;
}

size_t WideString_memoryUsage(const WideString * self){
    Debug_exists(self);
    //
    return self->capacity * sizeof(wchar_t);
}



signed char WideString_compare(const WideString * self, const WideString * other){
    size_t i = 0;
    while (i < self->len && i < other->len){
        if (self->str[i] > other->str[i]){
            return -1;
        } else if (self->str[i] < other->str[i]){
            return 1;
        }
        ++i;
    }
    if (self->len == other->len) return 0;
    else return self->len > other->len ? -1 : 1;
}

WideString * WideString_toNewSubstring(const WideString * self, size_t left, size_t right){
    Debug_exists(self);
    Debug_maxbound(right, self->len - 1);
    Debug_maxbound(left, right);
    
    WideString * newbie = WideString_new();
    Private_ensureCapacity(newbie, right - left + 1);
    while (left <= right){
        newbie->str[newbie->len++] = self->str[left++];
    }
    newbie->str[newbie->len] = '\0';
    return newbie;
}

wchar_t * WideString_toNewSubwchars(const WideString * self, size_t left, size_t right){
    Debug_exists(self);
    Debug_maxbound(right, self->len - 1);
    Debug_maxbound(left, right);
    //
    wchar_t * const newbie = Mem_malloc(sizeof(wchar_t) * (right - left + 2));
    size_t i = 0;
    while (left <= right){
        newbie[i++] = self->str[left++];
    }
    newbie[i] = '\0';
    return newbie;
}

void WideString_appendSubwchars(WideString * self, const wchar_t * wchars, size_t left, size_t right){
    Debug_exists(self);
    Debug_exists(wchars);
    Debug_maxbound(left, right);
    //
    Private_ensureCapacity(self, right - left + 1);
    while (left <= right){
        self->str[self->len++] = wchars[left++];
    }
    self->str[self->len] = '\0';
}

void WideString_appendSubstring(WideString * self, const WideString * pattern, size_t left, size_t right){
    Debug_exists(self);
    Debug_exists(pattern);
    Debug_maxbound(left, right);
    //
    WideString_appendSubwchars(self, pattern->str, left, right);
}


List * WideString_toNewWords(const WideString * self, AnswerWChar isWordLetter){
    Debug_exists(self);
    Debug_exists(isWordLetter);
    //
    List * list = List_new();
    wchar_t * cur = self->str;  
    FSMRead_Words state = FSM_WORDS_LAUNCH;
    while (state != FSM_WORDS_SHUTDOWN){   
        switch (state = FSM_readWord(state, *cur, isWordLetter)){
            case FSM_WORDS_START_WORD: List_addLast(list, WideString_new());
            case FSM_WORDS_READ_WORD:  WideString_appendWChar(List_at(list, List_size(list) - 1), *cur);
            default: break;
        }
        ++cur;
    }
    return list;
}

void WideString_rewrite(WideString * self, const wchar_t * wchars){
    Debug_exists(self);
    Debug_exists(wchars);
    //
    self->len = 0;
    Private_ensureCapacity(self, wcslen(wchars));
    while (*wchars != '\0'){
        self->str[self->len++] = *wchars++;
    }
    self->str[self->len] = '\0';
}

// https://stackoverflow.com/a/2624210/9259330
size_t WideString_hashify(const WideString * self){
    Debug_exists(self);
    //
    size_t hash = 7;
    const wchar_t * cur = self->str;
    while (*cur != '\0') {
        hash = hash*31 + *cur++;
    }
    return hash;
}

size_t WChars_hashify(const wchar_t * self){
    Debug_exists(self);
    //
    size_t hash = 7;
    while (*self != '\0') {
        hash = hash*31 + *self++;
    }
    return hash;
}

bool WideString_equals(const WideString * self, const WideString * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    return wcscmp(self->str, other->str) == 0;
}

bool WChars_equals(const wchar_t * self, const wchar_t * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    return wcscmp(self, other) == 0;
}


signed long long WideString_searchSpecWChar(const WideString * self, AnswerWChar isTarget){
    Debug_exists(self);
    Debug_exists(isTarget);
    //
    for (size_t i = 0; self->str[i] != '\0'; ++i){
        if (isTarget(self->str[i])) return i;
    }
    return -1;
}

signed long long WideString_searchWChar(const WideString * self, wchar_t suspect){
    Debug_exists(self);
    //
    for (size_t i = 0; self->str[i] != '\0'; ++i){
        if (suspect == self->str[i]) return i;
    }
    return -1;
}

signed long long WideString_searchSubstring(const WideString * self, const WideString * pattern){
    return WideString_searchSubwchars(self, pattern->str);
}

signed long long WideString_searchSubwchars(const WideString * self, const wchar_t * pattern){
    Debug_exists(self);
    Debug_exists(pattern);
    //
    wchar_t * answered = wcsstr(self->str, pattern);
    return answered == NULL ? -1 : answered - self->str;
}

signed long long WideString_searchLastSpecWChar(const WideString * self, AnswerWChar isTarget){
    Debug_exists(self);
    Debug_exists(isTarget);
    //
    for (size_t i = self->len - 1; i > 0; --i){
        if (isTarget(self->str[i])) return i;
    }
    return isTarget(self->str[0]) ? 0 : -1;
}

signed long long WideString_searchLastWChar(const WideString * self, wchar_t template){
    Debug_exists(self);
    //
    for (size_t i = self->len - 1; i > 0; --i){
        if (template == self->str[i]) return i;
    }
    return template == self->str[0] ? 0 : -1;
}


static bool Private_isNonSpace(wchar_t suspect){
    return !isspace(suspect);
}

static bool Private_isSpace(wchar_t suspect){
    return isspace(suspect);
}

void WideString_trim(WideString * self){
    Debug_exists(self);
    //
    signed long long firstNonSpace = WideString_searchSpecWChar(self, (AnswerWChar)Private_isNonSpace);
    switch (firstNonSpace) {
        case 0: {
            WideString_reduce(self, WideString_searchLastSpecWChar(self, (AnswerWChar)Private_isNonSpace) + 1);
            return;
        }
        default: {
            WideString_cut(self, firstNonSpace, WideString_searchLastSpecWChar(self, (AnswerWChar)Private_isNonSpace));
        }
        case -1: return;
    }
}

WideString * WideString_newCopyOf(const WideString * pattern){
    Debug_exists(pattern);
    //
    return WideString_toNewWideString(pattern->str);
}

void WideString_reduce(WideString * self, size_t newLength){
    Debug_exists(self);
    Debug_maxbound(newLength, self->len);
    //
    self->str[newLength] = '\0';
    self->len = newLength;
}

void WideString_cut(WideString * self, size_t left, size_t right){
    Debug_exists(self);
    Debug_maxbound(right, self->len - 1);
    Debug_maxbound(left, right);
    //
    if (left == 0){
        WideString_reduce(self, right + 1);
        return;
    }
    self->len = right - left + 1;
    size_t i = 0;
    while(left != right + 1){
        self->str[i++] = self->str[left++];
    }
    self->str[i] = '\0';
}

size_t WideString_readFrom(FILE * file, WideString * self){
    Debug_exists(file);
    Debug_exists(self);
    //
    int ch; 
    self->len = 0;
    Private_ensureCapacity(self, Mem_getFileSize(file) / sizeof(wchar_t));
    while ((ch = fgetc(file))!= EOF){
        self->str[self->len++] = ch;
    }
    self->str[self->len] = '\0';
    return self->len;
}

size_t WideString_appendFrom(FILE * file, WideString * self){
    Debug_exists(file);
    Debug_exists(self);
    //
    int ch; 
    Private_ensureCapacity(self, Mem_getFileSize(file) / sizeof(wchar_t));
    while ((ch = fgetc(file))!= EOF){
        self->str[self->len++] = ch;
    }
    self->str[self->len] = '\0';
    return self->len;
}


size_t WideString_writeFormat(WideString * self, wchar_t * fmt, ...){
    Debug_exists(self);
    Debug_exists(fmt);
    //
    va_list argPtr;
    va_list supporter;
    va_start(argPtr, fmt);
    va_copy(supporter, argPtr);
    
    signed int res = vswprintf(self->str, self->capacity, fmt, supporter);
    while (res < 0){
        self->capacity = self->capacity << 1;
        free(self->str);
        self->str = Mem_malloc(sizeof(wchar_t) * self->capacity);
        va_end(supporter);
        va_copy(supporter, argPtr);
        res = vswprintf(self->str, self->capacity, fmt, supporter);
    }
    va_end(argPtr);
    va_end(supporter);
    self->len = res;
    return res;
}

size_t WideString_appendFormat(WideString * self, wchar_t * fmt, ...){
    Debug_exists(self);
    Debug_exists(fmt);
    //
    va_list argPtr;
    va_list supporter;
    va_start(argPtr, fmt);
    va_copy(supporter, argPtr);
    
    signed int res = vswprintf(self->str + self->len, self->capacity - self->len, fmt, supporter);
    while (res < 0){
        self->capacity = self->capacity << 1;
        self->str = Mem_reallocate(self->str, sizeof(wchar_t) * self->capacity);
        va_end(supporter);
        va_copy(supporter, argPtr);
        res = vswprintf(self->str + self->len, self->capacity - self->len, fmt, supporter);
    }
    va_end(argPtr);
    va_end(supporter);
    self->len += res;
    return self->len;
}

typedef enum {
    FSMW_CSV_LAUNCH,
    FSMW_CSV_SHUTDOWN,

    FSMW_CSV_SKIP_STRING,
    FSMW_CSV_SKIP,
    FSMW_CSV_DETECTED_STRING,
    FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE,
    FSMW_CSV_INSERT_DQUOTE
} FSMWriteCsvValue;

static FSMWriteCsvValue FSM_writeCsvValue(FSMWriteCsvValue currentState, wchar_t signal){
    if (signal == '\0') return FSMW_CSV_SHUTDOWN;
    switch (currentState){
        case FSMW_CSV_SKIP:
        case FSMW_CSV_LAUNCH:{  
            switch (signal){
                case '\"': return FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE;
                case '\n':
                case ',': return FSMW_CSV_DETECTED_STRING;
                default : return FSMW_CSV_SKIP;
            }
        }
        case FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE:
        case FSMW_CSV_INSERT_DQUOTE:
        case FSMW_CSV_DETECTED_STRING:
        case FSMW_CSV_SKIP_STRING:{
            return signal == '\"' ? FSMW_CSV_INSERT_DQUOTE : FSMW_CSV_SKIP_STRING;
        }
        default: Debug_shutdown("FSM was supposed to be shutdown at this state");
    }
}

void WideString_appendCsvFormatedWChars(WideString * self, const wchar_t * str){
    Debug_exists(self);
    Debug_exists(str);
    if (str[0] == '\0') return;
    //
    FSMWriteCsvValue state = FSMW_CSV_LAUNCH;
    size_t start = self->len;
    while (state != FSMW_CSV_SHUTDOWN){
        switch (state = FSM_writeCsvValue(state, *str)){
            case FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE:
                WideString_insert(self, start,'\"');
            case FSMW_CSV_INSERT_DQUOTE: {
                WideString_append(self, L"\"\"");
                break;
            }
            case FSMW_CSV_DETECTED_STRING:
                WideString_insert(self, start,'\"');
            case FSMW_CSV_SKIP_STRING:
            case FSMW_CSV_SKIP:{
                WideString_appendWChar(self, *str);
                break;
            }
            case FSMW_CSV_SHUTDOWN:{
                if (self->str[start] == '\"'){
                    WideString_appendWChar(self, '\"');
                }
                break;
            }       
            default: break;
        }
        ++str;
    }
}

void WideString_appendCsvFromated(WideString * self, const WideString * src){
    WideString_appendCsvFormatedWChars(self, src->str);
}



typedef enum {
    FSMR_CSV_LAUNCH,
    FSMR_CSV_SHUTDOWN,

    FSMR_CSV_START_STRING,
    FSMR_CSV_READ_STRING,
    FSMR_CSV_READ_ORDINARY,
    FSMR_CSV_QUERY_DQUOTE,
    FSMR_CSV_READ_DQUOTE,

    FSMR_CSV_UNTERMINATED_STRING,
    FSMR_CSV_ERROR
} FSMReadCsvValue;

static FSMReadCsvValue FSM_readCsvValue(FSMReadCsvValue currentState, wchar_t signal){
    switch (currentState){
        case FSMR_CSV_LAUNCH: {
            switch (signal){
                case '\n':
                case '\0':
                case ',' : return FSMR_CSV_SHUTDOWN;
                case '\"': return FSMR_CSV_START_STRING;
                default  : return FSMR_CSV_READ_ORDINARY;
            }
        }
        case FSMR_CSV_READ_DQUOTE:
        case FSMR_CSV_READ_STRING:
        case FSMR_CSV_START_STRING:{
            switch (signal){
                case '\0': return FSMR_CSV_UNTERMINATED_STRING;
                case '\"': return FSMR_CSV_QUERY_DQUOTE;
                default  : return FSMR_CSV_READ_STRING;
            }
        }
        case FSMR_CSV_READ_ORDINARY:{
            switch (signal){                
                case '\"': return FSMR_CSV_ERROR;
                case '\n':
                case ',' :
                case '\0': return FSMR_CSV_SHUTDOWN;
                default  : return FSMR_CSV_READ_ORDINARY;
            }
        }
        case FSMR_CSV_QUERY_DQUOTE:{
            switch (signal){
                case '\"': return FSMR_CSV_READ_DQUOTE;
                case ',' :
                case '\n':
                case '\0': return FSMR_CSV_SHUTDOWN;
                default:   return FSMR_CSV_ERROR;
            }
        }
        default: Debug_shutdown("FSM was supposed to be shutdown at this state");
    }
}

signed long long WideString_appendCsvDeformated(WideString * self, const WideString * src, size_t start){
    Debug_exists(self);
    Debug_exists(src);
    Debug_maxbound(start, src->len - 1);
    //
    FSMReadCsvValue state = FSMR_CSV_LAUNCH;
    const wchar_t * str = src->str + start;
    while (true){
        switch (state = FSM_readCsvValue(state, *str)){
            case FSMR_CSV_READ_ORDINARY:
            case FSMR_CSV_READ_STRING:  
            case FSMR_CSV_READ_DQUOTE:{
                WideString_appendWChar(self, *str);
                break;
            }
            case FSMR_CSV_UNTERMINATED_STRING:
                return CSV_WSTR_DEFORMAT_UTERMINATED_STRING;
            case FSMR_CSV_ERROR:
                return CSV_WSTR_DEFORMAT_ERROR;
            case FSMR_CSV_SHUTDOWN:{
                return str - src->str;
            }
            default: break;
        }
        ++str;
    }
}

void WideString_swap(WideString * self, size_t apple, size_t jack){
    Debug_exists(self);
    Debug_maxbound(apple, self->len - 1);
    Debug_maxbound(jack, self->len - 1);
    //
    wchar_t keeper = self->str[apple];
    self->str[apple] = self->str[jack];
    self->str[jack] = keeper;
}


void WideString_reverse(WideString * self){
    Debug_exists(self);
    //
    for (size_t i = 0, j = self->len - 1; i < j; ++i, --j){
        WideString_swap(self, i, j);
    }
}