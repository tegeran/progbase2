#include <queue.h>
#include <error.h>
#include <slnode.h>
#include <mem.h>

typedef struct __Queue {
    SLNode * head;
    SLNode * tail;
    size_t size;
} Queue;

Queue * Queue_new(void){
    Queue * newbie = Mem_malloc(sizeof(Queue));
    newbie->head = newbie->tail = NULL;
    newbie->size = 0;
    return newbie;
}

void Queue_free(Queue * self){
    Debug_exists(self);
    if (self->head){
        while (self->head->next != NULL){
            (void)SLNode_removeNext(self->head);
        }
        SLNode_free(self->head);
    }
    //
    free(self);
}

void Queue_freeWhole(Queue * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    if (self->head){
        while (self->head->next != NULL){
            destroy(SLNode_removeNext(self->head));
        }
        SLNode_freeWhole(self->head, destroy);
    }
    free(self);
}

void Queue_enqueue(Queue * self, void * val){
    Debug_exists(self);
    //
    if (self->size == 0){
        self->head = self->tail = SLNode_new(val);
    } else {
        SLNode_addAfter(self->tail, SLNode_new(val));
        self->tail = self->tail->next;
    }
    ++self->size;
}

void * Queue_dequeue(Queue * self){
    Debug_exists(self);
    Debug_suppose(self->size > 0, "An attempt to extract a value from an empty queue was made");
    //
    --self->size;
    SLNode * pervocursnik = self->head;
    self->head = pervocursnik->next;
    void * stepuha = pervocursnik->val;
    SLNode_free(pervocursnik);
    return stepuha;
}

void * Queue_peek(Queue * self){
    Debug_exists(self);
    Debug_suppose(self->size > 0, "An attempt to extract a value from an empty queue was made");
    //
    return SLNode_at(self->head);
}

bool Queue_isEmpty(Queue * self){
    Debug_exists(self);
    //
    return self->size == 0;
}

size_t Queue_size(Queue * self){
    Debug_exists(self);
    //
    return self->size;
}