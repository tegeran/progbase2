#include <tree.h>
#include <list.h>
#include <error.h>


Tree * Tree_new(void * val){
    Tree * newbie = malloc(sizeof(Tree));
    Error_checkHeap(newbie);
    //
    newbie->children = List_new();
    newbie->val = val;
    return newbie;
}

void Tree_free(Tree * self){
    Debug_exists(self);
    //
    List_free(self->children);
    free(self);
}


void Tree_freeWhole(Tree * self, Destructor destructor){
    Debug_exists(self);
    Debug_exists(destructor);
    //
    destructor(Tree_at(self));
    while (!List_isEmpty(self->children)){
        Tree_freeWhole(List_removeAt(self->children, 0), destructor);
    }
    List_free(self->children);
    free(self);
}

Tree * Tree_childAt(const Tree * self, size_t index){
    Debug_exists(self);
    Debug_suppose(!List_isEmpty(self->children), "An attempt to extact a child from the leaf node was made");
    Debug_maxbound(index, List_size(self->children) - 1);
    //
    return List_at(self->children, index);
}

Tree * Tree_setChild(Tree * self, size_t index, Tree * child){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->children) - 1);
    //
    return List_set(self->children, index, child);
}

void * Tree_at(const Tree * self){
    Debug_exists(self);
    //
    return self->val;
}

void * Tree_set(Tree * self, void * val){
    Debug_exists(self);
    //
    void * prev = self->val;
    self->val = val;
    return prev;
}


size_t Tree_childrenSize(const Tree * self){
    Debug_exists(self);
    //
    return List_size(self->children);
}

bool Tree_isLeaf(const Tree * self){
    Debug_exists(self);
    //
    return List_size(self->children) == 0;
}

Tree * Tree_removeChild(Tree * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->children) - 1);
    //
    return List_removeAt(self->children, index);
}

void Tree_insertChild(Tree * self, size_t index, Tree * child){
    Debug_exists(self);
    Debug_maxbound(index, List_size(self->children));
    //
    List_insert(self->children, index, child);
}

void Tree_addLastChild(Tree * self, Tree * child){
    Debug_exists(self);
    //
    List_addLast(self->children, child);
}

void Tree_addFirstChild(Tree * self, Tree * child){
    Debug_exists(self);
    //
    List_addFirst(self->children, child);
}

size_t Tree_size(const Tree * self){
    Debug_exists(self);
    //
    size_t size = 1;
    for (size_t i = 0; i < List_size(self->children); ++i){
        size += Tree_size(List_at(self->children, i));
    }
    return size;
}