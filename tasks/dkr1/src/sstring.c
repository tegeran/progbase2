#include <sstring.h>
#include <error.h>
#include <mem.h>
#include <wchar.h>
#include <fsm.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <floatingpoint.h>


#define String_INITIAL_CAPACITY 32

typedef struct __String{
    char * str;
    size_t len;
    size_t capacity;
} String;

const char * String_readOnly(const String * self){
    return self->str;
}

static void Private_ensureCapacity(String * self, size_t appending){
    if (self->len + appending >= self->capacity){
        do {
            self->capacity = self->capacity << 1;
        } while (self->len + appending >= self->capacity);

        self->str = Mem_reallocate(self->str, sizeof(char) * self->capacity); // mem reallocate performs a check
    }                                                                // for NULL pointer
}

String * String_toNewString(const char * chars){
    Debug_exists(chars);
    //
    String * newbie = String_new();
    String_append(newbie, chars);
    return newbie;
}


// adds a new object to the end of list (time complexity O(1))
void String_append(String * self, const char * chars){
    Debug_exists(self);
    Debug_exists(chars);
    //
    Private_ensureCapacity(self, strlen(chars));
    while (*chars != '\0'){
        self->str[self->len++] = *chars++;
    };
    self->str[self->len] = '\0';
}

void String_appendChar(String * self, char character){
    Debug_exists(self);
    if (character == '\0') return;
    //
    Private_ensureCapacity(self, 1);
    self->str[self->len++] = character;
    self->str[self->len] = '\0';
}

void String_concatenate(String * self, const String * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    char * cur = other->str;
    Private_ensureCapacity(self, other->len);
    while (*cur != '\0'){
        self->str[self->len++] = *cur++;
    }
    self->str[self->len] = '\0';
}

char * String_toNewChars(const String * self){
    Debug_exists(self);
    //
    char * newbie = Mem_malloc(sizeof(char) * (self->len + 1));
    const char * cur = self->str;
    for (size_t i = 0; *cur != '\0'; ++i){
        newbie[i] = *cur++;
    }
    newbie[self->len] = '\0';
    return newbie;
}


void String_print(const String * self){
    Debug_exists(self);
    //
    fputs(self->str, stdout);
}

void String_printWide(const String * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    const char * cur = self->str;
    while (*cur != '\0'){
        putwchar(*cur++);
    }
}

void String_writeTo(FILE * file, const String * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    fputs(self->str, file);
}

void String_writeToPath       (const char *   path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path, "w");
    fputs(self->str, file);
    fclose(file);
}
void String_writeToPathString (const String * path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path->str, "w");
    fputs(self->str, file);
    fclose(file);
}
void String_appendToPath      (const char *   path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path, "a");
    fputs(self->str, file);
    fclose(file);
}
void String_appendToPathString(const String * path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path->str, "a");
    fputs(self->str, file);
    fclose(file);
}

String * String_new(void){
    String * newbie = Mem_malloc(sizeof(String)); // mem malloc performs a check for NULL pointer
    newbie->capacity = String_INITIAL_CAPACITY;
    newbie->str = Mem_malloc(sizeof(char) * newbie->capacity);
    newbie->len = 0;
    newbie->str[0] = '\0';
    return newbie;
}

void String_free(String * self){
    Debug_exists(self);
    //
    free(self->str);
    free(self);
}

char String_at(const String * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1); // restrict interacting with '\0'
    //
    return self->str[index];
}

size_t String_length(const String * self){
    Debug_exists(self);
    //
    return self->len;
}

char String_set(String * self, size_t index, char value){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1); // restrict interacting with '\0'
    //
    char prev = self->str[index];
    self->str[index] = value;
    if (value == '\0'){
        self->len = index;
    }
    return prev;
}

void String_insert(String * self, size_t index, char value){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1);
    //
    if (value == '\0'){
        self->str[index] = '\0';
        self->len = index;
        return;
    }
    Private_ensureCapacity(self, 1);
    for (size_t i = self->len++; i > index; --i){
        self->str[i] = self->str[i - 1];
    }
    self->str[index] = value;
}

//
void String_removeBy(String * self, AnswerChar caseRemove){
    Debug_exists(self);
    Debug_exists(caseRemove);
    //
    for (size_t i = 0; i < self->len; ++i){
        if (caseRemove(self->str[i])){
            String_removeAt(self, i--);
        }
    }
}
void String_clear(String * self){
    Debug_exists(self);
    //
    self->len = 0;
    self->str[0] = '\0';
}   


char String_removeAt(String * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1);
    //
    char badboy = self->str[index];
    for (size_t i = index + 1; i <= self->len; ++i){
        self->str[i - 1] = self->str[i];
    }
    --self->len;
    return badboy;
}

void String_addStringFirst(String * self, const String * addingStr){
    Debug_exists(self);
    Debug_exists(addingStr);
    //
    if (addingStr->len == 0) return;
    size_t newLen = addingStr->len + self->len;   // query new length
    size_t i = self->len;                 
    Private_ensureCapacity(self, addingStr->len);
    while (i != 0){
        self->str[newLen--] = self->str[i--];
    }
    self->str[newLen] = self->str[0];     // transfering 0-th character
    const char * chars = addingStr->str;
    while (*chars){
        self->str[i++] = *chars++;        // i == 0 at this point
    }
    self->len += addingStr->len;
}


void String_addCharsFirst(String * self, const char * chars){
    Debug_exists(self);
    Debug_exists(chars);
    //
    if (*chars == '\0') return;
    size_t adding = strlen(chars);        
    size_t newLen = adding + self->len;   // query new length
    size_t i = self->len;                 
    Private_ensureCapacity(self, adding);
    while (i != 0){
        self->str[newLen--] = self->str[i--];
    }
    self->str[newLen] = self->str[0];     // transfering 0-th character
    while (*chars){
        self->str[i++] = *chars++;        // i == 0 at this point
    }
    self->len += adding;
}

void String_addCharFirst(String * self, char character){
    Debug_exists(self);
    //
    if (character == '\0'){
        self->str[0] = '\0';
        self->len = 0;
        return;
    }
    Private_ensureCapacity(self, 1);
    for (size_t i = ++self->len; i > 0; --i){
        self->str[i] = self->str[i - 1];
    }
    self->str[0] = character;
}

bool String_isEmpty(const String * self){
    Debug_exists(self);
    //
    return self->len == 0;
}

size_t String_memoryUsage(const String * self){
    Debug_exists(self);
    //
    return self->capacity * sizeof(char);
}



signed char String_compare(const String * self, const String * other){
    size_t i = 0;
    while (i < self->len && i < other->len){
        if (self->str[i] > other->str[i]){
            return -1;
        } else if (self->str[i] < other->str[i]){
            return 1;
        }
        ++i;
    }
    if (self->len == other->len) return 0;
    else return self->len > other->len ? -1 : 1;
}

String * String_toNewSubstring(const String * self, size_t left, size_t right){
    Debug_exists(self);
    Debug_maxbound(right, self->len - 1);
    Debug_maxbound(left, right);
    
    String * newbie = String_new();
    Private_ensureCapacity(newbie, right - left + 1);
    while (left <= right){
        newbie->str[newbie->len++] = self->str[left++];
    }
    newbie->str[newbie->len] = '\0';
    return newbie;
}

char * String_toNewSubchars(const String * self, size_t left, size_t right){
    Debug_exists(self);
    Debug_maxbound(right, self->len - 1);
    Debug_maxbound(left, right);
    //
    char * const newbie = Mem_malloc(sizeof(char) * (right - left + 2));
    size_t i = 0;
    while (left <= right){
        newbie[i++] = self->str[left++];
    }
    newbie[i] = '\0';
    return newbie;
}

void String_appendSubchars(String * self, const char * chars, size_t left, size_t right){
    Debug_exists(self);
    Debug_exists(chars);
    Debug_maxbound(left, right);
    //
    Private_ensureCapacity(self, right - left + 1);
    while (left <= right){
        self->str[self->len++] = chars[left++];
    }
    self->str[self->len] = '\0';
}

void String_appendSubstring(String * self, const String * pattern, size_t left, size_t right){
    Debug_exists(self);
    Debug_exists(pattern);
    Debug_maxbound(left, right);
    //
    String_appendSubchars(self, pattern->str, left, right);
}


List * String_toNewWords(const String * self, AnswerChar isWordLetter){
    Debug_exists(self);
    Debug_exists(isWordLetter);
    //
    List * list = List_new();
    char * cur = self->str;  
    FSMRead_Words state = FSM_WORDS_LAUNCH;
    while (state != FSM_WORDS_SHUTDOWN){   
        switch (state = FSM_readWord(state, *cur, (AnswerWChar)isWordLetter)){
            case FSM_WORDS_START_WORD: List_addLast(list, String_new());
            case FSM_WORDS_READ_WORD:  String_appendChar(List_at(list, List_size(list) - 1), *cur);
            default: break;
        }
        ++cur;
    }
    return list;
}

void String_appendNewWordsToList(const String * self, AnswerChar isWordLetter, List * list){
    Debug_exists(self);
    Debug_exists(isWordLetter);
    Debug_exists(list);
    //
    char * cur = self->str;  
    FSMRead_Words state = FSM_WORDS_LAUNCH;
    while (state != FSM_WORDS_SHUTDOWN){   
        switch (state = FSM_readWord(state, *cur, (AnswerWChar)isWordLetter)){
            case FSM_WORDS_START_WORD: List_addLast(list, String_new());
            case FSM_WORDS_READ_WORD:  String_appendChar(List_at(list, List_size(list) - 1), *cur);
            default: break;
        }
        ++cur;
    }
}


void String_rewrite(String * self, const char * chars){
    Debug_exists(self);
    Debug_exists(chars);
    //
    self->len = 0;
    Private_ensureCapacity(self, strlen(chars));
    while (*chars != '\0'){
        self->str[self->len++] = *chars++;
    }
    self->str[self->len] = '\0';
}

// https://stackoverflow.com/a/2624210/9259330
size_t String_hashify(const String * self){
    Debug_exists(self);
    //
    size_t hash = 7;
    const char * cur = self->str;
    while (*cur != '\0') {
        hash = hash*31 + *cur++;
    }
    return hash;
}

size_t Chars_hashify(const char * self){
    Debug_exists(self);
    //
    size_t hash = 7;
    while (*self != '\0') {
        hash = hash*31 + *self++;
    }
    return hash;
}

bool String_equals(const String * self, const String * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    return strcmp(self->str, other->str) == 0;
}

bool Chars_equals(const char * self, const char * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    return strcmp(self, other) == 0;
}


signed long long String_searchChar(const String * self, char template){
    Debug_exists(self);
    //
    for (size_t i = 0; self->str[i] != '\0'; ++i){
        if (template == self->str[i]) return i;
    }
    return -1;
}

signed long long String_searchSpecChar(const String * self, AnswerChar isTarget){
    Debug_exists(self);
    Debug_exists(isTarget);
    //
    for (size_t i = 0; self->str[i] != '\0'; ++i){
        if (isTarget(self->str[i])) return i;
    }
    return -1;
}


signed long long String_searchSubstring(const String * self, const String * pattern){
    return String_searchSubchars(self, pattern->str);
}


signed long long String_searchSubchars(const String * self, const char * pattern){
    Debug_exists(self);
    Debug_exists(pattern);
    //
    char * answered = strstr(self->str, pattern);
    return answered == NULL ? -1 : answered - self->str;
}

signed long long String_searchLastChar(const String * self, char template){
    Debug_exists(self);
    //
    for (size_t i = self->len - 1; i > 0; --i){
        if (template == self->str[i]) return i;
    }
    return template == self->str[0] ? 0 : -1;
}

signed long long String_searchLastSpecChar(const String * self, AnswerChar isTarget){
    Debug_exists(self);
    Debug_exists(isTarget);
    //
    for (size_t i = self->len - 1; i > 0; --i){
        if (isTarget(self->str[i])) return i;
    }
    return isTarget(self->str[0]) ? 0 : -1;
}


static bool Private_isNonSpace(char suspect){
    return !isspace(suspect);
}

static bool Private_isSpace(char suspect){
    return isspace(suspect);
}

void String_trim(String * self){
    Debug_exists(self);
    //
    signed long long firstNonSpace = String_searchSpecChar(self, (AnswerChar)Private_isNonSpace);
    switch (firstNonSpace) {
        case 0: {
            String_reduce(self, String_searchLastSpecChar(self, (AnswerChar)Private_isNonSpace) + 1);
            return;
        }
        default: {
            String_cut(self, firstNonSpace, String_searchLastSpecChar(self, (AnswerChar)Private_isNonSpace));
        }
        case -1: return;
    }
}

String * String_newCopyOf(const String * pattern){
    Debug_exists(pattern);
    //
    return String_toNewString(pattern->str);
}

void String_reduce(String * self, size_t newLength){
    Debug_exists(self);
    Debug_maxbound(newLength, self->len);
    //
    self->str[newLength] = '\0';
    self->len = newLength;
}

void String_cut(String * self, size_t left, size_t right){
    Debug_exists(self);
    Debug_maxbound(right, self->len - 1);
    Debug_maxbound(left, right);
    //
    if (left == 0){
        String_reduce(self, right + 1);
        return;
    }
    self->len = right - left + 1;
    size_t i = 0;
    while(left != right + 1){
        self->str[i++] = self->str[left++];
    }
    self->str[i] = '\0';
}

size_t String_readFrom(FILE * file, String * self){
    Debug_exists(file);
    Debug_exists(self);
    //
    self->len = 0;
    Private_ensureCapacity(self, Mem_getFileSize(file) / sizeof(char));

    self->len = fread(self->str, sizeof(char), self->capacity - 1, file);
    /*while ((ch = fgetc(file))!= EOF){
        self->str[self->len++] = ch;
    }*/
    self->str[self->len] = '\0';
    return self->len;
}

size_t String_readLine  (FILE * file, String * self){
    Debug_exists(file);
    Debug_exists(self);
    //
    self->len = 0;
    int ch;
    while ((ch = fgetc(file)) != '\n' && ch != EOF){
        String_appendChar(self, (char)ch);
    }
    return self->len;
}

size_t String_getLine(String * self){
    Debug_exists(self);
    //
    self->len = 0;
    int ch;
    while ((ch = getchar()) != '\n' && ch != EOF){
        String_appendChar(self, (char)ch);
    }
    return self->len;


}



size_t String_appendFrom(FILE * file, String * self){
    Debug_exists(file);
    Debug_exists(self);
    //
    Private_ensureCapacity(self, Mem_getFileSize(file) / sizeof(char));

    self->len += fread(self->str + self->len, sizeof(char), self->capacity - 1 - self->len, file);

    /*while ((ch = fgetc(file))!= EOF){
        self->str[self->len++] = ch;
    }*/
    self->str[self->len] = '\0';
    return self->len;
}


size_t String_readFromPath(const char * path, String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path, "rb");
    if (!file) return 0;
    (void)String_readFrom(file, self);
    fclose(file);
    return self->len;
}

size_t String_appendFromPath(const char * path, String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path, "rb");
    if (!file) return 0;
    (void)String_appendFrom(file, self);
    fclose(file);
    return self->len;
}

size_t String_readFromPathString(const String * path, String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path->str, "rb");
    if (!file) return 0;
    (void)String_readFrom(file, self);
    fclose(file);
    return self->len;
}

size_t String_appendFromPathString(const String * path, String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path->str, "rb");
    if (!file) return 0;
    (void)String_appendFrom(file, self);
    fclose(file);
    return self->len;
}


size_t String_writeFormat(String * self, char * fmt, ...){
    Debug_exists(self);
    Debug_exists(fmt);
    //
    va_list argPtr;
    va_list supporter;
    va_start(argPtr, fmt);
    va_copy(supporter, argPtr);
    
    size_t properLen = vsnprintf(self->str, self->capacity, fmt, argPtr);
    if (properLen >= self->capacity){
        do {
            self->capacity = self->capacity << 1;
        } while (properLen >= self->capacity);

        free(self->str);
        self->str = Mem_malloc(sizeof(char) * self->capacity);  
        properLen = vsnprintf(self->str, self->capacity, fmt, supporter);
        va_end(supporter);
    }   
    va_end(argPtr);
    self->len = properLen;
    return properLen;
}

size_t String_appendFormat(String * self, char * fmt, ...){
    Debug_exists(self);
    Debug_exists(fmt);
    //
    va_list argPtr;
    va_list supporter;
    va_start(argPtr, fmt);
    va_copy(supporter, argPtr);
    
    size_t properLen = vsnprintf(self->str + self->len, self->capacity - self->len, fmt, argPtr);
    if (properLen >= self->capacity - self->len){
        do {
            self->capacity = self->capacity << 1;
        } while (self->capacity - self->len <= properLen);

        self->str = Mem_reallocate(self->str, sizeof(char) * self->capacity);  
        vsnprintf(self->str  + self->len, self->capacity - self->len, fmt, supporter);
        va_end(supporter);
    }   
    va_end(argPtr);
    self->len += properLen;
    return self->len;
}

typedef enum {
    FSMW_CSV_LAUNCH,
    FSMW_CSV_SHUTDOWN,

    FSMW_CSV_SKIP_STRING,
    FSMW_CSV_SKIP,
    FSMW_CSV_DETECTED_STRING,
    FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE,
    FSMW_CSV_INSERT_DQUOTE
} FSMWriteCsvValue;

static FSMWriteCsvValue FSM_writeCsvValue(FSMWriteCsvValue currentState, char signal){
    if (signal == '\0') return FSMW_CSV_SHUTDOWN;
    switch (currentState){
        case FSMW_CSV_SKIP:
        case FSMW_CSV_LAUNCH:{  
            switch (signal){
                case '\"': return FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE;
                case '\n':
                case ',': return FSMW_CSV_DETECTED_STRING;
                default : return FSMW_CSV_SKIP;
            }
        }
        case FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE:
        case FSMW_CSV_INSERT_DQUOTE:
        case FSMW_CSV_DETECTED_STRING:
        case FSMW_CSV_SKIP_STRING:{
            return signal == '\"' ? FSMW_CSV_INSERT_DQUOTE : FSMW_CSV_SKIP_STRING;
        }
        default: Debug_shutdown("FSM was supposed to be shutdown at this state");
    }
}

void String_appendCsvFormatedChars(String * self, const char * str){
    Debug_exists(self);
    Debug_exists(str);
    if (str[0] == '\0') return;
    //
    FSMWriteCsvValue state = FSMW_CSV_LAUNCH;
    size_t start = self->len;
    while (state != FSMW_CSV_SHUTDOWN){
        switch (state = FSM_writeCsvValue(state, *str)){
            case FSMW_CSV_DETECTED_STRING_INSERT_DQUOTE:
                String_insert(self, start,'\"');
            case FSMW_CSV_INSERT_DQUOTE: {
                String_append(self, "\"\"");
                break;
            }
            case FSMW_CSV_DETECTED_STRING:
                String_insert(self, start,'\"');
            case FSMW_CSV_SKIP_STRING:
            case FSMW_CSV_SKIP:{
                String_appendChar(self, *str);
                break;
            }
            case FSMW_CSV_SHUTDOWN:{
                if (self->str[start] == '\"'){
                    String_appendChar(self, '\"');
                }
                break;
            }       
            default: break;
        }
        ++str;
    }
}

void String_appendCsvFromated(String * self, const String * src){
    String_appendCsvFormatedChars(self, src->str);
}



typedef enum {
    FSMR_CSV_LAUNCH,
    FSMR_CSV_SHUTDOWN,

    FSMR_CSV_START_STRING,
    FSMR_CSV_READ_STRING,
    FSMR_CSV_READ_ORDINARY,
    FSMR_CSV_QUERY_DQUOTE,
    FSMR_CSV_READ_DQUOTE,

    FSMR_CSV_UNTERMINATED_STRING,
    FSMR_CSV_ERROR
} FSMReadCsvValue;

static FSMReadCsvValue FSM_readCsvValue(FSMReadCsvValue currentState, char signal){
    switch (currentState){
        case FSMR_CSV_LAUNCH: {
            switch (signal){
                case '\n':
                case '\0':
                case ',' : return FSMR_CSV_SHUTDOWN;
                case '\"': return FSMR_CSV_START_STRING;
                default  : return FSMR_CSV_READ_ORDINARY;
            }
        }
        case FSMR_CSV_READ_DQUOTE:
        case FSMR_CSV_READ_STRING:
        case FSMR_CSV_START_STRING:{
            switch (signal){
                case '\0': return FSMR_CSV_UNTERMINATED_STRING;
                case '\"': return FSMR_CSV_QUERY_DQUOTE;
                default  : return FSMR_CSV_READ_STRING;
            }
        }
        case FSMR_CSV_READ_ORDINARY:{
            switch (signal){                
                case '\"': return FSMR_CSV_ERROR;
                case '\n':
                case ',' :
                case '\0': return FSMR_CSV_SHUTDOWN;
                default  : return FSMR_CSV_READ_ORDINARY;
            }
        }
        case FSMR_CSV_QUERY_DQUOTE:{
            switch (signal){
                case '\"': return FSMR_CSV_READ_DQUOTE;
                case ',' :
                case '\n':
                case '\0': return FSMR_CSV_SHUTDOWN;
                default:   return FSMR_CSV_ERROR;
            }
        }
        default: Debug_shutdown("FSM was supposed to be shutdown at this state");
    }
}

signed long long String_appendCsvDeformated(String * self, const String * src, size_t start){
    Debug_exists(self);
    Debug_exists(src);
    Debug_maxbound(start, src->len - 1);
    //
    FSMReadCsvValue state = FSMR_CSV_LAUNCH;
    const char * str = src->str + start;
    while (true){
        switch (state = FSM_readCsvValue(state, *str)){
            case FSMR_CSV_READ_ORDINARY:
            case FSMR_CSV_READ_STRING:  
            case FSMR_CSV_READ_DQUOTE:{
                String_appendChar(self, *str);
                break;
            }
            case FSMR_CSV_UNTERMINATED_STRING:
                return CSV_DEFORMAT_UTERMINATED_STRING;
            case FSMR_CSV_ERROR:
                return CSV_DEFORMAT_ERROR;
            case FSMR_CSV_SHUTDOWN:{
                return str - src->str;
            }
            default: break;
        }
        ++str;
    }
}

void String_swap(String * self, size_t apple, size_t jack){
    Debug_exists(self);
    Debug_maxbound(apple, self->len - 1);
    Debug_maxbound(jack, self->len - 1);
    //
    char keeper = self->str[apple];
    self->str[apple] = self->str[jack];
    self->str[jack] = keeper;
}


void String_reverse(String * self){
    Debug_exists(self);
    //
    for (size_t i = 0, j = self->len - 1; i < j; ++i, --j){
        String_swap(self, i, j);
    }
}

int String_scanFormat(String * self, char * fmt, ...){
    Debug_exists(self);
    Debug_exists(fmt);
    //
    va_list arg;
    va_start(arg, fmt);
    int res = vsscanf(self->str, fmt, arg);
    va_end(arg);
    return res;
}

signed long long String_searchOccurence   (const String * self, char template, size_t occurence){
    Debug_exists(self);
    //
    for (size_t i = 0; i < self->len; ++i){
        if (self->str[i] == template){
            if (--occurence == 0){
                return i;
            }
        }
    }
    return -1;
}


signed char String_compareLength(const String * self, const String * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    if (self->len > other->len) return -1;
    return self->len < other->len; 
}



void String_writeWideTo (FILE * file, const String * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    const char * ch = self->str;
    while (*ch){
        fputwc(*ch++, file);
    }
}
void String_writeWideToPath       (const char * path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path, "w");
    String_writeWideTo(file, self);
    fclose(file);
}
void String_writeWideToPathString (const String * path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path->str, "w");
    String_writeWideTo(file, self);
    fclose(file);
}
void String_appendWideToPath      (const char * path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path, "a");
    String_writeWideTo(file, self);
    fclose(file);
}
void String_appendWideToPathString(const String * path, const String * self){
    Debug_exists(path);
    Debug_exists(self);
    //
    FILE * file = fopen(path->str, "a");
    String_writeWideTo(file, self);
    fclose(file);
}


void String_appendNewNumbersToList(const String * self, List * dest){
    Debug_exists(self);
    Debug_exists(dest);
    const char * ch = self->str;
    do {
        while (!isdigit(*ch)){
            if (!*ch) return;
            ++ch;
        }
        Float * newbie = Float_new(atof(ch));
        List_addLast(dest, newbie);
        if (ch > self->str && ch[-1] == '-') {
            Float_set(List_atLast(dest), -Float_at(newbie));
        }
        FSMRead_Float state = FSM_FLOAT_LAUNCH;
        while ((state = FSM_readFloat(state, *ch)) != FSM_FLOAT_SHUTDOWN){
            ++ch;
        }
    } while (*ch);
}

void String_setCopy(String * self, const String * template){
    Debug_exists(self);
    Debug_exists(template);
    //
    self->len = 0;
    Private_ensureCapacity(self, template->len);
    const char * ch = template->str;
    while (*ch){
        self->str[self->len++] = *ch++;
    }
    self->str[self->len] = '\0';
}


void String_insertString(String * self, size_t index, const String * template){
    Debug_exists(self);
    Debug_exists(template);
    Debug_maxbound(index, self->len);
    //
    //
    if (template->len == 0) return;
    size_t newLen = template->len + self->len;   // query new length
    size_t i = self->len;                 
    Private_ensureCapacity(self, template->len);
    while (i != index){
        self->str[newLen--] = self->str[i--];
    }
    self->str[newLen] = self->str[index];    
    const char * chars = template->str;
    while (*chars){
        self->str[i++] = *chars++;        // i == 0 at this point
    }
    self->len += template->len;
}   

char String_delchar(String * self, size_t index){
    Debug_exists(self);
    Debug_maxbound(index, self->len - 1);
    //
    char * ch = self->str + index;
    char deleted = *ch;
    while (*ch++){
        ch[-1] = *ch;
    }
    --self->len;
    return deleted;
}