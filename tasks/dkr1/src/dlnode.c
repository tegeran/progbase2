#include <dlnode.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <error.h>



// Construcor and destructor
DLNode * DLNode_new(void * val){
    DLNode * newbie = malloc(sizeof(DLNode));
    Error_checkHeap(newbie);
    newbie->val = val;
    newbie->next = NULL;
    newbie->prev = NULL;
    return newbie;
}


void DLNode_free(DLNode * self){
    Debug_exists(self);
    //
    free(self);
}


void DLNode_freeWhole(DLNode * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    //
    destroy(self->val);
    DLNode_free(self);
}


// Getters
void * DLNode_at(DLNode * self){
    Debug_exists(self);
    //
    return self->val;
}


DLNode * DLNode_getNext(DLNode * self){
    Debug_exists(self);
    //
    return self->next;
}


DLNode * DLNode_getPrev(DLNode * self){
    Debug_exists(self);
    //
    return self->prev;
}

// Setters
void * DLNode_set(DLNode * self, void * newVal){
    Debug_exists(self);
    //
    void * oldVal = self->val;
    self->val = newVal;
    return oldVal;
}


DLNode * DLNode_setNext(DLNode * self, DLNode * next){
    Debug_exists(self);
    //
    DLNode * oldNext = self->next;
    self->next = next;
    return oldNext;
}

DLNode * DLNode_setPrev(DLNode * self, DLNode * prev){
    Debug_exists(self);
    //
    DLNode * oldPrev = self->prev;
    self->prev = prev;
    return oldPrev;
}

DLNode * DLNode_remove(DLNode * self){
    Debug_exists(self);
    //
    if (self->prev != NULL)
        self->prev->next = self->next;

    if (self->next != NULL)
        self->next->prev = self->prev;

    self->next = NULL;
    self->prev = NULL;
    return self;
}

DLNode * DLNode_ncRemove(DLNode * self){
    Debug_exists(self);
    //
    self->prev->next = self->next;
    self->next->prev = self->prev;
    self->next = NULL;
    self->prev = NULL;
    return self;
}

void DLNode_addAfter(DLNode * src, DLNode * newbie){
    Debug_exists(src);
    Debug_exists(newbie);
    //
    newbie->prev = src;
    newbie->next = src->next;
    if (src->next != NULL)
        src->next->prev = newbie;
    src->next = src;
}

void DLNode_ncAddAfter(DLNode * src, DLNode * newbie){
    Debug_exists(src);
    Debug_exists(newbie);
    //
    newbie->prev = src;
    newbie->next = src->next;
    src->next->prev = newbie;
    src->next = newbie;
}

void DLNode_addPrev(DLNode * src, DLNode * newbie){
    Debug_exists(src);
    Debug_exists(newbie);
    //
    newbie->next = src;
    newbie->prev = src->prev;
    if (src->prev != NULL)
        src->prev->next = newbie;
    src->prev = newbie;
}

void DLNode_ncAddPrev(DLNode * src, DLNode * newbie){
    Debug_exists(src);
    Debug_exists(newbie);
    //
    newbie->next = src;
    newbie->prev = src->prev;
    src->prev->next = newbie;
    src->prev = newbie;
}