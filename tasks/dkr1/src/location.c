#include <location.h>
#include <stdlib.h>
#include <error.h>


Location * Location_new(signed int y, signed int x){
    Location * newbie = malloc(sizeof(Location));
    *newbie = loc(y,x);
    Error_checkHeap(newbie);
    return newbie;
}
void Location_free(Location * self){
    Debug_exists(self);
    //
    free(self);
}

signed int Location_getY(Location * self){
    Debug_exists(self);
    //
    return self->y;
}

signed int Location_getX(Location * self){
    Debug_exists(self);
    //
    return self->x;
}

signed int Location_setY(Location * self, signed int y){
    Debug_exists(self);
    //
    signed int prev = self->y;
    self->y = y;
    return prev;
}

signed int Location_setX(Location * self, signed int x){
    Debug_exists(self);
    //
    signed int prev = self->x;
    self->x = x;
    return prev;
}

void Location_setYX(Location * self, signed int y, signed int x){
    Debug_exists(self);
    //
    self->y = y;
    self->x = x;
}

bool Location_equals(Location * self, Location * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    return self->x == other->x && self->y == other->y;
}