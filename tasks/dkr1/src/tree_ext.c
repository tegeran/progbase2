#include <tree_ext.h>
#include <tree.h>
#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>

#define NO_DEBUG_TRACE
#include <error.h>
#include <sstring.h>

static size_t printFancyFromStart(Tree * self, signed int y, signed int x, Printer print);
static size_t printFancyFromEnd(Tree * self, signed int y, signed int x, Printer print);

static void move(signed int y, signed int x){
    wprintf(L"\033[%i;%iH", y, x); // Thanks RA for conPos
}

// Developed by myself
void Tree_printFancy(Tree * self, Location loc, Printer print, bool fromStart){
    Debug_UTF8();
    Debug_exists(print);
    Debug_exists(self);
    Debug_minbound(loc.y, 1);
    Debug_minbound(loc.x, 1);
    //
    if (fromStart)
        move(loc.y + printFancyFromStart(self, loc.y, loc.x, print), 1);
    else move(loc.y + printFancyFromEnd(self, loc.y, loc.x, print) , 1);
}

static void printVLine(wchar_t token, unsigned short height, signed int y, signed int x){
    Debug_UTF8();
    while (height != 0){
        move(y++, x);
        putwchar(token);
        --height;
    }
}

// out of recursion may bring some optimization
const static unsigned int  INBRANCH_SIZE = sizeof(TREE_INBRANCH) / sizeof(TREE_INBRANCH[0]);
const static unsigned int OUTBRANCH_SIZE = sizeof(TREE_OUTBRANCH) / sizeof(TREE_OUTBRANCH[0]);

static size_t printFancyFromStart(Tree * self, signed int y, signed int x, Printer print){
    if (self == NULL) return 0L;
    //
    size_t descendants = y;
    move(y, x);
    print(Tree_at(self));
    if (!Tree_isLeaf(self)){ // in order not to experience unsigned value overflow in while()
        move(++y, x); 
        size_t i = 0;
        while(i < Tree_childrenSize(self) - 1){
            wprintf(TREE_INBRANCH);
            size_t curChildren = printFancyFromStart(Tree_childAt(self, i), y, x + INBRANCH_SIZE, print);
            printVLine(TREE_VLINE, curChildren , y + 1, x);
            move((y += curChildren), x);
            ++i;
        }
        wprintf(TREE_OUTBRANCH);
        y += printFancyFromStart(Tree_childAt(self, i), y, x + OUTBRANCH_SIZE, print) - 1;
    }   
    return y - descendants + 1;
}


static size_t printFancyFromEnd(Tree * self, signed int y, signed int x, Printer print){
    if (self == NULL) return 0L;
    //
    size_t descendants = y;
    move(y, x);
    print(Tree_at(self));
    if (!Tree_isLeaf(self)){ // in order not to experience unsigned value overflow in i = chidrenSize()
        move(++y, x); 
        size_t i = Tree_childrenSize(self) - 1;
        while(i != 0){
            wprintf(TREE_INBRANCH);
            size_t curChildren = printFancyFromEnd(Tree_childAt(self, i), y, x + INBRANCH_SIZE, print);
            printVLine(TREE_VLINE, curChildren , y + 1, x);
            move((y += curChildren), x);
            --i;
        }
        wprintf(TREE_OUTBRANCH);
        y += printFancyFromEnd(Tree_childAt(self, i), y, x + OUTBRANCH_SIZE, print) - 1;
    }   
    return y - descendants + 1;
}

static void printHLine(wchar_t token, unsigned int size){
    while(size != 0){
        putwchar(token);
        --size;
    }
}


signed long long Tree_linearSearchChild(Tree * self, void * key, Comparator compare){
    Debug_exists(self);
    Debug_exists(compare);
    //
    signed long long i = 0;
    while(i < Tree_childrenSize(self)){
        if (compare(Tree_at(Tree_childAt(self, i)), key) == 0)
            return i;
        else ++i;
    }
    return - i - 1;
}   


signed long long Tree_sortedLinearSearchChild(Tree * self, void * key, Comparator compare){
    Debug_exists(self);
    Debug_exists(compare);
    //
    signed long long i = 0;
    while(i < Tree_childrenSize(self)){
        switch (compare(key, Tree_at(Tree_childAt(self, i)))){
            case  0: {return i;}
            case -1: {return - i - 1;}
        }
        ++i;
    }
    return -Tree_childrenSize(self) - 1;
}

static void Private_writeFancyFromStart(Tree * self,
                                      String * dest,
                                StringWriter   _write,
                                      String * indent,
                                        bool   root, 
                                        bool   last);


static void Private_writeFancyFromEnd(Tree * self,
                                    String * dest,
                              StringWriter   _write,
                                    String * indent,
                                      bool   root, 
                                      bool   last);
            
void Tree_appendToString(Tree * self, String * dest, StringWriter _write, bool fromFirstChild){
    Debug_exists(self);
    Debug_exists(dest);
    Debug_exists(_write);
    //
    String * indent = String_new();
    if (fromFirstChild)
        Private_writeFancyFromStart(self, dest, _write, indent, true, true);
    else
        Private_writeFancyFromEnd(self, dest, _write, indent, true, true);
    String_free(indent);
}

// FOLLOWING FUCNTIONS ARE TOTALY STOLEN FROM RUSLAN GADYNIAK ANATOLIYOVYCH @rulline telegram

static const char * _TREE_VLINE = "┃ ";
static const char * _TREE_INBRANCH = "┣━";
static const char * _TREE_OUTBRANCH = "┗━";


static void Private_writeFancyFromStart(Tree * self,
                                      String * dest,
                                StringWriter   _write,
                                      String * indent,
                                        bool   root, 
                                        bool   last){
    String_concatenate(dest, indent);
    TRACE_CALL_LINE();
    String * localIndent = String_newCopyOf(indent);
    if (last){
        if (!root){
            String_append(dest, _TREE_OUTBRANCH);
            String_append(localIndent, "  ");
        } 
    //    
    } else {
        String_append(dest, _TREE_INBRANCH);
        String_append(localIndent, _TREE_VLINE);
    }
    _write(dest, Tree_at(self));
    String_appendChar(dest, '\n');
    for (size_t i = 0; i < Tree_childrenSize(self); ++i){
        Private_writeFancyFromStart(
            Tree_childAt(self, i),
            dest, 
            _write, 
            localIndent, 
            false,
            i == Tree_childrenSize(self) - 1
        );
    }
    String_free(localIndent);
}

static void Private_writeFancyFromEnd(Tree * self,
                                    String * dest,
                              StringWriter   _write,
                                    String * indent,
                                      bool   root, 
                                      bool   last){
    TRACE_CALL_LINE();
    String_concatenate(dest, indent);
    String * localIndent = String_newCopyOf(indent);
    if (last){
        if (!root){
            String_append(dest, _TREE_OUTBRANCH);
            String_append(localIndent, "  ");
        } 
    //    
    } else {
        String_append(dest, _TREE_INBRANCH);
        String_append(localIndent, _TREE_VLINE);
    }
    _write(dest, Tree_at(self));
    String_appendChar(dest, '\n');
    for (long long i = (long long)Tree_childrenSize(self) - 1; i >= 0; --i){
        Private_writeFancyFromEnd(
            Tree_childAt(self, i),
            dest, 
            _write, 
            localIndent, 
            false,
            i == 0
        );
    }
    String_free(localIndent);
}
