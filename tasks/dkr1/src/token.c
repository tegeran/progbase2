#include <token.h>
#include <error.h>
#include <trie.h>
#include <ctype.h>
#include <character.h> 
#include <string.h>
#include <cui.h>
#include <mem.h>


Lexeme * Lexeme_new(LexemeType type, String * name){
    Lexeme * newbie = Mem_malloc(sizeof(Lexeme));
    *newbie = (Lexeme){
        .type = type,
        .name = name
    };
    return newbie;
}


Token * Token_new(Lexeme * lex, Location loc){
    Debug_exists(lex);
    //
    Token * newbie = Mem_malloc(sizeof(Token));
    *newbie = (Token){
        .lex = lex,
        .loc = loc
    };
    return newbie;
}


void Lexeme_free(Lexeme * self){
    Debug_exists(self);
    //
    free(self);
}

void Lexeme_freeWhole(Lexeme * self){
    Debug_exists(self);
    //
    if (self->name != NULL)
        String_free(self->name);
    free(self);
}

void Token_free(Token * self){
    Debug_exists(self);
    //
    free(self);
}

void Token_freeWhole(Token * self){
    Debug_exists(self);
    Debug_exists(self->lex);
    //
    Lexeme_freeWhole(self->lex);
    free(self);
}

bool Lexeme_equals(Lexeme * self, Lexeme * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    if (self->type != other->type) return false;
    if (self->name != NULL) return String_compare(self->name, other->name) == 0;
    return other->name == NULL;
}

bool Token_equals(Token * self, Token * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    return Lexeme_equals(self->lex, other->lex) && Location_equals(&self->loc, &other->loc);
}

void LexemeType_print(LexemeType type){
    Debug_suppose(type < TOKEN_TOTAL_TOKEN_TYPES, "UNDEFINED TOKEN TYPE");
    //
    fputs(LEXEME_NAMES[type], stdout);
}

void LexemeType_appendTo(String * str, LexemeType type){
    Debug_suppose(type < TOKEN_TOTAL_TOKEN_TYPES, "UNDEFINED TOKEN TYPE");
    //
    String_append(str, LEXEME_NAMES[type]);
}

static void Private_LexemeType_writeTo(FILE * file, LexemeType type){
    Debug_exists(file);
    Debug_suppose(type < TOKEN_TOTAL_TOKEN_TYPES, "UNDEFINED TOKEN TYPE");
    //
    for (int i = 0; LEXEME_NAMES[type][i] != '\0'; ++i){
        fputc(LEXEME_NAMES[type][i], file);
    }
}

void LexemeType_printWide(LexemeType type){
    Debug_suppose(type < TOKEN_TOTAL_TOKEN_TYPES, "UNDEFINED TOKEN TYPE");
    Debug_UTF8();
    //
    for (int i = 0; LEXEME_NAMES[type][i] != '\0'; ++i){
        putwchar((int)LEXEME_NAMES[type][i]);
    }
}

// multi - multi line comment, single - single line comment
typedef enum FSMSpace{
    FSM_SPACE_LAUNCH,
    FSM_SPACE_TERMINATE,

    FSM_SPACE_SINGLE_SKIP,
    FSM_SPACE_SINGLE_SKIP_TAB,

    FSM_SPACE_MULTI_SKIP,
    FSM_SPACE_MULTI_SKIP_TAB,
    FSM_SPACE_MULTI_NEWLINE,
   
    FSM_SPACE_SKIP_SPACE,
    FSM_SPACE_SKIP_NEWLINE,
    FSM_SPACE_SKIP_TAB
} FSMSpace;

static FSMSpace FSM_readSpace(FSMSpace currentState, const char signal){
    switch(currentState){
        case FSM_SPACE_LAUNCH: 
        case FSM_SPACE_SKIP_SPACE: 
        case FSM_SPACE_SKIP_NEWLINE:
        case FSM_SPACE_SKIP_TAB:{
            switch (signal){
                case '\0': return FSM_SPACE_TERMINATE;
                case '\n': return FSM_SPACE_SKIP_NEWLINE;
                case  '#': return FSM_SPACE_SINGLE_SKIP;
                case  '~': return FSM_SPACE_MULTI_SKIP;
                case '\t': return FSM_SPACE_SKIP_TAB;
                default:{
                    if (isspace(signal)) 
                        return FSM_SPACE_SKIP_SPACE;
                    else
                        return FSM_SPACE_TERMINATE; 
                }
            }
        }
        case FSM_SPACE_SINGLE_SKIP: 
        case FSM_SPACE_SINGLE_SKIP_TAB:{
            switch (signal){
                case '\0': return FSM_SPACE_TERMINATE;
                case '\n': return FSM_SPACE_SKIP_NEWLINE;
                case '\t': return FSM_SPACE_SINGLE_SKIP_TAB;
                default:   return FSM_SPACE_SINGLE_SKIP;
            }
        }
        case FSM_SPACE_MULTI_SKIP: 
        case FSM_SPACE_MULTI_SKIP_TAB:
        case FSM_SPACE_MULTI_NEWLINE:{
            switch (signal){
                case '\0': return FSM_SPACE_TERMINATE;
                case '\n': return FSM_SPACE_MULTI_NEWLINE;
                case  '~': return FSM_SPACE_SKIP_SPACE;
                case '\t': return FSM_SPACE_MULTI_SKIP_TAB;
                default:   return FSM_SPACE_MULTI_SKIP;
                
            }
        }
        default: {
            Debug_shutdown("Statme machine was supposed to be shutdown at FSM_SPACE_SHUTDOWN state");
        }
    }


}

const char * Token_skipSpaces(const char * str, Location * update){
    Debug_exists(str);
    // skip spaces
    FSMSpace state = FSM_SPACE_LAUNCH;
    while (true){
        switch ((state = FSM_readSpace(state, *str))){
            case FSM_SPACE_SINGLE_SKIP:
            case FSM_SPACE_MULTI_SKIP:
            case FSM_SPACE_SKIP_SPACE:{
                ++update->x;
                break;
            }
            case FSM_SPACE_SINGLE_SKIP_TAB:
            case FSM_SPACE_MULTI_SKIP_TAB:
            case FSM_SPACE_SKIP_TAB:{
                update->x += 3;
                break;
            }
            case FSM_SPACE_MULTI_NEWLINE:
            case FSM_SPACE_SKIP_NEWLINE:{
                ++update->y;
                update->x = 1;
                break;
            }
            case FSM_SPACE_TERMINATE:{
                return str;
            }
            default: break;
        }
        ++str;
    }
}

bool Lexeme_isIdLetter(const char suspect){
    return isalnum(suspect) || suspect == '_';
}

bool Lexeme_isIdStart(const char suspect){
    return isalpha(suspect) || suspect == '_';
}

bool Token_isEscapeId(const char suspect){
    return
    suspect == 'n'  ||
    suspect == 't'  ||
    suspect == '\\' ||
    suspect == '\'' ||
    suspect == '\"' ||
    suspect == 'b'  ||
    suspect == 'v'  ||
    suspect == 'r'  ||
    suspect == 'f'  ||
    suspect == 'a'  ||
    suspect == '0';
}


// i don't know why, but compilator throws inmpicit declaration of isascii 
// despite I included <ctype.h>
#define isascii(c)	(((c) & ~0x7f) == 0)	/* If C is a 7 bit value.  */

bool Token_isChar(const char suspect){
    return isascii(suspect);
}

void Token_print(Token * self){
    Debug_exists(self);
    //
    putchar('{');
    LexemeType_print(self->lex->type);
    switch (self->lex->type){
        case LexemeType_IDENTIFIER:{
            fputs(", ", stdout);
            String_print(self->lex->name);
            break;
        }
        case LexemeType_STRING:{
            fputs(", \"", stdout);
            String_print(self->lex->name);
            fputs("\"", stdout);
            break;
        }
        case LexemeType_CHARACTER:{
            fputs(", \'", stdout);
            String_print(self->lex->name);
            fputs("\'", stdout);
            break;
        }
        case LexemeType_FLOAT:{
            fputs(", ", stdout);
            String_print(self->lex->name);
            break;
        }
        default: break;
    }
    putchar('}');
}


void Token_appendTo(String * str, Token * self){
    Debug_exists(str);
    Debug_exists(self);
    //
    String_append(str, "❲");
    LexemeType_appendTo(str, self->lex->type);
    switch (self->lex->type){
        case LexemeType_IDENTIFIER:{
            String_append(str, ", ");
            String_concatenate(str, self->lex->name);
            break;
        }
        case LexemeType_STRING:{
            String_append(str, ", \"");
            String_concatenate(str, self->lex->name);
            String_append(str, "\"");
            break;
        }
        case LexemeType_CHARACTER:{
            String_append(str, ", \'");
            String_concatenate(str, self->lex->name);
            String_append(str, "\'");
            break;
        }
        case LexemeType_FLOAT:{
            String_append(str, ", ");
            String_concatenate(str, self->lex->name);
            break;
        }
        default: break;
    }
    String_append(str, "❳");
}


void Token_printWide(Token * self){
    Debug_exists(self);
    //
    putwchar(L'❲');
    LexemeType_printWide(self->lex->type);
    switch (self->lex->type){
        case LexemeType_IDENTIFIER:{
            fputws(L", ", stdout);
            String_printWide(self->lex->name);
            break;
        }
        case LexemeType_STRING:{
            fputws(L", \"", stdout);
            String_printWide(self->lex->name);
            fputws(L"\"", stdout);
            break;
        }
        case LexemeType_CHARACTER:{
            fputws(L", \'", stdout);
            String_printWide(self->lex->name);
            fputws(L"\'", stdout);
            break;
        }
        case LexemeType_FLOAT:{
            fputws(L", ", stdout);
            String_printWide(self->lex->name);
            break;
        }
        default: break;
    }
    putwchar(L'❳');
}

void Token_writeTo(FILE * file, Token * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    fputs("❲", file);
    Private_LexemeType_writeTo(file, self->lex->type);
    switch (self->lex->type){
        case LexemeType_IDENTIFIER:{
            fputs(", ", file);
            String_writeTo(file, self->lex->name);
            break;
        }
        case LexemeType_STRING:{
            fputs(", \"", file);
            String_writeTo(file, self->lex->name);
            fputs("\"", file);
            break;
        }
        case LexemeType_CHARACTER:{
            fputs(", \'", file);
            String_writeTo(file, self->lex->name);
            fputs("\'", file);
            break;
        }
        case LexemeType_FLOAT:{
            fputs(", ", file);
            String_writeTo(file, self->lex->name);
            break;
        }
        default: break;
    }
    fputs("❳", file);
}

char Token_getEscapes(const char suspect){
    switch (suspect){
        case 'n':  return '\n';
        case 't':  return '\t';
        case '\\': return '\\';
        case '\'': return '\'';
        case '\"': return '\"';
        case 'b':  return '\b';
        case 'v':  return '\v';
        case 'r':  return '\r';
        case 'f':  return '\f';
        case 'a':  return '\a';
        case '0':  return '\0';
        default: return '-';
    }
}



bool Lexeme_isIdLike(const Lexeme * self){
    Debug_exists(self);
    //
    switch(self->type){
        case LexemeType_CASE:
        case LexemeType_FALSE:
        case LexemeType_NEW:
        case LexemeType_OTHERWISE:
        case LexemeType_TRUE:
        case LexemeType_UNDEFINED:
        case LexemeType_WHILE:
        case LexemeType_IDENTIFIER:
        case LexemeType_DEFINE:
        case LexemeType_RETURN:
        case LexemeType_ENDL:
            return true;
        default:
            return false;
    }
}           


LexemeType Token_typeAt(Token * self){
    Debug_exists(self);
    Debug_exists(self->lex);
    //
    return self->lex->type;
}
