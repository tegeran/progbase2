#include <std_function.h>
#include <mem.h>
#include <error.h>
#include <floatingpoint.h>
#include <sstring.h>
#include <math.h>
#include <stack.h>

Function * Function_new(StdRoutine func){
    Function * newbie = Mem_malloc(sizeof(Function));
    newbie->f = func;
    return newbie;
}
void Function_free(Function * self){
    Debug_exists(self);
    //
    free(self);
}
StdRoutine Function_at(Function * self){
    Debug_exists(self);
    //
    return self->f;
}


#define CHECK(...)                   __CHECK(-1L, __VA_ARGS__)
#define EXPECT(expr, errorMsg, ...) __EXPECT(-1L, expr, errorMsg, __VA_ARGS__)

static size_t getIndex(Calamity * prog, Tree * ast){
    DynamicType * entity = evaluate_new(prog, ast);
    CHECK(entity);
    EXPECT(DynamicType_isNumeric(entity), 
        "index designator results in non-numeric value", 
            ast, 
                entity);
    //
    signed long long index = DynamicType_typeAt(entity) == Type_CHARACTER 
        ? DynamicType_charAt(entity) 
        : (signed long long)DynamicType_floatAt(entity);
    DynamicType_free(entity);

    EXPECT(index >= 0, 
            "index designator results in a negative number", 
                ast);
    //
    return index;
}


#undef CHECK
#undef EXPECT  
#define CHECK(...)                   __CHECK(NULL, __VA_ARGS__)
#define EXPECT(expr, errorMsg, ...) __EXPECT(NULL, expr, errorMsg, __VA_ARGS__)

DynamicType * std_getter   (Calamity * prog, Tree * ast){
    Debug_suppose(Tree_childrenSize(ast) == 2, "getter called with not 2 arguments");
    Debug_suppose(AstNode_typeAt(ast->val) == AstNodeType_PRIM_ARGLIST, 
            "getter called at non prim_arglist");
    //
    DynamicType * entity = evaluate_new(prog, Tree_childAt(ast, 0));
    CHECK(entity);
    signed long long index = getIndex(prog, Tree_childAt(ast, 1));
    CHECK(index != -1, entity);   
    switch (DynamicType_typeAt(entity)){
        case Type_TYPE_LIST:{
            EXPECT(index < List_size(DynamicType_listAt(entity)),
                "getter array index out of bounds exception (segmentation fault)", 
                    ast,
                        entity);
            //
            DynamicType * at = List_removeAt(DynamicType_listAt(entity), index);
            DynamicType_free(entity);
            return at;
        }
        case Type_STRING:{
            EXPECT(index < String_length(DynamicType_stringAt(entity)),
                "getter string index out of bounds exception (segmentation fault)", 
                    ast,
                        entity);
            //
            DynamicType_setChar(entity, String_at(DynamicType_stringAt(entity), index));
            return entity;
        }
        default:{
            EXPECT(false, 
                "dimension designator exception (segmentation fault)", 
                    ast,
                        entity);
            //
            return entity;
        }
    }
}
DynamicType * std_ranger   (Calamity * prog, Tree * ast){
    Debug_suppose(Tree_childrenSize(ast) == 3, "ranger called with not 3 arguments");
    Debug_suppose(AstNode_typeAt(ast->val) == AstNodeType_PRIM_ARGLIST, 
            "ranger called at non prim_arglist");
    DynamicType * entity = evaluate_new(prog, Tree_childAt(ast, 0));
    CHECK(entity);

    signed long long leftIndex = getIndex(prog, Tree_childAt(ast, 1));
    CHECK(leftIndex != -1, entity);
    signed long long rightIndex = getIndex(prog, Tree_childAt(ast, 2));
    CHECK(rightIndex != -1, entity);

    EXPECT(rightIndex >= leftIndex, 
        "ranger right operand is less then left", 
            Tree_childAt(ast, 2), 
                entity);
    //
    switch (DynamicType_typeAt(entity)){
        case Type_TYPE_LIST:{
            EXPECT(rightIndex < List_size(DynamicType_listAt(entity)),
                "ranger array index out of bounds exception (segmentation fault)", 
                    Tree_childAt(ast, 2), 
                        entity);
            //
            List_cut(DynamicType_listAt(entity), leftIndex, rightIndex, (Destructor)DynamicType_free);
            return entity;
        }
        case Type_STRING:{
            EXPECT(rightIndex < String_length(DynamicType_stringAt(entity)),
                "ranger string index out of bounds exception (segmentation fault)", 
                    Tree_childAt(ast, 2), 
                        entity);
            //
            String_cut(DynamicType_stringAt(entity), leftIndex, rightIndex);
            return entity;
        }
        default:{
            EXPECT(false, 
                "dimension designator exception (segmentation fault)", 
                    Tree_childAt(ast, 2), 
                        entity);
            //
            return entity;
        }
    }
}


DynamicType * std_digify   (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) == 1, 
        "digify function is defined only for 1 argument: digify(entity)", 
            ast);
    //
    DynamicType * entity = evaluate_new(prog, Tree_childAt(argList, 0));
    CHECK(entity);
    switch (DynamicType_typeAt(entity)){
        case Type_STRING:{
            List * numbers = List_new();
            String_appendNewNumbersToList(DynamicType_stringAt(entity), numbers);
            DynamicType_setType(entity, Type_TYPE_LIST);
            if (!List_isEmpty(DynamicType_listAt(entity))){
                List_clear(DynamicType_listAt(entity), (Destructor)DynamicType_free);
            }
            while (!List_isEmpty(numbers)){
                DynamicType * floaat = DynamicType_new(Type_FLOAT);
                Float * num = List_removeAt(numbers, 0);
                DynamicType_setFloat(floaat, Float_at(num));
                List_addLast(DynamicType_listAt(entity), floaat);
                Float_free(num);
            }
            List_free(numbers);
            return entity;
        }
        case Type_CHARACTER:{
            DynamicType_setFloat(entity, DynamicType_charAt(entity));
            return entity;
        }
        case Type_UNDEFINED:
        case Type_FLOAT:{
            return entity;
        }
        case Type_BOOLEAN:{
            DynamicType_setFloat(entity, DynamicType_boolAt(entity) == true);
            return entity;
        }
        case Type_TYPE_LIST:{
            DynamicType_setUndef(entity);
            return entity;
        }
    }
    Debug_shutdown("std_digify unhandled case");
    return NULL;
}
static DynamicType * std_insertVar(Calamity * prog, Tree * argList){
    const char * name =  String_readOnly(AstNode_nameAt(Tree_childAt(argList, 0)->val));
    EXPECT(Dict_contains(prog->vars, name), 
        "use of undeclared variable identifier", 
            Tree_childAt(argList, 0));

    DynamicType * storage = Dict_get(prog->vars, name);
    signed long long index = getIndex(prog, Tree_childAt(argList, 1));
    CHECK(index != -1L);
    DynamicType * entity = evaluate_new(prog, Tree_childAt(argList, 2));
    CHECK(entity);
    switch (DynamicType_typeAt(storage)){
        case Type_TYPE_LIST:{
            EXPECT(index <= List_size(DynamicType_listAt(storage)),
                "insert array index out of bounds exception", 
                    Tree_childAt(argList, 1),
                        entity);

            List_insert(DynamicType_listAt(storage), index, entity);
            break;
        }
        case Type_STRING:{
            EXPECT(index <= String_length(DynamicType_stringAt(storage)),
                "insert string index out of bounds exception", 
                    Tree_childAt(argList, 1),
                        entity);
            //
            if (DynamicType_typeAt(entity) == Type_CHARACTER 
                    && DynamicType_charAt(entity) == '\0'){
                //
                DynamicType_free(entity);
                String_reduce(DynamicType_stringAt(storage), index);
                break;
            }
            String * str = String_new();
            DynamicType_appendToString(str, entity);
            String_insertString(DynamicType_stringAt(storage), index, str);
            String_free(str);
            DynamicType_free(entity);
            break;
        }
        default:{
            EXPECT(index == 0, 
                "insert index for single primitive type value exceeds 0", 
                    Tree_childAt(argList, 1),
                        entity); 
            DynamicType * oldStorage = DynamicType_new(Type_UNDEFINED);
            DynamicType_memCopy(oldStorage, storage);
            DynamicType_memLeakUndef(storage);
            DynamicType_setType(storage, Type_TYPE_LIST);
            List_addLast(DynamicType_listAt(storage), oldStorage);
            List_addLast(DynamicType_listAt(storage), entity);
            break;
        }
    }
    return DynamicType_newCopyOf(storage);
}
DynamicType * std_insert   (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) == 3, 
        "insert function is defined only for 3 arguments: insert(dest, index, entity)", 
            ast);
    //
    if (AstNode_typeAt(Tree_childAt(argList, 0)->val) == AstNodeType_IDENTIFIER
        && Tree_childrenSize(Tree_childAt(argList, 0)) == 0){
        return std_insertVar(prog, argList);
    }
    //   
    DynamicType * storage = evaluate_new(prog, Tree_childAt(argList, 0));
    CHECK(storage);

    signed long long index = getIndex(prog, Tree_childAt(argList, 1));
    CHECK(index != -1L,
            storage);

    DynamicType * entity = evaluate_new(prog, Tree_childAt(argList, 2));
    CHECK(entity,
            storage);

    switch (DynamicType_typeAt(storage)){
        case Type_TYPE_LIST:{
            EXPECT(index <= List_size(DynamicType_listAt(storage)),
                "insert array index out of bounds exception", 
                    Tree_childAt(argList, 1),
                        storage, entity);

            List_insert(DynamicType_listAt(storage), index, entity);
            break;
        }
        case Type_STRING:{
            EXPECT(index <= String_length(DynamicType_stringAt(storage)),
                "insert string index out of bounds exception", 
                    Tree_childAt(argList, 1),
                        storage, entity);

            if (DynamicType_typeAt(entity) == Type_CHARACTER 
                    && DynamicType_charAt(entity) == '\0'){
                //
                DynamicType_free(entity);
                String_reduce(DynamicType_stringAt(storage), index);
                break;
            }
            String * str = String_new();
            DynamicType_appendToString(str, entity);
            String_insertString(DynamicType_stringAt(storage), index, str);
            String_free(str);
            DynamicType_free(entity);
            break;
        }
        default:{
            EXPECT(index == 0, 
                "insert index for single primitive type value exceeds 0", 
                    Tree_childAt(argList, 1),
                        storage, entity); 
            //
            DynamicType * newList = DynamicType_new(Type_TYPE_LIST);
            List_addLast(DynamicType_listAt(newList), storage);
            List_addLast(DynamicType_listAt(newList), entity);
            return newList;
        }
    }
    return storage;    
}

DynamicType * std_print    (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) > 0, 
        "print function is defined only for 1 or more arguments: print(entity, ...)", 
            ast);
    //
    for (size_t i = 0; i < Tree_childrenSize(argList); ++i){
        DynamicType * entity = evaluate_new(prog, Tree_childAt(argList, i));
        CHECK(entity);
        DynamicType_print(entity);
        DynamicType_free(entity);
    }
    return DynamicType_new(Type_UNDEFINED);
}

DynamicType * std_write    (Calamity * prog, Tree * ast){
    Tree * arglist = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(arglist) > 1, 
        "write function is defined only for 2 or more arguments: write(filepath, entity ...)", 
            ast);
    //
    DynamicType * path = evaluate_new(prog, Tree_childAt(arglist, 0));
    CHECK(path);
    EXPECT(DynamicType_typeAt(path) == Type_STRING,
        "write function path parameter results in non-string type value",
            ast,
                path);
    //
    FILE * dest = fopen(String_readOnly(DynamicType_stringAt(path)), "w");
    DynamicType_free(path);
    for (size_t i = 1; i < Tree_childrenSize(arglist); ++i){
        DynamicType * entity = evaluate_new(prog, Tree_childAt(arglist, i));
        if (!entity){
            fclose(dest);
            return NULL;
        }
        DynamicType_writeTo(dest, entity);
        DynamicType_free(entity);
    }

    fclose(dest);
    return DynamicType_new(Type_UNDEFINED);
}

DynamicType * std_read     (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) <= 1, 
        "read function is defined only for 0 or 1 argument: read(file)", 
            ast);
    //
    if (Tree_childrenSize(argList) == 0){
        DynamicType * string = DynamicType_new(Type_STRING);
        String_getLine(DynamicType_stringAt(string));
        return string;
    } else {
        DynamicType * path = evaluate_new(prog, Tree_childAt(argList, 0));
        CHECK(path);
        EXPECT(DynamicType_typeAt(path) == Type_STRING, 
            "read argument rusults in non-string type value", 
                Tree_childAt(argList, 0),
                    path);
        //
        FILE * file = fopen(String_readOnly(DynamicType_stringAt(path)), "r");
        if (!file){
            DynamicType_setUndef(path);
            return path;
        }
        String_readFrom(file, DynamicType_stringAt(path));
        fclose(file);
        return path;
    }
}
static DynamicType * std_removeVar(Calamity * prog, Tree * argList){
    const char * name =  String_readOnly(AstNode_nameAt(Tree_childAt(argList, 0)->val));
    EXPECT(Dict_contains(prog->vars, name), 
        "use of undeclared variable identifier", 
            Tree_childAt(argList, 0));
    //
    DynamicType * storage = Dict_get(prog->vars, name);
    signed long long index = getIndex(prog, Tree_childAt(argList, 1));
    CHECK(index != -1L);
    switch (DynamicType_typeAt(storage)){
        case Type_TYPE_LIST:{
            EXPECT(index < List_size(DynamicType_listAt(storage)),
                "remove array index out of bounds exception", 
                    Tree_childAt(argList, 1));
            //
            return List_removeAt(DynamicType_listAt(storage), index);
        }
        case Type_STRING:{
            EXPECT(index < String_length(DynamicType_stringAt(storage)),
                "remove string index out of bounds exception", 
                    Tree_childAt(argList, 1));
            //
            DynamicType * ch = DynamicType_new(Type_CHARACTER);
            DynamicType_setChar(ch, String_delchar(DynamicType_stringAt(storage), index));
            return ch;
        }
        default:{
            EXPECT(index == 0, 
                "remove index for single primitive type value exceeds 0", 
                    Tree_childAt(argList, 1)); 
            //
            DynamicType * removed = DynamicType_new(Type_UNDEFINED);
            DynamicType_memDuplicate(removed, storage);
            DynamicType_memLeakUndef(storage);
            return removed;
        }
    }
}

DynamicType * std_remove   (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) == 2, 
        "remove function is defined only for 2 arguments: remove(entity, index)", 
            ast);
    //
    if (AstNode_typeAt(Tree_childAt(argList, 0)->val) == AstNodeType_IDENTIFIER
        && Tree_childrenSize(Tree_childAt(argList, 0)) == 0){
            return std_removeVar(prog, argList);
    }
    //    
    DynamicType * storage = evaluate_new(prog, Tree_childAt(argList, 0));
    CHECK(storage);
    signed long long index = getIndex(prog, Tree_childAt(argList, 1));
    CHECK(index != -1L, storage);
    switch (DynamicType_typeAt(storage)){
        case Type_TYPE_LIST:{
            EXPECT(index < List_size(DynamicType_listAt(storage)),
                "remove array index out of bounds exception", 
                    Tree_childAt(argList, 1),
                        storage);
            //
            DynamicType * removed = List_removeAt(DynamicType_listAt(storage), index);
            DynamicType_free(storage);
            return removed;
        }
        case Type_STRING:{
            EXPECT(index < String_length(DynamicType_stringAt(storage)),
                "remove string index out of bounds exception", 
                    Tree_childAt(argList, 1),
                        storage);
            //
            DynamicType_setChar(storage, String_at(DynamicType_stringAt(storage), index));
            return storage;
        }
        default:{
            EXPECT(index == 0, 
                "remove index for single primitive type value exceeds 0", 
                    Tree_childAt(argList, 1),
                        storage); 
            //
            return storage;
        }
    }
}

DynamicType * std_sizeof   (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) == 1, 
        "sizeof function is defined only for 1 argument: sizeof(entity)", 
            ast);
    //
    DynamicType * entity = evaluate_new(prog, Tree_childAt(argList, 0));
    CHECK(entity);
    switch (DynamicType_typeAt(entity)){
        case Type_TYPE_LIST:{
            DynamicType_setFloat(entity, List_size(DynamicType_listAt(entity)));
            break;
        }
        case Type_STRING:{
            DynamicType_setFloat(entity, String_length(DynamicType_stringAt(entity)));
            break;
        }
        default:{ // case single primitive bool char float or undefined
            DynamicType_setFloat(entity, 1);
            break;
        }
    }
    return entity;
}

DynamicType * std_sqrt     (Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) == 1, 
        "sqrt function is defined only for 1 numeric argument: sizeof(entity)", 
            ast);
    //
    DynamicType * arg = evaluate_new(prog, Tree_childAt(argList, 0));
    CHECK(arg);
    EXPECT(DynamicType_isNumeric(arg), 
        "sqrt argument results in non-numeric value", 
            Tree_childAt(argList, 0), 
                arg);
    switch (DynamicType_typeAt(arg)){
        case Type_CHARACTER:{
            EXPECT(DynamicType_charAt(arg) >= 0, 
                "sqrt argument results in a negative number",
                    Tree_childAt(argList, 0),
                        arg);
            DynamicType_setFloat(arg, sqrt((float)DynamicType_charAt(arg)));
            break;
        }
        default:{
            EXPECT(DynamicType_floatAt(arg) >= 0, 
                "sqrt argument results in a negative number",
                    Tree_childAt(argList, 0),
                        arg);
            DynamicType_setFloat(arg, sqrt(DynamicType_floatAt(arg)));
            break;
        }
    }
    return arg;
}

DynamicType * std_stringify(Calamity * prog, Tree * ast){
    Tree * argList = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(argList) >= 1, 
        "stringify function is defined only for 1 or more arguments: stringify(entity, ...)", 
            ast);
    //
    DynamicType * string = DynamicType_new(Type_STRING);
    for (size_t i = 0; i < Tree_childrenSize(argList); ++i){
        DynamicType * entity = evaluate_new(prog, Tree_childAt(argList, i));
        CHECK(entity, string);
        DynamicType_appendToString(DynamicType_stringAt(string), entity);
        DynamicType_free(entity);
    }
    return string;
}

DynamicType * std_typeof(Calamity * prog, Tree * ast){
    Tree * arglist = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(arglist) == 1, 
        "typeof function is defined only for 1 argument: typeof(entity)", 
            ast);
    //
    DynamicType * value = evaluate_new(prog, Tree_childAt(arglist, 0));
    CHECK(value);
    switch (DynamicType_typeAt(value)){
        case Type_BOOLEAN:{
            DynamicType_setType(value, Type_STRING);
            String_append(DynamicType_stringAt(value), "boolean");
            return value;
        }
        case Type_CHARACTER:{
            DynamicType_setType(value, Type_STRING);
            String_append(DynamicType_stringAt(value), "character");
            return value;
        }
        case Type_FLOAT:{
            DynamicType_setType(value, Type_STRING);
            String_append(DynamicType_stringAt(value), "number");
            return value;
        }
        case Type_STRING:{
            String_rewrite(DynamicType_stringAt(value), "string");
            return value;
        }
        case Type_TYPE_LIST:{
            DynamicType_setType(value, Type_STRING);
            String_append(DynamicType_stringAt(value), "array");
            return value;
        }
        case Type_UNDEFINED:{
            DynamicType_setType(value, Type_STRING);
            String_append(DynamicType_stringAt(value), "undefined");
            return value;
        }
    }
    Debug_shutdown("Unhandled type for typeof operator");
    return NULL;
}

DynamicType * std_escape(Calamity * prog, Tree * ast){
    Tree * arglist = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(arglist) == 1, 
        "escape function is defined only for 1 argument: escape(status)", 
            ast);
    DynamicType * status = evaluate_new(prog, Tree_childAt(arglist, 0));
    CHECK(status);
    prog->status = status;
    prog->state = ProgState_ESCAPE;
    return NULL;
}

DynamicType * Calamity_appendVarStorage(Calamity * prog, Tree * ast, DynamicType * entity){
    Stack * dimensions = Stack_new();
    Stack_push(dimensions, ast);
    while (AstNode_typeAt(Tree_at(Stack_peek(dimensions))) == AstNodeType_PRIM_ARGLIST){
        Stack_push(dimensions, Tree_childAt(Stack_peek(dimensions), 0));
    }
    Tree * idNode = Stack_pop(dimensions);
    if (AstNode_typeAt(Tree_at(idNode)) != AstNodeType_IDENTIFIER || !Tree_isLeaf(idNode)){
        Calamity_setError("can not append to an rvalue", ast->val, prog);
        goto exception;
    }
    const char * name = String_readOnly(AstNode_nameAt(Tree_at(idNode)));
    if (!Dict_contains(prog->vars, name)){
        Calamity_setError("use of undeclared variable identifier", idNode->val, prog);
        goto exception;
    }
    DynamicType * storage = Dict_get(prog->vars, name);
    while(!Stack_isEmpty(dimensions)){
        Tree * operator = Stack_pop(dimensions);
        if (Tree_childrenSize(operator) != 2){
            Calamity_setError("can not append to range (segmentation fault)", idNode->val, prog);
            goto exception;
        }
        signed long long index = getIndex(prog, Tree_childAt(operator, 1));
        if (index < 0) goto exception;
        switch (DynamicType_typeAt(storage)){
            case Type_TYPE_LIST:{
                if (index >= List_size(DynamicType_listAt(storage))){
                    Calamity_setError("getter array index out of bounds exception (segmentation fault)", idNode->val, prog);
                    goto exception;
                }
                storage = List_at(DynamicType_listAt(storage), index);
                break;
            }
            case Type_STRING:{
                if (!Stack_isEmpty(dimensions)){                   
                    Calamity_setError("dimension designator exception (segmentation fault)", idNode->val, prog);
                    goto exception;
                }
                if (index >= String_length(DynamicType_stringAt(storage))){
                    Calamity_setError("string index out of bounds exception", idNode->val, prog);
                    goto exception;
                }
                if (DynamicType_typeAt(entity) != Type_STRING){
                    String * stringified = String_new();
                    DynamicType_appendToString(stringified, entity);
                DynamicType_free(entity);
                    String_insertString(DynamicType_stringAt(storage), index, stringified);    
                    String_free(stringified);
                } else {
                    String_insertString(DynamicType_stringAt(storage), index, DynamicType_stringAt(entity));
                    DynamicType_free(entity);
                }
                Stack_free(dimensions);
                return DynamicType_newCopyOf(storage);
            }
            default:{
                // if (!Stack_isEmpty(dimensions)){
                    Calamity_setError("dimension designator exception (segmentation fault)", idNode->val, prog);
                    goto exception;
                // }
                break;
            }
        }
    }
    DynamicType_memAppend(storage, entity);
    Stack_free(dimensions);
    return DynamicType_newCopyOf(storage);
    exception:
    DynamicType_free(entity);
    Stack_free(dimensions);
    return NULL;
}

DynamicType * Calamity_setVarStorage(Calamity * prog, Tree * ast, DynamicType * entity){
    Stack * dimensions = Stack_new();
    Stack_push(dimensions, ast);
    while (AstNode_typeAt(Tree_at(Stack_peek(dimensions))) == AstNodeType_PRIM_ARGLIST){
        Stack_push(dimensions, Tree_childAt(Stack_peek(dimensions), 0));
    }
    Tree * idNode = Stack_pop(dimensions);
    if (AstNode_typeAt(Tree_at(idNode)) != AstNodeType_IDENTIFIER || !Tree_isLeaf(idNode)){
        Calamity_setError("can not assign to an rvalue", ast->val, prog);
        goto exception;
    }
    const char * name = String_readOnly(AstNode_nameAt(Tree_at(idNode)));
    if (!Dict_contains(prog->vars, name)){
        Calamity_setError("use of undeclared variable identifier", idNode->val, prog);
        goto exception;
    }
    DynamicType * storage = Dict_get(prog->vars, name);
    while(!Stack_isEmpty(dimensions)){
        Tree * operator = Stack_pop(dimensions);
        if (Tree_childrenSize(operator) != 2){
            Calamity_setError("can not assign to range (segmentation fault)", idNode->val, prog);
            goto exception;
        }
        signed long long index = getIndex(prog, Tree_childAt(operator, 1));
        if (index < 0) goto exception;
        switch (DynamicType_typeAt(storage)){
            case Type_TYPE_LIST:{
                if (index >= List_size(DynamicType_listAt(storage))){
                    Calamity_setError("getter array index out of bounds exception (segmentation fault)", idNode->val, prog);
                    goto exception;
                }
                storage = List_at(DynamicType_listAt(storage), index);
                break;
            }
            case Type_STRING:{
                if (!Stack_isEmpty(dimensions)){                   
                    Calamity_setError("dimension designator exception (segmentation fault)", idNode->val, prog);
                    goto exception;
                }
                if (index >= String_length(DynamicType_stringAt(storage))){
                    Calamity_setError("string index out of bounds exception", idNode->val, prog);
                    goto exception;
                }
                String * stringified = String_new();
                DynamicType_appendToString(stringified, entity);
                if (String_length(stringified) > 1){
                    Calamity_setError("can not expand value to a single character", ast->val, prog);
                    String_free(stringified);
                    goto exception;
                }
                DynamicType_free(entity);
                String_set(DynamicType_stringAt(storage), index, String_at(stringified, 0));
                String_free(stringified);
                Stack_free(dimensions);
                return DynamicType_newCopyOf(storage);
            }
            default:{
                // if (!Stack_isEmpty(dimensions)){
                    Calamity_setError("dimension designator exception (segmentation fault)", idNode->val, prog);
                    goto exception;
                // }
                break;
            }
        }
    }
    DynamicType_memReassign(storage, entity);
    Stack_free(dimensions);
    return DynamicType_newCopyOf(storage);
    exception:
    DynamicType_free(entity);
    Stack_free(dimensions);
    return NULL;
}

DynamicType * std_array    (Calamity * prog, Tree * ast){
    Tree * arglist = Tree_childAt(ast, 0);
    EXPECT(Tree_childrenSize(arglist) == 1, 
        "array function is defined only for 1 numeric argument: array(size)", 
            ast);
    DynamicType * size = evaluate_new(prog, Tree_childAt(arglist, 0));
    CHECK(size);
    EXPECT(DynamicType_isNumeric(size), 
        "size argument for array(size) results in non-numeric value", 
            Tree_childAt(arglist, 0), 
                size);
    //
    signed long long sizeNum = DynamicType_typeAt(size) == Type_CHARACTER 
        ? DynamicType_charAt(size) 
        : (signed long long)DynamicType_floatAt(size);
    
    EXPECT(sizeNum >= 0, 
            "size argument for array(size) results in a negative number", 
                Tree_childAt(arglist, 0),
                    size);
    //
    List * array = List_new();
    while (sizeNum != 0){
        List_addLast(array, DynamicType_new(Type_UNDEFINED));
        --sizeNum;
    }
    DynamicType_setList(size, array);
    return size;
}