#include <boolean.h>
#include <stdlib.h>
#include <error.h>
#include <stdio.h>
#include <wchar.h>

Boolean * Boolean_new(bool b){
    Boolean * newbie = malloc(sizeof(Boolean));
    Error_checkHeap(newbie);
    newbie->b = b;
    return newbie;
}

void Boolean_free(Boolean * self){
    Debug_exists(self);
    //
    free(self);
}

bool Boolean_at(Boolean * self){
    Debug_exists(self);
    //
    return self->b;
}

bool Boolean_set(Boolean * self, bool b){
    Debug_exists(self);
    //
    bool prev = self->b;
    self->b = b;
    return prev;
}

void Boolean_print(Boolean * self){
    Debug_exists(self);
    //
    if (self->b == true) fputs("true", stdout);
    else fputs("false", stdout);
}

void Boolean_printWide(Boolean * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    if (self->b == true) wprintf(L"true");
    else wprintf(L"false");
}

void Boolean_writeTo(FILE * file, Boolean * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    if (self->b == true) fprintf(file, "true");
    else fprintf(file, "false");
}

signed char Boolean_compare(Boolean * self, Boolean * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    if (self->b == other->b) return 0;
    else if (self->b) return -1;
    else return 1;
}