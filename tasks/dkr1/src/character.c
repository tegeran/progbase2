#include <character.h>
#include <error.h>
#include <wchar.h>
#include <ctype.h>

Character * Character_new(char ch){
    Character * newbie = malloc(sizeof(Character));
    Error_checkHeap(newbie);
    //
    newbie->ch = ch;
    return newbie;
}

void Character_free(Character * self){
    Debug_exists(self);
    //
    free(self);
}

char Character_at(Character * self){
    Debug_exists(self);
    //
    return self->ch;
}

char Character_set(Character * self, char ch){
    Debug_exists(self);
    //
    char prev = self->ch;
    self->ch = ch;
    return prev;
}

void Character_print(Character * self){
    Debug_exists(self);
    //
    putchar(self->ch);
}

void Character_printWide(Character * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    putwchar((wchar_t)self->ch);
}

signed char Character_compare(Character * left, Character * right){
    Debug_exists(left);
    Debug_exists(right);
    //
    if (left->ch > right->ch) return -1;
    else return right->ch > left->ch;
}

void Character_writeTo(FILE * file, Character * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    fputc(self->ch, file);
    fflush(file);
}

bool Character_isVowel(Character * self){
    Debug_exists(self);
    //
    return Char_isVowel(self->ch);    
}

bool Char_isVowel(char self){
    switch (self){
        case 'A':
        case 'a':
        case 'E':
        case 'e':
        case 'I':
        case 'i':
        case 'O':
        case 'o':
        case 'U':
        case 'u':
        case 'Y':
        case 'y':
            return true;
        default:
            return false;
    } 
}

bool Character_isConsonant(Character * self){
    Debug_exists(self);
    //
    return Char_isConsonant(self->ch);
}

bool Char_isConsonant(char self){
    return isalpha(self) && !Char_isVowel(self);
}