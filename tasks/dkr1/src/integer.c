#include <integer.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>

#include <error.h>

void Integer_initRandomness(unsigned int seed){
    srand(seed);    
}


Integer * Integer_new(int val){
    Integer * newbie = malloc(sizeof(Integer));
    Error_checkHeap(newbie);

    newbie->val = val;
    return newbie;
}


Integer * Integer_newRandom(int min, int max){
    Debug_suppose(min <= max, "Illegal random range bounds");
    // 
    return Integer_new((rand() % (max - min + 1)) + min);
}

void Integer_free(Integer * self){
    Debug_exists(self);
    //
    free(self);
}

void Integer_print(Integer * self){
    Debug_exists(self);
    //
    printf("%i", self->val);
}

void Integer_printWide(Integer * self){
    Debug_UTF8();
    wprintf(L"%i", self->val);
}

int Integer_set(Integer * self, int val){
    Debug_exists(self);
    //
    int keeper = self->val;
    self->val = val;
    return keeper;
}

int Integer_at(Integer * self){
    Debug_exists(self);
    //
    return self->val;
}

bool Integer_isNegative(Integer * self){
    return Integer_at(self) < 0;
}


signed char Integer_compare(Integer * left, Integer * right){
    Debug_exists(left);
    Debug_exists(right);
    //
    if (left->val == right->val) return 0; 
    return left->val > right->val ? -1 : 1;
}