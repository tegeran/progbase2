#include <dict.h>
#include <error.h>
#include <mem.h>
#include <hash_table.h>
#include <sstring.h>

typedef struct __Dict {
    HashTable * htable;
} Dict;

Dict * Dict_new(void){
    Dict * newbie = Mem_malloc(sizeof(Dict));
    newbie->htable = HashTable_new((Hashifier)Chars_hashify, 
                                   (Equaler)Chars_equals);
    return newbie;
}

void Dict_free(Dict * self){
    Debug_exists(self);
    //
    HashTable_free(self->htable);
    free(self);
}

void Dict_freeWhole(Dict * self, Destructor destroy){
    Debug_exists(self);
    //
    HashTable_freeWhole(self->htable, destroy);
    free(self);
}

void Dict_set(Dict * self, const char * key, void * value){
    Debug_exists(self);
    Debug_exists(key);
    //
    HashTable_insert(self->htable, key, value);
}

bool Dict_contains(Dict * self, const char * key){
    Debug_exists(self);
    Debug_exists(key);
    //
    return HashTable_contains(self->htable, key);
}

void * Dict_get(Dict * self, const char * key){
    Debug_exists(self);
    Debug_exists(key);
    //
    return HashTable_get(self->htable, key);
}

void * Dict_remove(Dict * self, const char * key){
    Debug_exists(self);
    Debug_exists(key);
    //
    return HashTable_remove(self->htable, key);
}

void Dict_keys(Dict * self, List * keys){
    Debug_exists(self);
    Debug_exists(keys);
    //
    HashTable_getKeys(self->htable, keys);
}

size_t Dict_count(Dict * self){
    Debug_exists(self);
    //
    return HashTable_size(self->htable);
}