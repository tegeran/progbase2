#include <lexer.h>
#include <trie.h>
#include <error.h>
#include <token.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdarg.h>
#include <sstring.h>
#include <list_ext.h>
#include <mem.h>
#include <wchar.h>
#include <cui.h>
#include <fsm.h>
#include <dict.h>


static void Private_printError(Location loc, const char * input, const wchar_t * fmt, ...);
static void Private_charUpdateLocation(const char suspect, Location * update){
    if (suspect == '\n'){
        ++update->y;
        update->x = 1;
    } else {
        ++update->x;
    }
}

// in sake of memory optimization
typedef struct __Statistic{
    unsigned int stat;
    Lexeme * lex;
} Statistic;

static Statistic * Statistic_new(Lexeme * lexeme){
    Statistic * newbie = Mem_malloc(sizeof(Statistic));
    newbie->stat = 0;
    newbie->lex = lexeme;
    return newbie;
}

static void Statistic_freeUnusedKeyword(Statistic * self){
    Debug_exists(self);
    Debug_suppose(self->lex->type != LexemeType_STRING, "Strings are not supposed to be stored on a trie");
    Debug_suppose(self->lex->type != LexemeType_CHARACTER, "Characters are not supposed to be stored on a trie");
    Debug_suppose(self->lex->type != LexemeType_FLOAT, "Floats are not supposed to be stored on a trie");
    //
    if (self->lex->type != LexemeType_IDENTIFIER && self->stat == 0){
        Lexeme_free(self->lex);
    }
    free(self);
}

LexerStatus Lexer_splitTokens(const register char * input, register List * tokens){
    Debug_exists(tokens);
    Debug_exists(input);
    //
    // all kewords and indetifiers tokens are stroed on a prefix tree
    // so all statTrie and tokens tokens in list share common memory on this tree

    Trie * statTrie = Trie_new(); // manual at https://www.youtube.com/watch?v=zIjfhVPRZCg&t=188s

    // inserting ketwords to the trie
    for (unsigned int i = 0; i < TOKEN_TOTAL_KEYWORDS; ++i){  
        Trie_insert(
            statTrie, 
            LEXEME_NAMES[i], 
            Statistic_new(
                Lexeme_new(
                    (LexemeType)i, 
                    NULL
                )
            )
        );
    }
    Location loc = loc(1, 1);          // token text location memorizing
    input = Token_skipSpaces(input, &loc);
    while (*input != '\0'){
        const char * scout = input;    // goodboy

        // first of all query current word if it was already written to trie

        Statistic * stat = NULL;
        if ((stat = Trie_query(statTrie, input, &scout)) != NULL 
            && imply(Lexeme_isIdLike(stat->lex), !Lexeme_isIdLetter(scout[1]))){ // imply: A -> B == !A && B
                // ^^^  For example identifier otherwise_ = 0;
            ++stat->stat; // increment its statistic
            List_addLast(tokens, Token_new(stat->lex, loc));
            loc.x += (size_t)(scout - input) + 1;
            input = scout + 1;

        // case it is a literal or new identifier handle it by finite state machine
        } else if (Lexeme_isIdStart(*input)){

            Token * soldier = Token_new(Lexeme_new(LexemeType_IDENTIFIER, String_new()), loc);
            String_appendChar(soldier->lex->name, *input++);
            ++loc.x;

            while (Lexeme_isIdLetter(*input)){
                String_appendChar(soldier->lex->name, *input);
                ++loc.x;
                ++input;
            }
            Trie_insert(statTrie, String_readOnly(soldier->lex->name), Statistic_new(soldier->lex));
            List_addLast(tokens, soldier);

        } else if (*input == TOKEN_LITERAL_START_CHAR){

            Token * soldier = Token_new(Lexeme_new(LexemeType_CHARACTER, String_new()), loc);
            ++input; // skip '
            ++loc.x;

            FSMRead_Character state = FSM_CHAR_LAUNCH;
            do {
                switch (state = FSM_readCharacter(state, *input)){
                    case FSM_CHAR_READ_CHAR:{
                        String_appendChar(soldier->lex->name, *input);
                        break;
                    }
                    case FSM_CHAR_START_ESCAPES: break;
                    case FSM_CHAR_READ_ESCAPES:{
                        String_appendChar(soldier->lex->name, Token_getEscapes(*input));
                        break;
                    }
                    case FSM_CHAR_SHUTDOWN:{
                        List_addLast(tokens, soldier);
                        break;
                    }
                    case FSM_CHAR_UNHANDLED_CHAR: {
                        Token_freeWhole(soldier);
                        Private_printError(loc, input, L"Unhandled character detected '%c'", *input);
                        goto unhandled_token;
                    }
                    case FSM_CHAR_UNTERMINATED_CHAR:{
                        Token_freeWhole(soldier);
                        Private_printError(loc, input, L"Unterminated character literal detected '%c'", *input);
                        goto unterminated_literal;
                    }
                    case FSM_CHAR_INVALID_ESCAPES:{
                        Token_freeWhole(soldier);
                        Private_printError(loc, input, L"Unhandled escape sequnce identifier detected '%c'", *input);
                        goto unhandled_token;
                    }
                    default: break;
                }
                Private_charUpdateLocation(*input++, &loc);
            } while (state != FSM_CHAR_SHUTDOWN);

        } else if (*input == TOKEN_LITERAL_START_STRING){

            Token * soldier = Token_new(Lexeme_new(LexemeType_STRING, String_new()), loc);
            ++input; // skip "
            ++loc.x;

            FSMRead_String state = FSM_STRING_LAUNCH;
            do {
                switch (state = FSM_readString(state, *input)){
                    case FSM_STRING_READ_STRING:{
                        String_appendChar(soldier->lex->name, *input);
                        break;
                    }
                    case FSM_STRING_READ_ESCAPES:{
                        String_appendChar(soldier->lex->name, Token_getEscapes(*input));
                        break;
                    }
                    case FSM_STRING_SHUTDOWN:{
                        List_addLast(tokens, soldier);
                        break;
                    }
                    case FSM_STRING_UNHANDLED_CHAR: {
                        Token_freeWhole(soldier);
                        Private_printError(loc, input, L"Unhandled character detected '%c'", *input);
                        goto unhandled_token;
                    }
                    case FSM_STRING_UNTERMINATED_STRING:{
                        Private_printError(
                            soldier->loc, 
                            input - String_length(soldier->lex->name) - 1, 
                            L"Unterminated string literal detected '%c'", 
                            *input
                        );
                        Token_freeWhole(soldier);
                        goto unterminated_literal;
                    }
                    case FSM_STRING_INVALID_ESCAPES:{
                        Token_freeWhole(soldier);
                        Private_printError(loc, input, L"Unhandled escape sequnce identifier detected '%c'", *input);
                        goto unhandled_token;
                    }
                    default: break;
                }
                Private_charUpdateLocation(*input++, &loc);
            } while (state != FSM_STRING_SHUTDOWN);

        } else if (isdigit(*input)){

            Token * soldier = Token_new(Lexeme_new(LexemeType_FLOAT, String_new()), loc);
            String_appendChar(soldier->lex->name, *input++);
            ++loc.x;
            
            FSMRead_Float state = FSM_FLOAT_LAUNCH;
            do {
                switch(state = FSM_readFloat(state, *input)){
                    case FSM_FLOAT_UNTERMINATED:{
                        if (*input != '.'){     // faced [4...] issue (4. is recognized as unterminated float)
                            Token_freeWhole(soldier);
                            --loc.x;
                            --input;
                            Private_printError(loc, input, L"digit is expected to finish floating point number mantissa");
                            goto unterminated_literal;
                        } else {
                            --input;
                            --loc.x;
                            state = FSM_FLOAT_SHUTDOWN;
                            break;
                        }
                    }
                    default: {
                        if (state != FSM_FLOAT_SHUTDOWN){
                            String_appendChar(soldier->lex->name, *input);
                            ++input;
                            ++loc.x;
                        };
                    }
                }
            } while (state != FSM_FLOAT_SHUTDOWN);
            List_addLast(tokens, soldier);

        } else {
            Private_printError(loc, input, L"Unhandled character detected");
            goto unhandled_token;
        }
        input = Token_skipSpaces(input, &loc);
    }
    // freeing trie and unused tokens on it
    Trie_free(statTrie, (Destructor)Statistic_freeUnusedKeyword);
    statTrie = NULL;
    return LEXER_SUCCESS;
    
    // aborting procedure
    unhandled_token:
    Trie_free(statTrie, (Destructor)Statistic_freeUnusedKeyword);
    statTrie = NULL;
    Lexer_clearTokens(tokens);
    return LEXER_ERROR_UNHANDLED_TOKEN;

    unterminated_literal:
    Trie_free(statTrie, (Destructor)Statistic_freeUnusedKeyword);
    statTrie = NULL;
    Lexer_clearTokens(tokens);
    return LEXER_ERROR_UNTERMINATED_LITERAL;
}

void Lexer_clearTokens(List * tokens){
    Debug_exists(tokens);
    //
    // near quadratic time complexity algorithm
    // the problem is: tokens may share common memory in the list

    while (!List_isEmpty(tokens)){
        for (size_t j = 1; j < List_size(tokens); ++j){
            if (((Token *)List_at(tokens, j))->lex == ((Token *)List_at(tokens, 0))->lex){
                Token_free(List_removeAt(tokens, j));
                --j;
            }
        }
        Token_freeWhole(List_removeAt(tokens, 0));
    }
}

void Lexer_printTokens(List * tokens){
    Debug_exists(tokens);
    if (List_isEmpty(tokens)) return;
    //
    int prev = ((Token *)List_at(tokens, 0))->loc.y;
    for (size_t i = 0; i < List_size(tokens); ++i){
        if (prev < ((Token *)List_at(tokens, i))->loc.y){
            prev = ((Token *)List_at(tokens, i))->loc.y;
            putchar('\n');
        }
        Token_print(List_at(tokens, i));
    }
}

void Lexer_printTokensWide(List * tokens){
    Debug_exists(tokens);
    Debug_UTF8();
    //
    if (List_isEmpty(tokens)) return;
    //
    int prev = ((Token *)List_at(tokens, 0))->loc.y;
    for (size_t i = 0; i < List_size(tokens); ++i){
        if (prev < ((Token *)List_at(tokens, i))->loc.y){
            prev = ((Token *)List_at(tokens, i))->loc.y;
            putwchar(L'\n');
        }
        Token_printWide(List_at(tokens, i));
    }
}

void Lexer_writeTokensTo(const char * filePath, List * tokens){
    Debug_exists(filePath);
    Debug_exists(tokens);
    //
    FILE * file = fopen(filePath, "w");// writing mode never returns NULL, but why not
    if (!file) return;

    int prev = ((Token *)List_at(tokens, 0))->loc.y;
    for (size_t i = 0; i < List_size(tokens); ++i){
        if (prev < ((Token *)List_at(tokens, i))->loc.y){
            prev = ((Token *)List_at(tokens, i))->loc.y;
            fputc('\n', file);
        }
        Token_writeTo(file, List_at(tokens, i));
        fputc(' ', file);
    }
    fclose (file);
}

static void Private_printError(Location loc, const char * input, const wchar_t * fmt, ...){
    Debug_exists(fmt);
    //
    Cui_setFgColorWideOf(stderr, 500); // 100 - red digit, 010 - green digit, 001 - blue digit

    fwprintf(stderr, L"Unresolved compilation problem (%i : %i)\n", loc.y, loc.x);
    va_list argPtr;
    va_start(argPtr, fmt);
    vfwprintf(stderr, fmt, argPtr);
    va_end(argPtr);
    fwprintf(stderr, L"\n");
    unsigned int issue = loc.x;
    while (loc.x != 1){
        --loc.x;
        --input;
    }
    fwprintf(stderr, L"line %i: \n", loc.y);
    Cui_setFgColorWideOf(stderr, 5);
    while (loc.x < issue){
        fputwc(*input, stderr);
        ++loc.x;
        ++input;
    }
    Cui_setFgColorWideOf(stderr, 500);
    fputwc(*input, stderr);
    Cui_setFgColorWideOf(stderr, 5);
    ++input;
    while(*input != '\0' && *input != '\n'){
        fputwc(*input, stderr);
        ++input;
    }
    fputwc(L'\n', stderr);
    for (int i = 1; i < issue; ++i){
        fputwc(L' ', stderr);
    }
    Cui_setFgColorWideOf(stderr, 500);
    fwprintf(stderr, L"🠹\n");
}