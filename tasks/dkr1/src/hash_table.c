#include <hash_table.h>
#include <slnode.h>
#include <mem.h>
#include <error.h>

#define INITIAL_CAPACITY 47
#define OVERLOADING_COEFICIENT 0.75f


typedef struct __HashNode {
    const void * key;
    void * value;
} HashNode;


static HashNode * HashNode_new(const void * key, void * value){
    HashNode * newbie = Mem_malloc(sizeof(HashNode));
    *newbie = (HashNode){.key = key, .value = value};
    return newbie;
}

static void HashNode_free(HashNode * self){
    Debug_exists(self);
    //
    free(self);
}

static void HashNode_freeWhole(HashNode * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    //
    destroy(self->value);
    free(self);
}

static const void * HashNode_keyAt(HashNode * self){
    Debug_exists(self);
    //
    return self->key;
}
static void * HashNode_valueAt(HashNode * self){
    Debug_exists(self);
    //
    return self->value;
}

static const void * HashNode_setKey(HashNode * self, const void * key){
    Debug_exists(self);
    //
    const void * prev = self->key;
    self->key = key;
    return prev;
}

static void * HashNode_setValue(HashNode * self, void * value){
    Debug_exists(self);
    //
    void * prev = self->value;
    self->value = value;
    return prev;
}
static void Private_freeHashNodes(SLNode * self){
    Debug_exists(self);
    //
    while (self->next != NULL){
        HashNode_free(SLNode_removeNext(self));
    }
    SLNode_freeWhole(self, (Destructor)HashNode_free);
}

static void Private_freeWholeHashNodes(SLNode * self, Destructor destroy){
    Debug_exists(self);
    Debug_exists(destroy);
    //
    while (self->next != NULL){
        HashNode * removed = SLNode_removeNext(self);
        destroy(removed->value);
        HashNode_free(removed);
    }
    destroy(HashNode_valueAt(SLNode_at(self)));
    SLNode_freeWhole(self, (Destructor)HashNode_free);
}

typedef struct __HashTable {
    SLNode ** items;
    Hashifier hashify;
    Equaler keyEquals;
    size_t size;
    size_t capacity;
} HashTable;


static void Private_handleOverloading(HashTable * self){
    Debug_exists(self);
    //
    if (((float)self->size) / (float)self->capacity <= OVERLOADING_COEFICIENT) return;

    size_t i = self->capacity - 1;

    self->capacity = self->capacity << 1;
    SLNode ** newbie = Mem_calloc(sizeof(SLNode *) * self->capacity);
    
    size_t size = self->size;
    while (true){
        while (self->items[i] != NULL){
            SLNode * next = self->items[i]->next;
            size_t soldier = (self->hashify)(HashNode_keyAt(SLNode_at(self->items[i]))) % self->capacity; // get key mapped

            SLNode_setNext(self->items[i], newbie[soldier]);
            newbie[soldier] = self->items[i];
            self->items[i] = next;
            if (--size == 0) break;
        }
        if (i == 0) break;
        --i;
    }
    free(self->items);
    self->items = newbie;
}

HashTable * HashTable_new(Hashifier hashify, Equaler keyEquals){
    Debug_exists(hashify);
    Debug_exists(keyEquals);
    //
    HashTable * newbie = Mem_malloc(sizeof(HashTable));
    *newbie = (HashTable){
        .items = Mem_calloc(sizeof(SLNode *) * INITIAL_CAPACITY),
        .hashify = hashify,
        .keyEquals = keyEquals,
        .size = 0,
        .capacity = INITIAL_CAPACITY
    };
    return newbie;
}

void HashTable_free(HashTable * self){
    Debug_exists(self);
    //
    for (size_t i = 0; i < self->capacity; ++i){
        if (self->items[i] != NULL){
            Private_freeHashNodes(self->items[i]);
        }
    }
    free(self->items);
    free(self);
}


void HashTable_freeWhole(HashTable * self, Destructor destroy){
    Debug_exists(self);
    //
    for (size_t i = 0; i < self->capacity; ++i){
        if (self->items[i] != NULL){
            Private_freeWholeHashNodes(self->items[i], (Destructor)destroy);
        }
    }
    free(self->items);
    free(self);
}


void HashTable_insert(HashTable * self, const void * key, void * value){
    Debug_exists(self);
    //
    ++self->size;
    Private_handleOverloading(self);

    size_t soldier = (self->hashify)(key) % self->capacity; // get key mapped

    SLNode * newbie = SLNode_new(HashNode_new(key, value));

    newbie->next = self->items[soldier];

    self->items[soldier] = newbie;

}

bool HashTable_contains(HashTable * self, const void * key){
    Debug_exists(self);
    //
    size_t soldier = (self->hashify)(key) % self->capacity;  // get key mapped
    if (self->items[soldier] == NULL) return false;

    SLNode * cur = self->items[soldier];
    while (cur != NULL && !(self->keyEquals)(HashNode_keyAt(SLNode_at(cur)), key)){
        cur = SLNode_getNext(cur);
    }
    return cur != NULL;
}

void * HashTable_get(HashTable * self, const void * key){
    Debug_exists(self);
    Debug_suppose(!HashTable_isEmpty(self), 
                     "An attempt to get value from an emty hash table was made");
    Debug_suppose(self->items[(self->hashify)(key) % self->capacity] != NULL,
                     "An attempt to extract value from invalid key was made");
    //
    SLNode * cur = self->items[(self->hashify)(key) % self->capacity]; 
    while (!(self->keyEquals)(HashNode_keyAt(SLNode_at(cur)), key)){
        cur = SLNode_getNext(cur);
        //
        Debug_suppose(cur != NULL, "An attempt to extract value from invalid key was made");
    }
    return HashNode_valueAt(SLNode_at(cur));
 }



void * HashTable_remove(HashTable * self, const void * key){
    Debug_exists(self);
    Debug_suppose(!HashTable_isEmpty(self), 
                     "An attempt to remove value from an emty hash table was made");
    Debug_suppose(self->items[(self->hashify)(key) % self->capacity] != NULL,
                     "An attempt to extract value by invalid key was made");
    //
    --self->size;
    size_t looser = (self->hashify)(key) % self->capacity;  // get key mapped

    // case performing head removal
    if ((self->keyEquals)(HashNode_keyAt(SLNode_at(self->items[looser])), key)){
        SLNode * next = SLNode_getNext(self->items[looser]);
        void * surviver = HashNode_valueAt(SLNode_at(self->items[looser]));
        SLNode_freeWhole(self->items[looser], (Destructor)HashNode_free);
        self->items[looser] = next;
        return surviver;
    }

    // perform linear search for a victim
    SLNode * cur = self->items[looser];
    while (!(self->keyEquals)(HashNode_keyAt(SLNode_at(cur->next)), key)){
        cur = cur->next;
        //
        Debug_suppose(cur != NULL, "An attempt to remove value by invalid key was made");
    }
    // free hashnode and return its value
    HashNode * removed =  SLNode_removeNext(cur);
    void * surviver = HashNode_valueAt(removed);
    HashNode_free(removed);
    return surviver;
}

void HashTable_getKeys(HashTable * self, List * keys){
    Debug_exists(self);
    Debug_exists(keys);
    //
    for (size_t i = 0; i < self->capacity; ++i){
        SLNode * cur = self->items[i];
        while (cur != NULL){
            List_addLast(keys, (void *)HashNode_keyAt(SLNode_at(cur)));
            cur = cur->next;
        }
    }
}

size_t HashTable_size(HashTable * self){
    Debug_exists(self);
    //
    return self->size;
}



bool HashTable_isEmpty(HashTable * self){
    Debug_exists(self);
    //
    return self->size == 0;
}



void HashTable_clear(HashTable * self){
    Debug_exists(self);
    //
    for (size_t i = 0; i < self->capacity; ++i){
        if (self->items[i] != NULL){
            Private_freeHashNodes(self->items[i]);
            self->items[i] = NULL;
        }
    }
}

void HashTable_clearWhole(HashTable * self, Destructor destroy){
    Debug_exists(self);
    //
    for (size_t i = 0; i < self->capacity; ++i){
        if (self->items[i] != NULL){
            Private_freeWholeHashNodes(self->items[i], (Destructor)destroy);
            self->items[i] = NULL;
        }
    }
}