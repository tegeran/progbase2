#include <trie.h>
#include <error.h>
#include <stdlib.h>


typedef struct __Trie {
    Tree * root;
} Trie;

typedef struct __Fruit {
    char key;
    bool isWord;
    void * ptr;
} Fruit;


#define getProperChild(self, character ) \
Tree_sortedLinearSearchChild(self, &(Fruit){.key = character}, (Comparator)compareFruit)

static Fruit * Fruit_new(char key, bool isWord, void * ptr);
static void Fruit_free(Fruit * self);
static void Fruit_freeWhole(Fruit * self, Destructor destroy);

static void * Fruit_ptrAt(Fruit * self);
static char Fruit_keyAt(Fruit * self);
static bool Fruit_isWordAt(Fruit * self);


static void * Fruit_setPtr(Fruit * self, void * ptr);
static char Fruit_setKey(Fruit * self, char key);
static bool Fruit_setIsWord(Fruit * self, bool isWord);



Trie * Trie_new(void){
    Trie * newbie = malloc(sizeof(Trie));
    Error_checkHeap(newbie);
    //
    newbie->root = Tree_new(Fruit_new('#', false, NULL));
    return newbie;
}


Fruit * Fruit_new(char key, bool isWord, void * ptr){
    Fruit * newbie = malloc(sizeof(Fruit));
    Error_checkHeap(newbie);

    newbie->key = key;
    newbie->isWord = isWord;
    newbie->ptr = ptr;
    return newbie;
}

static void Fruit_free(Fruit * self){
    Debug_exists(self);
    //
    free(self);
}

static void Fruit_freeWhole(Fruit * self, Destructor destroy){
    Debug_exists(self);
    //
    destroy(self->ptr);
    free(self);
    return;
}

static void * Fruit_ptrAt(Fruit * self){
    Debug_exists(self);
    //
    return self->ptr;
}

static char Fruit_keyAt(Fruit * self){
    Debug_exists(self);
    //
    return self->key;
}

static bool Fruit_isWordAt(Fruit * self){
    Debug_exists(self);
    //
    return self->isWord;
}


static void * Fruit_setPtr(Fruit * self, void * ptr){
    Debug_exists(self);
    //
    void * prev = self->ptr;
    self->ptr = ptr;
    return prev;
}

static char Fruit_setKey(Fruit * self, char key){
    Debug_exists(self);
    //
    char prev = self->key;
    self->key = key;
    return prev;
}

static bool Fruit_setIsWord(Fruit * self, bool isWord){
    Debug_exists(self);
    //
    bool prev = self->isWord;
    self->isWord = isWord;
    return prev;
}

static signed char compareFruit(Fruit * bibba, Fruit * bobba){
    if (bibba->key > bobba->key) 
        return -1;
    else return bobba->key > bibba->key; 
}



static Destructor ptrDestructor = NULL;

static void freeFruit(Fruit * self){
    Debug_exists(self);
    Debug_exists(ptrDestructor);
    
    if (self->isWord)
        Fruit_freeWhole(self, ptrDestructor);
    else Fruit_free(self);
}

void Trie_free(Trie * self, Destructor destruct){
    Debug_exists(self);
    Debug_exists(destruct);
    //
    ptrDestructor = destruct;
    Tree_freeWhole(self->root, (Destructor)freeFruit);
    free(self);
}



static bool ninsert(Tree * self, const char * word, void * ptr, size_t nchars){

    if (*word == '\0' || nchars == 0){
        if (Fruit_isWordAt(Tree_at(self))){
            return false;
        } else {
            Fruit_setIsWord(Tree_at(self), true);
            Fruit_setPtr(Tree_at(self), ptr);
            return true;
        }
    }

    // search for a child
    signed long long i = getProperChild(self, *word);

    if (i < 0){ // case child was not found function returns the index where it shoud be inserted
        i = -(i + 1);
        Tree_insertChild(self, i, Tree_new(Fruit_new(*word, false, NULL)));
    } 
    return ninsert(Tree_childAt(self, i), word + 1, ptr, nchars - 1);
}

bool Trie_ninsert(Trie * self, const char * word, void * ptr, size_t nchars){
    Debug_exists(self);
    Debug_exists(word);
    //
    return ninsert(self->root, word, ptr, nchars);
}

static bool insert(Tree * self, const char * word, void * ptr);

bool Trie_insert(Trie * self, const char * word, void * ptr){
    Debug_exists(self);
    Debug_exists(word);
    //
    return insert(self->root, word, ptr);
}

static bool insert(Tree * self, const char * word, void * ptr){

    if (*word == '\0'){
        if (Fruit_isWordAt(Tree_at(self))){
            return false;
        } else {
            Fruit_setIsWord(Tree_at(self), true);
            Fruit_setPtr(Tree_at(self), ptr);
            return true;
        }
    }

    // search for a child
    signed long long i = getProperChild(self, *word);

    if (i < 0){ // case child was not found function returns the index where it shoud be inserted
        i = -(i + 1);
        Tree_insertChild(self, i, Tree_new(Fruit_new(*word, false, NULL)));
    } 
    return insert(Tree_childAt(self, i), word + 1, ptr);

}




static void * ndelete(Tree * self, const char * word, bool * deleteSelf, bool * status, size_t nchars){
    if (*word == '\0' || nchars == 0) {

        if (!Fruit_isWordAt(Tree_at(self))){
            *status = FAILURE;
            return NULL;
        } else {
            void * ptr = Fruit_ptrAt(Tree_at(self));
            if (Tree_isLeaf(self)){
                Tree_freeWhole(self, (Destructor)Fruit_free);
                *deleteSelf = true;
            }
            else {
                Fruit_setPtr(Tree_at(self), NULL);
                Fruit_setIsWord(Tree_at(self), false);
            }
            return ptr;
        }

    } else { // case we are in a middle of a word
        signed long long i = getProperChild(self, *word);
        if (i < 0){ 
            *status = FAILURE; 
            return NULL;  
        }
        void * recurrentReturn = ndelete(Tree_childAt(self, i), word + 1, deleteSelf, status, nchars - 1);
        if (*deleteSelf){
            Tree_removeChild(self, i); // this pointer was freed deeply in recursion, just remove it from list
            if (Tree_childrenSize(self) <= 1 && !Fruit_isWordAt(Tree_at(self))){  
                Tree_freeWhole(self, (Destructor)Fruit_free);
            }
            else {
                *deleteSelf = false;
            }
        } 
        return recurrentReturn;
    }
}



void * Trie_nremove(Trie * self, const char * word, bool * status, size_t nchars){
    Debug_exists(self);
    Debug_exists(word);
    Debug_exists(status);
    //

    *status = SUCCESS;

    if (*word == '\0' || nchars == 0){
        if (Fruit_isWordAt(Tree_at(self->root))){
            Fruit_setIsWord(Tree_at(self->root), false);
            return Fruit_setPtr(Tree_at(self->root), NULL);
        } else {
            *status = FAILURE;
            return NULL;
        }
    }
    bool deleteSelf = false;

    signed long long i = getProperChild(self->root, *word);

    if (i < 0) {
        *status = FAILURE;
        return NULL;
    }

    void * returned = ndelete(Tree_childAt(self->root, i), word + 1, &deleteSelf, status, nchars - 1);

    if (deleteSelf) {
        Tree_removeChild(self->root, i);
    }

    return returned;
}


static void * delete(Tree * self, const char * word, bool * deleteSelf, bool * status){
    if (*word == '\0') {

        if (!Fruit_isWordAt(Tree_at(self))){
            *status = FAILURE;
            return NULL;
        } else {
            void * ptr = Fruit_ptrAt(Tree_at(self));
            if (Tree_isLeaf(self)){
                Tree_freeWhole(self, (Destructor)Fruit_free);
                *deleteSelf = true;
            }
            else {
                Fruit_setPtr(Tree_at(self), NULL);
                Fruit_setIsWord(Tree_at(self), false);
            }
            return ptr;
        }

    } else { // case we are in a middle of a word
        signed long long i = getProperChild(self, *word);
        if (i < 0){ 
            *status = FAILURE; 
            return NULL;  
        }
        void * recurrentReturn = delete(Tree_childAt(self, i), word + 1, deleteSelf, status);
        if (*deleteSelf){
            Tree_removeChild(self, i); // this pointer was freed deeply in recursion, just remove it from list
            if (Tree_childrenSize(self) <= 1 && !Fruit_isWordAt(Tree_at(self))){  
                Tree_freeWhole(self, (Destructor)Fruit_free);
            }
            else {
                *deleteSelf = false;
            }
        } 
        return recurrentReturn;
    }
}



void * Trie_remove(Trie * self, const char * word, bool * status){
    Debug_exists(self);
    Debug_exists(word);
    Debug_exists(status);
    //

    *status = SUCCESS;

    if (*word == '\0'){
        if (Fruit_isWordAt(Tree_at(self->root))){
            Fruit_setIsWord(Tree_at(self->root), false);
            return Fruit_setPtr(Tree_at(self->root), NULL);
        } else {
            *status = FAILURE;
            return NULL;
        }
    }
    bool deleteSelf = false;

    signed long long i = getProperChild(self->root, *word);

    if (i < 0) {
        *status = FAILURE;
        return NULL;
    }

    void * returned = delete(Tree_childAt(self->root, i), word + 1, &deleteSelf, status);

    if (deleteSelf) {
        Tree_removeChild(self->root, i);
    }

    return returned;
}


static void * nsearch(Tree * self, const char * word, bool * status, size_t nchars){
    if (*word == '\0' || nchars == 0) {
        if (Fruit_isWordAt(Tree_at(self))){
            *status = SUCCESS;
            return Fruit_ptrAt(Tree_at(self));
        } else {
            *status = FAILURE;
            return NULL;
        }
    }


    signed long long i = getProperChild(self, *word);
    if (i < 0){
        *status = FAILURE;
        return NULL;
    }
    else return nsearch(Tree_childAt(self, i), word + 1, status, nchars - 1);
    
}


void * Trie_nsearch(Trie * self, const char * word, bool * status, size_t nchars){
    Debug_exists(self);
    Debug_exists(word);
    Debug_exists(status);
    //
    return nsearch(self->root, word, status, nchars);
}

static void * search(Tree * self, const char * word, bool * status){
    if (*word == '\0') {
        if (Fruit_isWordAt(Tree_at(self))){
            *status = SUCCESS;
            return Fruit_ptrAt(Tree_at(self));
        } else {
            *status = FAILURE;
            return NULL;
        }
    }


    signed long long i = getProperChild(self, *word);
    if (i < 0){
        *status = FAILURE;
        return NULL;
    }
    else return search(Tree_childAt(self, i), word + 1, status);
    
}

void * Trie_search(Trie * self, const char * word, bool * status){
    Debug_exists(self);
    Debug_exists(word);
    Debug_exists(status);
    //
    return search(self->root, word, status);
}

bool Trie_isEmpty(Trie * self){
    Debug_exists(self);
    //
    return Tree_isLeaf(self->root) && Fruit_isWordAt(Tree_at(self->root)) == false;
}


static Printer ptrPrinter = NULL;

static void printFruit(Fruit * fruit){
    Debug_exists(fruit);
    Debug_suppose(ptrPrinter != NULL, "Print function is NULL");
    //
    putwchar(fruit->key);
    if (Fruit_isWordAt(fruit)){
        wprintf(L"🚩");

        ptrPrinter(fruit->ptr);
    }
}

void Trie_print(Trie * self, Location loc, Printer print, bool inAlphabeticOrder){
    Debug_exists(self);
    Debug_suppose(!Trie_isEmpty(self), "Tried to print an empty trie");
    Debug_UTF8();
    //
    ptrPrinter = print;
    Tree_printFancy(self->root, loc, (Printer)printFruit, !inAlphabeticOrder);    
}

void * Trie_query(Trie * self, const char * str, const char ** keyEnd){
    Debug_exists(self);
    Debug_exists(str);
    Debug_exists(keyEnd);
    //
    if (*str == '\0'){
        if (!Fruit_isWordAt(Tree_at(self->root))){
            *keyEnd = NULL;;
            return NULL;
        } else {
            *keyEnd = str;
            return Fruit_ptrAt(Tree_at(self->root));
        }
    }
    Tree * lastWord = NULL;
    Tree * cur = self->root;
    signed long long i = -1L;
    while (*str != '\0' && (i = getProperChild(cur, *str)) >= 0){
        cur = Tree_childAt(cur, i);
        if (Fruit_isWordAt(Tree_at(cur))){
            lastWord = cur;
            *keyEnd = str;
        }
        ++str;
    }
    if (lastWord == NULL){
        *keyEnd = NULL;
        return NULL;        
    } else return Fruit_ptrAt(Tree_at(lastWord));
}
