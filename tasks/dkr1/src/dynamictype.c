#include <dynamictype.h>
#include <error.h>
#include <floatingpoint.h>
#include <sstring.h>
#include <list.h>
#include <boolean.h>
#include <character.h>
#include <list_ext.h>
#include <wchar.h>
#include <mem.h>

typedef struct __DynamicType{
    Type type;
    void * val;
} DynamicType;


static void Private_clearVal(DynamicType * self){
    Debug_exists(self);
    //
    switch (self->type){
        case Type_CHARACTER:{
            Character_free(self->val);
            break;
        }
        case Type_FLOAT:{
            Float_free(self->val);
            break;
        }
        case Type_STRING:{
            String_free(self->val);
            break;
        }
        case Type_BOOLEAN:{
            Boolean_free(self->val);
            break;
        }
        case Type_TYPE_LIST:{
            List_freeWhole(self->val, (Destructor)DynamicType_free);
            break;
        }
        case Type_UNDEFINED:
            break;
        // Undefined type has one value -> NULL
    }
}

static void Private_newVal(DynamicType * self){
    switch(self->type){
        case Type_CHARACTER:{
            self->val = Character_new('\0');
            break;
        }
        case Type_STRING:{
            self->val = String_new();
            break;
        }
        case Type_FLOAT:{
            self->val = Float_new(0);
            break;
        }
        case Type_BOOLEAN:{
            self->val = Boolean_new(false);
            break;
        }
        case Type_TYPE_LIST:{
            self->val = List_new();
            break;
        }
        case Type_UNDEFINED:{
            self->val = NULL;
            break;
        }
    }
}


DynamicType * DynamicType_new(Type type){
    DynamicType * newbie = Mem_malloc(sizeof(DynamicType));
    newbie->type = type;
    
    Private_newVal(newbie);
    return newbie;
}

void DynamicType_free(DynamicType * self){
    Debug_exists(self);
    //
    Private_clearVal(self);
    free(self);
}

Type DynamicType_typeAt(DynamicType * self){
    Debug_exists(self);
    //
    return self->type;
}

Type DynamicType_setType(DynamicType * self, Type type){
    Debug_exists(self);
    //
    Type prev = self->type;
    if (type != prev || prev == Type_STRING || prev == Type_TYPE_LIST){
        Private_clearVal(self);
        self->type = type;
        Private_newVal(self);
    }
    return prev;
}

signed char DynamicType_compare(DynamicType * self, DynamicType * other){
    Debug_exists(self);
    Debug_exists(other);
    //
    if (self->type != other->type) return -2;
    //
    switch (self->type){
        case Type_CHARACTER: return Character_compare(self->val, other->val);
        case Type_BOOLEAN:   return Boolean_compare(self->val, other->val);
        case Type_FLOAT:     return Float_compare(self->val, other->val);
        case Type_STRING:    return String_compare(self->val, other->val);
        case Type_TYPE_LIST: {
            
            if (List_size(self->val) > List_size(other->val)) return -1;
            else if (List_size(other->val) > List_size(self->val)) return 1;

            for (size_t i = 0; i < List_size(self->val); ++i){
                signed char res = DynamicType_compare(List_at(self->val, i), 
                                                      List_at(other->val, i));
                if (res != 0){
                    return res;
                }
            }
            return 0;
        }
        default: return other->type != Type_UNDEFINED;
    }
}

void DynamicType_print(DynamicType * self){
    Debug_exists(self);
    //
    switch(self->type){
        case Type_CHARACTER:{
            Character_print(self->val); 
            fflush(stdout);
            return;
        }
        case Type_FLOAT:{
            Float_print(self->val); 
            fflush(stdout);
            return;
        }
        case Type_STRING:{
            String_print(self->val); 
            fflush(stdout);
            return;
        }
        case Type_BOOLEAN:{
            Boolean_print(self->val); 
            fflush(stdout);
            return;
        }
        case Type_TYPE_LIST:{ 
            List_print(self->val, (Printer)DynamicType_print);
            fflush(stdout);
            return;
        }
        case Type_UNDEFINED:{
            fputs("undefined", stdout); 
            fflush(stdout);
            return;
        }
    }
}

void DynamicType_printWide(DynamicType * self){
    Debug_exists(self);
    Debug_UTF8();
    //
    switch(self->type){
        case Type_CHARACTER:{
            Character_printWide(self->val); 
            return;
        }
        case Type_FLOAT:{
            Float_printWide(self->val); 
            return;
        }
        case Type_STRING:{
            String_printWide(self->val); 
            return;
        }
        case Type_BOOLEAN:{
            Boolean_printWide(self->val); 
            return;
        }
        case Type_TYPE_LIST:{ 
            List_printWide(self->val, (Printer)DynamicType_printWide);
            return;
        }
        case Type_UNDEFINED:{
            wprintf(L"undefined"); 
            return;
        }
    }
}

void DynamicType_writeTo(FILE * file, DynamicType * self){
    Debug_exists(self);
    Debug_exists(file);
    //
    switch(self->type){
        case Type_CHARACTER:{
            Character_writeTo(file, self->val); 
            return;
        }
        case Type_FLOAT:{
            Float_writeTo(file, self->val); 
            return;
        }
        case Type_STRING:{
            String_writeTo(file, self->val); 
            return;
        }
        case Type_BOOLEAN:{
            Boolean_writeTo(file, self->val); 
            return;
        }
        case Type_TYPE_LIST:{
            if (List_isEmpty(self->val)) return;
            for (size_t i = 0; i < List_size(self->val) - 1; ++i){
                DynamicType_writeTo(file, List_at(self->val, i));
                char ch = ' ';
                fwrite(&ch, sizeof(char), 1, file);
            }
            DynamicType_writeTo(file, List_at(self->val, List_size(self->val) - 1));
            return;
        }
        case Type_UNDEFINED:{
            fprintf(file, "undefined"); 
            return;
        }
    }
}

char DynamicType_charAt(DynamicType * self){
    Debug_exists(self);
    Debug_tip(self->type == Type_CHARACTER, "Type unsafe action detected");
    //
    return Character_at(self->val);
}

String * DynamicType_stringAt(DynamicType * self){
    Debug_exists(self);
    Debug_tip(self->type == Type_STRING, "Type unsafe action detected");
    //
    return self->val;
}

float DynamicType_floatAt(DynamicType * self){
    Debug_exists(self);
    Debug_tip(self->type == Type_FLOAT, "Type unsafe action detected");
    //
    return Float_at(self->val);
}

bool DynamicType_boolAt(DynamicType * self){
    Debug_exists(self);
    Debug_tip(self->type == Type_BOOLEAN, "Type unsafe action detected");
    //
    return Boolean_at(self->val);
}


void DynamicType_setChar(DynamicType * self, char character){
    Debug_exists(self);
    //
    DynamicType_setType(self, Type_CHARACTER);
    Character_set(self->val, character);
}

void DynamicType_setString(DynamicType * self, String * str){
    Debug_exists(self);
    Debug_exists(str);
    //
    Private_clearVal(self);
    self->type = Type_STRING;
    self->val = str;
}

void DynamicType_setFloat(DynamicType * self, float floatingPoint){
    Debug_exists(self);
    //
    DynamicType_setType(self, Type_FLOAT);
    Float_set(self->val, floatingPoint);
}

void DynamicType_setBool(DynamicType * self, bool boolean){
    Debug_exists(self);
    //
    DynamicType_setType(self, Type_BOOLEAN);
    Boolean_set(self->val, boolean);
}


List * DynamicType_listAt(DynamicType * self){
    Debug_exists(self);
    Debug_tip(self->type == Type_TYPE_LIST, "Type unsafe action detected");
    //
    return self->val;
}

void DynamicType_setList(DynamicType * self, List * list){
    Debug_exists(self);
    Debug_exists(list);
    //
    Private_clearVal(self);
    self->type = Type_TYPE_LIST;
    self->val = list;
}

void DynamicType_setUndef(DynamicType * self){
    Debug_exists(self);
    //
    DynamicType_setType(self, Type_UNDEFINED);
}

void DynamicType_setCopy(DynamicType * self, DynamicType * template){
    switch (template->type){
        case Type_BOOLEAN: {
            DynamicType_setBool(self, Boolean_at(template->val));
            return;
        }
        case Type_FLOAT: {
            DynamicType_setFloat(self, Float_at(template->val));
            return;
        }
        case Type_CHARACTER: {
            DynamicType_setChar(self, Character_at(template->val));
            return;
        }
        case Type_STRING: {
            DynamicType_setType(self, Type_STRING);
            String_setCopy(DynamicType_stringAt(self), (template->val));
            return;
        }
        case Type_TYPE_LIST: {
            DynamicType_setType(self, Type_TYPE_LIST);
            if (!List_isEmpty(self->val)) List_freeWhole(self->val, (Destructor)DynamicType_free);
            for (size_t i = 0; i < List_size(template->val); ++i){
                List_addLast(self->val, DynamicType_newCopyOf(List_at(template->val, i)));
            }
            return;
        }
        default :{
            DynamicType_setUndef(self);
        } 
    }
}

DynamicType * DynamicType_newCopyOf(DynamicType * template){
    DynamicType * newbie = DynamicType_new(Type_UNDEFINED);
    DynamicType_setCopy(newbie, template);
    return newbie;
}

void DynamicType_appendToString(String * dest, DynamicType * self){
    Debug_exists(dest);
    Debug_exists(self);
    //
    switch(self->type){
        case Type_CHARACTER:{
            String_appendChar(dest, Character_at(self->val)); 
            return;
        }
        case Type_FLOAT:{
            String_appendFormat(dest, "%f", Float_at(self->val)); 
            return;
        }
        case Type_STRING:{
            String_concatenate(dest, self->val); 
            return;
        }
        case Type_BOOLEAN:{
            if (Boolean_at(self->val))
                String_append(dest, "true");
            else
                String_append(dest, "false");
            return;
        }
        case Type_TYPE_LIST:{ 
            for (size_t i = 0; i < List_size(self->val); ++i){
                DynamicType_appendToString(dest, List_at(DynamicType_listAt(self->val), i));
            }
            return;
        }
        case Type_UNDEFINED:{
            String_append(dest, "undefined"); 
            return;
        }
    }
}

void DynamicType_addFirstToString(String * dest, DynamicType * self){
    Debug_exists(dest);
    Debug_exists(self);
    //
    switch(self->type){
        case Type_CHARACTER:{
            String_addCharFirst(dest, Character_at(self->val)); 
            return;
        }
        case Type_FLOAT:{
            String * temp = String_new();
            String_appendFormat(temp, "%f", Float_at(self->val)); 
            String_addStringFirst(dest, temp);
            String_free(temp);
            return;
        }
        case Type_STRING:{
            String_addStringFirst(dest, self->val); 
            return;
        }
        case Type_BOOLEAN:{
            if (Boolean_at(self->val))
                String_addCharsFirst(dest, "true");
            else
                String_addCharsFirst(dest, "false");
            return;
        }
        case Type_TYPE_LIST:{ 
            for (size_t i = 0; i < List_size(self->val); ++i){
                DynamicType_appendToString(dest, List_at(DynamicType_listAt(self->val), i));
            }
            return;
        }
        case Type_UNDEFINED:{
            String_addCharsFirst(dest, "undefined"); 
            return;
        }
    }
}


bool DynamicType_convertToBool(DynamicType * self){
    Debug_exists(self);
    //
    switch (self->type){
        case Type_BOOLEAN:   return Boolean_at(self->val);
        case Type_CHARACTER: return Character_at(self->val) != '\0';
        case Type_STRING:    return String_length(self->val) != 0;
        case Type_FLOAT:     return float_compare(Float_at(self->val), 0) != 0;
        case Type_TYPE_LIST: return List_size(self->val) != 0;
        case Type_UNDEFINED: Debug_shutdown("Boolean value for undefined type is undefined");
    }
    return false;
}

void DynamicType_memCopy(DynamicType * self, DynamicType * share){
    Debug_exists(self);
    Debug_exists(self);
    Private_clearVal(self);
    //
    self->type = share->type;
    self->val = share->val;
}


void DynamicType_memAppend(DynamicType * self, DynamicType * other){
    Debug_exists(self);
    Debug_exists(other);
    //

    switch (self->type){
        case Type_TYPE_LIST:{

            if (other->type == Type_TYPE_LIST){

                if (List_isEmpty(self->val)){
                    DynamicType_memReassign(self, other);
                    return;
                } else if (List_isEmpty(other->val)){
                    DynamicType_free(other);
                    return;
                }
                do {

                    List_addLast(self->val, List_removeAt(other->val, 0));

                } while (!List_isEmpty(other->val));

                DynamicType_free(other);
            
            } else {
                List_addLast(self->val, other);
            }
            return;
        }
        case Type_STRING:{
            switch (other->type){
                case  Type_TYPE_LIST:{
                    DynamicType * newbie = Mem_malloc(sizeof(DynamicType));
                    newbie->type = self->type;
                    newbie->val = self->val;
                    List_addFirst(other->val, newbie);
                    self->val = other->val;
                    self->type = Type_TYPE_LIST;
                    free(other);
                    return;
                }
                default: {
                    DynamicType_appendToString(self->val, other); // appending is faster then addfirsting
                    DynamicType_free(other);
                    return;
                }
            }
        }
        default:{
            switch (other->type){
                case Type_TYPE_LIST:{
                    DynamicType * newbie = Mem_malloc(sizeof(DynamicType));
                    newbie->type = self->type;
                    newbie->val = self->val;
                    List_addFirst(other->val, newbie);
                    self->val = other->val;
                    self->type = Type_TYPE_LIST;
                    free(other);
                    return;
                }
                case Type_STRING:{
                    DynamicType_addFirstToString(other->val, self);
                    DynamicType_memReassign(self, other);
                    return;
                }
                case Type_CHARACTER:{
                    if (other->type == Type_CHARACTER){
                        char selfCh = DynamicType_charAt(self);
                        DynamicType_setType(self, Type_STRING);
                        String_appendChar(DynamicType_stringAt(self), selfCh);
                        String_appendChar(DynamicType_stringAt(self), DynamicType_charAt(other));
                        DynamicType_free(other);
                        return;
                    }
                }
                default:{
                    DynamicType * selfEntity = Mem_malloc(sizeof(DynamicType));
                    selfEntity->type = self->type;
                    selfEntity->val = self->val;
                    self->type = Type_TYPE_LIST;
                    self->val = List_new();
                    List_addLast(self->val, selfEntity);
                    List_addLast(self->val, other);
                    return;
                }
            }
        }
    }
}
DynamicType * DynamicType_memUniteConcatenate(DynamicType * self, DynamicType * other){
    Debug_exists(self);
    Debug_exists(other);
    //

    switch (self->type){
        case Type_TYPE_LIST:{

            if (other->type == Type_TYPE_LIST){

                if (List_isEmpty(self->val)){
                    DynamicType_free(self);
                    return other;
                } else if (List_isEmpty(other->val)){
                    DynamicType_free(other);
                    return self;
                }

                do {

                    List_addLast(self->val, List_removeAt(other->val, 0));

                } while (!List_isEmpty(other->val));

                DynamicType_free(other);

            } else {
                List_addLast(self->val, other);
            }
            return self;
        }
        case Type_STRING:{
            switch (other->type){
                case  Type_TYPE_LIST:{
                    List_addFirst(other->val, self);
                    return other;
                }
                default: {
                    DynamicType_appendToString(self->val, other); // appending is faster then addfirsting
                    DynamicType_free(other);
                    return self;
                }
            }
        }
        case Type_CHARACTER:{
            if (other->type == Type_CHARACTER){
                char selfCh = DynamicType_charAt(self);
                DynamicType_setType(self, Type_STRING);
                String_appendChar(DynamicType_stringAt(self), selfCh);
                String_appendChar(DynamicType_stringAt(self), DynamicType_charAt(other));
                DynamicType_free(other);
                return self;
            }
        }
        default:{
            switch (other->type){
                case Type_TYPE_LIST:{
                    List_addFirst(other->val, self);
                    return other;
                }
                case Type_STRING:{
                    DynamicType_addFirstToString(other->val, self);
                    DynamicType_free(self);
                    return other;
                }
                default:{
                    DynamicType * list = DynamicType_new(Type_TYPE_LIST);
                    List_addLast(list->val, self);
                    List_addLast(list->val, other);
                    return list;
                }
            }
        }
    }
}

void DynamicType_castBinaryNumOperands(DynamicType * self, DynamicType * other){
    Debug_suppose(self->type == Type_FLOAT || self->type == Type_CHARACTER, "Casting");

    switch (self->type){
        case Type_CHARACTER:{
            switch (other->type){
                case Type_FLOAT:{
                    DynamicType_setFloat(self, DynamicType_charAt(self));
                    return;
                }
                default:{ // case Type_CHARACTER
                    return;
                }
            }
        }
        case Type_FLOAT:{
            switch (other->type){
                case Type_CHARACTER:{
                    DynamicType_setFloat(other, DynamicType_charAt(other));
                    return;
                }
                default:{  // case Type_FLOAT
                    return;
                }
            }
        }
        default: Debug_shutdown("An attempt to cast from non-numeric type to numeric was made");
    }
}

bool DynamicType_isNumeric(DynamicType * value){
    switch (DynamicType_typeAt(value)){
        case Type_CHARACTER:
        case Type_FLOAT: return true;
        default:        return false;
    }
}


void DynamicType_memReassign(DynamicType * self, DynamicType * share){
    Debug_exists(self);
    Debug_exists(self);
    Private_clearVal(self);
    //
    self->type = share->type;
    self->val = share->val;
    free(share);
}


void DynamicType_memDuplicate(DynamicType * self, DynamicType * share){
    Debug_exists(self);
    Debug_exists(share);
    //
    self->type = share->type;
    self->val = share->val;
}

void DynamicType_freeBox(DynamicType * self){
    Debug_exists(self);
    //
    free(self);
}

void DynamicType_memLeakUndef(DynamicType * self){
    Debug_exists(self);
    //
    self->type = Type_UNDEFINED;
    self->val = NULL;
}