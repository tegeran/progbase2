#pragma once
#include <stdbool.h>

typedef struct __Iterator Iterator;
typedef long IteratorDistance;

void Iterator_free(Iterator * self);

void * Iterator_value    (const Iterator * self);
void   Iterator_next     (Iterator * self);
void   Iterator_prev     (Iterator * self);
void   Iterator_advance  (Iterator * self, IteratorDistance n);
bool   Iterator_equals   (const Iterator * self, const Iterator * other);
bool   Iterator_inbound  (const Iterator * self); // returns true if iterator is currently 
                                            // pointing on a valid element of a structure
                                            // java-like method hasNext()
IteratorDistance Iterator_distance (Iterator * begin, Iterator * end);
