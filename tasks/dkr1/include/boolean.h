#pragma once

#include <stdbool.h>
#include <stdio.h>

typedef struct __Boolean{
    bool b;
} Boolean;

Boolean * Boolean_new(bool b);
void Boolean_free(Boolean * self);

bool Boolean_at(Boolean * self);
bool Boolean_set(Boolean * self, bool b);

void Boolean_print(Boolean * self);
void Boolean_printWide(Boolean * self);
void Boolean_writeTo(FILE * file, Boolean * self);

signed char Boolean_compare(Boolean * self, Boolean * other);