#pragma once

typedef struct __DLNode {
    void * val;
    struct __DLNode * next;
    struct __DLNode * prev;
} DLNode;

typedef void (* Destructor)(void *);

void DLNode_freeWhole(DLNode * self, Destructor valDestructor);

// Construcor and destructor
// AHTUNG PROGRAMMIERER! Constructor aborts workflow if experineced heap overflow
DLNode * DLNode_new(void * val);
void DLNode_free(DLNode * self);


// Getters
void * DLNode_at(DLNode * self);
DLNode * DLNode_getNext(DLNode * self);
DLNode * DLNode_getPrev(DLNode * self);

// Setters
void * DLNode_set(DLNode * self, void * newVal);
DLNode * DLNode_setNext(DLNode * self, DLNode * next);
DLNode * DLNode_setPrev(DLNode * self, DLNode * prev);

// nc means that no checks for NULL pointer at nodes prev and next fields will be executed
// Precondition, newbie != NULL && src != NULL && self != NULL, otherwise behaviour is undefined
DLNode * DLNode_remove(DLNode * self);  // self is returned for both funtions
DLNode * DLNode_ncRemove(DLNode * self);

void DLNode_addAfter(DLNode * src, DLNode * newbie);  
void DLNode_ncAddAfter(DLNode * src, DLNode * newbie);

void DLNode_addPrev(DLNode * src, DLNode * newbie);   
void DLNode_ncAddPrev(DLNode * src, DLNode * newbie); 