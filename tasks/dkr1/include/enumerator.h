#pragma once

#include <stdbool.h>
// Superinterface

typedef struct __Enumerator Enumerator;

void Enumerator_free(Enumerator * self);

void * Enumerator_current  (Enumerator * self);
bool   Enumerator_moveNext (Enumerator * self);
void   Enumerator_reset    (Enumerator * self);
bool   Enumerator_isInbound(Enumerator * self);
