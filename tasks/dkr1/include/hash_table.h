#pragma once

#include <stdlib.h>
#include <list.h>
#include <stdbool.h>

typedef bool (*Equaler)(const void *, const void *);
typedef size_t (*Hashifier)(const void *);
typedef void (*Destructor)(void *);

typedef struct __HashTable HashTable;

HashTable * HashTable_new(Hashifier hashify, Equaler keyEquals);
void HashTable_free(HashTable * self);
void HashTable_freeWhole(HashTable * self, Destructor destroy);
void HashTable_insert(HashTable * self, const void * key, void * value);

bool HashTable_contains(HashTable * self, const void * key);
void * HashTable_get(HashTable * self, const void * key);
void * HashTable_remove(HashTable * self, const void * key);

void HashTable_getKeys(HashTable * self, List * keys);
size_t HashTable_size(HashTable * self);

void HashTable_clear(HashTable * self);
void HashTable_clearWhole(HashTable * self, Destructor destroy);

bool HashTable_isEmpty(HashTable * self);