#pragma once

#include <stdbool.h>
#include <stdlib.h>

typedef void (*Destructor)(void *);

typedef struct __Queue Queue;

Queue * Queue_new(void);
void Queue_free(Queue * self);
void Queue_freeWhole(Queue * self, Destructor destroy);

void Queue_enqueue(Queue * self, void * val);
void * Queue_dequeue(Queue * self);
void * Queue_peek(Queue * self);
bool Queue_isEmpty(Queue * self);
size_t Queue_size(Queue * self);