#pragma once

#include <tree.h>
#include <sstring.h>
#include <dynamictype.h>





DynamicType * Interpreter_execute(const Tree * astTree, const String * code);

DynamicType * Calamity_execute(const String * code);