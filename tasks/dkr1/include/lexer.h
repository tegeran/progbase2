#pragma once

#include <token.h>
#include <list.h>

typedef enum __LexerStatus{
    LEXER_SUCCESS,
    LEXER_ERROR_NULL_LIST,
    LEXER_ERROR_NULL_INPUT,
    LEXER_ERROR_UNHANDLED_TOKEN,
    LEXER_ERROR_UNTERMINATED_LITERAL
} LexerStatus;


LexerStatus Lexer_splitTokens(const register char * input, register List * tokens);
void Lexer_clearTokens(List * tokens);


void Lexer_printTokens(List * tokens);
void Lexer_printTokensWide(List * tokens);
void Lexer_writeTokensTo(const char * filePath, List * tokens);