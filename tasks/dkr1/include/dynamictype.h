#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <sstring.h>

typedef enum Type {
    Type_CHARACTER,
    Type_FLOAT,
    Type_STRING,
    Type_BOOLEAN,

    Type_UNDEFINED,

    Type_TYPE_LIST

} Type;

typedef struct __DynamicType DynamicType;

DynamicType * DynamicType_new(Type type);
DynamicType * DynamicType_newCopyOf(DynamicType * template);
void DynamicType_free(DynamicType * self);

void DynamicType_freeBox(DynamicType * self);

DynamicType * DynamicType_memUniteConcatenate(DynamicType * self, DynamicType * other);

Type DynamicType_typeAt(DynamicType * self);
Type DynamicType_setType(DynamicType * self, Type type);

signed char DynamicType_compare(DynamicType * self, DynamicType * other);

void DynamicType_print(DynamicType * self);
void DynamicType_printWide(DynamicType * self);
void DynamicType_writeTo(FILE * file, DynamicType * self);
void DynamicType_appendToString(String * dest, DynamicType * self);
void DynamicType_addFirstToString(String * dest, DynamicType * self);

void DynamicType_setCopy(DynamicType * self, DynamicType * template);

char     DynamicType_charAt(DynamicType * self);
String * DynamicType_stringAt(DynamicType * self);
float    DynamicType_floatAt(DynamicType * self);
bool     DynamicType_boolAt(DynamicType * self);
List *   DynamicType_listAt(DynamicType * self);


void DynamicType_memLeakUndef(DynamicType * self);
void DynamicType_memCopy(DynamicType * self, DynamicType * share);
void DynamicType_memReassign(DynamicType * self, DynamicType * share);
void DynamicType_memDuplicate(DynamicType * self, DynamicType * share);
void DynamicType_memAppend(DynamicType * self, DynamicType * other);

void DynamicType_setUndef(DynamicType * self);
void DynamicType_setChar(DynamicType * self, char character);
void DynamicType_setString(DynamicType * self, String * str);
void DynamicType_setFloat(DynamicType * self, float floatingPoint);
void DynamicType_setBool(DynamicType * self, bool boolean);
void DynamicType_setList(DynamicType * self, List * list);

bool DynamicType_convertToBool(DynamicType * self);

void DynamicType_castBinaryNumOperands(DynamicType * self, DynamicType * other);
bool DynamicType_isNumeric(DynamicType * value);