#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <location.h>
#include <list.h>
#include <dynamictype.h>
#include <sstring.h>
//#include <queue.h>


typedef enum {
    // Logical operators
    LexemeType_AND,
    LexemeType_EQ,
    LexemeType_GEQ,
    LexemeType_GREATER,
    LexemeType_LEQ,
    LexemeType_LESS,
    LexemeType_NEQ,
    LexemeType_NOT,
    LexemeType_OR,

    // Memory operators
    LexemeType_ASSIGN,
    LexemeType_CONCATENATE,
    LexemeType_NEW,
    LexemeType_RANGE, //  [ i...j )  ... - in particular (may be considered as punctuation)

    // Punctuation
    LexemeType_BLOCK_FINISH,
    LexemeType_BLOCK_START,
    LexemeType_COMA,
    LexemeType_LEFT_BRACKET,
    LexemeType_LEFT_PAR,
    LexemeType_RIGHT_BRACKET,
    LexemeType_RIGHT_PAR,
    LexemeType_TERMINATING,

    // Constructions
    LexemeType_CASE,
    LexemeType_OTHERWISE,
    LexemeType_WHILE,

    // Mathematic operators 
    // precedence: POW -> UNARY -> MULT/DIV/REMAINDER -> PLUS/MINUS
    // SQRT's precedence is defined by superposition

    LexemeType_DIV,
    LexemeType_MINUS,
    LexemeType_MULT,
    LexemeType_PLUS,
    LexemeType_POW,
    LexemeType_REMAINDER,

    // Boolean literals
    LexemeType_FALSE,
    LexemeType_TRUE,

    // Undefined literal
    LexemeType_UNDEFINED,
    

    LexemeType_DEFINE,
    LexemeType_RETURN,
    LexemeType_APPEND,
    LexemeType_ENDL,

    // Volatile storage
    LexemeType_IDENTIFIER,


    LexemeType_STRING,
    LexemeType_CHARACTER,
    LexemeType_FLOAT

    // functions:
} LexemeType;

typedef struct __Lexeme {
    LexemeType type;
    String * name;
} Lexeme;

typedef struct __Token {
    Lexeme * lex;      // a bit of memory optimization
    Location loc;
} Token;


Lexeme * Lexeme_new(LexemeType type, String * name);
Token * Token_new(Lexeme * lex, Location loc);

void Lexeme_free(Lexeme * self);
void Lexeme_freeWhole(Lexeme * self);

void Token_free(Token * self);
void Token_freeWhole(Token * self);

bool Lexeme_equals(Lexeme * self, Lexeme * other);
bool Token_equals(Token * self, Token * other);

void LexemeType_print(LexemeType type);
void LexemeType_appendTo(String * str, LexemeType type);
void Token_appendTo(String * str, Token * self);


void LexemeType_printWide(LexemeType type);
void Token_print(Token * self);
void Token_printWide(Token * self);
void Token_writeTo(FILE * file, Token * self);


LexemeType Token_typeAt(Token * self);


LexemeType Token_setType(Token * self, LexemeType type);


bool Lexeme_isIdLike(const Lexeme * self);
char Token_getEscapes(const char escapesIdentifier);
bool Lexeme_isIdLetter(const char suspect);
bool Lexeme_isIdStart(const char suspect);
bool Token_isEscapeId(const char suspect);
bool Token_isChar(const char suspect);
const char * Token_skipSpaces(const char * str, Location * update);

// Keword identifiers
////
#define LEXEME_NAME_AND           "&&"       
#define LEXEME_NAME_ASSIGN        "="         
#define LEXEME_NAME_BLOCK_FINISH  "}"         
#define LEXEME_NAME_BLOCK_START   "{"         
#define LEXEME_NAME_CASE          "if"      
#define LEXEME_NAME_COMA          ","         
#define LEXEME_NAME_CONCATENATE   "++"        
#define LEXEME_NAME_DIV           "/"       
#define LEXEME_NAME_EQ            "=="        
#define LEXEME_NAME_FALSE         "false"     
#define LEXEME_NAME_GEQ           ">="        
#define LEXEME_NAME_GREATER       ">"           
#define LEXEME_NAME_LEFT_BRACKET  "["         
#define LEXEME_NAME_LEFT_PAR      "("         
#define LEXEME_NAME_LEQ           "<="        
#define LEXEME_NAME_LESS          "<"         
#define LEXEME_NAME_MINUS         "-"         
#define LEXEME_NAME_MULT          "*"         
#define LEXEME_NAME_NEQ           "!="        
#define LEXEME_NAME_NEW           "new"       
#define LEXEME_NAME_NOT           "!"       
#define LEXEME_NAME_OR            "||"        
#define LEXEME_NAME_OTHERWISE     "else" 
#define LEXEME_NAME_PLUS          "+"         
#define LEXEME_NAME_POW           "^"           
#define LEXEME_NAME_RANGE         "..."       
#define LEXEME_NAME_REMAINDER     "%"          
#define LEXEME_NAME_RIGHT_BRACKET "]"         
#define LEXEME_NAME_RIGHT_PAR     ")"         
#define LEXEME_NAME_TERMINATING   ";"         
#define LEXEME_NAME_TRUE          "true"      
#define LEXEME_NAME_UNDEFINED     "undefined" 
#define LEXEME_NAME_WHILE         "while"     
#define LEXEME_NAME_DEFINE        "define"
#define LEXEME_NAME_RETURN        "return"
#define LEXEME_NAME_APPEND        "++="
#define LEXEME_NAME_ENDL          "endl"

// Storage token identifiers
#define LEXEME_NAME_IDENTIFIER "ID"

#define LEXEME_NAME_STRING    "STR"
#define LEXEME_NAME_CHARACTER "CHAR"
#define LEXEME_NAME_FLOAT     "FLOAT"

// Commentaries
#define TOKEN_SINGLE_COMMENT  '#'
#define TOKEN_MULTI_COMMENT   '~'

// Literal delimiters
#define TOKEN_LITERAL_START_CHAR   '\''
#define TOKEN_LITERAL_FINISH_CHAR  '\''
#define TOKEN_LITERAL_START_STRING '\"'
#define TOKEN_LITERAL_FINISH_STRING '\"'
#define TOKEN_LITERAL_MANTISSA_DELIMITER '.'
#define TOKEN_LITERAL_ESPACES_START '\\'


#define TOKEN_VOLATILE_TYPES 4
#define TOKEN_TOTAL_TOKEN_TYPES (sizeof(LEXEME_NAMES) / sizeof(LEXEME_NAMES[0]))
#define TOKEN_TOTAL_KEYWORDS (((TOKEN_TOTAL_TOKEN_TYPES) - TOKEN_VOLATILE_TYPES))
// Global spreadsheet of kewords
const static char * LEXEME_NAMES[] = {
    LEXEME_NAME_AND,
    LEXEME_NAME_EQ,
    LEXEME_NAME_GEQ,
    LEXEME_NAME_GREATER,
    LEXEME_NAME_LEQ,
    LEXEME_NAME_LESS,
    LEXEME_NAME_NEQ,
    LEXEME_NAME_NOT,
    LEXEME_NAME_OR,

    LEXEME_NAME_ASSIGN,
    LEXEME_NAME_CONCATENATE,
    LEXEME_NAME_NEW,
    LEXEME_NAME_RANGE, 
    
    LEXEME_NAME_BLOCK_FINISH,
    LEXEME_NAME_BLOCK_START,
    LEXEME_NAME_COMA,
    LEXEME_NAME_LEFT_BRACKET,
    LEXEME_NAME_LEFT_PAR,
    LEXEME_NAME_RIGHT_BRACKET,
    LEXEME_NAME_RIGHT_PAR,
    LEXEME_NAME_TERMINATING,
    
    LEXEME_NAME_CASE,
    LEXEME_NAME_OTHERWISE,
    LEXEME_NAME_WHILE,
    
    LEXEME_NAME_DIV,
    LEXEME_NAME_MINUS,
    LEXEME_NAME_MULT,
    LEXEME_NAME_PLUS,
    LEXEME_NAME_POW,
    LEXEME_NAME_REMAINDER,

    LEXEME_NAME_FALSE,
    LEXEME_NAME_TRUE,
    
    LEXEME_NAME_UNDEFINED,

    LEXEME_NAME_DEFINE,
    LEXEME_NAME_RETURN,
    LEXEME_NAME_APPEND,
    LEXEME_NAME_ENDL,
    
    LEXEME_NAME_IDENTIFIER,
    LEXEME_NAME_STRING,
    LEXEME_NAME_CHARACTER,
    LEXEME_NAME_FLOAT
    
};