#pragma once
#include <tree.h>
#include <dynamictype.h>
#include <sstring.h>
#include <location.h>
#include <sstring.h>

typedef enum {
    AstNodeType_UNKNOWN, 

    AstNodeType_AND,
    AstNodeType_EQ,
    AstNodeType_GEQ,
    AstNodeType_GREATER,
    AstNodeType_LEQ,
    AstNodeType_LESS,
    AstNodeType_NEQ,
    AstNodeType_NOT,
    AstNodeType_OR,

    AstNodeType_ASSIGN,
    AstNodeType_CONCATENATE,
    AstNodeType_DIV,
    AstNodeType_MINUS,
    AstNodeType_MULT,
    AstNodeType_PLUS,
    AstNodeType_POW,
    AstNodeType_REMAINDER,

    AstNodeType_LITERAL,
    AstNodeType_IDENTIFIER,

    AstNodeType_ARGLIST,
    AstNodeType_BLOCK,
    AstNodeType_CASE,
    AstNodeType_WHILE,
    AstNodeType_DECLARATION,
    AstNodeType_CALAMITY,

    AstNodeType_COMPOUND_LITERAL,
    AstNodeType_BLANK_STATEMENT,
    AstNodeType_PRIM_ARGLIST,

    AstNodeType_DEFINE,
    AstNodeType_RETURN,
    AstNodeType_APPEND
} AstNodeType;

typedef struct __AstNode {
   AstNodeType   type;
   DynamicType * val;
   String * name;
   Location loc;
} AstNode;

AstNodeType AstNode_typeAt(AstNode * self);
DynamicType * AstNode_valAt(AstNode * self);
String * AstNode_nameAt(AstNode * self);
Location AstNode_locationAt(AstNode * self);

AstNodeType AstNode_setType(AstNode * self, AstNodeType type);
DynamicType * AstNode_setVal(AstNode * self, DynamicType * val);
String * AstNode_setName(AstNode * self, String * name);
Location AstNode_setLocation(AstNode * self, Location loc);


AstNode * AstNode_new(AstNodeType type, DynamicType * val, Location loc, String * name);
void AstNode_free(AstNode * self);
void AstNode_freeWhole(AstNode * self);

void AstNode_appendToString(String * str, AstNode * self);
void AstNode_printWide(AstNode * self);
void AstNodeType_printWide(AstNodeType type);
const char * AstNodeType_toConstChars(AstNodeType type);

#define AST_NAME_UNKNOWN  "Unknown"
#define AST_NAME_AND  "and"
#define AST_NAME_EQ  "=="
#define AST_NAME_GEQ  ">="
#define AST_NAME_GREATER  ">"
#define AST_NAME_LEQ  "<="
#define AST_NAME_LESS  "<"
#define AST_NAME_NEQ  "!="
#define AST_NAME_NOT  "!"
#define AST_NAME_OR  "or"
#define AST_NAME_ASSIGN  "="
#define AST_NAME_CONCATENATE  "++"
#define AST_NAME_DIV  "/"
#define AST_NAME_MINUS  "-"
#define AST_NAME_MULT  "*"
#define AST_NAME_PLUS  "+"
#define AST_NAME_POW  "^"
#define AST_NAME_REMAINDER  "%"
#define AST_NAME_LITERAL  "LL"
#define AST_NAME_IDENTIFIER  "ID"
#define AST_NAME_ARGLIST  "Arglist"
#define AST_NAME_BLOCK  "Block"
#define AST_NAME_CASE  "case"
#define AST_NAME_WHILE  "while"
#define AST_NAME_DECLARATION  "Declare"
#define AST_NAME_CALAMITY  "Calamity"
#define AST_NAME_COMPOUND_LITERAL "compound_literal"
#define AST_NAME_BLANK_STATEMENT "Blank statement"
#define AST_NAME_PRIM_ARGLIST "Prim arglist"
#define AST_NAME_DEFINE "define"
#define AST_NAME_RETURN "return"
#define AST_NAME_APPEND "++="

static const char * AST_TYPE_NAMES[] = {
    AST_NAME_UNKNOWN,

    AST_NAME_AND,
    AST_NAME_EQ,
    AST_NAME_GEQ,
    AST_NAME_GREATER,
    AST_NAME_LEQ,
    AST_NAME_LESS,
    AST_NAME_NEQ,
    AST_NAME_NOT,
    AST_NAME_OR,

    AST_NAME_ASSIGN,
    AST_NAME_CONCATENATE,
    AST_NAME_DIV,
    AST_NAME_MINUS,
    AST_NAME_MULT,
    AST_NAME_PLUS,
    AST_NAME_POW,
    AST_NAME_REMAINDER,

    AST_NAME_LITERAL,
    AST_NAME_IDENTIFIER,
    
    AST_NAME_ARGLIST,
    AST_NAME_BLOCK,
    AST_NAME_CASE,
    AST_NAME_WHILE,
    AST_NAME_DECLARATION,
    AST_NAME_CALAMITY,

    AST_NAME_COMPOUND_LITERAL,
    AST_NAME_BLANK_STATEMENT,
    AST_NAME_PRIM_ARGLIST,

    AST_NAME_DEFINE,
    AST_NAME_RETURN,
    AST_NAME_APPEND
};