#pragma once
#include <stdio.h>
#include <stdbool.h>

typedef bool (*AnswerChar)(char);


typedef struct __Character{
    char ch;
} Character;

Character * Character_new(char ch);
void Character_free(Character * self);

char Character_at(Character * self);
char Character_set(Character * self, char ch);

void Character_print(Character * self);
void Character_printWide(Character * self);
void Character_writeTo(FILE * file, Character * self);


signed char Character_compare(Character * left, Character * right);

bool Character_isVowel(Character * self);
bool Char_isVowel(char self);

bool Character_isConsonant(Character * self);
bool Char_isConsonant(char self);