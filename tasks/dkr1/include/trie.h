#pragma once

#include <tree.h>
#include <tree_ext.h>
#include <stdbool.h>
#include <location.h>

// PREFIX TREE

#define FAILURE false
#define SUCCESS true

typedef struct __Trie Trie;

Trie * Trie_new(void);
void Trie_free(Trie * self, Destructor destruct);


// returns true if the inserted value was not previously in the trie
bool Trie_insert(Trie * self, const char * word, void * information);
bool Trie_ninsert(Trie * self, const char * word, void * information, size_t nchars);

void * Trie_remove(Trie * self, const char * word, bool * status);
void * Trie_nremove(Trie * self, const char * word, bool * status, size_t nchars);

void * Trie_search(Trie * self, const char * word, bool * status);
void * Trie_nsearch(Trie * self, const char * word, bool * status, size_t nchars);

bool Trie_isEmpty(Trie * self);

void Trie_print(Trie * self, Location loc, Printer print, bool inAlphabeticOrder);

void * Trie_query(Trie * self, const char * str, const char ** keyEnd);