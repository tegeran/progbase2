#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <list.h>

typedef struct __Tree{
    void * val;
    List * children;
} Tree;

typedef void (*Destructor)(void *);

Tree * Tree_new(void * val);
void Tree_free(Tree * self);
void Tree_freeWhole(Tree * self, Destructor valDestructor);

Tree * Tree_childAt(const Tree * self, size_t index);
Tree * Tree_setChild(Tree * self, size_t index, Tree * child);

void * Tree_at(const Tree * self);
void * Tree_set(Tree * self, void * val);


size_t Tree_size(const Tree * self);

size_t Tree_childrenSize(const Tree * self);
bool Tree_isLeaf(const Tree * self);

Tree * Tree_removeChild(Tree * self, size_t index);
void Tree_insertChild(Tree * self, size_t index, Tree * child);

void Tree_addLastChild(Tree * self, Tree * value);
void Tree_addFirstChild(Tree * self, Tree * value);

