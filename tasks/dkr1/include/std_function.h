#pragma once

#include <dynamictype.h>
#include <dict.h>
#include <interpreter_def.h>
#include <tree.h>

typedef DynamicType * (*StdRoutine)(Calamity * prog, Tree * exec);

typedef struct __Function {
    StdRoutine f;
} Function;

Function * Function_new(StdRoutine func);
void Function_free(Function * self);
StdRoutine Function_at(Function * self);


DynamicType * Calamity_setVarStorage(Calamity * prog, Tree * ast, DynamicType * entity);
DynamicType * Calamity_appendVarStorage(Calamity * prog, Tree * ast, DynamicType * entity);

DynamicType * std_getter   (Calamity * prog, Tree * name);
DynamicType * std_ranger   (Calamity * prog, Tree * name);

DynamicType * std_digify   (Calamity * prog, Tree * name);
DynamicType * std_insert   (Calamity * prog, Tree * name);
DynamicType * std_print    (Calamity * prog, Tree * name);
DynamicType * std_write    (Calamity * prog, Tree * name);
DynamicType * std_read     (Calamity * prog, Tree * name);
DynamicType * std_remove   (Calamity * prog, Tree * name);
DynamicType * std_sizeof   (Calamity * prog, Tree * name);
DynamicType * std_sqrt     (Calamity * prog, Tree * name);
DynamicType * std_stringify(Calamity * prog, Tree * name);
DynamicType * std_typeof   (Calamity * prog, Tree * ast);
DynamicType * std_escape   (Calamity * prog, Tree * ast);
DynamicType * std_array    (Calamity * prog, Tree * ast);