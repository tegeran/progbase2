#pragma once

#include <stdlib.h>

typedef struct __IntList IntList;

IntList * IntList_new(void);
void IntList_free(IntList * self);

void IntList_addFirst(IntList * self, int value);
void IntList_addLast(IntList * self, int value);
void IntList_insert(IntList * self, size_t index, int value);
int IntList_removeAt(IntList * self, size_t index);
size_t IntList_size(IntList * self);
int IntList_at(IntList * self, size_t index);

/**
 *  @returns old value at index
 */
int IntList_set(IntList * self, size_t index, int value);


typedef signed char (*ComparatorInt)(int left, int right);
