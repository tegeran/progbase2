#pragma once

#include <dynamic_array.h> 
#include <stdio.h>

typedef void (*Printer)(void *);

void DynamicArray_print(DynamicArray * self, Printer print);
void DynamicArray_printWide(DynamicArray * self, Printer print);
void DynamicArray_writeTo(FILE * file, DynamicArray * self, Printer print);