#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <list.h>
#include <wide_character.h>
#include <wchar.h>

typedef struct __WideString WideString;

typedef bool (*AnswerWChar)(wchar_t);

// hash functions
size_t WideString_hashify(const WideString * self);
size_t WChars_hashify(const wchar_t * self);
signed char WideString_compare(const WideString * self, const WideString * other);
bool WideString_equals(const WideString * self, const WideString * other);
bool WChars_equals(const wchar_t * self, const wchar_t * other);

// constructors
WideString * WideString_new(void);
void WideString_free(WideString * self);
WideString * WideString_toNewWideString(const wchar_t * wchars);
WideString * WideString_toNewSubstring(const WideString * self, size_t left, size_t right);
WideString * WideString_newCopyOf(const WideString * pattern);
wchar_t * WideString_toNewWChars(const WideString * self);
wchar_t * WideString_toNewSubwchars(const WideString * self, size_t left, size_t right);
// list consists of WideString objects
List * WideString_toNewWords(const WideString * self, AnswerWChar isWordLetter);

wchar_t WideString_at(const WideString * self, size_t index);
size_t WideString_length(const WideString * self);
size_t WideString_memoryUsage(const WideString * self);

wchar_t WideString_set(WideString * self, size_t index, wchar_t token);
void WideString_insert(WideString * self, size_t index, wchar_t token);
//
void WideString_removeBy(WideString * self, AnswerWChar caseRemove);
void WideString_clear(WideString * self);

wchar_t WideString_removeAt(WideString * self, size_t index);
//
void WideString_rewrite(WideString * self, const wchar_t * wchars);

void WideString_concatenate(WideString * self, const WideString * other);
void WideString_append(WideString * self, const wchar_t * wchars);

void WideString_appendWChar(WideString * self, wchar_t wchars);
void WideString_appendSubwchars(WideString * self, const wchar_t * wchars, size_t left, size_t right);
void WideString_appendSubstring(WideString * self, const WideString * pattern, size_t left, size_t right);


void WideString_addWCharFirst(WideString * self, wchar_t wcharacter);
void WideString_addWCharsFirst(WideString * self, wchar_t * wchars);
bool WideString_isEmpty(const WideString * self);

void WideString_print(const WideString * self);
void WideString_writeTo(FILE * file, const WideString * self);


signed long long WideString_searchWChar(const WideString * self, wchar_t template);
signed long long WideString_searchSpecWChar(const WideString * self, AnswerWChar isTarget);
signed long long WideString_searchSubwchars(const WideString * self, const wchar_t * template);
signed long long WideString_searchSubstring(const WideString * self, const WideString * template);
signed long long WideString_searchLastSpecWChar(const WideString * self, AnswerWChar isTarget);
signed long long WideString_searchLastWChar(const WideString * self, wchar_t template);

void WideString_swap(WideString * self, size_t apple, size_t jack);
void WideString_reverse(WideString * self);
void WideString_trim(WideString * self);
void WideString_reduce(WideString * self, size_t newLength); // length does not include '\0'
void WideString_cut(WideString * self, size_t left, size_t right);
size_t WideString_readFrom(FILE * file, WideString * self);
size_t WideString_appendFrom(FILE * file, WideString * self);

size_t WideString_writeFormat(WideString * self, wchar_t * fmt, ...);
size_t WideString_appendFormat(WideString * self, wchar_t * fmt, ...);


void WideString_appendCsvFormatedWChars(WideString * self, const wchar_t * src);
void WideString_appendCsvFromated(WideString * self, const WideString * src);

enum {CSV_WSTR_DEFORMAT_ERROR = -2,
      CSV_WSTR_DEFORMAT_UTERMINATED_STRING = -1
};
signed long long WideString_appendCsvDeformated(WideString * self, const WideString * src, size_t start);