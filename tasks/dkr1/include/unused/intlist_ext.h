#pragma once

#include <intlist.h>
#include <stdbool.h>

// value maintained in list is supposed to be Integer 

typedef bool (*AnswerInt)(int val);

IntList * IntList_newArrayCopy(int * array, size_t size);
IntList * IntList_newInit(size_t size, int values);
IntList * IntList_newInitRandom(size_t size, int seed, int min, int max);

void IntList_print(IntList * self);
void IntList_printWide(IntList * self);
void IntList_removeBy(IntList * self, AnswerInt caseRemove);


