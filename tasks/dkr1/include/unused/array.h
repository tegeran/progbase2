#pragma once
#include <stdlib.h>
#include <stdbool.h>

typedef struct __Array{
    void ** a;
    size_t size;
} Array;

typedef struct __IntArray{
    signed int * a;
    size_t size;
} IntArray;

typedef struct __UIntArray{
    unsigned int * a;
    size_t size;
} UIntArray;

typedef struct __CharArray{
    signed char * a;
    size_t size;
} CharArray;

typedef struct __UCharArray{
    unsigned char * a;
    size_t size;
} UCharArray;

typedef struct __LongArray{
    signed long int * a;
    size_t size;
} LongArray;


typedef struct __ULongArray{
    unsigned long int * a;
    size_t size;
} ULongArray;

typedef struct __LongLongArray{
    signed long long int * a;
    size_t size;
} LongLongArray;

typedef struct __ULongLongArray{
    signed long long int * a;
    size_t size;
} ULongLongArray;


typedef struct __DoubleArray{
    double * a;
    size_t size;
} DoubleArray;

typedef struct __FloatArray{
    float * a;
    size_t size;
} FloatArray;

typedef struct __BoolArray{
    bool * a;
    size_t size;
} BoolArray;

typedef signed char (*Comparator)(void * left, void * right);

Array * Array_new(size_t size);
void Array_free(Array * self);

size_t Array_sizeAt(Array * self);
void * Array_at(Array * self, size_t index);
void ** Array_arrayAt(Array * self);


size_t Array_setSize(Array * self, size_t size);
void * Array_set(Array * self, size_t index, void * val);
void ** Array_setArray(Array * self, void ** array);


void Array_swap(Array * self, size_t little, size_t pip);
signed long long Array_binarySearch(Array * self, void * template, Comparator compare);
signed long long Array_linearSearch(Array * self, void * template, Comparator compare);

void Array_fill(Array * self, void * value);
void Array_reverse(Array * self);
void Array_shift(Array * self, signed int shift);
void Array_nullify(Array * self);