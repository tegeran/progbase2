#pragma once

#include <stdbool.h>
#include <stdlib.h>

#define DynamicArray_INITIAL_CAPACITY 16


typedef bool (*Answer)(void *);
typedef void (*Destructor)(void *);
typedef struct __DynamicArray DynamicArray;


DynamicArray * DynamicArray_new(void);
void DynamicArray_free(DynamicArray * self);
void DynamicArray_freeWhole(DynamicArray * self, Destructor destroy);


void * DynamicArray_at(DynamicArray * self, size_t index);
size_t DynamicArray_size(DynamicArray * self);
size_t DynamicArray_memoryUsage(DynamicArray * self);

void * DynamicArray_set(DynamicArray * self, size_t index, void * value);
void DynamicArray_insert(DynamicArray * self, size_t index, void * value);

//
void DynamicArray_removeBy(DynamicArray * self, Answer caseRemove, Destructor destroyValue);
void DynamicArray_clear(DynamicArray * self, Destructor destructVal);

// NOTE: first node has index 0

void * DynamicArray_removeAt(DynamicArray * self, size_t index);

// adds a new object to the end of list (time complexity O(1))
void DynamicArray_addLast(DynamicArray * self, void * value);
void DynamicArray_addFirst(DynamicArray * self, void * value);
bool DynamicArray_isEmpty(DynamicArray * self);