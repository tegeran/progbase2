#pragma once 
#include <unused/bintree.h>
#include <wchar.h>
#include <location.h>

typedef void (*Printer)(void *);
typedef signed char (*Comparator)(void *, void *); // returns -1 if left val is greater
                                                   // retruns  0 if values are equal
                                                   // returns  1 if right val is greater

// used for fancy tree printing (totally changable)
const static wchar_t TREE_VLINE = L'┃';
const static wchar_t TREE_RIGHT_INBRANCH[] = L"┣╾───⬏ ";
const static wchar_t TREE_RIGHT_OUTBRANCH[] = L"┗╾───⬏ ";
const static wchar_t TREE_LEFT_OUTBRANCH[] = L"┗╾─⬎ ";

BinTree * BinTree_newInt(int val);
void BinTree_freeInt(BinTree * self);

void BinTree_freeWhole(BinTree * self, Destructor valDestructor);

int BinTree_getInt(BinTree * self);
int BinTree_setInt(BinTree * self, int val);

bool BinTree_contains(BinTree * self, void * val, Comparator compare);
BinTree * BinTree_search(BinTree * self, void * val, Comparator compare);

void BinTree_printIntFormat(BinTree * self);
void BinTree_printIntInOrder(BinTree * self);
void BinTree_printFancy(BinTree * self, Location loc, Printer print);