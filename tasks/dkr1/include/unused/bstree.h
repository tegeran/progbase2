#pragma once

#include <stdbool.h>
#include <unused/bintree.h>
#include <stdlib.h>
#include <location.h>

typedef struct __BSTree BSTree;

BSTree * BSTree_new(void);
void BSTree_free(BSTree * self);
void BSTree_freeWhole(BSTree * self);

void BSTree_insert(BSTree * self, int key);

void BSTree_clear(BSTree * self);
BinTree * BSTree_root(BSTree * self);

bool BSTree_isEmpty(BSTree * self);
bool BSTree_contains(BSTree * self, int val);

void BSTree_printFormat(BSTree * self);
void BSTree_printInOrder(BSTree * self);

void BSTree_insertArr(BSTree * self, int * arr, size_t size);
void BSTree_printFancy(BSTree * self, Location loc);