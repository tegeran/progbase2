#pragma once

#include <list.h> 
#include <stdio.h>

typedef signed char (*Comparator)(void *, void *);
typedef void (*Printer)(void *);

signed long long List_linearSearch(List * self, void * key, Comparator compare);

List * List_newArrayCopy(void ** array, size_t size);
List * List_newArrayCopyOfRange(void ** array, size_t from, size_t to);
List * List_newListCopy(List * source);
List * List_newListCopyOfRange(List * source, size_t from, size_t to);

List * List_newUnited(List ** srcLists, size_t size);

void List_arrayInsert(List * self, size_t listIndex, void ** array, size_t size);
void List_arrayAppend(List * self, void ** array, size_t size);

void List_listAppend(List * self, List * source);
void List_listInsert(List * self, size_t selfIndex, List * source);


void List_merge(List * self, List * source, Comparator precendence);

void List_print(List * self, Printer print);
void List_printWide(List * self, Printer print);
void List_writeTo(FILE * file, List * self, Printer print);

size_t List_addToSorted(List * self, void * entity, Comparator compare);