#pragma once

#include <location.h>
#include <stdio.h>
#include <wchar.h>

void Cui_setFgColor(int id);  // id has 3 digits first defines RED, second GREEN, third BLUE

void Cui_setBgColor(int id);

void Cui_setFgColorWide(int id);

void Cui_setBgColorWide(int id);

void Cui_move(Location loc);
void Cui_moveWide(Location loc);


void Cui_setFgColorOf(FILE * stream, int id);  // id has 3 digits first defines RED, second GREEN, third BLUE

void Cui_setBgColorOf(FILE * stream,int id);

void Cui_setFgColorWideOf(FILE * stream, int id);

void Cui_setBgColorWideOf(FILE * stream, int id);

void Cui_printHLine(unsigned short size, char token);
void Cui_printVLine(unsigned short size, char token, Location loc);

void Cui_printHLineWide(unsigned short size, wchar_t token);
void Cui_printVLineWide(unsigned short size, wchar_t token, Location loc);
Location Cui_getLocation(void);