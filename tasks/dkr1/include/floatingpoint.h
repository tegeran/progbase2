#pragma once
#include <stdio.h>

#define FLOAT_PRECISION 1e-6f


typedef struct __Float {
    float f;
} Float;

Float * Float_new(float f);
void Float_free(Float * self);

float Float_at(Float * self);
float Float_set(Float * self, float f);

void Float_print(Float * self);
void Float_printWide(Float * self);
void Float_writeTo(FILE * file, Float * self);

signed char Float_compare(Float * self, Float * other);
signed char float_compare(float self, float other);