#pragma once
/*
 * Copyright (C) Vitaly Kryvenko 2018.
 * e-mail:   gerzoh1@gmail.com
 * Telegram: @Simpliest_One
 * 
 * benedictions to my teacher Ruslan Gadyniak
 */ 


#include <stdbool.h>
#include <stdlib.h>
#include <enumerator.h>
#include <iterator.h>

typedef struct __List List;

typedef void (*Destructor)(void *);
typedef bool (*Answer)(void *);
typedef signed char (*Comparator)(void * left, void * right);

// Implements Enumerator (enumerable)
Enumerator * List_getNewEnumerator(const List * self);

// Implements Iterator (iterable)
Iterator * List_getNewBeginIterator (const List * self);
Iterator * List_getNewEndIterator   (const List * self);


// Constructors and destructors
// AHTUNG PROGRAMMIERER! Constructors abort workflow if experienced heap overflow

List * List_new(void);
List * List_newCap(size_t size, void * init);

void List_free(List * self);
// added:
void List_freeWhole(List * self, Destructor valDestructor);
//
void List_removeBy(List * self, Answer caseRemove, Destructor valDestructor);
void List_clear(List * self, Destructor destructVal);
 
// NOTE: first node has index 0
// beware: const qualifier provides immutability of list values
// but its internal structure may be changed

void * List_at(const List * self, size_t index);
void * List_atLast(const List * self);
void * List_atFirst(const List * self);

void * List_set(List * self, size_t index, void * value);
void * List_setFirst(List * self, void * value);
void * List_setLast(List * self, void * value);


void * List_removeAt(List * self, size_t index);
void * List_removeFirst(List * self);
void * List_removeLast(List * self);
void   List_cut(List * self, size_t left, size_t right, Destructor destroy);

void * List_remove(List * self, void * pattern, Comparator caseRemove);

void List_insert(List * self, size_t index, void * value);

size_t List_size(const List * self);

// adds a new object to the end of list (time complexity O(1))
void List_addLast(List * self, void * value);
void List_addFirst(List * self, void * value);
bool List_isEmpty(const List * self);

void List_insertionSort(List * self, Comparator compare);