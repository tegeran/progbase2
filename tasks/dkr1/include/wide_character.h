#pragma once
#include <stdio.h>
#include <stdbool.h>
#include <wchar.h>

typedef bool (*AnswerWChar)(wchar_t);


typedef struct __WideCharacter{
    wchar_t ch;
} WideCharacter;

WideCharacter * WideCharacter_new(wchar_t ch);
void WideCharacter_free(WideCharacter * self);

wchar_t WideCharacter_at(WideCharacter * self);
wchar_t WideCharacter_set(WideCharacter * self, wchar_t ch);

void WideCharacter_print(WideCharacter * self);
void WideCharacter_writeTo(FILE * file, WideCharacter * self);


signed char WideCharacter_compare(WideCharacter * left, WideCharacter * right);

bool WideCharacter_isVowel(WideCharacter * self);
bool WChar_isVowel(wchar_t self);

bool WideCharacter_isConsonant(WideCharacter * self);
bool WChar_isConsonant(wchar_t self);