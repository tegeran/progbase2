#pragma once 
#include <stdbool.h>
#include <list.h>
#include <tree.h>
#include <ast.h>


Tree * Parser_buildNewAstTree(const register List * tokens, const String * code);
