#pragma once

#include <tree.h>
#include <wchar.h>
#include <location.h>
#include <sstring.h>

typedef signed char (*Comparator)(void *, void *); // returns 0 if values are equal
typedef void (*Printer)(void *);

// used for fancy tree printing (totally changable)
const static wchar_t TREE_VLINE = L'┃';
const static wchar_t TREE_INBRANCH[] = L"┣╾⟶ ";
const static wchar_t TREE_OUTBRANCH[] = L"┗╾⟶ ";

void Tree_printFancy(Tree * self, Location loc, Printer print, bool fromStart);
signed long long Tree_linearSearchChild(Tree * self, void * key, Comparator compare);
signed long long Tree_sortedLinearSearchChild(Tree * self, void * key, Comparator compare);

void Tree_appendToString(Tree * self, String * dest, StringWriter _write, bool fromStart);