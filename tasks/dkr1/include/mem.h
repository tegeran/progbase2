
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// UNIVERSAL TESTING FUNCTIONS

#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <sstring.h>
#include <list.h>

//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS quantity of symbols read if operation was successful or FAIL if not
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// writes content from file located at (filePath) to (string)
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
size_t Mem_readFile(const char * filePath, char *string, size_t stringSize);
size_t Mem_getFileSize(FILE * file);

bool Mem_fileExists(const char * filePath);


typedef enum{
    MEM_FAILURE = -1,
    MEM_SUCCESS
} MemStatus;

void * Mem_reallocate(void * ptr, size_t size);
void * Mem_malloc(size_t size);
void * Mem_calloc(size_t size);

typedef enum {
    MEM_REG_FILE,
    MEM_DIR,
    MEM_INDIR,
    MEM_DEFAULT
} MemDirType;

MemStatus Mem_getDirNames (const char   * directory, MemDirType type, List * listOfNames);
MemStatus Mem_sgetDirNames(const String * directory, MemDirType type, List * listOfNames);

void Mem_printFilesStatistic(const char * path,  size_t * letters, size_t * lines, size_t * words);