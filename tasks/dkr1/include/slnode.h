#pragma once

#include <stdlib.h>
#include <slnode.h>

typedef struct __SLNode{
    void * val;
    struct __SLNode * next;
} SLNode;

typedef void (* Destructor)(void *);
void SLNode_freeWhole(SLNode * self, Destructor valDestructor);

// Construcor and destructor
// AHTUNG PROGRAMMIERER! Constructor aborts workflow if experineced heap overflow
SLNode * SLNode_new(void * val);
void SLNode_free(SLNode * self);


// Getters
void * SLNode_at(SLNode * self);
SLNode * SLNode_getNext(SLNode * self);

// Setters
void * SLNode_set(SLNode * self, void * newVal);
SLNode * SLNode_setNext(SLNode * self, SLNode * next);

void * SLNode_removeNext(SLNode * self);
SLNode * SLNode_popNext(SLNode * self);
void SLNode_addAfter(SLNode * self, SLNode * newbie);