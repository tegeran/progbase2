#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include "pupiltablemodel.h"
#include <QVector>
#include "pupil.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow){

	ui->setupUi(this);
	ui->tableView->setEditTriggers(  QAbstractItemView::DoubleClicked
								   | QAbstractItemView::SelectedClicked
								   | QAbstractItemView::EditKeyPressed);

	ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui->tableView->setModel(new PupilTableModel(QVector<Pupil>(), this));
	connect(ui->tableView->selectionModel(),
			SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
			this,
			SLOT(checkDeleteButton())
	);
}

MainWindow::~MainWindow(){
	delete ui;
}

inline PupilTableModel * MainWindow::model(){
	return static_cast<PupilTableModel *>(ui->tableView->model());
}

inline QModelIndexList MainWindow::selectedRows(){
	return ui->tableView->selectionModel()->selectedRows();
}

void MainWindow::checkDeleteButton(){
	ui->button_delete->setEnabled(selected());
}

void MainWindow::on_button_insert_clicked(){
	if (selected()){
		model()->insertRow(ui->tableView->currentIndex().row());
		checkDeleteButton();
	} else {
		on_button_add_clicked();
	}
}

void MainWindow::on_button_add_clicked(){
	model()->insertRow(model()->rowCount());
	checkDeleteButton();
}

void MainWindow::on_button_delete_clicked(){
	QModelIndexList selected = selectedRows();
	qSort(selected);
	for (int i = selected.size() - 1; i >= 0; --i){
		model()->removeRow(selected[i].row());
	}
	checkDeleteButton();
}

inline bool MainWindow::selected() const{
	return ui->tableView->selectionModel()->selection().indexes().size();
}

void MainWindow::on_action_reset_triggered(){
	PupilTableModel &model = *this->model();
	for (const QModelIndex &index : selectedRows()){
		model.reset(index);
	}
}

void MainWindow::on_action_random_triggered(){
	PupilTableModel &model = *this->model();
	for (const QModelIndex &index : selectedRows()){
		model.randomize(index);
	}
}
