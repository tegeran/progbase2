#ifndef QEXT
#define QEXT
#include <QString>
#include <string>
#include "message_exception.h"
#include <QException>
#include <QJsonDocument>
#include <location.h>
#include <QDomDocument>

namespace Qext {

	QString randomString(const int &size, const QChar &max = QChar(0xff - 1));
	QString randomString(const int &size, const QChar &min, const QChar &max);
	int random(const int &right);   // right both inclusive
	int random(const int &left, const int &right);

    namespace File {


        QString read(const QString & path); // throws MessageException
        void write(const QString & src, const QString & path); // throws MessageException

        namespace Json {
            class JsonException : public MessageException {
            private:
                int m_offset;
            public:
                inline int offset();
                JsonException(const char * const & msg, const int & offset);
                JsonException(std::string msg, const int & offset);
                JsonException(const QString &msg, const int & offset);
            };


            QJsonDocument read(const QString & path); // throws JsonException
            void write(const QJsonDocument & document, const QString & path); // throws MessageException
        }

        namespace Xml {
            class XmlException : public MessageException {
            private:
                Location<int> m_loc;
            public:
                inline Location<int> location();
                inline int y();
                inline int x();

                XmlException(const char * const & msg, const int &y, const int &x);
                XmlException(std::string msg, const int &y, const int &x);
                XmlException(const QString& msg, const int &y, const int &x);

            };


            QDomDocument read(const QString & path);
            void write(const QDomDocument & document, const QString & path);  // throws MessageException
        }
    }


}




#endif // QEXT

