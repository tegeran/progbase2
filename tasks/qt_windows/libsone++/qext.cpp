#include "qext.h"
#include <QFile>
#include <QTextStream>
#include <QByteArray>
#include "message_exception.h"

namespace Qext {
	QString randomString(const int &size, const QChar &max){
		QString str;
		str.reserve(size);
		for (int i = 0; i < size; ++i){
			str[i] = QChar(random(1, max.unicode()));
		}
		return str;
	}


	QString randomString(const int &size, const QChar &min, const QChar &max){
		QString str;
		str.reserve(size);
		for (int i = 0; i < size; ++i){
			str[i] = QChar(random(min.unicode(), max.unicode()));
		}
		return str;
	}

    namespace File {
        QString read(const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::ReadOnly)){
                throw MessageException("failed to read file \"" + path.toStdString() + '\"');
            }
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            return content;
        }

        void write(const QString &src, const QString &path){
            QFile file(path);
            if (!file.open(QIODevice::WriteOnly)){
                throw MessageException("failed to open file \""
                                       + path.toStdString()
                                       + "\" for writing");
            }
            QTextStream stream;
            stream << src;
            file.close();
        }

        namespace Json {
            QJsonDocument read(const QString &path){
                QByteArray dataBytes;
                {
                    QString content = ::Qext::File::read(path);
                    dataBytes.append(content);
                }
                QJsonParseError err;
                QJsonDocument document = { QJsonDocument::fromJson(dataBytes, &err) };
                if (err.error != QJsonParseError::NoError){
                    throw JsonException("failed while parsing json file ("
                                        + err.errorString().toStdString() + ")", err.offset);
                }
                return document;
            }

            inline int JsonException::offset(){
                return m_offset;
            }

            JsonException::JsonException(const char * const &msg, const int &offset)
                : MessageException(msg), m_offset(offset) {}

            JsonException::JsonException(std::string msg, const int &offset)
                : MessageException(std::move(msg)), m_offset(offset) {}

            JsonException::JsonException(const QString &msg, const int &offset)
                : MessageException(msg.toStdString()), m_offset(offset) {}

            void write(const QJsonDocument &document, const QString &path){
                QFile file(path);
                if (!file.open(QIODevice::WriteOnly)){
                    throw MessageException("failed to write json document to \""
                                           + path.toStdString() + "\"");
                }
                QTextStream stream(&file);
                stream << document.toJson();
                file.close();
            }

        }

        namespace Xml {
            int XmlException::y(){
                return m_loc.y;
            }

            int XmlException::x(){
                return m_loc.x;
            }

            Location<int> XmlException::location(){
                return m_loc;
            }

            XmlException::XmlException(const char * const &msg, const int &y, const int &x)
                : MessageException(msg), m_loc(y, x) {}

            XmlException::XmlException(const QString &msg, const int &y, const int &x)
                : MessageException(msg.toStdString()), m_loc(y, x) {}

            XmlException::XmlException(std::string msg, const int &y, const int &x)
                : MessageException(std::move(msg)), m_loc(y, x) {}


            QDomDocument read(const QString &path){
                QFile file(path);
                QString err;
                Location<int> eloc;
                QDomDocument document;
                if (!document.setContent(&file, &err, &eloc.y, &eloc.x)){
                    file.close();
                    throw MessageException("failed while parsing xml file ("
                                           + err.toStdString()
                                           + "), error occured at "
                                           + eloc.toStdString());
                }
                file.close();
                return document;
            }

            void write(const QDomDocument &document, const QString & path){
                QFile file(path);
                if (!file.open(QIODevice::WriteOnly)){
                    throw MessageException("failed to write xml document to \""
                                           + path.toStdString() + "\"");
                }
                QTextStream stream(&file);
                stream << document.toString();
                file.close();
            }
        }

	}

	int random(const int &right){
		return qrand() % (right + 1);
	}

	int random(const int &left, const int &right){
		return (qrand() % (right - left + 1)) + left;
	}

}
