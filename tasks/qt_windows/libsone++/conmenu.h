//
// Created by tegeran on 20.03.18.
//
#include <vector>
#include <string>
#include "location.h"

#ifndef A_OUT_CONMENU_H
#define A_OUT_CONMENU_H


class ConMenu {
private:

    std::vector<const char * >  options;
               unsigned short   actual;
    Location < unsigned short > location;

    void advanceDown();
    void advanceUp();


public:
    explicit ConMenu(std::vector<const char * > & options);

    void post(unsigned short y = 1, unsigned short x = 1);
    void refresh();
    void unpost();
    // returns -1 if escape character was fed
    signed long long listen();
};


#endif //A_OUT_CONMENU_H
