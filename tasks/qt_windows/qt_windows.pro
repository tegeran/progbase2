#-------------------------------------------------
#
# Project created by QtCreator 2018-04-12T16:20:08
#
#-------------------------------------------------

QT       += core gui
QT += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = qt_windows
TEMPLATE = app

CONFIG += c++14

INCLUDEPATH += libsone++
SOURCES += main.cpp\
           mainwindow.cpp\
           pupil.cpp \
       pupiltablemodel.cpp\
libsone++/conmenu.cpp\
libsone++/cui.cpp\
libsone++/error.cpp\
libsone++/message_exception.cpp\
libsone++/qext.cpp\
libsone++/std_ext.cpp


HEADERS  += mainwindow.h\
            pupil.h \
    pupiltablemodel.h\
libsone++/conmenu.h\
libsone++/cui.h\
libsone++/error.h\
libsone++/message_exception.h\
libsone++/qext.h\
libsone++/std_ext.h
libsone++/location.h

FORMS    += mainwindow.ui

