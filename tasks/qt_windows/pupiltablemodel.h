#ifndef PUPILTABLEMODEL_H
#define PUPILTABLEMODEL_H

#include <QAbstractTableModel>
#include "pupil.h"
#include <QVector>

class PupilTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	enum Column{
		Name,
		Class,
		Age,
		Score,
		Alive
	};




	explicit PupilTableModel(const QVector<Pupil> &pups, QObject *parent = nullptr);
	explicit PupilTableModel(      QVector<Pupil> &&pups, QObject *parent = nullptr);

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Editable:
	bool setData(const QModelIndex &index, const QVariant &value,
				 int role = Qt::EditRole) override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

	// Add data:
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	// Remove data:
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

	void reset(const QModelIndex &pup);
	void randomize(const QModelIndex &pup);
private:
	QVector<Pupil> pups;
};

#endif // PUPILTABLEMODEL_H
