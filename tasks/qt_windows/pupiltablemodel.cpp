#include "pupiltablemodel.h"
#include <utility>
#include <QString>
#include "error.h"
#include "libsone++/std_ext.h"
#include <limits>
#include <QDebug>

const static QString COL_HEADERS[] = {
	QStringLiteral("Name" ),
	QStringLiteral("Class"),
	QStringLiteral("Age"  ),
	QStringLiteral("Score"),
	QStringLiteral("Alive")
};

PupilTableModel::PupilTableModel(const QVector<Pupil> &pups, QObject *parent)
	: QAbstractTableModel(parent), pups(pups) {}
PupilTableModel::PupilTableModel(      QVector<Pupil> &&pups, QObject *parent)
	: QAbstractTableModel(parent), pups(std::move(pups)) {}

QVariant PupilTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (role != Qt::DisplayRole || section < 0)
		return QVariant();
	switch (orientation){
		case Qt::Orientation::Vertical:{
			return section < pups.size()
					? QVariant(QString::number(section + 1))
					: QVariant();
		}
		case Qt::Orientation::Horizontal:{
			return static_cast<size_t>(section) < std_ext::size(COL_HEADERS)
					? QVariant(COL_HEADERS[section])
					: QVariant();
		}
	}
	Debug_shutdown("Undefined behaviour (core not dumped)");
	return QVariant();
}

bool PupilTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role){
	Q_UNUSED(section);
	Q_UNUSED(orientation);
	Q_UNUSED(value);
	Q_UNUSED(role);
	return false;
}


int PupilTableModel::rowCount(const QModelIndex &parent) const{
	Q_UNUSED(parent)
	return pups.size();
}

int PupilTableModel::columnCount(const QModelIndex &parent) const{
		Q_UNUSED(parent);
		return Column::Alive + 1;
}

QVariant PupilTableModel::data(const QModelIndex &index, int role) const{
	if (!index.isValid()
		|| index.row() >= pups.size())
		return QVariant();

	switch (role){
		case Qt::EditRole:
		case Qt::DisplayRole: {
			switch (index.column()){
				case Column::Name:{
					return QVariant(pups[index.row()].name);
				}
				case Column::Class:{
					return QVariant(pups[index.row()]._class);
				}
				case Column::Age:{
					return QVariant(pups[index.row()].age);
				}
				case Column::Score:{
					return QVariant(static_cast<double>(pups[index.row()].score));
				}
				case Column::Alive:{
					return QVariant(pups[index.row()].alive);
				}
				default: return QVariant();
			}
		}
		default: return QVariant();
	}
	Debug_shutdown("Undefined behaviour (core not dumped)");
	return QVariant();
}

bool PupilTableModel::setData(const QModelIndex &index, const QVariant &value, int role){		
	if (!index.isValid()
		|| role != Qt::EditRole
		|| index.row() >= pups.size())
		return false;

	switch (index.column()){
		case Column::Name:{
			if (value.type() != QVariant::String)
				return false;

			pups[index.row()].name = value.toString();
			break;
		}
		case Column::Class:{
			if (value.type() != QVariant::String)
				return false;

			pups[index.row()]._class = value.toString();
			break;
		}
		case Column::Age:{
			if (value.type() != QVariant::Int)
				return false;

			int newAge = value.toInt();
			if (   newAge < 0
				|| newAge > std::numeric_limits<short>::max())
				return false;

			pups[index.row()].age = static_cast<short>(newAge);
			break;
		}
		case Column::Score:{
			qDebug() << value.typeName();
			if (value.type() != QVariant::Double)
				return false;

			pups[index.row()].score = value.toFloat();
			break;
		}
		case Column::Alive:{
			if (value.type() != QVariant::Bool) return false;

			pups[index.row()].alive = value.toBool();
			break;
		}
		default: return false;
	}
	emit dataChanged(index, index, QVector<int>() << role);
	return true;
}

Qt::ItemFlags PupilTableModel::flags(const QModelIndex &index) const{
	if (!index.isValid() || index.row() >= pups.size())
		return Qt::NoItemFlags;

	if (index.column() == Column::Alive){
		return Qt::ItemIsUserCheckable
			 | Qt::ItemIsEditable
			 | Qt::ItemIsSelectable
			 | Qt::ItemIsEnabled
			 | Qt::ItemNeverHasChildren;
	}

	return Qt::ItemIsEditable
		 | Qt::ItemIsSelectable
		 | Qt::ItemIsEnabled
		 | Qt::ItemNeverHasChildren;
}

bool PupilTableModel::insertRows(int row, int count, const QModelIndex &parent){
	beginInsertRows(parent, row, row + count - 1);

	while (count--){
		pups.insert(row, Pupil());
	}
	endInsertRows();
	return true;
}

bool PupilTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
	beginRemoveRows(parent, row, row + count - 1);
	while (count--){
		if (row >= pups.size()){
			qDebug() << "Panic";
		}
		pups.removeAt(row);
	}
	endRemoveRows();
	return true;
}

void PupilTableModel::reset(const QModelIndex & pup){
	Debug_suppose(pup.isValid(), "Invalid index");
	pups[pup.row()].reset();
	emit dataChanged(pup, createIndex(pup.row(), Column::Alive));
}

void PupilTableModel::randomize(const QModelIndex & pup){
	pups[pup.row()].randomize();
	emit dataChanged(pup, createIndex(pup.row(), Column::Alive));
}
