#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "pupil.h"
#include <QVector>
#include <QAbstractItemModel>
#include "pupiltablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
	explicit MainWindow(QWidget * parent = nullptr);
    ~MainWindow();
	inline PupilTableModel * model();
	inline QModelIndexList selectedRows();
	inline bool selected() const;

private slots:
	void on_button_insert_clicked();
	void on_button_add_clicked();
	void on_button_delete_clicked();
	void checkDeleteButton();

	void on_action_reset_triggered();

	void on_action_random_triggered();

private:
	Ui::MainWindow * ui;
};

#endif // MAINWINDOW_H
