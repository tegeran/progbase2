#include "pupil.h"
#include <string>
#include <vector>
#include <utility>
#include "qext.h"

Pupil::Pupil(QString        name,
             QString        _class,
             unsigned short age,
             float          score,
             bool           alive)

    : name     (std::move(name  )),
      _class   (std::move(_class)),
      age      (std::move(age   )),
      score    (std::move(score )),
      alive    (std::move(alive )) {}

Pupil::Pupil(const Pupil &toCopy)
    : Pupil(
         toCopy.name,
         toCopy._class,
         toCopy.age,
         toCopy.score,
         toCopy.alive
          ) {}

Pupil::Pupil(Pupil &&toMove)
  : name     (std::move(toMove.name  )),
    _class   (std::move(toMove._class)),
    age      (std::move(toMove.age   )),
    score    (std::move(toMove.score )),
    alive    (std::move(toMove.alive )) {

    toMove.age = 0;
    toMove.score = 0;
    toMove.alive = false;
}

Pupil & Pupil::operator=(const Pupil &toCopyAssign){
    this->name      = toCopyAssign.name;
    this->_class    = toCopyAssign._class;
    this->age       = toCopyAssign.age;
    this->score     = toCopyAssign.score;
    this->alive     = toCopyAssign.alive;
    return *this;
}

Pupil &Pupil::operator=(Pupil &&toMoveAssign){
    this->name      = std::move(toMoveAssign.name     );
    this->_class    = std::move(toMoveAssign._class   );
    this->age       = std::move(toMoveAssign.age      );
    this->score     = std::move(toMoveAssign.score    );
    this->alive     = std::move(toMoveAssign.alive    );
    toMoveAssign.age = 0;
    toMoveAssign.score = 0;
    toMoveAssign.alive = false;
	return *this;
}

void Pupil::randomize(){					// 20 is a random limitation
	this->name   = Qext::randomString(Qext::random(20), '0', 'z');
	this->_class = Qext::randomString(Qext::random(20), '0', 'z');
	this->age    = Qext::random(-1, 120);
	this->score  = Qext::random(-1, 100);
	this->alive  = Qext::random(0, 1);
}

void Pupil::reset(){
	this->name   = "no name";
	this->_class = "no class";
	this->age    = 0;
	this->score  = 0;
	this->alive  = true;
}


