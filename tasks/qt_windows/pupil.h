#ifndef PUPIL_H
#define PUPIL_H
#include <QString>
#include <QMetaType>

class Pupil{
public:
    QString         name;
    QString         _class;
    unsigned short  age;
    float           score;
    bool            alive;

    explicit Pupil(
            QString        name      = "no name",
            QString        _class    = "no class",
            unsigned short age       = 0,
            float          score     = 0,
            bool           alive     = true);

    Pupil(const Pupil & toCopy);
    Pupil(Pupil && toMove);
    Pupil & operator=(const Pupil & toCopyAssign);
    Pupil & operator=(Pupil && toMoveAssign);
	void randomize();
	void reset();
};

Q_DECLARE_METATYPE(Pupil)

#endif // PUPIL_H
