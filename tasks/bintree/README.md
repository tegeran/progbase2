# sample-cmake-project-c
Sample Project with CMake build for C

Fast build from root directory:
```sh
sh build.sh
```

Fast run from root directory:
```sh
./build/a.out
```
