#pragma once


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


// Здається мені це погана ідея заради зберігання 4 байтів інформації витрачати ще 8 байтів додаткових

typedef struct __Integer Integer;

// sets a seed for Integer_newRandom() constructor
void Integer_initRandomness(unsigned int seed);

// Constructors abort workflow if experienced heap overflow
Integer * Integer_new(int val);

Integer * Integer_newRandom(int min, int max);


int Integer_set(Integer * self, int val);
int Integer_get(Integer * self);

void Integer_free(Integer * self);


// prints integer to stdout
void Integer_print(Integer * self);
void Integer_printWide(Integer * self);

// retruns true if Integer value is negative
bool Integer_isNegative(Integer * self);

signed char Integer_compare(Integer * left, Integer * right);