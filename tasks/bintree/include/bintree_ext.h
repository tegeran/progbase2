#pragma once 
#include <bintree.h>
#include <wchar.h>

typedef void (*Printer)(void *);
typedef signed char (*Comparator)(void *, void *); // returns -1 if left val is greater
                                                   // retruns  0 if values are equal
                                                   // returns  1 if right val is greater

// used for fancy tree printing (totally changable)
const static wchar_t TREE_VLINE = L'┃';
const static wchar_t TREE_RIGHT_INBRANCH[] = L"┣╾───⬏ ";
const static wchar_t TREE_RIGHT_OUTBRANCH[] = L"┗╾───⬏ ";
const static wchar_t TREE_LEFT_OUTBRANCH[] = L"┗╾─⬎ ";

void BinTree_freeWhole(BinTree * self, Destructor valDestructor);

// Comparator is used to find a root to the place where val should be
// use this function if the tree is formed according to a particular rule
BinTree * BinTree_searchOrdered(BinTree * self, void * val, Comparator compare);
BinTree * BinTree_search(BinTree * self, void * val, Comparator compare);



void BinTree_printFormat(BinTree * self, Printer print);
void BinTree_printInOrder(BinTree * self, Printer print);
void BinTree_printFancy(BinTree * self, unsigned short y, unsigned short x, Printer print);