#pragma once
#include <stdbool.h>

typedef struct __BinTree BinTree;
typedef void (*Destructor)(void *);


BinTree * BinTree_new(void * value);
void BinTree_free(BinTree * self);
void BinTree_freeAll(BinTree * self, Destructor valDestructor);

void * BinTree_at(BinTree * self);
void * BinTree_set(BinTree * self, void * value);

BinTree * BinTree_getLeft(BinTree * self);
BinTree * BinTree_getRight(BinTree * self);

BinTree * BinTree_setLeft(BinTree * self, BinTree * left);
BinTree * BinTree_setRight(BinTree * self, BinTree * right);

bool BinTree_hasLeft(BinTree * self);
bool BinTree_hasRight(BinTree * self);
bool BinTree_isLeaf(BinTree * self);