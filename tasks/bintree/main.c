#include <bstree.h>
#include <locale.h>
#include <wchar.h>
#include <assert.h>

int main(void) {

    system("clear");

    setlocale(LC_ALL, "");

    int sequence[] = {14, 23, 24, 16, 56, -6, 232
        -22, 15, 27, 42, 26, 25 , 123, 2323, -221, 4, 2, -3, -19, 5};

    BSTree * bereza = BSTree_new();
    
    BSTree_insertArr(bereza, sequence, sizeof(sequence) / sizeof(sequence[0]));

    BSTree_printFancy(bereza, 1, 1);

    BSTree_printFormat(bereza);

    BSTree_printInOrder(bereza);

    BSTree_freeWhole(bereza);

    return EXIT_SUCCESS;
}