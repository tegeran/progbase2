#include <error.h>
#include <stdio.h>
#include <stdlib.h>

void Error_checkHeap(void * heapMemory){
    if (heapMemory == NULL){
        perror("Heap overflow is detected");
        abort();
    }
}