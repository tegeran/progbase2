#include <bstree.h>
#include <stdlib.h>
#include <error.h>
#include <bintree_ext.h>
#include <integer.h>

#include <bintree.h>


typedef struct __BSTree {
    BinTree * root;
} BSTree;

static void clear(BinTree * self);
static void insert(BinTree * self, int key);

BSTree * BSTree_new(void){
    BSTree * newbie = malloc(sizeof(BSTree));
    Error_checkHeap(newbie);
    //
    newbie->root = NULL;
    return newbie;
}

void BSTree_free(BSTree * self){
    assert(self != NULL);
    assert(BSTree_isEmpty(self));
    //
    free(self);
}

void BSTree_freeWhole(BSTree * self){
    assert(self != NULL);   
    //
    BSTree_clear(self);
    //
    BSTree_free(self);
}

bool BSTree_isEmpty(BSTree * self){
    assert(self != NULL);
    //
    return self->root == NULL;
}

void BSTree_insert(BSTree * self, int key){
    assert(self != NULL);
    //
    if (BSTree_isEmpty(self))
        self->root = BinTree_new(Integer_new(key));
    else 
        insert(self->root, key);
}

void BSTree_clear(BSTree * self){
    assert(self != NULL);
    //
    if (BSTree_isEmpty(self) == false)
        BinTree_freeWhole(self->root, (Destructor)Integer_free);

    //
    self->root = NULL;
}

void BSTree_printInOrder(BSTree * self){
    assert(self != NULL);
    //
    if (!BSTree_isEmpty(self)){
        BinTree_printInOrder(self->root, (Printer)Integer_printWide);
    }
}

void BSTree_printFormat(BSTree * self){
    assert(self != NULL);
    //
    if (!BSTree_isEmpty(self)){
        BinTree_printFormat(self->root, (Printer)Integer_printWide);
    }
}

void BSTree_insertArr(BSTree * self, int * arr, size_t size){
    assert(self != NULL);
    assert(arr != NULL);
    //
    for (size_t i = 0; i < size; ++i){
        BSTree_insert(self, arr[i]);
    }
}



static void clear(BinTree * self){
    assert(self != NULL);
    //
    BinTree_freeWhole(self, (Destructor)Integer_free);
}

static void insert(BinTree * self, int key){
    assert(self != NULL);
    //
    assert(key != Integer_get(BinTree_at(self)));

    if (key < Integer_get(BinTree_at(self))){
        //
        if (BinTree_getLeft(self) == NULL)
            BinTree_setLeft(self, BinTree_new(Integer_new(key)));
        else
            insert(BinTree_getLeft(self),  key);
    //        
    }   
    else {
        if (BinTree_getRight(self) == NULL)
            BinTree_setRight(self, BinTree_new(Integer_new(key)));
        else
            insert(BinTree_getRight(self),  key);
    //    
    }
}


bool BSTree_contains(BSTree * self, int val){
    assert(self != NULL);
    if (BSTree_isEmpty(self)) return false;
    //
    Integer * toCompare = Integer_new(val);
    bool answer =  BinTree_searchOrdered(self->root, toCompare, (Comparator)Integer_compare) != NULL;
    Integer_free(toCompare);
    return answer;
}

void BSTree_printFancy(BSTree * self, unsigned short y, unsigned short x){
    assert(self != NULL);
    //
    if (!BSTree_isEmpty(self))
        BinTree_printFancy(self->root, y, x, (Printer)Integer_printWide);
}