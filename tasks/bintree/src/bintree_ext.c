#include <bintree_ext.h>
#include <assert.h>
#include <error.h>
#include <integer.h>
#include <stdio.h>
#include <progbase/console.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>


extern const wchar_t TREE_VLINE;
extern const wchar_t TREE_RIGHT_INBRANCH[];
extern const wchar_t TREE_RIGHT_OUTBRANCH[];
extern const wchar_t TREE_LEFT_OUTBRANCH[];


// Через те що, бібліотека <wchar.h> якось конфліктує з функціями i/o для стандартних char 
// прийшлося переписати функції, що використовують їх під використання відповідних
// функцій бібліотеки широких чарів

static void move(unsigned short y, unsigned short x){
    wprintf(L"\033[%i;%iH", y, x); // Thanks RA for conPos
}

static void printTree(BinTree * self, int level, Printer print);
static size_t printFancy(BinTree * self, unsigned short y, unsigned short x, Printer print);
static void printHLine(wchar_t token, unsigned int size);
static void printVLine(wchar_t token, unsigned short height, unsigned short y, unsigned short x);


void BinTree_freeWhole(BinTree * self, Destructor valDestructor){
    assert(self != NULL);
    assert(valDestructor != NULL);
    //
    if (BinTree_hasLeft(self)){
        BinTree_freeWhole(BinTree_getLeft(self), valDestructor);
    }
    if (BinTree_hasRight(self)){
        BinTree_freeWhole(BinTree_getRight(self), valDestructor);
    }
    BinTree_freeAll(self, valDestructor);
}

BinTree * BinTree_searchOrdered(BinTree * self, void * val, Comparator compare){
    if (self == NULL) return NULL;
    signed char compared = (*compare)(val, BinTree_at(self));
    switch (compared){
        case 0:
            return self;
        case 1:
            return BinTree_searchOrdered(BinTree_getLeft(self), val, compare);
        case -1:
            return BinTree_searchOrdered(BinTree_getRight(self), val, compare);
        default:
            consider(1 == 0, "Unhandled comparator return value received", false);
    }
}

BinTree * BinTree_search(BinTree * self, void * val, Comparator compare){
    if (self == NULL) 
        return NULL;
    if (compare(BinTree_at(self), val) == 0)
        return self;
    BinTree * traverseLeft = BinTree_search(BinTree_getLeft(self), val, compare);
    return traverseLeft ? traverseLeft : 
        BinTree_search(BinTree_getRight(self), val, compare);
}


void BinTree_printFormat(BinTree * self, Printer print){
    assert(self != NULL);
    consider(strcmp(setlocale(0, NULL), "C"), "UTF-8 format is unavailable", VOID);
    assert(print != NULL);
    //
    printTree(self, 0, print);
}

void BinTree_printInOrder(BinTree * self, Printer print){
    assert(print != NULL);
    if (self == NULL) return;
    //
    BinTree_printInOrder(BinTree_getLeft(self), print);
    //
    (*print)(BinTree_at(self)); 
    wprintf(L", ");
    //
    BinTree_printInOrder(BinTree_getRight(self), print);
}

void BinTree_printFancy(BinTree * self, unsigned short y, unsigned short x, Printer print){
    consider(strcmp(setlocale(0, NULL), "C"), "UTF-8 format is unavailable", VOID);
    assert(print != NULL);
    //
    if (self == NULL) return;
    move(y + printFancy(self, y, x, print) , 1);
}

static void printVLine(wchar_t token, unsigned short height, unsigned short y, unsigned short x){
    while (height != 0){
        move(y++, x);
        putwchar(token);
        --height;
    }
}

static void printTree(BinTree * self, int level, Printer print){
    printHLine(L'.', level << 1);
    if (self == NULL){
        wprintf(L"(null)\n");
        return;
    }
    (*print)(BinTree_at(self));
    putwchar(L'\n');
    if (!BinTree_isLeaf(self)){
        printTree(BinTree_getLeft(self), level + 1, print);
        printTree(BinTree_getRight(self), level + 1, print);
    }
}

static size_t printFancy(BinTree * self, unsigned short y, unsigned short x, Printer print){
    if (self == NULL) return 0;
    //
    int kinder = 0;
    fflush(stdout);
    move(y, x);
    (*print)(BinTree_at(self));
    if (BinTree_hasRight(self)){
        move(y + 1, x);
        if (BinTree_hasLeft(self)){
            wprintf(TREE_RIGHT_INBRANCH);
            kinder = printFancy(BinTree_getRight(self), y + 1, 
                x + sizeof(TREE_RIGHT_INBRANCH) / sizeof(TREE_RIGHT_INBRANCH[0]) - 1, print);
        }
        else {
            wprintf(TREE_RIGHT_OUTBRANCH); 
            kinder = printFancy(BinTree_getRight(self), y + 1,
                x + sizeof(TREE_RIGHT_OUTBRANCH) / sizeof(TREE_RIGHT_OUTBRANCH[0]) - 1, print);
        }
    }
    if (BinTree_hasLeft(self)){
        if (BinTree_hasRight(self)){
            printVLine(TREE_VLINE, kinder - 1, y + 2 , x);
        }
        move(y + 1 + kinder, x);
        wprintf(TREE_LEFT_OUTBRANCH);
        kinder += printFancy(BinTree_getLeft(self), y + 1 + kinder,
            x + sizeof(TREE_LEFT_OUTBRANCH) / sizeof(TREE_LEFT_OUTBRANCH[0]) - 1, print);
    }
    return kinder + 1;
    
}


static void printHLine(wchar_t token, unsigned int size){
    while(size != 0){
        putwchar(token);
        --size;
    }
}

