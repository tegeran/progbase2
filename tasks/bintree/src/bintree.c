#include <bintree.h>
#include <stdlib.h>
#include <assert.h>
#include <error.h>

typedef struct __BinTree {
    void * val;
    BinTree * left;
    BinTree * right;
} BinTree;

BinTree * BinTree_new(void * value){
    BinTree * newbie = malloc(sizeof(BinTree));
    Error_checkHeap(newbie);
    newbie->val = value;
    newbie->left = NULL;
    newbie->right = NULL;
    return newbie;
}

void BinTree_free(BinTree * self){
    assert(self != NULL);
    //
    free(self);
}

void BinTree_freeAll(BinTree * self, Destructor valDestructor){
    assert(self != NULL);
    assert(valDestructor != NULL);
    //
    (*valDestructor)(self->val);
    free(self);
}

void * BinTree_at(BinTree * self){
    assert(self != NULL);
    //
    return self->val;
}

void * BinTree_set(BinTree * self, void * value){
    assert(self != NULL);
    //
    void * prev = self->val;
    self->val = value;
    return prev;

}

BinTree * BinTree_getLeft(BinTree * self){
    assert(self != NULL);
    //
    return self->left;
}

BinTree * BinTree_getRight(BinTree * self){
    assert(self != NULL);
    //
    return self->right;
}

BinTree * BinTree_setLeft(BinTree * self, BinTree * left){
    assert(self != NULL);
    //
    BinTree * prev = self->left;
    self->left = left;
    return prev;
}

BinTree * BinTree_setRight(BinTree * self, BinTree * right){
    assert(self != NULL);
    //
    BinTree * prev = self->right;
    self->right = right;
    return prev;
}


bool BinTree_hasLeft(BinTree * self){
    assert(self != NULL);
    //
    return self->left != NULL;
}

bool BinTree_hasRight(BinTree * self){
    assert(self != NULL);
    //
    return self->right != NULL;
}

bool BinTree_isLeaf(BinTree * self){
    assert(self != NULL);
    //
    return self->right == NULL && self->left == NULL;
}