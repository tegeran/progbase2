#pragma once

#include <stdlib.h>

typedef struct __DLNode DLNode;
typedef void (* freeVal)(void *);
void DLNode_freeAll(DLNode * self, freeVal valDestructor);

// Construcor and destructor
// AHTUNG PROGRAMMIERER! Constructor aborts workflow if experineced heap overflow
DLNode * DLNode_new(void * val);
void DLNode_free(DLNode * self);


// Getters
void * DLNode_at(DLNode * self);
DLNode * DLNode_getNext(DLNode * self);
DLNode * DLNode_getPrev(DLNode * self);

// Setters
void * DLNode_set(DLNode * self, void * newVal);
DLNode * DLNode_setNext(DLNode * self, DLNode * next);
DLNode * DLNode_setPrev(DLNode * self, DLNode * prev);