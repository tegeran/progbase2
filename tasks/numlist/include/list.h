#pragma once

#include <stdbool.h>
#include <stdlib.h>


typedef struct __List List;

typedef void (*Destructor)(void *);
typedef bool (*RemoveCase)(void *);

// Constructors and destructors
// AHTUNG PROGRAMMIERER! Constructors abort workflow if experienced heap overflow

List * List_new(void);
void List_free(List * self);
// added:
void List_freeWhole(List * self, Destructor valDestructor);
//
void List_removeBy(List * self, RemoveCase caseRemove, Destructor valDestructor);

// NOTE: first node has index 0
void * List_at(List * self, size_t index);
void * List_set(List * self, size_t index, void * value);
void * List_removeAt(List * self, size_t index);
void List_insert(List * self, size_t index, void * value);


size_t List_size(List * self);

// adds a new object to the end of list (time complexity O(1))
void List_addLast(List * self, void * value);
void List_addFirst(List * self, void * value);
bool List_isEmpty(List * self);