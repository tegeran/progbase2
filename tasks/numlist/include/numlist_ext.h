#pragma once

#include <numlist.h>
#include <stdbool.h>

// value maintained in list is supposed to be Integer 

typedef bool (*NumRemoveCase)(int val);

NumList * NumList_newInit(size_t size, int values);
NumList * NumList_newInitRandom(size_t size, int seed, int min, int max);

void NumList_print(NumList * self);
void NumList_removeBy(NumList * self, NumRemoveCase caseRemove);

