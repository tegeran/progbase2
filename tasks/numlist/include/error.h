#pragma once
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define VOID 

#ifdef CONSIDER_SOFT
    #define consider(considered, errorReport, returnVal) \
        if (!(considered)) {                             \
            perror(errorReport); return returnVal;       \
        }
#elif defined CONSIDER_OFF
    #define consider(considered, errorReport, returnVal)  
#else 
    #define consider(considered, errorReport, returnVal) \
        assert((considered) && (errorReport))
#endif

#define considerNonNull(pointer, returnVal) \
        consider((pointer) != NULL, "An attempt to dereference NULL pointer was made", returnVal)

void Error_checkHeap(void * heapMemory);