#include <integer.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>

#include <error.h>

typedef struct __Integer{
    int val;
} Integer;

void Integer_initRandomness(unsigned int seed){
    srand(seed);    
}


Integer * Integer_new(int val){
    Integer * newbie = malloc(sizeof(Integer));
    Error_checkHeap(newbie);

    newbie->val = val;
    return newbie;
}


Integer * Integer_newRandom(int min, int max){
    consider(min <= max, "Illegal random range bounds", NULL);
    // 
    return Integer_new((rand() % (max - min + 1)) + min);
}

void Integer_free(Integer * self){
    considerNonNull(self, VOID);
    //
    free(self);
}

void Integer_print(Integer * self){
    considerNonNull(self, VOID);
    //
    printf("%i", self->val);
}

void Integer_printWide(Integer * self){
    consider(strcmp(setlocale(0, NULL), "C"), "UTF-8 format is unavailable", VOID);
    wprintf(L"%i", self->val);
}

int Integer_set(Integer * self, int val){
    considerNonNull(self, 0);
    //
    int keeper = self->val;
    self->val = val;
    return keeper;
}

int Integer_get(Integer * self){
    considerNonNull(self, 0);
    //
    return self->val;
}

bool Integer_isNegative(Integer * self){
    return Integer_get(self) < 0;
}


signed char Integer_compare(Integer * left, Integer * right){
    assert(left != NULL);
    assert(right != NULL);
    //
    if (left->val == right->val) return 0; 
    return left->val > right->val ? -1 : 1;
}