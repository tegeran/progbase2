#include <numlist_ext.h>
#include <stdio.h>


#include <error.h>




typedef bool (*NumRemoveCase)(int val);

NumList * NumList_newInit(size_t size, int values){
    NumList * newbie = NumList_new();
    while (size != 0) {
        NumList_add(newbie, values);
        --size;
    }
    return newbie;
}

NumList * NumList_newInitRandom(size_t size, int seed, int min, int max){
    NumList * newbie = NumList_new();
    srand(seed);
    while(size != 0){
        NumList_add(newbie, rand() % (max - min + 1) + min);
        --size;
    }
    return newbie;
}

void NumList_print(NumList * self){
    considerNonNull(self, VOID);
    //
    printf("[ ");
    const size_t SIZE = NumList_count(self);
    for (int i = 0; i < SIZE - 1; ++i){
        printf("%i, ", NumList_at(self, i));
    }
    printf("%i", NumList_at(self, SIZE - 1));
    puts(" ]");
}

void NumList_removeBy(NumList * self, NumRemoveCase caseRemove){
    considerNonNull(self, VOID);
    considerNonNull(caseRemove, VOID);
    //
    size_t size = NumList_count(self);
    for (int i = 0; i < size; ++i){
        if ((*caseRemove)(NumList_at(self, i))){
            NumList_removeAt(self, i);
            --size;
            --i;
        }
    }
}
