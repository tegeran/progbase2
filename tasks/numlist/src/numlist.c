#include <numlist.h>
#include <list.h>
#include <integer.h>

#include <error.h>



typedef struct __NumList {
    List * list;
} NumList;



NumList * NumList_new(void){
    NumList * newbie = malloc(sizeof(NumList));
    Error_checkHeap(newbie);
    newbie->list = List_new();
    return newbie;
}

void NumList_free(NumList * self){
    considerNonNull(self, VOID);
    //
    List_freeWhole(self->list, (Destructor)Integer_free);
    free(self);
}

void NumList_add(NumList * self, int value){
    considerNonNull(self, VOID);
    //
    List_addLast(self->list, Integer_new(value));
}

void NumList_insert(NumList * self, size_t index, int value){
    considerNonNull(self, VOID);
    consider(index < List_size(self->list), "Numlist index out of bounds exception", VOID);
    //
    List_insert(self->list, index, Integer_new(value));
}

int NumList_removeAt(NumList * self, size_t index){
    considerNonNull(self, 0);
    consider(index < List_size(self->list), "Numlist index out of bounds exception", 0);
    //
    Integer * trash = List_removeAt(self->list, index);
    int val = Integer_get(trash);
    Integer_free(trash);
    return val;
}

size_t NumList_count(NumList * self){
    considerNonNull(self, 0);
    //
    return List_size(self->list);
}

int NumList_at(NumList * self, size_t index){
    considerNonNull(self, 0);
    consider(index < List_size(self->list), "Numlist index out of bounds exception", 0);
    //
    return Integer_get(List_at(self->list, index));
}

/**
 *  @returns old value at index
 */
int NumList_set(NumList * self, size_t index, int value){
    considerNonNull(self, 0);
    consider(index < List_size(self->list), "Numlist index out of bounds exception", 0);
    //
    return Integer_set(List_at(self->list, index), value);
}