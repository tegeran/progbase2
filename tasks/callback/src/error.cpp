#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <cui.h>
#include <iostream>

namespace Debug {

    static unsigned short _____stackLevel = 0;

    unsigned short _____getLevel() {
        return _____stackLevel;
    }

    void _____stackPushFunc(const char *func) {
        ++_____stackLevel;
        Cui::printHLine(_____stackLevel, LEVEL_TOKEN);
        std::cout << "[" << func << "]";
    }
    void _____stackPopFunc(const char **func) {
        --_____stackLevel;
        Cui::printHLine(_____stackLevel, LEVEL_TOKEN);
        std::cout << "<" << *func << "<<" << ESCAPING_STR;
    }
}