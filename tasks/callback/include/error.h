#pragma once
#include <cassert>
#include <clocale>
namespace Debug {

#define imply(presupposition, conclusion) (!(presupposition) || (conclusion))

#ifndef NO_DEBUG
    #define Debug_exists(expression)  assert((expression) && "An attempt to dereference NULL pointer was made");
    #define Debug_UTF8() assert(strcmp(setlocale(0, NULL), "C") && "An attempt to print wide character without UTF-8 format was made")
    #define Debug_inbound(leftBound, suspect, rightBound) assert((suspect) >= (leftBound) && (suspect) <= (rightBound) && "Index out of bounds exception");
    #define Debug_maxbound(suspect, rightBound) assert((suspect) <= (rightBound) && "Index out of bounds exception");
    #define Debug_minbound(suspect, leftBound) assert((suspect) >= (leftBound) && "Index out of bounds exception");
    #define Debug_suppose(expression, errorMessage) assert((expression) && (errorMessage))
    #define Debug_shutdown(reason) assert(0 && (reason))

    #ifndef NO_DEBUG_TIPS
        #define Debug_tip(expression, tipMessage) assert((expression) && (tipMessage))
    #else
        #define Debug_tip(expression, tipMessage)
    #endif

    #ifndef NO_DEBUG_TRACE

    unsigned short _____getLevel(); // return current function stack height
    void _____stackPushFunc(const char *func);

    void _____stackPopFunc(const char **func);

        #define LEVEL_TOKEN '.'
        #define ESCAPING_STR " <<< "

        #define TRACE_CALL()                        \
                _____stackPushFunc(__func__);          \
                const char * _____escapedFunction __attribute__((cleanup(_____stackPopFunc))) = __func__

        #define TRACE_CALL_LINE() TRACE_CALL(); putchar('\n');
    #else
        #define TRACE_CALL()
        #define TRACE_CALL_LINE()
    #endif

#else
    #define Debug_exists(expression)
    #define Debug_UTF8()
    #define Debug_inbound(leftBound, suspect, rightBound)
    #define Debug_maxbound(suspect, rightBound)
    #define Debug_minbound(suspect, leftBound)
    #define Debug_suppose(expression, errorMessage)
    #define Debug_shutdown(reason)

    #definetip(expression, tipMessage)

    #define TRACE_CALL()
    #define TRACE_CALL_LINE()
#endif
}