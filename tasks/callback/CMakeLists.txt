cmake_minimum_required(VERSION 3.9)
project(callback)

include_directories(include)
file(GLOB SOURCES "src/*cpp")

set(CMAKE_CXX_STANDARD 17)

SET(COMPILER_FLAGS -std=c++17)

add_executable(callback main.cpp ${SOURCES})