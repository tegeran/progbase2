#include <cstdlib>
#include <iostream>
#include "include/cui.h"
#include <string>
#include <algorithm>

namespace Task1{
	bool isVowel(const char &ch);
	bool isConsonant(const char &ch);
	unsigned int consonantsAt(const char * ch);
	int compareConsonants(const char * const & left, const char * const& right);
}

namespace Task2 {
	class Teacher{
	public:
		const char * name;
		const char * labor;
		short int load;

		static int compareLoad(const Teacher * const left, const Teacher * const right);

		friend std::ostream & operator<<(std::ostream & stream, const Teacher &self);
	};
}

namespace Task3{
	void Array_foreach(int arr[], int len, void (*action)(int, int));
	void Array_foreachReversed(int arr[], int len, void (*action)(int, int));
}


namespace Templates {
	template<typename T, size_t N>
	constexpr size_t sizeat(const T (&array)[N]) noexcept {
		return N;
	}
	template<typename T, size_t N>
	constexpr void print(const char * const& msg, const T (&array)[N]) noexcept {
		std::cout << Cui::reverse << msg << Cui::reverse_off << "[ ";
		for (size_t i = 0; i < N; ++i){
			std::cout << array[i] << ' ';
		}
		std::cout << "]\n";
	};
}


int main() {
//	std::cout << std::is_trivial<Task2::Teacher>::value; // must be trivial for qsort()

	const char * words[] = {"aaaaa", "chchchch", "pisat", "cursach", "nado" "uzhe"};

	std::cout << Cui::fgnd(0,4,0) << "Task 1:\n" << Cui::reset <<
			  "Використовуючи стандартну функцію qsort() і callback-функції, \n"
			  "виконати сортування масиву слів у порядку спадання кількості приголосних букв.\n";


	Templates::print("Before:", words);

	std::qsort(words,
			   Templates::sizeat(words),
			   sizeof(char *),
			   (__compar_fn_t)Task1::compareConsonants);


	Templates::print("After :", words);


	std::cout << Cui::fgnd(0,4,0) << "Task 2:\n" << Cui::reset <<
			  "Описати структуру Викладач і за допомогою qsort(), виконати \n"
			"сортування масиву елементів Викладач у порядку зростання кількості пар на тиждень.\n";

	Task2::Teacher teachers[]{
			{"Sedjevick" , "lab", 12},
			{"Kernigan"  , "lab", 23},
			{"Hadyniak"  , "lab", 10500},
			{"Jobs Steve"      , "lab", 0},
			{"Lavavej"   , "lab", 3},
			{"Jerry Cain", "lab", 23}
	};

	Templates::print("Before: \n", teachers);

	std::qsort(teachers,
			   Templates::sizeat(teachers),
			   sizeof(Task2::Teacher),
			   (__compar_fn_t)Task2::Teacher::compareLoad);


	Templates::print("After :\n", teachers);


	std::cout << Cui::fgnd(0,4,0) << "Task 3:\n" << Cui::reset <<
			  "У функції main() створити і вивести тестовий масив цілих чисел \n"
			  "та викликати функції Array_foreach() та Array_foreachReversed \n"
			  "на тестовому масиві чисел із callback-функціями, що виводять у \n"
			  "консоль індекс і значення елементів на парних позиціях.\n";
	

	int ints[] = {1, 76, 72, -12, -12, 5, 32, 45, 90, 110};
	Templates::print("Actual:                ", ints);

	std::cout << Cui::reverse << "Array_foreach:         " << Cui::reverse_off <<  "[ ";
	Task3::Array_foreach(ints, Templates::sizeat(ints),
						 [](int entity, int i){
							 if (!(i & 1)) std::cout << entity << ' ';
						 });

	std::cout << "]\n";

	std::cout << Cui::reverse << "Array_foreachReversed: " << Cui::reverse_off <<  "[ ";
	Task3::Array_foreachReversed(ints, Templates::sizeat(ints),
						 [](int entity, int i){
							 if (!(i & 1)) std::cout << entity << ' ';
						 });

	std::cout << "]\n";
	return 0;
}


namespace  Task1 {
	bool isConsonant(const char &ch){
		return !isVowel(ch);
	}
	bool isVowel(const char &ch) {
		switch (ch) {
			case 'A':
			case 'a':

			case 'E':
			case 'e':

			case 'I':
			case 'i':

			case 'O':
			case 'o':

			case 'U':
			case 'u':
				return true;
			default:
				return false;
		}
	}

	unsigned int consonantsAt(const char * ch){
		unsigned int i = 0;
		while (*ch) if (isConsonant(*ch++)) ++i;
		return i;
	}


	int compareConsonants(const char * const & left, const char * const& right){
		return consonantsAt(left) - consonantsAt(right);
	}
}

namespace Task2 {
	int Teacher::compareLoad(const Teacher * const left, const Teacher * const right){
		return left->load - right->load;
	}

	std::ostream & operator<<(std::ostream & stream, const Teacher &self) {
		std::cout << "Name:  " << self.name  << " \t"
				  << "Labor: " << self.labor << " \t"
				  << "Load:  " << self.load  << "\n";
	}

}

namespace Task3 {
	void Array_foreach(int arr[], int len, void (*action)(int, int)){
		for (int i = 0; i < len; ++i) {
			action(arr[i], i);
		}
	}



	void Array_foreachReversed(int arr[], int len, void (*action)(int, int)){
		for (int i = len - 1; i >= 0; --i) {
			action(arr[i], i);
		}
	}
}