#include<pupil.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include "error.h"
#include "sstring.h"
#include <errno.h>
#include "extesion.h"
#include "mem.h"

String * Pupil_toNewXml(Pupil *pup) {
	Debug_exists(pup);

	xmlDoc * doc = xmlNewDoc(BAD_CAST "1.0");

	xmlNode * pupNode = xmlNewNode(nullptr, BAD_CAST "pupil");
	xmlDocSetRootElement(doc, pupNode);

	xmlNewProp(pupNode, BAD_CAST "name",  BAD_CAST String_c_str(pup->name ));
	xmlNewProp(pupNode, BAD_CAST "_class", BAD_CAST String_c_str(pup->class));

	String * bufString = String_new();
	String_appendFormat(bufString, "%i", pup->age);
	xmlNewProp(pupNode, BAD_CAST "age", BAD_CAST String_c_str(bufString));
	String_clear(bufString);

	String_appendFormat(bufString, "%f", pup->score);
	xmlNewProp(pupNode, BAD_CAST "score", BAD_CAST String_c_str(bufString));

	xmlNewProp(pupNode, BAD_CAST "alive", BAD_CAST (pup->alive ? "true" : "false"));

	foreach_v(const String *, book, pup->textbooks){
		xmlNode * bookNode = xmlNewNode(nullptr, BAD_CAST "book");
		xmlNodeSetContent(bookNode, BAD_CAST String_c_str(book));
		xmlAddChild(pupNode, bookNode);
	}

	xmlBuffer * bufferPtr = xmlBufferCreate();
	xmlNodeDump(bufferPtr, nullptr, (xmlNode *)doc, 0, 1);
	String_rewrite(bufString, (char *)bufferPtr->content);
	xmlBufferFree(bufferPtr);
	xmlFreeDoc(doc);
	return bufString;
}

Pupil * Pupil_newFromXmlFile(const char * path) {
	xmlDoc * doc = xmlReadFile(path, nullptr, XML_PARSE_PEDANTIC);
	///
	if (!doc)
		goto error_on_name;
	///
	xmlNode * pupNode = xmlDocGetRootElement(doc);
	///
	if (    pupNode->type != XML_ELEMENT_NODE
		||	pupNode->next != nullptr
	 	|| !Chars_equals((const char *)pupNode->name, "pupil")){
		//
		goto error_on_name;
	}
	///
	xmlChar * pupName = xmlGetProp(pupNode, BAD_CAST "name");
	///
	if (!pupName)
		goto error_on_name;
	///
	Pupil * pup = Mem_malloc(sizeof(Pupil));
	pup->name = String_newWrap((char *)pupName);
	xmlChar * pupClass = xmlGetProp(pupNode, BAD_CAST "_class");
	///
	if (!pupClass)
		goto error_on_class;
	///
	pup->class = String_newWrap((char *)pupClass);

	xmlChar * pupAge = xmlGetProp(pupNode, BAD_CAST "age");
	///
	if (!pupAge )
		goto error_on_age;
	///
	unsigned long age = strtoul((char *)pupAge, nullptr, 10);
	xmlFree(pupAge);
	///
	if (errno || age > USHRT_MAX) goto error_on_age;
	///
	pup->age = (unsigned short)age;
	xmlChar * pupScore = xmlGetProp(pupNode, BAD_CAST "score");
	///
	if (!pupScore)
		goto error_on_age;
	///
	pup->score = strtof((char *)pupScore, nullptr);
	xmlFree(pupScore);
	///
	if (errno)
		goto error_on_age;
	///
	xmlChar * alive = xmlGetProp(pupNode, BAD_CAST "alive");
	///
	if (!alive)
		goto error_on_age;
	///
	if (Chars_equals((char *)alive, "true")){
		pup->alive = true;
	} else if (Chars_equals((char *)alive, "false")){
		pup->alive = false;
	} else {
		xmlFree(alive);
		goto error_on_age;
	}
	xmlFree(alive);
	xmlNode * bookNode = pupNode->children;
	pup->textbooks = Vector_new(String_p, String_free);
	while (bookNode){
//		if (!Chars_equals((char *)bookNode->name, "book")){
//			goto error_on_books;
//		}
		if (!xmlNodeIsText(bookNode)){
			goto error_on_books;
		}
		if ((bookNode = bookNode->next)) {
			if (!Chars_equals((char *) bookNode->name, "book")) {
				goto error_on_books;
			}
			Vector_pushBack(pup->textbooks, String_newWrap((char *) xmlNodeGetContent(bookNode)));
			bookNode = bookNode->next;
		}
	}
	xmlFreeDoc(doc);
	return pup;

	/// ERRNO
	error_on_books:
	Vector_free(pup->textbooks);
	error_on_age:
	String_free(pup->class);
	error_on_class:
	String_free(pup->name);
	free(pup);
	error_on_name:
	xmlFreeDoc(doc);
	errno = 0;
	return nullptr;
}


void Pupil_print(const Pupil *pup) {
	Debug_exists(pup);
	fputs("Name: ", stdout);
	String_println(pup->name);

	fputs("Class: ", stdout);
	String_println(pup->class);

	printf("Age: %i", pup->age);
	printf("\nScore: %f", pup->score);
	fputs("Alive: ", stdout);
	puts(pup->alive ? "true" : "false");
	fputs("Books: [ ", stdout);
	foreach_v(const String *, book, pup->textbooks){
		fputs("\"", stdout);
		String_print(book);
		fputs("\" ", stdout);
	}
	putchar(']');
}

Pupil * Pupil_new(void) {
	Pupil * newbie = Mem_malloc(sizeof(Pupil));
	*newbie = _Pupil(String_new(), String_new(), 0, 0, Vector_new(String_p, String_free), false);
	return newbie;
}

void Pupil_free(Pupil *pup) {
	Debug_exists(pup);

	Vector_free(pup->textbooks);
	String_free(pup->name);
	String_free(pup->class);
	free(pup);
}
