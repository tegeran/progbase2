#define _BSD_SOURCE
#define _POSIX_SOURCE

#include <cui.h>
#include <stdio.h>
#include <wchar.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>


void Cui_setFgColor(int id){  // id has 3 digits first defines RED, second GREEN, third BLUE
    short fgRed   = (id % 1000 - id % 100) / 100;
    short fgGreen = (id % 100  - id % 10)  / 10;
    short fgBlue  =  id % 10;
    printf("\033[38;5;%im", 16 + 36 * fgRed + 6 * fgGreen + fgBlue);
}

void Cui_setBgColor(int id){
    short bgRed   = (id% 1000 - id% 100) / 100;
    short bgGreen = (id% 100  - id% 10)  / 10;
    short bgBlue  =  id% 10;
    printf("\033[48;5;%im", 16 + 36 * bgRed + 6 * bgGreen + bgBlue);
}


void Cui_setFgColorWide(int id){  // id has 3 digits first defines RED, second GREEN, third BLUE
    short fgRed   = (id % 1000 - id % 100) / 100;
    short fgGreen = (id % 100  - id % 10)  / 10;
    short fgBlue  =  id % 10;
    wprintf(L"\033[38;5;%im", 16 + 36 * fgRed + 6 * fgGreen + fgBlue);
}

void Cui_setBgColorWide(int id){
    short bgRed   = (id% 1000 - id% 100) / 100;
    short bgGreen = (id% 100  - id% 10)  / 10;
    short bgBlue  =  id% 10;
    wprintf(L"\033[48;5;%im", 16 + 36 * bgRed + 6 * bgGreen + bgBlue);
}

void Cui_move(Location loc){
    printf("\033[%i;%iH", loc.y, loc.x); // Thanks RA for conPos
}

void Cui_moveWide(Location loc){
    wprintf(L"\033[%i;%iH", loc.y, loc.x); // Thanks RA for conPos
}

void Cui_setFgColorOf(FILE * stream, int id){  // id has 3 digits first defines RED, second GREEN, third BLUE
    short fgRed   = (id % 1000 - id % 100) / 100;
    short fgGreen = (id % 100  - id % 10)  / 10;
    short fgBlue  =  id % 10;
    fprintf(stream, "\033[38;5;%im", 16 + 36 * fgRed + 6 * fgGreen + fgBlue);
}

void Cui_setBgColorOf(FILE * stream, int id){
    short bgRed   = (id% 1000 - id% 100) / 100;
    short bgGreen = (id% 100  - id% 10)  / 10;
    short bgBlue  =  id% 10;
    fprintf(stream, "\033[48;5;%im", 16 + 36 * bgRed + 6 * bgGreen + bgBlue);
}


void Cui_setFgColorWideOf(FILE * stream, int id){  // id has 3 digits first defines RED, second GREEN, third BLUE
    short fgRed   = (id % 1000 - id % 100) / 100;
    short fgGreen = (id % 100  - id % 10)  / 10;
    short fgBlue  =  id % 10;
    fwprintf(stream, L"\033[38;5;%im", 16 + 36 * fgRed + 6 * fgGreen + fgBlue);
}

void Cui_setBgColorWideOf(FILE * stream, int id){
    short bgRed   = (id% 1000 - id% 100) / 100;
    short bgGreen = (id% 100  - id% 10)  / 10;
    short bgBlue  =  id% 10;
    fwprintf(stream, L"\033[48;5;%im", 16 + 36 * bgRed + 6 * bgGreen + bgBlue);
}

void Cui_printHLine(unsigned short size, char token){
    for (unsigned short i = 0; i < size; ++i){
        putchar(token);
    }
}

void Cui_printVLine(unsigned short size, char token, Location loc){
    for (unsigned short i = 0; i < size; ++i){
        putchar(token);
        ++loc.y;
        Cui_move(loc);
    }
}

void Cui_printHLineWide(unsigned short size, wchar_t token){
    for (unsigned short i = 0; i < size; ++i){
        putwchar(token);
    }
}

void Cui_printVLineWide(unsigned short size, wchar_t token, Location loc){
    for (unsigned short i = 0; i < size; ++i){
        putwchar(token);
        ++loc.y;
        Cui_moveWide(loc);
    }
}


// Ruslan Hadyniak conGetPos function copy:
Location Cui_getLocation(void) {
	Location pos = {0, 0};
	struct termios save, raw ;
	fflush(stdout);
	tcgetattr(0, &save);
	cfmakeraw(&raw);
	tcsetattr(0, TCSANOW, &raw);
	if (isatty(fileno(stdin))) {
        char cmd[] = "\033[6n";
        char buf[8] = {0};
		write(1, cmd, sizeof(cmd));
		read (0, buf, sizeof(buf));
		sscanf(buf, "\033[%i;%i", &(pos.y), &(pos.x));
	}
	tcsetattr(0, TCSANOW, &save);
	fflush(stdout);
	return pos;
}

void Cui_reverse(void) {
	printf("\033[7m");
}

void Cui_reverseOff(void){
	printf("\033[27m");
}

void Cui_printReverse(const char *message) {
	Cui_reverse();
	fputs(message, stdout);
	Cui_reverseOff();
}

void Cui_reset(void) {
	printf("\033[m");
}

void Cui_recover(void) {
	fflush(stdout);
	system("clear");
}
