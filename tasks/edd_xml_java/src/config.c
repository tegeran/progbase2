//
// Created by tegeran on 21.04.18.
//

#include "config.h"
#include "mem.h"
#include "extesion.h"
#include "error.h"
#include <libxml/parser.h>
#include <libxml/tree.h>


typedef struct __Config {
	String * path;
} Config;

void Config_free(Config * self){
	String_free(self->path);
	free(self);
}


static bool __Config_fromXml(Config * self, const String * xml){
	xmlDoc * doc = xmlReadMemory(String_c_str(xml), (int)String_length(xml), nullptr, nullptr, XML_PARSE_PEDANTIC);
	if (!doc) { goto error_on_start; }

	xmlNode * pathNode = xmlDocGetRootElement(doc);
	if (!Chars_equals((const char *)pathNode->name, "config")
		|| pathNode->children) {
		goto error_on_node;
	}

	xmlChar * name = xmlGetProp(pathNode, BAD_CAST "path");
	if (!name || Chars_empty((char *)name)) { goto error_on_content; }

	String_wrap(self->path, (char *)name);
	xmlFreeDoc(doc);
	return true;

	error_on_content:
	xmlFree(name);
	error_on_node:
	xmlFreeDoc(doc);
	error_on_start:
	return false;
}


static void __Config_toXml(Config * self){
	xmlDoc * doc = xmlNewDoc(BAD_CAST "1.0");

	xmlNode * pathNode = xmlNewNode(nullptr, BAD_CAST "config");

	xmlDocSetRootElement(doc, pathNode);

	xmlNewProp(pathNode, BAD_CAST "path", BAD_CAST String_c_str(self->path));

	FILE * file = fopen("configuration.xml", "w");
	Debug_exists(file);
	xmlDocDump(file, doc);
	fclose(file);
	xmlFreeDoc(doc);
}



Config * Config_createNew(String * path){
	Debug_exists(path);
	Debug_suppose(!String_empty(path), "Empty filepath violation");
	Config * newbie = Mem_malloc(sizeof(Config));
	newbie->path = path;
	__Config_toXml(newbie);
	return newbie;
}


Config * Config_readNew(void){
	if (!Mem_fileExists("configuration.xml")){
		return nullptr;
	}
	Config * newbie = Mem_malloc(sizeof(Config));
	newbie->path = String_new();
	if (!String_readFromPath("configuration.xml", newbie->path)
		|| !__Config_fromXml(newbie, newbie->path)){
		Config_free(newbie);
		return nullptr;
	}
	//
	return newbie;
}

void Config_setPath(Config * self, String * path){
	String_free(self->path);
	self->path = path;
	__Config_toXml(self);
}

String * Config_path(Config * self){
	Debug_exists(self);
	return self->path;
}
