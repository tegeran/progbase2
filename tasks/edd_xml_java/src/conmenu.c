//
// Created by tegeran on 20.03.18.
//

#include "conmenu.h"
#include "progbase/console.h"
#include "cui.h"
#include <stdio.h>
#include "mem.h"
#include "error.h"


typedef struct __ConMenu {

    Vector(const_char_p) * options;
    unsigned short   actual;
    Location location;
	bool posted;
} ConMenu;

ConMenu * ConMenu_new(Vector(const_char_p) * options){
    Debug_exists(Vector_at(options, 0));
    ConMenu * newbie = Mem_malloc(sizeof(ConMenu));
    newbie->actual = 0;
    newbie->location.y = 1;
    newbie->location.x = 1;
    newbie->options = options;
	newbie->posted = false;
    return newbie;
}

static void advanceDown(ConMenu * self){
    if (self->actual == Vector_size(self->options) - 1) {
        Cui_move(_Location(self->location.y + self->actual, self->location.x));
        fputs(*Vector_at(self->options, self->actual), stdout);
        Cui_move(self->location);
        Cui_printReverse(*Vector_at(self->options, (self->actual = 0)));
    }
    else {
        Cui_move(_Location(self->location.y + self->actual, self->location.x));
        fputs(*Vector_at(self->options, self->actual++), stdout);
        Cui_move(_Location(self->location.y + self->actual, self->location.x));
        Cui_printReverse(*Vector_at(self->options, self->actual));
    }
}

static void advanceUp(ConMenu * self){
    if (self->actual == 0) {
        self->actual = Vector_size(self->options) - 1;
        Cui_move(self->location);
        fputs(*Vector_front(self->options), stdout);
        Cui_move(_Location(self->location.y + Vector_size(self->options) - 1, self->location.x));
        Cui_printReverse(*Vector_back(self->options));
    }
    else {
        Cui_move(_Location(self->location.y + self->actual, self->location.x));
        fputs(*Vector_at(self->options, self->actual--), stdout);
        Cui_move(_Location(self->location.y + self->actual, self->location.x));
        Cui_printReverse(*Vector_at(self->options, self->actual));
    }
}

signed long long ConMenu_listen(ConMenu * self) {
    //
    do {
        int ch = Console_getChar();
        switch (ch){
            case 'W':
            case 'w':{
                advanceUp(self);
                break;
            }
            case 'S':
            case 's':{
                advanceDown(self);
                break;
            }
            case '\n':
            case ' ':{
                return self->actual;
            }
            case 27:{
                return CMR_Escaped;
            }
            default:break;
        }
		continue;
    } while (true);
}

void _ConMenu_post(ConMenu * self, unsigned short y, unsigned short x) {
    Debug_minbound(y, 1);
    Debug_minbound(x, 1);
    //
	self->posted = true;
    self->location = _Location(y, x);
    ConMenu_refresh(self);
    Console_hideCursor();
    Console_lockInput();
}

void ConMenu_unpost(ConMenu * self) {
    Cui_reset();
    Cui_move(self->location);
	if (!self->posted) { return; }

    unsigned short y = self->location.y;
    foreach_v(const char *, ch, self->options){
        Debug_exists(ch);
        while(*ch){
            putchar(' ');
            ++ch;
        }
        Cui_move(_Location(++y, self->location.x));
    }
    Console_showCursor();
    Console_unlockInput();
	Cui_move(self->location);
	self->posted = false;
}

void ConMenu_refresh(ConMenu * self) {
    unsigned short y = self->location.y;
    Cui_move(_Location(y, self->location.x));                 // spliting into two cycles may bring better performance for a bit
    unsigned int i = 0;
    while (i < self->actual){
        fputs(*Vector_at(self->options, i++), stdout);
        Cui_move(_Location(++y, self->location.x));
    }
    Cui_printReverse(*Vector_at(self->options, self->actual));
    while (++i < Vector_size(self->options)){
        Cui_move(_Location(++y, self->location.x));
        fputs(*Vector_at(self->options, i), stdout);
    }
	self->posted = true;
}

void ConMenu_free(ConMenu * self) {
	Debug_exists(self);
	ConMenu_unpost(self);
	free(self);
}

void ConMenu_free_p(ConMenu ** self) {
	ConMenu_unpost(*self);
	ConMenu_freeWhole(*self);
}

Vector(const_char_p) * ConMenu_options(ConMenu * self){
    Debug_exists(self);
    return self->options;
}

Vector(const_char_p) * ConMenu_setOptions(ConMenu * self, Vector(const_char_p) * newOptions){
    Debug_exists(self)
    Vector(const_char_p) * prev = self->options;
    self->options = newOptions;
    return prev;
}

signed long long ConMenu_react(ConMenu *self, int ch) {
	Debug_exists(self);
    switch (ch){
        case 'W':
        case 'w':{
            advanceUp(self);
            return CMR_MovedSelector;
        }
        case 'S':
        case 's':{
            advanceDown(self);
            return CMR_MovedSelector;
        }
        case '\n':
        case ' ':{
            return self->actual;
        }
        case 27:{
            return CMR_Escaped;
        }
        default: return CMR_Idle;
    }
}

void ConMenu_freeWhole(ConMenu *self) {
    Debug_exists(self);
    ConMenu_unpost(self);
    Vector_free(self->options);
    free(self);
}
