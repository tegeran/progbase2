//
// Created by tegeran on 18.04.18.
//

#include "vector.h"
#include "error.h"

void * __malloc(size_t size) {
	void *newbie = malloc(size);
	Error_checkHeap(newbie);
	return newbie;
}

void *__realloc(void * old, size_t size) {
	Debug_exists(old);
	old = realloc(old, size);
	Error_checkHeap(old);
	return old;
}
