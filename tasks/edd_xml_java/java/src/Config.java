import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamInclude;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@XStreamAlias("config")
public class Config {

	// http://samolisov.blogspot.com/2007/11/xstream-java-xml.html
	@XStreamAsAttribute()
	@XStreamAlias("path")
	private String _path;

	public String path(){
		return _path;
	}

	public Config(){ this._path = null; }
	public Config(String path) {this._path = path;}

	public static Config read() throws IOException, XStreamException {
		XStream stream = new XStream(new DomDriver());
		stream.processAnnotations(Config.class);
		return (Config)stream.fromXML(new File("../cmake-build-debug/configuration.xml"));
	}

}
