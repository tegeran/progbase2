import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.util.List;

@XStreamAlias("pupil")
public class Pupil {

	@XStreamAlias("name")
	private String  _name;
	private String  _class;
	@XStreamAlias("age")
	private short   _age;
	@XStreamAlias("score")
	private float   _score;
	@XStreamAlias("alive")
	private boolean _alive;
	@XStreamImplicit(itemFieldName = "book")
	private List    _textbooks;

	public static Pupil fromXml(String xml){
		XStream stream = new XStream(new DomDriver());
		stream.processAnnotations(Pupil.class);
		return (Pupil)stream.fromXML(xml);
	}

	@Override
	public String toString(){
		return    "Name:  " + _name  +
				"\nClass: " + _class +
				"\nAge:   " + _age   +
				"\nScore: " + _score +
				"\nAlive: " + _alive +
				"\nBooks: " + _textbooks;
	}


	public String  name()      { return _name;      }
	public String  klass()     { return _class;     }
	public short   age()       { return _age;       }
	public float   score()     { return _score;     }
	public boolean alive()     { return _alive;     }
	public List    textbooks() { return _textbooks; }

	public void setName(String _name)         { this._name = _name;           }
	public void setClass(String _class)       { this._class = _class;         }
	public void setAge(short _age)            { this._age = _age;             }
	public void setScore(float _score)        { this._score = _score;         }
	public void setAlive(boolean _alive)      { this._alive = _alive;         }
	public void setTextbooks(List _textbooks) { this._textbooks = _textbooks; }
}