#ifndef SSTRING_H
#define SSTRING_H
/*
 * Copyright (C) Vitaly Kryvenko 2018.
 * e-mail:   gerzoh1@gmail.com
 * Telegram: @Simpliest_One
 * 
 * benedictions to my teacher Ruslan Gadyniak 
 */ 

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "vector.h"
#include "extesion.h"

#define foreach_s(Type, variable, string)					    			    \
for (const char * ___ch_##variable = String_c_str(string); *___ch_##variable; )	\
for (Type variable = *___ch_##variable; 									    \
	*___ch_##variable;														    \
	variable = *(++___ch_##variable))

#define foreach_sptr(Type, variable, string)					    			    \
for (char * ___ch_##variable = (char *)String_c_str(string); *___ch_##variable; )	\
for (Type variable = ___ch_##variable; 									            \
	*___ch_##variable;														        \
	variable = ++___ch_##variable)


// ALIASES (from c++ std::string ):
#define String_size(string) String_length(string)

#define String_pushBack(self, character)  String_appendChar(self, character)
#define String_pushFront(self, character) String_addCharFirst(self, character)

#define String_prependChars(self, chars) String_addCharsFirst(self, chars)
#define String_prepend(self, other) 	 String_addStringFirst(self, other)
#define String_empty(self) 			     String_isEmpty(self)

#define String_front(self)   String_at((self), 0)
#define String_back(self)    String_at((self), String_length(self) - 1)
#define String_popBack(self) String_delchar(self, String_length(self) - 1)
#define String_erase(self)   String_clear(self)
#define String_newOf(chars)  String_toNewString(chars)



#define auto_String AUTO_DESTR(String_free_p) String
typedef struct __String String;
typedef String * String_p;

VECTOR_TEMPLATE_PROTOTYPE(String_p)


typedef void (*StringWriter)(String * str, const void * value);
typedef bool (*AnswerChar)(char);

// hash functions
size_t String_hashify(const String * self);
size_t Chars_hashify (const char * self);
bool String_equals(const String * self, const String * other);
bool Chars_equals (const char *   self, const char * other);
bool Chars_empty  (const char * self);
signed char String_compare(const String * self, const String * other);
signed char String_compareLength(const String * self, const String * other);

/*
 * Beware: while reading returned value do not modify or free String object, 
 * which provided readonly chars pointer.
 * 
 * Once have modified according String object be sure not to use
 * returned readonly string after it, otherwise behaviour is undefined.
 * 
 * If you need to modify original String while reading (char *)
 * take a dynamic copy of it by String_toNewChars() in the fist place.
 */
const char * String_readOnly(const String * self);
const char * String_c_str(const String * self); // alias for String_readOnly()


void String_reserve(const String * self, size_t size);
String * String_newReserved(size_t size);
void String_shrinkToFit(const String * self);
// AHTUNG PROGRAMMIERER! Constructors abort workflow if experienced heap overflow
String * String_new(void);
// beware dynamicChars must be allocated on heap so that ne String object
// takes the ownership of dynamicChars memory
String * String_newWrap(char * dynamicChars);
void String_wrap(String * self, char * dynamicChars);
void String_free(String * self);
void String_free_p(String ** self);
String * String_toNewString(const char * chars);
String * String_toNewSubstring(const String * self, size_t left, size_t right);
String * String_newCopyOf(const String * pattern);
char * String_toNewChars (const String * self);
char * String_toNewSubchars(const String * self, size_t left, size_t right);
// returned list consists of String objects
Vector(String_p) * String_toNewWords(const String * self, AnswerChar isWordLetter);
void String_appendNewWordsToVector(const String * self, AnswerChar isWordLetter, Vector(String_p) * list);

// '/0' is not reachable, last relevant index is String_length() - 1
char String_at(const String * self, size_t index);
// returns the number of char characters (excluding '\0') stored in String
size_t String_length     (const String * self);
// returns the number of bytes currently allocated for string storage array
size_t String_memoryUsage(const String * self); 

char String_set   (String * self, size_t index, char token);
void String_insert(String * self, size_t index, char token);
char String_delchar(String * self, size_t index);

 // remove particular chars
void String_removeBy(String * self, AnswerChar caseRemove);
// set string's length to 0 the same as String_set(self, 0, '\0')
void String_clear(String * self); 

char String_removeAt(String * self, size_t index); /// deprecated, use String_delchar instead
//
// the same as subsequent String_clear(self); String_append(self, chars)
void String_rewrite(String * self, const char * chars);
void String_setCopy(String * self, const String * template);

void String_concatenate(String * self, const String * other);
void String_append     (String * self, const char * chars);
void String_insertString(String * self, size_t index, const String * template);


void String_appendChar     (String * self, char character);
void String_appendSubchars (String * self, const char * chars, size_t left, size_t right);
void String_appendSubstring(String * self, const String * pattern, size_t left, size_t right);


void String_addCharFirst (String * self, char character);
void String_addCharsFirst(String * self, const char * chars);
void String_addStringFirst(String * self, const String * adding);
bool String_isEmpty(const String * self); // the same as String_length(self) == 0


void String_print    (const String * self);
void String_println  (const String * self);
void String_printWide(const String * self);
void String_writeTo           (FILE * file, const String * self);
void String_writeToPath       (const char *   path, const String * self);
void String_writeToPathString (const String * path, const String * self);
void String_appendToPath      (const char *   path, const String * self);
void String_appendToPathString(const String * path, const String * self);

void String_writeWideTo (FILE * file, const String * self);
void String_writeWideToPath       (const char * path, const String * self);
void String_writeWideToPathString (const String * path, const String * self);
void String_appendWideToPath      (const char * path, const String * self);
void String_appendWideToPathString(const String * path, const String * self);

bool String_endsWith(const String * self, const char * ending);
bool String_beginsWith(const String * self, const char * beginning);

signed long long String_searchSpecChar    (const String * self, AnswerChar isTarget);
signed long long String_searchChar        (const String * self, char template);
signed long long String_searchSubchars    (const String * self, const char * pattern);
signed long long String_searchSubstring   (const String * self, const String * pattern);
signed long long String_searchLastSpecChar(const String * self, AnswerChar isTarget);
signed long long String_searchLastChar    (const String * self, char template);
signed long long String_searchOccurence   (const String * self, char template, size_t occurence);

void String_swap   (String * self, size_t apple, size_t jack);
void String_reverse(String * self);
void String_trim   (String * self);
void String_reduce (String * self, size_t newLength); // length does not include '\0'
void String_cut    (String * self, size_t left, size_t right); // results to string between left and right

size_t String_getLine(String * self);
size_t String_readLine  (FILE * file, String * self);
size_t String_readFrom  (FILE * file, String * self); 
size_t String_appendFrom(FILE * file, String * self);

size_t String_readFromPath  (const char * path, String * self);
size_t String_appendFromPath(const char * path, String * self);

size_t String_readFromPathString  (const String * path, String * self);
size_t String_appendFromPathString(const String * path, String * self);

// return value is the new strings length
size_t String_writeFormat (String * self, char * fmt, ...); // rewrites string (all previous data is discarded)
size_t String_appendFormat(String * self, char * fmt, ...);

// return value and usage is totaly analogous to sscanf()
int String_scanFormat(String * self, char * fmt, ...);

void String_appendCsvFormatedChars(String * self, const char * src);
void String_appendCsvFromated     (String * self, const String * src);

enum {CSV_DEFORMAT_ERROR = -2,
      CSV_DEFORMAT_UTERMINATED_STRING = -1
};
signed long long String_appendCsvDeformated(String * self, const String * src, size_t start);

#endif // SSTRING_H