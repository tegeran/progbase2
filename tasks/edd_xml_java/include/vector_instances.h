//
// Created by tegeran on 19.04.18.
//

#ifndef EDD_XML_JAVA_VECTOR_INSTANCES_H
#define EDD_XML_JAVA_VECTOR_INSTANCES_H

#include "vector.h"

typedef const char * const_char_p;
typedef void * void_p;
VECTOR_TEMPLATE_PROTOTYPE(void_p)
VECTOR_TEMPLATE_PROTOTYPE(const_char_p)


#endif //EDD_XML_JAVA_VECTOR_INSTANCES_H
