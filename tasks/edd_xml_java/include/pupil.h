//
// Created by tegeran on 18.04.18.
//

#ifndef EDD_XML_JAVA_PUPIL_H
#define EDD_XML_JAVA_PUPIL_H

#include "sstring.h"
#include <stdbool.h>

typedef struct __Pupil {
	String *           name;
	String *           class;
	unsigned short     age;
	float              score;
	Vector(String_p) * textbooks;
	bool               alive;
} Pupil;

#define _Pupil(NAME, CLASS, AGE, SCORE, TEXTBOOKS, ALIVE) \
((Pupil){.name = (NAME),      .class = (CLASS),\
         .age  = (AGE),       .score = (SCORE),\
    .textbooks = (TEXTBOOKS), .alive = (ALIVE)})

String * Pupil_toNewXml(Pupil * pup);
Pupil * Pupil_newFromXmlFile(const char * path);

Pupil * Pupil_new(void);

void Pupil_free(Pupil * pup);
void Pupil_freeLocal(Pupil * pup);
void Pupil_print(const Pupil * pup);

#endif //EDD_XML_JAVA_PUPIL_H
