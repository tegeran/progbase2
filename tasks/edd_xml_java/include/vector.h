//
// Created by tegeran on 18.04.18.
//

/*
 * Quick reference:
 * In order to use Vector(Type) of an arbitrary Type, an instantiation of particular
 * kind of Vector(Type) must be provided in the first place.
 * To provide an instantiation simply put macro VECTOR_TEMPLATE_PROTOTYPE(Type) to <header>.h file and
 * VECTOR_TEMPLATE_IMPL(Type) to <source>.c file.
 * VECTOR_TEMPLATE_INSTANCE(Type) can be used if particular kind of Vector(Type)
 * is used only in one translation unit (deprecated, as possible ODR(one definition rule)
 * violation, because it provides both declaration and definition of Vector(Type)).
 *
 * General expample of usage:
 *
 * header.h:
 * VECTOR_TEMPLATE_PROTOTYPE(int)
 *
 * typedef char * char_p; // typedef must be provided for verbose types (types which contain spaces)
 *						  // use Vector_p(Type) macro to get Vector(Type) * type
 *						  // or  Vector_pp(Type) to get Vector(Type) ** type
 *						  // it is useful for multidimensional vectors
 *						  // e.g. Vector(Vector_p(int))
 * VECTOR_TEMPLATE_PROTOTYPE(char_p)
 * ...
 *
 * source.c:
 * VECTOR_TEMPLATE_IMPL(int)
 * VECTOR_TEMPLATE_IMPL(char_p)
 * ...
 *
 * main.c:
 * #include "stdio.h"
 * #include "header.h"
 * int main(void){
 * 		Vector(int) * vect = Vector_new(int);
 * 		Vector_pushBack(vect, 23);
 * 		Vector_append(vect, 87, 34, 54, 65);
 * 		foreach_v(int, i, vect){ // read
 * 			printf("%i", i);
 * 		}
 * 		foreach_vptr(int *, i, vect){ // modify and read
 * 			*i += 2;
 * 		}
 * 		for (size_t i = 0; i < Vector_size(vect); ++i){ // general for loop
 * 			printf("%i", *Vector_at(vect, i));
 *
 * 			// Vector_at() returns a pointer to the value,
 * 			// so that it is possible to modify it in-place
 * 			// e.g. : *Vector_at(vect, i) = 37;
 * 		}
 * 		Vector_free(vect); // explicit call to Vector_free() must be performed in this case
 *
 *		// auto_ provides automatic free of Vector, works only for poiner to Vector,
 *		// so asterisk ~~~~~V  must be provided to emphasize that it is a pointer
 * 		auto_Vector(char_p) * strs = Vector_new(char_p, free); // optional items destructor function
 * 		Vector_add(strs, strdup("str2"));
 * 		Vector_add(strs, 0, strdup("str1"));
 * 		Vector_prepend(strs, strdup("str-1"), strdup("0"));
 *      foreach_v(const char *, ch, strs){
 *      	puts(ch);
 *      }
 *		free(Vector_removeAt(strs, 1));
 *		Vector_destroyAt(strs, 1); // if no items destructor provided, simple Vector_removeAt() is called
 *
 *      // auto_Vector(Type) * is implicitly freed after escaping the block it was declared in
 *      // so no explicit Vector_free() is required, otherwise undefined behaviour (double free)
 *      // all items remaining in vector will be destroyed if items destructor function was provided
 *
 * }
 *
 *
 *
 *
 */



#ifndef EDD_XML_JAVA_VECTOR_H
#define EDD_XML_JAVA_VECTOR_H
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <ctype.h>
#include "error.h"
#include "extesion.h"

void * __malloc(size_t size);
void * __realloc(void * old, size_t size);

// https://stackoverflow.com/a/11035347/92593
#define Vector_expansion(Type) Vector_##Type
#define Vector_p_Expansion(Type) Vector_p_##Type
#define Vector_pp_Expansion(Type) Vector_pp_#Type

/*
 * VECTOR TYPE IS STATED BY THE FOLLOWING MACRO:
 * Vector(Type) is a substitution for Vector<Type>.
 * Analogous to C++ template arguments identifying
 */
#define Vector(Type) Vector_expansion(Type)
#define Vector_p(Type)  Vector_p_Expansion(Type)
#define Vectpr_pp(Type) Vector_pp_Expansion(Type)

#define auto_Vector(Type) AUTO_DESTR(Vector_free_p_##Type) Vector(Type)

#define v_data(vector) (vector)->items
#define foreach_v(Type, variable, vector)						  \
for (__extension__ __auto_type ___it_##variable = (vector)->items;\
     ___it_##variable;                                            \
     ___it_##variable = nullptr)                                  \
for (Type variable;	                                              \
	(___it_##variable != (vector)->items + (vector)->size)	      \
		? ((variable = *___it_##variable), true)	              \
		: false;                                                  \
	++___it_##variable)

#define foreach_vptr(Type, var, vector)                     \
for (size_t __i_##var = 0;                                  \
     __i_##var < (vector)->size;                            \
     __i_##var = (vector)->size)                            \
for (Type var = (vector)->items;                            \
	 __i_##var < (vector)->size;                            \
	 var = (vector)->items + (++__i_##var))


#define VECTOR_INITIAL_CAPACITY 16

// BEWARE, if Type cosists of several words separated by space e.g. "unsigned long long" or "const char *"
// you have to provide a typedef for it e.g. ULongLong or const_char_p
// EVERYTHING IS PRIVATE!!! Interaction with Vectors fields causes undefined behaviour
#define VECTOR_TEMPLATE_PROTOTYPE_EXPANSION(Type)                                                      \
																			                           \
typedef struct __VectorFunctions_##Type VectorFunctions_##Type;							               \
typedef struct __Vector_##Type {													                   \
	Type * items;              													                       \
	size_t size;																                       \
	size_t capacity;															                       \
	VectorFunctions_##Type * namespace; 											                   \
	void (*destrItems)(Type);															               \
} Vector(Type);																	                       \
																				                       \
typedef Vector(Type) *  Vector_p_##Type;                                                               \
typedef Vector(Type) ** Vector_pp_##Type;                                                              \
			    					 													               \
typedef signed char (* Comparator_##Type)(Type, Type);                                                 \
Vector(Type) * Vector_newReserved_##Type(size_t size, void (*itemDestr)(Type));					       \
Vector(Type) * Vector_new_##Type        (void (*itemDestr)(Type));                                     \
Vector(Type) * Vector_newArr_##Type     (Type * arr, size_t size, void (*itemDestr)(Type));            \
		void   Vector_free_p_##Type     (Vector(Type) **self);                                         \
        void   Vector_free_##Type       (Vector(Type) * self);                                         \
        void   Vector_reserve_##Type    (Vector(Type) * self, size_t reservation);                     \
        void   Vector_clear_##Type      (Vector(Type) * self);							               \
        void   Vector_clearWhole_##Type (Vector(Type) * self);    						               \
		size_t Vector_size_##Type	    (Vector(Type) * self);							               \
        Type * Vector_at_##Type		    (Vector(Type) * self, size_t index);			               \
		void   Vector_set_##Type	    (Vector(Type) * self, size_t index, Type value);               \
		void   Vector_add_##Type        (Vector(Type) * self, size_t index, Type value);               \
	    void   Vector_destroyAt_##Type  (Vector(Type) * self, size_t index);                           \
		Type   Vector_removeAt_##Type   (Vector(Type) * self, size_t index);                           \
		bool   Vector_empty_##Type	    (Vector(Type) * self);						                   \
  const char * Vector_type_##Type       (Vector(Type) * self);							               \
      size_t   Vector_addToSorted_##Type(Vector(Type) * self, Type entity, Comparator_##Type compare); \
       void    Vector_pushArray_##Type  (Vector(Type) * self, size_t index, Type * arr, size_t size);  \
                                                                                                       \
typedef struct __VectorFunctions_##Type {										                       \
        void   (*free      ) (Vector(Type) * self);                                                    \
		void   (*reserve   ) (Vector(Type) * self, size_t reservation);                                \
        void   (*clear     ) (Vector(Type) * self);							                           \
        void   (*clearWhole) (Vector(Type) * self);    						                           \
		size_t (*size	   ) (Vector(Type) * self);							                           \
        Type * (*at		   ) (Vector(Type) * self, size_t index);				                       \
		void   (*set	   ) (Vector(Type) * self, size_t index, Type value);                          \
		void   (*pushArray ) (Vector(Type) * self, size_t index, Type * arr, size_t size);             \
		void   (*add       ) (Vector(Type) * self, size_t index, Type value);                          \
		void   (*destroyAt ) (Vector(Type) * self, size_t index);                                      \
		Type   (*removeAt  ) (Vector(Type) * self, size_t index);                                      \
		bool   (*empty	   ) (Vector(Type) * self);						                               \
  const char * (*type      ) (Vector(Type) * self);							                           \
      size_t   (*addToSorted)(Vector(Type) * self, Type entity, Comparator_##Type compare);            \
} VectorFunctions_##Type;							 						                           \

#define VECTOR_TEMPLATE_PROTOTYPE(Type) VECTOR_TEMPLATE_PROTOTYPE_EXPANSION(Type)


#define Vector_new_Expansion(Type, destr, ...)      Vector_new_##Type(destr)
#define Vector_add_Expansion(self, value, index, ...) ((self)->namespace->add((self), (index), (value)))

#define Vector_newArr_Expansion(Type, arr, size, destr, ...) Vector_newArr_##Type(arr, size, destr)
#define Vector_newReserved_Expansion(Type, size, destr, ...) Vector_newReserved_##Type(size, destr)
/*
 *  PUBLIC INTERFACE:
 */

/*
 * auto_Vector(Type) * vect = Vector_new(Type, destructor);
 * This statement defines a new initialized Vector<Type> * with items of type <Type> on stack
 * if items use heap memory, optional destructor argument can be specified
 * to free all items after the local vector variable is about to be destroyed
 * Vector and all of its items (if destructor for them was specified and it is not nullptr) will be freed
 * after the local pointer variable of this vector is destroyed
 */

#define arr(...) {__VA_ARGS__}
#define Vector_data(self)                  ((self)->items)
#define Vector_c_arr(self)                 (Vector_data(self))

#define Vector_newReserved(Type, ...)      (Vector_newReserved_Expansion(Type, __VA_ARGS__, nullptr, nullptr))
#define Vector_new(...)              	   (Vector_new_Expansion(__VA_ARGS__, nullptr, nullptr))
#define Vector_newInits(Type, destr, ...)  (Vector_newArr_##Type((Type[])arr(__VA_ARGS__), sizeat((Type[])arr(__VA_ARGS__)), destr))
#define Vector_newArr(Type, arr, ...)      (Vector_newArr_Expansion(Type, arr, __VA_ARGS__, nullptr, nullptr))
#define Vector_free(self)                  ((self)->namespace->free       ((self)))
#define Vector_clear(self)                 ((self)->namespace->clear      ((self)))
#define Vector_clearWhole(self)            ((self)->namespace->clearWhole ((self)))
#define Vector_size(self)                  ((self)->namespace->size       ((self)))
#define Vector_at(self, index)             ((self)->namespace->at         ((self), (index)))
#define Vector_set(self, index, value)     ((self)->namespace->set        ((self), (index), (value)))
#define Vector_add(self, ...)              (Vector_add_Expansion          ((self), __VA_ARGS__, Vector_size(self), NULL))
#define Vector_insert(self, index, value)  (Vector_add((self), (value), (index)))
#define Vector_pushBack(self, value)	   (Vector_add                    ((self), (value)))
#define Vector_pushFront(self, value)      (Vector_add                    ((self), (value), 0))
#define Vector_removeAt(self, index)       ((self)->namespace->removeAt   ((self), (index)))
#define Vector_destroyAt(self, index)      ((self)->namespace->destroyAt  ((self), (index)))
#define Vector_popBack(self)               ((self)->namespace->removeAt   ((self), Vector_size(self) - 1))
#define Vector_popFront(self)              ((self)->namespace->removeAt   ((self), 0))
#define Vector_empty(self)				   ((self)->namespace->empty      ((self)))
#define Vector_type(self)				   ((self)->namespace->type       ((self)))
#define Vector_back(self)			       ((self)->namespace->at         ((self), Vector_size(self) - 1))
#define Vector_front(self)				   ((self)->namespace->at         ((self), 0))
#define Vector_addToSorted(self, entity, comparator) ((self)->namespace->addToSorted((self), (entity), (comparator)))
#define Vector_valdestr(self)              ((self)->destrItems)
#define Vector_setValdestr(self, valdestr) ((self)->destrItems = (valdestr))
#define Vector_noValdestr(self)            ((self)->destrItems = nullptr)
#define Vector_destroyBack(self)           ((self)->namespace->destroyAt  ((self), Vector_size(self) - 1))
#define Vector_destroyFront(self)          ((self)->namespace->destroyAt  ((self), 0))
#define Vector_indexOf(self, item)         ((item) - (self)->items)

#define Vector_pushArray(self, index, arr, size)\
 ((self)->namespace->pushArray((self), (index), (arr), (size)))

#define Vector_pushItems(self, index, ...) \
 ((self)->namespace->pushArray((self), (index), ((__typeof(*self->items)[])arr(__VA_ARGS__)),\
										  sizeat((__typeof(*self->items)[])arr(__VA_ARGS__))))

#define Vector_append(self, ...) (Vector_pushItems((self), (self)->size, __VA_ARGS__))
#define Vector_prepend(self, ...)(Vector_pushItems((self), 0, __VA_ARGS__ ))

#define VECTOR_TEMPLATE_IMPL_EXPANSION(Type)												                  \
															                                      \
VectorFunctions_##Type typeGlobalFunctions_##Type = {                                             \
	.free        = Vector_free_##Type,		                                                      \
	.reserve     = Vector_reserve_##Type,		                                                  \
	.clear       = Vector_clear_##Type,		                                                      \
	.clearWhole  = Vector_clearWhole_##Type,	                                                  \
	.size        = Vector_size_##Type,		                                                      \
	.at          = Vector_at_##Type,			                                                  \
	.set         = Vector_set_##Type,		                                                      \
	.pushArray   = Vector_pushArray_##Type,                                                       \
	.add         = Vector_add_##Type,		                                                      \
	.destroyAt   = Vector_destroyAt_##Type,	                                                      \
	.removeAt    = Vector_removeAt_##Type,	                                                      \
	.empty       = Vector_empty_##Type,		                                                      \
	.type        = Vector_type_##Type,		                                                      \
	.addToSorted = Vector_addToSorted_##Type,                                                     \
};											                                                      \
void   Vector_free_p_##Type     (Vector(Type) **self){                                            \
	Vector_free_##Type(*self);																	  \
}                                                                                                 \
Vector(Type) * Vector_newReserved_##Type (size_t size, void (*itemDestr)(Type)){                  \
	Vector(Type) * newbie = __malloc(sizeof(Vector(Type)));                                       \
	*newbie = (Vector(Type)){                                                                     \
        .items = __malloc(size * sizeof(Type)),                                                   \
        .size = 0,                                                                                \
        .capacity = size, 			                                                              \
        .namespace = &typeGlobalFunctions_##Type,                                                 \
		.destrItems = itemDestr                                                                   \
	};                                                                                            \
	return newbie;                                                                                \
}                                                                                                 \
Vector(Type) * Vector_new_##Type (void (*itemDestr)(Type)){                                       \
	return Vector_newReserved_##Type(VECTOR_INITIAL_CAPACITY, itemDestr);                         \
}                                                                                                 \
	                                                                                              \
Vector(Type) * Vector_newArr_##Type     (Type * arr, size_t size, void (*itemDestr)(Type)){	      \
	Vector(Type) * newbie = __malloc(sizeof(Vector(Type)));	                                      \
	*newbie = (Vector(Type)){	                                                                  \
        .items = __malloc(size * sizeof(Type)),	                                                  \
        .size = size,	                                                                          \
        .capacity = size,	                                                                      \
        .namespace = &typeGlobalFunctions_##Type,	                                              \
        .destrItems = itemDestr	                                                                  \
    };	                                                                                          \
    memcpy(newbie->items, arr, size * sizeof(Type));	                                          \
	return newbie;	                                                                              \
}	                                                                                              \
	                                                                                              \
void Vector_free_##Type (Vector(Type) * self){                                                    \
	Debug_exists(self);                                                                           \
	if (self->destrItems){                                                                        \
		for (size_t i = 0; i < self->size; ++i){                                                  \
			self->destrItems(self->items[i]);                                                     \
		}                                                                                         \
	}                                                                                             \
	free(self->items);                                                                            \
	free(self);                                                                                   \
}                                                                                                 \
                                                                                                  \
                                                                                                  \
void Vector_clear_##Type (Vector(Type) * self){                                                   \
	Debug_exists(self);                                                                           \
	self->size = 0;                                                                               \
}                                                                                                 \
void Vector_clearWhole_##Type (Vector(Type) * self){                                              \
	Debug_exists(self);                                                                           \
	if (!self->destrItems) {                                                                      \
		Debug_shutdown("No destructor for items provided");                                       \
		return;                                                                                   \
	}                                                                                             \
	for (size_t i = 0; i < self->size; ++i){                                                      \
		self->destrItems(self->items[i]);                                                         \
	}                                                                                             \
	self->size = 0;                                                                               \
}                                                                                                 \
size_t Vector_size_##Type (Vector(Type) * self){                                                  \
	Debug_exists(self);                                                                           \
	return self->size;                                                                            \
}                                                                                                 \
Type * Vector_at_##Type	(Vector(Type) * self, size_t index){                                      \
	Debug_exists(self);                                                                           \
	Debug_maxbound(index, self->size - 1);                                                        \
	return self->items + index;                                                                   \
}                                                                                                 \
void Vector_set_##Type (Vector(Type) * self, size_t index, Type value){                           \
	Debug_exists(self);                                                                           \
	self->items[index] = value;                                                                   \
}                                                                                                 \
                                                                                                  \
static void Vector_ensureCapacity_##Type(Vector(Type) * self){                                    \
	Debug_exists(self);                                                                           \
	if (self->size >= self->capacity){                                                            \
		self->capacity = self->capacity << 1;                                                     \
		Type * newMem = __malloc(self->capacity * sizeof(Type));                                  \
		memmove(newMem, self->items, self->size * sizeof(Type));                                  \
		free(self->items);                                                                        \
		self->items = newMem;                                                                     \
	}                                                                                             \
}                                                                                                 \
                                                                                                  \
void Vector_add_##Type (Vector(Type) * self, size_t index, Type value){                           \
	Debug_exists(self);                                                                           \
	Debug_maxbound(index, self->size);                                                            \
	Vector_ensureCapacity_##Type(self);                                                           \
	for (size_t i = self->size++; i > index; --i){                                                \
		self->items[i] = self->items[i - 1];                                                      \
 	}                                                                                             \
	self->items[index] = value;                                                                   \
}                                                                                                 \
Type Vector_removeAt_##Type (Vector(Type) * self, size_t index){                                  \
    Debug_exists(self);                                                                           \
    Debug_suppose(self->size != 0, "An attempt to remove value from empty vector was made");      \
    Debug_maxbound(index, self->size - 1);                                                        \
	Type badboy = self->items[index];                                                             \
    for (size_t i = index + 1; i < self->size; ++i){                                              \
        self->items[i - 1] = self->items[i];                                                      \
    }                                                                                             \
	--self->size;                                                                                 \
	return badboy;                                                                                \
}                                                                                                 \
                                                                                                  \
void Vector_destroyAt_##Type (Vector(Type) * self, size_t index){                                 \
	Debug_exists(self);                                                                           \
	Debug_suppose(self->size != 0, "An attempt to remove value from empty vector was made");      \
	Debug_maxbound(index, self->size - 1);                                                        \
	if (self->destrItems){                                                                        \
		self->destrItems(self->items[index]);                                                     \
	}                                                                                             \
	Vector_removeAt_##Type(self, index);                                                          \
}                                                                                                 \
bool   Vector_empty_##Type (Vector(Type) * self){                                                 \
	return !self->size;                                                                           \
}                                                                                                 \
const char * Vector_type_##Type (Vector(Type) * self){                                            \
	return #Type;                                                                                 \
}																					              \
typedef signed char (* Comparator_##Type)(Type, Type);	                                          \
size_t Vector_addToSorted_##Type(Vector(Type) * self, Type entity, Comparator_##Type compare){	  \
	Debug_exists(self);	                                                                          \
	Debug_exists(compare);	                                                                      \
		                                                                                          \
	for (size_t i = 0; i < Vector_size(self); ++i){	                                              \
		if (compare(*Vector_at(self, i), entity) != -1){	                                      \
			Vector_add(self, entity, i);	                                                      \
			return i;	                                                                          \
		}	                                                                                      \
	}	                                                                                          \
	Vector_pushBack(self, entity);	                                                              \
	return Vector_size(self) - 1;	                                                              \
}	                                                                                              \
void Vector_reserve_##Type    (Vector(Type) * self, size_t reservation){                          \
	Debug_exists(self);                                                                           \
	if (reservation <= self->capacity){                                                           \
		return;                                                                                   \
	}                                                                                             \
	self->items = __realloc(self->items, reservation * sizeof(Type));                             \
	self->capacity = reservation;                                                                 \
}                                                                                                 \
void Vector_pushArray_##Type  (Vector(Type) * self, size_t index, Type * arr, size_t size){       \
	Debug_exists(self);                                                                           \
	Debug_exists(arr);	                                                                          \
	Debug_maxbound(index, self->size);	                                                          \
	Vector_reserve_##Type(self, self->size + size);	                                              \
	memmove(self->items + index + size, self->items + index, (self->size - index) * sizeof(Type));\
	memcpy(self->items + index, arr, size * sizeof(Type));                                        \
	self->size += size;                                                                           \
}

#define VECTOR_TEMPLATE_IMPL(Type) VECTOR_TEMPLATE_IMPL_EXPANSION(Type)

#define VECTOR_TEMPLATE_INSTANCE(Type) VECTOR_TEMPLATE_PROTOTYPE(Type) VECTOR_TEMPLATE_IMPL(Type)

#endif //EDD_XML_JAVA_VECTOR_H
