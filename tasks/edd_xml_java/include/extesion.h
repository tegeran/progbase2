#pragma once

#define nullptr ((void *)0)
#define sizeat(arr) (sizeof(arr)/sizeof((arr)[0]))
#define slitlen(literal) (sizeat(literal) - 1)

#define imply(presupposition, conclusion) (!(presupposition) || (conclusion))

#define AUTO_DESTR(destructor) __extension__ __attribute((cleanup(destructor)))

#define swap(var1, var2) {\
	__extension__ __auto_type keeper = var1;\
	var1 = var2;\
	var2 = keeper;\
}
