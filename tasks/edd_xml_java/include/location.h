#pragma once

#include <stdbool.h>

typedef struct __Location {
    signed int y;
    signed int x;
} Location;

#define _Location(Y, X) (Location){.y = (Y), .x = (X)}

Location * Location_new(signed int y, signed int x);
void Location_free(Location * self);

signed int Location_getY(Location * self);
signed int Location_getX(Location * self);

signed int Location_setY(Location * self, signed int y);
signed int Location_setX(Location * self, signed int x);
void Location_setYX(Location * self, signed int y, signed int x);
bool Location_equals(Location * self, Location * other);