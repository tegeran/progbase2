#pragma once
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include "extesion.h"
#include "errno.h"

#ifndef NO_DEBUG
    #define Debug_exists(expression)  assert((expression) && "An attempt to dereference NULL pointer was made");
    #define Debug_UTF8() assert(strcmp(setlocale(0, NULL), "C") && "An attempt to print wide character without UTF-8 format was made")
    #define Debug_inbound(leftBound, suspect, rightBound) assert((suspect) >= (leftBound) && (suspect) <= (rightBound) && "Index out of bounds exception");
    #define Debug_maxbound(suspect, rightBound) assert((suspect) <= (rightBound) && "Index out of bounds exception");
    #define Debug_minbound(suspect, leftBound) assert((suspect) >= (leftBound) && "Index out of bounds exception");
    #define Debug_suppose(expression, errorMessage) assert((expression) && (errorMessage))
    #define Debug_shutdown(reason) assert(0 && (reason))

        #ifndef NO_DEBUG_TIPS
            #define Debug_tip(expression, tipMessage) assert((expression) && (tipMessage))
        #else
            #define Debug_tip(expression, tipMessage)
        #endif

	#ifndef NO_ERRNO_LOG
		#define Debug_logErrno() {printf("[errno =  %i]", errno); fflush(stdout);}
	#else
		#define Debug_logErrno()
	#endif
#else
    #define Debug_exists(expression)
    #define Debug_UTF8() 
    #define Debug_inbound(leftBound, suspect, rightBound)
    #define Debug_maxbound(suspect, rightBound)
    #define Debug_minbound(suspect, leftBound) 
    #define Debug_suppose(expression, errorMessage)
    #define Debug_shutdown(reason)

	#define Debug_logErrno()
    #define Debug_tip(expression, tipMessage)

    #define TRACE_CALL() 
    #define TRACE_CALL_LINE()
#endif

void Error_checkHeap(void * heapMemory);