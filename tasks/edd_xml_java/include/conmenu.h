//
// Created by tegeran on 20.03.18.
//
#pragma once
#include "cui.h"
#include "vector.h"
#include "vector_instances.h"

#define auto_ConMenu AUTO_DESTR(ConMenu_free_p) ConMenu

typedef struct __ConMenu ConMenu;

ConMenu * ConMenu_new(Vector(const_char_p) * options);
void ConMenu_free(ConMenu * self);
void ConMenu_freeWhole(ConMenu * self);
void ConMenu_free_p(ConMenu ** self);

Vector(const_char_p) * ConMenu_options(ConMenu * self);
Vector(const_char_p) * ConMenu_setOptions(ConMenu * self, Vector(const_char_p) * newOptions);

typedef enum __ConMenuResponse{
	CMR_Idle = -3,
	CMR_MovedSelector = -2,
	CMR_Escaped = -1 // escape sequnse fed
} ConMenuResponse;

// if returned value > 0, then user has chosen, otherwise one of the states above is returned
signed long long ConMenu_react(ConMenu *self, int ch);
signed long long ConMenu_listen(ConMenu * self);

#define ConMenu_post_Expansion(menu, y, x, ...) _ConMenu_post(menu, y, x)
#define ConMenu_post(...) ConMenu_post_Expansion(__VA_ARGS__, 1, 1, 1)
void _ConMenu_post(ConMenu * self, unsigned short y, unsigned short x);
void ConMenu_refresh(ConMenu * self);
void ConMenu_unpost(ConMenu * self);

