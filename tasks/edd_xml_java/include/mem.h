
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// UNIVERSAL TESTING FUNCTIONS

#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "sstring.h"
#include "vector.h"
#include "extesion.h"



size_t Mem_readFile(const char * filePath, char *string, size_t stringSize);
size_t Mem_getFileSize(FILE * file);

bool Mem_fileExists(const char * filePath);


typedef enum{
    MEM_FAILURE = -1,
    MEM_SUCCESS
} MemStatus;

void * Mem_reallocate(void * ptr, size_t size);
void * Mem_malloc(size_t size);
void * Mem_calloc(size_t size);


typedef enum {
    MEM_REG_FILE,
    MEM_DIR,
    MEM_INDIR,
    MEM_DEFAULT,
    MEM_PUBLIC_FILE,
    MEM_PUBLIC_DIR
} MemDirType;

MemStatus Mem_getDirNames (const char   * directory, MemDirType type, Vector(String_p) * listOfNames);

void Mem_printFilesStatistic(const char * path,  size_t * letters, size_t * lines, size_t * words);