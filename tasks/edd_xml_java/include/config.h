//
// Created by tegeran on 21.04.18.
//

#ifndef EDD_XML_JAVA_CONFIG_H
#define EDD_XML_JAVA_CONFIG_H

#include "sstring.h"

typedef struct __Config Config;

Config * Config_readNew(void);
Config * Config_createNew(String * path);

void Config_free(Config * self);

void Config_setPath(Config * self, String * path);
String * Config_path(Config * self);



#endif //EDD_XML_JAVA_CONFIG_H
