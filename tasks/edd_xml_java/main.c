#include <stdio.h>
#include <libxml/parser.h>
#include "pupil.h"
#include "sstring.h"
#include <progbase/events.h>
#include <progbase/console.h>
#include <errno.h>
#include "config.h"
#include "conmenu.h"
#include "mem.h"

enum {
	Event_KeyFed,

	Event_GoToPupilMenu,
	Event_GoToMainMenu,
	Event_GoToConfigMenu,

	Event_XmlFailure,
	Event_GoToFailureMenu,
	Event_XmlCreateDefault,
//	Event_XmlTryNewFile,

	Event_NewConfig,
	Event_QueryConfig,
	Event_SentConfig,
	Event_ChangedConfig,

	Event_NewPupilName,
	Event_NewPupilClass,
	Event_NewPupilAge,
	Event_NewPupilScore,
	Event_NewPupilAlive,
	Event_NewPupilAddBook,
	Event_NewPupilRemoveBook
};

#define extern_emitExpansion(id, data, destr, ...) EventSystem_emit(Event_new(nullptr, id, data, destr))
#define extern_emit(...) extern_emitExpansion(__VA_ARGS__, nullptr, nullptr, nullptr)

#define emit_Expansion(id, data, destr, ...) EventSystem_emit(Event_new(self, id, data, destr))
#define emit(...) (emit_Expansion(__VA_ARGS__, nullptr, nullptr, nullptr))

void Handler_xmlFailure(EventHandler *self, Event *event);
void Handler_mainMenu(EventHandler * self, Event * event);
void Handler_configMenu(EventHandler * self, Event * event);
void Handler_dataMenu(EventHandler * self, Event * event);
void Handler_data(EventHandler * self, Event * event);
void Handler_config(EventHandler * self, Event * event);
void Handler_keyListener(EventHandler * self, Event * event);
static void EventSystem_addMenuHandler(void (*handler)(EventHandler *, Event *));

static Config * readConfig(void);

int main() {
	freopen ("errorlog.err", "a", stderr); // http://www.cplusplus.com/forum/general/48883/
	Cui_recover();
	EventSystem_init();

	EventSystem_addHandler(EventHandler_new(malloc(sizeof(int)), free, Handler_keyListener));
	EventSystem_addMenuHandler(Handler_mainMenu);
	EventSystem_addHandler(EventHandler_new(nullptr, nullptr, Handler_config));
	EventSystem_addHandler(EventHandler_new(Pupil_new(), (DestructorFunction)Pupil_free, Handler_data));
	EventSystem_loop();


	EventSystem_cleanup();
	xmlCleanupParser();
	fclose(stderr);
	return EXIT_SUCCESS;
}


static void EventSystem_addMenuHandler(void (*handler)(EventHandler *, Event *)) {
	EventSystem_addHandler(
			EventHandler_new(
					ConMenu_new(
							Vector_new(const_char_p)),
					(DestructorFunction) ConMenu_freeWhole,
					handler
			)
	);
}


void Handler_mainMenu(EventHandler * self, Event * event){
	ConMenu * menu = self->data;
	switch (event->type){
		case Event_GoToMainMenu: {
			Vector_append(ConMenu_options(menu), "> Config", "> Data",  "> Escape");
			ConMenu_post(menu);
			return;
		}
		case Event_KeyFed:{
			enum Options { Config, Data, Escape };
			switch (ConMenu_react(menu, *(int *)event->data)){
				case Config:{ emit (Event_GoToConfigMenu); return; }
				case Data:  { emit (Event_GoToPupilMenu ); return; }
				case CMR_Escaped:
				case Escape:{ EventSystem_exit(); 		   return; }
				default: return;
			}
			return;
		}
		case Event_GoToPupilMenu:{
			ConMenu_unpost(menu);
			EventSystem_removeHandler(self);
			EventSystem_addMenuHandler(Handler_dataMenu);
			return;
		}
		case Event_GoToConfigMenu:{
			ConMenu_unpost(menu);
			EventSystem_removeHandler(self);
			EventSystem_addMenuHandler(Handler_configMenu);
			return;
		}
		case Event_XmlFailure:{
			ConMenu_unpost(menu);
			EventSystem_removeHandler(self);
			return;
		}
	}
}

static String * readString(const char * msg){
	String * data = String_new();
	do {
		Cui_recover();
		Cui_printReverse(msg);
		putchar(' ');
	} while (!String_getLine(data));
	Cui_recover();
	return data;
}

void Handler_configMenu(EventHandler * self, Event * event){
	enum Options {
		ChangeConfig,
		Escape
	};
	ConMenu * menu = self->data;
	switch (event->type){
		case Event_GoToConfigMenu:{
			Vector_append(ConMenu_options(menu), "> Change config", "> Back");
			ConMenu_post(menu);
			return;
		}
		case Event_KeyFed:{
			switch (ConMenu_react(menu, *(int *)event->data)){
				case ChangeConfig:{
					ConMenu_unpost(menu);
					emit(Event_NewConfig, readString("New config:"));
					return;
				}
				case CMR_Escaped:
				case Escape:      { emit (Event_GoToMainMenu); return; }
				default: return;
			}
			return;
		}
		case Event_GoToMainMenu: {
			EventSystem_addMenuHandler(Handler_mainMenu);
		}// ................
		case Event_XmlFailure:{
			ConMenu_unpost(menu);
			EventSystem_removeHandler(self);
			return;
		}
	}
}

static void DataMenu_getData(EventHandler * self, ConMenu * menu, const char * msg, int eventId){
	ConMenu_unpost(menu);
	emit (eventId, readString(msg));
	ConMenu_post(menu);
}

void Handler_dataMenu(EventHandler * self, Event * event){
	enum Options {
		Name,
		Class,
		Age,
		Score,
		Alive,
		AddBook,
		DelBook,
		Escape
	};
	ConMenu * menu = self->data;
	switch (event->type){
		case Event_GoToPupilMenu:{
			Vector_append(ConMenu_options(menu),
						  "> Name",
						  "> Class",
						  "> Age",
						  "> Score",
						  "> Alive",
						  "> Add Textbook",
						  "> Remove book",
						  "> Back");
			ConMenu_post(menu);
			return;
		}
		case Event_KeyFed:{
			switch (ConMenu_react(menu, *(int *)event->data)){
				case Name:   { DataMenu_getData(self, menu, "New name:",        Event_NewPupilName      ); return; }
				case Class:  { DataMenu_getData(self, menu, "New class:",       Event_NewPupilClass     ); return; }
				case Age:    { DataMenu_getData(self, menu, "New age:",         Event_NewPupilAge       ); return; }
				case Score:  { DataMenu_getData(self, menu, "New score:",       Event_NewPupilScore     ); return; }
				case Alive:  { DataMenu_getData(self, menu, "New alive:",       Event_NewPupilAlive     ); return; }
				case AddBook:{ DataMenu_getData(self, menu, "New textbook:",    Event_NewPupilAddBook   ); return; }
				case DelBook:{ DataMenu_getData(self, menu, "Delete textbook:", Event_NewPupilRemoveBook); return; }
				case CMR_Escaped:
				case Escape: { emit (Event_GoToMainMenu); return; }
				default: return;
			}
			return;
		}
		case Event_GoToMainMenu:{
			ConMenu_unpost(menu);
			EventSystem_removeHandler(self);
			EventSystem_addMenuHandler(Handler_mainMenu);
			return;
		}
	}
}

void Handler_data(EventHandler * self, Event * event){
	Pupil * pup = self->data;
	switch (event->type){
		case Event_XmlCreateDefault:{
			Pupil_free(self->data);
			self->data = Pupil_new();
			emit (Event_QueryConfig);
			emit (Event_GoToMainMenu);
			return;
		}
		case Event_ChangedConfig:{
			Pupil * newpup = Pupil_newFromXmlFile(String_c_str(event->data));
			if (!newpup){
				EventSystem_addMenuHandler(Handler_xmlFailure);
				emit (Event_XmlFailure);
				return;
			}

			Pupil_free(self->data);
			self->data = newpup;
			emit (Event_GoToMainMenu);
			return;
		}
		case Event_SentConfig:{
			auto_String * xml = Pupil_toNewXml(pup);
			String_writeToPathString(event->data, xml);
			return;
		}
		case Event_NewPupilName:{
			String_free(pup->name);
			pup->name = event->data;
			emit (Event_QueryConfig);
			return;
		}
		case Event_NewPupilClass:{
			String_free(pup->class);
			pup->class = event->data;
			emit (Event_QueryConfig);
			return;
		}
		case Event_NewPupilAge:{
			long age = strtol(String_c_str(event->data), nullptr, 10);
			//
			if (errno){
				errno = 0;
			} else if (age <= USHRT_MAX && age >= 0){
				//
				pup->age = (unsigned short)age;
				emit (Event_QueryConfig);
			}
			String_free(event->data);
			return;
		}
		case Event_NewPupilScore:{
			float score = strtof(String_c_str(event->data), nullptr);
			if (!errno) {
				pup->score = score;
				emit (Event_QueryConfig);
			} else {
				errno = 0;
			}
			String_free(event->data);
			return;
		}
		case Event_NewPupilAlive:{
			String_trim(event->data);
			if (Chars_equals(String_c_str(event->data), "true")){
				pup->alive = true;
				emit (Event_QueryConfig);
			} else if (Chars_equals(String_c_str(event->data), "false")){
				pup->alive = false;
				emit (Event_QueryConfig);
			}
			String_free(event->data);
			return;
		}
		case Event_NewPupilAddBook:{
			Vector_pushBack(pup->textbooks, event->data);
			emit (Event_QueryConfig);
			return;
		}
		case Event_NewPupilRemoveBook:{
			foreach_vptr(String **, str, pup->textbooks){
				if (String_equals(*str, event->data)){
					Vector_destroyAt(pup->textbooks, Vector_indexOf(pup->textbooks, str));
//					String_free(event->data);
					emit (Event_QueryConfig);
					break;
				}
			}
			String_free(event->data);
			return;
		}
	}
}

static Config * readConfig(void){
	Config * cfg = Config_readNew();
	if (!cfg){
		return Config_createNew(readString("Failed to read \"configuration.xml\". Creating new one.\nSpecify new data filepath:"));
	}
	else return cfg;
}

void Handler_config(EventHandler * self, Event * event){
	switch (event->type){
		case Event_NewConfig:{
			Debug_suppose(!String_empty(event->data), "Empty filename violation");
			Config_setPath(self->data, event->data);
			emit (Event_ChangedConfig, Config_path(self->data));
			return;
		}
		case StartEventTypeId: { emit (Event_ChangedConfig, Config_path((self->data = readConfig()))); return; }
		case Event_QueryConfig:{ emit (Event_SentConfig,    Config_path(self->data));                  return; }
		case ExitEventTypeId:  { Config_free(self->data); 											   return; }
	}
}

void Handler_xmlFailure(EventHandler *self, Event *event){
	ConMenu * menu = self->data;
	switch (event->type) {
		case Event_XmlFailure:{
			emit (Event_GoToFailureMenu);
			return;
		}
		case Event_GoToFailureMenu: {
			Cui_recover();
			Vector_append(ConMenu_options(menu), "> Create default instance",
						                         "> Change configuration",
						                         "> Escape"
			);
			puts("Failed to read data file");
			ConMenu_post(menu, 2, 1);
			return;
		}
		case Event_KeyFed:{
			enum { CreateDefault, ChangeConfig, Escape };
			switch (ConMenu_react(menu, *(int *)event->data)){
				case CreateDefault:{
					emit (Event_XmlCreateDefault);
					return;
				}
				case ChangeConfig:{
					ConMenu_unpost(menu);
					emit (Event_NewConfig, readString("Specify new data filepath:"));
					return;
				}
				case CMR_Escaped:
				case Escape:{ EventSystem_exit(); return; }
				default: return;
			}
			return;
		}
		case Event_XmlCreateDefault:
		case Event_NewConfig:{
			Cui_recover();
			ConMenu_unpost(menu);
			EventSystem_removeHandler(self);
			EventSystem_addMenuHandler(Handler_mainMenu);
		}
	}
}





void Handler_keyListener(EventHandler * self, Event * event){
	switch (event->type){
		case UpdateEventTypeId:{
			if (Console_isKeyDown()){
				*(int *)self->data = getchar();
				emit (Event_KeyFed, self->data);
			} else if (errno == EAGAIN){
				errno = 0;
			}
			return;
		}
	}
}

